
package za.co.ominsure.synapse.iwyze.lob.backend.search.payment.vo;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * Is mapped into TIA metadata type "obj_payment_method".
 * 
 * <p>Java class for PaymentMethod complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PaymentMethod">
 *   &lt;complexContent>
 *     &lt;extension base="{http://infrastructure.tia.dk/schema/common/v2/}TIAObject">
 *       &lt;sequence>
 *         &lt;element name="paymentMethod" type="{http://infrastructure.tia.dk/schema/common/v2/}paymentMethod"/>
 *         &lt;element name="paymentDetailsNeededYN" type="{http://infrastructure.tia.dk/schema/common/v2/}tiaYN"/>
 *         &lt;element name="paymentInYN" type="{http://infrastructure.tia.dk/schema/common/v2/}tiaYN" minOccurs="0"/>
 *         &lt;element name="paymentOutYN" type="{http://infrastructure.tia.dk/schema/common/v2/}tiaYN" minOccurs="0"/>
 *         &lt;element name="payPer" type="{http://infrastructure.tia.dk/schema/account/v2/}payPer"/>
 *         &lt;element name="daysBeforeDuedate" type="{http://infrastructure.tia.dk/schema/account/v2/}daysBeforeDuedate" minOccurs="0"/>
 *         &lt;element name="itemText" type="{http://infrastructure.tia.dk/schema/account/v2/}itemText" minOccurs="0"/>
 *         &lt;element name="approveCollectionsYN" type="{http://infrastructure.tia.dk/schema/common/v2/}tiaYN" minOccurs="0"/>
 *         &lt;element name="approvePaymentsYN" type="{http://infrastructure.tia.dk/schema/common/v2/}tiaYN" minOccurs="0"/>
 *         &lt;element name="deferredAllocationYN" type="{http://infrastructure.tia.dk/schema/common/v2/}tiaYN" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PaymentMethod", propOrder = {
    "paymentMethod",
    "paymentDetailsNeededYN",
    "paymentInYN",
    "paymentOutYN",
    "payPer",
    "daysBeforeDuedate",
    "itemText",
    "approveCollectionsYN",
    "approvePaymentsYN",
    "deferredAllocationYN"
})
public class PaymentMethod
    extends TIAObject
{

    @XmlElement(required = true)
    protected String paymentMethod;
    @XmlElement(required = true)
    protected String paymentDetailsNeededYN;
    @XmlElementRef(name = "paymentInYN", namespace = "http://infrastructure.tia.dk/schema/account/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> paymentInYN;
    @XmlElementRef(name = "paymentOutYN", namespace = "http://infrastructure.tia.dk/schema/account/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> paymentOutYN;
    @XmlSchemaType(name = "integer")
    protected int payPer;
    @XmlElementRef(name = "daysBeforeDuedate", namespace = "http://infrastructure.tia.dk/schema/account/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> daysBeforeDuedate;
    @XmlElementRef(name = "itemText", namespace = "http://infrastructure.tia.dk/schema/account/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> itemText;
    @XmlElementRef(name = "approveCollectionsYN", namespace = "http://infrastructure.tia.dk/schema/account/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> approveCollectionsYN;
    @XmlElementRef(name = "approvePaymentsYN", namespace = "http://infrastructure.tia.dk/schema/account/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> approvePaymentsYN;
    @XmlElementRef(name = "deferredAllocationYN", namespace = "http://infrastructure.tia.dk/schema/account/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> deferredAllocationYN;

    /**
     * Gets the value of the paymentMethod property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaymentMethod() {
        return paymentMethod;
    }

    /**
     * Sets the value of the paymentMethod property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaymentMethod(String value) {
        this.paymentMethod = value;
    }

    /**
     * Gets the value of the paymentDetailsNeededYN property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaymentDetailsNeededYN() {
        return paymentDetailsNeededYN;
    }

    /**
     * Sets the value of the paymentDetailsNeededYN property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaymentDetailsNeededYN(String value) {
        this.paymentDetailsNeededYN = value;
    }

    /**
     * Gets the value of the paymentInYN property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPaymentInYN() {
        return paymentInYN;
    }

    /**
     * Sets the value of the paymentInYN property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPaymentInYN(JAXBElement<String> value) {
        this.paymentInYN = value;
    }

    /**
     * Gets the value of the paymentOutYN property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPaymentOutYN() {
        return paymentOutYN;
    }

    /**
     * Sets the value of the paymentOutYN property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPaymentOutYN(JAXBElement<String> value) {
        this.paymentOutYN = value;
    }

    /**
     * Gets the value of the payPer property.
     * 
     */
    public int getPayPer() {
        return payPer;
    }

    /**
     * Sets the value of the payPer property.
     * 
     */
    public void setPayPer(int value) {
        this.payPer = value;
    }

    /**
     * Gets the value of the daysBeforeDuedate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getDaysBeforeDuedate() {
        return daysBeforeDuedate;
    }

    /**
     * Sets the value of the daysBeforeDuedate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setDaysBeforeDuedate(JAXBElement<Integer> value) {
        this.daysBeforeDuedate = value;
    }

    /**
     * Gets the value of the itemText property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getItemText() {
        return itemText;
    }

    /**
     * Sets the value of the itemText property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setItemText(JAXBElement<String> value) {
        this.itemText = value;
    }

    /**
     * Gets the value of the approveCollectionsYN property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getApproveCollectionsYN() {
        return approveCollectionsYN;
    }

    /**
     * Sets the value of the approveCollectionsYN property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setApproveCollectionsYN(JAXBElement<String> value) {
        this.approveCollectionsYN = value;
    }

    /**
     * Gets the value of the approvePaymentsYN property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getApprovePaymentsYN() {
        return approvePaymentsYN;
    }

    /**
     * Sets the value of the approvePaymentsYN property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setApprovePaymentsYN(JAXBElement<String> value) {
        this.approvePaymentsYN = value;
    }

    /**
     * Gets the value of the deferredAllocationYN property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDeferredAllocationYN() {
        return deferredAllocationYN;
    }

    /**
     * Sets the value of the deferredAllocationYN property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDeferredAllocationYN(JAXBElement<String> value) {
        this.deferredAllocationYN = value;
    }

}
