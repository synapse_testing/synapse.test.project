
package za.co.ominsure.synapse.iwyze.lob.backend.claim.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Additional comments
 * 
 * <p>Java class for ClaimMajorEvent complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ClaimMajorEvent">
 *   &lt;complexContent>
 *     &lt;extension base="{http://infrastructure.tia.dk/schema/claim/v2/}ClaimCatastrophe">
 *       &lt;sequence>
 *         &lt;element name="majorEventNo" type="{http://infrastructure.tia.dk/schema/common/v2/}long10" minOccurs="0"/>
 *         &lt;element name="majorEventType" type="{http://infrastructure.tia.dk/schema/common/v2/}string3" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ClaimMajorEvent", propOrder = {
    "majorEventNo",
    "majorEventType"
})
public class ClaimMajorEvent
    extends ClaimCatastrophe
{

    @XmlElement(nillable = true)
    protected Long majorEventNo;
    @XmlElement(nillable = true)
    protected String majorEventType;

    /**
     * Gets the value of the majorEventNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getMajorEventNo() {
        return majorEventNo;
    }

    /**
     * Sets the value of the majorEventNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setMajorEventNo(Long value) {
        this.majorEventNo = value;
    }

    /**
     * Gets the value of the majorEventType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMajorEventType() {
        return majorEventType;
    }

    /**
     * Sets the value of the majorEventType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMajorEventType(String value) {
        this.majorEventType = value;
    }

}
