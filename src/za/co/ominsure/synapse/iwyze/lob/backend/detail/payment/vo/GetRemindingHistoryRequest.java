
package za.co.ominsure.synapse.iwyze.lob.backend.detail.payment.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="inputToken" type="{http://infrastructure.tia.dk/schema/common/v2/}InputToken"/>
 *         &lt;element name="partyNameId" type="{http://infrastructure.tia.dk/schema/common/v2/}nameIdNo"/>
 *         &lt;element name="filterToken" type="{http://infrastructure.tia.dk/schema/account/v2/}FilterTokenRemHistory"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "inputToken",
    "partyNameId",
    "filterToken"
})
@XmlRootElement(name = "getRemindingHistoryRequest")
public class GetRemindingHistoryRequest {

    @XmlElement(required = true)
    protected InputToken inputToken;
    @XmlSchemaType(name = "unsignedLong")
    protected long partyNameId;
    @XmlElement(required = true)
    protected FilterTokenRemHistory filterToken;

    /**
     * Gets the value of the inputToken property.
     * 
     * @return
     *     possible object is
     *     {@link InputToken }
     *     
     */
    public InputToken getInputToken() {
        return inputToken;
    }

    /**
     * Sets the value of the inputToken property.
     * 
     * @param value
     *     allowed object is
     *     {@link InputToken }
     *     
     */
    public void setInputToken(InputToken value) {
        this.inputToken = value;
    }

    /**
     * Gets the value of the partyNameId property.
     * 
     */
    public long getPartyNameId() {
        return partyNameId;
    }

    /**
     * Sets the value of the partyNameId property.
     * 
     */
    public void setPartyNameId(long value) {
        this.partyNameId = value;
    }

    /**
     * Gets the value of the filterToken property.
     * 
     * @return
     *     possible object is
     *     {@link FilterTokenRemHistory }
     *     
     */
    public FilterTokenRemHistory getFilterToken() {
        return filterToken;
    }

    /**
     * Sets the value of the filterToken property.
     * 
     * @param value
     *     allowed object is
     *     {@link FilterTokenRemHistory }
     *     
     */
    public void setFilterToken(FilterTokenRemHistory value) {
        this.filterToken = value;
    }

}
