
package za.co.ominsure.synapse.iwyze.lob.backend.policy.vo;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RiskTypeGroupCollection complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RiskTypeGroupCollection">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="riskTypeGroup" type="{http://infrastructure.tia.dk/schema/policy/v2/}RiskTypeGroup" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RiskTypeGroupCollection", propOrder = {
    "riskTypeGroups"
})
public class RiskTypeGroupCollection {

    @XmlElement(name = "riskTypeGroup")
    protected List<RiskTypeGroup> riskTypeGroups;

    /**
     * Gets the value of the riskTypeGroups property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the riskTypeGroups property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRiskTypeGroups().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RiskTypeGroup }
     * 
     * 
     */
    public List<RiskTypeGroup> getRiskTypeGroups() {
        if (riskTypeGroups == null) {
            riskTypeGroups = new ArrayList<RiskTypeGroup>();
        }
        return this.riskTypeGroups;
    }

}
