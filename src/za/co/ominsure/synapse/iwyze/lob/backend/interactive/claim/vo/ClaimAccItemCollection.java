
package za.co.ominsure.synapse.iwyze.lob.backend.interactive.claim.vo;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ClaimAccItemCollection complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ClaimAccItemCollection">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="claimAccItem" type="{http://infrastructure.tia.dk/schema/claim/v2/}ClaimAccItem" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ClaimAccItemCollection", propOrder = {
    "claimAccItems"
})
public class ClaimAccItemCollection {

    @XmlElement(name = "claimAccItem")
    protected List<ClaimAccItem> claimAccItems;

    /**
     * Gets the value of the claimAccItems property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the claimAccItems property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getClaimAccItems().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ClaimAccItem }
     * 
     * 
     */
    public List<ClaimAccItem> getClaimAccItems() {
        if (claimAccItems == null) {
            claimAccItems = new ArrayList<ClaimAccItem>();
        }
        return this.claimAccItems;
    }

}
