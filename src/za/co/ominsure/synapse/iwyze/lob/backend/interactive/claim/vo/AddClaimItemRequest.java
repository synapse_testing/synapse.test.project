
package za.co.ominsure.synapse.iwyze.lob.backend.interactive.claim.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="inputToken" type="{http://infrastructure.tia.dk/schema/common/v2/}InputToken"/>
 *         &lt;element name="claimNo" type="{http://infrastructure.tia.dk/schema/common/v2/}long10" minOccurs="0"/>
 *         &lt;element name="subclaimNo" type="{http://infrastructure.tia.dk/schema/common/v2/}long10" minOccurs="0"/>
 *         &lt;element name="claimItem" type="{http://infrastructure.tia.dk/schema/claim/v2/}ClaimItem" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "inputToken",
    "claimNo",
    "subclaimNo",
    "claimItem"
})
@XmlRootElement(name = "addClaimItemRequest")
public class AddClaimItemRequest {

    @XmlElement(required = true)
    protected InputToken inputToken;
    @XmlElement(nillable = true)
    protected Long claimNo;
    @XmlElement(nillable = true)
    protected Long subclaimNo;
    protected ClaimItem claimItem;

    /**
     * Gets the value of the inputToken property.
     * 
     * @return
     *     possible object is
     *     {@link InputToken }
     *     
     */
    public InputToken getInputToken() {
        return inputToken;
    }

    /**
     * Sets the value of the inputToken property.
     * 
     * @param value
     *     allowed object is
     *     {@link InputToken }
     *     
     */
    public void setInputToken(InputToken value) {
        this.inputToken = value;
    }

    /**
     * Gets the value of the claimNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getClaimNo() {
        return claimNo;
    }

    /**
     * Sets the value of the claimNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setClaimNo(Long value) {
        this.claimNo = value;
    }

    /**
     * Gets the value of the subclaimNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getSubclaimNo() {
        return subclaimNo;
    }

    /**
     * Sets the value of the subclaimNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setSubclaimNo(Long value) {
        this.subclaimNo = value;
    }

    /**
     * Gets the value of the claimItem property.
     * 
     * @return
     *     possible object is
     *     {@link ClaimItem }
     *     
     */
    public ClaimItem getClaimItem() {
        return claimItem;
    }

    /**
     * Sets the value of the claimItem property.
     * 
     * @param value
     *     allowed object is
     *     {@link ClaimItem }
     *     
     */
    public void setClaimItem(ClaimItem value) {
        this.claimItem = value;
    }

}
