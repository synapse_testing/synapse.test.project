
package za.co.ominsure.synapse.iwyze.lob.backend.policy.vo;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Additional comments
 * 
 * <p>Java class for ProdLineVerInProduct complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ProdLineVerInProduct">
 *   &lt;complexContent>
 *     &lt;extension base="{http://infrastructure.tia.dk/schema/common/v2/}TIAObject">
 *       &lt;sequence>
 *         &lt;element name="productLineId" type="{http://infrastructure.tia.dk/schema/common/v2/}string5" minOccurs="0"/>
 *         &lt;element name="productLineVerNo" type="{http://infrastructure.tia.dk/schema/common/v2/}decimal9Point3" minOccurs="0"/>
 *         &lt;element name="sectionNo" type="{http://infrastructure.tia.dk/schema/common/v2/}int3" minOccurs="0"/>
 *         &lt;element name="optionalYN" type="{http://infrastructure.tia.dk/schema/common/v2/}string1" minOccurs="0"/>
 *         &lt;element name="prodLineDependencyCollection" type="{http://infrastructure.tia.dk/schema/policy/v2/}ProdLineDependencyCollection" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProdLineVerInProduct", propOrder = {
    "productLineId",
    "productLineVerNo",
    "sectionNo",
    "optionalYN",
    "prodLineDependencyCollection"
})
public class ProdLineVerInProduct
    extends TIAObject
{

    @XmlElement(nillable = true)
    protected String productLineId;
    @XmlElement(nillable = true)
    protected BigDecimal productLineVerNo;
    @XmlElement(nillable = true)
    protected Long sectionNo;
    protected String optionalYN;
    protected ProdLineDependencyCollection prodLineDependencyCollection;

    /**
     * Gets the value of the productLineId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductLineId() {
        return productLineId;
    }

    /**
     * Sets the value of the productLineId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductLineId(String value) {
        this.productLineId = value;
    }

    /**
     * Gets the value of the productLineVerNo property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getProductLineVerNo() {
        return productLineVerNo;
    }

    /**
     * Sets the value of the productLineVerNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setProductLineVerNo(BigDecimal value) {
        this.productLineVerNo = value;
    }

    /**
     * Gets the value of the sectionNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getSectionNo() {
        return sectionNo;
    }

    /**
     * Sets the value of the sectionNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setSectionNo(Long value) {
        this.sectionNo = value;
    }

    /**
     * Gets the value of the optionalYN property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOptionalYN() {
        return optionalYN;
    }

    /**
     * Sets the value of the optionalYN property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOptionalYN(String value) {
        this.optionalYN = value;
    }

    /**
     * Gets the value of the prodLineDependencyCollection property.
     * 
     * @return
     *     possible object is
     *     {@link ProdLineDependencyCollection }
     *     
     */
    public ProdLineDependencyCollection getProdLineDependencyCollection() {
        return prodLineDependencyCollection;
    }

    /**
     * Sets the value of the prodLineDependencyCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProdLineDependencyCollection }
     *     
     */
    public void setProdLineDependencyCollection(ProdLineDependencyCollection value) {
        this.prodLineDependencyCollection = value;
    }

}
