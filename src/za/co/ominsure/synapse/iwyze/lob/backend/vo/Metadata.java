package za.co.ominsure.synapse.iwyze.lob.backend.vo;

public class Metadata {
	private String userId;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}
}