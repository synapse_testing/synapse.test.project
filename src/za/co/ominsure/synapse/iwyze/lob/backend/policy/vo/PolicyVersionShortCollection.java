
package za.co.ominsure.synapse.iwyze.lob.backend.policy.vo;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PolicyVersionShortCollection complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PolicyVersionShortCollection">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="policyVersionShort" type="{http://infrastructure.tia.dk/schema/policy/v2/}PolicyVersionShort" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PolicyVersionShortCollection", propOrder = {
    "policyVersionShorts"
})
public class PolicyVersionShortCollection {

    @XmlElement(name = "policyVersionShort")
    protected List<PolicyVersionShort> policyVersionShorts;

    /**
     * Gets the value of the policyVersionShorts property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the policyVersionShorts property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPolicyVersionShorts().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PolicyVersionShort }
     * 
     * 
     */
    public List<PolicyVersionShort> getPolicyVersionShorts() {
        if (policyVersionShorts == null) {
            policyVersionShorts = new ArrayList<PolicyVersionShort>();
        }
        return this.policyVersionShorts;
    }

}
