
package za.co.ominsure.synapse.iwyze.lob.backend.caseItem.vo;

import java.math.BigDecimal;
import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;



/**
 * <p>Java class for SearchTokenCase complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SearchTokenCase">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="assignedUserId" type="{http://infrastructure.tia.dk/schema/common/v2/}userId" minOccurs="0"/>
 *         &lt;element name="caseType" type="{http://infrastructure.tia.dk/schema/case/v2/}caseType" minOccurs="0"/>
 *         &lt;element name="actionId" type="{http://infrastructure.tia.dk/schema/common/v2/}actionId" minOccurs="0"/>
 *         &lt;element name="nameIdNo" type="{http://infrastructure.tia.dk/schema/common/v2/}nameIdNo" minOccurs="0"/>
 *         &lt;element name="policyNo" type="{http://infrastructure.tia.dk/schema/common/v2/}policyNo" minOccurs="0"/>
 *         &lt;element name="claimNo" type="{http://infrastructure.tia.dk/schema/common/v2/}claimNo" minOccurs="0"/>
 *         &lt;element name="subclaimNo" type="{http://infrastructure.tia.dk/schema/common/v2/}claimNo" minOccurs="0"/>
 *         &lt;element name="complaintNo" type="{http://infrastructure.tia.dk/schema/common/v2/}complaintNo" minOccurs="0"/>
 *         &lt;element name="etransNo" type="{http://infrastructure.tia.dk/schema/common/v2/}etransNo" minOccurs="0"/>
 *         &lt;element name="serviceSupplierAlias" type="{http://infrastructure.tia.dk/schema/case/v2/}serviceSupplierAlias" minOccurs="0"/>
 *         &lt;element name="documentTypeId" type="{http://infrastructure.tia.dk/schema/case/v2/}documentTypeId" minOccurs="0"/>
 *         &lt;element name="writingDate" type="{http://infrastructure.tia.dk/schema/common/v2/}date" minOccurs="0"/>
 *         &lt;element name="claItemNo" type="{http://infrastructure.tia.dk/schema/case/v2/}claItemNo" minOccurs="0"/>
 *         &lt;element name="leftLongitude" type="{http://infrastructure.tia.dk/schema/case/v2/}longitude" minOccurs="0"/>
 *         &lt;element name="topLatitude" type="{http://infrastructure.tia.dk/schema/case/v2/}latitude" minOccurs="0"/>
 *         &lt;element name="rightLongitude" type="{http://infrastructure.tia.dk/schema/case/v2/}longitude" minOccurs="0"/>
 *         &lt;element name="bottomLatitude" type="{http://infrastructure.tia.dk/schema/case/v2/}latitude" minOccurs="0"/>
 *         &lt;element name="objectNo" type="{http://infrastructure.tia.dk/schema/common/v2/}long10" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SearchTokenCase", namespace = "http://infrastructure.tia.dk/schema/case/v2/", propOrder = {
    "assignedUserId",
    "caseType",
    "actionId",
    "nameIdNo",
    "policyNo",
    "claimNo",
    "subclaimNo",
    "complaintNo",
    "etransNo",
    "serviceSupplierAlias",
    "documentTypeId",
    "writingDate",
    "claItemNo",
    "leftLongitude",
    "topLatitude",
    "rightLongitude",
    "bottomLatitude",
    "objectNo"
})
public class SearchTokenCase {

    protected String assignedUserId;
    protected String caseType;
    @XmlSchemaType(name = "integer")
    protected Integer actionId;
    @XmlSchemaType(name = "unsignedLong")
    protected Long nameIdNo;
    @XmlSchemaType(name = "unsignedLong")
    protected BigInteger policyNo;
    @XmlSchemaType(name = "unsignedLong")
    protected Long claimNo;
    @XmlSchemaType(name = "unsignedLong")
    protected Long subclaimNo;
    @XmlSchemaType(name = "unsignedLong")
    protected Long complaintNo;
    @XmlSchemaType(name = "integer")
    protected Integer etransNo;
    protected String serviceSupplierAlias;
    protected String documentTypeId;
    @XmlSchemaType(name = "date")
    protected String writingDate;
    @XmlSchemaType(name = "unsignedLong")
    protected Long claItemNo;
    protected BigDecimal leftLongitude;
    protected BigDecimal topLatitude;
    protected BigDecimal rightLongitude;
    protected BigDecimal bottomLatitude;
    protected Long objectNo;

    /**
     * Gets the value of the assignedUserId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAssignedUserId() {
        return assignedUserId;
    }

    /**
     * Sets the value of the assignedUserId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAssignedUserId(String value) {
        this.assignedUserId = value;
    }

    /**
     * Gets the value of the caseType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCaseType() {
        return caseType;
    }

    /**
     * Sets the value of the caseType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCaseType(String value) {
        this.caseType = value;
    }

    /**
     * Gets the value of the actionId property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getActionId() {
        return actionId;
    }

    /**
     * Sets the value of the actionId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setActionId(Integer value) {
        this.actionId = value;
    }

    /**
     * Gets the value of the nameIdNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getNameIdNo() {
        return nameIdNo;
    }

    /**
     * Sets the value of the nameIdNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setNameIdNo(Long value) {
        this.nameIdNo = value;
    }

    /**
     * Gets the value of the policyNo property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getPolicyNo() {
        return policyNo;
    }

    /**
     * Sets the value of the policyNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setPolicyNo(BigInteger value) {
        this.policyNo = value;
    }

    /**
     * Gets the value of the claimNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getClaimNo() {
        return claimNo;
    }

    /**
     * Sets the value of the claimNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setClaimNo(Long value) {
        this.claimNo = value;
    }

    /**
     * Gets the value of the subclaimNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getSubclaimNo() {
        return subclaimNo;
    }

    /**
     * Sets the value of the subclaimNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setSubclaimNo(Long value) {
        this.subclaimNo = value;
    }

    /**
     * Gets the value of the complaintNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getComplaintNo() {
        return complaintNo;
    }

    /**
     * Sets the value of the complaintNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setComplaintNo(Long value) {
        this.complaintNo = value;
    }

    /**
     * Gets the value of the etransNo property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getEtransNo() {
        return etransNo;
    }

    /**
     * Sets the value of the etransNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setEtransNo(Integer value) {
        this.etransNo = value;
    }

    /**
     * Gets the value of the serviceSupplierAlias property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceSupplierAlias() {
        return serviceSupplierAlias;
    }

    /**
     * Sets the value of the serviceSupplierAlias property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceSupplierAlias(String value) {
        this.serviceSupplierAlias = value;
    }

    /**
     * Gets the value of the documentTypeId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDocumentTypeId() {
        return documentTypeId;
    }

    /**
     * Sets the value of the documentTypeId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDocumentTypeId(String value) {
        this.documentTypeId = value;
    }

    /**
     * Gets the value of the writingDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWritingDate() {
        return writingDate;
    }

    /**
     * Sets the value of the writingDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWritingDate(String value) {
        this.writingDate = value;
    }

    /**
     * Gets the value of the claItemNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getClaItemNo() {
        return claItemNo;
    }

    /**
     * Sets the value of the claItemNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setClaItemNo(Long value) {
        this.claItemNo = value;
    }

    /**
     * Gets the value of the leftLongitude property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getLeftLongitude() {
        return leftLongitude;
    }

    /**
     * Sets the value of the leftLongitude property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setLeftLongitude(BigDecimal value) {
        this.leftLongitude = value;
    }

    /**
     * Gets the value of the topLatitude property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTopLatitude() {
        return topLatitude;
    }

    /**
     * Sets the value of the topLatitude property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTopLatitude(BigDecimal value) {
        this.topLatitude = value;
    }

    /**
     * Gets the value of the rightLongitude property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRightLongitude() {
        return rightLongitude;
    }

    /**
     * Sets the value of the rightLongitude property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRightLongitude(BigDecimal value) {
        this.rightLongitude = value;
    }

    /**
     * Gets the value of the bottomLatitude property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getBottomLatitude() {
        return bottomLatitude;
    }

    /**
     * Sets the value of the bottomLatitude property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setBottomLatitude(BigDecimal value) {
        this.bottomLatitude = value;
    }

    /**
     * Gets the value of the objectNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getObjectNo() {
        return objectNo;
    }

    /**
     * Sets the value of the objectNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setObjectNo(Long value) {
        this.objectNo = value;
    }

}
