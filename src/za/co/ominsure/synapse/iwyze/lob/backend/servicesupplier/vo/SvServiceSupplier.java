
package za.co.ominsure.synapse.iwyze.lob.backend.servicesupplier.vo;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.WebServiceFeature;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.9-b130926.1035
 * Generated source version: 2.2
 * 
 */
@WebServiceClient(name = "svServiceSupplier", targetNamespace = "http://infrastructure.tia.dk/contract/party/v2/", wsdlLocation = "http://zamfqappv101.mufeq.co.za:7112/TIAServices/services/Party/ServiceSupplierRouterService")
public class SvServiceSupplier
    extends Service
{

    private final static URL SVSERVICESUPPLIER_WSDL_LOCATION;
    private final static WebServiceException SVSERVICESUPPLIER_EXCEPTION;
    private final static QName SVSERVICESUPPLIER_QNAME = new QName("http://infrastructure.tia.dk/contract/party/v2/", "svServiceSupplier");

    static {
        URL url = null;
        WebServiceException e = null;
        try {
            url = new URL("http://zamfqappv101.mufeq.co.za:7112/TIAServices/services/Party/ServiceSupplierRouterService");
        } catch (MalformedURLException ex) {
            e = new WebServiceException(ex);
        }
        SVSERVICESUPPLIER_WSDL_LOCATION = url;
        SVSERVICESUPPLIER_EXCEPTION = e;
    }

    public SvServiceSupplier() {
        super(__getWsdlLocation(), SVSERVICESUPPLIER_QNAME);
    }

    public SvServiceSupplier(WebServiceFeature... features) {
        super(__getWsdlLocation(), SVSERVICESUPPLIER_QNAME, features);
    }

    public SvServiceSupplier(URL wsdlLocation) {
        super(wsdlLocation, SVSERVICESUPPLIER_QNAME);
    }

    public SvServiceSupplier(URL wsdlLocation, WebServiceFeature... features) {
        super(wsdlLocation, SVSERVICESUPPLIER_QNAME, features);
    }

    public SvServiceSupplier(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public SvServiceSupplier(URL wsdlLocation, QName serviceName, WebServiceFeature... features) {
        super(wsdlLocation, serviceName, features);
    }

    /**
     * 
     * @return
     *     returns PtServiceSupplier
     */
    @WebEndpoint(name = "ptServiceSupplier_pt")
    public PtServiceSupplier getPtServiceSupplierPt() {
        return super.getPort(new QName("http://infrastructure.tia.dk/contract/party/v2/", "ptServiceSupplier_pt"), PtServiceSupplier.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns PtServiceSupplier
     */
    @WebEndpoint(name = "ptServiceSupplier_pt")
    public PtServiceSupplier getPtServiceSupplierPt(WebServiceFeature... features) {
        return super.getPort(new QName("http://infrastructure.tia.dk/contract/party/v2/", "ptServiceSupplier_pt"), PtServiceSupplier.class, features);
    }

    private static URL __getWsdlLocation() {
        if (SVSERVICESUPPLIER_EXCEPTION!= null) {
            throw SVSERVICESUPPLIER_EXCEPTION;
        }
        return SVSERVICESUPPLIER_WSDL_LOCATION;
    }

}
