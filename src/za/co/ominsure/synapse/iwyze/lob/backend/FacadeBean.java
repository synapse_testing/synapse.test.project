package za.co.ominsure.synapse.iwyze.lob.backend;

import java.sql.SQLException;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.core.Response;

import org.apache.commons.lang3.StringUtils;

import za.co.ominsure.synapse.cdm.claim.Audit;
import za.co.ominsure.synapse.cdm.claim.Audits;
import za.co.ominsure.synapse.cdm.claim.Case;
import za.co.ominsure.synapse.cdm.claim.Claim;
import za.co.ominsure.synapse.cdm.common.content.SearchRequest;
import za.co.ominsure.synapse.cdm.common.content.SearchResult;
import za.co.ominsure.synapse.cdm.finance.Payments;
import za.co.ominsure.synapse.cdm.nonapproved.ThirdParties;
import za.co.ominsure.synapse.cdm.policy.PolicyOld;
import za.co.ominsure.synapse.cdm.workflow.ajs.CreateRequest;
import za.co.ominsure.synapse.cdm.workflow.ajs.RecoveryStatusChangeRequest;
import za.co.ominsure.synapse.common.exception.HttpException;
import za.co.ominsure.synapse.iwyze.lob.backend.caseItem.vo.CaseItemGenericResponse;
import za.co.ominsure.synapse.iwyze.lob.backend.proxy.IwyzeProxy;
import za.co.ominsure.synapse.iwyze.lob.backend.servicesupplier.vo.GetServiceSupplierResponse;
import za.co.ominsure.synapse.iwyze.lob.backend.vo.AddClaimsLogRequest;
import za.co.ominsure.synapse.iwyze.lob.backend.vo.AddClaimsLogResponse;
import za.co.ominsure.synapse.iwyze.lob.backend.vo.GetClaimsLogResponse;
import za.co.ominsure.synapse.iwyze.lob.backend.vo.GetPolicyNumberResponse;

@Stateless
public class FacadeBean implements Facade {

	@EJB
	private IwyzeProxy proxy;


	@Override
	public Audits getAudits(String claimEventNumber, int offset, int pageSize) throws HttpException {
		if (StringUtils.isBlank(claimEventNumber))
			throw new HttpException("Bad Request claim event number  is required", Response.Status.BAD_REQUEST.getStatusCode());
		return proxy.getAudits(claimEventNumber, offset, pageSize);
	}

	@Override
	public void addAudit(Audits audits) throws HttpException {
		if (audits.getAudit().isEmpty()) {
			throw new HttpException("NO Audit Data provided for Audit logging! Audit data is empty!", 400);
		}

		for (Audit audit : audits.getAudit()) {
			if (StringUtils.isBlank(audit.getClaimNumber())) {
				throw new HttpException("claim number for Audit logging must be a valid value! ", 400);
			}
			if (StringUtils.isBlank(audit.getCaseNumber())) {
				throw new HttpException("case number for Audit logging must be a valid value! ", 400);
			}
		}
		proxy.addAudit(audits);
	}

	@Override
	public Case getCaseInfo(Long claimNumber, long caseNumber) throws HttpException {
		return proxy.getCaseInfo(claimNumber, caseNumber);
	}

	@Override
	public ThirdParties getThirdPartyInfo(String nameIdNumber, String siteName, int offset, int pageSize) throws HttpException {
		if (StringUtils.isBlank(nameIdNumber))
			throw new HttpException("Bad Request nameIdNumber  is required", Response.Status.BAD_REQUEST.getStatusCode());
		if (StringUtils.isBlank(siteName))
			throw new HttpException("Bad Request siteName  is required", Response.Status.BAD_REQUEST.getStatusCode());
		return proxy.getThirdPartyInfo(nameIdNumber, siteName, offset, pageSize);
	}

	@Override
	public Payments getPayments(String claimNumber, int offset, int pagesize) throws HttpException, SQLException {
		return proxy.getPayments(claimNumber, offset, pagesize);
	}

	@Override
	public SearchResult getDocumentsForPolicy(SearchRequest searchRequest) throws HttpException {
		return proxy.getDocumentsForPolicy(searchRequest);
	}

	@Override
	public Claim getClaim(String claimEventNumber, String siteName, int offset, int pageSize) throws HttpException {
		if (StringUtils.isBlank(claimEventNumber))
			throw new HttpException("Bad Request claim event number  is required", Response.Status.BAD_REQUEST.getStatusCode());
		if (StringUtils.isBlank(siteName))
			throw new HttpException("Bad Request  siteName  is required", Response.Status.BAD_REQUEST.getStatusCode());
		return proxy.getClaim(claimEventNumber, siteName, offset, pageSize);
	}

	@Override
	public CreateRequest getAJSData(String claimNumber) throws HttpException, SQLException {
		if (StringUtils.isBlank(claimNumber))
			throw new HttpException("Bad request.Please provide claim number", Response.Status.BAD_REQUEST.getStatusCode());
		return proxy.getAJSData(claimNumber);
	}

	@Override
	public PolicyOld getpolicyInformation(Long policyNumber, String caseNumber, String effectiveDate, Long lossRatioYears, String objectSequenceNumber) throws HttpException {
		if (policyNumber == null)
			throw new HttpException("Bad Request policy number  is required", Response.Status.BAD_REQUEST.getStatusCode());
		if (caseNumber == null)
			throw new HttpException("Bad Request case number  is required", Response.Status.BAD_REQUEST.getStatusCode());
		return proxy.getpolicyInformation(policyNumber, caseNumber, effectiveDate, lossRatioYears, objectSequenceNumber);
	}

	@Override
	public PolicyOld searchPolicy(String nameIdNumber, String caseNumber) throws HttpException {
		if (StringUtils.isBlank(nameIdNumber))
			throw new HttpException("Bad Request nameIdNumber  is required", Response.Status.BAD_REQUEST.getStatusCode());
		if (StringUtils.isBlank(caseNumber))
			throw new HttpException("Bad Request case number  is required", Response.Status.BAD_REQUEST.getStatusCode());
		return proxy.searchPolicy(nameIdNumber, caseNumber);
	}

	@Override
	public ThirdParties searchSubClaim(String claimNumber, String siteName) throws HttpException {
		if (StringUtils.isBlank(claimNumber))
			throw new HttpException("Bad Request claimNumber  is required", Response.Status.BAD_REQUEST.getStatusCode());
		if (StringUtils.isBlank(siteName))
			throw new HttpException("Bad Request siteName  is required", Response.Status.BAD_REQUEST.getStatusCode());
		return proxy.searchSubClaim(claimNumber, siteName);
	}

	@Override
	public Claim searchParty(String idNumber, String siteName) throws HttpException {
		if (StringUtils.isBlank(idNumber))
			throw new HttpException("Bad Request idNumber  is required", Response.Status.BAD_REQUEST.getStatusCode());
		if (StringUtils.isBlank(siteName))
			throw new HttpException("Bad Request siteName  is required", Response.Status.BAD_REQUEST.getStatusCode());
		return proxy.searchParty(idNumber, siteName);
	}

	@Override
	public AddClaimsLogResponse addClaimLog(AddClaimsLogRequest request) throws HttpException,SQLException {
		if (StringUtils.isBlank(request.getEventNo()))
			throw new HttpException("Bad Request claim event number is required", Response.Status.BAD_REQUEST.getStatusCode());
		if (StringUtils.isBlank(request.getClaimNo()))
			throw new HttpException("Bad Request claim number is required:", Response.Status.BAD_REQUEST.getStatusCode());
		return proxy.addClaimLog(request);
	}

	@Override
	public GetClaimsLogResponse getClaimsLog(String claimNumber) throws HttpException, SQLException {
		if (StringUtils.isBlank(claimNumber))
			throw new HttpException("Bad Request claim number is required", Response.Status.BAD_REQUEST.getStatusCode());
		return proxy.getClaimsLog(claimNumber);
	}

	@Override
	public void claimStatusUpdate(String claimNumber, RecoveryStatusChangeRequest request) throws HttpException {
		if (StringUtils.isBlank(claimNumber))
			throw new HttpException("Bad Request claim number is required", Response.Status.BAD_REQUEST.getStatusCode());
		if (request == null)
			throw new HttpException("Bad Request.", Response.Status.BAD_REQUEST.getStatusCode());
		proxy.claimStatusUpdate(claimNumber, request);
	}

	@Override
	public GetPolicyNumberResponse getPolicyDetail(String policyHolderId, String secret) throws HttpException {
		if (StringUtils.isBlank(policyHolderId))
			throw new HttpException("Bad Request policyHolderId required", Response.Status.BAD_REQUEST.getStatusCode());
		if (StringUtils.isBlank(secret))
			throw new HttpException("Bad Request secret required", Response.Status.BAD_REQUEST.getStatusCode());
		return proxy.getPolicyDetail(policyHolderId, secret);
	}

	@Override
	public GetServiceSupplierResponse getServiceSupplier(String receiverIdNumber, String sitename) throws HttpException {
		if (StringUtils.isBlank(receiverIdNumber))
			throw new HttpException("Bad Request receiverIdNo is required", Response.Status.BAD_REQUEST.getStatusCode());
		if (StringUtils.isBlank(sitename))
			throw new HttpException("Bad Request sitename is required:", Response.Status.BAD_REQUEST.getStatusCode());
		return proxy.getServiceSupplier(receiverIdNumber, sitename);
	}

	@Override
	public CaseItemGenericResponse searchCaseItem(String claimNumber, String siteName) throws HttpException {
		if (StringUtils.isBlank(claimNumber))
			throw new HttpException("Bad Request claimNumber  is required", Response.Status.BAD_REQUEST.getStatusCode());
		if (StringUtils.isBlank(siteName))
			throw new HttpException("Bad Request siteName  is required", Response.Status.BAD_REQUEST.getStatusCode());
		return proxy.searchCaseItem(claimNumber, siteName);
	}
}
