
package za.co.ominsure.synapse.iwyze.lob.backend.claim.payment.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="inputToken" type="{http://infrastructure.tia.dk/schema/common/v2/}InputToken"/>
 *         &lt;element name="claimAccItem" type="{http://infrastructure.tia.dk/schema/claim/v2/}ClaimAccItem" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "inputToken",
    "claimAccItem"
})
@XmlRootElement(name = "createClaimPaymentRequest")
public class CreateClaimPaymentRequest {

    @XmlElement(required = true)
    protected InputToken inputToken;
    protected ClaimAccItem claimAccItem;

    /**
     * Gets the value of the inputToken property.
     * 
     * @return
     *     possible object is
     *     {@link InputToken }
     *     
     */
    public InputToken getInputToken() {
        return inputToken;
    }

    /**
     * Sets the value of the inputToken property.
     * 
     * @param value
     *     allowed object is
     *     {@link InputToken }
     *     
     */
    public void setInputToken(InputToken value) {
        this.inputToken = value;
    }

    /**
     * Gets the value of the claimAccItem property.
     * 
     * @return
     *     possible object is
     *     {@link ClaimAccItem }
     *     
     */
    public ClaimAccItem getClaimAccItem() {
        return claimAccItem;
    }

    /**
     * Sets the value of the claimAccItem property.
     * 
     * @param value
     *     allowed object is
     *     {@link ClaimAccItem }
     *     
     */
    public void setClaimAccItem(ClaimAccItem value) {
        this.claimAccItem = value;
    }

}
