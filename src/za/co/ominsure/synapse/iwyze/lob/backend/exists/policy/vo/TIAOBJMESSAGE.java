
package za.co.ominsure.synapse.iwyze.lob.backend.exists.policy.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for TIA.OBJ_MESSAGE complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TIA.OBJ_MESSAGE">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="MESSAGE_ID" type="{http://xmlns.oracle.com/pcbpel/adapter/db/sp/checkExistingPolicyReference}string22" minOccurs="0"/>
 *         &lt;element name="MESSAGE_TYPE" type="{http://xmlns.oracle.com/pcbpel/adapter/db/sp/checkExistingPolicyReference}string1" minOccurs="0"/>
 *         &lt;element name="MESSAGE_TEXT" type="{http://xmlns.oracle.com/pcbpel/adapter/db/sp/checkExistingPolicyReference}string2000" minOccurs="0"/>
 *         &lt;element name="ENTITY_NAME" type="{http://xmlns.oracle.com/pcbpel/adapter/db/sp/checkExistingPolicyReference}string92" minOccurs="0"/>
 *         &lt;element name="ENTITY_ID" type="{http://xmlns.oracle.com/pcbpel/adapter/db/sp/checkExistingPolicyReference}string200" minOccurs="0"/>
 *         &lt;element name="ITEM_NAME" type="{http://xmlns.oracle.com/pcbpel/adapter/db/sp/checkExistingPolicyReference}string32" minOccurs="0"/>
 *         &lt;element name="TIMESTAMP" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TIA.OBJ_MESSAGE", propOrder = {
    "messageid",
    "messagetype",
    "messagetext",
    "entityname",
    "entityid",
    "itemname",
    "timestamp"
})
public class TIAOBJMESSAGE {

    @XmlElement(name = "MESSAGE_ID", nillable = true)
    protected String messageid;
    @XmlElement(name = "MESSAGE_TYPE", nillable = true)
    protected String messagetype;
    @XmlElement(name = "MESSAGE_TEXT", nillable = true)
    protected String messagetext;
    @XmlElement(name = "ENTITY_NAME", nillable = true)
    protected String entityname;
    @XmlElement(name = "ENTITY_ID", nillable = true)
    protected String entityid;
    @XmlElement(name = "ITEM_NAME", nillable = true)
    protected String itemname;
    @XmlElement(name = "TIMESTAMP", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar timestamp;

    /**
     * Gets the value of the messageid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMESSAGEID() {
        return messageid;
    }

    /**
     * Sets the value of the messageid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMESSAGEID(String value) {
        this.messageid = value;
    }

    /**
     * Gets the value of the messagetype property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMESSAGETYPE() {
        return messagetype;
    }

    /**
     * Sets the value of the messagetype property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMESSAGETYPE(String value) {
        this.messagetype = value;
    }

    /**
     * Gets the value of the messagetext property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMESSAGETEXT() {
        return messagetext;
    }

    /**
     * Sets the value of the messagetext property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMESSAGETEXT(String value) {
        this.messagetext = value;
    }

    /**
     * Gets the value of the entityname property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getENTITYNAME() {
        return entityname;
    }

    /**
     * Sets the value of the entityname property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setENTITYNAME(String value) {
        this.entityname = value;
    }

    /**
     * Gets the value of the entityid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getENTITYID() {
        return entityid;
    }

    /**
     * Sets the value of the entityid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setENTITYID(String value) {
        this.entityid = value;
    }

    /**
     * Gets the value of the itemname property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getITEMNAME() {
        return itemname;
    }

    /**
     * Sets the value of the itemname property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setITEMNAME(String value) {
        this.itemname = value;
    }

    /**
     * Gets the value of the timestamp property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTIMESTAMP() {
        return timestamp;
    }

    /**
     * Sets the value of the timestamp property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTIMESTAMP(XMLGregorianCalendar value) {
        this.timestamp = value;
    }

}
