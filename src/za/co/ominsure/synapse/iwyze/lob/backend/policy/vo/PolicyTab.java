
package za.co.ominsure.synapse.iwyze.lob.backend.policy.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Is mapped into TIA package P1100".
 * 
 * <p>Java class for PolicyTab complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PolicyTab">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="policyCollection" type="{http://infrastructure.tia.dk/schema/policy/v2/}PolicyCollection"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PolicyTab", propOrder = {
    "policyCollection"
})
public class PolicyTab {

    @XmlElement(required = true)
    protected PolicyCollection2 policyCollection;

    /**
     * Gets the value of the policyCollection property.
     * 
     * @return
     *     possible object is
     *     {@link PolicyCollection2 }
     *     
     */
    public PolicyCollection2 getPolicyCollection() {
        return policyCollection;
    }

    /**
     * Sets the value of the policyCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link PolicyCollection2 }
     *     
     */
    public void setPolicyCollection(PolicyCollection2 value) {
        this.policyCollection = value;
    }

}
