
package za.co.ominsure.synapse.iwyze.lob.backend.party.vo;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Is mapped into TIA metadata type "obj_person".
 * 
 * <p>Java class for Person complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Person">
 *   &lt;complexContent>
 *     &lt;extension base="{http://infrastructure.tia.dk/schema/party/v3/}AbstractParty">
 *       &lt;sequence>
 *         &lt;element name="title" type="{http://infrastructure.tia.dk/schema/party/v2/}title" minOccurs="0"/>
 *         &lt;element name="forename" type="{http://infrastructure.tia.dk/schema/common/v2/}string70" minOccurs="0"/>
 *         &lt;element name="civilRegCode" type="{http://infrastructure.tia.dk/schema/party/v2/}civilReg" minOccurs="0"/>
 *         &lt;element name="birthDate" type="{http://infrastructure.tia.dk/schema/common/v2/}date" minOccurs="0"/>
 *         &lt;element name="middleName" type="{http://infrastructure.tia.dk/schema/common/v2/}string70" minOccurs="0"/>
 *         &lt;element name="deceasedDate" type="{http://infrastructure.tia.dk/schema/common/v2/}date" minOccurs="0"/>
 *         &lt;element name="emigrationDate" type="{http://infrastructure.tia.dk/schema/common/v2/}date" minOccurs="0"/>
 *         &lt;element name="hiddenIdentity" type="{http://infrastructure.tia.dk/schema/common/v2/}tiaYN" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Person", namespace = "http://infrastructure.tia.dk/schema/party/v3/", propOrder = {
    "title",
    "forename",
    "civilRegCode",
    "birthDate",
    "middleName",
    "deceasedDate",
    "emigrationDate",
    "hiddenIdentity"
})
public class Person
    extends AbstractParty
{

    @XmlElementRef(name = "title", namespace = "http://infrastructure.tia.dk/schema/party/v3/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> title;
    @XmlElementRef(name = "forename", namespace = "http://infrastructure.tia.dk/schema/party/v3/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> forename;
    @XmlElementRef(name = "civilRegCode", namespace = "http://infrastructure.tia.dk/schema/party/v3/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> civilRegCode;
    @XmlElementRef(name = "birthDate", namespace = "http://infrastructure.tia.dk/schema/party/v3/", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> birthDate;
    @XmlElementRef(name = "middleName", namespace = "http://infrastructure.tia.dk/schema/party/v3/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> middleName;
    @XmlElementRef(name = "deceasedDate", namespace = "http://infrastructure.tia.dk/schema/party/v3/", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> deceasedDate;
    @XmlElementRef(name = "emigrationDate", namespace = "http://infrastructure.tia.dk/schema/party/v3/", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> emigrationDate;
    @XmlElementRef(name = "hiddenIdentity", namespace = "http://infrastructure.tia.dk/schema/party/v3/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> hiddenIdentity;

    /**
     * Gets the value of the title property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTitle() {
        return title;
    }

    /**
     * Sets the value of the title property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTitle(JAXBElement<String> value) {
        this.title = value;
    }

    /**
     * Gets the value of the forename property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getForename() {
        return forename;
    }

    /**
     * Sets the value of the forename property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setForename(JAXBElement<String> value) {
        this.forename = value;
    }

    /**
     * Gets the value of the civilRegCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCivilRegCode() {
        return civilRegCode;
    }

    /**
     * Sets the value of the civilRegCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCivilRegCode(JAXBElement<String> value) {
        this.civilRegCode = value;
    }

    /**
     * Gets the value of the birthDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getBirthDate() {
        return birthDate;
    }

    /**
     * Sets the value of the birthDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setBirthDate(JAXBElement<XMLGregorianCalendar> value) {
        this.birthDate = value;
    }

    /**
     * Gets the value of the middleName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getMiddleName() {
        return middleName;
    }

    /**
     * Sets the value of the middleName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setMiddleName(JAXBElement<String> value) {
        this.middleName = value;
    }

    /**
     * Gets the value of the deceasedDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getDeceasedDate() {
        return deceasedDate;
    }

    /**
     * Sets the value of the deceasedDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setDeceasedDate(JAXBElement<XMLGregorianCalendar> value) {
        this.deceasedDate = value;
    }

    /**
     * Gets the value of the emigrationDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getEmigrationDate() {
        return emigrationDate;
    }

    /**
     * Sets the value of the emigrationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setEmigrationDate(JAXBElement<XMLGregorianCalendar> value) {
        this.emigrationDate = value;
    }

    /**
     * Gets the value of the hiddenIdentity property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getHiddenIdentity() {
        return hiddenIdentity;
    }

    /**
     * Sets the value of the hiddenIdentity property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setHiddenIdentity(JAXBElement<String> value) {
        this.hiddenIdentity = value;
    }

}
