
package za.co.ominsure.synapse.iwyze.lob.backend.caseItem.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="inputToken" type="{http://infrastructure.tia.dk/schema/common/v2/}InputToken"/>
 *         &lt;element name="searchTokenCase" type="{http://infrastructure.tia.dk/schema/case/v2/}SearchTokenCase"/>
 *         &lt;element name="pageSort" type="{http://infrastructure.tia.dk/schema/common/v2/}PageSort" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "inputToken",
    "searchTokenCase",
    "pageSort"
})
@XmlRootElement(name = "searchCaseItemRequest", namespace = "http://infrastructure.tia.dk/schema/case/v3/")
public class SearchCaseItemRequest {

    @XmlElement(namespace = "http://infrastructure.tia.dk/schema/case/v3/", required = true)
    protected InputToken inputToken;
    @XmlElement(namespace = "http://infrastructure.tia.dk/schema/case/v3/", required = true)
    protected SearchTokenCase searchTokenCase;
    @XmlElement(namespace = "http://infrastructure.tia.dk/schema/case/v3/")
    protected PageSort pageSort;

    /**
     * Gets the value of the inputToken property.
     * 
     * @return
     *     possible object is
     *     {@link InputToken }
     *     
     */
    public InputToken getInputToken() {
        return inputToken;
    }

    /**
     * Sets the value of the inputToken property.
     * 
     * @param value
     *     allowed object is
     *     {@link InputToken }
     *     
     */
    public void setInputToken(InputToken value) {
        this.inputToken = value;
    }

    /**
     * Gets the value of the searchTokenCase property.
     * 
     * @return
     *     possible object is
     *     {@link SearchTokenCase }
     *     
     */
    public SearchTokenCase getSearchTokenCase() {
        return searchTokenCase;
    }

    /**
     * Sets the value of the searchTokenCase property.
     * 
     * @param value
     *     allowed object is
     *     {@link SearchTokenCase }
     *     
     */
    public void setSearchTokenCase(SearchTokenCase value) {
        this.searchTokenCase = value;
    }

    /**
     * Gets the value of the pageSort property.
     * 
     * @return
     *     possible object is
     *     {@link PageSort }
     *     
     */
    public PageSort getPageSort() {
        return pageSort;
    }

    /**
     * Sets the value of the pageSort property.
     * 
     * @param value
     *     allowed object is
     *     {@link PageSort }
     *     
     */
    public void setPageSort(PageSort value) {
        this.pageSort = value;
    }

}
