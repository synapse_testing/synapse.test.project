
package za.co.ominsure.synapse.iwyze.lob.backend.party.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="partyCollection" type="{http://infrastructure.tia.dk/schema/party/v3/}PartyCollection"/>
 *         &lt;element name="result" type="{http://infrastructure.tia.dk/schema/common/v2/}Result"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "partyCollection",
    "result"
})
@XmlRootElement(name = "searchPartyResponse", namespace = "http://infrastructure.tia.dk/schema/party/v3/")
public class SearchPartyResponse {

    @XmlElement(namespace = "http://infrastructure.tia.dk/schema/party/v3/", required = true)
    protected PartyCollection partyCollection;
    @XmlElement(namespace = "http://infrastructure.tia.dk/schema/party/v3/", required = true)
    protected Result result;

    /**
     * Gets the value of the partyCollection property.
     * 
     * @return
     *     possible object is
     *     {@link PartyCollection }
     *     
     */
    public PartyCollection getPartyCollection() {
        return partyCollection;
    }

    /**
     * Sets the value of the partyCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link PartyCollection }
     *     
     */
    public void setPartyCollection(PartyCollection value) {
        this.partyCollection = value;
    }

    /**
     * Gets the value of the result property.
     * 
     * @return
     *     possible object is
     *     {@link Result }
     *     
     */
    public Result getResult() {
        return result;
    }

    /**
     * Sets the value of the result property.
     * 
     * @param value
     *     allowed object is
     *     {@link Result }
     *     
     */
    public void setResult(Result value) {
        this.result = value;
    }

}
