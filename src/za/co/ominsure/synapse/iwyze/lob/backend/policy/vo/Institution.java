
package za.co.ominsure.synapse.iwyze.lob.backend.policy.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Is mapped into TIA metadata type "obj_institution".
 * 
 * <p>Java class for Institution complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Institution">
 *   &lt;complexContent>
 *     &lt;extension base="{http://infrastructure.tia.dk/schema/party/v2/}AbstractParty">
 *       &lt;sequence>
 *         &lt;element name="companyRegNo" type="{http://infrastructure.tia.dk/schema/party/v2/}companyRegNo" minOccurs="0"/>
 *         &lt;element name="companyVatNo" type="{http://infrastructure.tia.dk/schema/party/v2/}companyVatNo" minOccurs="0"/>
 *         &lt;element name="contactPerson" type="{http://infrastructure.tia.dk/schema/party/v2/}contactPerson" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Institution", namespace = "http://infrastructure.tia.dk/schema/party/v2/", propOrder = {
    "companyRegNo",
    "companyVatNo",
    "contactPerson"
})
public class Institution
    extends AbstractParty
{

    @XmlElement(nillable = true)
    protected String companyRegNo;
    @XmlElement(nillable = true)
    protected String companyVatNo;
    @XmlElement(nillable = true)
    protected String contactPerson;

    /**
     * Gets the value of the companyRegNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCompanyRegNo() {
        return companyRegNo;
    }

    /**
     * Sets the value of the companyRegNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCompanyRegNo(String value) {
        this.companyRegNo = value;
    }

    /**
     * Gets the value of the companyVatNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCompanyVatNo() {
        return companyVatNo;
    }

    /**
     * Sets the value of the companyVatNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCompanyVatNo(String value) {
        this.companyVatNo = value;
    }

    /**
     * Gets the value of the contactPerson property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContactPerson() {
        return contactPerson;
    }

    /**
     * Sets the value of the contactPerson property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContactPerson(String value) {
        this.contactPerson = value;
    }

}
