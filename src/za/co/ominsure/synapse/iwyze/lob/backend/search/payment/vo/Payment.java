
package za.co.ominsure.synapse.iwyze.lob.backend.search.payment.vo;

import java.math.BigDecimal;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Is mapped into TIA metadata type "obj_payment".
 * 
 * <p>Java class for Payment complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Payment">
 *   &lt;complexContent>
 *     &lt;extension base="{http://infrastructure.tia.dk/schema/common/v2/}TIAObject">
 *       &lt;sequence>
 *         &lt;element name="paymentNo" type="{http://infrastructure.tia.dk/schema/common/v2/}seqNo" minOccurs="0"/>
 *         &lt;element name="accountNo" type="{http://infrastructure.tia.dk/schema/common/v2/}seqNo"/>
 *         &lt;element name="amount" type="{http://infrastructure.tia.dk/schema/common/v2/}amount" minOccurs="0"/>
 *         &lt;element name="amountIn" type="{http://infrastructure.tia.dk/schema/common/v2/}amount" minOccurs="0"/>
 *         &lt;element name="currencyCode" type="{http://infrastructure.tia.dk/schema/common/v2/}currencyCode" minOccurs="0"/>
 *         &lt;element name="receiverId" type="{http://infrastructure.tia.dk/schema/common/v2/}seqNo" minOccurs="0"/>
 *         &lt;element name="dueDate" type="{http://infrastructure.tia.dk/schema/common/v2/}date" minOccurs="0"/>
 *         &lt;element name="valueDateIn" type="{http://infrastructure.tia.dk/schema/common/v2/}date" minOccurs="0"/>
 *         &lt;element name="transactionDateOut" type="{http://infrastructure.tia.dk/schema/common/v2/}date" minOccurs="0"/>
 *         &lt;element name="transactionDateIn" type="{http://infrastructure.tia.dk/schema/common/v2/}date" minOccurs="0"/>
 *         &lt;element name="paymentStatus" type="{http://infrastructure.tia.dk/schema/account/v2/}paymentStatus" minOccurs="0"/>
 *         &lt;element name="paymentStatusOut" type="{http://infrastructure.tia.dk/schema/account/v2/}paymentStatus" minOccurs="0"/>
 *         &lt;element name="paymentMethod" type="{http://infrastructure.tia.dk/schema/common/v2/}paymentMethod" minOccurs="0"/>
 *         &lt;element name="paymentDetailsId" type="{http://infrastructure.tia.dk/schema/common/v2/}seqNo" minOccurs="0"/>
 *         &lt;element name="paymentChannel" type="{http://infrastructure.tia.dk/schema/account/v2/}paymentChannelType" minOccurs="0"/>
 *         &lt;element name="recollectionTransId" type="{http://infrastructure.tia.dk/schema/common/v2/}transId" minOccurs="0"/>
 *         &lt;element name="externalCode" type="{http://infrastructure.tia.dk/schema/account/v2/}externalCode" minOccurs="0"/>
 *         &lt;element name="externalCodeIn" type="{http://infrastructure.tia.dk/schema/account/v2/}externalCode" minOccurs="0"/>
 *         &lt;element name="externalRefIn" type="{http://infrastructure.tia.dk/schema/account/v2/}externalRef" minOccurs="0"/>
 *         &lt;element name="groupLevel" type="{http://infrastructure.tia.dk/schema/account/v2/}groupLevel"/>
 *         &lt;element name="groupCriteria" type="{http://infrastructure.tia.dk/schema/account/v2/}groupCriteria"/>
 *         &lt;element name="receiverNameAddress" type="{http://infrastructure.tia.dk/schema/account/v2/}receiverNameAddress" minOccurs="0"/>
 *         &lt;element name="paymentDetailsText" type="{http://infrastructure.tia.dk/schema/account/v2/}paymentDetailsText" minOccurs="0"/>
 *         &lt;element name="paymentInstruction" type="{http://infrastructure.tia.dk/schema/account/v2/}paymentInstruction" minOccurs="0"/>
 *         &lt;element name="batchRun" type="{http://infrastructure.tia.dk/schema/common/v2/}batchNo" minOccurs="0"/>
 *         &lt;element name="batchOut" type="{http://infrastructure.tia.dk/schema/common/v2/}batchNo" minOccurs="0"/>
 *         &lt;element name="batchIn" type="{http://infrastructure.tia.dk/schema/common/v2/}batchNo" minOccurs="0"/>
 *         &lt;element name="paymentItemCollection" type="{http://infrastructure.tia.dk/schema/account/v2/}PaymentItemCollection" minOccurs="0"/>
 *         &lt;element name="paymentSpecificationCollection" type="{http://infrastructure.tia.dk/schema/account/v2/}PaymentSpecificationCollection" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Payment", propOrder = {
    "paymentNo",
    "accountNo",
    "amount",
    "amountIn",
    "currencyCode",
    "receiverId",
    "dueDate",
    "valueDateIn",
    "transactionDateOut",
    "transactionDateIn",
    "paymentStatus",
    "paymentStatusOut",
    "paymentMethod",
    "paymentDetailsId",
    "paymentChannel",
    "recollectionTransId",
    "externalCode",
    "externalCodeIn",
    "externalRefIn",
    "groupLevel",
    "groupCriteria",
    "receiverNameAddress",
    "paymentDetailsText",
    "paymentInstruction",
    "batchRun",
    "batchOut",
    "batchIn",
    "paymentItemCollection",
    "paymentSpecificationCollection"
})
public class Payment
    extends TIAObject
{

    @XmlElementRef(name = "paymentNo", namespace = "http://infrastructure.tia.dk/schema/account/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<Long> paymentNo;
    @XmlSchemaType(name = "unsignedLong")
    protected long accountNo;
    @XmlElementRef(name = "amount", namespace = "http://infrastructure.tia.dk/schema/account/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<BigDecimal> amount;
    @XmlElementRef(name = "amountIn", namespace = "http://infrastructure.tia.dk/schema/account/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<BigDecimal> amountIn;
    @XmlElementRef(name = "currencyCode", namespace = "http://infrastructure.tia.dk/schema/account/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> currencyCode;
    @XmlElementRef(name = "receiverId", namespace = "http://infrastructure.tia.dk/schema/account/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<Long> receiverId;
    @XmlElementRef(name = "dueDate", namespace = "http://infrastructure.tia.dk/schema/account/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> dueDate;
    @XmlElementRef(name = "valueDateIn", namespace = "http://infrastructure.tia.dk/schema/account/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> valueDateIn;
    @XmlElementRef(name = "transactionDateOut", namespace = "http://infrastructure.tia.dk/schema/account/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> transactionDateOut;
    @XmlElementRef(name = "transactionDateIn", namespace = "http://infrastructure.tia.dk/schema/account/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> transactionDateIn;
    @XmlElementRef(name = "paymentStatus", namespace = "http://infrastructure.tia.dk/schema/account/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> paymentStatus;
    @XmlElementRef(name = "paymentStatusOut", namespace = "http://infrastructure.tia.dk/schema/account/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> paymentStatusOut;
    @XmlElementRef(name = "paymentMethod", namespace = "http://infrastructure.tia.dk/schema/account/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> paymentMethod;
    @XmlElementRef(name = "paymentDetailsId", namespace = "http://infrastructure.tia.dk/schema/account/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<Long> paymentDetailsId;
    @XmlElementRef(name = "paymentChannel", namespace = "http://infrastructure.tia.dk/schema/account/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> paymentChannel;
    @XmlElementRef(name = "recollectionTransId", namespace = "http://infrastructure.tia.dk/schema/account/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<Long> recollectionTransId;
    @XmlElementRef(name = "externalCode", namespace = "http://infrastructure.tia.dk/schema/account/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> externalCode;
    @XmlElementRef(name = "externalCodeIn", namespace = "http://infrastructure.tia.dk/schema/account/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> externalCodeIn;
    @XmlElementRef(name = "externalRefIn", namespace = "http://infrastructure.tia.dk/schema/account/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> externalRefIn;
    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected GroupLevel groupLevel;
    @XmlSchemaType(name = "unsignedLong")
    protected long groupCriteria;
    @XmlElementRef(name = "receiverNameAddress", namespace = "http://infrastructure.tia.dk/schema/account/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> receiverNameAddress;
    @XmlElementRef(name = "paymentDetailsText", namespace = "http://infrastructure.tia.dk/schema/account/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> paymentDetailsText;
    @XmlElementRef(name = "paymentInstruction", namespace = "http://infrastructure.tia.dk/schema/account/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> paymentInstruction;
    @XmlElementRef(name = "batchRun", namespace = "http://infrastructure.tia.dk/schema/account/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<Long> batchRun;
    @XmlElementRef(name = "batchOut", namespace = "http://infrastructure.tia.dk/schema/account/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<Long> batchOut;
    @XmlElementRef(name = "batchIn", namespace = "http://infrastructure.tia.dk/schema/account/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<Long> batchIn;
    protected PaymentItemCollection paymentItemCollection;
    protected PaymentSpecificationCollection paymentSpecificationCollection;

    /**
     * Gets the value of the paymentNo property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Long }{@code >}
     *     
     */
    public JAXBElement<Long> getPaymentNo() {
        return paymentNo;
    }

    /**
     * Sets the value of the paymentNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Long }{@code >}
     *     
     */
    public void setPaymentNo(JAXBElement<Long> value) {
        this.paymentNo = value;
    }

    /**
     * Gets the value of the accountNo property.
     * 
     */
    public long getAccountNo() {
        return accountNo;
    }

    /**
     * Sets the value of the accountNo property.
     * 
     */
    public void setAccountNo(long value) {
        this.accountNo = value;
    }

    /**
     * Gets the value of the amount property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public JAXBElement<BigDecimal> getAmount() {
        return amount;
    }

    /**
     * Sets the value of the amount property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public void setAmount(JAXBElement<BigDecimal> value) {
        this.amount = value;
    }

    /**
     * Gets the value of the amountIn property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public JAXBElement<BigDecimal> getAmountIn() {
        return amountIn;
    }

    /**
     * Sets the value of the amountIn property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public void setAmountIn(JAXBElement<BigDecimal> value) {
        this.amountIn = value;
    }

    /**
     * Gets the value of the currencyCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCurrencyCode() {
        return currencyCode;
    }

    /**
     * Sets the value of the currencyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCurrencyCode(JAXBElement<String> value) {
        this.currencyCode = value;
    }

    /**
     * Gets the value of the receiverId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Long }{@code >}
     *     
     */
    public JAXBElement<Long> getReceiverId() {
        return receiverId;
    }

    /**
     * Sets the value of the receiverId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Long }{@code >}
     *     
     */
    public void setReceiverId(JAXBElement<Long> value) {
        this.receiverId = value;
    }

    /**
     * Gets the value of the dueDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getDueDate() {
        return dueDate;
    }

    /**
     * Sets the value of the dueDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setDueDate(JAXBElement<XMLGregorianCalendar> value) {
        this.dueDate = value;
    }

    /**
     * Gets the value of the valueDateIn property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getValueDateIn() {
        return valueDateIn;
    }

    /**
     * Sets the value of the valueDateIn property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setValueDateIn(JAXBElement<XMLGregorianCalendar> value) {
        this.valueDateIn = value;
    }

    /**
     * Gets the value of the transactionDateOut property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getTransactionDateOut() {
        return transactionDateOut;
    }

    /**
     * Sets the value of the transactionDateOut property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setTransactionDateOut(JAXBElement<XMLGregorianCalendar> value) {
        this.transactionDateOut = value;
    }

    /**
     * Gets the value of the transactionDateIn property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getTransactionDateIn() {
        return transactionDateIn;
    }

    /**
     * Sets the value of the transactionDateIn property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setTransactionDateIn(JAXBElement<XMLGregorianCalendar> value) {
        this.transactionDateIn = value;
    }

    /**
     * Gets the value of the paymentStatus property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getPaymentStatus() {
        return paymentStatus;
    }

    /**
     * Sets the value of the paymentStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setPaymentStatus(JAXBElement<Integer> value) {
        this.paymentStatus = value;
    }

    /**
     * Gets the value of the paymentStatusOut property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getPaymentStatusOut() {
        return paymentStatusOut;
    }

    /**
     * Sets the value of the paymentStatusOut property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setPaymentStatusOut(JAXBElement<Integer> value) {
        this.paymentStatusOut = value;
    }

    /**
     * Gets the value of the paymentMethod property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPaymentMethod() {
        return paymentMethod;
    }

    /**
     * Sets the value of the paymentMethod property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPaymentMethod(JAXBElement<String> value) {
        this.paymentMethod = value;
    }

    /**
     * Gets the value of the paymentDetailsId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Long }{@code >}
     *     
     */
    public JAXBElement<Long> getPaymentDetailsId() {
        return paymentDetailsId;
    }

    /**
     * Sets the value of the paymentDetailsId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Long }{@code >}
     *     
     */
    public void setPaymentDetailsId(JAXBElement<Long> value) {
        this.paymentDetailsId = value;
    }

    /**
     * Gets the value of the paymentChannel property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPaymentChannel() {
        return paymentChannel;
    }

    /**
     * Sets the value of the paymentChannel property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPaymentChannel(JAXBElement<String> value) {
        this.paymentChannel = value;
    }

    /**
     * Gets the value of the recollectionTransId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Long }{@code >}
     *     
     */
    public JAXBElement<Long> getRecollectionTransId() {
        return recollectionTransId;
    }

    /**
     * Sets the value of the recollectionTransId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Long }{@code >}
     *     
     */
    public void setRecollectionTransId(JAXBElement<Long> value) {
        this.recollectionTransId = value;
    }

    /**
     * Gets the value of the externalCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getExternalCode() {
        return externalCode;
    }

    /**
     * Sets the value of the externalCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setExternalCode(JAXBElement<String> value) {
        this.externalCode = value;
    }

    /**
     * Gets the value of the externalCodeIn property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getExternalCodeIn() {
        return externalCodeIn;
    }

    /**
     * Sets the value of the externalCodeIn property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setExternalCodeIn(JAXBElement<String> value) {
        this.externalCodeIn = value;
    }

    /**
     * Gets the value of the externalRefIn property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getExternalRefIn() {
        return externalRefIn;
    }

    /**
     * Sets the value of the externalRefIn property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setExternalRefIn(JAXBElement<String> value) {
        this.externalRefIn = value;
    }

    /**
     * Gets the value of the groupLevel property.
     * 
     * @return
     *     possible object is
     *     {@link GroupLevel }
     *     
     */
    public GroupLevel getGroupLevel() {
        return groupLevel;
    }

    /**
     * Sets the value of the groupLevel property.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupLevel }
     *     
     */
    public void setGroupLevel(GroupLevel value) {
        this.groupLevel = value;
    }

    /**
     * Gets the value of the groupCriteria property.
     * 
     */
    public long getGroupCriteria() {
        return groupCriteria;
    }

    /**
     * Sets the value of the groupCriteria property.
     * 
     */
    public void setGroupCriteria(long value) {
        this.groupCriteria = value;
    }

    /**
     * Gets the value of the receiverNameAddress property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getReceiverNameAddress() {
        return receiverNameAddress;
    }

    /**
     * Sets the value of the receiverNameAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setReceiverNameAddress(JAXBElement<String> value) {
        this.receiverNameAddress = value;
    }

    /**
     * Gets the value of the paymentDetailsText property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPaymentDetailsText() {
        return paymentDetailsText;
    }

    /**
     * Sets the value of the paymentDetailsText property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPaymentDetailsText(JAXBElement<String> value) {
        this.paymentDetailsText = value;
    }

    /**
     * Gets the value of the paymentInstruction property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPaymentInstruction() {
        return paymentInstruction;
    }

    /**
     * Sets the value of the paymentInstruction property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPaymentInstruction(JAXBElement<String> value) {
        this.paymentInstruction = value;
    }

    /**
     * Gets the value of the batchRun property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Long }{@code >}
     *     
     */
    public JAXBElement<Long> getBatchRun() {
        return batchRun;
    }

    /**
     * Sets the value of the batchRun property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Long }{@code >}
     *     
     */
    public void setBatchRun(JAXBElement<Long> value) {
        this.batchRun = value;
    }

    /**
     * Gets the value of the batchOut property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Long }{@code >}
     *     
     */
    public JAXBElement<Long> getBatchOut() {
        return batchOut;
    }

    /**
     * Sets the value of the batchOut property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Long }{@code >}
     *     
     */
    public void setBatchOut(JAXBElement<Long> value) {
        this.batchOut = value;
    }

    /**
     * Gets the value of the batchIn property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Long }{@code >}
     *     
     */
    public JAXBElement<Long> getBatchIn() {
        return batchIn;
    }

    /**
     * Sets the value of the batchIn property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Long }{@code >}
     *     
     */
    public void setBatchIn(JAXBElement<Long> value) {
        this.batchIn = value;
    }

    /**
     * Gets the value of the paymentItemCollection property.
     * 
     * @return
     *     possible object is
     *     {@link PaymentItemCollection }
     *     
     */
    public PaymentItemCollection getPaymentItemCollection() {
        return paymentItemCollection;
    }

    /**
     * Sets the value of the paymentItemCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link PaymentItemCollection }
     *     
     */
    public void setPaymentItemCollection(PaymentItemCollection value) {
        this.paymentItemCollection = value;
    }

    /**
     * Gets the value of the paymentSpecificationCollection property.
     * 
     * @return
     *     possible object is
     *     {@link PaymentSpecificationCollection }
     *     
     */
    public PaymentSpecificationCollection getPaymentSpecificationCollection() {
        return paymentSpecificationCollection;
    }

    /**
     * Sets the value of the paymentSpecificationCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link PaymentSpecificationCollection }
     *     
     */
    public void setPaymentSpecificationCollection(PaymentSpecificationCollection value) {
        this.paymentSpecificationCollection = value;
    }

}
