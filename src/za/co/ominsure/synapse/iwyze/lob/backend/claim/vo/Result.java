
package za.co.ominsure.synapse.iwyze.lob.backend.claim.vo;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * Mappes into TIA Commons type "obj_result"
 * 
 * <p>Java class for Result complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Result">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="resultCode" type="{http://infrastructure.tia.dk/schema/common/v2/}resultCode"/>
 *         &lt;element name="messageCollection" type="{http://infrastructure.tia.dk/schema/common/v2/}MessageCollection"/>
 *         &lt;element name="callDuration" type="{http://infrastructure.tia.dk/schema/common/v2/}callDuration"/>
 *         &lt;element name="logId" type="{http://infrastructure.tia.dk/schema/common/v2/}logId"/>
 *         &lt;element name="correlationId" type="{http://infrastructure.tia.dk/schema/common/v2/}correlationId"/>
 *         &lt;element name="operationSCN" type="{http://infrastructure.tia.dk/schema/common/v2/}operationSCN"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Result", namespace = "http://infrastructure.tia.dk/schema/common/v2/", propOrder = {
    "resultCode",
    "messageCollection",
    "callDuration",
    "logId",
    "correlationId",
    "operationSCN"
})
public class Result {

    @XmlSchemaType(name = "integer")
    protected Integer resultCode;
    @XmlElement(required = true)
    protected MessageCollection messageCollection;
    @XmlElement(required = true)
    @XmlSchemaType(name = "unsignedLong")
    protected BigInteger callDuration;
    @XmlElement(required = true)
    protected String logId;
    @XmlElement(required = true, nillable = true)
    protected String correlationId;
    @XmlElement(required = true, nillable = true)
    @XmlSchemaType(name = "unsignedLong")
    protected BigInteger operationSCN;

    /**
     * Gets the value of the resultCode property.
     * 
     */
    public Integer getResultCode() {
        return resultCode;
    }

    /**
     * Sets the value of the resultCode property.
     * 
     */
    public void setResultCode(Integer value) {
        this.resultCode = value;
    }

    /**
     * Gets the value of the messageCollection property.
     * 
     * @return
     *     possible object is
     *     {@link MessageCollection }
     *     
     */
    public MessageCollection getMessageCollection() {
        return messageCollection;
    }

    /**
     * Sets the value of the messageCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link MessageCollection }
     *     
     */
    public void setMessageCollection(MessageCollection value) {
        this.messageCollection = value;
    }

    /**
     * Gets the value of the callDuration property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getCallDuration() {
        return callDuration;
    }

    /**
     * Sets the value of the callDuration property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setCallDuration(BigInteger value) {
        this.callDuration = value;
    }

    /**
     * Gets the value of the logId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLogId() {
        return logId;
    }

    /**
     * Sets the value of the logId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLogId(String value) {
        this.logId = value;
    }

    /**
     * Gets the value of the correlationId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorrelationId() {
        return correlationId;
    }

    /**
     * Sets the value of the correlationId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorrelationId(String value) {
        this.correlationId = value;
    }

    /**
     * Gets the value of the operationSCN property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getOperationSCN() {
        return operationSCN;
    }

    /**
     * Sets the value of the operationSCN property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setOperationSCN(BigInteger value) {
        this.operationSCN = value;
    }

}
