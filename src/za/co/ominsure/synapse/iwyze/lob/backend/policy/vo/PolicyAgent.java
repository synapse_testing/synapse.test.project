
package za.co.ominsure.synapse.iwyze.lob.backend.policy.vo;

import java.math.BigDecimal;
import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * Is mapped into xxxx".
 * 
 * <p>Java class for PolicyAgent complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PolicyAgent">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="agentPositionOnPolicy" type="{http://infrastructure.tia.dk/schema/common/v2/}int2" minOccurs="0"/>
 *         &lt;element name="agentNo" type="{http://infrastructure.tia.dk/schema/policy/v2/}seqNo" minOccurs="0"/>
 *         &lt;element name="agentRole" type="{http://infrastructure.tia.dk/schema/policy/v2/}seqNo" minOccurs="0"/>
 *         &lt;element name="comGroup" type="{http://infrastructure.tia.dk/schema/policy/v2/}comGroup" minOccurs="0"/>
 *         &lt;element name="agentPct" type="{http://infrastructure.tia.dk/schema/common/v2/}decimal5Point2" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PolicyAgent", namespace = "http://infrastructure.tia.dk/schema/policy/v4/", propOrder = {
    "agentPositionOnPolicy",
    "agentNo",
    "agentRole",
    "comGroup",
    "agentPct"
})
public class PolicyAgent {

    protected Long agentPositionOnPolicy;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "unsignedLong")
    protected BigInteger agentNo;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "unsignedLong")
    protected BigInteger agentRole;
    @XmlElement(nillable = true)
    protected String comGroup;
    @XmlElement(nillable = true)
    protected BigDecimal agentPct;

    /**
     * Gets the value of the agentPositionOnPolicy property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getAgentPositionOnPolicy() {
        return agentPositionOnPolicy;
    }

    /**
     * Sets the value of the agentPositionOnPolicy property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setAgentPositionOnPolicy(Long value) {
        this.agentPositionOnPolicy = value;
    }

    /**
     * Gets the value of the agentNo property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getAgentNo() {
        return agentNo;
    }

    /**
     * Sets the value of the agentNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setAgentNo(BigInteger value) {
        this.agentNo = value;
    }

    /**
     * Gets the value of the agentRole property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getAgentRole() {
        return agentRole;
    }

    /**
     * Sets the value of the agentRole property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setAgentRole(BigInteger value) {
        this.agentRole = value;
    }

    /**
     * Gets the value of the comGroup property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComGroup() {
        return comGroup;
    }

    /**
     * Sets the value of the comGroup property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComGroup(String value) {
        this.comGroup = value;
    }

    /**
     * Gets the value of the agentPct property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAgentPct() {
        return agentPct;
    }

    /**
     * Sets the value of the agentPct property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAgentPct(BigDecimal value) {
        this.agentPct = value;
    }

}
