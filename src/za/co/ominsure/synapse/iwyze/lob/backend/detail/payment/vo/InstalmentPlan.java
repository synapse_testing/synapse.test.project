
package za.co.ominsure.synapse.iwyze.lob.backend.detail.payment.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Additional comments
 * 
 * <p>Java class for InstalmentPlan complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="InstalmentPlan">
 *   &lt;complexContent>
 *     &lt;extension base="{http://infrastructure.tia.dk/schema/common/v2/}TIAObject">
 *       &lt;sequence>
 *         &lt;element name="accountNo" type="{http://infrastructure.tia.dk/schema/common/v2/}long10" minOccurs="0"/>
 *         &lt;element name="currencyCode" type="{http://infrastructure.tia.dk/schema/common/v2/}string3" minOccurs="0"/>
 *         &lt;element name="planStartDate" type="{http://infrastructure.tia.dk/schema/common/v2/}date" minOccurs="0"/>
 *         &lt;element name="planEndDate" type="{http://infrastructure.tia.dk/schema/common/v2/}date" minOccurs="0"/>
 *         &lt;element name="mpPolicyNo" type="{http://infrastructure.tia.dk/schema/common/v2/}long10" minOccurs="0"/>
 *         &lt;element name="policyNo" type="{http://infrastructure.tia.dk/schema/common/v2/}long10" minOccurs="0"/>
 *         &lt;element name="policyLineNo" type="{http://infrastructure.tia.dk/schema/common/v2/}long10" minOccurs="0"/>
 *         &lt;element name="plannedInstalmentCollection" type="{http://infrastructure.tia.dk/schema/account/v2/}PlannedInstalmentCollection" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InstalmentPlan", propOrder = {
    "accountNo",
    "currencyCode",
    "planStartDate",
    "planEndDate",
    "mpPolicyNo",
    "policyNo",
    "policyLineNo",
    "plannedInstalmentCollection"
})
public class InstalmentPlan
    extends TIAObject
{

    @XmlElement(nillable = true)
    protected Long accountNo;
    @XmlElement(nillable = true)
    protected String currencyCode;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar planStartDate;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar planEndDate;
    @XmlElement(nillable = true)
    protected Long mpPolicyNo;
    @XmlElement(nillable = true)
    protected Long policyNo;
    @XmlElement(nillable = true)
    protected Long policyLineNo;
    protected PlannedInstalmentCollection plannedInstalmentCollection;

    /**
     * Gets the value of the accountNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getAccountNo() {
        return accountNo;
    }

    /**
     * Sets the value of the accountNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setAccountNo(Long value) {
        this.accountNo = value;
    }

    /**
     * Gets the value of the currencyCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrencyCode() {
        return currencyCode;
    }

    /**
     * Sets the value of the currencyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrencyCode(String value) {
        this.currencyCode = value;
    }

    /**
     * Gets the value of the planStartDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPlanStartDate() {
        return planStartDate;
    }

    /**
     * Sets the value of the planStartDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPlanStartDate(XMLGregorianCalendar value) {
        this.planStartDate = value;
    }

    /**
     * Gets the value of the planEndDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPlanEndDate() {
        return planEndDate;
    }

    /**
     * Sets the value of the planEndDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPlanEndDate(XMLGregorianCalendar value) {
        this.planEndDate = value;
    }

    /**
     * Gets the value of the mpPolicyNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getMpPolicyNo() {
        return mpPolicyNo;
    }

    /**
     * Sets the value of the mpPolicyNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setMpPolicyNo(Long value) {
        this.mpPolicyNo = value;
    }

    /**
     * Gets the value of the policyNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getPolicyNo() {
        return policyNo;
    }

    /**
     * Sets the value of the policyNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setPolicyNo(Long value) {
        this.policyNo = value;
    }

    /**
     * Gets the value of the policyLineNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getPolicyLineNo() {
        return policyLineNo;
    }

    /**
     * Sets the value of the policyLineNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setPolicyLineNo(Long value) {
        this.policyLineNo = value;
    }

    /**
     * Gets the value of the plannedInstalmentCollection property.
     * 
     * @return
     *     possible object is
     *     {@link PlannedInstalmentCollection }
     *     
     */
    public PlannedInstalmentCollection getPlannedInstalmentCollection() {
        return plannedInstalmentCollection;
    }

    /**
     * Sets the value of the plannedInstalmentCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link PlannedInstalmentCollection }
     *     
     */
    public void setPlannedInstalmentCollection(PlannedInstalmentCollection value) {
        this.plannedInstalmentCollection = value;
    }

}
