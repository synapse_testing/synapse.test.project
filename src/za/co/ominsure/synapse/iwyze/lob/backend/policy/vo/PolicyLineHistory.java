
package za.co.ominsure.synapse.iwyze.lob.backend.policy.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * Is mapped into undefined".
 * 
 * <p>Java class for PolicyLineHistory complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PolicyLineHistory">
 *   &lt;complexContent>
 *     &lt;extension base="{http://infrastructure.tia.dk/schema/common/v2/}TIAObject">
 *       &lt;sequence>
 *         &lt;element name="policyLineShortCollection" type="{http://infrastructure.tia.dk/schema/policy/v2/}PolicyLineShortCollection" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PolicyLineHistory", propOrder = {
    "policyLineShortCollection"
})
public class PolicyLineHistory
    extends TIAObject
{

    protected PolicyLineShortCollection policyLineShortCollection;

    /**
     * Gets the value of the policyLineShortCollection property.
     * 
     * @return
     *     possible object is
     *     {@link PolicyLineShortCollection }
     *     
     */
    public PolicyLineShortCollection getPolicyLineShortCollection() {
        return policyLineShortCollection;
    }

    /**
     * Sets the value of the policyLineShortCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link PolicyLineShortCollection }
     *     
     */
    public void setPolicyLineShortCollection(PolicyLineShortCollection value) {
        this.policyLineShortCollection = value;
    }

}
