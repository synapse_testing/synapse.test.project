
package za.co.ominsure.synapse.iwyze.lob.backend.policy.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * Is mapped into TIA metadata type "obj_party".
 * 
 * <p>Java class for AbstractParty complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AbstractParty">
 *   &lt;complexContent>
 *     &lt;extension base="{http://infrastructure.tia.dk/schema/common/v2/}TIAObject">
 *       &lt;sequence>
 *         &lt;element name="idNo" type="{http://infrastructure.tia.dk/schema/common/v2/}seqNo" minOccurs="0"/>
 *         &lt;element name="surname" type="{http://infrastructure.tia.dk/schema/common/v2/}string200" minOccurs="0"/>
 *         &lt;element name="name" type="{http://infrastructure.tia.dk/schema/party/v2/}shortName" minOccurs="0"/>
 *         &lt;element name="language" type="{http://infrastructure.tia.dk/schema/common/v2/}language" minOccurs="0"/>
 *         &lt;element name="currencyCode" type="{http://infrastructure.tia.dk/schema/common/v2/}currencyCode" minOccurs="0"/>
 *         &lt;element name="nameType" type="{http://infrastructure.tia.dk/schema/party/v2/}nameType" minOccurs="0"/>
 *         &lt;element name="obsoleteCode" type="{http://infrastructure.tia.dk/schema/party/v2/}obsoleteCode" minOccurs="0"/>
 *         &lt;element name="addressCollection" type="{http://infrastructure.tia.dk/schema/party/v2/}AddressCollection" minOccurs="0"/>
 *         &lt;element name="nameSort" type="{http://infrastructure.tia.dk/schema/party/v2/}nameSort" minOccurs="0"/>
 *         &lt;element name="idNoAlt" type="{http://infrastructure.tia.dk/schema/common/v2/}idNoAlt" minOccurs="0"/>
 *         &lt;element name="idNoSort" type="{http://infrastructure.tia.dk/schema/common/v2/}idNoSort" minOccurs="0"/>
 *         &lt;element name="contactInfoCollection" type="{http://infrastructure.tia.dk/schema/party/v2/}ContactInfoCollection" minOccurs="0"/>
 *         &lt;element name="customerInfo" type="{http://infrastructure.tia.dk/schema/party/v2/}Customer" minOccurs="0"/>
 *         &lt;element name="partyRoleCollection" type="{http://infrastructure.tia.dk/schema/party/v2/}PartyRoleCollection" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AbstractParty", namespace = "http://infrastructure.tia.dk/schema/party/v2/", propOrder = {
    "idNo",
    "surname",
    "name",
    "language",
    "currencyCode",
    "nameType",
    "obsoleteCode",
    "addressCollection",
    "nameSort",
    "idNoAlt",
    "idNoSort",
    "contactInfoCollection",
    "customerInfo",
    "partyRoleCollection"
})
@XmlSeeAlso({
    Institution.class,
    Person.class,
    PartyOther.class
})
public abstract class AbstractParty
    extends TIAObject
{

    @XmlElement(nillable = true)
    @XmlSchemaType(name = "unsignedLong")
    protected Long idNo;
    @XmlElement(nillable = true)
    protected String surname;
    @XmlElement(nillable = true)
    protected String name;
    @XmlElement(nillable = true)
    protected String language;
    @XmlElement(nillable = true)
    protected String currencyCode;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "integer")
    protected Integer nameType;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "integer")
    protected Integer obsoleteCode;
    protected AddressCollection addressCollection;
    @XmlElement(nillable = true)
    protected String nameSort;
    @XmlElement(nillable = true)
    protected String idNoAlt;
    @XmlElement(nillable = true)
    protected String idNoSort;
    protected ContactInfoCollection contactInfoCollection;
    @XmlElement(nillable = true)
    protected Customer customerInfo;
    protected PartyRoleCollection partyRoleCollection;

    /**
     * Gets the value of the idNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getIdNo() {
        return idNo;
    }

    /**
     * Sets the value of the idNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setIdNo(Long value) {
        this.idNo = value;
    }

    /**
     * Gets the value of the surname property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSurname() {
        return surname;
    }

    /**
     * Sets the value of the surname property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSurname(String value) {
        this.surname = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the language property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLanguage() {
        return language;
    }

    /**
     * Sets the value of the language property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLanguage(String value) {
        this.language = value;
    }

    /**
     * Gets the value of the currencyCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrencyCode() {
        return currencyCode;
    }

    /**
     * Sets the value of the currencyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrencyCode(String value) {
        this.currencyCode = value;
    }

    /**
     * Gets the value of the nameType property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNameType() {
        return nameType;
    }

    /**
     * Sets the value of the nameType property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNameType(Integer value) {
        this.nameType = value;
    }

    /**
     * Gets the value of the obsoleteCode property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getObsoleteCode() {
        return obsoleteCode;
    }

    /**
     * Sets the value of the obsoleteCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setObsoleteCode(Integer value) {
        this.obsoleteCode = value;
    }

    /**
     * Gets the value of the addressCollection property.
     * 
     * @return
     *     possible object is
     *     {@link AddressCollection }
     *     
     */
    public AddressCollection getAddressCollection() {
        return addressCollection;
    }

    /**
     * Sets the value of the addressCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link AddressCollection }
     *     
     */
    public void setAddressCollection(AddressCollection value) {
        this.addressCollection = value;
    }

    /**
     * Gets the value of the nameSort property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNameSort() {
        return nameSort;
    }

    /**
     * Sets the value of the nameSort property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNameSort(String value) {
        this.nameSort = value;
    }

    /**
     * Gets the value of the idNoAlt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdNoAlt() {
        return idNoAlt;
    }

    /**
     * Sets the value of the idNoAlt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdNoAlt(String value) {
        this.idNoAlt = value;
    }

    /**
     * Gets the value of the idNoSort property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdNoSort() {
        return idNoSort;
    }

    /**
     * Sets the value of the idNoSort property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdNoSort(String value) {
        this.idNoSort = value;
    }

    /**
     * Gets the value of the contactInfoCollection property.
     * 
     * @return
     *     possible object is
     *     {@link ContactInfoCollection }
     *     
     */
    public ContactInfoCollection getContactInfoCollection() {
        return contactInfoCollection;
    }

    /**
     * Sets the value of the contactInfoCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link ContactInfoCollection }
     *     
     */
    public void setContactInfoCollection(ContactInfoCollection value) {
        this.contactInfoCollection = value;
    }

    /**
     * Gets the value of the customerInfo property.
     * 
     * @return
     *     possible object is
     *     {@link Customer }
     *     
     */
    public Customer getCustomerInfo() {
        return customerInfo;
    }

    /**
     * Sets the value of the customerInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Customer }
     *     
     */
    public void setCustomerInfo(Customer value) {
        this.customerInfo = value;
    }

    /**
     * Gets the value of the partyRoleCollection property.
     * 
     * @return
     *     possible object is
     *     {@link PartyRoleCollection }
     *     
     */
    public PartyRoleCollection getPartyRoleCollection() {
        return partyRoleCollection;
    }

    /**
     * Sets the value of the partyRoleCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link PartyRoleCollection }
     *     
     */
    public void setPartyRoleCollection(PartyRoleCollection value) {
        this.partyRoleCollection = value;
    }

}
