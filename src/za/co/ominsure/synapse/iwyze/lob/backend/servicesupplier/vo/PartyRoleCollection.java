
package za.co.ominsure.synapse.iwyze.lob.backend.servicesupplier.vo;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * Is mapped into TIA metadata type "tab_party_role".
 * 
 * <p>Java class for PartyRoleCollection complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PartyRoleCollection">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;sequence>
 *           &lt;element name="bank" type="{http://infrastructure.tia.dk/schema/party/v2/}Bank" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;/sequence>
 *         &lt;sequence>
 *           &lt;element name="agent" type="{http://infrastructure.tia.dk/schema/party/v2/}Agent" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;/sequence>
 *         &lt;sequence>
 *           &lt;element name="serviceSupplier" type="{http://infrastructure.tia.dk/schema/party/v2/}ServiceSupplier" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;/sequence>
 *         &lt;sequence>
 *           &lt;element name="insuranceCompany" type="{http://infrastructure.tia.dk/schema/party/v2/}InsuranceCompany" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;/sequence>
 *         &lt;sequence>
 *           &lt;element name="affinity" type="{http://infrastructure.tia.dk/schema/party/v2/}Affinity" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;/sequence>
 *         &lt;sequence>
 *           &lt;element name="interestedParty" type="{http://infrastructure.tia.dk/schema/party/v2/}InterestedParty" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;/sequence>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PartyRoleCollection", propOrder = {
    "bank",
    "agent",
    "serviceSupplier",
    "insuranceCompany",
    "affinity",
    "interestedParty"
})
public class PartyRoleCollection {

    protected List<Bank> bank;
    protected List<Agent> agent;
    protected List<ServiceSupplier> serviceSupplier;
    protected List<InsuranceCompany> insuranceCompany;
    protected List<Affinity> affinity;
    protected List<InterestedParty> interestedParty;

    /**
     * Gets the value of the bank property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the bank property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBank().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Bank }
     * 
     * 
     */
    public List<Bank> getBank() {
        if (bank == null) {
            bank = new ArrayList<Bank>();
        }
        return this.bank;
    }

    /**
     * Gets the value of the agent property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the agent property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAgent().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Agent }
     * 
     * 
     */
    public List<Agent> getAgent() {
        if (agent == null) {
            agent = new ArrayList<Agent>();
        }
        return this.agent;
    }

    /**
     * Gets the value of the serviceSupplier property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the serviceSupplier property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getServiceSupplier().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ServiceSupplier }
     * 
     * 
     */
    public List<ServiceSupplier> getServiceSupplier() {
        if (serviceSupplier == null) {
            serviceSupplier = new ArrayList<ServiceSupplier>();
        }
        return this.serviceSupplier;
    }

    /**
     * Gets the value of the insuranceCompany property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the insuranceCompany property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInsuranceCompany().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link InsuranceCompany }
     * 
     * 
     */
    public List<InsuranceCompany> getInsuranceCompany() {
        if (insuranceCompany == null) {
            insuranceCompany = new ArrayList<InsuranceCompany>();
        }
        return this.insuranceCompany;
    }

    /**
     * Gets the value of the affinity property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the affinity property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAffinity().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Affinity }
     * 
     * 
     */
    public List<Affinity> getAffinity() {
        if (affinity == null) {
            affinity = new ArrayList<Affinity>();
        }
        return this.affinity;
    }

    /**
     * Gets the value of the interestedParty property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the interestedParty property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInterestedParty().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link InterestedParty }
     * 
     * 
     */
    public List<InterestedParty> getInterestedParty() {
        if (interestedParty == null) {
            interestedParty = new ArrayList<InterestedParty>();
        }
        return this.interestedParty;
    }

}
