package za.co.ominsure.synapse.iwyze.lob.backend.caseItem.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author Johan Prins
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "clegType", "requestId" })
@XmlRootElement(name = "CaseItemGenericResponse")
public class CaseItemGenericResponse {

	@XmlElement
	private String clegType;
	@XmlElement
	private String requestId;

	public String getClegType() {
		return clegType;
	}

	public void setClegType(String clegType) {
		this.clegType = clegType;
	}

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

}
