
package za.co.ominsure.synapse.iwyze.lob.backend.policy.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Is mapped into TIA metadata type "obj_customer".
 * 
 * <p>Java class for Customer complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Customer">
 *   &lt;complexContent>
 *     &lt;extension base="{http://infrastructure.tia.dk/schema/common/v2/}TIAObject">
 *       &lt;sequence>
 *         &lt;element name="potential" type="{http://infrastructure.tia.dk/schema/party/v2/}potential" minOccurs="0"/>
 *         &lt;element name="education" type="{http://infrastructure.tia.dk/schema/party/v2/}education" minOccurs="0"/>
 *         &lt;element name="occupationCode" type="{http://infrastructure.tia.dk/schema/party/v2/}occupationCode" minOccurs="0"/>
 *         &lt;element name="occupation" type="{http://infrastructure.tia.dk/schema/party/v2/}occupation" minOccurs="0"/>
 *         &lt;element name="profileCode" type="{http://infrastructure.tia.dk/schema/party/v2/}profileCode" minOccurs="0"/>
 *         &lt;element name="statusCode" type="{http://infrastructure.tia.dk/schema/party/v2/}statusCode" minOccurs="0"/>
 *         &lt;element name="incomeGroup" type="{http://infrastructure.tia.dk/schema/party/v2/}incomeGroup" minOccurs="0"/>
 *         &lt;element name="sex" type="{http://infrastructure.tia.dk/schema/party/v2/}sex" minOccurs="0"/>
 *         &lt;element name="maritalState" type="{http://infrastructure.tia.dk/schema/party/v2/}maritalState" minOccurs="0"/>
 *         &lt;element name="spouseBirthDate" type="{http://infrastructure.tia.dk/schema/common/v2/}date" minOccurs="0"/>
 *         &lt;element name="spouseName" type="{http://infrastructure.tia.dk/schema/party/v2/}spouseName" minOccurs="0"/>
 *         &lt;element name="spouseSex" type="{http://infrastructure.tia.dk/schema/party/v2/}spouseSex" minOccurs="0"/>
 *         &lt;element name="children" type="{http://infrastructure.tia.dk/schema/party/v2/}children" minOccurs="0"/>
 *         &lt;element name="household" type="{http://infrastructure.tia.dk/schema/party/v2/}household" minOccurs="0"/>
 *         &lt;element name="howNotToContact" type="{http://infrastructure.tia.dk/schema/party/v2/}contact" minOccurs="0"/>
 *         &lt;element name="howToContact" type="{http://infrastructure.tia.dk/schema/party/v2/}contact" minOccurs="0"/>
 *         &lt;element name="serviceCode" type="{http://infrastructure.tia.dk/schema/party/v2/}serviceCode" minOccurs="0"/>
 *         &lt;element name="yourRef" type="{http://infrastructure.tia.dk/schema/party/v2/}yourRef" minOccurs="0"/>
 *         &lt;element name="firstContact" type="{http://infrastructure.tia.dk/schema/common/v2/}date" minOccurs="0"/>
 *         &lt;element name="lastContact" type="{http://infrastructure.tia.dk/schema/common/v2/}date" minOccurs="0"/>
 *         &lt;element name="lastContactWay" type="{http://infrastructure.tia.dk/schema/party/v2/}contact" minOccurs="0"/>
 *         &lt;element name="reminderGroup" type="{http://infrastructure.tia.dk/schema/party/v2/}reminderGroup" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Customer", namespace = "http://infrastructure.tia.dk/schema/party/v2/", propOrder = {
    "potential",
    "education",
    "occupationCode",
    "occupation",
    "profileCode",
    "statusCode",
    "incomeGroup",
    "sex",
    "maritalState",
    "spouseBirthDate",
    "spouseName",
    "spouseSex",
    "children",
    "household",
    "howNotToContact",
    "howToContact",
    "serviceCode",
    "yourRef",
    "firstContact",
    "lastContact",
    "lastContactWay",
    "reminderGroup"
})
public class Customer
    extends TIAObject
{

    @XmlElement(nillable = true)
    protected String potential;
    @XmlElement(nillable = true)
    protected String education;
    @XmlElement(nillable = true)
    protected String occupationCode;
    @XmlElement(nillable = true)
    protected String occupation;
    @XmlElement(nillable = true)
    protected String profileCode;
    @XmlElement(nillable = true)
    protected String statusCode;
    @XmlElement(nillable = true)
    protected String incomeGroup;
    @XmlElement(nillable = true)
    protected String sex;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "integer")
    protected Integer maritalState;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar spouseBirthDate;
    @XmlElement(nillable = true)
    protected String spouseName;
    @XmlElement(nillable = true)
    protected String spouseSex;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "integer")
    protected Integer children;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "integer")
    protected Integer household;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "integer")
    protected Integer howNotToContact;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "integer")
    protected Integer howToContact;
    @XmlElement(nillable = true)
    protected String serviceCode;
    @XmlElement(nillable = true)
    protected String yourRef;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar firstContact;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar lastContact;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "integer")
    protected Integer lastContactWay;
    @XmlElement(nillable = true)
    protected String reminderGroup;

    /**
     * Gets the value of the potential property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPotential() {
        return potential;
    }

    /**
     * Sets the value of the potential property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPotential(String value) {
        this.potential = value;
    }

    /**
     * Gets the value of the education property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEducation() {
        return education;
    }

    /**
     * Sets the value of the education property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEducation(String value) {
        this.education = value;
    }

    /**
     * Gets the value of the occupationCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOccupationCode() {
        return occupationCode;
    }

    /**
     * Sets the value of the occupationCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOccupationCode(String value) {
        this.occupationCode = value;
    }

    /**
     * Gets the value of the occupation property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOccupation() {
        return occupation;
    }

    /**
     * Sets the value of the occupation property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOccupation(String value) {
        this.occupation = value;
    }

    /**
     * Gets the value of the profileCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProfileCode() {
        return profileCode;
    }

    /**
     * Sets the value of the profileCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProfileCode(String value) {
        this.profileCode = value;
    }

    /**
     * Gets the value of the statusCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatusCode() {
        return statusCode;
    }

    /**
     * Sets the value of the statusCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatusCode(String value) {
        this.statusCode = value;
    }

    /**
     * Gets the value of the incomeGroup property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIncomeGroup() {
        return incomeGroup;
    }

    /**
     * Sets the value of the incomeGroup property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIncomeGroup(String value) {
        this.incomeGroup = value;
    }

    /**
     * Gets the value of the sex property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSex() {
        return sex;
    }

    /**
     * Sets the value of the sex property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSex(String value) {
        this.sex = value;
    }

    /**
     * Gets the value of the maritalState property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaritalState() {
        return maritalState;
    }

    /**
     * Sets the value of the maritalState property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaritalState(Integer value) {
        this.maritalState = value;
    }

    /**
     * Gets the value of the spouseBirthDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getSpouseBirthDate() {
        return spouseBirthDate;
    }

    /**
     * Sets the value of the spouseBirthDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setSpouseBirthDate(XMLGregorianCalendar value) {
        this.spouseBirthDate = value;
    }

    /**
     * Gets the value of the spouseName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSpouseName() {
        return spouseName;
    }

    /**
     * Sets the value of the spouseName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSpouseName(String value) {
        this.spouseName = value;
    }

    /**
     * Gets the value of the spouseSex property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSpouseSex() {
        return spouseSex;
    }

    /**
     * Sets the value of the spouseSex property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSpouseSex(String value) {
        this.spouseSex = value;
    }

    /**
     * Gets the value of the children property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getChildren() {
        return children;
    }

    /**
     * Sets the value of the children property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setChildren(Integer value) {
        this.children = value;
    }

    /**
     * Gets the value of the household property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getHousehold() {
        return household;
    }

    /**
     * Sets the value of the household property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setHousehold(Integer value) {
        this.household = value;
    }

    /**
     * Gets the value of the howNotToContact property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getHowNotToContact() {
        return howNotToContact;
    }

    /**
     * Sets the value of the howNotToContact property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setHowNotToContact(Integer value) {
        this.howNotToContact = value;
    }

    /**
     * Gets the value of the howToContact property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getHowToContact() {
        return howToContact;
    }

    /**
     * Sets the value of the howToContact property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setHowToContact(Integer value) {
        this.howToContact = value;
    }

    /**
     * Gets the value of the serviceCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceCode() {
        return serviceCode;
    }

    /**
     * Sets the value of the serviceCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceCode(String value) {
        this.serviceCode = value;
    }

    /**
     * Gets the value of the yourRef property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getYourRef() {
        return yourRef;
    }

    /**
     * Sets the value of the yourRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setYourRef(String value) {
        this.yourRef = value;
    }

    /**
     * Gets the value of the firstContact property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFirstContact() {
        return firstContact;
    }

    /**
     * Sets the value of the firstContact property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFirstContact(XMLGregorianCalendar value) {
        this.firstContact = value;
    }

    /**
     * Gets the value of the lastContact property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getLastContact() {
        return lastContact;
    }

    /**
     * Sets the value of the lastContact property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setLastContact(XMLGregorianCalendar value) {
        this.lastContact = value;
    }

    /**
     * Gets the value of the lastContactWay property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getLastContactWay() {
        return lastContactWay;
    }

    /**
     * Sets the value of the lastContactWay property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setLastContactWay(Integer value) {
        this.lastContactWay = value;
    }

    /**
     * Gets the value of the reminderGroup property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReminderGroup() {
        return reminderGroup;
    }

    /**
     * Sets the value of the reminderGroup property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReminderGroup(String value) {
        this.reminderGroup = value;
    }

}
