
package za.co.ominsure.synapse.iwyze.lob.backend.policy.vo;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PendingPolicyTransShortCollection complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PendingPolicyTransShortCollection">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="pendingPolicyTransShort" type="{http://infrastructure.tia.dk/schema/policy/v2/}PendingPolicyTransShort" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PendingPolicyTransShortCollection", propOrder = {
    "pendingPolicyTransShorts"
})
public class PendingPolicyTransShortCollection {

    @XmlElement(name = "pendingPolicyTransShort")
    protected List<PendingPolicyTransShort> pendingPolicyTransShorts;

    /**
     * Gets the value of the pendingPolicyTransShorts property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the pendingPolicyTransShorts property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPendingPolicyTransShorts().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PendingPolicyTransShort }
     * 
     * 
     */
    public List<PendingPolicyTransShort> getPendingPolicyTransShorts() {
        if (pendingPolicyTransShorts == null) {
            pendingPolicyTransShorts = new ArrayList<PendingPolicyTransShort>();
        }
        return this.pendingPolicyTransShorts;
    }

}
