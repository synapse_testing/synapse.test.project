
package za.co.ominsure.synapse.iwyze.lob.backend.policy.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * Is mapped into obj_policy_line
 * 
 * <p>Java class for Clause complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Clause">
 *   &lt;complexContent>
 *     &lt;extension base="{http://infrastructure.tia.dk/schema/policy/v2/}Clause">
 *       &lt;sequence>
 *         &lt;element name="clauseVersion" type="{http://infrastructure.tia.dk/schema/common/v2/}seqNo" minOccurs="0"/>
 *         &lt;element name="clauseText" type="{http://infrastructure.tia.dk/schema/common/v2/}string4000" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Clause", namespace = "http://infrastructure.tia.dk/schema/policy/v4/", propOrder = {
    "clauseVersion",
    "clauseText"
})
public class Clause
    extends Clause2
{

    @XmlSchemaType(name = "unsignedLong")
    protected Long clauseVersion;
    protected String clauseText;

    /**
     * Gets the value of the clauseVersion property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getClauseVersion() {
        return clauseVersion;
    }

    /**
     * Sets the value of the clauseVersion property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setClauseVersion(Long value) {
        this.clauseVersion = value;
    }

    /**
     * Gets the value of the clauseText property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClauseText() {
        return clauseText;
    }

    /**
     * Sets the value of the clauseText property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClauseText(String value) {
        this.clauseText = value;
    }

}
