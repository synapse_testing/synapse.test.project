
package za.co.ominsure.synapse.iwyze.lob.backend.policy.vo;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Additional comments
 * 
 * <p>Java class for ObjectSlaveTypePlSpecific complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ObjectSlaveTypePlSpecific">
 *   &lt;complexContent>
 *     &lt;extension base="{http://infrastructure.tia.dk/schema/common/v2/}TIAObject">
 *       &lt;sequence>
 *         &lt;element name="objSlaveTypeId" type="{http://infrastructure.tia.dk/schema/common/v2/}string12" minOccurs="0"/>
 *         &lt;element name="objSlaveTypeVer" type="{http://infrastructure.tia.dk/schema/common/v2/}decimal9Point3" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ObjectSlaveTypePlSpecific", propOrder = {
    "objSlaveTypeId",
    "objSlaveTypeVer"
})
public class ObjectSlaveTypePlSpecific
    extends TIAObject
{

    @XmlElement(nillable = true)
    protected String objSlaveTypeId;
    @XmlElement(nillable = true)
    protected BigDecimal objSlaveTypeVer;

    /**
     * Gets the value of the objSlaveTypeId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getObjSlaveTypeId() {
        return objSlaveTypeId;
    }

    /**
     * Sets the value of the objSlaveTypeId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setObjSlaveTypeId(String value) {
        this.objSlaveTypeId = value;
    }

    /**
     * Gets the value of the objSlaveTypeVer property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getObjSlaveTypeVer() {
        return objSlaveTypeVer;
    }

    /**
     * Sets the value of the objSlaveTypeVer property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setObjSlaveTypeVer(BigDecimal value) {
        this.objSlaveTypeVer = value;
    }

}
