
package za.co.ominsure.synapse.iwyze.lob.backend.policy.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Is mapped into TIA metadata type "obj_person".
 * 
 * <p>Java class for Person complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Person">
 *   &lt;complexContent>
 *     &lt;extension base="{http://infrastructure.tia.dk/schema/party/v2/}AbstractParty">
 *       &lt;sequence>
 *         &lt;element name="title" type="{http://infrastructure.tia.dk/schema/party/v2/}title" minOccurs="0"/>
 *         &lt;element name="forename" type="{http://infrastructure.tia.dk/schema/party/v2/}longName" minOccurs="0"/>
 *         &lt;element name="civilRegCode" type="{http://infrastructure.tia.dk/schema/party/v2/}civilReg" minOccurs="0"/>
 *         &lt;element name="birthDate" type="{http://infrastructure.tia.dk/schema/common/v2/}date" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Person", namespace = "http://infrastructure.tia.dk/schema/party/v2/", propOrder = {
    "title",
    "forename",
    "civilRegCode",
    "birthDate"
})
public class Person
    extends AbstractParty
{

    @XmlElement(nillable = true)
    protected String title;
    @XmlElement(nillable = true)
    protected String forename;
    @XmlElement(nillable = true)
    protected String civilRegCode;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar birthDate;

    /**
     * Gets the value of the title property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets the value of the title property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTitle(String value) {
        this.title = value;
    }

    /**
     * Gets the value of the forename property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getForename() {
        return forename;
    }

    /**
     * Sets the value of the forename property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setForename(String value) {
        this.forename = value;
    }

    /**
     * Gets the value of the civilRegCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCivilRegCode() {
        return civilRegCode;
    }

    /**
     * Sets the value of the civilRegCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCivilRegCode(String value) {
        this.civilRegCode = value;
    }

    /**
     * Gets the value of the birthDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getBirthDate() {
        return birthDate;
    }

    /**
     * Sets the value of the birthDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setBirthDate(XMLGregorianCalendar value) {
        this.birthDate = value;
    }

}
