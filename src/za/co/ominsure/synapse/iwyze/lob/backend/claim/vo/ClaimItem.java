
package za.co.ominsure.synapse.iwyze.lob.backend.claim.vo;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Additional comments
 * 
 * <p>Java class for ClaimItem complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ClaimItem">
 *   &lt;complexContent>
 *     &lt;extension base="{http://infrastructure.tia.dk/schema/common/v2/}TIAObject">
 *       &lt;sequence>
 *         &lt;element name="itemNo" type="{http://infrastructure.tia.dk/schema/common/v2/}long10" minOccurs="0"/>
 *         &lt;element name="seqNo" type="{http://infrastructure.tia.dk/schema/common/v2/}long10" minOccurs="0"/>
 *         &lt;element name="receiverIdNo" type="{http://infrastructure.tia.dk/schema/common/v2/}long10" minOccurs="0"/>
 *         &lt;element name="objectPrice" type="{http://infrastructure.tia.dk/schema/common/v2/}amountwithoutrestriction" minOccurs="0"/>
 *         &lt;element name="objectAge" type="{http://infrastructure.tia.dk/schema/common/v2/}amountwithoutrestriction" minOccurs="0"/>
 *         &lt;element name="riskNo" type="{http://infrastructure.tia.dk/schema/common/v2/}int3" minOccurs="0"/>
 *         &lt;element name="subriskNo" type="{http://infrastructure.tia.dk/schema/common/v2/}int3" minOccurs="0"/>
 *         &lt;element name="estimateDate" type="{http://infrastructure.tia.dk/schema/common/v2/}timestamp" minOccurs="0"/>
 *         &lt;element name="newest" type="{http://infrastructure.tia.dk/schema/common/v2/}string1" minOccurs="0"/>
 *         &lt;element name="itemType" type="{http://infrastructure.tia.dk/schema/common/v2/}string3" minOccurs="0"/>
 *         &lt;element name="subitemType" type="{http://infrastructure.tia.dk/schema/common/v2/}string10" minOccurs="0"/>
 *         &lt;element name="currencyCode" type="{http://infrastructure.tia.dk/schema/common/v2/}string3" minOccurs="0"/>
 *         &lt;element name="currencyEstimate" type="{http://infrastructure.tia.dk/schema/common/v2/}amountwithoutrestriction" minOccurs="0"/>
 *         &lt;element name="estimate" type="{http://infrastructure.tia.dk/schema/common/v2/}amountwithoutrestriction" minOccurs="0"/>
 *         &lt;element name="currencyDiff" type="{http://infrastructure.tia.dk/schema/common/v2/}amountwithoutrestriction" minOccurs="0"/>
 *         &lt;element name="diff" type="{http://infrastructure.tia.dk/schema/common/v2/}amountwithoutrestriction" minOccurs="0"/>
 *         &lt;element name="exchangeRate" type="{http://infrastructure.tia.dk/schema/common/v2/}amountwithoutrestriction" minOccurs="0"/>
 *         &lt;element name="authorisedBy" type="{http://infrastructure.tia.dk/schema/common/v2/}string8" minOccurs="0"/>
 *         &lt;element name="glBatchNo" type="{http://infrastructure.tia.dk/schema/common/v2/}long10" minOccurs="0"/>
 *         &lt;element name="pay" type="{http://infrastructure.tia.dk/schema/common/v2/}string1" minOccurs="0"/>
 *         &lt;element name="role" type="{http://infrastructure.tia.dk/schema/common/v2/}string3" minOccurs="0"/>
 *         &lt;element name="suppressReinsurance" type="{http://infrastructure.tia.dk/schema/common/v2/}string1" minOccurs="0"/>
 *         &lt;element name="objectPercent" type="{http://infrastructure.tia.dk/schema/common/v2/}decimal30Point2" minOccurs="0"/>
 *         &lt;element name="itemStartDate" type="{http://infrastructure.tia.dk/schema/common/v2/}timestamp" minOccurs="0"/>
 *         &lt;element name="itemEndDate" type="{http://infrastructure.tia.dk/schema/common/v2/}timestamp" minOccurs="0"/>
 *         &lt;element name="transDate" type="{http://infrastructure.tia.dk/schema/common/v2/}timestamp" minOccurs="0"/>
 *         &lt;element name="accInterestMethod" type="{http://infrastructure.tia.dk/schema/common/v2/}long10" minOccurs="0"/>
 *         &lt;element name="documentDate" type="{http://infrastructure.tia.dk/schema/common/v2/}timestamp" minOccurs="0"/>
 *         &lt;element name="policyLineSeqNo" type="{http://infrastructure.tia.dk/schema/common/v2/}long10" minOccurs="0"/>
 *         &lt;element name="description" type="{http://infrastructure.tia.dk/schema/common/v2/}string2000" minOccurs="0"/>
 *         &lt;element name="objectNo" type="{http://infrastructure.tia.dk/schema/common/v2/}long10" minOccurs="0"/>
 *         &lt;element name="objectSeqNo" type="{http://infrastructure.tia.dk/schema/common/v2/}long10" minOccurs="0"/>
 *         &lt;element name="objectId" type="{http://infrastructure.tia.dk/schema/common/v2/}string30" minOccurs="0"/>
 *         &lt;element name="statCode1" type="{http://infrastructure.tia.dk/schema/common/v2/}string3" minOccurs="0"/>
 *         &lt;element name="statCode2" type="{http://infrastructure.tia.dk/schema/common/v2/}string3" minOccurs="0"/>
 *         &lt;element name="statCode3" type="{http://infrastructure.tia.dk/schema/common/v2/}string3" minOccurs="0"/>
 *         &lt;element name="status" type="{http://infrastructure.tia.dk/schema/common/v2/}string3" minOccurs="0"/>
 *         &lt;element name="firstOpenDate" type="{http://infrastructure.tia.dk/schema/common/v2/}timestamp" minOccurs="0"/>
 *         &lt;element name="firstCloseDate" type="{http://infrastructure.tia.dk/schema/common/v2/}timestamp" minOccurs="0"/>
 *         &lt;element name="reopenDate" type="{http://infrastructure.tia.dk/schema/common/v2/}timestamp" minOccurs="0"/>
 *         &lt;element name="reopenReason" type="{http://infrastructure.tia.dk/schema/common/v2/}string2000" minOccurs="0"/>
 *         &lt;element name="recloseDate" type="{http://infrastructure.tia.dk/schema/common/v2/}timestamp" minOccurs="0"/>
 *         &lt;element name="hoursSpend" type="{http://infrastructure.tia.dk/schema/common/v2/}amountwithoutrestriction" minOccurs="0"/>
 *         &lt;element name="workgroup" type="{http://infrastructure.tia.dk/schema/common/v2/}string10" minOccurs="0"/>
 *         &lt;element name="handler" type="{http://infrastructure.tia.dk/schema/common/v2/}string8" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ClaimItem", propOrder = {
    "itemNo",
    "seqNo",
    "receiverIdNo",
    "objectPrice",
    "objectAge",
    "riskNo",
    "subriskNo",
    "estimateDate",
    "newest",
    "itemType",
    "subitemType",
    "currencyCode",
    "currencyEstimate",
    "estimate",
    "currencyDiff",
    "diff",
    "exchangeRate",
    "authorisedBy",
    "glBatchNo",
    "pay",
    "role",
    "suppressReinsurance",
    "objectPercent",
    "itemStartDate",
    "itemEndDate",
    "transDate",
    "accInterestMethod",
    "documentDate",
    "policyLineSeqNo",
    "description",
    "objectNo",
    "objectSeqNo",
    "objectId",
    "statCode1",
    "statCode2",
    "statCode3",
    "status",
    "firstOpenDate",
    "firstCloseDate",
    "reopenDate",
    "reopenReason",
    "recloseDate",
    "hoursSpend",
    "workgroup",
    "handler"
})
public class ClaimItem
    extends TIAObject
{

    @XmlElement(nillable = true)
    protected Long itemNo;
    @XmlElement(nillable = true)
    protected Long seqNo;
    @XmlElement(nillable = true)
    protected Long receiverIdNo;
    @XmlElement(nillable = true)
    protected BigDecimal objectPrice;
    @XmlElement(nillable = true)
    protected BigDecimal objectAge;
    @XmlElement(nillable = true)
    protected Long riskNo;
    @XmlElement(nillable = true)
    protected Long subriskNo;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar estimateDate;
    @XmlElement(nillable = true)
    protected String newest;
    @XmlElement(nillable = true)
    protected String itemType;
    @XmlElement(nillable = true)
    protected String subitemType;
    @XmlElement(nillable = true)
    protected String currencyCode;
    @XmlElement(nillable = true)
    protected BigDecimal currencyEstimate;
    @XmlElement(nillable = true)
    protected BigDecimal estimate;
    @XmlElement(nillable = true)
    protected BigDecimal currencyDiff;
    @XmlElement(nillable = true)
    protected BigDecimal diff;
    @XmlElement(nillable = true)
    protected BigDecimal exchangeRate;
    @XmlElement(nillable = true)
    protected String authorisedBy;
    @XmlElement(nillable = true)
    protected Long glBatchNo;
    @XmlElement(nillable = true)
    protected String pay;
    @XmlElement(nillable = true)
    protected String role;
    @XmlElement(nillable = true)
    protected String suppressReinsurance;
    @XmlElement(nillable = true)
    protected BigDecimal objectPercent;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar itemStartDate;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar itemEndDate;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar transDate;
    @XmlElement(nillable = true)
    protected Long accInterestMethod;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar documentDate;
    @XmlElement(nillable = true)
    protected Long policyLineSeqNo;
    @XmlElement(nillable = true)
    protected String description;
    @XmlElement(nillable = true)
    protected Long objectNo;
    @XmlElement(nillable = true)
    protected Long objectSeqNo;
    @XmlElement(nillable = true)
    protected String objectId;
    @XmlElement(nillable = true)
    protected String statCode1;
    @XmlElement(nillable = true)
    protected String statCode2;
    @XmlElement(nillable = true)
    protected String statCode3;
    @XmlElement(nillable = true)
    protected String status;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar firstOpenDate;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar firstCloseDate;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar reopenDate;
    @XmlElement(nillable = true)
    protected String reopenReason;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar recloseDate;
    @XmlElement(nillable = true)
    protected BigDecimal hoursSpend;
    @XmlElement(nillable = true)
    protected String workgroup;
    @XmlElement(nillable = true)
    protected String handler;

    /**
     * Gets the value of the itemNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getItemNo() {
        return itemNo;
    }

    /**
     * Sets the value of the itemNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setItemNo(Long value) {
        this.itemNo = value;
    }

    /**
     * Gets the value of the seqNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getSeqNo() {
        return seqNo;
    }

    /**
     * Sets the value of the seqNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setSeqNo(Long value) {
        this.seqNo = value;
    }

    /**
     * Gets the value of the receiverIdNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getReceiverIdNo() {
        return receiverIdNo;
    }

    /**
     * Sets the value of the receiverIdNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setReceiverIdNo(Long value) {
        this.receiverIdNo = value;
    }

    /**
     * Gets the value of the objectPrice property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getObjectPrice() {
        return objectPrice;
    }

    /**
     * Sets the value of the objectPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setObjectPrice(BigDecimal value) {
        this.objectPrice = value;
    }

    /**
     * Gets the value of the objectAge property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getObjectAge() {
        return objectAge;
    }

    /**
     * Sets the value of the objectAge property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setObjectAge(BigDecimal value) {
        this.objectAge = value;
    }

    /**
     * Gets the value of the riskNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getRiskNo() {
        return riskNo;
    }

    /**
     * Sets the value of the riskNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setRiskNo(Long value) {
        this.riskNo = value;
    }

    /**
     * Gets the value of the subriskNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getSubriskNo() {
        return subriskNo;
    }

    /**
     * Sets the value of the subriskNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setSubriskNo(Long value) {
        this.subriskNo = value;
    }

    /**
     * Gets the value of the estimateDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEstimateDate() {
        return estimateDate;
    }

    /**
     * Sets the value of the estimateDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEstimateDate(XMLGregorianCalendar value) {
        this.estimateDate = value;
    }

    /**
     * Gets the value of the newest property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNewest() {
        return newest;
    }

    /**
     * Sets the value of the newest property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNewest(String value) {
        this.newest = value;
    }

    /**
     * Gets the value of the itemType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItemType() {
        return itemType;
    }

    /**
     * Sets the value of the itemType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItemType(String value) {
        this.itemType = value;
    }

    /**
     * Gets the value of the subitemType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubitemType() {
        return subitemType;
    }

    /**
     * Sets the value of the subitemType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubitemType(String value) {
        this.subitemType = value;
    }

    /**
     * Gets the value of the currencyCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrencyCode() {
        return currencyCode;
    }

    /**
     * Sets the value of the currencyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrencyCode(String value) {
        this.currencyCode = value;
    }

    /**
     * Gets the value of the currencyEstimate property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCurrencyEstimate() {
        return currencyEstimate;
    }

    /**
     * Sets the value of the currencyEstimate property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCurrencyEstimate(BigDecimal value) {
        this.currencyEstimate = value;
    }

    /**
     * Gets the value of the estimate property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getEstimate() {
        return estimate;
    }

    /**
     * Sets the value of the estimate property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setEstimate(BigDecimal value) {
        this.estimate = value;
    }

    /**
     * Gets the value of the currencyDiff property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCurrencyDiff() {
        return currencyDiff;
    }

    /**
     * Sets the value of the currencyDiff property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCurrencyDiff(BigDecimal value) {
        this.currencyDiff = value;
    }

    /**
     * Gets the value of the diff property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDiff() {
        return diff;
    }

    /**
     * Sets the value of the diff property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDiff(BigDecimal value) {
        this.diff = value;
    }

    /**
     * Gets the value of the exchangeRate property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getExchangeRate() {
        return exchangeRate;
    }

    /**
     * Sets the value of the exchangeRate property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setExchangeRate(BigDecimal value) {
        this.exchangeRate = value;
    }

    /**
     * Gets the value of the authorisedBy property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAuthorisedBy() {
        return authorisedBy;
    }

    /**
     * Sets the value of the authorisedBy property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAuthorisedBy(String value) {
        this.authorisedBy = value;
    }

    /**
     * Gets the value of the glBatchNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getGlBatchNo() {
        return glBatchNo;
    }

    /**
     * Sets the value of the glBatchNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setGlBatchNo(Long value) {
        this.glBatchNo = value;
    }

    /**
     * Gets the value of the pay property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPay() {
        return pay;
    }

    /**
     * Sets the value of the pay property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPay(String value) {
        this.pay = value;
    }

    /**
     * Gets the value of the role property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRole() {
        return role;
    }

    /**
     * Sets the value of the role property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRole(String value) {
        this.role = value;
    }

    /**
     * Gets the value of the suppressReinsurance property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSuppressReinsurance() {
        return suppressReinsurance;
    }

    /**
     * Sets the value of the suppressReinsurance property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSuppressReinsurance(String value) {
        this.suppressReinsurance = value;
    }

    /**
     * Gets the value of the objectPercent property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getObjectPercent() {
        return objectPercent;
    }

    /**
     * Sets the value of the objectPercent property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setObjectPercent(BigDecimal value) {
        this.objectPercent = value;
    }

    /**
     * Gets the value of the itemStartDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getItemStartDate() {
        return itemStartDate;
    }

    /**
     * Sets the value of the itemStartDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setItemStartDate(XMLGregorianCalendar value) {
        this.itemStartDate = value;
    }

    /**
     * Gets the value of the itemEndDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getItemEndDate() {
        return itemEndDate;
    }

    /**
     * Sets the value of the itemEndDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setItemEndDate(XMLGregorianCalendar value) {
        this.itemEndDate = value;
    }

    /**
     * Gets the value of the transDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTransDate() {
        return transDate;
    }

    /**
     * Sets the value of the transDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTransDate(XMLGregorianCalendar value) {
        this.transDate = value;
    }

    /**
     * Gets the value of the accInterestMethod property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getAccInterestMethod() {
        return accInterestMethod;
    }

    /**
     * Sets the value of the accInterestMethod property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setAccInterestMethod(Long value) {
        this.accInterestMethod = value;
    }

    /**
     * Gets the value of the documentDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDocumentDate() {
        return documentDate;
    }

    /**
     * Sets the value of the documentDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDocumentDate(XMLGregorianCalendar value) {
        this.documentDate = value;
    }

    /**
     * Gets the value of the policyLineSeqNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getPolicyLineSeqNo() {
        return policyLineSeqNo;
    }

    /**
     * Sets the value of the policyLineSeqNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setPolicyLineSeqNo(Long value) {
        this.policyLineSeqNo = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the objectNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getObjectNo() {
        return objectNo;
    }

    /**
     * Sets the value of the objectNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setObjectNo(Long value) {
        this.objectNo = value;
    }

    /**
     * Gets the value of the objectSeqNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getObjectSeqNo() {
        return objectSeqNo;
    }

    /**
     * Sets the value of the objectSeqNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setObjectSeqNo(Long value) {
        this.objectSeqNo = value;
    }

    /**
     * Gets the value of the objectId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getObjectId() {
        return objectId;
    }

    /**
     * Sets the value of the objectId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setObjectId(String value) {
        this.objectId = value;
    }

    /**
     * Gets the value of the statCode1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatCode1() {
        return statCode1;
    }

    /**
     * Sets the value of the statCode1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatCode1(String value) {
        this.statCode1 = value;
    }

    /**
     * Gets the value of the statCode2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatCode2() {
        return statCode2;
    }

    /**
     * Sets the value of the statCode2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatCode2(String value) {
        this.statCode2 = value;
    }

    /**
     * Gets the value of the statCode3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatCode3() {
        return statCode3;
    }

    /**
     * Sets the value of the statCode3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatCode3(String value) {
        this.statCode3 = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

    /**
     * Gets the value of the firstOpenDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFirstOpenDate() {
        return firstOpenDate;
    }

    /**
     * Sets the value of the firstOpenDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFirstOpenDate(XMLGregorianCalendar value) {
        this.firstOpenDate = value;
    }

    /**
     * Gets the value of the firstCloseDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFirstCloseDate() {
        return firstCloseDate;
    }

    /**
     * Sets the value of the firstCloseDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFirstCloseDate(XMLGregorianCalendar value) {
        this.firstCloseDate = value;
    }

    /**
     * Gets the value of the reopenDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getReopenDate() {
        return reopenDate;
    }

    /**
     * Sets the value of the reopenDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setReopenDate(XMLGregorianCalendar value) {
        this.reopenDate = value;
    }

    /**
     * Gets the value of the reopenReason property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReopenReason() {
        return reopenReason;
    }

    /**
     * Sets the value of the reopenReason property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReopenReason(String value) {
        this.reopenReason = value;
    }

    /**
     * Gets the value of the recloseDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getRecloseDate() {
        return recloseDate;
    }

    /**
     * Sets the value of the recloseDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setRecloseDate(XMLGregorianCalendar value) {
        this.recloseDate = value;
    }

    /**
     * Gets the value of the hoursSpend property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getHoursSpend() {
        return hoursSpend;
    }

    /**
     * Sets the value of the hoursSpend property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setHoursSpend(BigDecimal value) {
        this.hoursSpend = value;
    }

    /**
     * Gets the value of the workgroup property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWorkgroup() {
        return workgroup;
    }

    /**
     * Sets the value of the workgroup property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWorkgroup(String value) {
        this.workgroup = value;
    }

    /**
     * Gets the value of the handler property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHandler() {
        return handler;
    }

    /**
     * Sets the value of the handler property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHandler(String value) {
        this.handler = value;
    }

}
