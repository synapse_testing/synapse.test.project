
package za.co.ominsure.synapse.iwyze.lob.backend.policy.vo;

import java.math.BigDecimal;
import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Is mapped into xxxx".
 * 
 * <p>Java class for Clause complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Clause">
 *   &lt;complexContent>
 *     &lt;extension base="{http://infrastructure.tia.dk/schema/common/v2/}TIAObject">
 *       &lt;sequence>
 *         &lt;element name="clauseNo" type="{http://infrastructure.tia.dk/schema/policy/v2/}seqNo" minOccurs="0"/>
 *         &lt;element name="seqNo" type="{http://infrastructure.tia.dk/schema/policy/v2/}seqNo" minOccurs="0"/>
 *         &lt;element name="clauseId" type="{http://infrastructure.tia.dk/schema/policy/v2/}clauseId" minOccurs="0"/>
 *         &lt;element name="optional" type="{http://infrastructure.tia.dk/schema/common/v2/}string1" minOccurs="0"/>
 *         &lt;element name="cancelCode" type="{http://infrastructure.tia.dk/schema/policy/v2/}cancelCode" minOccurs="0"/>
 *         &lt;element name="newest" type="{http://infrastructure.tia.dk/schema/policy/v2/}newest" minOccurs="0"/>
 *         &lt;element name="coverStartDate" type="{http://infrastructure.tia.dk/schema/common/v2/}date" minOccurs="0"/>
 *         &lt;element name="coverEndDate" type="{http://infrastructure.tia.dk/schema/common/v2/}date" minOccurs="0"/>
 *         &lt;element name="preSeqNo" type="{http://infrastructure.tia.dk/schema/policy/v2/}seqNo" minOccurs="0"/>
 *         &lt;element name="sucSeqNo" type="{http://infrastructure.tia.dk/schema/policy/v2/}seqNo" minOccurs="0"/>
 *         &lt;element name="transId" type="{http://infrastructure.tia.dk/schema/policy/v2/}seqNo" minOccurs="0"/>
 *         &lt;element name="parameterCollection" type="{http://infrastructure.tia.dk/schema/common/v2/}KeyPairCollection" minOccurs="0"/>
 *         &lt;element name="creation" type="{http://infrastructure.tia.dk/schema/policy/v2/}creation" minOccurs="0"/>
 *         &lt;element name="sortNo" type="{http://infrastructure.tia.dk/schema/policy/v2/}sortNo" minOccurs="0"/>
 *         &lt;element name="seqNoQop" type="{http://infrastructure.tia.dk/schema/common/v2/}long10" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Clause", propOrder = {
    "clauseNo",
    "seqNo",
    "clauseId",
    "optional",
    "cancelCode",
    "newest",
    "coverStartDate",
    "coverEndDate",
    "preSeqNo",
    "sucSeqNo",
    "transId",
    "parameterCollection",
    "creation",
    "sortNo",
    "seqNoQop"
})
@XmlSeeAlso({
    Clause.class
})
public class Clause2
    extends TIAObject
{

    @XmlElement(nillable = true)
    @XmlSchemaType(name = "unsignedLong")
    protected BigInteger clauseNo;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "unsignedLong")
    protected BigInteger seqNo;
    @XmlSchemaType(name = "unsignedLong")
    protected BigInteger clauseId;
    protected String optional;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "unsignedInt")
    protected Long cancelCode;
    @XmlElement(nillable = true)
    protected String newest;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar coverStartDate;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar coverEndDate;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "unsignedLong")
    protected BigInteger preSeqNo;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "unsignedLong")
    protected BigInteger sucSeqNo;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "unsignedLong")
    protected BigInteger transId;
    protected KeyPairCollection parameterCollection;
    protected BigDecimal creation;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "unsignedInt")
    protected Long sortNo;
    @XmlElement(nillable = true)
    protected Long seqNoQop;

    /**
     * Gets the value of the clauseNo property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getClauseNo() {
        return clauseNo;
    }

    /**
     * Sets the value of the clauseNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setClauseNo(BigInteger value) {
        this.clauseNo = value;
    }

    /**
     * Gets the value of the seqNo property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getSeqNo() {
        return seqNo;
    }

    /**
     * Sets the value of the seqNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setSeqNo(BigInteger value) {
        this.seqNo = value;
    }

    /**
     * Gets the value of the clauseId property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getClauseId() {
        return clauseId;
    }

    /**
     * Sets the value of the clauseId property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setClauseId(BigInteger value) {
        this.clauseId = value;
    }

    /**
     * Gets the value of the optional property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOptional() {
        return optional;
    }

    /**
     * Sets the value of the optional property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOptional(String value) {
        this.optional = value;
    }

    /**
     * Gets the value of the cancelCode property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getCancelCode() {
        return cancelCode;
    }

    /**
     * Sets the value of the cancelCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setCancelCode(Long value) {
        this.cancelCode = value;
    }

    /**
     * Gets the value of the newest property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNewest() {
        return newest;
    }

    /**
     * Sets the value of the newest property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNewest(String value) {
        this.newest = value;
    }

    /**
     * Gets the value of the coverStartDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCoverStartDate() {
        return coverStartDate;
    }

    /**
     * Sets the value of the coverStartDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCoverStartDate(XMLGregorianCalendar value) {
        this.coverStartDate = value;
    }

    /**
     * Gets the value of the coverEndDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCoverEndDate() {
        return coverEndDate;
    }

    /**
     * Sets the value of the coverEndDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCoverEndDate(XMLGregorianCalendar value) {
        this.coverEndDate = value;
    }

    /**
     * Gets the value of the preSeqNo property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getPreSeqNo() {
        return preSeqNo;
    }

    /**
     * Sets the value of the preSeqNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setPreSeqNo(BigInteger value) {
        this.preSeqNo = value;
    }

    /**
     * Gets the value of the sucSeqNo property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getSucSeqNo() {
        return sucSeqNo;
    }

    /**
     * Sets the value of the sucSeqNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setSucSeqNo(BigInteger value) {
        this.sucSeqNo = value;
    }

    /**
     * Gets the value of the transId property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getTransId() {
        return transId;
    }

    /**
     * Sets the value of the transId property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setTransId(BigInteger value) {
        this.transId = value;
    }

    /**
     * Gets the value of the parameterCollection property.
     * 
     * @return
     *     possible object is
     *     {@link KeyPairCollection }
     *     
     */
    public KeyPairCollection getParameterCollection() {
        return parameterCollection;
    }

    /**
     * Sets the value of the parameterCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link KeyPairCollection }
     *     
     */
    public void setParameterCollection(KeyPairCollection value) {
        this.parameterCollection = value;
    }

    /**
     * Gets the value of the creation property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCreation() {
        return creation;
    }

    /**
     * Sets the value of the creation property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCreation(BigDecimal value) {
        this.creation = value;
    }

    /**
     * Gets the value of the sortNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getSortNo() {
        return sortNo;
    }

    /**
     * Sets the value of the sortNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setSortNo(Long value) {
        this.sortNo = value;
    }

    /**
     * Gets the value of the seqNoQop property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getSeqNoQop() {
        return seqNoQop;
    }

    /**
     * Sets the value of the seqNoQop property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setSeqNoQop(Long value) {
        this.seqNoQop = value;
    }

}
