
package za.co.ominsure.synapse.iwyze.lob.backend.policy.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="inputToken" type="{http://infrastructure.tia.dk/schema/common/v2/}InputToken"/>
 *         &lt;element name="policy" type="{http://infrastructure.tia.dk/schema/policy/v4/}Policy"/>
 *         &lt;element name="getDepth" type="{http://infrastructure.tia.dk/schema/policy/v2/}getDepth" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "inputToken",
    "policy",
    "getDepth"
})
@XmlRootElement(name = "createPolicyRequest", namespace = "http://infrastructure.tia.dk/schema/policy/v4/")
public class CreatePolicyRequest {

    @XmlElement(namespace = "http://infrastructure.tia.dk/schema/policy/v4/", required = true)
    protected InputToken inputToken;
    @XmlElement(namespace = "http://infrastructure.tia.dk/schema/policy/v4/", required = true)
    protected Policy policy;
    @XmlElement(namespace = "http://infrastructure.tia.dk/schema/policy/v4/")
    @XmlSchemaType(name = "string")
    protected GetDepth getDepth;

    /**
     * Gets the value of the inputToken property.
     * 
     * @return
     *     possible object is
     *     {@link InputToken }
     *     
     */
    public InputToken getInputToken() {
        return inputToken;
    }

    /**
     * Sets the value of the inputToken property.
     * 
     * @param value
     *     allowed object is
     *     {@link InputToken }
     *     
     */
    public void setInputToken(InputToken value) {
        this.inputToken = value;
    }

    /**
     * Gets the value of the policy property.
     * 
     * @return
     *     possible object is
     *     {@link Policy }
     *     
     */
    public Policy getPolicy() {
        return policy;
    }

    /**
     * Sets the value of the policy property.
     * 
     * @param value
     *     allowed object is
     *     {@link Policy }
     *     
     */
    public void setPolicy(Policy value) {
        this.policy = value;
    }

    /**
     * Gets the value of the getDepth property.
     * 
     * @return
     *     possible object is
     *     {@link GetDepth }
     *     
     */
    public GetDepth getGetDepth() {
        return getDepth;
    }

    /**
     * Sets the value of the getDepth property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetDepth }
     *     
     */
    public void setGetDepth(GetDepth value) {
        this.getDepth = value;
    }

}
