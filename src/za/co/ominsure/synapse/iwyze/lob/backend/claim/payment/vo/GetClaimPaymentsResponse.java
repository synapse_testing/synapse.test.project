
package za.co.ominsure.synapse.iwyze.lob.backend.claim.payment.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="claimAccItemCollection" type="{http://infrastructure.tia.dk/schema/claim/v2/}ClaimAccItemCollection"/>
 *         &lt;element name="result" type="{http://infrastructure.tia.dk/schema/common/v2/}Result"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "claimAccItemCollection", "result" })
@XmlRootElement(name = "getClaimPaymentsResponse")
public class GetClaimPaymentsResponse {

	@XmlElement(required = true)
	protected ClaimAccItemCollection claimAccItemCollection;
	@XmlElement(required = true)
	protected Result result;

	/**
	 * Gets the value of the claimAccItemCollection property.
	 * 
	 * @return possible object is {@link ClaimAccItemCollection }
	 * 
	 */
	public ClaimAccItemCollection getClaimAccItemCollection() {
		if (claimAccItemCollection == null)
			claimAccItemCollection = new ClaimAccItemCollection();
		return claimAccItemCollection;
	}

	/**
	 * Sets the value of the claimAccItemCollection property.
	 * 
	 * @param value
	 *            allowed object is {@link ClaimAccItemCollection }
	 * 
	 */
	public void setClaimAccItemCollection(ClaimAccItemCollection value) {
		this.claimAccItemCollection = value;
	}

	/**
	 * Gets the value of the result property.
	 * 
	 * @return possible object is {@link Result }
	 * 
	 */
	public Result getResult() {
		return result;
	}

	/**
	 * Sets the value of the result property.
	 * 
	 * @param value
	 *            allowed object is {@link Result }
	 * 
	 */
	public void setResult(Result value) {
		this.result = value;
	}

}
