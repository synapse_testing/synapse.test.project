
package za.co.ominsure.synapse.iwyze.lob.backend.policy.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Additional comments
 * 
 * <p>Java class for ProductLine complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ProductLine">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="productLineId" type="{http://infrastructure.tia.dk/schema/common/v2/}string5" minOccurs="0"/>
 *         &lt;element name="productLineNameTranslations" type="{http://infrastructure.tia.dk/schema/common/v2/}ValueTranslationPolicyCollection" minOccurs="0"/>
 *         &lt;element name="productLineVersionCollection" type="{http://infrastructure.tia.dk/schema/policy/v2/}ProductLineVersionCollection" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProductLine", propOrder = {
    "productLineId",
    "productLineNameTranslations",
    "productLineVersionCollection"
})
public class ProductLine {

    @XmlElement(nillable = true)
    protected String productLineId;
    protected ValueTranslationPolicyCollection productLineNameTranslations;
    protected ProductLineVersionCollection productLineVersionCollection;

    /**
     * Gets the value of the productLineId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductLineId() {
        return productLineId;
    }

    /**
     * Sets the value of the productLineId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductLineId(String value) {
        this.productLineId = value;
    }

    /**
     * Gets the value of the productLineNameTranslations property.
     * 
     * @return
     *     possible object is
     *     {@link ValueTranslationPolicyCollection }
     *     
     */
    public ValueTranslationPolicyCollection getProductLineNameTranslations() {
        return productLineNameTranslations;
    }

    /**
     * Sets the value of the productLineNameTranslations property.
     * 
     * @param value
     *     allowed object is
     *     {@link ValueTranslationPolicyCollection }
     *     
     */
    public void setProductLineNameTranslations(ValueTranslationPolicyCollection value) {
        this.productLineNameTranslations = value;
    }

    /**
     * Gets the value of the productLineVersionCollection property.
     * 
     * @return
     *     possible object is
     *     {@link ProductLineVersionCollection }
     *     
     */
    public ProductLineVersionCollection getProductLineVersionCollection() {
        return productLineVersionCollection;
    }

    /**
     * Sets the value of the productLineVersionCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProductLineVersionCollection }
     *     
     */
    public void setProductLineVersionCollection(ProductLineVersionCollection value) {
        this.productLineVersionCollection = value;
    }

}
