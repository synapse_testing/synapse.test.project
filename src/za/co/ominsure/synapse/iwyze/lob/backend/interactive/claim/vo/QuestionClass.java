
package za.co.ominsure.synapse.iwyze.lob.backend.interactive.claim.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Additional comments
 * 
 * <p>Java class for QuestionClass complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="QuestionClass">
 *   &lt;complexContent>
 *     &lt;extension base="{http://infrastructure.tia.dk/schema/common/v2/}TIAObject">
 *       &lt;sequence>
 *         &lt;element name="questionClass" type="{http://infrastructure.tia.dk/schema/common/v2/}string10" minOccurs="0"/>
 *         &lt;element name="questionCollection" type="{http://infrastructure.tia.dk/schema/claim/v2/}QuestionCollection" minOccurs="0"/>
 *         &lt;element name="valueTranslationCollection" type="{http://infrastructure.tia.dk/schema/common/v2/}ValueTranslationCollection" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QuestionClass", propOrder = {
    "questionClass",
    "questionCollection",
    "valueTranslationCollection"
})
public class QuestionClass
    extends TIAObject
{

    @XmlElement(nillable = true)
    protected String questionClass;
    protected QuestionCollection questionCollection;
    protected ValueTranslationCollection2 valueTranslationCollection;

    /**
     * Gets the value of the questionClass property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQuestionClass() {
        return questionClass;
    }

    /**
     * Sets the value of the questionClass property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQuestionClass(String value) {
        this.questionClass = value;
    }

    /**
     * Gets the value of the questionCollection property.
     * 
     * @return
     *     possible object is
     *     {@link QuestionCollection }
     *     
     */
    public QuestionCollection getQuestionCollection() {
        return questionCollection;
    }

    /**
     * Sets the value of the questionCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link QuestionCollection }
     *     
     */
    public void setQuestionCollection(QuestionCollection value) {
        this.questionCollection = value;
    }

    /**
     * Gets the value of the valueTranslationCollection property.
     * 
     * @return
     *     possible object is
     *     {@link ValueTranslationCollection2 }
     *     
     */
    public ValueTranslationCollection2 getValueTranslationCollection() {
        return valueTranslationCollection;
    }

    /**
     * Sets the value of the valueTranslationCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link ValueTranslationCollection2 }
     *     
     */
    public void setValueTranslationCollection(ValueTranslationCollection2 value) {
        this.valueTranslationCollection = value;
    }

}
