
package za.co.ominsure.synapse.iwyze.lob.backend.policy.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for UIConfigRiskItem complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="UIConfigRiskItem">
 *   &lt;complexContent>
 *     &lt;extension base="{http://infrastructure.tia.dk/schema/common/v2/}TIAObject">
 *       &lt;sequence>
 *         &lt;element name="columnName" type="{http://infrastructure.tia.dk/schema/common/v2/}string30" minOccurs="0"/>
 *         &lt;element name="enabled" type="{http://infrastructure.tia.dk/schema/common/v2/}tiaYN" minOccurs="0"/>
 *         &lt;element name="defaultValueType" type="{http://infrastructure.tia.dk/schema/common/v2/}string10" minOccurs="0"/>
 *         &lt;element name="defaultValue" type="{http://infrastructure.tia.dk/schema/common/v2/}string2000" minOccurs="0"/>
 *         &lt;element name="lovReference" type="{http://infrastructure.tia.dk/schema/common/v2/}string30" minOccurs="0"/>
 *         &lt;element name="referenceName" type="{http://infrastructure.tia.dk/schema/common/v2/}string30" minOccurs="0"/>
 *         &lt;element name="mandatory" type="{http://infrastructure.tia.dk/schema/common/v2/}tiaYN" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UIConfigRiskItem", namespace = "http://infrastructure.tia.dk/schema/policy/v4/", propOrder = {
    "columnName",
    "enabled",
    "defaultValueType",
    "defaultValue",
    "lovReference",
    "referenceName",
    "mandatory"
})
public class UIConfigRiskItem
    extends TIAObject
{

    @XmlElement(nillable = true)
    protected String columnName;
    @XmlElement(nillable = true)
    protected String enabled;
    @XmlElement(nillable = true)
    protected String defaultValueType;
    @XmlElement(nillable = true)
    protected String defaultValue;
    @XmlElement(nillable = true)
    protected String lovReference;
    @XmlElement(nillable = true)
    protected String referenceName;
    @XmlElement(nillable = true)
    protected String mandatory;

    /**
     * Gets the value of the columnName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getColumnName() {
        return columnName;
    }

    /**
     * Sets the value of the columnName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setColumnName(String value) {
        this.columnName = value;
    }

    /**
     * Gets the value of the enabled property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEnabled() {
        return enabled;
    }

    /**
     * Sets the value of the enabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEnabled(String value) {
        this.enabled = value;
    }

    /**
     * Gets the value of the defaultValueType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefaultValueType() {
        return defaultValueType;
    }

    /**
     * Sets the value of the defaultValueType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefaultValueType(String value) {
        this.defaultValueType = value;
    }

    /**
     * Gets the value of the defaultValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefaultValue() {
        return defaultValue;
    }

    /**
     * Sets the value of the defaultValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefaultValue(String value) {
        this.defaultValue = value;
    }

    /**
     * Gets the value of the lovReference property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLovReference() {
        return lovReference;
    }

    /**
     * Sets the value of the lovReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLovReference(String value) {
        this.lovReference = value;
    }

    /**
     * Gets the value of the referenceName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReferenceName() {
        return referenceName;
    }

    /**
     * Sets the value of the referenceName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReferenceName(String value) {
        this.referenceName = value;
    }

    /**
     * Gets the value of the mandatory property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMandatory() {
        return mandatory;
    }

    /**
     * Sets the value of the mandatory property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMandatory(String value) {
        this.mandatory = value;
    }

}
