
package za.co.ominsure.synapse.iwyze.lob.backend.policy.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Additional comments
 * 
 * <p>Java class for ClauseType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ClauseType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="clauseTypeId" type="{http://infrastructure.tia.dk/schema/common/v2/}long10" minOccurs="0"/>
 *         &lt;element name="clauseTypeVersionCollection" type="{http://infrastructure.tia.dk/schema/policy/v2/}ClauseTypeVersionCollection" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ClauseType", propOrder = {
    "clauseTypeId",
    "clauseTypeVersionCollection"
})
public class ClauseType {

    @XmlElement(nillable = true)
    protected Long clauseTypeId;
    protected ClauseTypeVersionCollection clauseTypeVersionCollection;

    /**
     * Gets the value of the clauseTypeId property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getClauseTypeId() {
        return clauseTypeId;
    }

    /**
     * Sets the value of the clauseTypeId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setClauseTypeId(Long value) {
        this.clauseTypeId = value;
    }

    /**
     * Gets the value of the clauseTypeVersionCollection property.
     * 
     * @return
     *     possible object is
     *     {@link ClauseTypeVersionCollection }
     *     
     */
    public ClauseTypeVersionCollection getClauseTypeVersionCollection() {
        return clauseTypeVersionCollection;
    }

    /**
     * Sets the value of the clauseTypeVersionCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link ClauseTypeVersionCollection }
     *     
     */
    public void setClauseTypeVersionCollection(ClauseTypeVersionCollection value) {
        this.clauseTypeVersionCollection = value;
    }

}
