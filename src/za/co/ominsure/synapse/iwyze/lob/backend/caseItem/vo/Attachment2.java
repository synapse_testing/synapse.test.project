
package za.co.ominsure.synapse.iwyze.lob.backend.caseItem.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Attachment complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Attachment">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element name="caseItemAttBlob" type="{http://infrastructure.tia.dk/schema/case/v2/}CaseItemAttBlob" minOccurs="0"/>
 *         &lt;element name="caseItemAttClob" type="{http://infrastructure.tia.dk/schema/case/v2/}CaseItemAttClob" minOccurs="0"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Attachment", namespace = "http://infrastructure.tia.dk/schema/case/v2/", propOrder = {
    "caseItemAttClob",
    "caseItemAttBlob"
})
public class Attachment2 {

    protected CaseItemAttClob2 caseItemAttClob;
    protected CaseItemAttBlob2 caseItemAttBlob;

    /**
     * Gets the value of the caseItemAttClob property.
     * 
     * @return
     *     possible object is
     *     {@link CaseItemAttClob2 }
     *     
     */
    public CaseItemAttClob2 getCaseItemAttClob() {
        return caseItemAttClob;
    }

    /**
     * Sets the value of the caseItemAttClob property.
     * 
     * @param value
     *     allowed object is
     *     {@link CaseItemAttClob2 }
     *     
     */
    public void setCaseItemAttClob(CaseItemAttClob2 value) {
        this.caseItemAttClob = value;
    }

    /**
     * Gets the value of the caseItemAttBlob property.
     * 
     * @return
     *     possible object is
     *     {@link CaseItemAttBlob2 }
     *     
     */
    public CaseItemAttBlob2 getCaseItemAttBlob() {
        return caseItemAttBlob;
    }

    /**
     * Sets the value of the caseItemAttBlob property.
     * 
     * @param value
     *     allowed object is
     *     {@link CaseItemAttBlob2 }
     *     
     */
    public void setCaseItemAttBlob(CaseItemAttBlob2 value) {
        this.caseItemAttBlob = value;
    }

}
