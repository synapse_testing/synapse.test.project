
package za.co.ominsure.synapse.iwyze.lob.backend.interactive.claim.vo;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Definition for type tab_varchar2
 * 
 * <p>Java class for StringCollection complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="StringCollection">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="stringLine" type="{http://infrastructure.tia.dk/schema/common/v2/}stringLine" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "StringCollection", namespace = "http://infrastructure.tia.dk/schema/common/v2/", propOrder = {
    "stringLines"
})
public class StringCollection {

    @XmlElement(name = "stringLine")
    protected List<String> stringLines;

    /**
     * Gets the value of the stringLines property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the stringLines property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getStringLines().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getStringLines() {
        if (stringLines == null) {
            stringLines = new ArrayList<String>();
        }
        return this.stringLines;
    }

}
