
package za.co.ominsure.synapse.iwyze.lob.backend.interactive.claim.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="subclaim" type="{http://infrastructure.tia.dk/schema/claim/v2/}Subclaim"/>
 *         &lt;element name="result" type="{http://infrastructure.tia.dk/schema/common/v2/}Result"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "subclaim",
    "result"
})
@XmlRootElement(name = "addSubClaimResponse")
public class AddSubClaimResponse {

    @XmlElement(required = true, nillable = true)
    protected Subclaim subclaim;
    @XmlElement(required = true)
    protected Result result;

    /**
     * Gets the value of the subclaim property.
     * 
     * @return
     *     possible object is
     *     {@link Subclaim }
     *     
     */
    public Subclaim getSubclaim() {
        return subclaim;
    }

    /**
     * Sets the value of the subclaim property.
     * 
     * @param value
     *     allowed object is
     *     {@link Subclaim }
     *     
     */
    public void setSubclaim(Subclaim value) {
        this.subclaim = value;
    }

    /**
     * Gets the value of the result property.
     * 
     * @return
     *     possible object is
     *     {@link Result }
     *     
     */
    public Result getResult() {
        return result;
    }

    /**
     * Sets the value of the result property.
     * 
     * @param value
     *     allowed object is
     *     {@link Result }
     *     
     */
    public void setResult(Result value) {
        this.result = value;
    }

}
