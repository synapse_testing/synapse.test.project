
package za.co.ominsure.synapse.iwyze.lob.backend.policy.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Additional comments
 * 
 * <p>Java class for ClauseTypeVersion complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ClauseTypeVersion">
 *   &lt;complexContent>
 *     &lt;extension base="{http://infrastructure.tia.dk/schema/common/v2/}TIAObject">
 *       &lt;sequence>
 *         &lt;element name="clauseTypeVerNo" type="{http://infrastructure.tia.dk/schema/common/v2/}long10" minOccurs="0"/>
 *         &lt;element name="title" type="{http://infrastructure.tia.dk/schema/common/v2/}string60" minOccurs="0"/>
 *         &lt;element name="guidance" type="{http://infrastructure.tia.dk/schema/common/v2/}string4000" minOccurs="0"/>
 *         &lt;element name="clauseText" type="{http://infrastructure.tia.dk/schema/common/v2/}string4000" minOccurs="0"/>
 *         &lt;element name="language" type="{http://infrastructure.tia.dk/schema/common/v2/}string3" minOccurs="0"/>
 *         &lt;element name="clauseType" type="{http://infrastructure.tia.dk/schema/common/v2/}string60" minOccurs="0"/>
 *         &lt;element name="versionNote" type="{http://infrastructure.tia.dk/schema/common/v2/}string2000" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ClauseTypeVersion", propOrder = {
    "clauseTypeVerNo",
    "title",
    "guidance",
    "clauseText",
    "language",
    "clauseType",
    "versionNote"
})
public class ClauseTypeVersion
    extends TIAObject
{

    @XmlElement(nillable = true)
    protected Long clauseTypeVerNo;
    @XmlElement(nillable = true)
    protected String title;
    @XmlElement(nillable = true)
    protected String guidance;
    @XmlElement(nillable = true)
    protected String clauseText;
    @XmlElement(nillable = true)
    protected String language;
    @XmlElement(nillable = true)
    protected String clauseType;
    @XmlElement(nillable = true)
    protected String versionNote;

    /**
     * Gets the value of the clauseTypeVerNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getClauseTypeVerNo() {
        return clauseTypeVerNo;
    }

    /**
     * Sets the value of the clauseTypeVerNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setClauseTypeVerNo(Long value) {
        this.clauseTypeVerNo = value;
    }

    /**
     * Gets the value of the title property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets the value of the title property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTitle(String value) {
        this.title = value;
    }

    /**
     * Gets the value of the guidance property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGuidance() {
        return guidance;
    }

    /**
     * Sets the value of the guidance property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGuidance(String value) {
        this.guidance = value;
    }

    /**
     * Gets the value of the clauseText property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClauseText() {
        return clauseText;
    }

    /**
     * Sets the value of the clauseText property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClauseText(String value) {
        this.clauseText = value;
    }

    /**
     * Gets the value of the language property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLanguage() {
        return language;
    }

    /**
     * Sets the value of the language property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLanguage(String value) {
        this.language = value;
    }

    /**
     * Gets the value of the clauseType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClauseType() {
        return clauseType;
    }

    /**
     * Sets the value of the clauseType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClauseType(String value) {
        this.clauseType = value;
    }

    /**
     * Gets the value of the versionNote property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVersionNote() {
        return versionNote;
    }

    /**
     * Sets the value of the versionNote property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVersionNote(String value) {
        this.versionNote = value;
    }

}
