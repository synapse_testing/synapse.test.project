
package za.co.ominsure.synapse.iwyze.lob.backend.policy.vo;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TariffBreakdownCollection complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TariffBreakdownCollection">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="tariffBreakdown" type="{http://infrastructure.tia.dk/schema/policy/v4/}TariffBreakdown" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TariffBreakdownCollection", namespace = "http://infrastructure.tia.dk/schema/policy/v4/", propOrder = {
    "tariffBreakdowns"
})
public class TariffBreakdownCollection {

    @XmlElement(name = "tariffBreakdown")
    protected List<TariffBreakdown> tariffBreakdowns;

    /**
     * Gets the value of the tariffBreakdowns property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the tariffBreakdowns property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTariffBreakdowns().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TariffBreakdown }
     * 
     * 
     */
    public List<TariffBreakdown> getTariffBreakdowns() {
        if (tariffBreakdowns == null) {
            tariffBreakdowns = new ArrayList<TariffBreakdown>();
        }
        return this.tariffBreakdowns;
    }

}
