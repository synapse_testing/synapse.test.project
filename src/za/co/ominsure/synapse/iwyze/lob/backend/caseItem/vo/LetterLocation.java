
package za.co.ominsure.synapse.iwyze.lob.backend.caseItem.vo;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for letterLocation.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="letterLocation">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="DELETED"/>
 *     &lt;enumeration value="DISK"/>
 *     &lt;enumeration value="NONE"/>
 *     &lt;enumeration value="NOT STORED"/>
 *     &lt;enumeration value="TAPE"/>
 *     &lt;enumeration value="NOTES"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "letterLocation", namespace = "http://infrastructure.tia.dk/schema/case/v2/")
@XmlEnum
public enum LetterLocation {

    DELETED("DELETED"),
    DISK("DISK"),
    NONE("NONE"),
    @XmlEnumValue("NOT STORED")
    NOT_STORED("NOT STORED"),
    TAPE("TAPE"),
    NOTES("NOTES");
    private final String value;

    LetterLocation(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static LetterLocation fromValue(String v) {
        for (LetterLocation c: LetterLocation.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
