
package za.co.ominsure.synapse.iwyze.lob.backend.policy.vo;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PremiumRiskSplitCollection complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PremiumRiskSplitCollection">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="premiumRiskSplit" type="{http://infrastructure.tia.dk/schema/policy/v3/}PremiumRiskSplit" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PremiumRiskSplitCollection", namespace = "http://infrastructure.tia.dk/schema/policy/v3/", propOrder = {
    "premiumRiskSplits"
})
public class PremiumRiskSplitCollection {

    @XmlElement(name = "premiumRiskSplit")
    protected List<PremiumRiskSplit> premiumRiskSplits;

    /**
     * Gets the value of the premiumRiskSplits property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the premiumRiskSplits property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPremiumRiskSplits().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PremiumRiskSplit }
     * 
     * 
     */
    public List<PremiumRiskSplit> getPremiumRiskSplits() {
        if (premiumRiskSplits == null) {
            premiumRiskSplits = new ArrayList<PremiumRiskSplit>();
        }
        return this.premiumRiskSplits;
    }

}
