
package za.co.ominsure.synapse.iwyze.lob.backend.policy.vo;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Additional comments
 * 
 * <p>Java class for CommissionItem complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CommissionItem">
 *   &lt;complexContent>
 *     &lt;extension base="{http://infrastructure.tia.dk/schema/common/v2/}TIAObject">
 *       &lt;sequence>
 *         &lt;element name="transactionDate" type="{http://infrastructure.tia.dk/schema/common/v2/}date" minOccurs="0"/>
 *         &lt;element name="comTransCode" type="{http://infrastructure.tia.dk/schema/common/v2/}int3" minOccurs="0"/>
 *         &lt;element name="transId" type="{http://infrastructure.tia.dk/schema/common/v2/}long10" minOccurs="0"/>
 *         &lt;element name="policyNo" type="{http://infrastructure.tia.dk/schema/common/v2/}long10" minOccurs="0"/>
 *         &lt;element name="policyYear" type="{http://infrastructure.tia.dk/schema/common/v2/}int2" minOccurs="0"/>
 *         &lt;element name="policyLineNo" type="{http://infrastructure.tia.dk/schema/common/v2/}long10" minOccurs="0"/>
 *         &lt;element name="policyLineSeqNo" type="{http://infrastructure.tia.dk/schema/common/v2/}long10" minOccurs="0"/>
 *         &lt;element name="riskNo" type="{http://infrastructure.tia.dk/schema/common/v2/}int3" minOccurs="0"/>
 *         &lt;element name="collectionGroup" type="{http://infrastructure.tia.dk/schema/common/v2/}int3" minOccurs="0"/>
 *         &lt;element name="productGroup" type="{http://infrastructure.tia.dk/schema/common/v2/}string3" minOccurs="0"/>
 *         &lt;element name="comAmount" type="{http://infrastructure.tia.dk/schema/common/v2/}decimal30Point2" minOccurs="0"/>
 *         &lt;element name="premiumAmount" type="{http://infrastructure.tia.dk/schema/common/v2/}decimal30Point2" minOccurs="0"/>
 *         &lt;element name="comFlatAmount" type="{http://infrastructure.tia.dk/schema/common/v2/}decimal30Point2" minOccurs="0"/>
 *         &lt;element name="comWrittenAmount" type="{http://infrastructure.tia.dk/schema/common/v2/}decimal30Point2" minOccurs="0"/>
 *         &lt;element name="accountItemNo" type="{http://infrastructure.tia.dk/schema/common/v2/}long10" minOccurs="0"/>
 *         &lt;element name="paymentStatus" type="{http://infrastructure.tia.dk/schema/common/v2/}int3" minOccurs="0"/>
 *         &lt;element name="centerCode" type="{http://infrastructure.tia.dk/schema/common/v2/}string10" minOccurs="0"/>
 *         &lt;element name="comCategory" type="{http://infrastructure.tia.dk/schema/common/v2/}string15" minOccurs="0"/>
 *         &lt;element name="agentRole" type="{http://infrastructure.tia.dk/schema/common/v2/}int3" minOccurs="0"/>
 *         &lt;element name="coverStartDate" type="{http://infrastructure.tia.dk/schema/common/v2/}date" minOccurs="0"/>
 *         &lt;element name="coverEndDate" type="{http://infrastructure.tia.dk/schema/common/v2/}date" minOccurs="0"/>
 *         &lt;element name="holdUntilPaid" type="{http://infrastructure.tia.dk/schema/common/v2/}string1" minOccurs="0"/>
 *         &lt;element name="holdAccItemNo" type="{http://infrastructure.tia.dk/schema/common/v2/}long10" minOccurs="0"/>
 *         &lt;element name="externalReference" type="{http://infrastructure.tia.dk/schema/common/v2/}string15" minOccurs="0"/>
 *         &lt;element name="comAlgorithm" type="{http://infrastructure.tia.dk/schema/common/v2/}string15" minOccurs="0"/>
 *         &lt;element name="comAlgorithmVersion" type="{http://infrastructure.tia.dk/schema/common/v2/}int3" minOccurs="0"/>
 *         &lt;element name="itemText" type="{http://infrastructure.tia.dk/schema/common/v2/}string60" minOccurs="0"/>
 *         &lt;element name="seqNo" type="{http://infrastructure.tia.dk/schema/common/v2/}long10" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CommissionItem", propOrder = {
    "transactionDate",
    "comTransCode",
    "transId",
    "policyNo",
    "policyYear",
    "policyLineNo",
    "policyLineSeqNo",
    "riskNo",
    "collectionGroup",
    "productGroup",
    "comAmount",
    "premiumAmount",
    "comFlatAmount",
    "comWrittenAmount",
    "accountItemNo",
    "paymentStatus",
    "centerCode",
    "comCategory",
    "agentRole",
    "coverStartDate",
    "coverEndDate",
    "holdUntilPaid",
    "holdAccItemNo",
    "externalReference",
    "comAlgorithm",
    "comAlgorithmVersion",
    "itemText",
    "seqNo"
})
public class CommissionItem
    extends TIAObject
{

    @XmlElement(nillable = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar transactionDate;
    @XmlElement(nillable = true)
    protected Long comTransCode;
    @XmlElement(nillable = true)
    protected Long transId;
    @XmlElement(nillable = true)
    protected Long policyNo;
    @XmlElement(nillable = true)
    protected Long policyYear;
    @XmlElement(nillable = true)
    protected Long policyLineNo;
    @XmlElement(nillable = true)
    protected Long policyLineSeqNo;
    @XmlElement(nillable = true)
    protected Long riskNo;
    @XmlElement(nillable = true)
    protected Long collectionGroup;
    @XmlElement(nillable = true)
    protected String productGroup;
    @XmlElement(nillable = true)
    protected BigDecimal comAmount;
    @XmlElement(nillable = true)
    protected BigDecimal premiumAmount;
    @XmlElement(nillable = true)
    protected BigDecimal comFlatAmount;
    @XmlElement(nillable = true)
    protected BigDecimal comWrittenAmount;
    @XmlElement(nillable = true)
    protected Long accountItemNo;
    @XmlElement(nillable = true)
    protected Long paymentStatus;
    @XmlElement(nillable = true)
    protected String centerCode;
    @XmlElement(nillable = true)
    protected String comCategory;
    @XmlElement(nillable = true)
    protected Long agentRole;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar coverStartDate;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar coverEndDate;
    @XmlElement(nillable = true)
    protected String holdUntilPaid;
    @XmlElement(nillable = true)
    protected Long holdAccItemNo;
    @XmlElement(nillable = true)
    protected String externalReference;
    @XmlElement(nillable = true)
    protected String comAlgorithm;
    @XmlElement(nillable = true)
    protected Long comAlgorithmVersion;
    @XmlElement(nillable = true)
    protected String itemText;
    @XmlElement(nillable = true)
    protected Long seqNo;

    /**
     * Gets the value of the transactionDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTransactionDate() {
        return transactionDate;
    }

    /**
     * Sets the value of the transactionDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTransactionDate(XMLGregorianCalendar value) {
        this.transactionDate = value;
    }

    /**
     * Gets the value of the comTransCode property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getComTransCode() {
        return comTransCode;
    }

    /**
     * Sets the value of the comTransCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setComTransCode(Long value) {
        this.comTransCode = value;
    }

    /**
     * Gets the value of the transId property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getTransId() {
        return transId;
    }

    /**
     * Sets the value of the transId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setTransId(Long value) {
        this.transId = value;
    }

    /**
     * Gets the value of the policyNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getPolicyNo() {
        return policyNo;
    }

    /**
     * Sets the value of the policyNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setPolicyNo(Long value) {
        this.policyNo = value;
    }

    /**
     * Gets the value of the policyYear property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getPolicyYear() {
        return policyYear;
    }

    /**
     * Sets the value of the policyYear property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setPolicyYear(Long value) {
        this.policyYear = value;
    }

    /**
     * Gets the value of the policyLineNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getPolicyLineNo() {
        return policyLineNo;
    }

    /**
     * Sets the value of the policyLineNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setPolicyLineNo(Long value) {
        this.policyLineNo = value;
    }

    /**
     * Gets the value of the policyLineSeqNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getPolicyLineSeqNo() {
        return policyLineSeqNo;
    }

    /**
     * Sets the value of the policyLineSeqNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setPolicyLineSeqNo(Long value) {
        this.policyLineSeqNo = value;
    }

    /**
     * Gets the value of the riskNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getRiskNo() {
        return riskNo;
    }

    /**
     * Sets the value of the riskNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setRiskNo(Long value) {
        this.riskNo = value;
    }

    /**
     * Gets the value of the collectionGroup property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getCollectionGroup() {
        return collectionGroup;
    }

    /**
     * Sets the value of the collectionGroup property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setCollectionGroup(Long value) {
        this.collectionGroup = value;
    }

    /**
     * Gets the value of the productGroup property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductGroup() {
        return productGroup;
    }

    /**
     * Sets the value of the productGroup property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductGroup(String value) {
        this.productGroup = value;
    }

    /**
     * Gets the value of the comAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getComAmount() {
        return comAmount;
    }

    /**
     * Sets the value of the comAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setComAmount(BigDecimal value) {
        this.comAmount = value;
    }

    /**
     * Gets the value of the premiumAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPremiumAmount() {
        return premiumAmount;
    }

    /**
     * Sets the value of the premiumAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPremiumAmount(BigDecimal value) {
        this.premiumAmount = value;
    }

    /**
     * Gets the value of the comFlatAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getComFlatAmount() {
        return comFlatAmount;
    }

    /**
     * Sets the value of the comFlatAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setComFlatAmount(BigDecimal value) {
        this.comFlatAmount = value;
    }

    /**
     * Gets the value of the comWrittenAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getComWrittenAmount() {
        return comWrittenAmount;
    }

    /**
     * Sets the value of the comWrittenAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setComWrittenAmount(BigDecimal value) {
        this.comWrittenAmount = value;
    }

    /**
     * Gets the value of the accountItemNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getAccountItemNo() {
        return accountItemNo;
    }

    /**
     * Sets the value of the accountItemNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setAccountItemNo(Long value) {
        this.accountItemNo = value;
    }

    /**
     * Gets the value of the paymentStatus property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getPaymentStatus() {
        return paymentStatus;
    }

    /**
     * Sets the value of the paymentStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setPaymentStatus(Long value) {
        this.paymentStatus = value;
    }

    /**
     * Gets the value of the centerCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCenterCode() {
        return centerCode;
    }

    /**
     * Sets the value of the centerCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCenterCode(String value) {
        this.centerCode = value;
    }

    /**
     * Gets the value of the comCategory property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCategory() {
        return comCategory;
    }

    /**
     * Sets the value of the comCategory property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCategory(String value) {
        this.comCategory = value;
    }

    /**
     * Gets the value of the agentRole property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getAgentRole() {
        return agentRole;
    }

    /**
     * Sets the value of the agentRole property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setAgentRole(Long value) {
        this.agentRole = value;
    }

    /**
     * Gets the value of the coverStartDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCoverStartDate() {
        return coverStartDate;
    }

    /**
     * Sets the value of the coverStartDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCoverStartDate(XMLGregorianCalendar value) {
        this.coverStartDate = value;
    }

    /**
     * Gets the value of the coverEndDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCoverEndDate() {
        return coverEndDate;
    }

    /**
     * Sets the value of the coverEndDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCoverEndDate(XMLGregorianCalendar value) {
        this.coverEndDate = value;
    }

    /**
     * Gets the value of the holdUntilPaid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHoldUntilPaid() {
        return holdUntilPaid;
    }

    /**
     * Sets the value of the holdUntilPaid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHoldUntilPaid(String value) {
        this.holdUntilPaid = value;
    }

    /**
     * Gets the value of the holdAccItemNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getHoldAccItemNo() {
        return holdAccItemNo;
    }

    /**
     * Sets the value of the holdAccItemNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setHoldAccItemNo(Long value) {
        this.holdAccItemNo = value;
    }

    /**
     * Gets the value of the externalReference property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExternalReference() {
        return externalReference;
    }

    /**
     * Sets the value of the externalReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExternalReference(String value) {
        this.externalReference = value;
    }

    /**
     * Gets the value of the comAlgorithm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComAlgorithm() {
        return comAlgorithm;
    }

    /**
     * Sets the value of the comAlgorithm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComAlgorithm(String value) {
        this.comAlgorithm = value;
    }

    /**
     * Gets the value of the comAlgorithmVersion property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getComAlgorithmVersion() {
        return comAlgorithmVersion;
    }

    /**
     * Sets the value of the comAlgorithmVersion property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setComAlgorithmVersion(Long value) {
        this.comAlgorithmVersion = value;
    }

    /**
     * Gets the value of the itemText property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItemText() {
        return itemText;
    }

    /**
     * Sets the value of the itemText property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItemText(String value) {
        this.itemText = value;
    }

    /**
     * Gets the value of the seqNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getSeqNo() {
        return seqNo;
    }

    /**
     * Sets the value of the seqNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setSeqNo(Long value) {
        this.seqNo = value;
    }

}
