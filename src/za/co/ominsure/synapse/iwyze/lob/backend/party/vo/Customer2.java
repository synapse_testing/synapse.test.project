
package za.co.ominsure.synapse.iwyze.lob.backend.party.vo;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Is mapped into TIA metadata type "obj_customer".
 * 
 * <p>Java class for Customer complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Customer">
 *   &lt;complexContent>
 *     &lt;extension base="{http://infrastructure.tia.dk/schema/common/v2/}TIAObject">
 *       &lt;sequence>
 *         &lt;element name="potential" type="{http://infrastructure.tia.dk/schema/party/v2/}potential" minOccurs="0"/>
 *         &lt;element name="education" type="{http://infrastructure.tia.dk/schema/party/v2/}education" minOccurs="0"/>
 *         &lt;element name="occupationCode" type="{http://infrastructure.tia.dk/schema/party/v2/}occupationCode" minOccurs="0"/>
 *         &lt;element name="occupation" type="{http://infrastructure.tia.dk/schema/party/v2/}occupation" minOccurs="0"/>
 *         &lt;element name="profileCode" type="{http://infrastructure.tia.dk/schema/party/v2/}profileCode" minOccurs="0"/>
 *         &lt;element name="statusCode" type="{http://infrastructure.tia.dk/schema/party/v2/}statusCode" minOccurs="0"/>
 *         &lt;element name="incomeGroup" type="{http://infrastructure.tia.dk/schema/party/v2/}incomeGroup" minOccurs="0"/>
 *         &lt;element name="sex" type="{http://infrastructure.tia.dk/schema/party/v2/}sex" minOccurs="0"/>
 *         &lt;element name="maritalState" type="{http://infrastructure.tia.dk/schema/party/v2/}maritalState" minOccurs="0"/>
 *         &lt;element name="spouseBirthDate" type="{http://infrastructure.tia.dk/schema/common/v2/}date" minOccurs="0"/>
 *         &lt;element name="spouseName" type="{http://infrastructure.tia.dk/schema/party/v2/}spouseName" minOccurs="0"/>
 *         &lt;element name="spouseSex" type="{http://infrastructure.tia.dk/schema/party/v2/}spouseSex" minOccurs="0"/>
 *         &lt;element name="children" type="{http://infrastructure.tia.dk/schema/party/v2/}children" minOccurs="0"/>
 *         &lt;element name="household" type="{http://infrastructure.tia.dk/schema/party/v2/}household" minOccurs="0"/>
 *         &lt;element name="howNotToContact" type="{http://infrastructure.tia.dk/schema/party/v2/}contact" minOccurs="0"/>
 *         &lt;element name="howToContact" type="{http://infrastructure.tia.dk/schema/party/v2/}contact" minOccurs="0"/>
 *         &lt;element name="serviceCode" type="{http://infrastructure.tia.dk/schema/party/v2/}serviceCode" minOccurs="0"/>
 *         &lt;element name="yourRef" type="{http://infrastructure.tia.dk/schema/party/v2/}yourRef" minOccurs="0"/>
 *         &lt;element name="firstContact" type="{http://infrastructure.tia.dk/schema/common/v2/}date" minOccurs="0"/>
 *         &lt;element name="lastContact" type="{http://infrastructure.tia.dk/schema/common/v2/}date" minOccurs="0"/>
 *         &lt;element name="lastContactWay" type="{http://infrastructure.tia.dk/schema/party/v2/}contact" minOccurs="0"/>
 *         &lt;element name="reminderGroup" type="{http://infrastructure.tia.dk/schema/party/v2/}reminderGroup" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Customer", propOrder = {
    "potential",
    "education",
    "occupationCode",
    "occupation",
    "profileCode",
    "statusCode",
    "incomeGroup",
    "sex",
    "maritalState",
    "spouseBirthDate",
    "spouseName",
    "spouseSex",
    "children",
    "household",
    "howNotToContact",
    "howToContact",
    "serviceCode",
    "yourRef",
    "firstContact",
    "lastContact",
    "lastContactWay",
    "reminderGroup"
})
@XmlSeeAlso({
    Customer.class
})
public class Customer2
    extends TIAObject
{

    @XmlElementRef(name = "potential", namespace = "http://infrastructure.tia.dk/schema/party/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> potential;
    @XmlElementRef(name = "education", namespace = "http://infrastructure.tia.dk/schema/party/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> education;
    @XmlElementRef(name = "occupationCode", namespace = "http://infrastructure.tia.dk/schema/party/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> occupationCode;
    @XmlElementRef(name = "occupation", namespace = "http://infrastructure.tia.dk/schema/party/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> occupation;
    @XmlElementRef(name = "profileCode", namespace = "http://infrastructure.tia.dk/schema/party/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> profileCode;
    @XmlElementRef(name = "statusCode", namespace = "http://infrastructure.tia.dk/schema/party/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> statusCode;
    @XmlElementRef(name = "incomeGroup", namespace = "http://infrastructure.tia.dk/schema/party/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> incomeGroup;
    @XmlElementRef(name = "sex", namespace = "http://infrastructure.tia.dk/schema/party/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> sex;
    @XmlElementRef(name = "maritalState", namespace = "http://infrastructure.tia.dk/schema/party/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> maritalState;
    @XmlElementRef(name = "spouseBirthDate", namespace = "http://infrastructure.tia.dk/schema/party/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> spouseBirthDate;
    @XmlElementRef(name = "spouseName", namespace = "http://infrastructure.tia.dk/schema/party/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> spouseName;
    @XmlElementRef(name = "spouseSex", namespace = "http://infrastructure.tia.dk/schema/party/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> spouseSex;
    @XmlElementRef(name = "children", namespace = "http://infrastructure.tia.dk/schema/party/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> children;
    @XmlElementRef(name = "household", namespace = "http://infrastructure.tia.dk/schema/party/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> household;
    @XmlElementRef(name = "howNotToContact", namespace = "http://infrastructure.tia.dk/schema/party/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> howNotToContact;
    @XmlElementRef(name = "howToContact", namespace = "http://infrastructure.tia.dk/schema/party/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> howToContact;
    @XmlElementRef(name = "serviceCode", namespace = "http://infrastructure.tia.dk/schema/party/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> serviceCode;
    @XmlElementRef(name = "yourRef", namespace = "http://infrastructure.tia.dk/schema/party/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> yourRef;
    @XmlElementRef(name = "firstContact", namespace = "http://infrastructure.tia.dk/schema/party/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> firstContact;
    @XmlElementRef(name = "lastContact", namespace = "http://infrastructure.tia.dk/schema/party/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> lastContact;
    @XmlElementRef(name = "lastContactWay", namespace = "http://infrastructure.tia.dk/schema/party/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> lastContactWay;
    @XmlElementRef(name = "reminderGroup", namespace = "http://infrastructure.tia.dk/schema/party/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> reminderGroup;

    /**
     * Gets the value of the potential property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPotential() {
        return potential;
    }

    /**
     * Sets the value of the potential property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPotential(JAXBElement<String> value) {
        this.potential = value;
    }

    /**
     * Gets the value of the education property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getEducation() {
        return education;
    }

    /**
     * Sets the value of the education property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setEducation(JAXBElement<String> value) {
        this.education = value;
    }

    /**
     * Gets the value of the occupationCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOccupationCode() {
        return occupationCode;
    }

    /**
     * Sets the value of the occupationCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOccupationCode(JAXBElement<String> value) {
        this.occupationCode = value;
    }

    /**
     * Gets the value of the occupation property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOccupation() {
        return occupation;
    }

    /**
     * Sets the value of the occupation property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOccupation(JAXBElement<String> value) {
        this.occupation = value;
    }

    /**
     * Gets the value of the profileCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getProfileCode() {
        return profileCode;
    }

    /**
     * Sets the value of the profileCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setProfileCode(JAXBElement<String> value) {
        this.profileCode = value;
    }

    /**
     * Gets the value of the statusCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getStatusCode() {
        return statusCode;
    }

    /**
     * Sets the value of the statusCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setStatusCode(JAXBElement<String> value) {
        this.statusCode = value;
    }

    /**
     * Gets the value of the incomeGroup property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getIncomeGroup() {
        return incomeGroup;
    }

    /**
     * Sets the value of the incomeGroup property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setIncomeGroup(JAXBElement<String> value) {
        this.incomeGroup = value;
    }

    /**
     * Gets the value of the sex property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSex() {
        return sex;
    }

    /**
     * Sets the value of the sex property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSex(JAXBElement<String> value) {
        this.sex = value;
    }

    /**
     * Gets the value of the maritalState property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getMaritalState() {
        return maritalState;
    }

    /**
     * Sets the value of the maritalState property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setMaritalState(JAXBElement<Integer> value) {
        this.maritalState = value;
    }

    /**
     * Gets the value of the spouseBirthDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getSpouseBirthDate() {
        return spouseBirthDate;
    }

    /**
     * Sets the value of the spouseBirthDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setSpouseBirthDate(JAXBElement<XMLGregorianCalendar> value) {
        this.spouseBirthDate = value;
    }

    /**
     * Gets the value of the spouseName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSpouseName() {
        return spouseName;
    }

    /**
     * Sets the value of the spouseName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSpouseName(JAXBElement<String> value) {
        this.spouseName = value;
    }

    /**
     * Gets the value of the spouseSex property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSpouseSex() {
        return spouseSex;
    }

    /**
     * Sets the value of the spouseSex property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSpouseSex(JAXBElement<String> value) {
        this.spouseSex = value;
    }

    /**
     * Gets the value of the children property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getChildren() {
        return children;
    }

    /**
     * Sets the value of the children property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setChildren(JAXBElement<Integer> value) {
        this.children = value;
    }

    /**
     * Gets the value of the household property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getHousehold() {
        return household;
    }

    /**
     * Sets the value of the household property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setHousehold(JAXBElement<Integer> value) {
        this.household = value;
    }

    /**
     * Gets the value of the howNotToContact property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getHowNotToContact() {
        return howNotToContact;
    }

    /**
     * Sets the value of the howNotToContact property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setHowNotToContact(JAXBElement<Integer> value) {
        this.howNotToContact = value;
    }

    /**
     * Gets the value of the howToContact property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getHowToContact() {
        return howToContact;
    }

    /**
     * Sets the value of the howToContact property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setHowToContact(JAXBElement<Integer> value) {
        this.howToContact = value;
    }

    /**
     * Gets the value of the serviceCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getServiceCode() {
        return serviceCode;
    }

    /**
     * Sets the value of the serviceCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setServiceCode(JAXBElement<String> value) {
        this.serviceCode = value;
    }

    /**
     * Gets the value of the yourRef property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getYourRef() {
        return yourRef;
    }

    /**
     * Sets the value of the yourRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setYourRef(JAXBElement<String> value) {
        this.yourRef = value;
    }

    /**
     * Gets the value of the firstContact property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getFirstContact() {
        return firstContact;
    }

    /**
     * Sets the value of the firstContact property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setFirstContact(JAXBElement<XMLGregorianCalendar> value) {
        this.firstContact = value;
    }

    /**
     * Gets the value of the lastContact property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getLastContact() {
        return lastContact;
    }

    /**
     * Sets the value of the lastContact property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setLastContact(JAXBElement<XMLGregorianCalendar> value) {
        this.lastContact = value;
    }

    /**
     * Gets the value of the lastContactWay property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getLastContactWay() {
        return lastContactWay;
    }

    /**
     * Sets the value of the lastContactWay property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setLastContactWay(JAXBElement<Integer> value) {
        this.lastContactWay = value;
    }

    /**
     * Gets the value of the reminderGroup property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getReminderGroup() {
        return reminderGroup;
    }

    /**
     * Sets the value of the reminderGroup property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setReminderGroup(JAXBElement<String> value) {
        this.reminderGroup = value;
    }

}
