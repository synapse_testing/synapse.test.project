
package za.co.ominsure.synapse.iwyze.lob.backend.claim.payment.vo;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SubclaimCollection complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SubclaimCollection">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="subclaim" type="{http://infrastructure.tia.dk/schema/claim/v2/}Subclaim" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SubclaimCollection", propOrder = {
    "subclaims"
})
public class SubclaimCollection {

    @XmlElement(name = "subclaim")
    protected List<Subclaim> subclaims;

    /**
     * Gets the value of the subclaims property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the subclaims property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSubclaims().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Subclaim }
     * 
     * 
     */
    public List<Subclaim> getSubclaims() {
        if (subclaims == null) {
            subclaims = new ArrayList<Subclaim>();
        }
        return this.subclaims;
    }

}
