
package za.co.ominsure.synapse.iwyze.lob.backend.party.vo;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * Additional comments
 * 
 * <p>Java class for SearchTokenAff complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SearchTokenAff">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="affinityGroupDesc" type="{http://infrastructure.tia.dk/schema/common/v2/}string2000" minOccurs="0"/>
 *         &lt;element name="affinityGroupName" type="{http://infrastructure.tia.dk/schema/common/v2/}string60" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SearchTokenAff", propOrder = {
    "affinityGroupDesc",
    "affinityGroupName"
})
public class SearchTokenAff {

    @XmlElementRef(name = "affinityGroupDesc", namespace = "http://infrastructure.tia.dk/schema/party/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> affinityGroupDesc;
    @XmlElementRef(name = "affinityGroupName", namespace = "http://infrastructure.tia.dk/schema/party/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> affinityGroupName;

    /**
     * Gets the value of the affinityGroupDesc property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAffinityGroupDesc() {
        return affinityGroupDesc;
    }

    /**
     * Sets the value of the affinityGroupDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAffinityGroupDesc(JAXBElement<String> value) {
        this.affinityGroupDesc = value;
    }

    /**
     * Gets the value of the affinityGroupName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAffinityGroupName() {
        return affinityGroupName;
    }

    /**
     * Sets the value of the affinityGroupName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAffinityGroupName(JAXBElement<String> value) {
        this.affinityGroupName = value;
    }

}
