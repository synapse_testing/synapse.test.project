
package za.co.ominsure.synapse.iwyze.lob.backend.policy.vo;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Is mapped into TIA metadata type "tab_relation".
 * 
 * <p>Java class for RelationCollection complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RelationCollection">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;sequence>
 *           &lt;element name="partyRelation" type="{http://infrastructure.tia.dk/schema/party/v2/}PartyRelation" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;/sequence>
 *         &lt;sequence>
 *           &lt;element name="affinityRelation" type="{http://infrastructure.tia.dk/schema/party/v2/}AffinityRelation" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;/sequence>
 *         &lt;sequence>
 *           &lt;element name="policyRelation" type="{http://infrastructure.tia.dk/schema/party/v2/}PolicyRelation" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;/sequence>
 *         &lt;sequence>
 *           &lt;element name="policyLineRelation" type="{http://infrastructure.tia.dk/schema/party/v2/}PolicyLineRelation" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;/sequence>
 *         &lt;sequence>
 *           &lt;element name="policyObjectRelation" type="{http://infrastructure.tia.dk/schema/party/v2/}PolicyObjectRelation" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;/sequence>
 *         &lt;sequence>
 *           &lt;element name="claimEventRelation" type="{http://infrastructure.tia.dk/schema/party/v2/}ClaimEventRelation" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;/sequence>
 *         &lt;sequence>
 *           &lt;element name="claimRelation" type="{http://infrastructure.tia.dk/schema/party/v2/}ClaimRelation" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;/sequence>
 *         &lt;sequence>
 *           &lt;element name="subclaimRelation" type="{http://infrastructure.tia.dk/schema/party/v2/}SubclaimRelation" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;/sequence>
 *         &lt;sequence>
 *           &lt;element name="claimItemRelation" type="{http://infrastructure.tia.dk/schema/party/v2/}ClaimItemRelation" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;/sequence>
 *         &lt;sequence>
 *           &lt;element name="claimThirdPartyRelation" type="{http://infrastructure.tia.dk/schema/party/v2/}ClaimThirdPartyRelation" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;/sequence>
 *         &lt;sequence>
 *           &lt;element name="accountRelation" type="{http://infrastructure.tia.dk/schema/party/v2/}AccountRelation" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;/sequence>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RelationCollection", namespace = "http://infrastructure.tia.dk/schema/party/v2/", propOrder = {
    "accountRelations",
    "claimThirdPartyRelations",
    "claimItemRelations",
    "subclaimRelations",
    "claimRelations",
    "claimEventRelations",
    "policyObjectRelations",
    "policyLineRelations",
    "policyRelations",
    "affinityRelations",
    "partyRelations"
})
public class RelationCollection {

    @XmlElement(name = "accountRelation")
    protected List<AccountRelation> accountRelations;
    @XmlElement(name = "claimThirdPartyRelation")
    protected List<ClaimThirdPartyRelation> claimThirdPartyRelations;
    @XmlElement(name = "claimItemRelation")
    protected List<ClaimItemRelation> claimItemRelations;
    @XmlElement(name = "subclaimRelation")
    protected List<SubclaimRelation> subclaimRelations;
    @XmlElement(name = "claimRelation")
    protected List<ClaimRelation> claimRelations;
    @XmlElement(name = "claimEventRelation")
    protected List<ClaimEventRelation> claimEventRelations;
    @XmlElement(name = "policyObjectRelation")
    protected List<PolicyObjectRelation> policyObjectRelations;
    @XmlElement(name = "policyLineRelation")
    protected List<PolicyLineRelation> policyLineRelations;
    @XmlElement(name = "policyRelation")
    protected List<PolicyRelation> policyRelations;
    @XmlElement(name = "affinityRelation")
    protected List<AffinityRelation> affinityRelations;
    @XmlElement(name = "partyRelation")
    protected List<PartyRelation> partyRelations;

    /**
     * Gets the value of the accountRelations property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the accountRelations property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAccountRelations().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AccountRelation }
     * 
     * 
     */
    public List<AccountRelation> getAccountRelations() {
        if (accountRelations == null) {
            accountRelations = new ArrayList<AccountRelation>();
        }
        return this.accountRelations;
    }

    /**
     * Gets the value of the claimThirdPartyRelations property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the claimThirdPartyRelations property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getClaimThirdPartyRelations().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ClaimThirdPartyRelation }
     * 
     * 
     */
    public List<ClaimThirdPartyRelation> getClaimThirdPartyRelations() {
        if (claimThirdPartyRelations == null) {
            claimThirdPartyRelations = new ArrayList<ClaimThirdPartyRelation>();
        }
        return this.claimThirdPartyRelations;
    }

    /**
     * Gets the value of the claimItemRelations property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the claimItemRelations property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getClaimItemRelations().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ClaimItemRelation }
     * 
     * 
     */
    public List<ClaimItemRelation> getClaimItemRelations() {
        if (claimItemRelations == null) {
            claimItemRelations = new ArrayList<ClaimItemRelation>();
        }
        return this.claimItemRelations;
    }

    /**
     * Gets the value of the subclaimRelations property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the subclaimRelations property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSubclaimRelations().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SubclaimRelation }
     * 
     * 
     */
    public List<SubclaimRelation> getSubclaimRelations() {
        if (subclaimRelations == null) {
            subclaimRelations = new ArrayList<SubclaimRelation>();
        }
        return this.subclaimRelations;
    }

    /**
     * Gets the value of the claimRelations property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the claimRelations property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getClaimRelations().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ClaimRelation }
     * 
     * 
     */
    public List<ClaimRelation> getClaimRelations() {
        if (claimRelations == null) {
            claimRelations = new ArrayList<ClaimRelation>();
        }
        return this.claimRelations;
    }

    /**
     * Gets the value of the claimEventRelations property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the claimEventRelations property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getClaimEventRelations().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ClaimEventRelation }
     * 
     * 
     */
    public List<ClaimEventRelation> getClaimEventRelations() {
        if (claimEventRelations == null) {
            claimEventRelations = new ArrayList<ClaimEventRelation>();
        }
        return this.claimEventRelations;
    }

    /**
     * Gets the value of the policyObjectRelations property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the policyObjectRelations property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPolicyObjectRelations().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PolicyObjectRelation }
     * 
     * 
     */
    public List<PolicyObjectRelation> getPolicyObjectRelations() {
        if (policyObjectRelations == null) {
            policyObjectRelations = new ArrayList<PolicyObjectRelation>();
        }
        return this.policyObjectRelations;
    }

    /**
     * Gets the value of the policyLineRelations property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the policyLineRelations property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPolicyLineRelations().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PolicyLineRelation }
     * 
     * 
     */
    public List<PolicyLineRelation> getPolicyLineRelations() {
        if (policyLineRelations == null) {
            policyLineRelations = new ArrayList<PolicyLineRelation>();
        }
        return this.policyLineRelations;
    }

    /**
     * Gets the value of the policyRelations property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the policyRelations property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPolicyRelations().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PolicyRelation }
     * 
     * 
     */
    public List<PolicyRelation> getPolicyRelations() {
        if (policyRelations == null) {
            policyRelations = new ArrayList<PolicyRelation>();
        }
        return this.policyRelations;
    }

    /**
     * Gets the value of the affinityRelations property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the affinityRelations property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAffinityRelations().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AffinityRelation }
     * 
     * 
     */
    public List<AffinityRelation> getAffinityRelations() {
        if (affinityRelations == null) {
            affinityRelations = new ArrayList<AffinityRelation>();
        }
        return this.affinityRelations;
    }

    /**
     * Gets the value of the partyRelations property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the partyRelations property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPartyRelations().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PartyRelation }
     * 
     * 
     */
    public List<PartyRelation> getPartyRelations() {
        if (partyRelations == null) {
            partyRelations = new ArrayList<PartyRelation>();
        }
        return this.partyRelations;
    }

}
