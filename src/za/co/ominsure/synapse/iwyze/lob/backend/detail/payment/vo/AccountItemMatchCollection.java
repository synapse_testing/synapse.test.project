
package za.co.ominsure.synapse.iwyze.lob.backend.detail.payment.vo;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Is mapped into TIA metadata type "tab_account_item_match".
 * 
 * <p>Java class for AccountItemMatchCollection complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AccountItemMatchCollection">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="accountItemMatch" type="{http://infrastructure.tia.dk/schema/account/v2/}AccountItemMatch" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AccountItemMatchCollection", propOrder = {
    "accountItemMatches"
})
public class AccountItemMatchCollection {

    @XmlElement(name = "accountItemMatch")
    protected List<AccountItemMatch> accountItemMatches;

    /**
     * Gets the value of the accountItemMatches property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the accountItemMatches property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAccountItemMatches().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AccountItemMatch }
     * 
     * 
     */
    public List<AccountItemMatch> getAccountItemMatches() {
        if (accountItemMatches == null) {
            accountItemMatches = new ArrayList<AccountItemMatch>();
        }
        return this.accountItemMatches;
    }

}
