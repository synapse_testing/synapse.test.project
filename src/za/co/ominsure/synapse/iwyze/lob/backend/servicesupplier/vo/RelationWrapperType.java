
package za.co.ominsure.synapse.iwyze.lob.backend.servicesupplier.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RelationWrapperType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RelationWrapperType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;sequence>
 *           &lt;element name="partyRelation" type="{http://infrastructure.tia.dk/schema/party/v2/}PartyRelation" minOccurs="0"/>
 *         &lt;/sequence>
 *         &lt;sequence>
 *           &lt;element name="affinityRelation" type="{http://infrastructure.tia.dk/schema/party/v2/}AffinityRelation" minOccurs="0"/>
 *         &lt;/sequence>
 *         &lt;sequence>
 *           &lt;element name="policyRelation" type="{http://infrastructure.tia.dk/schema/party/v2/}PolicyRelation" minOccurs="0"/>
 *         &lt;/sequence>
 *         &lt;sequence>
 *           &lt;element name="policyLineRelation" type="{http://infrastructure.tia.dk/schema/party/v2/}PolicyLineRelation" minOccurs="0"/>
 *         &lt;/sequence>
 *         &lt;sequence>
 *           &lt;element name="policyObjectRelation" type="{http://infrastructure.tia.dk/schema/party/v2/}PolicyObjectRelation" minOccurs="0"/>
 *         &lt;/sequence>
 *         &lt;sequence>
 *           &lt;element name="claimEventRelation" type="{http://infrastructure.tia.dk/schema/party/v2/}ClaimEventRelation" minOccurs="0"/>
 *         &lt;/sequence>
 *         &lt;sequence>
 *           &lt;element name="claimRelation" type="{http://infrastructure.tia.dk/schema/party/v2/}ClaimRelation" minOccurs="0"/>
 *         &lt;/sequence>
 *         &lt;sequence>
 *           &lt;element name="subclaimRelation" type="{http://infrastructure.tia.dk/schema/party/v2/}SubclaimRelation" minOccurs="0"/>
 *         &lt;/sequence>
 *         &lt;sequence>
 *           &lt;element name="claimItemRelation" type="{http://infrastructure.tia.dk/schema/party/v2/}ClaimItemRelation" minOccurs="0"/>
 *         &lt;/sequence>
 *         &lt;sequence>
 *           &lt;element name="claimThirdPartyRelation" type="{http://infrastructure.tia.dk/schema/party/v2/}ClaimThirdPartyRelation" minOccurs="0"/>
 *         &lt;/sequence>
 *         &lt;sequence>
 *           &lt;element name="accountRelation" type="{http://infrastructure.tia.dk/schema/party/v2/}AccountRelation" minOccurs="0"/>
 *         &lt;/sequence>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RelationWrapperType", propOrder = {
    "partyRelation",
    "affinityRelation",
    "policyRelation",
    "policyLineRelation",
    "policyObjectRelation",
    "claimEventRelation",
    "claimRelation",
    "subclaimRelation",
    "claimItemRelation",
    "claimThirdPartyRelation",
    "accountRelation"
})
public class RelationWrapperType {

    protected PartyRelation partyRelation;
    protected AffinityRelation affinityRelation;
    protected PolicyRelation policyRelation;
    protected PolicyLineRelation policyLineRelation;
    protected PolicyObjectRelation policyObjectRelation;
    protected ClaimEventRelation claimEventRelation;
    protected ClaimRelation claimRelation;
    protected SubclaimRelation subclaimRelation;
    protected ClaimItemRelation claimItemRelation;
    protected ClaimThirdPartyRelation claimThirdPartyRelation;
    protected AccountRelation accountRelation;

    /**
     * Gets the value of the partyRelation property.
     * 
     * @return
     *     possible object is
     *     {@link PartyRelation }
     *     
     */
    public PartyRelation getPartyRelation() {
        return partyRelation;
    }

    /**
     * Sets the value of the partyRelation property.
     * 
     * @param value
     *     allowed object is
     *     {@link PartyRelation }
     *     
     */
    public void setPartyRelation(PartyRelation value) {
        this.partyRelation = value;
    }

    /**
     * Gets the value of the affinityRelation property.
     * 
     * @return
     *     possible object is
     *     {@link AffinityRelation }
     *     
     */
    public AffinityRelation getAffinityRelation() {
        return affinityRelation;
    }

    /**
     * Sets the value of the affinityRelation property.
     * 
     * @param value
     *     allowed object is
     *     {@link AffinityRelation }
     *     
     */
    public void setAffinityRelation(AffinityRelation value) {
        this.affinityRelation = value;
    }

    /**
     * Gets the value of the policyRelation property.
     * 
     * @return
     *     possible object is
     *     {@link PolicyRelation }
     *     
     */
    public PolicyRelation getPolicyRelation() {
        return policyRelation;
    }

    /**
     * Sets the value of the policyRelation property.
     * 
     * @param value
     *     allowed object is
     *     {@link PolicyRelation }
     *     
     */
    public void setPolicyRelation(PolicyRelation value) {
        this.policyRelation = value;
    }

    /**
     * Gets the value of the policyLineRelation property.
     * 
     * @return
     *     possible object is
     *     {@link PolicyLineRelation }
     *     
     */
    public PolicyLineRelation getPolicyLineRelation() {
        return policyLineRelation;
    }

    /**
     * Sets the value of the policyLineRelation property.
     * 
     * @param value
     *     allowed object is
     *     {@link PolicyLineRelation }
     *     
     */
    public void setPolicyLineRelation(PolicyLineRelation value) {
        this.policyLineRelation = value;
    }

    /**
     * Gets the value of the policyObjectRelation property.
     * 
     * @return
     *     possible object is
     *     {@link PolicyObjectRelation }
     *     
     */
    public PolicyObjectRelation getPolicyObjectRelation() {
        return policyObjectRelation;
    }

    /**
     * Sets the value of the policyObjectRelation property.
     * 
     * @param value
     *     allowed object is
     *     {@link PolicyObjectRelation }
     *     
     */
    public void setPolicyObjectRelation(PolicyObjectRelation value) {
        this.policyObjectRelation = value;
    }

    /**
     * Gets the value of the claimEventRelation property.
     * 
     * @return
     *     possible object is
     *     {@link ClaimEventRelation }
     *     
     */
    public ClaimEventRelation getClaimEventRelation() {
        return claimEventRelation;
    }

    /**
     * Sets the value of the claimEventRelation property.
     * 
     * @param value
     *     allowed object is
     *     {@link ClaimEventRelation }
     *     
     */
    public void setClaimEventRelation(ClaimEventRelation value) {
        this.claimEventRelation = value;
    }

    /**
     * Gets the value of the claimRelation property.
     * 
     * @return
     *     possible object is
     *     {@link ClaimRelation }
     *     
     */
    public ClaimRelation getClaimRelation() {
        return claimRelation;
    }

    /**
     * Sets the value of the claimRelation property.
     * 
     * @param value
     *     allowed object is
     *     {@link ClaimRelation }
     *     
     */
    public void setClaimRelation(ClaimRelation value) {
        this.claimRelation = value;
    }

    /**
     * Gets the value of the subclaimRelation property.
     * 
     * @return
     *     possible object is
     *     {@link SubclaimRelation }
     *     
     */
    public SubclaimRelation getSubclaimRelation() {
        return subclaimRelation;
    }

    /**
     * Sets the value of the subclaimRelation property.
     * 
     * @param value
     *     allowed object is
     *     {@link SubclaimRelation }
     *     
     */
    public void setSubclaimRelation(SubclaimRelation value) {
        this.subclaimRelation = value;
    }

    /**
     * Gets the value of the claimItemRelation property.
     * 
     * @return
     *     possible object is
     *     {@link ClaimItemRelation }
     *     
     */
    public ClaimItemRelation getClaimItemRelation() {
        return claimItemRelation;
    }

    /**
     * Sets the value of the claimItemRelation property.
     * 
     * @param value
     *     allowed object is
     *     {@link ClaimItemRelation }
     *     
     */
    public void setClaimItemRelation(ClaimItemRelation value) {
        this.claimItemRelation = value;
    }

    /**
     * Gets the value of the claimThirdPartyRelation property.
     * 
     * @return
     *     possible object is
     *     {@link ClaimThirdPartyRelation }
     *     
     */
    public ClaimThirdPartyRelation getClaimThirdPartyRelation() {
        return claimThirdPartyRelation;
    }

    /**
     * Sets the value of the claimThirdPartyRelation property.
     * 
     * @param value
     *     allowed object is
     *     {@link ClaimThirdPartyRelation }
     *     
     */
    public void setClaimThirdPartyRelation(ClaimThirdPartyRelation value) {
        this.claimThirdPartyRelation = value;
    }

    /**
     * Gets the value of the accountRelation property.
     * 
     * @return
     *     possible object is
     *     {@link AccountRelation }
     *     
     */
    public AccountRelation getAccountRelation() {
        return accountRelation;
    }

    /**
     * Sets the value of the accountRelation property.
     * 
     * @param value
     *     allowed object is
     *     {@link AccountRelation }
     *     
     */
    public void setAccountRelation(AccountRelation value) {
        this.accountRelation = value;
    }

}
