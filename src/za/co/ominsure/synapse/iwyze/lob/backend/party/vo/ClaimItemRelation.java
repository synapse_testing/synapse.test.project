
package za.co.ominsure.synapse.iwyze.lob.backend.party.vo;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * Additional comments
 * 
 * <p>Java class for ClaimItemRelation complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ClaimItemRelation">
 *   &lt;complexContent>
 *     &lt;extension base="{http://infrastructure.tia.dk/schema/party/v2/}Relation">
 *       &lt;sequence>
 *         &lt;element name="claimItemNo" type="{http://infrastructure.tia.dk/schema/common/v2/}long10" minOccurs="0"/>
 *         &lt;element name="memberNameIdNo" type="{http://infrastructure.tia.dk/schema/common/v2/}long10" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ClaimItemRelation", propOrder = {
    "claimItemNo",
    "memberNameIdNo"
})
public class ClaimItemRelation
    extends Relation
{

    @XmlElementRef(name = "claimItemNo", namespace = "http://infrastructure.tia.dk/schema/party/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<Long> claimItemNo;
    @XmlElementRef(name = "memberNameIdNo", namespace = "http://infrastructure.tia.dk/schema/party/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<Long> memberNameIdNo;

    /**
     * Gets the value of the claimItemNo property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Long }{@code >}
     *     
     */
    public JAXBElement<Long> getClaimItemNo() {
        return claimItemNo;
    }

    /**
     * Sets the value of the claimItemNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Long }{@code >}
     *     
     */
    public void setClaimItemNo(JAXBElement<Long> value) {
        this.claimItemNo = value;
    }

    /**
     * Gets the value of the memberNameIdNo property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Long }{@code >}
     *     
     */
    public JAXBElement<Long> getMemberNameIdNo() {
        return memberNameIdNo;
    }

    /**
     * Sets the value of the memberNameIdNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Long }{@code >}
     *     
     */
    public void setMemberNameIdNo(JAXBElement<Long> value) {
        this.memberNameIdNo = value;
    }

}
