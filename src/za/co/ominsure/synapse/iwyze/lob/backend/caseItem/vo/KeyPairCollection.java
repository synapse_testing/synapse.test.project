
package za.co.ominsure.synapse.iwyze.lob.backend.caseItem.vo;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Definition for type tab_key_value_pair_varc2_varc2
 * 
 * <p>Java class for KeyPairCollection complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="KeyPairCollection">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="keyPair" type="{http://infrastructure.tia.dk/schema/common/v2/}KeyPair" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "KeyPairCollection", propOrder = {
    "keyPairs"
})
public class KeyPairCollection {

    @XmlElement(name = "keyPair")
    protected List<KeyPair> keyPairs;

    /**
     * Gets the value of the keyPairs property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the keyPairs property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getKeyPairs().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link KeyPair }
     * 
     * 
     */
    public List<KeyPair> getKeyPairs() {
        if (keyPairs == null) {
            keyPairs = new ArrayList<KeyPair>();
        }
        return this.keyPairs;
    }

}
