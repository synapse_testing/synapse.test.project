
package za.co.ominsure.synapse.iwyze.lob.backend.party.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for VersionToken complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="VersionToken">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="timestamp" type="{http://infrastructure.tia.dk/schema/common/v2/}timestamp"/>
 *         &lt;element name="userId" type="{http://infrastructure.tia.dk/schema/common/v2/}userId"/>
 *         &lt;element name="recordVersion" type="{http://infrastructure.tia.dk/schema/common/v2/}recordVersion"/>
 *         &lt;element name="recordTimestamp" type="{http://infrastructure.tia.dk/schema/common/v2/}timestamp"/>
 *         &lt;element name="recordUserid" type="{http://infrastructure.tia.dk/schema/common/v2/}userId"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VersionToken", namespace = "http://infrastructure.tia.dk/schema/common/v2/", propOrder = {
    "timestamp",
    "userId",
    "recordVersion",
    "recordTimestamp",
    "recordUserid"
})
public class VersionToken {

    @XmlElement(required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar timestamp;
    @XmlElement(required = true, nillable = true)
    protected String userId;
    @XmlElement(required = true, type = Long.class, nillable = true)
    @XmlSchemaType(name = "unsignedLong")
    protected Long recordVersion;
    @XmlElement(required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar recordTimestamp;
    @XmlElement(required = true, nillable = true)
    protected String recordUserid;

    /**
     * Gets the value of the timestamp property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTimestamp() {
        return timestamp;
    }

    /**
     * Sets the value of the timestamp property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTimestamp(XMLGregorianCalendar value) {
        this.timestamp = value;
    }

    /**
     * Gets the value of the userId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Sets the value of the userId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserId(String value) {
        this.userId = value;
    }

    /**
     * Gets the value of the recordVersion property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getRecordVersion() {
        return recordVersion;
    }

    /**
     * Sets the value of the recordVersion property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setRecordVersion(Long value) {
        this.recordVersion = value;
    }

    /**
     * Gets the value of the recordTimestamp property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getRecordTimestamp() {
        return recordTimestamp;
    }

    /**
     * Sets the value of the recordTimestamp property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setRecordTimestamp(XMLGregorianCalendar value) {
        this.recordTimestamp = value;
    }

    /**
     * Gets the value of the recordUserid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRecordUserid() {
        return recordUserid;
    }

    /**
     * Sets the value of the recordUserid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRecordUserid(String value) {
        this.recordUserid = value;
    }

}
