
package za.co.ominsure.synapse.iwyze.lob.backend.claim.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Additional comments
 * 
 * <p>Java class for SearchTokenSubclaim complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SearchTokenSubclaim">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="claimNo" type="{http://infrastructure.tia.dk/schema/common/v2/}long10" minOccurs="0"/>
 *         &lt;element name="thirdPartyId" type="{http://infrastructure.tia.dk/schema/common/v2/}long10" minOccurs="0"/>
 *         &lt;element name="objectSeqNo" type="{http://infrastructure.tia.dk/schema/common/v2/}long10" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SearchTokenSubclaim", propOrder = {
    "claimNo",
    "thirdPartyId",
    "objectSeqNo"
})
public class SearchTokenSubclaim {

    @XmlElement(nillable = true)
    protected Long claimNo;
    @XmlElement(nillable = true)
    protected Long thirdPartyId;
    @XmlElement(nillable = true)
    protected Long objectSeqNo;

    /**
     * Gets the value of the claimNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getClaimNo() {
        return claimNo;
    }

    /**
     * Sets the value of the claimNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setClaimNo(Long value) {
        this.claimNo = value;
    }

    /**
     * Gets the value of the thirdPartyId property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getThirdPartyId() {
        return thirdPartyId;
    }

    /**
     * Sets the value of the thirdPartyId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setThirdPartyId(Long value) {
        this.thirdPartyId = value;
    }

    /**
     * Gets the value of the objectSeqNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getObjectSeqNo() {
        return objectSeqNo;
    }

    /**
     * Sets the value of the objectSeqNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setObjectSeqNo(Long value) {
        this.objectSeqNo = value;
    }

}
