
package za.co.ominsure.synapse.iwyze.lob.backend.policy.vo;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Additional comments
 * 
 * <p>Java class for RiskType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RiskType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://infrastructure.tia.dk/schema/common/v2/}TIAObject">
 *       &lt;sequence>
 *         &lt;element name="riskNo" type="{http://infrastructure.tia.dk/schema/common/v2/}int3" minOccurs="0"/>
 *         &lt;element name="riskNameTranslations" type="{http://infrastructure.tia.dk/schema/common/v2/}ValueTranslationPolicyCollection" minOccurs="0"/>
 *         &lt;element name="defaultRiskTerms" type="{http://infrastructure.tia.dk/schema/common/v2/}string50" minOccurs="0"/>
 *         &lt;element name="defaultRiskExcess" type="{http://infrastructure.tia.dk/schema/common/v2/}decimal30Point2" minOccurs="0"/>
 *         &lt;element name="defaultRiskSum" type="{http://infrastructure.tia.dk/schema/common/v2/}decimal30Point2" minOccurs="0"/>
 *         &lt;element name="defaultRiskDiscount" type="{http://infrastructure.tia.dk/schema/common/v2/}decimal30Point2" minOccurs="0"/>
 *         &lt;element name="riskTermsLov" type="{http://infrastructure.tia.dk/schema/common/v2/}string12" minOccurs="0"/>
 *         &lt;element name="riskExcessLov" type="{http://infrastructure.tia.dk/schema/common/v2/}string12" minOccurs="0"/>
 *         &lt;element name="riskSumLov" type="{http://infrastructure.tia.dk/schema/common/v2/}string12" minOccurs="0"/>
 *         &lt;element name="riskDiscountLov" type="{http://infrastructure.tia.dk/schema/common/v2/}string12" minOccurs="0"/>
 *         &lt;element name="riskType" type="{http://infrastructure.tia.dk/schema/common/v2/}string3" minOccurs="0"/>
 *         &lt;element name="sortNo" type="{http://infrastructure.tia.dk/schema/common/v2/}int3" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RiskType", propOrder = {
    "riskNo",
    "riskNameTranslations",
    "defaultRiskTerms",
    "defaultRiskExcess",
    "defaultRiskSum",
    "defaultRiskDiscount",
    "riskTermsLov",
    "riskExcessLov",
    "riskSumLov",
    "riskDiscountLov",
    "riskType",
    "sortNo"
})
public class RiskType
    extends TIAObject
{

    @XmlElement(nillable = true)
    protected Long riskNo;
    protected ValueTranslationPolicyCollection riskNameTranslations;
    @XmlElement(nillable = true)
    protected String defaultRiskTerms;
    @XmlElement(nillable = true)
    protected BigDecimal defaultRiskExcess;
    @XmlElement(nillable = true)
    protected BigDecimal defaultRiskSum;
    @XmlElement(nillable = true)
    protected BigDecimal defaultRiskDiscount;
    @XmlElement(nillable = true)
    protected String riskTermsLov;
    @XmlElement(nillable = true)
    protected String riskExcessLov;
    @XmlElement(nillable = true)
    protected String riskSumLov;
    @XmlElement(nillable = true)
    protected String riskDiscountLov;
    @XmlElement(nillable = true)
    protected String riskType;
    @XmlElement(nillable = true)
    protected Long sortNo;

    /**
     * Gets the value of the riskNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getRiskNo() {
        return riskNo;
    }

    /**
     * Sets the value of the riskNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setRiskNo(Long value) {
        this.riskNo = value;
    }

    /**
     * Gets the value of the riskNameTranslations property.
     * 
     * @return
     *     possible object is
     *     {@link ValueTranslationPolicyCollection }
     *     
     */
    public ValueTranslationPolicyCollection getRiskNameTranslations() {
        return riskNameTranslations;
    }

    /**
     * Sets the value of the riskNameTranslations property.
     * 
     * @param value
     *     allowed object is
     *     {@link ValueTranslationPolicyCollection }
     *     
     */
    public void setRiskNameTranslations(ValueTranslationPolicyCollection value) {
        this.riskNameTranslations = value;
    }

    /**
     * Gets the value of the defaultRiskTerms property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefaultRiskTerms() {
        return defaultRiskTerms;
    }

    /**
     * Sets the value of the defaultRiskTerms property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefaultRiskTerms(String value) {
        this.defaultRiskTerms = value;
    }

    /**
     * Gets the value of the defaultRiskExcess property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDefaultRiskExcess() {
        return defaultRiskExcess;
    }

    /**
     * Sets the value of the defaultRiskExcess property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDefaultRiskExcess(BigDecimal value) {
        this.defaultRiskExcess = value;
    }

    /**
     * Gets the value of the defaultRiskSum property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDefaultRiskSum() {
        return defaultRiskSum;
    }

    /**
     * Sets the value of the defaultRiskSum property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDefaultRiskSum(BigDecimal value) {
        this.defaultRiskSum = value;
    }

    /**
     * Gets the value of the defaultRiskDiscount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDefaultRiskDiscount() {
        return defaultRiskDiscount;
    }

    /**
     * Sets the value of the defaultRiskDiscount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDefaultRiskDiscount(BigDecimal value) {
        this.defaultRiskDiscount = value;
    }

    /**
     * Gets the value of the riskTermsLov property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRiskTermsLov() {
        return riskTermsLov;
    }

    /**
     * Sets the value of the riskTermsLov property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRiskTermsLov(String value) {
        this.riskTermsLov = value;
    }

    /**
     * Gets the value of the riskExcessLov property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRiskExcessLov() {
        return riskExcessLov;
    }

    /**
     * Sets the value of the riskExcessLov property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRiskExcessLov(String value) {
        this.riskExcessLov = value;
    }

    /**
     * Gets the value of the riskSumLov property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRiskSumLov() {
        return riskSumLov;
    }

    /**
     * Sets the value of the riskSumLov property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRiskSumLov(String value) {
        this.riskSumLov = value;
    }

    /**
     * Gets the value of the riskDiscountLov property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRiskDiscountLov() {
        return riskDiscountLov;
    }

    /**
     * Sets the value of the riskDiscountLov property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRiskDiscountLov(String value) {
        this.riskDiscountLov = value;
    }

    /**
     * Gets the value of the riskType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRiskType() {
        return riskType;
    }

    /**
     * Sets the value of the riskType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRiskType(String value) {
        this.riskType = value;
    }

    /**
     * Gets the value of the sortNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getSortNo() {
        return sortNo;
    }

    /**
     * Sets the value of the sortNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setSortNo(Long value) {
        this.sortNo = value;
    }

}
