
package za.co.ominsure.synapse.iwyze.lob.backend.policy.vo;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MarketingCampaignShortCollection complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MarketingCampaignShortCollection">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="marketingCampaignShort" type="{http://infrastructure.tia.dk/schema/policy/v2/}MarketingCampaignShort" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MarketingCampaignShortCollection", propOrder = {
    "marketingCampaignShorts"
})
public class MarketingCampaignShortCollection {

    @XmlElement(name = "marketingCampaignShort")
    protected List<MarketingCampaignShort> marketingCampaignShorts;

    /**
     * Gets the value of the marketingCampaignShorts property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the marketingCampaignShorts property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMarketingCampaignShorts().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MarketingCampaignShort }
     * 
     * 
     */
    public List<MarketingCampaignShort> getMarketingCampaignShorts() {
        if (marketingCampaignShorts == null) {
            marketingCampaignShorts = new ArrayList<MarketingCampaignShort>();
        }
        return this.marketingCampaignShorts;
    }

}
