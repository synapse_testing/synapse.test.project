
package za.co.ominsure.synapse.iwyze.lob.backend.servicesupplier.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Attribute complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Attribute">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element name="varcharAttribute" type="{http://infrastructure.tia.dk/schema/common/v2/}VarcharAttribute"/>
 *         &lt;element name="numberAttribute" type="{http://infrastructure.tia.dk/schema/common/v2/}NumberAttribute"/>
 *         &lt;element name="dateAttribute" type="{http://infrastructure.tia.dk/schema/common/v2/}DateAttribute"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Attribute", namespace = "http://infrastructure.tia.dk/schema/common/v2/", propOrder = {
    "varcharAttribute",
    "numberAttribute",
    "dateAttribute"
})
@XmlSeeAlso({
    Flexfield.class
})
public class Attribute {

    protected VarcharAttribute varcharAttribute;
    protected NumberAttribute numberAttribute;
    protected DateAttribute dateAttribute;

    /**
     * Gets the value of the varcharAttribute property.
     * 
     * @return
     *     possible object is
     *     {@link VarcharAttribute }
     *     
     */
    public VarcharAttribute getVarcharAttribute() {
        return varcharAttribute;
    }

    /**
     * Sets the value of the varcharAttribute property.
     * 
     * @param value
     *     allowed object is
     *     {@link VarcharAttribute }
     *     
     */
    public void setVarcharAttribute(VarcharAttribute value) {
        this.varcharAttribute = value;
    }

    /**
     * Gets the value of the numberAttribute property.
     * 
     * @return
     *     possible object is
     *     {@link NumberAttribute }
     *     
     */
    public NumberAttribute getNumberAttribute() {
        return numberAttribute;
    }

    /**
     * Sets the value of the numberAttribute property.
     * 
     * @param value
     *     allowed object is
     *     {@link NumberAttribute }
     *     
     */
    public void setNumberAttribute(NumberAttribute value) {
        this.numberAttribute = value;
    }

    /**
     * Gets the value of the dateAttribute property.
     * 
     * @return
     *     possible object is
     *     {@link DateAttribute }
     *     
     */
    public DateAttribute getDateAttribute() {
        return dateAttribute;
    }

    /**
     * Sets the value of the dateAttribute property.
     * 
     * @param value
     *     allowed object is
     *     {@link DateAttribute }
     *     
     */
    public void setDateAttribute(DateAttribute value) {
        this.dateAttribute = value;
    }

}
