
package za.co.ominsure.synapse.iwyze.lob.backend.policy.vo;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for filterOption.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="filterOption">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="NO_CAN_OBJ"/>
 *     &lt;enumeration value="NO_CAN_PL"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "filterOption")
@XmlEnum
public enum FilterOption {

    NO_CAN_OBJ,
    NO_CAN_PL;

    public String value() {
        return name();
    }

    public static FilterOption fromValue(String v) {
        return valueOf(v);
    }

}
