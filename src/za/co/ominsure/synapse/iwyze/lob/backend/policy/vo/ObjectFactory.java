
package za.co.ominsure.synapse.iwyze.lob.backend.policy.vo;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the za.co.ominsure.synapse.iwyze.lob.backend.policy.vo package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: za.co.ominsure.synapse.iwyze.lob.backend.policy.vo
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetPolicyHistoryRequest }
     * 
     */
    public GetPolicyHistoryRequest createGetPolicyHistoryRequest() {
        return new GetPolicyHistoryRequest();
    }

    /**
     * Create an instance of {@link InputToken }
     * 
     */
    public InputToken createInputToken() {
        return new InputToken();
    }

    /**
     * Create an instance of {@link GetPolicyHistoryResponse }
     * 
     */
    public GetPolicyHistoryResponse createGetPolicyHistoryResponse() {
        return new GetPolicyHistoryResponse();
    }

    /**
     * Create an instance of {@link PolicyHistory }
     * 
     */
    public PolicyHistory createPolicyHistory() {
        return new PolicyHistory();
    }

    /**
     * Create an instance of {@link Result }
     * 
     */
    public Result createResult() {
        return new Result();
    }

    /**
     * Create an instance of {@link GetPolicyByAltIdRequest }
     * 
     */
    public GetPolicyByAltIdRequest createGetPolicyByAltIdRequest() {
        return new GetPolicyByAltIdRequest();
    }

    /**
     * Create an instance of {@link PolicyLineNoCollection }
     * 
     */
    public PolicyLineNoCollection createPolicyLineNoCollection() {
        return new PolicyLineNoCollection();
    }

    /**
     * Create an instance of {@link PageSort }
     * 
     */
    public PageSort createPageSort() {
        return new PageSort();
    }

    /**
     * Create an instance of {@link FilterOptionCollection }
     * 
     */
    public FilterOptionCollection createFilterOptionCollection() {
        return new FilterOptionCollection();
    }

    /**
     * Create an instance of {@link CreateQuoteOnPolicyResponse }
     * 
     */
    public CreateQuoteOnPolicyResponse createCreateQuoteOnPolicyResponse() {
        return new CreateQuoteOnPolicyResponse();
    }

    /**
     * Create an instance of {@link Policy }
     * 
     */
    public Policy createPolicy() {
        return new Policy();
    }

    /**
     * Create an instance of {@link GetCustomerPoliciesRequest }
     * 
     */
    public GetCustomerPoliciesRequest createGetCustomerPoliciesRequest() {
        return new GetCustomerPoliciesRequest();
    }

    /**
     * Create an instance of {@link RequestedStateCollection }
     * 
     */
    public RequestedStateCollection createRequestedStateCollection() {
        return new RequestedStateCollection();
    }

    /**
     * Create an instance of {@link GetPolicyRequest }
     * 
     */
    public GetPolicyRequest createGetPolicyRequest() {
        return new GetPolicyRequest();
    }

    /**
     * Create an instance of {@link ConvertQuoteToPolicyRequest }
     * 
     */
    public ConvertQuoteToPolicyRequest createConvertQuoteToPolicyRequest() {
        return new ConvertQuoteToPolicyRequest();
    }

    /**
     * Create an instance of {@link GetPolicyLineRequest }
     * 
     */
    public GetPolicyLineRequest createGetPolicyLineRequest() {
        return new GetPolicyLineRequest();
    }

    /**
     * Create an instance of {@link ObjectNoCollection }
     * 
     */
    public ObjectNoCollection createObjectNoCollection() {
        return new ObjectNoCollection();
    }

    /**
     * Create an instance of {@link SearchPolicyResponse }
     * 
     */
    public SearchPolicyResponse createSearchPolicyResponse() {
        return new SearchPolicyResponse();
    }

    /**
     * Create an instance of {@link PolicySearchResultCollection }
     * 
     */
    public PolicySearchResultCollection createPolicySearchResultCollection() {
        return new PolicySearchResultCollection();
    }

    /**
     * Create an instance of {@link GetCustomerPoliciesResponse }
     * 
     */
    public GetCustomerPoliciesResponse createGetCustomerPoliciesResponse() {
        return new GetCustomerPoliciesResponse();
    }

    /**
     * Create an instance of {@link PolicyCollection }
     * 
     */
    public PolicyCollection createPolicyCollection() {
        return new PolicyCollection();
    }

    /**
     * Create an instance of {@link ValidatePolicyRequest }
     * 
     */
    public ValidatePolicyRequest createValidatePolicyRequest() {
        return new ValidatePolicyRequest();
    }

    /**
     * Create an instance of {@link CreatePolicyResponse }
     * 
     */
    public CreatePolicyResponse createCreatePolicyResponse() {
        return new CreatePolicyResponse();
    }

    /**
     * Create an instance of {@link MergeQuoteOnPolicyToPolicyResponse }
     * 
     */
    public MergeQuoteOnPolicyToPolicyResponse createMergeQuoteOnPolicyToPolicyResponse() {
        return new MergeQuoteOnPolicyToPolicyResponse();
    }

    /**
     * Create an instance of {@link GetPolicyLineHistoryResponse }
     * 
     */
    public GetPolicyLineHistoryResponse createGetPolicyLineHistoryResponse() {
        return new GetPolicyLineHistoryResponse();
    }

    /**
     * Create an instance of {@link PolicyLineHistory }
     * 
     */
    public PolicyLineHistory createPolicyLineHistory() {
        return new PolicyLineHistory();
    }

    /**
     * Create an instance of {@link CreateQuoteOnPolicyRequest }
     * 
     */
    public CreateQuoteOnPolicyRequest createCreateQuoteOnPolicyRequest() {
        return new CreateQuoteOnPolicyRequest();
    }

    /**
     * Create an instance of {@link ModifyPolicyResponse }
     * 
     */
    public ModifyPolicyResponse createModifyPolicyResponse() {
        return new ModifyPolicyResponse();
    }

    /**
     * Create an instance of {@link ModifyPolicyRequest }
     * 
     */
    public ModifyPolicyRequest createModifyPolicyRequest() {
        return new ModifyPolicyRequest();
    }

    /**
     * Create an instance of {@link ModifyQuoteResponse }
     * 
     */
    public ModifyQuoteResponse createModifyQuoteResponse() {
        return new ModifyQuoteResponse();
    }

    /**
     * Create an instance of {@link GetPolicyLineResponse }
     * 
     */
    public GetPolicyLineResponse createGetPolicyLineResponse() {
        return new GetPolicyLineResponse();
    }

    /**
     * Create an instance of {@link PolicyLine }
     * 
     */
    public PolicyLine createPolicyLine() {
        return new PolicyLine();
    }

    /**
     * Create an instance of {@link CalculatePremiumResponse }
     * 
     */
    public CalculatePremiumResponse createCalculatePremiumResponse() {
        return new CalculatePremiumResponse();
    }

    /**
     * Create an instance of {@link CalculatePremiumRequest }
     * 
     */
    public CalculatePremiumRequest createCalculatePremiumRequest() {
        return new CalculatePremiumRequest();
    }

    /**
     * Create an instance of {@link SearchPolicyRequest }
     * 
     */
    public SearchPolicyRequest createSearchPolicyRequest() {
        return new SearchPolicyRequest();
    }

    /**
     * Create an instance of {@link SearchTokenPolicy }
     * 
     */
    public SearchTokenPolicy createSearchTokenPolicy() {
        return new SearchTokenPolicy();
    }

    /**
     * Create an instance of {@link CreateQuoteRequest }
     * 
     */
    public CreateQuoteRequest createCreateQuoteRequest() {
        return new CreateQuoteRequest();
    }

    /**
     * Create an instance of {@link ConvertQuoteToPolicyResponse }
     * 
     */
    public ConvertQuoteToPolicyResponse createConvertQuoteToPolicyResponse() {
        return new ConvertQuoteToPolicyResponse();
    }

    /**
     * Create an instance of {@link CreatePolicyRequest }
     * 
     */
    public CreatePolicyRequest createCreatePolicyRequest() {
        return new CreatePolicyRequest();
    }

    /**
     * Create an instance of {@link ModifyQuoteRequest }
     * 
     */
    public ModifyQuoteRequest createModifyQuoteRequest() {
        return new ModifyQuoteRequest();
    }

    /**
     * Create an instance of {@link GetPolicyResponse }
     * 
     */
    public GetPolicyResponse createGetPolicyResponse() {
        return new GetPolicyResponse();
    }

    /**
     * Create an instance of {@link GetPolicyLineHistoryRequest }
     * 
     */
    public GetPolicyLineHistoryRequest createGetPolicyLineHistoryRequest() {
        return new GetPolicyLineHistoryRequest();
    }

    /**
     * Create an instance of {@link CreateQuoteResponse }
     * 
     */
    public CreateQuoteResponse createCreateQuoteResponse() {
        return new CreateQuoteResponse();
    }

    /**
     * Create an instance of {@link MergeQuoteOnPolicyToPolicyRequest }
     * 
     */
    public MergeQuoteOnPolicyToPolicyRequest createMergeQuoteOnPolicyToPolicyRequest() {
        return new MergeQuoteOnPolicyToPolicyRequest();
    }

    /**
     * Create an instance of {@link ValidatePolicyResponse }
     * 
     */
    public ValidatePolicyResponse createValidatePolicyResponse() {
        return new ValidatePolicyResponse();
    }

    /**
     * Create an instance of {@link PolicyAgent }
     * 
     */
    public PolicyAgent createPolicyAgent() {
        return new PolicyAgent();
    }

    /**
     * Create an instance of {@link TariffBreakdown }
     * 
     */
    public TariffBreakdown createTariffBreakdown() {
        return new TariffBreakdown();
    }

    /**
     * Create an instance of {@link CustomerBasket }
     * 
     */
    public CustomerBasket createCustomerBasket() {
        return new CustomerBasket();
    }

    /**
     * Create an instance of {@link Risk }
     * 
     */
    public Risk createRisk() {
        return new Risk();
    }

    /**
     * Create an instance of {@link UIConfigObject }
     * 
     */
    public UIConfigObject createUIConfigObject() {
        return new UIConfigObject();
    }

    /**
     * Create an instance of {@link UIConfigRiskCollection }
     * 
     */
    public UIConfigRiskCollection createUIConfigRiskCollection() {
        return new UIConfigRiskCollection();
    }

    /**
     * Create an instance of {@link PolicyLineCollection }
     * 
     */
    public PolicyLineCollection createPolicyLineCollection() {
        return new PolicyLineCollection();
    }

    /**
     * Create an instance of {@link UIConfigRiskItem }
     * 
     */
    public UIConfigRiskItem createUIConfigRiskItem() {
        return new UIConfigRiskItem();
    }

    /**
     * Create an instance of {@link RiskCollection }
     * 
     */
    public RiskCollection createRiskCollection() {
        return new RiskCollection();
    }

    /**
     * Create an instance of {@link CustomerBasketCollection }
     * 
     */
    public CustomerBasketCollection createCustomerBasketCollection() {
        return new CustomerBasketCollection();
    }

    /**
     * Create an instance of {@link PolicyAgentCollection }
     * 
     */
    public PolicyAgentCollection createPolicyAgentCollection() {
        return new PolicyAgentCollection();
    }

    /**
     * Create an instance of {@link ObjectSlave }
     * 
     */
    public ObjectSlave createObjectSlave() {
        return new ObjectSlave();
    }

    /**
     * Create an instance of {@link PolicyLineObjectCollection }
     * 
     */
    public PolicyLineObjectCollection createPolicyLineObjectCollection() {
        return new PolicyLineObjectCollection();
    }

    /**
     * Create an instance of {@link PolicyLineObject }
     * 
     */
    public PolicyLineObject createPolicyLineObject() {
        return new PolicyLineObject();
    }

    /**
     * Create an instance of {@link UIConfigObjectCollection }
     * 
     */
    public UIConfigObjectCollection createUIConfigObjectCollection() {
        return new UIConfigObjectCollection();
    }

    /**
     * Create an instance of {@link UIConfiguration }
     * 
     */
    public UIConfiguration createUIConfiguration() {
        return new UIConfiguration();
    }

    /**
     * Create an instance of {@link UIConfigBlockCollection }
     * 
     */
    public UIConfigBlockCollection createUIConfigBlockCollection() {
        return new UIConfigBlockCollection();
    }

    /**
     * Create an instance of {@link ClauseCollection }
     * 
     */
    public ClauseCollection createClauseCollection() {
        return new ClauseCollection();
    }

    /**
     * Create an instance of {@link UIConfigBlock }
     * 
     */
    public UIConfigBlock createUIConfigBlock() {
        return new UIConfigBlock();
    }

    /**
     * Create an instance of {@link UIConfigRisk }
     * 
     */
    public UIConfigRisk createUIConfigRisk() {
        return new UIConfigRisk();
    }

    /**
     * Create an instance of {@link TariffBreakdownCollection }
     * 
     */
    public TariffBreakdownCollection createTariffBreakdownCollection() {
        return new TariffBreakdownCollection();
    }

    /**
     * Create an instance of {@link UIConfigRiskItemCollection }
     * 
     */
    public UIConfigRiskItemCollection createUIConfigRiskItemCollection() {
        return new UIConfigRiskItemCollection();
    }

    /**
     * Create an instance of {@link Clause }
     * 
     */
    public Clause createClause() {
        return new Clause();
    }

    /**
     * Create an instance of {@link ObjectSlaveCollection }
     * 
     */
    public ObjectSlaveCollection createObjectSlaveCollection() {
        return new ObjectSlaveCollection();
    }

    /**
     * Create an instance of {@link PolicySearchResult }
     * 
     */
    public PolicySearchResult createPolicySearchResult() {
        return new PolicySearchResult();
    }

    /**
     * Create an instance of {@link FaultMessageDetail }
     * 
     */
    public FaultMessageDetail createFaultMessageDetail() {
        return new FaultMessageDetail();
    }

    /**
     * Create an instance of {@link FlexfieldCollection }
     * 
     */
    public FlexfieldCollection createFlexfieldCollection() {
        return new FlexfieldCollection();
    }

    /**
     * Create an instance of {@link ValueTranslationPolicy }
     * 
     */
    public ValueTranslationPolicy createValueTranslationPolicy() {
        return new ValueTranslationPolicy();
    }

    /**
     * Create an instance of {@link NumberAttribute }
     * 
     */
    public NumberAttribute createNumberAttribute() {
        return new NumberAttribute();
    }

    /**
     * Create an instance of {@link KeyPair }
     * 
     */
    public KeyPair createKeyPair() {
        return new KeyPair();
    }

    /**
     * Create an instance of {@link TranslatedCode }
     * 
     */
    public TranslatedCode createTranslatedCode() {
        return new TranslatedCode();
    }

    /**
     * Create an instance of {@link ValueTranslationCollection }
     * 
     */
    public ValueTranslationCollection createValueTranslationCollection() {
        return new ValueTranslationCollection();
    }

    /**
     * Create an instance of {@link VersionToken }
     * 
     */
    public VersionToken createVersionToken() {
        return new VersionToken();
    }

    /**
     * Create an instance of {@link StringCollection }
     * 
     */
    public StringCollection createStringCollection() {
        return new StringCollection();
    }

    /**
     * Create an instance of {@link Attribute }
     * 
     */
    public Attribute createAttribute() {
        return new Attribute();
    }

    /**
     * Create an instance of {@link ValueTranslationClaim }
     * 
     */
    public ValueTranslationClaim createValueTranslationClaim() {
        return new ValueTranslationClaim();
    }

    /**
     * Create an instance of {@link VarcharAttribute }
     * 
     */
    public VarcharAttribute createVarcharAttribute() {
        return new VarcharAttribute();
    }

    /**
     * Create an instance of {@link MessageDetail }
     * 
     */
    public MessageDetail createMessageDetail() {
        return new MessageDetail();
    }

    /**
     * Create an instance of {@link KeyPairCollection }
     * 
     */
    public KeyPairCollection createKeyPairCollection() {
        return new KeyPairCollection();
    }

    /**
     * Create an instance of {@link Email }
     * 
     */
    public Email createEmail() {
        return new Email();
    }

    /**
     * Create an instance of {@link TranslatedCodeCollection }
     * 
     */
    public TranslatedCodeCollection createTranslatedCodeCollection() {
        return new TranslatedCodeCollection();
    }

    /**
     * Create an instance of {@link Flexfield }
     * 
     */
    public Flexfield createFlexfield() {
        return new Flexfield();
    }

    /**
     * Create an instance of {@link DateAttribute }
     * 
     */
    public DateAttribute createDateAttribute() {
        return new DateAttribute();
    }

    /**
     * Create an instance of {@link NumberCollection }
     * 
     */
    public NumberCollection createNumberCollection() {
        return new NumberCollection();
    }

    /**
     * Create an instance of {@link Value }
     * 
     */
    public Value createValue() {
        return new Value();
    }

    /**
     * Create an instance of {@link SiteSpecifics }
     * 
     */
    public SiteSpecifics createSiteSpecifics() {
        return new SiteSpecifics();
    }

    /**
     * Create an instance of {@link ValueTranslationPolicyCollection }
     * 
     */
    public ValueTranslationPolicyCollection createValueTranslationPolicyCollection() {
        return new ValueTranslationPolicyCollection();
    }

    /**
     * Create an instance of {@link ValueTranslation }
     * 
     */
    public ValueTranslation createValueTranslation() {
        return new ValueTranslation();
    }

    /**
     * Create an instance of {@link MessageCollection }
     * 
     */
    public MessageCollection createMessageCollection() {
        return new MessageCollection();
    }

    /**
     * Create an instance of {@link Number }
     * 
     */
    public Number createNumber() {
        return new Number();
    }

    /**
     * Create an instance of {@link CountrySpecifics }
     * 
     */
    public CountrySpecifics createCountrySpecifics() {
        return new CountrySpecifics();
    }

    /**
     * Create an instance of {@link EntityAttributeCollection }
     * 
     */
    public EntityAttributeCollection createEntityAttributeCollection() {
        return new EntityAttributeCollection();
    }

    /**
     * Create an instance of {@link ValueTranslationClaimCollection }
     * 
     */
    public ValueTranslationClaimCollection createValueTranslationClaimCollection() {
        return new ValueTranslationClaimCollection();
    }

    /**
     * Create an instance of {@link PolicyAgent2 }
     * 
     */
    public PolicyAgent2 createPolicyAgent2() {
        return new PolicyAgent2();
    }

    /**
     * Create an instance of {@link PolicyLineShortCollection }
     * 
     */
    public PolicyLineShortCollection createPolicyLineShortCollection() {
        return new PolicyLineShortCollection();
    }

    /**
     * Create an instance of {@link InterestedParty }
     * 
     */
    public InterestedParty createInterestedParty() {
        return new InterestedParty();
    }

    /**
     * Create an instance of {@link ClauseTypeVersion }
     * 
     */
    public ClauseTypeVersion createClauseTypeVersion() {
        return new ClauseTypeVersion();
    }

    /**
     * Create an instance of {@link ClauseTypePlSpecificCollection }
     * 
     */
    public ClauseTypePlSpecificCollection createClauseTypePlSpecificCollection() {
        return new ClauseTypePlSpecificCollection();
    }

    /**
     * Create an instance of {@link Product }
     * 
     */
    public Product createProduct() {
        return new Product();
    }

    /**
     * Create an instance of {@link PolicyLineShort }
     * 
     */
    public PolicyLineShort createPolicyLineShort() {
        return new PolicyLineShort();
    }

    /**
     * Create an instance of {@link PendingPolicyTransShortCollection }
     * 
     */
    public PendingPolicyTransShortCollection createPendingPolicyTransShortCollection() {
        return new PendingPolicyTransShortCollection();
    }

    /**
     * Create an instance of {@link ObjectTypeAttributeCollection }
     * 
     */
    public ObjectTypeAttributeCollection createObjectTypeAttributeCollection() {
        return new ObjectTypeAttributeCollection();
    }

    /**
     * Create an instance of {@link RiskTypeCollection }
     * 
     */
    public RiskTypeCollection createRiskTypeCollection() {
        return new RiskTypeCollection();
    }

    /**
     * Create an instance of {@link PolicyTab }
     * 
     */
    public PolicyTab createPolicyTab() {
        return new PolicyTab();
    }

    /**
     * Create an instance of {@link ProductLine }
     * 
     */
    public ProductLine createProductLine() {
        return new ProductLine();
    }

    /**
     * Create an instance of {@link RiskType }
     * 
     */
    public RiskType createRiskType() {
        return new RiskType();
    }

    /**
     * Create an instance of {@link PolicyCoHolderCollection }
     * 
     */
    public PolicyCoHolderCollection createPolicyCoHolderCollection() {
        return new PolicyCoHolderCollection();
    }

    /**
     * Create an instance of {@link ProdLineDependency }
     * 
     */
    public ProdLineDependency createProdLineDependency() {
        return new ProdLineDependency();
    }

    /**
     * Create an instance of {@link PolicyCoHolder }
     * 
     */
    public PolicyCoHolder createPolicyCoHolder() {
        return new PolicyCoHolder();
    }

    /**
     * Create an instance of {@link CommissionSummaryCollection }
     * 
     */
    public CommissionSummaryCollection createCommissionSummaryCollection() {
        return new CommissionSummaryCollection();
    }

    /**
     * Create an instance of {@link PolicyAgentCollection2 }
     * 
     */
    public PolicyAgentCollection2 createPolicyAgentCollection2() {
        return new PolicyAgentCollection2();
    }

    /**
     * Create an instance of {@link ObjectTypeVersion }
     * 
     */
    public ObjectTypeVersion createObjectTypeVersion() {
        return new ObjectTypeVersion();
    }

    /**
     * Create an instance of {@link RiskDefinition }
     * 
     */
    public RiskDefinition createRiskDefinition() {
        return new RiskDefinition();
    }

    /**
     * Create an instance of {@link ClauseCollection2 }
     * 
     */
    public ClauseCollection2 createClauseCollection2() {
        return new ClauseCollection2();
    }

    /**
     * Create an instance of {@link MarketingCampaignShort }
     * 
     */
    public MarketingCampaignShort createMarketingCampaignShort() {
        return new MarketingCampaignShort();
    }

    /**
     * Create an instance of {@link ObjectSlaveType }
     * 
     */
    public ObjectSlaveType createObjectSlaveType() {
        return new ObjectSlaveType();
    }

    /**
     * Create an instance of {@link ObjectTypeVersionCollection }
     * 
     */
    public ObjectTypeVersionCollection createObjectTypeVersionCollection() {
        return new ObjectTypeVersionCollection();
    }

    /**
     * Create an instance of {@link CommissionItemCollection }
     * 
     */
    public CommissionItemCollection createCommissionItemCollection() {
        return new CommissionItemCollection();
    }

    /**
     * Create an instance of {@link PolicyCollection2 }
     * 
     */
    public PolicyCollection2 createPolicyCollection2() {
        return new PolicyCollection2();
    }

    /**
     * Create an instance of {@link ProductVersion }
     * 
     */
    public ProductVersion createProductVersion() {
        return new ProductVersion();
    }

    /**
     * Create an instance of {@link PolicyLineCollection2 }
     * 
     */
    public PolicyLineCollection2 createPolicyLineCollection2() {
        return new PolicyLineCollection2();
    }

    /**
     * Create an instance of {@link ObjectType }
     * 
     */
    public ObjectType createObjectType() {
        return new ObjectType();
    }

    /**
     * Create an instance of {@link CommissionItem }
     * 
     */
    public CommissionItem createCommissionItem() {
        return new CommissionItem();
    }

    /**
     * Create an instance of {@link ProdLineVerInProduct }
     * 
     */
    public ProdLineVerInProduct createProdLineVerInProduct() {
        return new ProdLineVerInProduct();
    }

    /**
     * Create an instance of {@link RiskTypeGroup }
     * 
     */
    public RiskTypeGroup createRiskTypeGroup() {
        return new RiskTypeGroup();
    }

    /**
     * Create an instance of {@link ObjectSlaveTypePlSpecific }
     * 
     */
    public ObjectSlaveTypePlSpecific createObjectSlaveTypePlSpecific() {
        return new ObjectSlaveTypePlSpecific();
    }

    /**
     * Create an instance of {@link ProductShort }
     * 
     */
    public ProductShort createProductShort() {
        return new ProductShort();
    }

    /**
     * Create an instance of {@link ObjectTypePlSpecificCollection }
     * 
     */
    public ObjectTypePlSpecificCollection createObjectTypePlSpecificCollection() {
        return new ObjectTypePlSpecificCollection();
    }

    /**
     * Create an instance of {@link Policy2 }
     * 
     */
    public Policy2 createPolicy2() {
        return new Policy2();
    }

    /**
     * Create an instance of {@link ProdLineVerInProductCollection }
     * 
     */
    public ProdLineVerInProductCollection createProdLineVerInProductCollection() {
        return new ProdLineVerInProductCollection();
    }

    /**
     * Create an instance of {@link ProductShortCollection }
     * 
     */
    public ProductShortCollection createProductShortCollection() {
        return new ProductShortCollection();
    }

    /**
     * Create an instance of {@link PolicyVersionShort }
     * 
     */
    public PolicyVersionShort createPolicyVersionShort() {
        return new PolicyVersionShort();
    }

    /**
     * Create an instance of {@link ObjectSlaveTypeAttributeCollection }
     * 
     */
    public ObjectSlaveTypeAttributeCollection createObjectSlaveTypeAttributeCollection() {
        return new ObjectSlaveTypeAttributeCollection();
    }

    /**
     * Create an instance of {@link InterestedPartyCollection }
     * 
     */
    public InterestedPartyCollection createInterestedPartyCollection() {
        return new InterestedPartyCollection();
    }

    /**
     * Create an instance of {@link ObjectSlaveTypeAttribute }
     * 
     */
    public ObjectSlaveTypeAttribute createObjectSlaveTypeAttribute() {
        return new ObjectSlaveTypeAttribute();
    }

    /**
     * Create an instance of {@link PolicyLineObjectCollection2 }
     * 
     */
    public PolicyLineObjectCollection2 createPolicyLineObjectCollection2() {
        return new PolicyLineObjectCollection2();
    }

    /**
     * Create an instance of {@link MarketingCampaignShortCollection }
     * 
     */
    public MarketingCampaignShortCollection createMarketingCampaignShortCollection() {
        return new MarketingCampaignShortCollection();
    }

    /**
     * Create an instance of {@link PolicyLine2 }
     * 
     */
    public PolicyLine2 createPolicyLine2() {
        return new PolicyLine2();
    }

    /**
     * Create an instance of {@link ObjectTypeAttribute }
     * 
     */
    public ObjectTypeAttribute createObjectTypeAttribute() {
        return new ObjectTypeAttribute();
    }

    /**
     * Create an instance of {@link PolicySearchResult2 }
     * 
     */
    public PolicySearchResult2 createPolicySearchResult2() {
        return new PolicySearchResult2();
    }

    /**
     * Create an instance of {@link ObjectSlaveTypePlSpecificCollection }
     * 
     */
    public ObjectSlaveTypePlSpecificCollection createObjectSlaveTypePlSpecificCollection() {
        return new ObjectSlaveTypePlSpecificCollection();
    }

    /**
     * Create an instance of {@link ProdLineDependencyCollection }
     * 
     */
    public ProdLineDependencyCollection createProdLineDependencyCollection() {
        return new ProdLineDependencyCollection();
    }

    /**
     * Create an instance of {@link InsuranceObjectCollection }
     * 
     */
    public InsuranceObjectCollection createInsuranceObjectCollection() {
        return new InsuranceObjectCollection();
    }

    /**
     * Create an instance of {@link Risk2 }
     * 
     */
    public Risk2 createRisk2() {
        return new Risk2();
    }

    /**
     * Create an instance of {@link ProdLineVerTransition }
     * 
     */
    public ProdLineVerTransition createProdLineVerTransition() {
        return new ProdLineVerTransition();
    }

    /**
     * Create an instance of {@link ObjectSlaveTypeVersionCollection }
     * 
     */
    public ObjectSlaveTypeVersionCollection createObjectSlaveTypeVersionCollection() {
        return new ObjectSlaveTypeVersionCollection();
    }

    /**
     * Create an instance of {@link ProductVersionCollection }
     * 
     */
    public ProductVersionCollection createProductVersionCollection() {
        return new ProductVersionCollection();
    }

    /**
     * Create an instance of {@link SearchTokenPolicy2 }
     * 
     */
    public SearchTokenPolicy2 createSearchTokenPolicy2() {
        return new SearchTokenPolicy2();
    }

    /**
     * Create an instance of {@link InsuranceObject }
     * 
     */
    public InsuranceObject createInsuranceObject() {
        return new InsuranceObject();
    }

    /**
     * Create an instance of {@link ProdLineVerTransitionCollection }
     * 
     */
    public ProdLineVerTransitionCollection createProdLineVerTransitionCollection() {
        return new ProdLineVerTransitionCollection();
    }

    /**
     * Create an instance of {@link PendingPolicyTransShort }
     * 
     */
    public PendingPolicyTransShort createPendingPolicyTransShort() {
        return new PendingPolicyTransShort();
    }

    /**
     * Create an instance of {@link ProductLineVersion }
     * 
     */
    public ProductLineVersion createProductLineVersion() {
        return new ProductLineVersion();
    }

    /**
     * Create an instance of {@link RiskCollection2 }
     * 
     */
    public RiskCollection2 createRiskCollection2() {
        return new RiskCollection2();
    }

    /**
     * Create an instance of {@link ObjectSlaveTypeVersion }
     * 
     */
    public ObjectSlaveTypeVersion createObjectSlaveTypeVersion() {
        return new ObjectSlaveTypeVersion();
    }

    /**
     * Create an instance of {@link ProductLineVersionCollection }
     * 
     */
    public ProductLineVersionCollection createProductLineVersionCollection() {
        return new ProductLineVersionCollection();
    }

    /**
     * Create an instance of {@link PolicyLineObject2 }
     * 
     */
    public PolicyLineObject2 createPolicyLineObject2() {
        return new PolicyLineObject2();
    }

    /**
     * Create an instance of {@link PolicySearchResultCollection2 }
     * 
     */
    public PolicySearchResultCollection2 createPolicySearchResultCollection2() {
        return new PolicySearchResultCollection2();
    }

    /**
     * Create an instance of {@link PolicyVersionShortCollection }
     * 
     */
    public PolicyVersionShortCollection createPolicyVersionShortCollection() {
        return new PolicyVersionShortCollection();
    }

    /**
     * Create an instance of {@link ClauseTypeVersionCollection }
     * 
     */
    public ClauseTypeVersionCollection createClauseTypeVersionCollection() {
        return new ClauseTypeVersionCollection();
    }

    /**
     * Create an instance of {@link RiskTypeGroupCollection }
     * 
     */
    public RiskTypeGroupCollection createRiskTypeGroupCollection() {
        return new RiskTypeGroupCollection();
    }

    /**
     * Create an instance of {@link ClauseTypePlSpecific }
     * 
     */
    public ClauseTypePlSpecific createClauseTypePlSpecific() {
        return new ClauseTypePlSpecific();
    }

    /**
     * Create an instance of {@link CommissionSummary }
     * 
     */
    public CommissionSummary createCommissionSummary() {
        return new CommissionSummary();
    }

    /**
     * Create an instance of {@link ObjectTypePlSpecific }
     * 
     */
    public ObjectTypePlSpecific createObjectTypePlSpecific() {
        return new ObjectTypePlSpecific();
    }

    /**
     * Create an instance of {@link Clause2 }
     * 
     */
    public Clause2 createClause2() {
        return new Clause2();
    }

    /**
     * Create an instance of {@link ClauseType }
     * 
     */
    public ClauseType createClauseType() {
        return new ClauseType();
    }

    /**
     * Create an instance of {@link GetPartyRequest }
     * 
     */
    public GetPartyRequest createGetPartyRequest() {
        return new GetPartyRequest();
    }

    /**
     * Create an instance of {@link CreatePartyResponse }
     * 
     */
    public CreatePartyResponse createCreatePartyResponse() {
        return new CreatePartyResponse();
    }

    /**
     * Create an instance of {@link PartyElement }
     * 
     */
    public PartyElement createPartyElement() {
        return new PartyElement();
    }

    /**
     * Create an instance of {@link ModifyPartyRequest }
     * 
     */
    public ModifyPartyRequest createModifyPartyRequest() {
        return new ModifyPartyRequest();
    }

    /**
     * Create an instance of {@link CreatePartyRequest }
     * 
     */
    public CreatePartyRequest createCreatePartyRequest() {
        return new CreatePartyRequest();
    }

    /**
     * Create an instance of {@link SearchPartyResponse }
     * 
     */
    public SearchPartyResponse createSearchPartyResponse() {
        return new SearchPartyResponse();
    }

    /**
     * Create an instance of {@link PartyCollection }
     * 
     */
    public PartyCollection createPartyCollection() {
        return new PartyCollection();
    }

    /**
     * Create an instance of {@link GetPartyResponse }
     * 
     */
    public GetPartyResponse createGetPartyResponse() {
        return new GetPartyResponse();
    }

    /**
     * Create an instance of {@link ModifyPartyResponse }
     * 
     */
    public ModifyPartyResponse createModifyPartyResponse() {
        return new ModifyPartyResponse();
    }

    /**
     * Create an instance of {@link GetPartyByAltIdRequest }
     * 
     */
    public GetPartyByAltIdRequest createGetPartyByAltIdRequest() {
        return new GetPartyByAltIdRequest();
    }

    /**
     * Create an instance of {@link SearchPartyRequest }
     * 
     */
    public SearchPartyRequest createSearchPartyRequest() {
        return new SearchPartyRequest();
    }

    /**
     * Create an instance of {@link SearchTokenPar }
     * 
     */
    public SearchTokenPar createSearchTokenPar() {
        return new SearchTokenPar();
    }

    /**
     * Create an instance of {@link InsuranceCompany }
     * 
     */
    public InsuranceCompany createInsuranceCompany() {
        return new InsuranceCompany();
    }

    /**
     * Create an instance of {@link Address }
     * 
     */
    public Address createAddress() {
        return new Address();
    }

    /**
     * Create an instance of {@link InterestedParty2 }
     * 
     */
    public InterestedParty2 createInterestedParty2() {
        return new InterestedParty2();
    }

    /**
     * Create an instance of {@link Customer }
     * 
     */
    public Customer createCustomer() {
        return new Customer();
    }

    /**
     * Create an instance of {@link PolicyRelation }
     * 
     */
    public PolicyRelation createPolicyRelation() {
        return new PolicyRelation();
    }

    /**
     * Create an instance of {@link SearchTokenInc }
     * 
     */
    public SearchTokenInc createSearchTokenInc() {
        return new SearchTokenInc();
    }

    /**
     * Create an instance of {@link Institution }
     * 
     */
    public Institution createInstitution() {
        return new Institution();
    }

    /**
     * Create an instance of {@link SearchTokenAff }
     * 
     */
    public SearchTokenAff createSearchTokenAff() {
        return new SearchTokenAff();
    }

    /**
     * Create an instance of {@link ContactInfo }
     * 
     */
    public ContactInfo createContactInfo() {
        return new ContactInfo();
    }

    /**
     * Create an instance of {@link PartyRoleCollection }
     * 
     */
    public PartyRoleCollection createPartyRoleCollection() {
        return new PartyRoleCollection();
    }

    /**
     * Create an instance of {@link PartyRole }
     * 
     */
    public PartyRole createPartyRole() {
        return new PartyRole();
    }

    /**
     * Create an instance of {@link RelationCollection }
     * 
     */
    public RelationCollection createRelationCollection() {
        return new RelationCollection();
    }

    /**
     * Create an instance of {@link Agent }
     * 
     */
    public Agent createAgent() {
        return new Agent();
    }

    /**
     * Create an instance of {@link ClaimEventRelation }
     * 
     */
    public ClaimEventRelation createClaimEventRelation() {
        return new ClaimEventRelation();
    }

    /**
     * Create an instance of {@link PolicyObjectRelation }
     * 
     */
    public PolicyObjectRelation createPolicyObjectRelation() {
        return new PolicyObjectRelation();
    }

    /**
     * Create an instance of {@link ContactInfoCollection }
     * 
     */
    public ContactInfoCollection createContactInfoCollection() {
        return new ContactInfoCollection();
    }

    /**
     * Create an instance of {@link SearchTokenAgt }
     * 
     */
    public SearchTokenAgt createSearchTokenAgt() {
        return new SearchTokenAgt();
    }

    /**
     * Create an instance of {@link Affinity }
     * 
     */
    public Affinity createAffinity() {
        return new Affinity();
    }

    /**
     * Create an instance of {@link SearchTokenBan }
     * 
     */
    public SearchTokenBan createSearchTokenBan() {
        return new SearchTokenBan();
    }

    /**
     * Create an instance of {@link AccountRelation }
     * 
     */
    public AccountRelation createAccountRelation() {
        return new AccountRelation();
    }

    /**
     * Create an instance of {@link Person }
     * 
     */
    public Person createPerson() {
        return new Person();
    }

    /**
     * Create an instance of {@link SearchTokenSsu }
     * 
     */
    public SearchTokenSsu createSearchTokenSsu() {
        return new SearchTokenSsu();
    }

    /**
     * Create an instance of {@link LocationCollection }
     * 
     */
    public LocationCollection createLocationCollection() {
        return new LocationCollection();
    }

    /**
     * Create an instance of {@link Bank }
     * 
     */
    public Bank createBank() {
        return new Bank();
    }

    /**
     * Create an instance of {@link AffinityRelation }
     * 
     */
    public AffinityRelation createAffinityRelation() {
        return new AffinityRelation();
    }

    /**
     * Create an instance of {@link RelationWrapperType }
     * 
     */
    public RelationWrapperType createRelationWrapperType() {
        return new RelationWrapperType();
    }

    /**
     * Create an instance of {@link ServiceSupplier }
     * 
     */
    public ServiceSupplier createServiceSupplier() {
        return new ServiceSupplier();
    }

    /**
     * Create an instance of {@link AddressCollection }
     * 
     */
    public AddressCollection createAddressCollection() {
        return new AddressCollection();
    }

    /**
     * Create an instance of {@link ClaimThirdPartyRelation }
     * 
     */
    public ClaimThirdPartyRelation createClaimThirdPartyRelation() {
        return new ClaimThirdPartyRelation();
    }

    /**
     * Create an instance of {@link ClaimRelation }
     * 
     */
    public ClaimRelation createClaimRelation() {
        return new ClaimRelation();
    }

    /**
     * Create an instance of {@link WorkAreaCollection }
     * 
     */
    public WorkAreaCollection createWorkAreaCollection() {
        return new WorkAreaCollection();
    }

    /**
     * Create an instance of {@link SpecialityCollection }
     * 
     */
    public SpecialityCollection createSpecialityCollection() {
        return new SpecialityCollection();
    }

    /**
     * Create an instance of {@link PolicyLineRelation }
     * 
     */
    public PolicyLineRelation createPolicyLineRelation() {
        return new PolicyLineRelation();
    }

    /**
     * Create an instance of {@link PartyRelation }
     * 
     */
    public PartyRelation createPartyRelation() {
        return new PartyRelation();
    }

    /**
     * Create an instance of {@link SearchTokenLoc }
     * 
     */
    public SearchTokenLoc createSearchTokenLoc() {
        return new SearchTokenLoc();
    }

    /**
     * Create an instance of {@link PartyOther }
     * 
     */
    public PartyOther createPartyOther() {
        return new PartyOther();
    }

    /**
     * Create an instance of {@link SubclaimRelation }
     * 
     */
    public SubclaimRelation createSubclaimRelation() {
        return new SubclaimRelation();
    }

    /**
     * Create an instance of {@link ClaimItemRelation }
     * 
     */
    public ClaimItemRelation createClaimItemRelation() {
        return new ClaimItemRelation();
    }

    /**
     * Create an instance of {@link Location }
     * 
     */
    public Location createLocation() {
        return new Location();
    }

    /**
     * Create an instance of {@link Policy3 }
     * 
     */
    public Policy3 createPolicy3() {
        return new Policy3();
    }

    /**
     * Create an instance of {@link PolicyLineCollection3 }
     * 
     */
    public PolicyLineCollection3 createPolicyLineCollection3() {
        return new PolicyLineCollection3();
    }

    /**
     * Create an instance of {@link PremiumRiskSplit }
     * 
     */
    public PremiumRiskSplit createPremiumRiskSplit() {
        return new PremiumRiskSplit();
    }

    /**
     * Create an instance of {@link PolicyCollection3 }
     * 
     */
    public PolicyCollection3 createPolicyCollection3() {
        return new PolicyCollection3();
    }

    /**
     * Create an instance of {@link PremiumRiskSplitCollection }
     * 
     */
    public PremiumRiskSplitCollection createPremiumRiskSplitCollection() {
        return new PremiumRiskSplitCollection();
    }

    /**
     * Create an instance of {@link PolicyLine3 }
     * 
     */
    public PolicyLine3 createPolicyLine3() {
        return new PolicyLine3();
    }

}
