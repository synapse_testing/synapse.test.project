
package za.co.ominsure.synapse.iwyze.lob.backend.search.payment.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ValueTranslationPolicy complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ValueTranslationPolicy">
 *   &lt;complexContent>
 *     &lt;extension base="{http://infrastructure.tia.dk/schema/common/v2/}TranslatedCode">
 *       &lt;sequence>
 *         &lt;element name="sortNo" type="{http://infrastructure.tia.dk/schema/common/v2/}int3"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ValueTranslationPolicy", namespace = "http://infrastructure.tia.dk/schema/common/v2/", propOrder = {
    "sortNo"
})
public class ValueTranslationPolicy
    extends TranslatedCode
{

    @XmlElement(required = true, type = Long.class, nillable = true)
    protected Long sortNo;

    /**
     * Gets the value of the sortNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getSortNo() {
        return sortNo;
    }

    /**
     * Sets the value of the sortNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setSortNo(Long value) {
        this.sortNo = value;
    }

}
