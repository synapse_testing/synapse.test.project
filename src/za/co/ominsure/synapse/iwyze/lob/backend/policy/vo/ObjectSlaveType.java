
package za.co.ominsure.synapse.iwyze.lob.backend.policy.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Additional comments
 * 
 * <p>Java class for ObjectSlaveType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ObjectSlaveType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="objectSlaveTypeId" type="{http://infrastructure.tia.dk/schema/common/v2/}string12" minOccurs="0"/>
 *         &lt;element name="objectSlaveTypeVersionCollection" type="{http://infrastructure.tia.dk/schema/policy/v2/}ObjectSlaveTypeVersionCollection" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ObjectSlaveType", propOrder = {
    "objectSlaveTypeId",
    "objectSlaveTypeVersionCollection"
})
public class ObjectSlaveType {

    @XmlElement(nillable = true)
    protected String objectSlaveTypeId;
    protected ObjectSlaveTypeVersionCollection objectSlaveTypeVersionCollection;

    /**
     * Gets the value of the objectSlaveTypeId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getObjectSlaveTypeId() {
        return objectSlaveTypeId;
    }

    /**
     * Sets the value of the objectSlaveTypeId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setObjectSlaveTypeId(String value) {
        this.objectSlaveTypeId = value;
    }

    /**
     * Gets the value of the objectSlaveTypeVersionCollection property.
     * 
     * @return
     *     possible object is
     *     {@link ObjectSlaveTypeVersionCollection }
     *     
     */
    public ObjectSlaveTypeVersionCollection getObjectSlaveTypeVersionCollection() {
        return objectSlaveTypeVersionCollection;
    }

    /**
     * Sets the value of the objectSlaveTypeVersionCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjectSlaveTypeVersionCollection }
     *     
     */
    public void setObjectSlaveTypeVersionCollection(ObjectSlaveTypeVersionCollection value) {
        this.objectSlaveTypeVersionCollection = value;
    }

}
