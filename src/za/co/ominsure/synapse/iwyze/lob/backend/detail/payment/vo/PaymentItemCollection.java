
package za.co.ominsure.synapse.iwyze.lob.backend.detail.payment.vo;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Is mapped into TIA metadata type "tab_payment_item".
 * 
 * <p>Java class for PaymentItemCollection complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PaymentItemCollection">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="paymentItem" type="{http://infrastructure.tia.dk/schema/account/v2/}PaymentItem" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PaymentItemCollection", propOrder = {
    "paymentItems"
})
public class PaymentItemCollection {

    @XmlElement(name = "paymentItem")
    protected List<PaymentItem> paymentItems;

    /**
     * Gets the value of the paymentItems property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the paymentItems property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPaymentItems().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PaymentItem }
     * 
     * 
     */
    public List<PaymentItem> getPaymentItems() {
        if (paymentItems == null) {
            paymentItems = new ArrayList<PaymentItem>();
        }
        return this.paymentItems;
    }

}
