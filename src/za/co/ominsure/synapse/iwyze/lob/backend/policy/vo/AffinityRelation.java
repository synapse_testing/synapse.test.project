
package za.co.ominsure.synapse.iwyze.lob.backend.policy.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Additional comments
 * 
 * <p>Java class for AffinityRelation complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AffinityRelation">
 *   &lt;complexContent>
 *     &lt;extension base="{http://infrastructure.tia.dk/schema/party/v2/}Relation">
 *       &lt;sequence>
 *         &lt;element name="ownerNameIdNo" type="{http://infrastructure.tia.dk/schema/common/v2/}long10" minOccurs="0"/>
 *         &lt;element name="affinityNo" type="{http://infrastructure.tia.dk/schema/common/v2/}long10" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AffinityRelation", namespace = "http://infrastructure.tia.dk/schema/party/v2/", propOrder = {
    "ownerNameIdNo",
    "affinityNo"
})
public class AffinityRelation
    extends Relation
{

    @XmlElement(nillable = true)
    protected Long ownerNameIdNo;
    @XmlElement(nillable = true)
    protected Long affinityNo;

    /**
     * Gets the value of the ownerNameIdNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getOwnerNameIdNo() {
        return ownerNameIdNo;
    }

    /**
     * Sets the value of the ownerNameIdNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setOwnerNameIdNo(Long value) {
        this.ownerNameIdNo = value;
    }

    /**
     * Gets the value of the affinityNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getAffinityNo() {
        return affinityNo;
    }

    /**
     * Sets the value of the affinityNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setAffinityNo(Long value) {
        this.affinityNo = value;
    }

}
