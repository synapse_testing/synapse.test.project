
package za.co.ominsure.synapse.iwyze.lob.backend.caseItem.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TIAObject complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TIAObject">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="versionToken" type="{http://infrastructure.tia.dk/schema/common/v2/}VersionToken" minOccurs="0"/>
 *         &lt;element name="countrySpecifics" type="{http://infrastructure.tia.dk/schema/common/v2/}CountrySpecifics" minOccurs="0"/>
 *         &lt;element name="siteSpecifics" type="{http://infrastructure.tia.dk/schema/common/v2/}SiteSpecifics" minOccurs="0"/>
 *         &lt;element name="flexfieldCollection" type="{http://infrastructure.tia.dk/schema/common/v2/}FlexfieldCollection" minOccurs="0"/>
 *         &lt;element name="externalId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TIAObject", propOrder = {
    "versionToken",
    "countrySpecifics",
    "siteSpecifics",
    "flexfieldCollection",
    "externalId"
})
@XmlSeeAlso({
    CaseItem.class,
    CaseItemAttachment.class,
    TranslatedCode.class,
    CaseItem2 .class,
    CaseItemAttachment2 .class
})
public abstract class TIAObject {

    @XmlElement(nillable = true)
    protected VersionToken versionToken;
    @XmlElement(nillable = true)
    protected CountrySpecifics countrySpecifics;
    @XmlElement(nillable = true)
    protected SiteSpecifics siteSpecifics;
    @XmlElement(nillable = true)
    protected FlexfieldCollection flexfieldCollection;
    @XmlElement(nillable = true)
    protected String externalId;

    /**
     * Gets the value of the versionToken property.
     * 
     * @return
     *     possible object is
     *     {@link VersionToken }
     *     
     */
    public VersionToken getVersionToken() {
        return versionToken;
    }

    /**
     * Sets the value of the versionToken property.
     * 
     * @param value
     *     allowed object is
     *     {@link VersionToken }
     *     
     */
    public void setVersionToken(VersionToken value) {
        this.versionToken = value;
    }

    /**
     * Gets the value of the countrySpecifics property.
     * 
     * @return
     *     possible object is
     *     {@link CountrySpecifics }
     *     
     */
    public CountrySpecifics getCountrySpecifics() {
        return countrySpecifics;
    }

    /**
     * Sets the value of the countrySpecifics property.
     * 
     * @param value
     *     allowed object is
     *     {@link CountrySpecifics }
     *     
     */
    public void setCountrySpecifics(CountrySpecifics value) {
        this.countrySpecifics = value;
    }

    /**
     * Gets the value of the siteSpecifics property.
     * 
     * @return
     *     possible object is
     *     {@link SiteSpecifics }
     *     
     */
    public SiteSpecifics getSiteSpecifics() {
        return siteSpecifics;
    }

    /**
     * Sets the value of the siteSpecifics property.
     * 
     * @param value
     *     allowed object is
     *     {@link SiteSpecifics }
     *     
     */
    public void setSiteSpecifics(SiteSpecifics value) {
        this.siteSpecifics = value;
    }

    /**
     * Gets the value of the flexfieldCollection property.
     * 
     * @return
     *     possible object is
     *     {@link FlexfieldCollection }
     *     
     */
    public FlexfieldCollection getFlexfieldCollection() {
        return flexfieldCollection;
    }

    /**
     * Sets the value of the flexfieldCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexfieldCollection }
     *     
     */
    public void setFlexfieldCollection(FlexfieldCollection value) {
        this.flexfieldCollection = value;
    }

    /**
     * Gets the value of the externalId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExternalId() {
        return externalId;
    }

    /**
     * Sets the value of the externalId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExternalId(String value) {
        this.externalId = value;
    }

}
