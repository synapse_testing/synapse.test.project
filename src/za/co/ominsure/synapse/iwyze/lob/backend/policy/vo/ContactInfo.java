
package za.co.ominsure.synapse.iwyze.lob.backend.policy.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Is mapped into TIA metadata type "obj_contact_info".
 * 
 * <p>Java class for ContactInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ContactInfo">
 *   &lt;complexContent>
 *     &lt;extension base="{http://infrastructure.tia.dk/schema/common/v2/}TIAObject">
 *       &lt;sequence>
 *         &lt;element name="contactInfoType" type="{http://infrastructure.tia.dk/schema/party/v2/}contactInfoType" minOccurs="0"/>
 *         &lt;element name="contactInfoDetail" type="{http://infrastructure.tia.dk/schema/party/v2/}contactInfoDetail" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ContactInfo", namespace = "http://infrastructure.tia.dk/schema/party/v2/", propOrder = {
    "contactInfoType",
    "contactInfoDetail"
})
public class ContactInfo
    extends TIAObject
{

    @XmlElement(nillable = true)
    protected String contactInfoType;
    @XmlElement(nillable = true)
    protected String contactInfoDetail;

    /**
     * Gets the value of the contactInfoType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContactInfoType() {
        return contactInfoType;
    }

    /**
     * Sets the value of the contactInfoType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContactInfoType(String value) {
        this.contactInfoType = value;
    }

    /**
     * Gets the value of the contactInfoDetail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContactInfoDetail() {
        return contactInfoDetail;
    }

    /**
     * Sets the value of the contactInfoDetail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContactInfoDetail(String value) {
        this.contactInfoDetail = value;
    }

}
