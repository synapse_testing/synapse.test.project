
package za.co.ominsure.synapse.iwyze.lob.backend.caseItem.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="inputToken" type="{http://infrastructure.tia.dk/schema/common/v2/}InputToken"/>
 *         &lt;element name="requestId" type="{http://infrastructure.tia.dk/schema/common/v2/}requestId"/>
 *         &lt;element name="getAttachmentYN" type="{http://infrastructure.tia.dk/schema/common/v2/}tiaYN" minOccurs="0"/>
 *         &lt;element name="getLookupAttribsYN" type="{http://infrastructure.tia.dk/schema/common/v2/}tiaYN" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "inputToken",
    "requestId",
    "getAttachmentYN",
    "getLookupAttribsYN"
})
@XmlRootElement(name = "getCaseItemRequest", namespace = "http://infrastructure.tia.dk/schema/case/v3/")
public class GetCaseItemRequest {

    @XmlElement(namespace = "http://infrastructure.tia.dk/schema/case/v3/", required = true)
    protected InputToken inputToken;
    @XmlElement(namespace = "http://infrastructure.tia.dk/schema/case/v3/")
    @XmlSchemaType(name = "unsignedLong")
    protected long requestId;
    @XmlElement(namespace = "http://infrastructure.tia.dk/schema/case/v3/")
    protected String getAttachmentYN;
    @XmlElement(namespace = "http://infrastructure.tia.dk/schema/case/v3/")
    protected String getLookupAttribsYN;

    /**
     * Gets the value of the inputToken property.
     * 
     * @return
     *     possible object is
     *     {@link InputToken }
     *     
     */
    public InputToken getInputToken() {
        return inputToken;
    }

    /**
     * Sets the value of the inputToken property.
     * 
     * @param value
     *     allowed object is
     *     {@link InputToken }
     *     
     */
    public void setInputToken(InputToken value) {
        this.inputToken = value;
    }

    /**
     * Gets the value of the requestId property.
     * 
     */
    public long getRequestId() {
        return requestId;
    }

    /**
     * Sets the value of the requestId property.
     * 
     */
    public void setRequestId(long value) {
        this.requestId = value;
    }

    /**
     * Gets the value of the getAttachmentYN property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGetAttachmentYN() {
        return getAttachmentYN;
    }

    /**
     * Sets the value of the getAttachmentYN property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGetAttachmentYN(String value) {
        this.getAttachmentYN = value;
    }

    /**
     * Gets the value of the getLookupAttribsYN property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGetLookupAttribsYN() {
        return getLookupAttribsYN;
    }

    /**
     * Sets the value of the getLookupAttribsYN property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGetLookupAttribsYN(String value) {
        this.getLookupAttribsYN = value;
    }

}
