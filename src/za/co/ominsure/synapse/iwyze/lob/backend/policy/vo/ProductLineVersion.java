
package za.co.ominsure.synapse.iwyze.lob.backend.policy.vo;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Additional comments
 * 
 * <p>Java class for ProductLineVersion complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ProductLineVersion">
 *   &lt;complexContent>
 *     &lt;extension base="{http://infrastructure.tia.dk/schema/common/v2/}TIAObject">
 *       &lt;sequence>
 *         &lt;element name="productLineVerNo" type="{http://infrastructure.tia.dk/schema/common/v2/}decimal9Point3" minOccurs="0"/>
 *         &lt;element name="flatPremiumYN" type="{http://infrastructure.tia.dk/schema/common/v2/}string1" minOccurs="0"/>
 *         &lt;element name="toBeRenewedYN" type="{http://infrastructure.tia.dk/schema/common/v2/}string1" minOccurs="0"/>
 *         &lt;element name="informationOnlyYN" type="{http://infrastructure.tia.dk/schema/common/v2/}string1" minOccurs="0"/>
 *         &lt;element name="temporaryInsurance" type="{http://infrastructure.tia.dk/schema/common/v2/}string1" minOccurs="0"/>
 *         &lt;element name="riskTypeCollection" type="{http://infrastructure.tia.dk/schema/policy/v2/}RiskDefinition" minOccurs="0"/>
 *         &lt;element name="objectTypeCollection" type="{http://infrastructure.tia.dk/schema/policy/v2/}ObjectTypePlSpecificCollection" minOccurs="0"/>
 *         &lt;element name="clauseTypeCollection" type="{http://infrastructure.tia.dk/schema/policy/v2/}ClauseTypePlSpecificCollection" minOccurs="0"/>
 *         &lt;element name="versionTransitions" type="{http://infrastructure.tia.dk/schema/policy/v2/}ProdLineVerTransitionCollection" minOccurs="0"/>
 *         &lt;element name="mainProductLineId" type="{http://infrastructure.tia.dk/schema/common/v2/}string5" minOccurs="0"/>
 *         &lt;element name="mainProductLineVerNo" type="{http://infrastructure.tia.dk/schema/common/v2/}decimal9Point3" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProductLineVersion", propOrder = {
    "productLineVerNo",
    "flatPremiumYN",
    "toBeRenewedYN",
    "informationOnlyYN",
    "temporaryInsurance",
    "riskTypeCollection",
    "objectTypeCollection",
    "clauseTypeCollection",
    "versionTransitions",
    "mainProductLineId",
    "mainProductLineVerNo"
})
public class ProductLineVersion
    extends TIAObject
{

    @XmlElement(nillable = true)
    protected BigDecimal productLineVerNo;
    protected String flatPremiumYN;
    protected String toBeRenewedYN;
    @XmlElement(nillable = true)
    protected String informationOnlyYN;
    @XmlElement(nillable = true)
    protected String temporaryInsurance;
    protected RiskDefinition riskTypeCollection;
    protected ObjectTypePlSpecificCollection objectTypeCollection;
    protected ClauseTypePlSpecificCollection clauseTypeCollection;
    protected ProdLineVerTransitionCollection versionTransitions;
    @XmlElement(nillable = true)
    protected String mainProductLineId;
    @XmlElement(nillable = true)
    protected BigDecimal mainProductLineVerNo;

    /**
     * Gets the value of the productLineVerNo property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getProductLineVerNo() {
        return productLineVerNo;
    }

    /**
     * Sets the value of the productLineVerNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setProductLineVerNo(BigDecimal value) {
        this.productLineVerNo = value;
    }

    /**
     * Gets the value of the flatPremiumYN property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlatPremiumYN() {
        return flatPremiumYN;
    }

    /**
     * Sets the value of the flatPremiumYN property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlatPremiumYN(String value) {
        this.flatPremiumYN = value;
    }

    /**
     * Gets the value of the toBeRenewedYN property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getToBeRenewedYN() {
        return toBeRenewedYN;
    }

    /**
     * Sets the value of the toBeRenewedYN property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setToBeRenewedYN(String value) {
        this.toBeRenewedYN = value;
    }

    /**
     * Gets the value of the informationOnlyYN property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInformationOnlyYN() {
        return informationOnlyYN;
    }

    /**
     * Sets the value of the informationOnlyYN property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInformationOnlyYN(String value) {
        this.informationOnlyYN = value;
    }

    /**
     * Gets the value of the temporaryInsurance property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTemporaryInsurance() {
        return temporaryInsurance;
    }

    /**
     * Sets the value of the temporaryInsurance property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTemporaryInsurance(String value) {
        this.temporaryInsurance = value;
    }

    /**
     * Gets the value of the riskTypeCollection property.
     * 
     * @return
     *     possible object is
     *     {@link RiskDefinition }
     *     
     */
    public RiskDefinition getRiskTypeCollection() {
        return riskTypeCollection;
    }

    /**
     * Sets the value of the riskTypeCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link RiskDefinition }
     *     
     */
    public void setRiskTypeCollection(RiskDefinition value) {
        this.riskTypeCollection = value;
    }

    /**
     * Gets the value of the objectTypeCollection property.
     * 
     * @return
     *     possible object is
     *     {@link ObjectTypePlSpecificCollection }
     *     
     */
    public ObjectTypePlSpecificCollection getObjectTypeCollection() {
        return objectTypeCollection;
    }

    /**
     * Sets the value of the objectTypeCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjectTypePlSpecificCollection }
     *     
     */
    public void setObjectTypeCollection(ObjectTypePlSpecificCollection value) {
        this.objectTypeCollection = value;
    }

    /**
     * Gets the value of the clauseTypeCollection property.
     * 
     * @return
     *     possible object is
     *     {@link ClauseTypePlSpecificCollection }
     *     
     */
    public ClauseTypePlSpecificCollection getClauseTypeCollection() {
        return clauseTypeCollection;
    }

    /**
     * Sets the value of the clauseTypeCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link ClauseTypePlSpecificCollection }
     *     
     */
    public void setClauseTypeCollection(ClauseTypePlSpecificCollection value) {
        this.clauseTypeCollection = value;
    }

    /**
     * Gets the value of the versionTransitions property.
     * 
     * @return
     *     possible object is
     *     {@link ProdLineVerTransitionCollection }
     *     
     */
    public ProdLineVerTransitionCollection getVersionTransitions() {
        return versionTransitions;
    }

    /**
     * Sets the value of the versionTransitions property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProdLineVerTransitionCollection }
     *     
     */
    public void setVersionTransitions(ProdLineVerTransitionCollection value) {
        this.versionTransitions = value;
    }

    /**
     * Gets the value of the mainProductLineId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMainProductLineId() {
        return mainProductLineId;
    }

    /**
     * Sets the value of the mainProductLineId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMainProductLineId(String value) {
        this.mainProductLineId = value;
    }

    /**
     * Gets the value of the mainProductLineVerNo property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMainProductLineVerNo() {
        return mainProductLineVerNo;
    }

    /**
     * Sets the value of the mainProductLineVerNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMainProductLineVerNo(BigDecimal value) {
        this.mainProductLineVerNo = value;
    }

}
