
package za.co.ominsure.synapse.iwyze.lob.backend.search.payment.vo;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * Is mapped into TIA metadata type "obj_collection_request_confirm".
 * 
 * <p>Java class for CollectionRequestConfirm complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CollectionRequestConfirm">
 *   &lt;complexContent>
 *     &lt;extension base="{http://infrastructure.tia.dk/schema/common/v2/}TIAObject">
 *       &lt;sequence>
 *         &lt;element name="paymentNo" type="{http://infrastructure.tia.dk/schema/common/v2/}seqNo"/>
 *         &lt;element name="externalCode" type="{http://infrastructure.tia.dk/schema/account/v2/}externalCode" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CollectionRequestConfirm", propOrder = {
    "paymentNo",
    "externalCode"
})
public class CollectionRequestConfirm
    extends TIAObject
{

    @XmlSchemaType(name = "unsignedLong")
    protected long paymentNo;
    @XmlElementRef(name = "externalCode", namespace = "http://infrastructure.tia.dk/schema/account/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> externalCode;

    /**
     * Gets the value of the paymentNo property.
     * 
     */
    public long getPaymentNo() {
        return paymentNo;
    }

    /**
     * Sets the value of the paymentNo property.
     * 
     */
    public void setPaymentNo(long value) {
        this.paymentNo = value;
    }

    /**
     * Gets the value of the externalCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getExternalCode() {
        return externalCode;
    }

    /**
     * Sets the value of the externalCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setExternalCode(JAXBElement<String> value) {
        this.externalCode = value;
    }

}
