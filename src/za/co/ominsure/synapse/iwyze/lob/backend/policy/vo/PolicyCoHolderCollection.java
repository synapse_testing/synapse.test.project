
package za.co.ominsure.synapse.iwyze.lob.backend.policy.vo;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PolicyCoHolderCollection complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PolicyCoHolderCollection">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="policyCoHolder" type="{http://infrastructure.tia.dk/schema/policy/v2/}PolicyCoHolder" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PolicyCoHolderCollection", propOrder = {
    "policyCoHolders"
})
public class PolicyCoHolderCollection {

    @XmlElement(name = "policyCoHolder")
    protected List<PolicyCoHolder> policyCoHolders;

    /**
     * Gets the value of the policyCoHolders property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the policyCoHolders property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPolicyCoHolders().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PolicyCoHolder }
     * 
     * 
     */
    public List<PolicyCoHolder> getPolicyCoHolders() {
        if (policyCoHolders == null) {
            policyCoHolders = new ArrayList<PolicyCoHolder>();
        }
        return this.policyCoHolders;
    }

}
