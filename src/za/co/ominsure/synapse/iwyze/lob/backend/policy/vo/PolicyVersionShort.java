
package za.co.ominsure.synapse.iwyze.lob.backend.policy.vo;

import java.math.BigDecimal;
import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Is mapped into undefined".
 * 
 * <p>Java class for PolicyVersionShort complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PolicyVersionShort">
 *   &lt;complexContent>
 *     &lt;extension base="{http://infrastructure.tia.dk/schema/common/v2/}TIAObject">
 *       &lt;sequence>
 *         &lt;element name="policySeqNo" type="{http://infrastructure.tia.dk/schema/policy/v2/}seqNo"/>
 *         &lt;element name="coverStartDate" type="{http://infrastructure.tia.dk/schema/common/v2/}date"/>
 *         &lt;element name="coverEndDate" type="{http://infrastructure.tia.dk/schema/common/v2/}date"/>
 *         &lt;element name="renewalDate" type="{http://infrastructure.tia.dk/schema/common/v2/}date"/>
 *         &lt;element name="transactionType" type="{http://infrastructure.tia.dk/schema/policy/v2/}transactionType"/>
 *         &lt;element name="policyYear" type="{http://infrastructure.tia.dk/schema/policy/v2/}policyYear"/>
 *         &lt;element name="policyStatus" type="{http://infrastructure.tia.dk/schema/policy/v2/}policyStatus"/>
 *         &lt;element name="cancelCode" type="{http://infrastructure.tia.dk/schema/policy/v2/}cancelCode"/>
 *         &lt;element name="referToUnderwriter" type="{http://infrastructure.tia.dk/schema/policy/v2/}referToUnderwriter"/>
 *         &lt;element name="newest" type="{http://infrastructure.tia.dk/schema/policy/v2/}newest"/>
 *         &lt;element name="pricePaid" type="{http://infrastructure.tia.dk/schema/policy/v2/}policyAmount"/>
 *         &lt;element name="preSeqNo" type="{http://infrastructure.tia.dk/schema/policy/v2/}seqNo"/>
 *         &lt;element name="sucSeqNo" type="{http://infrastructure.tia.dk/schema/policy/v2/}seqNo"/>
 *         &lt;element name="transId" type="{http://infrastructure.tia.dk/schema/policy/v2/}seqNo"/>
 *         &lt;element name="seqNoQop" type="{http://infrastructure.tia.dk/schema/common/v2/}long10" minOccurs="0"/>
 *         &lt;element name="mpPolicyNo" type="{http://infrastructure.tia.dk/schema/common/v2/}long10" minOccurs="0"/>
 *         &lt;element name="mpTransId" type="{http://infrastructure.tia.dk/schema/common/v2/}long10" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PolicyVersionShort", propOrder = {
    "policySeqNo",
    "coverStartDate",
    "coverEndDate",
    "renewalDate",
    "transactionType",
    "policyYear",
    "policyStatus",
    "cancelCode",
    "referToUnderwriter",
    "newest",
    "pricePaid",
    "preSeqNo",
    "sucSeqNo",
    "transId",
    "seqNoQop",
    "mpPolicyNo",
    "mpTransId"
})
public class PolicyVersionShort
    extends TIAObject
{

    @XmlElement(required = true, nillable = true)
    @XmlSchemaType(name = "unsignedLong")
    protected BigInteger policySeqNo;
    @XmlElement(required = true, nillable = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar coverStartDate;
    @XmlElement(required = true, nillable = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar coverEndDate;
    @XmlElement(required = true, nillable = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar renewalDate;
    @XmlElement(required = true, nillable = true)
    protected String transactionType;
    @XmlElement(required = true, type = Long.class, nillable = true)
    @XmlSchemaType(name = "unsignedInt")
    protected Long policyYear;
    @XmlElement(required = true, nillable = true)
    protected String policyStatus;
    @XmlElement(required = true, type = Long.class, nillable = true)
    @XmlSchemaType(name = "unsignedInt")
    protected Long cancelCode;
    @XmlElement(required = true, type = Long.class, nillable = true)
    @XmlSchemaType(name = "unsignedInt")
    protected Long referToUnderwriter;
    @XmlElement(required = true, nillable = true)
    protected String newest;
    @XmlElement(required = true, nillable = true)
    protected BigDecimal pricePaid;
    @XmlElement(required = true, nillable = true)
    @XmlSchemaType(name = "unsignedLong")
    protected BigInteger preSeqNo;
    @XmlElement(required = true, nillable = true)
    @XmlSchemaType(name = "unsignedLong")
    protected BigInteger sucSeqNo;
    @XmlElement(required = true, nillable = true)
    @XmlSchemaType(name = "unsignedLong")
    protected BigInteger transId;
    @XmlElement(nillable = true)
    protected Long seqNoQop;
    @XmlElement(nillable = true)
    protected Long mpPolicyNo;
    @XmlElement(nillable = true)
    protected Long mpTransId;

    /**
     * Gets the value of the policySeqNo property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getPolicySeqNo() {
        return policySeqNo;
    }

    /**
     * Sets the value of the policySeqNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setPolicySeqNo(BigInteger value) {
        this.policySeqNo = value;
    }

    /**
     * Gets the value of the coverStartDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCoverStartDate() {
        return coverStartDate;
    }

    /**
     * Sets the value of the coverStartDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCoverStartDate(XMLGregorianCalendar value) {
        this.coverStartDate = value;
    }

    /**
     * Gets the value of the coverEndDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCoverEndDate() {
        return coverEndDate;
    }

    /**
     * Sets the value of the coverEndDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCoverEndDate(XMLGregorianCalendar value) {
        this.coverEndDate = value;
    }

    /**
     * Gets the value of the renewalDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getRenewalDate() {
        return renewalDate;
    }

    /**
     * Sets the value of the renewalDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setRenewalDate(XMLGregorianCalendar value) {
        this.renewalDate = value;
    }

    /**
     * Gets the value of the transactionType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionType() {
        return transactionType;
    }

    /**
     * Sets the value of the transactionType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionType(String value) {
        this.transactionType = value;
    }

    /**
     * Gets the value of the policyYear property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getPolicyYear() {
        return policyYear;
    }

    /**
     * Sets the value of the policyYear property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setPolicyYear(Long value) {
        this.policyYear = value;
    }

    /**
     * Gets the value of the policyStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPolicyStatus() {
        return policyStatus;
    }

    /**
     * Sets the value of the policyStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPolicyStatus(String value) {
        this.policyStatus = value;
    }

    /**
     * Gets the value of the cancelCode property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getCancelCode() {
        return cancelCode;
    }

    /**
     * Sets the value of the cancelCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setCancelCode(Long value) {
        this.cancelCode = value;
    }

    /**
     * Gets the value of the referToUnderwriter property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getReferToUnderwriter() {
        return referToUnderwriter;
    }

    /**
     * Sets the value of the referToUnderwriter property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setReferToUnderwriter(Long value) {
        this.referToUnderwriter = value;
    }

    /**
     * Gets the value of the newest property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNewest() {
        return newest;
    }

    /**
     * Sets the value of the newest property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNewest(String value) {
        this.newest = value;
    }

    /**
     * Gets the value of the pricePaid property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPricePaid() {
        return pricePaid;
    }

    /**
     * Sets the value of the pricePaid property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPricePaid(BigDecimal value) {
        this.pricePaid = value;
    }

    /**
     * Gets the value of the preSeqNo property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getPreSeqNo() {
        return preSeqNo;
    }

    /**
     * Sets the value of the preSeqNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setPreSeqNo(BigInteger value) {
        this.preSeqNo = value;
    }

    /**
     * Gets the value of the sucSeqNo property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getSucSeqNo() {
        return sucSeqNo;
    }

    /**
     * Sets the value of the sucSeqNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setSucSeqNo(BigInteger value) {
        this.sucSeqNo = value;
    }

    /**
     * Gets the value of the transId property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getTransId() {
        return transId;
    }

    /**
     * Sets the value of the transId property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setTransId(BigInteger value) {
        this.transId = value;
    }

    /**
     * Gets the value of the seqNoQop property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getSeqNoQop() {
        return seqNoQop;
    }

    /**
     * Sets the value of the seqNoQop property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setSeqNoQop(Long value) {
        this.seqNoQop = value;
    }

    /**
     * Gets the value of the mpPolicyNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getMpPolicyNo() {
        return mpPolicyNo;
    }

    /**
     * Sets the value of the mpPolicyNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setMpPolicyNo(Long value) {
        this.mpPolicyNo = value;
    }

    /**
     * Gets the value of the mpTransId property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getMpTransId() {
        return mpTransId;
    }

    /**
     * Sets the value of the mpTransId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setMpTransId(Long value) {
        this.mpTransId = value;
    }

}
