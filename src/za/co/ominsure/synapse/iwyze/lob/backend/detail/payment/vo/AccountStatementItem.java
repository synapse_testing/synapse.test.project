
package za.co.ominsure.synapse.iwyze.lob.backend.detail.payment.vo;

import java.math.BigDecimal;
import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Is mapped into TIA metadata type "obj_account_statement_item".
 * 
 * <p>Java class for AccountStatementItem complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AccountStatementItem">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="transactionDate" type="{http://infrastructure.tia.dk/schema/common/v2/}date" minOccurs="0"/>
 *         &lt;element name="accountItemNo" type="{http://infrastructure.tia.dk/schema/common/v2/}seqNo" minOccurs="0"/>
 *         &lt;element name="mpPolicyNo" type="{http://infrastructure.tia.dk/schema/common/v2/}long10" minOccurs="0"/>
 *         &lt;element name="policyNo" type="{http://infrastructure.tia.dk/schema/common/v2/}policyNo" minOccurs="0"/>
 *         &lt;element name="policyLineNo" type="{http://infrastructure.tia.dk/schema/common/v2/}policyLineNo" minOccurs="0"/>
 *         &lt;element name="claimNo" type="{http://infrastructure.tia.dk/schema/common/v2/}seqNo" minOccurs="0"/>
 *         &lt;element name="itemText" type="{http://infrastructure.tia.dk/schema/account/v2/}itemText" minOccurs="0"/>
 *         &lt;element name="startDate" type="{http://infrastructure.tia.dk/schema/common/v2/}date" minOccurs="0"/>
 *         &lt;element name="endDate" type="{http://infrastructure.tia.dk/schema/common/v2/}date" minOccurs="0"/>
 *         &lt;element name="paymentStatus" type="{http://infrastructure.tia.dk/schema/account/v2/}paymentStatus" minOccurs="0"/>
 *         &lt;element name="source" type="{http://infrastructure.tia.dk/schema/account/v2/}source" minOccurs="0"/>
 *         &lt;element name="itemId" type="{http://infrastructure.tia.dk/schema/common/v2/}seqNo" minOccurs="0"/>
 *         &lt;element name="itemReference" type="{http://infrastructure.tia.dk/schema/account/v2/}itemReference" minOccurs="0"/>
 *         &lt;element name="specification" type="{http://infrastructure.tia.dk/schema/account/v2/}specificationText" minOccurs="0"/>
 *         &lt;element name="currencyCode" type="{http://infrastructure.tia.dk/schema/common/v2/}currencyCode" minOccurs="0"/>
 *         &lt;element name="amount" type="{http://infrastructure.tia.dk/schema/common/v2/}amount" minOccurs="0"/>
 *         &lt;element name="paidCurrencyCode" type="{http://infrastructure.tia.dk/schema/common/v2/}currencyCode" minOccurs="0"/>
 *         &lt;element name="paidAmount" type="{http://infrastructure.tia.dk/schema/common/v2/}amount" minOccurs="0"/>
 *         &lt;element name="paidAccountItemNo" type="{http://infrastructure.tia.dk/schema/common/v2/}seqNo" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AccountStatementItem", propOrder = {
    "transactionDate",
    "accountItemNo",
    "mpPolicyNo",
    "policyNo",
    "policyLineNo",
    "claimNo",
    "itemText",
    "startDate",
    "endDate",
    "paymentStatus",
    "source",
    "itemId",
    "itemReference",
    "specification",
    "currencyCode",
    "amount",
    "paidCurrencyCode",
    "paidAmount",
    "paidAccountItemNo"
})
public class AccountStatementItem {

    @XmlElement(nillable = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar transactionDate;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "unsignedLong")
    protected Long accountItemNo;
    @XmlElement(nillable = true)
    protected Long mpPolicyNo;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "unsignedLong")
    protected BigInteger policyNo;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "unsignedLong")
    protected BigInteger policyLineNo;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "unsignedLong")
    protected Long claimNo;
    @XmlElement(nillable = true)
    protected String itemText;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar startDate;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar endDate;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "integer")
    protected Integer paymentStatus;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "integer")
    protected Integer source;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "unsignedLong")
    protected Long itemId;
    @XmlElement(nillable = true)
    protected String itemReference;
    @XmlElement(nillable = true)
    protected String specification;
    @XmlElement(nillable = true)
    protected String currencyCode;
    @XmlElement(nillable = true)
    protected BigDecimal amount;
    @XmlElement(nillable = true)
    protected String paidCurrencyCode;
    @XmlElement(nillable = true)
    protected BigDecimal paidAmount;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "unsignedLong")
    protected Long paidAccountItemNo;

    /**
     * Gets the value of the transactionDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTransactionDate() {
        return transactionDate;
    }

    /**
     * Sets the value of the transactionDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTransactionDate(XMLGregorianCalendar value) {
        this.transactionDate = value;
    }

    /**
     * Gets the value of the accountItemNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getAccountItemNo() {
        return accountItemNo;
    }

    /**
     * Sets the value of the accountItemNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setAccountItemNo(Long value) {
        this.accountItemNo = value;
    }

    /**
     * Gets the value of the mpPolicyNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getMpPolicyNo() {
        return mpPolicyNo;
    }

    /**
     * Sets the value of the mpPolicyNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setMpPolicyNo(Long value) {
        this.mpPolicyNo = value;
    }

    /**
     * Gets the value of the policyNo property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getPolicyNo() {
        return policyNo;
    }

    /**
     * Sets the value of the policyNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setPolicyNo(BigInteger value) {
        this.policyNo = value;
    }

    /**
     * Gets the value of the policyLineNo property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getPolicyLineNo() {
        return policyLineNo;
    }

    /**
     * Sets the value of the policyLineNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setPolicyLineNo(BigInteger value) {
        this.policyLineNo = value;
    }

    /**
     * Gets the value of the claimNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getClaimNo() {
        return claimNo;
    }

    /**
     * Sets the value of the claimNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setClaimNo(Long value) {
        this.claimNo = value;
    }

    /**
     * Gets the value of the itemText property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItemText() {
        return itemText;
    }

    /**
     * Sets the value of the itemText property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItemText(String value) {
        this.itemText = value;
    }

    /**
     * Gets the value of the startDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getStartDate() {
        return startDate;
    }

    /**
     * Sets the value of the startDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setStartDate(XMLGregorianCalendar value) {
        this.startDate = value;
    }

    /**
     * Gets the value of the endDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEndDate() {
        return endDate;
    }

    /**
     * Sets the value of the endDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEndDate(XMLGregorianCalendar value) {
        this.endDate = value;
    }

    /**
     * Gets the value of the paymentStatus property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPaymentStatus() {
        return paymentStatus;
    }

    /**
     * Sets the value of the paymentStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPaymentStatus(Integer value) {
        this.paymentStatus = value;
    }

    /**
     * Gets the value of the source property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getSource() {
        return source;
    }

    /**
     * Sets the value of the source property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSource(Integer value) {
        this.source = value;
    }

    /**
     * Gets the value of the itemId property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getItemId() {
        return itemId;
    }

    /**
     * Sets the value of the itemId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setItemId(Long value) {
        this.itemId = value;
    }

    /**
     * Gets the value of the itemReference property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItemReference() {
        return itemReference;
    }

    /**
     * Sets the value of the itemReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItemReference(String value) {
        this.itemReference = value;
    }

    /**
     * Gets the value of the specification property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSpecification() {
        return specification;
    }

    /**
     * Sets the value of the specification property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSpecification(String value) {
        this.specification = value;
    }

    /**
     * Gets the value of the currencyCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrencyCode() {
        return currencyCode;
    }

    /**
     * Sets the value of the currencyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrencyCode(String value) {
        this.currencyCode = value;
    }

    /**
     * Gets the value of the amount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * Sets the value of the amount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAmount(BigDecimal value) {
        this.amount = value;
    }

    /**
     * Gets the value of the paidCurrencyCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaidCurrencyCode() {
        return paidCurrencyCode;
    }

    /**
     * Sets the value of the paidCurrencyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaidCurrencyCode(String value) {
        this.paidCurrencyCode = value;
    }

    /**
     * Gets the value of the paidAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPaidAmount() {
        return paidAmount;
    }

    /**
     * Sets the value of the paidAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPaidAmount(BigDecimal value) {
        this.paidAmount = value;
    }

    /**
     * Gets the value of the paidAccountItemNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getPaidAccountItemNo() {
        return paidAccountItemNo;
    }

    /**
     * Sets the value of the paidAccountItemNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setPaidAccountItemNo(Long value) {
        this.paidAccountItemNo = value;
    }

}
