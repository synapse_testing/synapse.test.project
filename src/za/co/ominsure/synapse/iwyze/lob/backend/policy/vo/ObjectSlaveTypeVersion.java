
package za.co.ominsure.synapse.iwyze.lob.backend.policy.vo;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Additional comments
 * 
 * <p>Java class for ObjectSlaveTypeVersion complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ObjectSlaveTypeVersion">
 *   &lt;complexContent>
 *     &lt;extension base="{http://infrastructure.tia.dk/schema/common/v2/}TIAObject">
 *       &lt;sequence>
 *         &lt;element name="objectSlaveTypeVerNo" type="{http://infrastructure.tia.dk/schema/common/v2/}decimal9Point3" minOccurs="0"/>
 *         &lt;element name="objectSlaveAttributeCollection" type="{http://infrastructure.tia.dk/schema/policy/v2/}ObjectSlaveTypeAttributeCollection" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ObjectSlaveTypeVersion", propOrder = {
    "objectSlaveTypeVerNo",
    "objectSlaveAttributeCollection"
})
public class ObjectSlaveTypeVersion
    extends TIAObject
{

    @XmlElement(nillable = true)
    protected BigDecimal objectSlaveTypeVerNo;
    protected ObjectSlaveTypeAttributeCollection objectSlaveAttributeCollection;

    /**
     * Gets the value of the objectSlaveTypeVerNo property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getObjectSlaveTypeVerNo() {
        return objectSlaveTypeVerNo;
    }

    /**
     * Sets the value of the objectSlaveTypeVerNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setObjectSlaveTypeVerNo(BigDecimal value) {
        this.objectSlaveTypeVerNo = value;
    }

    /**
     * Gets the value of the objectSlaveAttributeCollection property.
     * 
     * @return
     *     possible object is
     *     {@link ObjectSlaveTypeAttributeCollection }
     *     
     */
    public ObjectSlaveTypeAttributeCollection getObjectSlaveAttributeCollection() {
        return objectSlaveAttributeCollection;
    }

    /**
     * Sets the value of the objectSlaveAttributeCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjectSlaveTypeAttributeCollection }
     *     
     */
    public void setObjectSlaveAttributeCollection(ObjectSlaveTypeAttributeCollection value) {
        this.objectSlaveAttributeCollection = value;
    }

}
