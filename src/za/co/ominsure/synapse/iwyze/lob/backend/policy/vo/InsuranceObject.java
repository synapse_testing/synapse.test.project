
package za.co.ominsure.synapse.iwyze.lob.backend.policy.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * Additional comments
 * 
 * <p>Java class for InsuranceObject complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="InsuranceObject">
 *   &lt;complexContent>
 *     &lt;extension base="{http://infrastructure.tia.dk/schema/common/v2/}TIAObject">
 *       &lt;sequence>
 *         &lt;element name="seqNo" type="{http://infrastructure.tia.dk/schema/common/v2/}seqNo"/>
 *         &lt;element name="language" type="{http://infrastructure.tia.dk/schema/common/v2/}language"/>
 *         &lt;element name="objectId" type="{http://infrastructure.tia.dk/schema/common/v2/}varchar2"/>
 *         &lt;element name="objectName" type="{http://infrastructure.tia.dk/schema/common/v2/}varchar2"/>
 *         &lt;element name="prodId" type="{http://infrastructure.tia.dk/schema/common/v2/}varchar2"/>
 *         &lt;element name="image" type="{http://infrastructure.tia.dk/schema/common/v2/}BLOB"/>
 *         &lt;element name="wizardId" type="{http://infrastructure.tia.dk/schema/common/v2/}varchar2"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InsuranceObject", propOrder = {
    "seqNo",
    "language",
    "objectId",
    "objectName",
    "prodId",
    "image",
    "wizardId"
})
public class InsuranceObject
    extends TIAObject
{

    @XmlSchemaType(name = "unsignedLong")
    protected long seqNo;
    @XmlElement(required = true)
    protected String language;
    @XmlElement(required = true)
    protected String objectId;
    @XmlElement(required = true)
    protected String objectName;
    @XmlElement(required = true)
    protected String prodId;
    @XmlElement(required = true)
    protected byte[] image;
    @XmlElement(required = true)
    protected String wizardId;

    /**
     * Gets the value of the seqNo property.
     * 
     */
    public long getSeqNo() {
        return seqNo;
    }

    /**
     * Sets the value of the seqNo property.
     * 
     */
    public void setSeqNo(long value) {
        this.seqNo = value;
    }

    /**
     * Gets the value of the language property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLanguage() {
        return language;
    }

    /**
     * Sets the value of the language property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLanguage(String value) {
        this.language = value;
    }

    /**
     * Gets the value of the objectId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getObjectId() {
        return objectId;
    }

    /**
     * Sets the value of the objectId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setObjectId(String value) {
        this.objectId = value;
    }

    /**
     * Gets the value of the objectName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getObjectName() {
        return objectName;
    }

    /**
     * Sets the value of the objectName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setObjectName(String value) {
        this.objectName = value;
    }

    /**
     * Gets the value of the prodId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProdId() {
        return prodId;
    }

    /**
     * Sets the value of the prodId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProdId(String value) {
        this.prodId = value;
    }

    /**
     * Gets the value of the image property.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getImage() {
        return image;
    }

    /**
     * Sets the value of the image property.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setImage(byte[] value) {
        this.image = value;
    }

    /**
     * Gets the value of the wizardId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWizardId() {
        return wizardId;
    }

    /**
     * Sets the value of the wizardId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWizardId(String value) {
        this.wizardId = value;
    }

}
