
package za.co.ominsure.synapse.iwyze.lob.backend.policy.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * Is mapped into undefined.
 * 
 * <p>Java class for SearchTokenPolicy complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SearchTokenPolicy">
 *   &lt;complexContent>
 *     &lt;extension base="{http://infrastructure.tia.dk/schema/policy/v2/}SearchTokenPolicy">
 *       &lt;sequence>
 *         &lt;element name="prevPolicyNo" type="{http://infrastructure.tia.dk/schema/common/v2/}string20" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SearchTokenPolicy", namespace = "http://infrastructure.tia.dk/schema/policy/v4/", propOrder = {
    "prevPolicyNo"
})
public class SearchTokenPolicy
    extends SearchTokenPolicy2
{

    protected String prevPolicyNo;

    /**
     * Gets the value of the prevPolicyNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrevPolicyNo() {
        return prevPolicyNo;
    }

    /**
     * Sets the value of the prevPolicyNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrevPolicyNo(String value) {
        this.prevPolicyNo = value;
    }

}
