
package za.co.ominsure.synapse.iwyze.lob.backend.party.vo;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * Is mapped into TIA metadata type "obj_address".
 * 
 * <p>Java class for Address complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Address">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="addressType" type="{http://infrastructure.tia.dk/schema/party/v2/}addressType" minOccurs="0"/>
 *         &lt;element name="houseCoName" type="{http://infrastructure.tia.dk/schema/common/v2/}string70" minOccurs="0"/>
 *         &lt;element name="street" type="{http://infrastructure.tia.dk/schema/party/v2/}street" minOccurs="0"/>
 *         &lt;element name="streetNo" type="{http://infrastructure.tia.dk/schema/party/v2/}streetNo" minOccurs="0"/>
 *         &lt;element name="floor" type="{http://infrastructure.tia.dk/schema/party/v2/}floor" minOccurs="0"/>
 *         &lt;element name="floorExt" type="{http://infrastructure.tia.dk/schema/party/v2/}floorExt" minOccurs="0"/>
 *         &lt;element name="postArea" type="{http://infrastructure.tia.dk/schema/party/v2/}postArea" minOccurs="0"/>
 *         &lt;element name="postStreet" type="{http://infrastructure.tia.dk/schema/party/v2/}postStreet" minOccurs="0"/>
 *         &lt;element name="postalRegion" type="{http://infrastructure.tia.dk/schema/party/v2/}postalRegion" minOccurs="0"/>
 *         &lt;element name="city" type="{http://infrastructure.tia.dk/schema/party/v2/}city" minOccurs="0"/>
 *         &lt;element name="country" type="{http://infrastructure.tia.dk/schema/party/v2/}country" minOccurs="0"/>
 *         &lt;element name="countryCode" type="{http://infrastructure.tia.dk/schema/party/v2/}countryCode" minOccurs="0"/>
 *         &lt;element name="county" type="{http://infrastructure.tia.dk/schema/party/v2/}county" minOccurs="0"/>
 *         &lt;element name="addressCode" type="{http://infrastructure.tia.dk/schema/common/v2/}string100" minOccurs="0"/>
 *         &lt;element name="buildingName" type="{http://infrastructure.tia.dk/schema/common/v2/}string50" minOccurs="0"/>
 *         &lt;element name="buildingNumber" type="{http://infrastructure.tia.dk/schema/common/v2/}string20" minOccurs="0"/>
 *         &lt;element name="departmentName" type="{http://infrastructure.tia.dk/schema/common/v2/}string60" minOccurs="0"/>
 *         &lt;element name="dependLocality" type="{http://infrastructure.tia.dk/schema/common/v2/}string35" minOccurs="0"/>
 *         &lt;element name="dependStreet" type="{http://infrastructure.tia.dk/schema/common/v2/}string60" minOccurs="0"/>
 *         &lt;element name="dependStreetDesc" type="{http://infrastructure.tia.dk/schema/common/v2/}string20" minOccurs="0"/>
 *         &lt;element name="poBox" type="{http://infrastructure.tia.dk/schema/common/v2/}string6" minOccurs="0"/>
 *         &lt;element name="streetDescriptor" type="{http://infrastructure.tia.dk/schema/common/v2/}string20" minOccurs="0"/>
 *         &lt;element name="subBuilding" type="{http://infrastructure.tia.dk/schema/common/v2/}string30" minOccurs="0"/>
 *         &lt;element name="unknownAddress" type="{http://infrastructure.tia.dk/schema/common/v2/}tiaYN" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Address", namespace = "http://infrastructure.tia.dk/schema/party/v3/", propOrder = {
    "addressType",
    "houseCoName",
    "street",
    "streetNo",
    "floor",
    "floorExt",
    "postArea",
    "postStreet",
    "postalRegion",
    "city",
    "country",
    "countryCode",
    "county",
    "addressCode",
    "buildingName",
    "buildingNumber",
    "departmentName",
    "dependLocality",
    "dependStreet",
    "dependStreetDesc",
    "poBox",
    "streetDescriptor",
    "subBuilding",
    "unknownAddress"
})
public class Address {

    @XmlElementRef(name = "addressType", namespace = "http://infrastructure.tia.dk/schema/party/v3/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> addressType;
    @XmlElementRef(name = "houseCoName", namespace = "http://infrastructure.tia.dk/schema/party/v3/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> houseCoName;
    @XmlElementRef(name = "street", namespace = "http://infrastructure.tia.dk/schema/party/v3/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> street;
    @XmlElementRef(name = "streetNo", namespace = "http://infrastructure.tia.dk/schema/party/v3/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> streetNo;
    @XmlElementRef(name = "floor", namespace = "http://infrastructure.tia.dk/schema/party/v3/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> floor;
    @XmlElementRef(name = "floorExt", namespace = "http://infrastructure.tia.dk/schema/party/v3/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> floorExt;
    @XmlElementRef(name = "postArea", namespace = "http://infrastructure.tia.dk/schema/party/v3/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> postArea;
    @XmlElementRef(name = "postStreet", namespace = "http://infrastructure.tia.dk/schema/party/v3/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> postStreet;
    @XmlElementRef(name = "postalRegion", namespace = "http://infrastructure.tia.dk/schema/party/v3/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> postalRegion;
    @XmlElementRef(name = "city", namespace = "http://infrastructure.tia.dk/schema/party/v3/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> city;
    @XmlElementRef(name = "country", namespace = "http://infrastructure.tia.dk/schema/party/v3/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> country;
    @XmlElementRef(name = "countryCode", namespace = "http://infrastructure.tia.dk/schema/party/v3/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> countryCode;
    @XmlElementRef(name = "county", namespace = "http://infrastructure.tia.dk/schema/party/v3/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> county;
    @XmlElementRef(name = "addressCode", namespace = "http://infrastructure.tia.dk/schema/party/v3/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> addressCode;
    @XmlElementRef(name = "buildingName", namespace = "http://infrastructure.tia.dk/schema/party/v3/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> buildingName;
    @XmlElementRef(name = "buildingNumber", namespace = "http://infrastructure.tia.dk/schema/party/v3/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> buildingNumber;
    @XmlElementRef(name = "departmentName", namespace = "http://infrastructure.tia.dk/schema/party/v3/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> departmentName;
    @XmlElementRef(name = "dependLocality", namespace = "http://infrastructure.tia.dk/schema/party/v3/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> dependLocality;
    @XmlElementRef(name = "dependStreet", namespace = "http://infrastructure.tia.dk/schema/party/v3/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> dependStreet;
    @XmlElementRef(name = "dependStreetDesc", namespace = "http://infrastructure.tia.dk/schema/party/v3/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> dependStreetDesc;
    @XmlElementRef(name = "poBox", namespace = "http://infrastructure.tia.dk/schema/party/v3/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> poBox;
    @XmlElementRef(name = "streetDescriptor", namespace = "http://infrastructure.tia.dk/schema/party/v3/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> streetDescriptor;
    @XmlElementRef(name = "subBuilding", namespace = "http://infrastructure.tia.dk/schema/party/v3/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> subBuilding;
    @XmlElementRef(name = "unknownAddress", namespace = "http://infrastructure.tia.dk/schema/party/v3/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> unknownAddress;

    /**
     * Gets the value of the addressType property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAddressType() {
        return addressType;
    }

    /**
     * Sets the value of the addressType property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAddressType(JAXBElement<String> value) {
        this.addressType = value;
    }

    /**
     * Gets the value of the houseCoName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getHouseCoName() {
        return houseCoName;
    }

    /**
     * Sets the value of the houseCoName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setHouseCoName(JAXBElement<String> value) {
        this.houseCoName = value;
    }

    /**
     * Gets the value of the street property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getStreet() {
        return street;
    }

    /**
     * Sets the value of the street property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setStreet(JAXBElement<String> value) {
        this.street = value;
    }

    /**
     * Gets the value of the streetNo property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getStreetNo() {
        return streetNo;
    }

    /**
     * Sets the value of the streetNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setStreetNo(JAXBElement<String> value) {
        this.streetNo = value;
    }

    /**
     * Gets the value of the floor property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getFloor() {
        return floor;
    }

    /**
     * Sets the value of the floor property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setFloor(JAXBElement<String> value) {
        this.floor = value;
    }

    /**
     * Gets the value of the floorExt property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getFloorExt() {
        return floorExt;
    }

    /**
     * Sets the value of the floorExt property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setFloorExt(JAXBElement<String> value) {
        this.floorExt = value;
    }

    /**
     * Gets the value of the postArea property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPostArea() {
        return postArea;
    }

    /**
     * Sets the value of the postArea property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPostArea(JAXBElement<String> value) {
        this.postArea = value;
    }

    /**
     * Gets the value of the postStreet property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPostStreet() {
        return postStreet;
    }

    /**
     * Sets the value of the postStreet property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPostStreet(JAXBElement<String> value) {
        this.postStreet = value;
    }

    /**
     * Gets the value of the postalRegion property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPostalRegion() {
        return postalRegion;
    }

    /**
     * Sets the value of the postalRegion property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPostalRegion(JAXBElement<String> value) {
        this.postalRegion = value;
    }

    /**
     * Gets the value of the city property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCity() {
        return city;
    }

    /**
     * Sets the value of the city property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCity(JAXBElement<String> value) {
        this.city = value;
    }

    /**
     * Gets the value of the country property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCountry() {
        return country;
    }

    /**
     * Sets the value of the country property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCountry(JAXBElement<String> value) {
        this.country = value;
    }

    /**
     * Gets the value of the countryCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCountryCode() {
        return countryCode;
    }

    /**
     * Sets the value of the countryCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCountryCode(JAXBElement<String> value) {
        this.countryCode = value;
    }

    /**
     * Gets the value of the county property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCounty() {
        return county;
    }

    /**
     * Sets the value of the county property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCounty(JAXBElement<String> value) {
        this.county = value;
    }

    /**
     * Gets the value of the addressCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAddressCode() {
        return addressCode;
    }

    /**
     * Sets the value of the addressCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAddressCode(JAXBElement<String> value) {
        this.addressCode = value;
    }

    /**
     * Gets the value of the buildingName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getBuildingName() {
        return buildingName;
    }

    /**
     * Sets the value of the buildingName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setBuildingName(JAXBElement<String> value) {
        this.buildingName = value;
    }

    /**
     * Gets the value of the buildingNumber property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getBuildingNumber() {
        return buildingNumber;
    }

    /**
     * Sets the value of the buildingNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setBuildingNumber(JAXBElement<String> value) {
        this.buildingNumber = value;
    }

    /**
     * Gets the value of the departmentName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDepartmentName() {
        return departmentName;
    }

    /**
     * Sets the value of the departmentName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDepartmentName(JAXBElement<String> value) {
        this.departmentName = value;
    }

    /**
     * Gets the value of the dependLocality property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDependLocality() {
        return dependLocality;
    }

    /**
     * Sets the value of the dependLocality property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDependLocality(JAXBElement<String> value) {
        this.dependLocality = value;
    }

    /**
     * Gets the value of the dependStreet property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDependStreet() {
        return dependStreet;
    }

    /**
     * Sets the value of the dependStreet property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDependStreet(JAXBElement<String> value) {
        this.dependStreet = value;
    }

    /**
     * Gets the value of the dependStreetDesc property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDependStreetDesc() {
        return dependStreetDesc;
    }

    /**
     * Sets the value of the dependStreetDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDependStreetDesc(JAXBElement<String> value) {
        this.dependStreetDesc = value;
    }

    /**
     * Gets the value of the poBox property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPoBox() {
        return poBox;
    }

    /**
     * Sets the value of the poBox property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPoBox(JAXBElement<String> value) {
        this.poBox = value;
    }

    /**
     * Gets the value of the streetDescriptor property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getStreetDescriptor() {
        return streetDescriptor;
    }

    /**
     * Sets the value of the streetDescriptor property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setStreetDescriptor(JAXBElement<String> value) {
        this.streetDescriptor = value;
    }

    /**
     * Gets the value of the subBuilding property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSubBuilding() {
        return subBuilding;
    }

    /**
     * Sets the value of the subBuilding property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSubBuilding(JAXBElement<String> value) {
        this.subBuilding = value;
    }

    /**
     * Gets the value of the unknownAddress property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getUnknownAddress() {
        return unknownAddress;
    }

    /**
     * Sets the value of the unknownAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setUnknownAddress(JAXBElement<String> value) {
        this.unknownAddress = value;
    }

}
