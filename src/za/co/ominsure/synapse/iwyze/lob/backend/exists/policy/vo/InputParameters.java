
package za.co.ominsure.synapse.iwyze.lob.backend.exists.policy.vo;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="P_INPUT_TOKEN" type="{http://xmlns.oracle.com/pcbpel/adapter/db/sp/checkExistingPolicyReference}TIA.OBJ_INPUT_TOKEN" minOccurs="0"/>
 *         &lt;element name="P_POLICY_HOLDER_ID" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "pinputtoken",
    "ppolicyholderid"
})
@XmlRootElement(name = "InputParameters")
public class InputParameters {

    @XmlElement(name = "P_INPUT_TOKEN", nillable = true)
    protected TIAOBJINPUTTOKEN pinputtoken;
    @XmlElement(name = "P_POLICY_HOLDER_ID", nillable = true)
    protected BigDecimal ppolicyholderid;

    /**
     * Gets the value of the pinputtoken property.
     * 
     * @return
     *     possible object is
     *     {@link TIAOBJINPUTTOKEN }
     *     
     */
    public TIAOBJINPUTTOKEN getPINPUTTOKEN() {
        return pinputtoken;
    }

    /**
     * Sets the value of the pinputtoken property.
     * 
     * @param value
     *     allowed object is
     *     {@link TIAOBJINPUTTOKEN }
     *     
     */
    public void setPINPUTTOKEN(TIAOBJINPUTTOKEN value) {
        this.pinputtoken = value;
    }

    /**
     * Gets the value of the ppolicyholderid property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPPOLICYHOLDERID() {
        return ppolicyholderid;
    }

    /**
     * Sets the value of the ppolicyholderid property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPPOLICYHOLDERID(BigDecimal value) {
        this.ppolicyholderid = value;
    }

}
