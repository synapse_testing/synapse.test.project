
package za.co.ominsure.synapse.iwyze.lob.backend.policy.vo;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Is mapped into TIA metadata type "tab_party_role".
 * 
 * <p>Java class for PartyRoleCollection complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PartyRoleCollection">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;sequence>
 *           &lt;element name="bank" type="{http://infrastructure.tia.dk/schema/party/v2/}Bank" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;/sequence>
 *         &lt;sequence>
 *           &lt;element name="agent" type="{http://infrastructure.tia.dk/schema/party/v2/}Agent" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;/sequence>
 *         &lt;sequence>
 *           &lt;element name="serviceSupplier" type="{http://infrastructure.tia.dk/schema/party/v2/}ServiceSupplier" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;/sequence>
 *         &lt;sequence>
 *           &lt;element name="insuranceCompany" type="{http://infrastructure.tia.dk/schema/party/v2/}InsuranceCompany" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;/sequence>
 *         &lt;sequence>
 *           &lt;element name="affinity" type="{http://infrastructure.tia.dk/schema/party/v2/}Affinity" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;/sequence>
 *         &lt;sequence>
 *           &lt;element name="interestedParty" type="{http://infrastructure.tia.dk/schema/party/v2/}InterestedParty" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;/sequence>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PartyRoleCollection", namespace = "http://infrastructure.tia.dk/schema/party/v2/", propOrder = {
    "interestedParties",
    "affinities",
    "insuranceCompanies",
    "serviceSuppliers",
    "agents",
    "banks"
})
public class PartyRoleCollection {

    @XmlElement(name = "interestedParty")
    protected List<InterestedParty2> interestedParties;
    @XmlElement(name = "affinity")
    protected List<Affinity> affinities;
    @XmlElement(name = "insuranceCompany")
    protected List<InsuranceCompany> insuranceCompanies;
    @XmlElement(name = "serviceSupplier")
    protected List<ServiceSupplier> serviceSuppliers;
    @XmlElement(name = "agent")
    protected List<Agent> agents;
    @XmlElement(name = "bank")
    protected List<Bank> banks;

    /**
     * Gets the value of the interestedParties property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the interestedParties property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInterestedParties().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link InterestedParty2 }
     * 
     * 
     */
    public List<InterestedParty2> getInterestedParties() {
        if (interestedParties == null) {
            interestedParties = new ArrayList<InterestedParty2>();
        }
        return this.interestedParties;
    }

    /**
     * Gets the value of the affinities property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the affinities property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAffinities().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Affinity }
     * 
     * 
     */
    public List<Affinity> getAffinities() {
        if (affinities == null) {
            affinities = new ArrayList<Affinity>();
        }
        return this.affinities;
    }

    /**
     * Gets the value of the insuranceCompanies property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the insuranceCompanies property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInsuranceCompanies().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link InsuranceCompany }
     * 
     * 
     */
    public List<InsuranceCompany> getInsuranceCompanies() {
        if (insuranceCompanies == null) {
            insuranceCompanies = new ArrayList<InsuranceCompany>();
        }
        return this.insuranceCompanies;
    }

    /**
     * Gets the value of the serviceSuppliers property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the serviceSuppliers property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getServiceSuppliers().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ServiceSupplier }
     * 
     * 
     */
    public List<ServiceSupplier> getServiceSuppliers() {
        if (serviceSuppliers == null) {
            serviceSuppliers = new ArrayList<ServiceSupplier>();
        }
        return this.serviceSuppliers;
    }

    /**
     * Gets the value of the agents property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the agents property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAgents().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Agent }
     * 
     * 
     */
    public List<Agent> getAgents() {
        if (agents == null) {
            agents = new ArrayList<Agent>();
        }
        return this.agents;
    }

    /**
     * Gets the value of the banks property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the banks property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBanks().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Bank }
     * 
     * 
     */
    public List<Bank> getBanks() {
        if (banks == null) {
            banks = new ArrayList<Bank>();
        }
        return this.banks;
    }

}
