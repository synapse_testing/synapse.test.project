
package za.co.ominsure.synapse.iwyze.lob.backend.caseItem.vo;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the za.co.ominsure.synapse.iwyze.lob.backend.caseItem.vo package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: za.co.ominsure.synapse.iwyze.lob.backend.caseItem.vo
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link SearchCaseItemRequest }
     * 
     */
    public SearchCaseItemRequest createSearchCaseItemRequest() {
        return new SearchCaseItemRequest();
    }

    /**
     * Create an instance of {@link InputToken }
     * 
     */
    public InputToken createInputToken() {
        return new InputToken();
    }

    /**
     * Create an instance of {@link SearchTokenCase }
     * 
     */
    public SearchTokenCase createSearchTokenCase() {
        return new SearchTokenCase();
    }

    /**
     * Create an instance of {@link PageSort }
     * 
     */
    public PageSort createPageSort() {
        return new PageSort();
    }

    /**
     * Create an instance of {@link FaultMessageDetailtest }
     * 
     */
    public FaultMessageDetailtest createFaultMessageDetailtest() {
        return new FaultMessageDetailtest();
    }

    /**
     * Create an instance of {@link GetCaseItemResponse }
     * 
     */
    public GetCaseItemResponse createGetCaseItemResponse() {
        return new GetCaseItemResponse();
    }

    /**
     * Create an instance of {@link CaseItem }
     * 
     */
    public CaseItem createCaseItem() {
        return new CaseItem();
    }

    /**
     * Create an instance of {@link Result }
     * 
     */
    public Result createResult() {
        return new Result();
    }

    /**
     * Create an instance of {@link CreateCaseItemResponse }
     * 
     */
    public CreateCaseItemResponse createCreateCaseItemResponse() {
        return new CreateCaseItemResponse();
    }

    /**
     * Create an instance of {@link SearchCaseItemResponse }
     * 
     */
    public SearchCaseItemResponse createSearchCaseItemResponse() {
        return new SearchCaseItemResponse();
    }

    /**
     * Create an instance of {@link CaseItemCollection }
     * 
     */
    public CaseItemCollection createCaseItemCollection() {
        return new CaseItemCollection();
    }

    /**
     * Create an instance of {@link CreateCaseItemRequest }
     * 
     */
    public CreateCaseItemRequest createCreateCaseItemRequest() {
        return new CreateCaseItemRequest();
    }

    /**
     * Create an instance of {@link GetCaseItemRequest }
     * 
     */
    public GetCaseItemRequest createGetCaseItemRequest() {
        return new GetCaseItemRequest();
    }

    /**
     * Create an instance of {@link ModifyCaseItemRequest }
     * 
     */
    public ModifyCaseItemRequest createModifyCaseItemRequest() {
        return new ModifyCaseItemRequest();
    }

    /**
     * Create an instance of {@link ModifyCaseItemResponse }
     * 
     */
    public ModifyCaseItemResponse createModifyCaseItemResponse() {
        return new ModifyCaseItemResponse();
    }

    /**
     * Create an instance of {@link CaseItemAttBlob }
     * 
     */
    public CaseItemAttBlob createCaseItemAttBlob() {
        return new CaseItemAttBlob();
    }

    /**
     * Create an instance of {@link Attachment }
     * 
     */
    public Attachment createAttachment() {
        return new Attachment();
    }

    /**
     * Create an instance of {@link CaseItemAttClob }
     * 
     */
    public CaseItemAttClob createCaseItemAttClob() {
        return new CaseItemAttClob();
    }

    /**
     * Create an instance of {@link AttachmentCollection }
     * 
     */
    public AttachmentCollection createAttachmentCollection() {
        return new AttachmentCollection();
    }

    /**
     * Create an instance of {@link FaultMessageDetail }
     * 
     */
    public FaultMessageDetail createFaultMessageDetail() {
        return new FaultMessageDetail();
    }

    /**
     * Create an instance of {@link FlexfieldCollection }
     * 
     */
    public FlexfieldCollection createFlexfieldCollection() {
        return new FlexfieldCollection();
    }

    /**
     * Create an instance of {@link ValueTranslationPolicy }
     * 
     */
    public ValueTranslationPolicy createValueTranslationPolicy() {
        return new ValueTranslationPolicy();
    }

    /**
     * Create an instance of {@link NumberAttribute }
     * 
     */
    public NumberAttribute createNumberAttribute() {
        return new NumberAttribute();
    }

    /**
     * Create an instance of {@link KeyPair }
     * 
     */
    public KeyPair createKeyPair() {
        return new KeyPair();
    }

    /**
     * Create an instance of {@link TranslatedCode }
     * 
     */
    public TranslatedCode createTranslatedCode() {
        return new TranslatedCode();
    }

    /**
     * Create an instance of {@link ValueTranslationCollection }
     * 
     */
    public ValueTranslationCollection createValueTranslationCollection() {
        return new ValueTranslationCollection();
    }

    /**
     * Create an instance of {@link VersionToken }
     * 
     */
    public VersionToken createVersionToken() {
        return new VersionToken();
    }

    /**
     * Create an instance of {@link StringCollection }
     * 
     */
    public StringCollection createStringCollection() {
        return new StringCollection();
    }

    /**
     * Create an instance of {@link Attribute }
     * 
     */
    public Attribute createAttribute() {
        return new Attribute();
    }

    /**
     * Create an instance of {@link ValueTranslationClaim }
     * 
     */
    public ValueTranslationClaim createValueTranslationClaim() {
        return new ValueTranslationClaim();
    }

    /**
     * Create an instance of {@link VarcharAttribute }
     * 
     */
    public VarcharAttribute createVarcharAttribute() {
        return new VarcharAttribute();
    }

    /**
     * Create an instance of {@link MessageDetail }
     * 
     */
    public MessageDetail createMessageDetail() {
        return new MessageDetail();
    }

    /**
     * Create an instance of {@link KeyPairCollection }
     * 
     */
    public KeyPairCollection createKeyPairCollection() {
        return new KeyPairCollection();
    }

    /**
     * Create an instance of {@link Email }
     * 
     */
    public Email createEmail() {
        return new Email();
    }

    /**
     * Create an instance of {@link TranslatedCodeCollection }
     * 
     */
    public TranslatedCodeCollection createTranslatedCodeCollection() {
        return new TranslatedCodeCollection();
    }

    /**
     * Create an instance of {@link Flexfield }
     * 
     */
    public Flexfield createFlexfield() {
        return new Flexfield();
    }

    /**
     * Create an instance of {@link DateAttribute }
     * 
     */
    public DateAttribute createDateAttribute() {
        return new DateAttribute();
    }

    /**
     * Create an instance of {@link NumberCollection }
     * 
     */
    public NumberCollection createNumberCollection() {
        return new NumberCollection();
    }

    /**
     * Create an instance of {@link Value }
     * 
     */
    public Value createValue() {
        return new Value();
    }

    /**
     * Create an instance of {@link SiteSpecifics }
     * 
     */
    public SiteSpecifics createSiteSpecifics() {
        return new SiteSpecifics();
    }

    /**
     * Create an instance of {@link ValueTranslationPolicyCollection }
     * 
     */
    public ValueTranslationPolicyCollection createValueTranslationPolicyCollection() {
        return new ValueTranslationPolicyCollection();
    }

    /**
     * Create an instance of {@link ValueTranslation }
     * 
     */
    public ValueTranslation createValueTranslation() {
        return new ValueTranslation();
    }

    /**
     * Create an instance of {@link MessageCollection }
     * 
     */
    public MessageCollection createMessageCollection() {
        return new MessageCollection();
    }

    /**
     * Create an instance of {@link Number }
     * 
     */
    public Number createNumber() {
        return new Number();
    }

    /**
     * Create an instance of {@link CountrySpecifics }
     * 
     */
    public CountrySpecifics createCountrySpecifics() {
        return new CountrySpecifics();
    }

    /**
     * Create an instance of {@link EntityAttributeCollection }
     * 
     */
    public EntityAttributeCollection createEntityAttributeCollection() {
        return new EntityAttributeCollection();
    }

    /**
     * Create an instance of {@link ValueTranslationClaimCollection }
     * 
     */
    public ValueTranslationClaimCollection createValueTranslationClaimCollection() {
        return new ValueTranslationClaimCollection();
    }

    /**
     * Create an instance of {@link Attachment2 }
     * 
     */
    public Attachment2 createAttachment2() {
        return new Attachment2();
    }

    /**
     * Create an instance of {@link CaseItemAttClob2 }
     * 
     */
    public CaseItemAttClob2 createCaseItemAttClob2() {
        return new CaseItemAttClob2();
    }

    /**
     * Create an instance of {@link CaseItemAttBlob2 }
     * 
     */
    public CaseItemAttBlob2 createCaseItemAttBlob2() {
        return new CaseItemAttBlob2();
    }

    /**
     * Create an instance of {@link CaseItem2 }
     * 
     */
    public CaseItem2 createCaseItem2() {
        return new CaseItem2();
    }

    /**
     * Create an instance of {@link CaseItemCollection2 }
     * 
     */
    public CaseItemCollection2 createCaseItemCollection2() {
        return new CaseItemCollection2();
    }

}
