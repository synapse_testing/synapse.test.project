
package za.co.ominsure.synapse.iwyze.lob.backend.exists.policy.vo;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TIA.TAB_ATTRIBUTE complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TIA.TAB_ATTRIBUTE">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DUMMY_ATTRIBUTE_ITEM" type="{http://xmlns.oracle.com/pcbpel/adapter/db/sp/checkExistingPolicyReference}TIA.OBJ_ATTRIBUTE" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TIA.TAB_ATTRIBUTE", propOrder = {
    "dummyattributeitems"
})
public class TIATABATTRIBUTE {

    @XmlElement(name = "DUMMY_ATTRIBUTE_ITEM", nillable = true)
    protected List<TIAOBJATTRIBUTE> dummyattributeitems;

    /**
     * Gets the value of the dummyattributeitems property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the dummyattributeitems property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDUMMYATTRIBUTEITEMS().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TIAOBJATTRIBUTE }
     * 
     * 
     */
    public List<TIAOBJATTRIBUTE> getDUMMYATTRIBUTEITEMS() {
        if (dummyattributeitems == null) {
            dummyattributeitems = new ArrayList<TIAOBJATTRIBUTE>();
        }
        return this.dummyattributeitems;
    }

}
