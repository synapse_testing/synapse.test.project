
package za.co.ominsure.synapse.iwyze.lob.backend.interactive.claim.vo;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ClaimNrcEventCollection complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ClaimNrcEventCollection">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="claimNrcEvent" type="{http://infrastructure.tia.dk/schema/claim/v2/}ClaimNrcEvent" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ClaimNrcEventCollection", propOrder = {
    "claimNrcEvents"
})
public class ClaimNrcEventCollection {

    @XmlElement(name = "claimNrcEvent")
    protected List<ClaimNrcEvent> claimNrcEvents;

    /**
     * Gets the value of the claimNrcEvents property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the claimNrcEvents property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getClaimNrcEvents().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ClaimNrcEvent }
     * 
     * 
     */
    public List<ClaimNrcEvent> getClaimNrcEvents() {
        if (claimNrcEvents == null) {
            claimNrcEvents = new ArrayList<ClaimNrcEvent>();
        }
        return this.claimNrcEvents;
    }

}
