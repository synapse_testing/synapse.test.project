
package za.co.ominsure.synapse.iwyze.lob.backend.search.payment.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * Is mapped into TIA metadata type "obj_reminding_history".
 * 
 * <p>Java class for RemindingHistory complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RemindingHistory">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="accountNo" type="{http://infrastructure.tia.dk/schema/common/v2/}unsignedLong10"/>
 *         &lt;element name="reminderCollection" type="{http://infrastructure.tia.dk/schema/account/v2/}ReminderCollection" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RemindingHistory", propOrder = {
    "accountNo",
    "reminderCollection"
})
public class RemindingHistory {

    @XmlSchemaType(name = "unsignedLong")
    protected long accountNo;
    protected ReminderCollection reminderCollection;

    /**
     * Gets the value of the accountNo property.
     * 
     */
    public long getAccountNo() {
        return accountNo;
    }

    /**
     * Sets the value of the accountNo property.
     * 
     */
    public void setAccountNo(long value) {
        this.accountNo = value;
    }

    /**
     * Gets the value of the reminderCollection property.
     * 
     * @return
     *     possible object is
     *     {@link ReminderCollection }
     *     
     */
    public ReminderCollection getReminderCollection() {
        return reminderCollection;
    }

    /**
     * Sets the value of the reminderCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReminderCollection }
     *     
     */
    public void setReminderCollection(ReminderCollection value) {
        this.reminderCollection = value;
    }

}
