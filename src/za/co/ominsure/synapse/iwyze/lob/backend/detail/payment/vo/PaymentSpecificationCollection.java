
package za.co.ominsure.synapse.iwyze.lob.backend.detail.payment.vo;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Is mapped into TIA metadata type "tab_payment_specification".
 * 
 * <p>Java class for PaymentSpecificationCollection complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PaymentSpecificationCollection">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="paymentSpecification" type="{http://infrastructure.tia.dk/schema/account/v2/}PaymentSpecification" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PaymentSpecificationCollection", propOrder = {
    "paymentSpecifications"
})
public class PaymentSpecificationCollection {

    @XmlElement(name = "paymentSpecification")
    protected List<PaymentSpecification> paymentSpecifications;

    /**
     * Gets the value of the paymentSpecifications property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the paymentSpecifications property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPaymentSpecifications().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PaymentSpecification }
     * 
     * 
     */
    public List<PaymentSpecification> getPaymentSpecifications() {
        if (paymentSpecifications == null) {
            paymentSpecifications = new ArrayList<PaymentSpecification>();
        }
        return this.paymentSpecifications;
    }

}
