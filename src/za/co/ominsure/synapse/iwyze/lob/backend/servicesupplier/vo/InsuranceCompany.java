
package za.co.ominsure.synapse.iwyze.lob.backend.servicesupplier.vo;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * Additional comments
 * 
 * <p>Java class for InsuranceCompany complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="InsuranceCompany">
 *   &lt;complexContent>
 *     &lt;extension base="{http://infrastructure.tia.dk/schema/party/v2/}PartyRole">
 *       &lt;sequence>
 *         &lt;element name="insurerCode" type="{http://infrastructure.tia.dk/schema/common/v2/}string3" minOccurs="0"/>
 *         &lt;element name="insuranceCompanyName" type="{http://infrastructure.tia.dk/schema/common/v2/}string60" minOccurs="0"/>
 *         &lt;element name="parentInsurerCode" type="{http://infrastructure.tia.dk/schema/common/v2/}string3" minOccurs="0"/>
 *         &lt;element name="periodOfCancellation" type="{http://infrastructure.tia.dk/schema/common/v2/}int3" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InsuranceCompany", propOrder = {
    "insurerCode",
    "insuranceCompanyName",
    "parentInsurerCode",
    "periodOfCancellation"
})
public class InsuranceCompany
    extends PartyRole
{

    @XmlElementRef(name = "insurerCode", namespace = "http://infrastructure.tia.dk/schema/party/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> insurerCode;
    @XmlElementRef(name = "insuranceCompanyName", namespace = "http://infrastructure.tia.dk/schema/party/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> insuranceCompanyName;
    @XmlElementRef(name = "parentInsurerCode", namespace = "http://infrastructure.tia.dk/schema/party/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> parentInsurerCode;
    @XmlElementRef(name = "periodOfCancellation", namespace = "http://infrastructure.tia.dk/schema/party/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<Long> periodOfCancellation;

    /**
     * Gets the value of the insurerCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getInsurerCode() {
        return insurerCode;
    }

    /**
     * Sets the value of the insurerCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setInsurerCode(JAXBElement<String> value) {
        this.insurerCode = value;
    }

    /**
     * Gets the value of the insuranceCompanyName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getInsuranceCompanyName() {
        return insuranceCompanyName;
    }

    /**
     * Sets the value of the insuranceCompanyName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setInsuranceCompanyName(JAXBElement<String> value) {
        this.insuranceCompanyName = value;
    }

    /**
     * Gets the value of the parentInsurerCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getParentInsurerCode() {
        return parentInsurerCode;
    }

    /**
     * Sets the value of the parentInsurerCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setParentInsurerCode(JAXBElement<String> value) {
        this.parentInsurerCode = value;
    }

    /**
     * Gets the value of the periodOfCancellation property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Long }{@code >}
     *     
     */
    public JAXBElement<Long> getPeriodOfCancellation() {
        return periodOfCancellation;
    }

    /**
     * Sets the value of the periodOfCancellation property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Long }{@code >}
     *     
     */
    public void setPeriodOfCancellation(JAXBElement<Long> value) {
        this.periodOfCancellation = value;
    }

}
