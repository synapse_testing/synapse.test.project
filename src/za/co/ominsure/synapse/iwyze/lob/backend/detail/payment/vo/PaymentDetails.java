
package za.co.ominsure.synapse.iwyze.lob.backend.detail.payment.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Is mapped into TIA metadata type "obj_payment_details".
 * 
 * <p>Java class for PaymentDetails complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PaymentDetails">
 *   &lt;complexContent>
 *     &lt;extension base="{http://infrastructure.tia.dk/schema/common/v2/}TIAObject">
 *       &lt;sequence>
 *         &lt;element name="paymentDetailsId" type="{http://infrastructure.tia.dk/schema/common/v2/}seqNo" minOccurs="0"/>
 *         &lt;element name="nameIdNo" type="{http://infrastructure.tia.dk/schema/common/v2/}seqNo" minOccurs="0"/>
 *         &lt;element name="paymentMethod" type="{http://infrastructure.tia.dk/schema/common/v2/}paymentMethod" minOccurs="0"/>
 *         &lt;element name="startDate" type="{http://infrastructure.tia.dk/schema/common/v2/}date" minOccurs="0"/>
 *         &lt;element name="accountHolderName" type="{http://infrastructure.tia.dk/schema/account/v2/}accountHolderName" minOccurs="0"/>
 *         &lt;element name="bankAccountNo" type="{http://infrastructure.tia.dk/schema/account/v2/}bankAccountNo" minOccurs="0"/>
 *         &lt;element name="bankIdNo" type="{http://infrastructure.tia.dk/schema/account/v2/}bankIdNo" minOccurs="0"/>
 *         &lt;element name="bankCodeType" type="{http://infrastructure.tia.dk/schema/account/v2/}bankCodeType" minOccurs="0"/>
 *         &lt;element name="bankCode" type="{http://infrastructure.tia.dk/schema/account/v2/}bankCode" minOccurs="0"/>
 *         &lt;element name="giroType" type="{http://infrastructure.tia.dk/schema/account/v2/}giroType" minOccurs="0"/>
 *         &lt;element name="identification" type="{http://infrastructure.tia.dk/schema/account/v2/}identification" minOccurs="0"/>
 *         &lt;element name="creditorNo" type="{http://infrastructure.tia.dk/schema/account/v2/}creditorNo" minOccurs="0"/>
 *         &lt;element name="externalPaymentId" type="{http://infrastructure.tia.dk/schema/account/v2/}externalPaymentId" minOccurs="0"/>
 *         &lt;element name="paymentChannel" type="{http://infrastructure.tia.dk/schema/account/v2/}paymentChannelType" minOccurs="0"/>
 *         &lt;element name="ccNo" type="{http://infrastructure.tia.dk/schema/account/v2/}ccNo" minOccurs="0"/>
 *         &lt;element name="ccExpiryDate" type="{http://infrastructure.tia.dk/schema/common/v2/}date" minOccurs="0"/>
 *         &lt;element name="ccContinuous" type="{http://infrastructure.tia.dk/schema/common/v2/}tiaYN" minOccurs="0"/>
 *         &lt;element name="ccContinuousDate" type="{http://infrastructure.tia.dk/schema/common/v2/}date" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PaymentDetails", propOrder = {
    "paymentDetailsId",
    "nameIdNo",
    "paymentMethod",
    "startDate",
    "accountHolderName",
    "bankAccountNo",
    "bankIdNo",
    "bankCodeType",
    "bankCode",
    "giroType",
    "identification",
    "creditorNo",
    "externalPaymentId",
    "paymentChannel",
    "ccNo",
    "ccExpiryDate",
    "ccContinuous",
    "ccContinuousDate"
})
public class PaymentDetails
    extends TIAObject
{

    @XmlElement(nillable = true)
    @XmlSchemaType(name = "unsignedLong")
    protected Long paymentDetailsId;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "unsignedLong")
    protected Long nameIdNo;
    @XmlElement(nillable = true)
    protected String paymentMethod;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar startDate;
    @XmlElement(nillable = true)
    protected String accountHolderName;
    @XmlElement(nillable = true)
    protected String bankAccountNo;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "unsignedLong")
    protected Long bankIdNo;
    @XmlElement(nillable = true)
    protected String bankCodeType;
    @XmlElement(nillable = true)
    protected String bankCode;
    @XmlElement(nillable = true)
    protected String giroType;
    @XmlElement(nillable = true)
    protected String identification;
    @XmlElement(nillable = true)
    protected String creditorNo;
    @XmlElement(nillable = true)
    protected String externalPaymentId;
    @XmlElement(nillable = true)
    protected String paymentChannel;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "unsignedLong")
    protected Long ccNo;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar ccExpiryDate;
    @XmlElement(nillable = true)
    protected String ccContinuous;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar ccContinuousDate;

    /**
     * Gets the value of the paymentDetailsId property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getPaymentDetailsId() {
        return paymentDetailsId;
    }

    /**
     * Sets the value of the paymentDetailsId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setPaymentDetailsId(Long value) {
        this.paymentDetailsId = value;
    }

    /**
     * Gets the value of the nameIdNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getNameIdNo() {
        return nameIdNo;
    }

    /**
     * Sets the value of the nameIdNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setNameIdNo(Long value) {
        this.nameIdNo = value;
    }

    /**
     * Gets the value of the paymentMethod property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaymentMethod() {
        return paymentMethod;
    }

    /**
     * Sets the value of the paymentMethod property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaymentMethod(String value) {
        this.paymentMethod = value;
    }

    /**
     * Gets the value of the startDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getStartDate() {
        return startDate;
    }

    /**
     * Sets the value of the startDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setStartDate(XMLGregorianCalendar value) {
        this.startDate = value;
    }

    /**
     * Gets the value of the accountHolderName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountHolderName() {
        return accountHolderName;
    }

    /**
     * Sets the value of the accountHolderName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountHolderName(String value) {
        this.accountHolderName = value;
    }

    /**
     * Gets the value of the bankAccountNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBankAccountNo() {
        return bankAccountNo;
    }

    /**
     * Sets the value of the bankAccountNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBankAccountNo(String value) {
        this.bankAccountNo = value;
    }

    /**
     * Gets the value of the bankIdNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getBankIdNo() {
        return bankIdNo;
    }

    /**
     * Sets the value of the bankIdNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setBankIdNo(Long value) {
        this.bankIdNo = value;
    }

    /**
     * Gets the value of the bankCodeType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBankCodeType() {
        return bankCodeType;
    }

    /**
     * Sets the value of the bankCodeType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBankCodeType(String value) {
        this.bankCodeType = value;
    }

    /**
     * Gets the value of the bankCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBankCode() {
        return bankCode;
    }

    /**
     * Sets the value of the bankCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBankCode(String value) {
        this.bankCode = value;
    }

    /**
     * Gets the value of the giroType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGiroType() {
        return giroType;
    }

    /**
     * Sets the value of the giroType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGiroType(String value) {
        this.giroType = value;
    }

    /**
     * Gets the value of the identification property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdentification() {
        return identification;
    }

    /**
     * Sets the value of the identification property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdentification(String value) {
        this.identification = value;
    }

    /**
     * Gets the value of the creditorNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCreditorNo() {
        return creditorNo;
    }

    /**
     * Sets the value of the creditorNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCreditorNo(String value) {
        this.creditorNo = value;
    }

    /**
     * Gets the value of the externalPaymentId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExternalPaymentId() {
        return externalPaymentId;
    }

    /**
     * Sets the value of the externalPaymentId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExternalPaymentId(String value) {
        this.externalPaymentId = value;
    }

    /**
     * Gets the value of the paymentChannel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaymentChannel() {
        return paymentChannel;
    }

    /**
     * Sets the value of the paymentChannel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaymentChannel(String value) {
        this.paymentChannel = value;
    }

    /**
     * Gets the value of the ccNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getCcNo() {
        return ccNo;
    }

    /**
     * Sets the value of the ccNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setCcNo(Long value) {
        this.ccNo = value;
    }

    /**
     * Gets the value of the ccExpiryDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCcExpiryDate() {
        return ccExpiryDate;
    }

    /**
     * Sets the value of the ccExpiryDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCcExpiryDate(XMLGregorianCalendar value) {
        this.ccExpiryDate = value;
    }

    /**
     * Gets the value of the ccContinuous property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCcContinuous() {
        return ccContinuous;
    }

    /**
     * Sets the value of the ccContinuous property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCcContinuous(String value) {
        this.ccContinuous = value;
    }

    /**
     * Gets the value of the ccContinuousDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCcContinuousDate() {
        return ccContinuousDate;
    }

    /**
     * Sets the value of the ccContinuousDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCcContinuousDate(XMLGregorianCalendar value) {
        this.ccContinuousDate = value;
    }

}
