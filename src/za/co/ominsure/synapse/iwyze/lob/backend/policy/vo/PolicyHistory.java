
package za.co.ominsure.synapse.iwyze.lob.backend.policy.vo;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * Is mapped into undefined".
 * 
 * <p>Java class for PolicyHistory complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PolicyHistory">
 *   &lt;complexContent>
 *     &lt;extension base="{http://infrastructure.tia.dk/schema/common/v2/}TIAObject">
 *       &lt;sequence>
 *         &lt;element name="policyNo" type="{http://infrastructure.tia.dk/schema/common/v2/}policyNo"/>
 *         &lt;element name="prodId" type="{http://infrastructure.tia.dk/schema/policy/v2/}prodId"/>
 *         &lt;element name="mp" type="{http://infrastructure.tia.dk/schema/common/v2/}string1" minOccurs="0"/>
 *         &lt;element name="policyVersionCollection" type="{http://infrastructure.tia.dk/schema/policy/v2/}PolicyVersionShortCollection"/>
 *         &lt;element name="pendingTransactionCollection" type="{http://infrastructure.tia.dk/schema/policy/v2/}PendingPolicyTransShortCollection" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PolicyHistory", propOrder = {
    "policyNo",
    "prodId",
    "mp",
    "policyVersionCollection",
    "pendingTransactionCollection"
})
public class PolicyHistory
    extends TIAObject
{

    @XmlElement(required = true)
    @XmlSchemaType(name = "unsignedLong")
    protected BigInteger policyNo;
    @XmlElement(required = true)
    protected String prodId;
    @XmlElement(nillable = true)
    protected String mp;
    @XmlElement(required = true)
    protected PolicyVersionShortCollection policyVersionCollection;
    protected PendingPolicyTransShortCollection pendingTransactionCollection;

    /**
     * Gets the value of the policyNo property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getPolicyNo() {
        return policyNo;
    }

    /**
     * Sets the value of the policyNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setPolicyNo(BigInteger value) {
        this.policyNo = value;
    }

    /**
     * Gets the value of the prodId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProdId() {
        return prodId;
    }

    /**
     * Sets the value of the prodId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProdId(String value) {
        this.prodId = value;
    }

    /**
     * Gets the value of the mp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMp() {
        return mp;
    }

    /**
     * Sets the value of the mp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMp(String value) {
        this.mp = value;
    }

    /**
     * Gets the value of the policyVersionCollection property.
     * 
     * @return
     *     possible object is
     *     {@link PolicyVersionShortCollection }
     *     
     */
    public PolicyVersionShortCollection getPolicyVersionCollection() {
        return policyVersionCollection;
    }

    /**
     * Sets the value of the policyVersionCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link PolicyVersionShortCollection }
     *     
     */
    public void setPolicyVersionCollection(PolicyVersionShortCollection value) {
        this.policyVersionCollection = value;
    }

    /**
     * Gets the value of the pendingTransactionCollection property.
     * 
     * @return
     *     possible object is
     *     {@link PendingPolicyTransShortCollection }
     *     
     */
    public PendingPolicyTransShortCollection getPendingTransactionCollection() {
        return pendingTransactionCollection;
    }

    /**
     * Sets the value of the pendingTransactionCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link PendingPolicyTransShortCollection }
     *     
     */
    public void setPendingTransactionCollection(PendingPolicyTransShortCollection value) {
        this.pendingTransactionCollection = value;
    }

}
