
package za.co.ominsure.synapse.iwyze.lob.backend.claim.payment.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Additional comments
 * 
 * <p>Java class for ClaimEvent complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ClaimEvent">
 *   &lt;complexContent>
 *     &lt;extension base="{http://infrastructure.tia.dk/schema/common/v2/}TIAObject">
 *       &lt;sequence>
 *         &lt;element name="eventNo" type="{http://infrastructure.tia.dk/schema/common/v2/}long10" minOccurs="0"/>
 *         &lt;element name="eventType" type="{http://infrastructure.tia.dk/schema/common/v2/}string3" minOccurs="0"/>
 *         &lt;element name="causeType" type="{http://infrastructure.tia.dk/schema/common/v2/}string3" minOccurs="0"/>
 *         &lt;element name="subcauseType" type="{http://infrastructure.tia.dk/schema/common/v2/}string3" minOccurs="0"/>
 *         &lt;element name="incidentDate" type="{http://infrastructure.tia.dk/schema/common/v2/}timestamp" minOccurs="0"/>
 *         &lt;element name="exactIncidentDate" type="{http://infrastructure.tia.dk/schema/common/v2/}string1" minOccurs="0"/>
 *         &lt;element name="incidentDesc" type="{http://infrastructure.tia.dk/schema/common/v2/}string2000" minOccurs="0"/>
 *         &lt;element name="placeType" type="{http://infrastructure.tia.dk/schema/common/v2/}string3" minOccurs="0"/>
 *         &lt;element name="locationId" type="{http://infrastructure.tia.dk/schema/common/v2/}string15" minOccurs="0"/>
 *         &lt;element name="placeDesc" type="{http://infrastructure.tia.dk/schema/common/v2/}string2000" minOccurs="0"/>
 *         &lt;element name="createDate" type="{http://infrastructure.tia.dk/schema/common/v2/}timestamp" minOccurs="0"/>
 *         &lt;element name="expectedFinishDate" type="{http://infrastructure.tia.dk/schema/common/v2/}timestamp" minOccurs="0"/>
 *         &lt;element name="locked" type="{http://infrastructure.tia.dk/schema/common/v2/}string269" minOccurs="0"/>
 *         &lt;element name="claMajorEventNo" type="{http://infrastructure.tia.dk/schema/common/v2/}long10" minOccurs="0"/>
 *         &lt;element name="claNrcEventNo" type="{http://infrastructure.tia.dk/schema/common/v2/}long10" minOccurs="0"/>
 *         &lt;element name="claimCollection" type="{http://infrastructure.tia.dk/schema/claim/v2/}ClaimCollection" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ClaimEvent", propOrder = {
    "eventNo",
    "eventType",
    "causeType",
    "subcauseType",
    "incidentDate",
    "exactIncidentDate",
    "incidentDesc",
    "placeType",
    "locationId",
    "placeDesc",
    "createDate",
    "expectedFinishDate",
    "locked",
    "claMajorEventNo",
    "claNrcEventNo",
    "claimCollection"
})
public class ClaimEvent
    extends TIAObject
{

    @XmlElement(nillable = true)
    protected Long eventNo;
    @XmlElement(nillable = true)
    protected String eventType;
    @XmlElement(nillable = true)
    protected String causeType;
    @XmlElement(nillable = true)
    protected String subcauseType;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar incidentDate;
    @XmlElement(nillable = true)
    protected String exactIncidentDate;
    @XmlElement(nillable = true)
    protected String incidentDesc;
    @XmlElement(nillable = true)
    protected String placeType;
    @XmlElement(nillable = true)
    protected String locationId;
    @XmlElement(nillable = true)
    protected String placeDesc;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar createDate;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar expectedFinishDate;
    @XmlElement(nillable = true)
    protected String locked;
    @XmlElement(nillable = true)
    protected Long claMajorEventNo;
    @XmlElement(nillable = true)
    protected Long claNrcEventNo;
    protected ClaimCollection claimCollection;

    /**
     * Gets the value of the eventNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getEventNo() {
        return eventNo;
    }

    /**
     * Sets the value of the eventNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setEventNo(Long value) {
        this.eventNo = value;
    }

    /**
     * Gets the value of the eventType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEventType() {
        return eventType;
    }

    /**
     * Sets the value of the eventType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEventType(String value) {
        this.eventType = value;
    }

    /**
     * Gets the value of the causeType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCauseType() {
        return causeType;
    }

    /**
     * Sets the value of the causeType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCauseType(String value) {
        this.causeType = value;
    }

    /**
     * Gets the value of the subcauseType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubcauseType() {
        return subcauseType;
    }

    /**
     * Sets the value of the subcauseType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubcauseType(String value) {
        this.subcauseType = value;
    }

    /**
     * Gets the value of the incidentDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getIncidentDate() {
        return incidentDate;
    }

    /**
     * Sets the value of the incidentDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setIncidentDate(XMLGregorianCalendar value) {
        this.incidentDate = value;
    }

    /**
     * Gets the value of the exactIncidentDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExactIncidentDate() {
        return exactIncidentDate;
    }

    /**
     * Sets the value of the exactIncidentDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExactIncidentDate(String value) {
        this.exactIncidentDate = value;
    }

    /**
     * Gets the value of the incidentDesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIncidentDesc() {
        return incidentDesc;
    }

    /**
     * Sets the value of the incidentDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIncidentDesc(String value) {
        this.incidentDesc = value;
    }

    /**
     * Gets the value of the placeType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPlaceType() {
        return placeType;
    }

    /**
     * Sets the value of the placeType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPlaceType(String value) {
        this.placeType = value;
    }

    /**
     * Gets the value of the locationId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLocationId() {
        return locationId;
    }

    /**
     * Sets the value of the locationId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLocationId(String value) {
        this.locationId = value;
    }

    /**
     * Gets the value of the placeDesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPlaceDesc() {
        return placeDesc;
    }

    /**
     * Sets the value of the placeDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPlaceDesc(String value) {
        this.placeDesc = value;
    }

    /**
     * Gets the value of the createDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCreateDate() {
        return createDate;
    }

    /**
     * Sets the value of the createDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCreateDate(XMLGregorianCalendar value) {
        this.createDate = value;
    }

    /**
     * Gets the value of the expectedFinishDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getExpectedFinishDate() {
        return expectedFinishDate;
    }

    /**
     * Sets the value of the expectedFinishDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setExpectedFinishDate(XMLGregorianCalendar value) {
        this.expectedFinishDate = value;
    }

    /**
     * Gets the value of the locked property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLocked() {
        return locked;
    }

    /**
     * Sets the value of the locked property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLocked(String value) {
        this.locked = value;
    }

    /**
     * Gets the value of the claMajorEventNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getClaMajorEventNo() {
        return claMajorEventNo;
    }

    /**
     * Sets the value of the claMajorEventNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setClaMajorEventNo(Long value) {
        this.claMajorEventNo = value;
    }

    /**
     * Gets the value of the claNrcEventNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getClaNrcEventNo() {
        return claNrcEventNo;
    }

    /**
     * Sets the value of the claNrcEventNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setClaNrcEventNo(Long value) {
        this.claNrcEventNo = value;
    }

    /**
     * Gets the value of the claimCollection property.
     * 
     * @return
     *     possible object is
     *     {@link ClaimCollection }
     *     
     */
    public ClaimCollection getClaimCollection() {
        return claimCollection;
    }

    /**
     * Sets the value of the claimCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link ClaimCollection }
     *     
     */
    public void setClaimCollection(ClaimCollection value) {
        this.claimCollection = value;
    }

}
