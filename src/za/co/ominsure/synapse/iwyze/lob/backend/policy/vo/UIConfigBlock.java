
package za.co.ominsure.synapse.iwyze.lob.backend.policy.vo;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for UIConfigBlock complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="UIConfigBlock">
 *   &lt;complexContent>
 *     &lt;extension base="{http://infrastructure.tia.dk/schema/common/v2/}TIAObject">
 *       &lt;sequence>
 *         &lt;element name="sortNo" type="{http://infrastructure.tia.dk/schema/common/v2/}numberType" minOccurs="0"/>
 *         &lt;element name="readOnly" type="{http://infrastructure.tia.dk/schema/common/v2/}tiaYN" minOccurs="0"/>
 *         &lt;element name="disclosed" type="{http://infrastructure.tia.dk/schema/common/v2/}tiaYN" minOccurs="0"/>
 *         &lt;element name="blockName" type="{http://infrastructure.tia.dk/schema/common/v2/}string10" minOccurs="0"/>
 *         &lt;element name="blockDescription" type="{http://infrastructure.tia.dk/schema/common/v2/}string60" minOccurs="0"/>
 *         &lt;element name="blockType" type="{http://infrastructure.tia.dk/schema/common/v2/}string100" minOccurs="0"/>
 *         &lt;element name="blockVersion" type="{http://infrastructure.tia.dk/schema/common/v2/}numberType" minOccurs="0"/>
 *         &lt;element name="status" type="{http://infrastructure.tia.dk/schema/common/v2/}tiaYN" minOccurs="0"/>
 *         &lt;element name="uiConfigObjects" type="{http://infrastructure.tia.dk/schema/policy/v4/}UIConfigObjectCollection" minOccurs="0"/>
 *         &lt;element name="uiConfigRisks" type="{http://infrastructure.tia.dk/schema/policy/v4/}UIConfigRiskCollection" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UIConfigBlock", namespace = "http://infrastructure.tia.dk/schema/policy/v4/", propOrder = {
    "sortNo",
    "readOnly",
    "disclosed",
    "blockName",
    "blockDescription",
    "blockType",
    "blockVersion",
    "status",
    "uiConfigObjects",
    "uiConfigRisks"
})
public class UIConfigBlock
    extends TIAObject
{

    @XmlElement(nillable = true)
    protected BigDecimal sortNo;
    @XmlElement(nillable = true)
    protected String readOnly;
    @XmlElement(nillable = true)
    protected String disclosed;
    @XmlElement(nillable = true)
    protected String blockName;
    @XmlElement(nillable = true)
    protected String blockDescription;
    @XmlElement(nillable = true)
    protected String blockType;
    @XmlElement(nillable = true)
    protected BigDecimal blockVersion;
    @XmlElement(nillable = true)
    protected String status;
    @XmlElement(nillable = true)
    protected UIConfigObjectCollection uiConfigObjects;
    @XmlElement(nillable = true)
    protected UIConfigRiskCollection uiConfigRisks;

    /**
     * Gets the value of the sortNo property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSortNo() {
        return sortNo;
    }

    /**
     * Sets the value of the sortNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSortNo(BigDecimal value) {
        this.sortNo = value;
    }

    /**
     * Gets the value of the readOnly property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReadOnly() {
        return readOnly;
    }

    /**
     * Sets the value of the readOnly property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReadOnly(String value) {
        this.readOnly = value;
    }

    /**
     * Gets the value of the disclosed property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDisclosed() {
        return disclosed;
    }

    /**
     * Sets the value of the disclosed property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDisclosed(String value) {
        this.disclosed = value;
    }

    /**
     * Gets the value of the blockName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBlockName() {
        return blockName;
    }

    /**
     * Sets the value of the blockName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBlockName(String value) {
        this.blockName = value;
    }

    /**
     * Gets the value of the blockDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBlockDescription() {
        return blockDescription;
    }

    /**
     * Sets the value of the blockDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBlockDescription(String value) {
        this.blockDescription = value;
    }

    /**
     * Gets the value of the blockType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBlockType() {
        return blockType;
    }

    /**
     * Sets the value of the blockType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBlockType(String value) {
        this.blockType = value;
    }

    /**
     * Gets the value of the blockVersion property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getBlockVersion() {
        return blockVersion;
    }

    /**
     * Sets the value of the blockVersion property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setBlockVersion(BigDecimal value) {
        this.blockVersion = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

    /**
     * Gets the value of the uiConfigObjects property.
     * 
     * @return
     *     possible object is
     *     {@link UIConfigObjectCollection }
     *     
     */
    public UIConfigObjectCollection getUiConfigObjects() {
        return uiConfigObjects;
    }

    /**
     * Sets the value of the uiConfigObjects property.
     * 
     * @param value
     *     allowed object is
     *     {@link UIConfigObjectCollection }
     *     
     */
    public void setUiConfigObjects(UIConfigObjectCollection value) {
        this.uiConfigObjects = value;
    }

    /**
     * Gets the value of the uiConfigRisks property.
     * 
     * @return
     *     possible object is
     *     {@link UIConfigRiskCollection }
     *     
     */
    public UIConfigRiskCollection getUiConfigRisks() {
        return uiConfigRisks;
    }

    /**
     * Sets the value of the uiConfigRisks property.
     * 
     * @param value
     *     allowed object is
     *     {@link UIConfigRiskCollection }
     *     
     */
    public void setUiConfigRisks(UIConfigRiskCollection value) {
        this.uiConfigRisks = value;
    }

}
