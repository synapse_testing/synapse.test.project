
package za.co.ominsure.synapse.iwyze.lob.backend.claim.payment.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Additional comments
 * 
 * <p>Java class for ThirdParty complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ThirdParty">
 *   &lt;complexContent>
 *     &lt;extension base="{http://infrastructure.tia.dk/schema/common/v2/}TIAObject">
 *       &lt;sequence>
 *         &lt;element name="thirdPartyNo" type="{http://infrastructure.tia.dk/schema/common/v2/}long10" minOccurs="0"/>
 *         &lt;element name="nameIdNo" type="{http://infrastructure.tia.dk/schema/common/v2/}long10" minOccurs="0"/>
 *         &lt;element name="description" type="{http://infrastructure.tia.dk/schema/common/v2/}string2000" minOccurs="0"/>
 *         &lt;element name="thirdPartyType" type="{http://infrastructure.tia.dk/schema/common/v2/}string3" minOccurs="0"/>
 *         &lt;element name="ownClaimNo" type="{http://infrastructure.tia.dk/schema/common/v2/}long10" minOccurs="0"/>
 *         &lt;element name="responsibilityPercent" type="{http://infrastructure.tia.dk/schema/common/v2/}string3" minOccurs="0"/>
 *         &lt;element name="tpResponsibilityPercent" type="{http://infrastructure.tia.dk/schema/common/v2/}string3" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ThirdParty", propOrder = {
    "thirdPartyNo",
    "nameIdNo",
    "description",
    "thirdPartyType",
    "ownClaimNo",
    "responsibilityPercent",
    "tpResponsibilityPercent"
})
public class ThirdParty
    extends TIAObject
{

    @XmlElement(nillable = true)
    protected Long thirdPartyNo;
    @XmlElement(nillable = true)
    protected Long nameIdNo;
    @XmlElement(nillable = true)
    protected String description;
    @XmlElement(nillable = true)
    protected String thirdPartyType;
    @XmlElement(nillable = true)
    protected Long ownClaimNo;
    @XmlElement(nillable = true)
    protected String responsibilityPercent;
    @XmlElement(nillable = true)
    protected String tpResponsibilityPercent;

    /**
     * Gets the value of the thirdPartyNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getThirdPartyNo() {
        return thirdPartyNo;
    }

    /**
     * Sets the value of the thirdPartyNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setThirdPartyNo(Long value) {
        this.thirdPartyNo = value;
    }

    /**
     * Gets the value of the nameIdNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getNameIdNo() {
        return nameIdNo;
    }

    /**
     * Sets the value of the nameIdNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setNameIdNo(Long value) {
        this.nameIdNo = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the thirdPartyType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getThirdPartyType() {
        return thirdPartyType;
    }

    /**
     * Sets the value of the thirdPartyType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setThirdPartyType(String value) {
        this.thirdPartyType = value;
    }

    /**
     * Gets the value of the ownClaimNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getOwnClaimNo() {
        return ownClaimNo;
    }

    /**
     * Sets the value of the ownClaimNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setOwnClaimNo(Long value) {
        this.ownClaimNo = value;
    }

    /**
     * Gets the value of the responsibilityPercent property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResponsibilityPercent() {
        return responsibilityPercent;
    }

    /**
     * Sets the value of the responsibilityPercent property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResponsibilityPercent(String value) {
        this.responsibilityPercent = value;
    }

    /**
     * Gets the value of the tpResponsibilityPercent property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTpResponsibilityPercent() {
        return tpResponsibilityPercent;
    }

    /**
     * Sets the value of the tpResponsibilityPercent property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTpResponsibilityPercent(String value) {
        this.tpResponsibilityPercent = value;
    }

}
