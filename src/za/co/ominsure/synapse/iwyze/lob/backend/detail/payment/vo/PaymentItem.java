
package za.co.ominsure.synapse.iwyze.lob.backend.detail.payment.vo;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * Is mapped into TIA metadata type "obj_payment_item".
 * 
 * <p>Java class for PaymentItem complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PaymentItem">
 *   &lt;complexContent>
 *     &lt;extension base="{http://infrastructure.tia.dk/schema/common/v2/}TIAObject">
 *       &lt;sequence>
 *         &lt;element name="accountItemNo" type="{http://infrastructure.tia.dk/schema/common/v2/}seqNo" minOccurs="0"/>
 *         &lt;element name="amount" type="{http://infrastructure.tia.dk/schema/common/v2/}amount" minOccurs="0"/>
 *         &lt;element name="approvedBy" type="{http://infrastructure.tia.dk/schema/common/v2/}userId" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PaymentItem", propOrder = {
    "accountItemNo",
    "amount",
    "approvedBy"
})
public class PaymentItem
    extends TIAObject
{

    @XmlElement(nillable = true)
    @XmlSchemaType(name = "unsignedLong")
    protected Long accountItemNo;
    @XmlElement(nillable = true)
    protected BigDecimal amount;
    @XmlElement(nillable = true)
    protected String approvedBy;

    /**
     * Gets the value of the accountItemNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getAccountItemNo() {
        return accountItemNo;
    }

    /**
     * Sets the value of the accountItemNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setAccountItemNo(Long value) {
        this.accountItemNo = value;
    }

    /**
     * Gets the value of the amount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * Sets the value of the amount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAmount(BigDecimal value) {
        this.amount = value;
    }

    /**
     * Gets the value of the approvedBy property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApprovedBy() {
        return approvedBy;
    }

    /**
     * Sets the value of the approvedBy property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApprovedBy(String value) {
        this.approvedBy = value;
    }

}
