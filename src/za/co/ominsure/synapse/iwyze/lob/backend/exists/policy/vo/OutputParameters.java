
package za.co.ominsure.synapse.iwyze.lob.backend.exists.policy.vo;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="P_POLICY_HOLDER_ID" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="P_POLICY_DETAILS" type="{http://xmlns.oracle.com/pcbpel/adapter/db/sp/checkExistingPolicyReference}TIA.TAB_POLICY_DETAILS" minOccurs="0"/>
 *         &lt;element name="P_RESULT" type="{http://xmlns.oracle.com/pcbpel/adapter/db/sp/checkExistingPolicyReference}TIA.OBJ_RESULT" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "ppolicyholderid",
    "ppolicydetails",
    "presult"
})
@XmlRootElement(name = "OutputParameters")
public class OutputParameters {

    @XmlElement(name = "P_POLICY_HOLDER_ID", nillable = true)
    protected BigDecimal ppolicyholderid;
    @XmlElement(name = "P_POLICY_DETAILS", nillable = true)
    protected TIATABPOLICYDETAILS ppolicydetails;
    @XmlElement(name = "P_RESULT", nillable = true)
    protected TIAOBJRESULT presult;

    /**
     * Gets the value of the ppolicyholderid property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPPOLICYHOLDERID() {
        return ppolicyholderid;
    }

    /**
     * Sets the value of the ppolicyholderid property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPPOLICYHOLDERID(BigDecimal value) {
        this.ppolicyholderid = value;
    }

    /**
     * Gets the value of the ppolicydetails property.
     * 
     * @return
     *     possible object is
     *     {@link TIATABPOLICYDETAILS }
     *     
     */
    public TIATABPOLICYDETAILS getPPOLICYDETAILS() {
        return ppolicydetails;
    }

    /**
     * Sets the value of the ppolicydetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link TIATABPOLICYDETAILS }
     *     
     */
    public void setPPOLICYDETAILS(TIATABPOLICYDETAILS value) {
        this.ppolicydetails = value;
    }

    /**
     * Gets the value of the presult property.
     * 
     * @return
     *     possible object is
     *     {@link TIAOBJRESULT }
     *     
     */
    public TIAOBJRESULT getPRESULT() {
        return presult;
    }

    /**
     * Sets the value of the presult property.
     * 
     * @param value
     *     allowed object is
     *     {@link TIAOBJRESULT }
     *     
     */
    public void setPRESULT(TIAOBJRESULT value) {
        this.presult = value;
    }

}
