
package za.co.ominsure.synapse.iwyze.lob.backend.search.payment.vo;

import java.math.BigDecimal;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for Value complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Value">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element name="varchar2Value" type="{http://infrastructure.tia.dk/schema/common/v2/}varchar2"/>
 *         &lt;element name="numberValue" type="{http://infrastructure.tia.dk/schema/common/v2/}number"/>
 *         &lt;element name="dateValue" type="{http://infrastructure.tia.dk/schema/common/v2/}timestamp"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Value", namespace = "http://infrastructure.tia.dk/schema/common/v2/", propOrder = {
    "varchar2Value",
    "numberValue",
    "dateValue"
})
public class Value {

    @XmlElementRef(name = "varchar2Value", namespace = "http://infrastructure.tia.dk/schema/common/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> varchar2Value;
    @XmlElementRef(name = "numberValue", namespace = "http://infrastructure.tia.dk/schema/common/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<BigDecimal> numberValue;
    @XmlElementRef(name = "dateValue", namespace = "http://infrastructure.tia.dk/schema/common/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> dateValue;

    /**
     * Gets the value of the varchar2Value property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getVarchar2Value() {
        return varchar2Value;
    }

    /**
     * Sets the value of the varchar2Value property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setVarchar2Value(JAXBElement<String> value) {
        this.varchar2Value = value;
    }

    /**
     * Gets the value of the numberValue property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public JAXBElement<BigDecimal> getNumberValue() {
        return numberValue;
    }

    /**
     * Sets the value of the numberValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public void setNumberValue(JAXBElement<BigDecimal> value) {
        this.numberValue = value;
    }

    /**
     * Gets the value of the dateValue property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getDateValue() {
        return dateValue;
    }

    /**
     * Sets the value of the dateValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setDateValue(JAXBElement<XMLGregorianCalendar> value) {
        this.dateValue = value;
    }

}
