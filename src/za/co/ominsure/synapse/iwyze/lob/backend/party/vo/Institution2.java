
package za.co.ominsure.synapse.iwyze.lob.backend.party.vo;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * Is mapped into TIA metadata type "obj_institution".
 * 
 * <p>Java class for Institution complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Institution">
 *   &lt;complexContent>
 *     &lt;extension base="{http://infrastructure.tia.dk/schema/party/v2/}AbstractParty">
 *       &lt;sequence>
 *         &lt;element name="companyRegNo" type="{http://infrastructure.tia.dk/schema/party/v2/}companyRegNo" minOccurs="0"/>
 *         &lt;element name="companyVatNo" type="{http://infrastructure.tia.dk/schema/party/v2/}companyVatNo" minOccurs="0"/>
 *         &lt;element name="contactPerson" type="{http://infrastructure.tia.dk/schema/party/v2/}contactPerson" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Institution", propOrder = {
    "companyRegNo",
    "companyVatNo",
    "contactPerson"
})
public class Institution2
    extends AbstractParty2
{

    @XmlElementRef(name = "companyRegNo", namespace = "http://infrastructure.tia.dk/schema/party/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> companyRegNo;
    @XmlElementRef(name = "companyVatNo", namespace = "http://infrastructure.tia.dk/schema/party/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> companyVatNo;
    @XmlElementRef(name = "contactPerson", namespace = "http://infrastructure.tia.dk/schema/party/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> contactPerson;

    /**
     * Gets the value of the companyRegNo property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCompanyRegNo() {
        return companyRegNo;
    }

    /**
     * Sets the value of the companyRegNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCompanyRegNo(JAXBElement<String> value) {
        this.companyRegNo = value;
    }

    /**
     * Gets the value of the companyVatNo property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCompanyVatNo() {
        return companyVatNo;
    }

    /**
     * Sets the value of the companyVatNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCompanyVatNo(JAXBElement<String> value) {
        this.companyVatNo = value;
    }

    /**
     * Gets the value of the contactPerson property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getContactPerson() {
        return contactPerson;
    }

    /**
     * Sets the value of the contactPerson property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setContactPerson(JAXBElement<String> value) {
        this.contactPerson = value;
    }

}
