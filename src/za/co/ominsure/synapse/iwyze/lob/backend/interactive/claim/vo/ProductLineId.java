
package za.co.ominsure.synapse.iwyze.lob.backend.interactive.claim.vo;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for productLineId.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="productLineId">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="CASCO"/>
 *     &lt;enumeration value="MTPL"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "productLineId", namespace = "http://infrastructure.tia.dk/schema/common/v2/")
@XmlEnum
public enum ProductLineId {

    CASCO,
    MTPL;

    public String value() {
        return name();
    }

    public static ProductLineId fromValue(String v) {
        return valueOf(v);
    }

}
