
package za.co.ominsure.synapse.iwyze.lob.backend.search.payment.vo;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * Is mapped into TIA metadata type "tab_account_item_match".
 * 
 * <p>Java class for AccountItemMatchCollection complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AccountItemMatchCollection">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="accountItemMatch" type="{http://infrastructure.tia.dk/schema/account/v2/}AccountItemMatch" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AccountItemMatchCollection", propOrder = {
    "accountItemMatch"
})
public class AccountItemMatchCollection {

    protected List<AccountItemMatch> accountItemMatch;

    /**
     * Gets the value of the accountItemMatch property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the accountItemMatch property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAccountItemMatch().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AccountItemMatch }
     * 
     * 
     */
    public List<AccountItemMatch> getAccountItemMatch() {
        if (accountItemMatch == null) {
            accountItemMatch = new ArrayList<AccountItemMatch>();
        }
        return this.accountItemMatch;
    }

}
