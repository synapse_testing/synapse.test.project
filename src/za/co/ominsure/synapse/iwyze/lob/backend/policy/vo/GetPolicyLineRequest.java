
package za.co.ominsure.synapse.iwyze.lob.backend.policy.vo;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="inputToken" type="{http://infrastructure.tia.dk/schema/common/v2/}InputToken"/>
 *         &lt;element name="policyLineNo" type="{http://infrastructure.tia.dk/schema/common/v2/}policyLineNo" minOccurs="0"/>
 *         &lt;element name="policyLineSeqNo" type="{http://infrastructure.tia.dk/schema/policy/v2/}seqNo" minOccurs="0"/>
 *         &lt;element name="objectNoCollection" type="{http://infrastructure.tia.dk/schema/policy/v2/}ObjectNoCollection" minOccurs="0"/>
 *         &lt;element name="pageSort" type="{http://infrastructure.tia.dk/schema/common/v2/}PageSort" minOccurs="0"/>
 *         &lt;element name="filterOption" type="{http://infrastructure.tia.dk/schema/policy/v2/}FilterOptionCollection" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "inputToken",
    "policyLineNo",
    "policyLineSeqNo",
    "objectNoCollection",
    "pageSort",
    "filterOption"
})
@XmlRootElement(name = "getPolicyLineRequest", namespace = "http://infrastructure.tia.dk/schema/policy/v4/")
public class GetPolicyLineRequest {

    @XmlElement(namespace = "http://infrastructure.tia.dk/schema/policy/v4/", required = true)
    protected InputToken inputToken;
    @XmlElement(namespace = "http://infrastructure.tia.dk/schema/policy/v4/")
    @XmlSchemaType(name = "unsignedLong")
    protected BigInteger policyLineNo;
    @XmlElement(namespace = "http://infrastructure.tia.dk/schema/policy/v4/")
    @XmlSchemaType(name = "unsignedLong")
    protected BigInteger policyLineSeqNo;
    @XmlElement(namespace = "http://infrastructure.tia.dk/schema/policy/v4/")
    protected ObjectNoCollection objectNoCollection;
    @XmlElement(namespace = "http://infrastructure.tia.dk/schema/policy/v4/")
    protected PageSort pageSort;
    @XmlElement(namespace = "http://infrastructure.tia.dk/schema/policy/v4/")
    protected FilterOptionCollection filterOption;

    /**
     * Gets the value of the inputToken property.
     * 
     * @return
     *     possible object is
     *     {@link InputToken }
     *     
     */
    public InputToken getInputToken() {
        return inputToken;
    }

    /**
     * Sets the value of the inputToken property.
     * 
     * @param value
     *     allowed object is
     *     {@link InputToken }
     *     
     */
    public void setInputToken(InputToken value) {
        this.inputToken = value;
    }

    /**
     * Gets the value of the policyLineNo property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getPolicyLineNo() {
        return policyLineNo;
    }

    /**
     * Sets the value of the policyLineNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setPolicyLineNo(BigInteger value) {
        this.policyLineNo = value;
    }

    /**
     * Gets the value of the policyLineSeqNo property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getPolicyLineSeqNo() {
        return policyLineSeqNo;
    }

    /**
     * Sets the value of the policyLineSeqNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setPolicyLineSeqNo(BigInteger value) {
        this.policyLineSeqNo = value;
    }

    /**
     * Gets the value of the objectNoCollection property.
     * 
     * @return
     *     possible object is
     *     {@link ObjectNoCollection }
     *     
     */
    public ObjectNoCollection getObjectNoCollection() {
        return objectNoCollection;
    }

    /**
     * Sets the value of the objectNoCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjectNoCollection }
     *     
     */
    public void setObjectNoCollection(ObjectNoCollection value) {
        this.objectNoCollection = value;
    }

    /**
     * Gets the value of the pageSort property.
     * 
     * @return
     *     possible object is
     *     {@link PageSort }
     *     
     */
    public PageSort getPageSort() {
        return pageSort;
    }

    /**
     * Sets the value of the pageSort property.
     * 
     * @param value
     *     allowed object is
     *     {@link PageSort }
     *     
     */
    public void setPageSort(PageSort value) {
        this.pageSort = value;
    }

    /**
     * Gets the value of the filterOption property.
     * 
     * @return
     *     possible object is
     *     {@link FilterOptionCollection }
     *     
     */
    public FilterOptionCollection getFilterOption() {
        return filterOption;
    }

    /**
     * Sets the value of the filterOption property.
     * 
     * @param value
     *     allowed object is
     *     {@link FilterOptionCollection }
     *     
     */
    public void setFilterOption(FilterOptionCollection value) {
        this.filterOption = value;
    }

}
