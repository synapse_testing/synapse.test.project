
package za.co.ominsure.synapse.iwyze.lob.backend.search.payment.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="paymentCollection" type="{http://infrastructure.tia.dk/schema/account/v2/}PaymentCollection"/>
 *         &lt;element name="result" type="{http://infrastructure.tia.dk/schema/common/v2/}Result"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "paymentCollection",
    "result"
})
@XmlRootElement(name = "searchPaymentResponse")
public class SearchPaymentResponse {

    @XmlElement(required = true)
    protected PaymentCollection paymentCollection;
    @XmlElement(required = true)
    protected Result result;

    /**
     * Gets the value of the paymentCollection property.
     * 
     * @return
     *     possible object is
     *     {@link PaymentCollection }
     *     
     */
    public PaymentCollection getPaymentCollection() {
        return paymentCollection;
    }

    /**
     * Sets the value of the paymentCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link PaymentCollection }
     *     
     */
    public void setPaymentCollection(PaymentCollection value) {
        this.paymentCollection = value;
    }

    /**
     * Gets the value of the result property.
     * 
     * @return
     *     possible object is
     *     {@link Result }
     *     
     */
    public Result getResult() {
        return result;
    }

    /**
     * Sets the value of the result property.
     * 
     * @param value
     *     allowed object is
     *     {@link Result }
     *     
     */
    public void setResult(Result value) {
        this.result = value;
    }

}
