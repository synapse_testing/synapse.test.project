
package za.co.ominsure.synapse.iwyze.lob.backend.policy.vo;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for UIConfigRiskItemCollection complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="UIConfigRiskItemCollection">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="uiConfigRiskItem" type="{http://infrastructure.tia.dk/schema/policy/v4/}UIConfigRiskItem" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UIConfigRiskItemCollection", namespace = "http://infrastructure.tia.dk/schema/policy/v4/", propOrder = {
    "uiConfigRiskItems"
})
public class UIConfigRiskItemCollection {

    @XmlElement(name = "uiConfigRiskItem")
    protected List<UIConfigRiskItem> uiConfigRiskItems;

    /**
     * Gets the value of the uiConfigRiskItems property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the uiConfigRiskItems property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getUiConfigRiskItems().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link UIConfigRiskItem }
     * 
     * 
     */
    public List<UIConfigRiskItem> getUiConfigRiskItems() {
        if (uiConfigRiskItems == null) {
            uiConfigRiskItems = new ArrayList<UIConfigRiskItem>();
        }
        return this.uiConfigRiskItems;
    }

}
