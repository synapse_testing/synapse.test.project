
package za.co.ominsure.synapse.iwyze.lob.backend.policy.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="policyLine" type="{http://infrastructure.tia.dk/schema/policy/v4/}PolicyLine"/>
 *         &lt;element name="result" type="{http://infrastructure.tia.dk/schema/common/v2/}Result"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "policyLine",
    "result"
})
@XmlRootElement(name = "getPolicyLineResponse", namespace = "http://infrastructure.tia.dk/schema/policy/v4/")
public class GetPolicyLineResponse {

    @XmlElement(namespace = "http://infrastructure.tia.dk/schema/policy/v4/", required = true, nillable = true)
    protected PolicyLine policyLine;
    @XmlElement(namespace = "http://infrastructure.tia.dk/schema/policy/v4/", required = true)
    protected Result result;

    /**
     * Gets the value of the policyLine property.
     * 
     * @return
     *     possible object is
     *     {@link PolicyLine }
     *     
     */
    public PolicyLine getPolicyLine() {
        return policyLine;
    }

    /**
     * Sets the value of the policyLine property.
     * 
     * @param value
     *     allowed object is
     *     {@link PolicyLine }
     *     
     */
    public void setPolicyLine(PolicyLine value) {
        this.policyLine = value;
    }

    /**
     * Gets the value of the result property.
     * 
     * @return
     *     possible object is
     *     {@link Result }
     *     
     */
    public Result getResult() {
        return result;
    }

    /**
     * Sets the value of the result property.
     * 
     * @param value
     *     allowed object is
     *     {@link Result }
     *     
     */
    public void setResult(Result value) {
        this.result = value;
    }

}
