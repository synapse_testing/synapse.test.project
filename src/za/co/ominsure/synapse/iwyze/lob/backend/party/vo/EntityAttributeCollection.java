
package za.co.ominsure.synapse.iwyze.lob.backend.party.vo;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for EntityAttributeCollection complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="EntityAttributeCollection">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="varcharAttribute" type="{http://infrastructure.tia.dk/schema/common/v2/}VarcharAttribute" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="numberAttribute" type="{http://infrastructure.tia.dk/schema/common/v2/}NumberAttribute" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="dateAttribute" type="{http://infrastructure.tia.dk/schema/common/v2/}DateAttribute" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EntityAttributeCollection", namespace = "http://infrastructure.tia.dk/schema/common/v2/", propOrder = {
    "varcharAttribute",
    "numberAttribute",
    "dateAttribute"
})
public class EntityAttributeCollection {

    protected List<VarcharAttribute> varcharAttribute;
    protected List<NumberAttribute> numberAttribute;
    protected List<DateAttribute> dateAttribute;

    /**
     * Gets the value of the varcharAttribute property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the varcharAttribute property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getVarcharAttribute().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link VarcharAttribute }
     * 
     * 
     */
    public List<VarcharAttribute> getVarcharAttribute() {
        if (varcharAttribute == null) {
            varcharAttribute = new ArrayList<VarcharAttribute>();
        }
        return this.varcharAttribute;
    }

    /**
     * Gets the value of the numberAttribute property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the numberAttribute property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getNumberAttribute().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link NumberAttribute }
     * 
     * 
     */
    public List<NumberAttribute> getNumberAttribute() {
        if (numberAttribute == null) {
            numberAttribute = new ArrayList<NumberAttribute>();
        }
        return this.numberAttribute;
    }

    /**
     * Gets the value of the dateAttribute property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the dateAttribute property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDateAttribute().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DateAttribute }
     * 
     * 
     */
    public List<DateAttribute> getDateAttribute() {
        if (dateAttribute == null) {
            dateAttribute = new ArrayList<DateAttribute>();
        }
        return this.dateAttribute;
    }

}
