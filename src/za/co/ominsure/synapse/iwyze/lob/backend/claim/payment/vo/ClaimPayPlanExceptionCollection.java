
package za.co.ominsure.synapse.iwyze.lob.backend.claim.payment.vo;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ClaimPayPlanExceptionCollection complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ClaimPayPlanExceptionCollection">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="claimPayPlanException" type="{http://infrastructure.tia.dk/schema/claim/v2/}ClaimPayPlanException" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ClaimPayPlanExceptionCollection", propOrder = {
    "claimPayPlanExceptions"
})
public class ClaimPayPlanExceptionCollection {

    @XmlElement(name = "claimPayPlanException")
    protected List<ClaimPayPlanException> claimPayPlanExceptions;

    /**
     * Gets the value of the claimPayPlanExceptions property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the claimPayPlanExceptions property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getClaimPayPlanExceptions().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ClaimPayPlanException }
     * 
     * 
     */
    public List<ClaimPayPlanException> getClaimPayPlanExceptions() {
        if (claimPayPlanExceptions == null) {
            claimPayPlanExceptions = new ArrayList<ClaimPayPlanException>();
        }
        return this.claimPayPlanExceptions;
    }

}
