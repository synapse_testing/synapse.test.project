package za.co.ominsure.synapse.iwyze.lob.backend.vo;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;

@XmlType(name = "SiteNameEnum", namespace = "http://synapse.ominsure.co.za/webservice/iwyze")
@XmlEnum
public enum SiteNameEnum {

	@XmlEnumValue("1")
	TOP_SITE("1"),

	@XmlEnumValue("2")
	SHARED("2"),

	@XmlEnumValue("3")
	INTERNAL("3"),

	@XmlEnumValue("4")
	AFFINITY("4"),

	@XmlEnumValue("5")
	IWYZE("5"),

	@XmlEnumValue("6")
	SUREWISE("6"),

	@XmlEnumValue("7")
	DA("7"),

	@XmlEnumValue("8")
	FNB("8"),

	@XmlEnumValue("9")
	NEDBANK("9"),

	@XmlEnumValue("10")
	TFG("10"),

	@XmlEnumValue("11")
	MOTORSURE("11"),

	@XmlEnumValue("12")
	OLDMUTUAL("12"),
	
	@XmlEnumValue("13")
	PINEAPPLE("13"),

	@XmlEnumValue("14")
	IEMAS("14");

	private final String value;

	SiteNameEnum(String v) {
		value = v;
	}

	public String value() {
		return value;
	}

	public static SiteNameEnum fromValue(String v) {
		for (SiteNameEnum c : SiteNameEnum.values()) {
			if (c.value.equalsIgnoreCase(v)) {
				return c;
			}
		}
		throw new IllegalArgumentException(v);
	}
}
