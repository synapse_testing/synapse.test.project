
package za.co.ominsure.synapse.iwyze.lob.backend.caseItem.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Attachment complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Attachment">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element name="caseItemAttBlob" type="{http://infrastructure.tia.dk/schema/case/v3/}CaseItemAttBlob" minOccurs="0"/>
 *         &lt;element name="caseItemAttClob" type="{http://infrastructure.tia.dk/schema/case/v3/}CaseItemAttClob" minOccurs="0"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Attachment", namespace = "http://infrastructure.tia.dk/schema/case/v3/", propOrder = {
    "caseItemAttClob",
    "caseItemAttBlob"
})
public class Attachment {

    protected CaseItemAttClob caseItemAttClob;
    protected CaseItemAttBlob caseItemAttBlob;

    /**
     * Gets the value of the caseItemAttClob property.
     * 
     * @return
     *     possible object is
     *     {@link CaseItemAttClob }
     *     
     */
    public CaseItemAttClob getCaseItemAttClob() {
        return caseItemAttClob;
    }

    /**
     * Sets the value of the caseItemAttClob property.
     * 
     * @param value
     *     allowed object is
     *     {@link CaseItemAttClob }
     *     
     */
    public void setCaseItemAttClob(CaseItemAttClob value) {
        this.caseItemAttClob = value;
    }

    /**
     * Gets the value of the caseItemAttBlob property.
     * 
     * @return
     *     possible object is
     *     {@link CaseItemAttBlob }
     *     
     */
    public CaseItemAttBlob getCaseItemAttBlob() {
        return caseItemAttBlob;
    }

    /**
     * Sets the value of the caseItemAttBlob property.
     * 
     * @param value
     *     allowed object is
     *     {@link CaseItemAttBlob }
     *     
     */
    public void setCaseItemAttBlob(CaseItemAttBlob value) {
        this.caseItemAttBlob = value;
    }

}
