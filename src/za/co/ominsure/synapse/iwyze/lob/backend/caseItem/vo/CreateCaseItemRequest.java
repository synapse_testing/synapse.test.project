
package za.co.ominsure.synapse.iwyze.lob.backend.caseItem.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="inputToken" type="{http://infrastructure.tia.dk/schema/common/v2/}InputToken"/>
 *         &lt;element name="caseItem" type="{http://infrastructure.tia.dk/schema/case/v3/}CaseItem"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "inputToken",
    "caseItem"
})
@XmlRootElement(name = "createCaseItemRequest", namespace = "http://infrastructure.tia.dk/schema/case/v3/")
public class CreateCaseItemRequest {

    @XmlElement(namespace = "http://infrastructure.tia.dk/schema/case/v3/", required = true)
    protected InputToken inputToken;
    @XmlElement(namespace = "http://infrastructure.tia.dk/schema/case/v3/", required = true)
    protected CaseItem caseItem;

    /**
     * Gets the value of the inputToken property.
     * 
     * @return
     *     possible object is
     *     {@link InputToken }
     *     
     */
    public InputToken getInputToken() {
        return inputToken;
    }

    /**
     * Sets the value of the inputToken property.
     * 
     * @param value
     *     allowed object is
     *     {@link InputToken }
     *     
     */
    public void setInputToken(InputToken value) {
        this.inputToken = value;
    }

    /**
     * Gets the value of the caseItem property.
     * 
     * @return
     *     possible object is
     *     {@link CaseItem }
     *     
     */
    public CaseItem getCaseItem() {
        return caseItem;
    }

    /**
     * Sets the value of the caseItem property.
     * 
     * @param value
     *     allowed object is
     *     {@link CaseItem }
     *     
     */
    public void setCaseItem(CaseItem value) {
        this.caseItem = value;
    }

}
