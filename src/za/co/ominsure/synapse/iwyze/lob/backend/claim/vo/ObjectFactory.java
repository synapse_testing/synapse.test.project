
package za.co.ominsure.synapse.iwyze.lob.backend.claim.vo;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the za.co.ominsure.synapse.iwyze.lob.backend.claim.vo package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: za.co.ominsure.synapse.iwyze.lob.backend.claim.vo
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link AddClaimNotificationRequest }
     * 
     */
    public AddClaimNotificationRequest createAddClaimNotificationRequest() {
        return new AddClaimNotificationRequest();
    }

    /**
     * Create an instance of {@link InputToken }
     * 
     */
    public InputToken createInputToken() {
        return new InputToken();
    }

    /**
     * Create an instance of {@link Claim }
     * 
     */
    public Claim createClaim() {
        return new Claim();
    }

    /**
     * Create an instance of {@link CreateClaimNotificationResponse }
     * 
     */
    public CreateClaimNotificationResponse createCreateClaimNotificationResponse() {
        return new CreateClaimNotificationResponse();
    }

    /**
     * Create an instance of {@link ClaimEvent }
     * 
     */
    public ClaimEvent createClaimEvent() {
        return new ClaimEvent();
    }

    /**
     * Create an instance of {@link Result }
     * 
     */
    public Result createResult() {
        return new Result();
    }

    /**
     * Create an instance of {@link GetClaimRequest }
     * 
     */
    public GetClaimRequest createGetClaimRequest() {
        return new GetClaimRequest();
    }

    /**
     * Create an instance of {@link ModifyStatusOpenClaimRequest }
     * 
     */
    public ModifyStatusOpenClaimRequest createModifyStatusOpenClaimRequest() {
        return new ModifyStatusOpenClaimRequest();
    }

    /**
     * Create an instance of {@link CreateClaimNotificationRequest }
     * 
     */
    public CreateClaimNotificationRequest createCreateClaimNotificationRequest() {
        return new CreateClaimNotificationRequest();
    }

    /**
     * Create an instance of {@link ModifyStatusOpenClaimResponse }
     * 
     */
    public ModifyStatusOpenClaimResponse createModifyStatusOpenClaimResponse() {
        return new ModifyStatusOpenClaimResponse();
    }

    /**
     * Create an instance of {@link CreateClaimResponse }
     * 
     */
    public CreateClaimResponse createCreateClaimResponse() {
        return new CreateClaimResponse();
    }

    /**
     * Create an instance of {@link AddClaimNotificationResponse }
     * 
     */
    public AddClaimNotificationResponse createAddClaimNotificationResponse() {
        return new AddClaimNotificationResponse();
    }

    /**
     * Create an instance of {@link AddClaimResponse }
     * 
     */
    public AddClaimResponse createAddClaimResponse() {
        return new AddClaimResponse();
    }

    /**
     * Create an instance of {@link AddClaimRequest }
     * 
     */
    public AddClaimRequest createAddClaimRequest() {
        return new AddClaimRequest();
    }

    /**
     * Create an instance of {@link ModifyClaimResponse }
     * 
     */
    public ModifyClaimResponse createModifyClaimResponse() {
        return new ModifyClaimResponse();
    }

    /**
     * Create an instance of {@link CreateClaimRequest }
     * 
     */
    public CreateClaimRequest createCreateClaimRequest() {
        return new CreateClaimRequest();
    }

    /**
     * Create an instance of {@link ModifyClaimRequest }
     * 
     */
    public ModifyClaimRequest createModifyClaimRequest() {
        return new ModifyClaimRequest();
    }

    /**
     * Create an instance of {@link GetClaimResponse }
     * 
     */
    public GetClaimResponse createGetClaimResponse() {
        return new GetClaimResponse();
    }

    /**
     * Create an instance of {@link SubclaimCollection }
     * 
     */
    public SubclaimCollection createSubclaimCollection() {
        return new SubclaimCollection();
    }

    /**
     * Create an instance of {@link ClaimPaymentItem }
     * 
     */
    public ClaimPaymentItem createClaimPaymentItem() {
        return new ClaimPaymentItem();
    }

    /**
     * Create an instance of {@link SearchTokenClaim }
     * 
     */
    public SearchTokenClaim createSearchTokenClaim() {
        return new SearchTokenClaim();
    }

    /**
     * Create an instance of {@link ClaimAccItem }
     * 
     */
    public ClaimAccItem createClaimAccItem() {
        return new ClaimAccItem();
    }

    /**
     * Create an instance of {@link SearchTokenSubclaim }
     * 
     */
    public SearchTokenSubclaim createSearchTokenSubclaim() {
        return new SearchTokenSubclaim();
    }

    /**
     * Create an instance of {@link ClaimEventCollection }
     * 
     */
    public ClaimEventCollection createClaimEventCollection() {
        return new ClaimEventCollection();
    }

    /**
     * Create an instance of {@link ThirdPartyCollection }
     * 
     */
    public ThirdPartyCollection createThirdPartyCollection() {
        return new ThirdPartyCollection();
    }

    /**
     * Create an instance of {@link ClaimMajorEventCollection }
     * 
     */
    public ClaimMajorEventCollection createClaimMajorEventCollection() {
        return new ClaimMajorEventCollection();
    }

    /**
     * Create an instance of {@link Subclaim }
     * 
     */
    public Subclaim createSubclaim() {
        return new Subclaim();
    }

    /**
     * Create an instance of {@link AnswerSet }
     * 
     */
    public AnswerSet createAnswerSet() {
        return new AnswerSet();
    }

    /**
     * Create an instance of {@link SearchTokenNrc }
     * 
     */
    public SearchTokenNrc createSearchTokenNrc() {
        return new SearchTokenNrc();
    }

    /**
     * Create an instance of {@link ClaimNrcEvent }
     * 
     */
    public ClaimNrcEvent createClaimNrcEvent() {
        return new ClaimNrcEvent();
    }

    /**
     * Create an instance of {@link AnswerCollection }
     * 
     */
    public AnswerCollection createAnswerCollection() {
        return new AnswerCollection();
    }

    /**
     * Create an instance of {@link SearchTokenClaimHist }
     * 
     */
    public SearchTokenClaimHist createSearchTokenClaimHist() {
        return new SearchTokenClaimHist();
    }

    /**
     * Create an instance of {@link ClaimItemCollection }
     * 
     */
    public ClaimItemCollection createClaimItemCollection() {
        return new ClaimItemCollection();
    }

    /**
     * Create an instance of {@link SearchTokenMajor }
     * 
     */
    public SearchTokenMajor createSearchTokenMajor() {
        return new SearchTokenMajor();
    }

    /**
     * Create an instance of {@link ClaimHistoryCollection }
     * 
     */
    public ClaimHistoryCollection createClaimHistoryCollection() {
        return new ClaimHistoryCollection();
    }

    /**
     * Create an instance of {@link Question }
     * 
     */
    public Question createQuestion() {
        return new Question();
    }

    /**
     * Create an instance of {@link ClaimPayPlanExceptionCollection }
     * 
     */
    public ClaimPayPlanExceptionCollection createClaimPayPlanExceptionCollection() {
        return new ClaimPayPlanExceptionCollection();
    }

    /**
     * Create an instance of {@link ClaimMajorEvent }
     * 
     */
    public ClaimMajorEvent createClaimMajorEvent() {
        return new ClaimMajorEvent();
    }

    /**
     * Create an instance of {@link ThirdParty }
     * 
     */
    public ThirdParty createThirdParty() {
        return new ThirdParty();
    }

    /**
     * Create an instance of {@link ClaimNrcEventCollection }
     * 
     */
    public ClaimNrcEventCollection createClaimNrcEventCollection() {
        return new ClaimNrcEventCollection();
    }

    /**
     * Create an instance of {@link SearchTokenClaimItem }
     * 
     */
    public SearchTokenClaimItem createSearchTokenClaimItem() {
        return new SearchTokenClaimItem();
    }

    /**
     * Create an instance of {@link ClaimPaymentItemCollection }
     * 
     */
    public ClaimPaymentItemCollection createClaimPaymentItemCollection() {
        return new ClaimPaymentItemCollection();
    }

    /**
     * Create an instance of {@link ClaimTaskCollection }
     * 
     */
    public ClaimTaskCollection createClaimTaskCollection() {
        return new ClaimTaskCollection();
    }

    /**
     * Create an instance of {@link LanguageCollection }
     * 
     */
    public LanguageCollection createLanguageCollection() {
        return new LanguageCollection();
    }

    /**
     * Create an instance of {@link ClaimCatastrophe }
     * 
     */
    public ClaimCatastrophe createClaimCatastrophe() {
        return new ClaimCatastrophe();
    }

    /**
     * Create an instance of {@link ClaimPaymentPlan }
     * 
     */
    public ClaimPaymentPlan createClaimPaymentPlan() {
        return new ClaimPaymentPlan();
    }

    /**
     * Create an instance of {@link ClaimAccItemCollection }
     * 
     */
    public ClaimAccItemCollection createClaimAccItemCollection() {
        return new ClaimAccItemCollection();
    }

    /**
     * Create an instance of {@link ValueTranslationCollection }
     * 
     */
    public ValueTranslationCollection createValueTranslationCollection() {
        return new ValueTranslationCollection();
    }

    /**
     * Create an instance of {@link ClaimHistory }
     * 
     */
    public ClaimHistory createClaimHistory() {
        return new ClaimHistory();
    }

    /**
     * Create an instance of {@link Answer }
     * 
     */
    public Answer createAnswer() {
        return new Answer();
    }

    /**
     * Create an instance of {@link ClaimReferralCollection }
     * 
     */
    public ClaimReferralCollection createClaimReferralCollection() {
        return new ClaimReferralCollection();
    }

    /**
     * Create an instance of {@link QuestionClass }
     * 
     */
    public QuestionClass createQuestionClass() {
        return new QuestionClass();
    }

    /**
     * Create an instance of {@link ClaimPayPlanException }
     * 
     */
    public ClaimPayPlanException createClaimPayPlanException() {
        return new ClaimPayPlanException();
    }

    /**
     * Create an instance of {@link ClaimPaymentPlanItemCollection }
     * 
     */
    public ClaimPaymentPlanItemCollection createClaimPaymentPlanItemCollection() {
        return new ClaimPaymentPlanItemCollection();
    }

    /**
     * Create an instance of {@link ClaimReferral }
     * 
     */
    public ClaimReferral createClaimReferral() {
        return new ClaimReferral();
    }

    /**
     * Create an instance of {@link ClaimTask }
     * 
     */
    public ClaimTask createClaimTask() {
        return new ClaimTask();
    }

    /**
     * Create an instance of {@link ClaimItem }
     * 
     */
    public ClaimItem createClaimItem() {
        return new ClaimItem();
    }

    /**
     * Create an instance of {@link QuestionCollection }
     * 
     */
    public QuestionCollection createQuestionCollection() {
        return new QuestionCollection();
    }

    /**
     * Create an instance of {@link ClaimCollection }
     * 
     */
    public ClaimCollection createClaimCollection() {
        return new ClaimCollection();
    }

    /**
     * Create an instance of {@link ClaimPaymentPlanItem }
     * 
     */
    public ClaimPaymentPlanItem createClaimPaymentPlanItem() {
        return new ClaimPaymentPlanItem();
    }

    /**
     * Create an instance of {@link AnswerSetCollection }
     * 
     */
    public AnswerSetCollection createAnswerSetCollection() {
        return new AnswerSetCollection();
    }

    /**
     * Create an instance of {@link FaultMessageDetail }
     * 
     */
    public FaultMessageDetail createFaultMessageDetail() {
        return new FaultMessageDetail();
    }

    /**
     * Create an instance of {@link FlexfieldCollection }
     * 
     */
    public FlexfieldCollection createFlexfieldCollection() {
        return new FlexfieldCollection();
    }

    /**
     * Create an instance of {@link ValueTranslationPolicy }
     * 
     */
    public ValueTranslationPolicy createValueTranslationPolicy() {
        return new ValueTranslationPolicy();
    }

    /**
     * Create an instance of {@link NumberAttribute }
     * 
     */
    public NumberAttribute createNumberAttribute() {
        return new NumberAttribute();
    }

    /**
     * Create an instance of {@link KeyPair }
     * 
     */
    public KeyPair createKeyPair() {
        return new KeyPair();
    }

    /**
     * Create an instance of {@link TranslatedCode }
     * 
     */
    public TranslatedCode createTranslatedCode() {
        return new TranslatedCode();
    }

    /**
     * Create an instance of {@link ValueTranslationCollection2 }
     * 
     */
    public ValueTranslationCollection2 createValueTranslationCollection2() {
        return new ValueTranslationCollection2();
    }

    /**
     * Create an instance of {@link VersionToken }
     * 
     */
    public VersionToken createVersionToken() {
        return new VersionToken();
    }

    /**
     * Create an instance of {@link StringCollection }
     * 
     */
    public StringCollection createStringCollection() {
        return new StringCollection();
    }

    /**
     * Create an instance of {@link Attribute }
     * 
     */
    public Attribute createAttribute() {
        return new Attribute();
    }

    /**
     * Create an instance of {@link ValueTranslationClaim }
     * 
     */
    public ValueTranslationClaim createValueTranslationClaim() {
        return new ValueTranslationClaim();
    }

    /**
     * Create an instance of {@link VarcharAttribute }
     * 
     */
    public VarcharAttribute createVarcharAttribute() {
        return new VarcharAttribute();
    }

    /**
     * Create an instance of {@link MessageDetail }
     * 
     */
    public MessageDetail createMessageDetail() {
        return new MessageDetail();
    }

    /**
     * Create an instance of {@link KeyPairCollection }
     * 
     */
    public KeyPairCollection createKeyPairCollection() {
        return new KeyPairCollection();
    }

    /**
     * Create an instance of {@link Email }
     * 
     */
    public Email createEmail() {
        return new Email();
    }

    /**
     * Create an instance of {@link TranslatedCodeCollection }
     * 
     */
    public TranslatedCodeCollection createTranslatedCodeCollection() {
        return new TranslatedCodeCollection();
    }

    /**
     * Create an instance of {@link Flexfield }
     * 
     */
    public Flexfield createFlexfield() {
        return new Flexfield();
    }

    /**
     * Create an instance of {@link DateAttribute }
     * 
     */
    public DateAttribute createDateAttribute() {
        return new DateAttribute();
    }

    /**
     * Create an instance of {@link NumberCollection }
     * 
     */
    public NumberCollection createNumberCollection() {
        return new NumberCollection();
    }

    /**
     * Create an instance of {@link Value }
     * 
     */
    public Value createValue() {
        return new Value();
    }

    /**
     * Create an instance of {@link SiteSpecifics }
     * 
     */
    public SiteSpecifics createSiteSpecifics() {
        return new SiteSpecifics();
    }

    /**
     * Create an instance of {@link ValueTranslationPolicyCollection }
     * 
     */
    public ValueTranslationPolicyCollection createValueTranslationPolicyCollection() {
        return new ValueTranslationPolicyCollection();
    }

    /**
     * Create an instance of {@link ValueTranslation }
     * 
     */
    public ValueTranslation createValueTranslation() {
        return new ValueTranslation();
    }

    /**
     * Create an instance of {@link MessageCollection }
     * 
     */
    public MessageCollection createMessageCollection() {
        return new MessageCollection();
    }

    /**
     * Create an instance of {@link Number }
     * 
     */
    public Number createNumber() {
        return new Number();
    }

    /**
     * Create an instance of {@link CountrySpecifics }
     * 
     */
    public CountrySpecifics createCountrySpecifics() {
        return new CountrySpecifics();
    }

    /**
     * Create an instance of {@link EntityAttributeCollection }
     * 
     */
    public EntityAttributeCollection createEntityAttributeCollection() {
        return new EntityAttributeCollection();
    }

    /**
     * Create an instance of {@link PageSort }
     * 
     */
    public PageSort createPageSort() {
        return new PageSort();
    }

    /**
     * Create an instance of {@link ValueTranslationClaimCollection }
     * 
     */
    public ValueTranslationClaimCollection createValueTranslationClaimCollection() {
        return new ValueTranslationClaimCollection();
    }

}
