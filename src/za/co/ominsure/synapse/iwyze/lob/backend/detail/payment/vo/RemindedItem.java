
package za.co.ominsure.synapse.iwyze.lob.backend.detail.payment.vo;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * Is mapped into TIA metadata type "obj_reminded_item".
 * 
 * <p>Java class for RemindedItem complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RemindedItem">
 *   &lt;complexContent>
 *     &lt;extension base="{http://infrastructure.tia.dk/schema/common/v2/}TIAObject">
 *       &lt;sequence>
 *         &lt;element name="accountItemNo" type="{http://infrastructure.tia.dk/schema/common/v2/}seqNo" minOccurs="0"/>
 *         &lt;element name="remindedAmount" type="{http://infrastructure.tia.dk/schema/common/v2/}amount" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RemindedItem", propOrder = {
    "accountItemNo",
    "remindedAmount"
})
public class RemindedItem
    extends TIAObject
{

    @XmlElement(nillable = true)
    @XmlSchemaType(name = "unsignedLong")
    protected Long accountItemNo;
    @XmlElement(nillable = true)
    protected BigDecimal remindedAmount;

    /**
     * Gets the value of the accountItemNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getAccountItemNo() {
        return accountItemNo;
    }

    /**
     * Sets the value of the accountItemNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setAccountItemNo(Long value) {
        this.accountItemNo = value;
    }

    /**
     * Gets the value of the remindedAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRemindedAmount() {
        return remindedAmount;
    }

    /**
     * Sets the value of the remindedAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRemindedAmount(BigDecimal value) {
        this.remindedAmount = value;
    }

}
