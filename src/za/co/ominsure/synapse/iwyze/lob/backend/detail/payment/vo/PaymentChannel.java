
package za.co.ominsure.synapse.iwyze.lob.backend.detail.payment.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * Is mapped into TIA metadata type "obj_payment_channel".
 * 
 * <p>Java class for PaymentChannel complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PaymentChannel">
 *   &lt;complexContent>
 *     &lt;extension base="{http://infrastructure.tia.dk/schema/common/v2/}TIAObject">
 *       &lt;sequence>
 *         &lt;element name="paymentChannel" type="{http://infrastructure.tia.dk/schema/account/v2/}paymentChannelType"/>
 *         &lt;element name="title" type="{http://infrastructure.tia.dk/schema/account/v2/}paymentChannelTitle" minOccurs="0"/>
 *         &lt;element name="paymentMethod" type="{http://infrastructure.tia.dk/schema/common/v2/}paymentMethod"/>
 *         &lt;element name="currencyCode" type="{http://infrastructure.tia.dk/schema/common/v2/}currencyCode"/>
 *         &lt;element name="bankIdNo" type="{http://infrastructure.tia.dk/schema/account/v2/}bankIdNo" minOccurs="0"/>
 *         &lt;element name="centerCode" type="{http://infrastructure.tia.dk/schema/common/v2/}centerCode" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PaymentChannel", propOrder = {
    "paymentChannel",
    "title",
    "paymentMethod",
    "currencyCode",
    "bankIdNo",
    "centerCode"
})
public class PaymentChannel
    extends TIAObject
{

    @XmlElement(required = true)
    protected String paymentChannel;
    @XmlElement(nillable = true)
    protected String title;
    @XmlElement(required = true)
    protected String paymentMethod;
    @XmlElement(required = true)
    protected String currencyCode;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "unsignedLong")
    protected Long bankIdNo;
    @XmlElement(nillable = true)
    protected String centerCode;

    /**
     * Gets the value of the paymentChannel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaymentChannel() {
        return paymentChannel;
    }

    /**
     * Sets the value of the paymentChannel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaymentChannel(String value) {
        this.paymentChannel = value;
    }

    /**
     * Gets the value of the title property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets the value of the title property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTitle(String value) {
        this.title = value;
    }

    /**
     * Gets the value of the paymentMethod property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaymentMethod() {
        return paymentMethod;
    }

    /**
     * Sets the value of the paymentMethod property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaymentMethod(String value) {
        this.paymentMethod = value;
    }

    /**
     * Gets the value of the currencyCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrencyCode() {
        return currencyCode;
    }

    /**
     * Sets the value of the currencyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrencyCode(String value) {
        this.currencyCode = value;
    }

    /**
     * Gets the value of the bankIdNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getBankIdNo() {
        return bankIdNo;
    }

    /**
     * Sets the value of the bankIdNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setBankIdNo(Long value) {
        this.bankIdNo = value;
    }

    /**
     * Gets the value of the centerCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCenterCode() {
        return centerCode;
    }

    /**
     * Sets the value of the centerCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCenterCode(String value) {
        this.centerCode = value;
    }

}
