
package za.co.ominsure.synapse.iwyze.lob.backend.search.payment.vo;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * Is mapped into TIA metadata type "tab_account_item".
 * 
 * <p>Java class for AccountItemCollection complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AccountItemCollection">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="accountItem" type="{http://infrastructure.tia.dk/schema/account/v2/}AccountItem" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AccountItemCollection", propOrder = {
    "accountItem"
})
public class AccountItemCollection {

    protected List<AccountItem> accountItem;

    /**
     * Gets the value of the accountItem property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the accountItem property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAccountItem().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AccountItem }
     * 
     * 
     */
    public List<AccountItem> getAccountItem() {
        if (accountItem == null) {
            accountItem = new ArrayList<AccountItem>();
        }
        return this.accountItem;
    }

}
