
package za.co.ominsure.synapse.iwyze.lob.backend.interactive.claim.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Additional comments
 * 
 * <p>Java class for ClaimPaymentItem complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ClaimPaymentItem">
 *   &lt;complexContent>
 *     &lt;extension base="{http://infrastructure.tia.dk/schema/common/v2/}TIAObject">
 *       &lt;sequence>
 *         &lt;element name="claPaymentItemNo" type="{http://infrastructure.tia.dk/schema/common/v2/}long10" minOccurs="0"/>
 *         &lt;element name="paymentType" type="{http://infrastructure.tia.dk/schema/common/v2/}string10" minOccurs="0"/>
 *         &lt;element name="currencyCode" type="{http://infrastructure.tia.dk/schema/common/v2/}string3" minOccurs="0"/>
 *         &lt;element name="currencyAmount" type="{http://infrastructure.tia.dk/schema/common/v2/}doublewithoutrestriction" minOccurs="0"/>
 *         &lt;element name="amount" type="{http://infrastructure.tia.dk/schema/common/v2/}doublewithoutrestriction" minOccurs="0"/>
 *         &lt;element name="description" type="{http://infrastructure.tia.dk/schema/common/v2/}string2000" minOccurs="0"/>
 *         &lt;element name="taxPct" type="{http://infrastructure.tia.dk/schema/common/v2/}doublewithoutrestriction" minOccurs="0"/>
 *         &lt;element name="taxDeductibles" type="{http://infrastructure.tia.dk/schema/common/v2/}doublewithoutrestriction" minOccurs="0"/>
 *         &lt;element name="taxAmount" type="{http://infrastructure.tia.dk/schema/common/v2/}doublewithoutrestriction" minOccurs="0"/>
 *         &lt;element name="claItemSeqNo" type="{http://infrastructure.tia.dk/schema/common/v2/}long10" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ClaimPaymentItem", propOrder = {
    "claPaymentItemNo",
    "paymentType",
    "currencyCode",
    "currencyAmount",
    "amount",
    "description",
    "taxPct",
    "taxDeductibles",
    "taxAmount",
    "claItemSeqNo"
})
public class ClaimPaymentItem
    extends TIAObject
{

    @XmlElement(nillable = true)
    protected Long claPaymentItemNo;
    @XmlElement(nillable = true)
    protected String paymentType;
    @XmlElement(nillable = true)
    protected String currencyCode;
    @XmlElement(nillable = true)
    protected Double currencyAmount;
    @XmlElement(nillable = true)
    protected Double amount;
    @XmlElement(nillable = true)
    protected String description;
    @XmlElement(nillable = true)
    protected Double taxPct;
    @XmlElement(nillable = true)
    protected Double taxDeductibles;
    @XmlElement(nillable = true)
    protected Double taxAmount;
    @XmlElement(nillable = true)
    protected Long claItemSeqNo;

    /**
     * Gets the value of the claPaymentItemNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getClaPaymentItemNo() {
        return claPaymentItemNo;
    }

    /**
     * Sets the value of the claPaymentItemNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setClaPaymentItemNo(Long value) {
        this.claPaymentItemNo = value;
    }

    /**
     * Gets the value of the paymentType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaymentType() {
        return paymentType;
    }

    /**
     * Sets the value of the paymentType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaymentType(String value) {
        this.paymentType = value;
    }

    /**
     * Gets the value of the currencyCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrencyCode() {
        return currencyCode;
    }

    /**
     * Sets the value of the currencyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrencyCode(String value) {
        this.currencyCode = value;
    }

    /**
     * Gets the value of the currencyAmount property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getCurrencyAmount() {
        return currencyAmount;
    }

    /**
     * Sets the value of the currencyAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setCurrencyAmount(Double value) {
        this.currencyAmount = value;
    }

    /**
     * Gets the value of the amount property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getAmount() {
        return amount;
    }

    /**
     * Sets the value of the amount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setAmount(Double value) {
        this.amount = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the taxPct property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getTaxPct() {
        return taxPct;
    }

    /**
     * Sets the value of the taxPct property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setTaxPct(Double value) {
        this.taxPct = value;
    }

    /**
     * Gets the value of the taxDeductibles property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getTaxDeductibles() {
        return taxDeductibles;
    }

    /**
     * Sets the value of the taxDeductibles property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setTaxDeductibles(Double value) {
        this.taxDeductibles = value;
    }

    /**
     * Gets the value of the taxAmount property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getTaxAmount() {
        return taxAmount;
    }

    /**
     * Sets the value of the taxAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setTaxAmount(Double value) {
        this.taxAmount = value;
    }

    /**
     * Gets the value of the claItemSeqNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getClaItemSeqNo() {
        return claItemSeqNo;
    }

    /**
     * Sets the value of the claItemSeqNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setClaItemSeqNo(Long value) {
        this.claItemSeqNo = value;
    }

}
