
package za.co.ominsure.synapse.iwyze.lob.backend.claim.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="inputToken" type="{http://infrastructure.tia.dk/schema/common/v2/}InputToken"/>
 *         &lt;element name="claimEvent" type="{http://infrastructure.tia.dk/schema/claim/v2/}ClaimEvent" minOccurs="0"/>
 *         &lt;element name="lock" type="{http://infrastructure.tia.dk/schema/common/v2/}tiaYN" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "inputToken",
    "claimEvent",
    "lock"
})
@XmlRootElement(name = "createClaimRequest")
public class CreateClaimRequest {

    @XmlElement(required = true)
    protected InputToken inputToken;
    protected ClaimEvent claimEvent;
    @XmlElement(nillable = true)
    protected String lock;

    /**
     * Gets the value of the inputToken property.
     * 
     * @return
     *     possible object is
     *     {@link InputToken }
     *     
     */
    public InputToken getInputToken() {
        return inputToken;
    }

    /**
     * Sets the value of the inputToken property.
     * 
     * @param value
     *     allowed object is
     *     {@link InputToken }
     *     
     */
    public void setInputToken(InputToken value) {
        this.inputToken = value;
    }

    /**
     * Gets the value of the claimEvent property.
     * 
     * @return
     *     possible object is
     *     {@link ClaimEvent }
     *     
     */
    public ClaimEvent getClaimEvent() {
        return claimEvent;
    }

    /**
     * Sets the value of the claimEvent property.
     * 
     * @param value
     *     allowed object is
     *     {@link ClaimEvent }
     *     
     */
    public void setClaimEvent(ClaimEvent value) {
        this.claimEvent = value;
    }

    /**
     * Gets the value of the lock property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLock() {
        return lock;
    }

    /**
     * Sets the value of the lock property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLock(String value) {
        this.lock = value;
    }

}
