
package za.co.ominsure.synapse.iwyze.lob.backend.exists.policy.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TIA.OBJ_COUNTRY_SPECIFICS complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TIA.OBJ_COUNTRY_SPECIFICS">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DUMMY_ATTRIBUTE" type="{http://xmlns.oracle.com/pcbpel/adapter/db/sp/checkExistingPolicyReference}TIA.TAB_ATTRIBUTE" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TIA.OBJ_COUNTRY_SPECIFICS", propOrder = {
    "dummyattribute"
})
public class TIAOBJCOUNTRYSPECIFICS {

    @XmlElement(name = "DUMMY_ATTRIBUTE", nillable = true)
    protected TIATABATTRIBUTE dummyattribute;

    /**
     * Gets the value of the dummyattribute property.
     * 
     * @return
     *     possible object is
     *     {@link TIATABATTRIBUTE }
     *     
     */
    public TIATABATTRIBUTE getDUMMYATTRIBUTE() {
        return dummyattribute;
    }

    /**
     * Sets the value of the dummyattribute property.
     * 
     * @param value
     *     allowed object is
     *     {@link TIATABATTRIBUTE }
     *     
     */
    public void setDUMMYATTRIBUTE(TIATABATTRIBUTE value) {
        this.dummyattribute = value;
    }

}
