
package za.co.ominsure.synapse.iwyze.lob.backend.claim.vo;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Additional comments
 * 
 * <p>Java class for Claim complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Claim">
 *   &lt;complexContent>
 *     &lt;extension base="{http://infrastructure.tia.dk/schema/common/v2/}TIAObject">
 *       &lt;sequence>
 *         &lt;element name="claimNo" type="{http://infrastructure.tia.dk/schema/common/v2/}long10" minOccurs="0"/>
 *         &lt;element name="nameIdNo" type="{http://infrastructure.tia.dk/schema/common/v2/}long10" minOccurs="0"/>
 *         &lt;element name="policyLineNo" type="{http://infrastructure.tia.dk/schema/common/v2/}long10" minOccurs="0"/>
 *         &lt;element name="policyLineSeqNo" type="{http://infrastructure.tia.dk/schema/common/v2/}long10" minOccurs="0"/>
 *         &lt;element name="riskNo" type="{http://infrastructure.tia.dk/schema/common/v2/}int3" minOccurs="0"/>
 *         &lt;element name="subriskNo" type="{http://infrastructure.tia.dk/schema/common/v2/}int3" minOccurs="0"/>
 *         &lt;element name="customerStatus" type="{http://infrastructure.tia.dk/schema/common/v2/}int2" minOccurs="0"/>
 *         &lt;element name="informerType" type="{http://infrastructure.tia.dk/schema/common/v2/}string3" minOccurs="0"/>
 *         &lt;element name="informerName" type="{http://infrastructure.tia.dk/schema/common/v2/}string32" minOccurs="0"/>
 *         &lt;element name="informerContact" type="{http://infrastructure.tia.dk/schema/common/v2/}string2000" minOccurs="0"/>
 *         &lt;element name="reinsuranceNo" type="{http://infrastructure.tia.dk/schema/common/v2/}string15" minOccurs="0"/>
 *         &lt;element name="lossOfBonus" type="{http://infrastructure.tia.dk/schema/common/v2/}string3" minOccurs="0"/>
 *         &lt;element name="declineCode" type="{http://infrastructure.tia.dk/schema/common/v2/}string3" minOccurs="0"/>
 *         &lt;element name="declineReason" type="{http://infrastructure.tia.dk/schema/common/v2/}string2000" minOccurs="0"/>
 *         &lt;element name="notificationDate" type="{http://infrastructure.tia.dk/schema/common/v2/}timestamp" minOccurs="0"/>
 *         &lt;element name="discoverDate" type="{http://infrastructure.tia.dk/schema/common/v2/}timestamp" minOccurs="0"/>
 *         &lt;element name="claimType" type="{http://infrastructure.tia.dk/schema/common/v2/}string3" minOccurs="0"/>
 *         &lt;element name="responsibilityPercent" type="{http://infrastructure.tia.dk/schema/common/v2/}string3" minOccurs="0"/>
 *         &lt;element name="closeCode" type="{http://infrastructure.tia.dk/schema/common/v2/}string3" minOccurs="0"/>
 *         &lt;element name="claimNoAlt" type="{http://infrastructure.tia.dk/schema/common/v2/}string30" minOccurs="0"/>
 *         &lt;element name="claimNoSort" type="{http://infrastructure.tia.dk/schema/common/v2/}string30" minOccurs="0"/>
 *         &lt;element name="description" type="{http://infrastructure.tia.dk/schema/common/v2/}string2000" minOccurs="0"/>
 *         &lt;element name="objectNo" type="{http://infrastructure.tia.dk/schema/common/v2/}long10" minOccurs="0"/>
 *         &lt;element name="objectSeqNo" type="{http://infrastructure.tia.dk/schema/common/v2/}long10" minOccurs="0"/>
 *         &lt;element name="objectId" type="{http://infrastructure.tia.dk/schema/common/v2/}string30" minOccurs="0"/>
 *         &lt;element name="statCode1" type="{http://infrastructure.tia.dk/schema/common/v2/}string3" minOccurs="0"/>
 *         &lt;element name="statCode2" type="{http://infrastructure.tia.dk/schema/common/v2/}string3" minOccurs="0"/>
 *         &lt;element name="statCode3" type="{http://infrastructure.tia.dk/schema/common/v2/}string3" minOccurs="0"/>
 *         &lt;element name="status" type="{http://infrastructure.tia.dk/schema/common/v2/}string3" minOccurs="0"/>
 *         &lt;element name="firstOpenDate" type="{http://infrastructure.tia.dk/schema/common/v2/}timestamp" minOccurs="0"/>
 *         &lt;element name="firstCloseDate" type="{http://infrastructure.tia.dk/schema/common/v2/}timestamp" minOccurs="0"/>
 *         &lt;element name="reopenDate" type="{http://infrastructure.tia.dk/schema/common/v2/}timestamp" minOccurs="0"/>
 *         &lt;element name="reopenReason" type="{http://infrastructure.tia.dk/schema/common/v2/}string2000" minOccurs="0"/>
 *         &lt;element name="recloseDate" type="{http://infrastructure.tia.dk/schema/common/v2/}timestamp" minOccurs="0"/>
 *         &lt;element name="hoursSpend" type="{http://infrastructure.tia.dk/schema/common/v2/}decimal38Point2" minOccurs="0"/>
 *         &lt;element name="workgroup" type="{http://infrastructure.tia.dk/schema/common/v2/}string10" minOccurs="0"/>
 *         &lt;element name="handler" type="{http://infrastructure.tia.dk/schema/common/v2/}string8" minOccurs="0"/>
 *         &lt;element name="claimClass" type="{http://infrastructure.tia.dk/schema/common/v2/}string10" minOccurs="0"/>
 *         &lt;element name="questionClass" type="{http://infrastructure.tia.dk/schema/common/v2/}string10" minOccurs="0"/>
 *         &lt;element name="questionStatus" type="{http://infrastructure.tia.dk/schema/common/v2/}string3" minOccurs="0"/>
 *         &lt;element name="tasklistClass" type="{http://infrastructure.tia.dk/schema/common/v2/}string10" minOccurs="0"/>
 *         &lt;element name="itemClass" type="{http://infrastructure.tia.dk/schema/common/v2/}string10" minOccurs="0"/>
 *         &lt;element name="referralCode" type="{http://infrastructure.tia.dk/schema/common/v2/}string3" minOccurs="0"/>
 *         &lt;element name="scorecardScore" type="{http://infrastructure.tia.dk/schema/common/v2/}decimal38Point0" minOccurs="0"/>
 *         &lt;element name="categoryResult" type="{http://infrastructure.tia.dk/schema/common/v2/}string10" minOccurs="0"/>
 *         &lt;element name="assignProfileId" type="{http://infrastructure.tia.dk/schema/common/v2/}string6" minOccurs="0"/>
 *         &lt;element name="answerSetCollection" type="{http://infrastructure.tia.dk/schema/claim/v2/}AnswerSetCollection" minOccurs="0"/>
 *         &lt;element name="claimItemCollection" type="{http://infrastructure.tia.dk/schema/claim/v2/}ClaimItemCollection" minOccurs="0"/>
 *         &lt;element name="claimTaskCollection" type="{http://infrastructure.tia.dk/schema/claim/v2/}ClaimTaskCollection" minOccurs="0"/>
 *         &lt;element name="subclaimCollection" type="{http://infrastructure.tia.dk/schema/claim/v2/}SubclaimCollection" minOccurs="0"/>
 *         &lt;element name="thirdPartyCollection" type="{http://infrastructure.tia.dk/schema/claim/v2/}ThirdPartyCollection" minOccurs="0"/>
 *         &lt;element name="referralCollection" type="{http://infrastructure.tia.dk/schema/claim/v2/}ClaimReferralCollection" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Claim", propOrder = {
    "claimNo",
    "nameIdNo",
    "policyLineNo",
    "policyLineSeqNo",
    "riskNo",
    "subriskNo",
    "customerStatus",
    "informerType",
    "informerName",
    "informerContact",
    "reinsuranceNo",
    "lossOfBonus",
    "declineCode",
    "declineReason",
    "notificationDate",
    "discoverDate",
    "claimType",
    "responsibilityPercent",
    "closeCode",
    "claimNoAlt",
    "claimNoSort",
    "description",
    "objectNo",
    "objectSeqNo",
    "objectId",
    "statCode1",
    "statCode2",
    "statCode3",
    "status",
    "firstOpenDate",
    "firstCloseDate",
    "reopenDate",
    "reopenReason",
    "recloseDate",
    "hoursSpend",
    "workgroup",
    "handler",
    "claimClass",
    "questionClass",
    "questionStatus",
    "tasklistClass",
    "itemClass",
    "referralCode",
    "scorecardScore",
    "categoryResult",
    "assignProfileId",
    "answerSetCollection",
    "claimItemCollection",
    "claimTaskCollection",
    "subclaimCollection",
    "thirdPartyCollection",
    "referralCollection"
})
public class Claim
    extends TIAObject
{

    @XmlElement(nillable = true)
    protected Long claimNo;
    @XmlElement(nillable = true)
    protected Long nameIdNo;
    @XmlElement(nillable = true)
    protected Long policyLineNo;
    @XmlElement(nillable = true)
    protected Long policyLineSeqNo;
    @XmlElement(nillable = true)
    protected Long riskNo;
    @XmlElement(nillable = true)
    protected Long subriskNo;
    @XmlElement(nillable = true)
    protected Long customerStatus;
    @XmlElement(nillable = true)
    protected String informerType;
    @XmlElement(nillable = true)
    protected String informerName;
    @XmlElement(nillable = true)
    protected String informerContact;
    @XmlElement(nillable = true)
    protected String reinsuranceNo;
    @XmlElement(nillable = true)
    protected String lossOfBonus;
    @XmlElement(nillable = true)
    protected String declineCode;
    @XmlElement(nillable = true)
    protected String declineReason;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar notificationDate;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar discoverDate;
    @XmlElement(nillable = true)
    protected String claimType;
    @XmlElement(nillable = true)
    protected String responsibilityPercent;
    @XmlElement(nillable = true)
    protected String closeCode;
    @XmlElement(nillable = true)
    protected String claimNoAlt;
    @XmlElement(nillable = true)
    protected String claimNoSort;
    @XmlElement(nillable = true)
    protected String description;
    @XmlElement(nillable = true)
    protected Long objectNo;
    @XmlElement(nillable = true)
    protected Long objectSeqNo;
    @XmlElement(nillable = true)
    protected String objectId;
    @XmlElement(nillable = true)
    protected String statCode1;
    @XmlElement(nillable = true)
    protected String statCode2;
    @XmlElement(nillable = true)
    protected String statCode3;
    @XmlElement(nillable = true)
    protected String status;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar firstOpenDate;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar firstCloseDate;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar reopenDate;
    @XmlElement(nillable = true)
    protected String reopenReason;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar recloseDate;
    @XmlElement(nillable = true)
    protected BigDecimal hoursSpend;
    @XmlElement(nillable = true)
    protected String workgroup;
    @XmlElement(nillable = true)
    protected String handler;
    @XmlElement(nillable = true)
    protected String claimClass;
    @XmlElement(nillable = true)
    protected String questionClass;
    @XmlElement(nillable = true)
    protected String questionStatus;
    @XmlElement(nillable = true)
    protected String tasklistClass;
    @XmlElement(nillable = true)
    protected String itemClass;
    @XmlElement(nillable = true)
    protected String referralCode;
    @XmlElement(nillable = true)
    protected BigDecimal scorecardScore;
    @XmlElement(nillable = true)
    protected String categoryResult;
    @XmlElement(nillable = true)
    protected String assignProfileId;
    protected AnswerSetCollection answerSetCollection;
    protected ClaimItemCollection claimItemCollection;
    protected ClaimTaskCollection claimTaskCollection;
    protected SubclaimCollection subclaimCollection;
    protected ThirdPartyCollection thirdPartyCollection;
    protected ClaimReferralCollection referralCollection;

    /**
     * Gets the value of the claimNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getClaimNo() {
        return claimNo;
    }

    /**
     * Sets the value of the claimNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setClaimNo(Long value) {
        this.claimNo = value;
    }

    /**
     * Gets the value of the nameIdNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getNameIdNo() {
        return nameIdNo;
    }

    /**
     * Sets the value of the nameIdNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setNameIdNo(Long value) {
        this.nameIdNo = value;
    }

    /**
     * Gets the value of the policyLineNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getPolicyLineNo() {
        return policyLineNo;
    }

    /**
     * Sets the value of the policyLineNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setPolicyLineNo(Long value) {
        this.policyLineNo = value;
    }

    /**
     * Gets the value of the policyLineSeqNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getPolicyLineSeqNo() {
        return policyLineSeqNo;
    }

    /**
     * Sets the value of the policyLineSeqNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setPolicyLineSeqNo(Long value) {
        this.policyLineSeqNo = value;
    }

    /**
     * Gets the value of the riskNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getRiskNo() {
        return riskNo;
    }

    /**
     * Sets the value of the riskNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setRiskNo(Long value) {
        this.riskNo = value;
    }

    /**
     * Gets the value of the subriskNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getSubriskNo() {
        return subriskNo;
    }

    /**
     * Sets the value of the subriskNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setSubriskNo(Long value) {
        this.subriskNo = value;
    }

    /**
     * Gets the value of the customerStatus property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getCustomerStatus() {
        return customerStatus;
    }

    /**
     * Sets the value of the customerStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setCustomerStatus(Long value) {
        this.customerStatus = value;
    }

    /**
     * Gets the value of the informerType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInformerType() {
        return informerType;
    }

    /**
     * Sets the value of the informerType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInformerType(String value) {
        this.informerType = value;
    }

    /**
     * Gets the value of the informerName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInformerName() {
        return informerName;
    }

    /**
     * Sets the value of the informerName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInformerName(String value) {
        this.informerName = value;
    }

    /**
     * Gets the value of the informerContact property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInformerContact() {
        return informerContact;
    }

    /**
     * Sets the value of the informerContact property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInformerContact(String value) {
        this.informerContact = value;
    }

    /**
     * Gets the value of the reinsuranceNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReinsuranceNo() {
        return reinsuranceNo;
    }

    /**
     * Sets the value of the reinsuranceNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReinsuranceNo(String value) {
        this.reinsuranceNo = value;
    }

    /**
     * Gets the value of the lossOfBonus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLossOfBonus() {
        return lossOfBonus;
    }

    /**
     * Sets the value of the lossOfBonus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLossOfBonus(String value) {
        this.lossOfBonus = value;
    }

    /**
     * Gets the value of the declineCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeclineCode() {
        return declineCode;
    }

    /**
     * Sets the value of the declineCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeclineCode(String value) {
        this.declineCode = value;
    }

    /**
     * Gets the value of the declineReason property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeclineReason() {
        return declineReason;
    }

    /**
     * Sets the value of the declineReason property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeclineReason(String value) {
        this.declineReason = value;
    }

    /**
     * Gets the value of the notificationDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getNotificationDate() {
        return notificationDate;
    }

    /**
     * Sets the value of the notificationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setNotificationDate(XMLGregorianCalendar value) {
        this.notificationDate = value;
    }

    /**
     * Gets the value of the discoverDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDiscoverDate() {
        return discoverDate;
    }

    /**
     * Sets the value of the discoverDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDiscoverDate(XMLGregorianCalendar value) {
        this.discoverDate = value;
    }

    /**
     * Gets the value of the claimType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClaimType() {
        return claimType;
    }

    /**
     * Sets the value of the claimType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClaimType(String value) {
        this.claimType = value;
    }

    /**
     * Gets the value of the responsibilityPercent property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResponsibilityPercent() {
        return responsibilityPercent;
    }

    /**
     * Sets the value of the responsibilityPercent property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResponsibilityPercent(String value) {
        this.responsibilityPercent = value;
    }

    /**
     * Gets the value of the closeCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCloseCode() {
        return closeCode;
    }

    /**
     * Sets the value of the closeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCloseCode(String value) {
        this.closeCode = value;
    }

    /**
     * Gets the value of the claimNoAlt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClaimNoAlt() {
        return claimNoAlt;
    }

    /**
     * Sets the value of the claimNoAlt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClaimNoAlt(String value) {
        this.claimNoAlt = value;
    }

    /**
     * Gets the value of the claimNoSort property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClaimNoSort() {
        return claimNoSort;
    }

    /**
     * Sets the value of the claimNoSort property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClaimNoSort(String value) {
        this.claimNoSort = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the objectNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getObjectNo() {
        return objectNo;
    }

    /**
     * Sets the value of the objectNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setObjectNo(Long value) {
        this.objectNo = value;
    }

    /**
     * Gets the value of the objectSeqNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getObjectSeqNo() {
        return objectSeqNo;
    }

    /**
     * Sets the value of the objectSeqNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setObjectSeqNo(Long value) {
        this.objectSeqNo = value;
    }

    /**
     * Gets the value of the objectId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getObjectId() {
        return objectId;
    }

    /**
     * Sets the value of the objectId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setObjectId(String value) {
        this.objectId = value;
    }

    /**
     * Gets the value of the statCode1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatCode1() {
        return statCode1;
    }

    /**
     * Sets the value of the statCode1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatCode1(String value) {
        this.statCode1 = value;
    }

    /**
     * Gets the value of the statCode2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatCode2() {
        return statCode2;
    }

    /**
     * Sets the value of the statCode2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatCode2(String value) {
        this.statCode2 = value;
    }

    /**
     * Gets the value of the statCode3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatCode3() {
        return statCode3;
    }

    /**
     * Sets the value of the statCode3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatCode3(String value) {
        this.statCode3 = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

    /**
     * Gets the value of the firstOpenDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFirstOpenDate() {
        return firstOpenDate;
    }

    /**
     * Sets the value of the firstOpenDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFirstOpenDate(XMLGregorianCalendar value) {
        this.firstOpenDate = value;
    }

    /**
     * Gets the value of the firstCloseDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFirstCloseDate() {
        return firstCloseDate;
    }

    /**
     * Sets the value of the firstCloseDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFirstCloseDate(XMLGregorianCalendar value) {
        this.firstCloseDate = value;
    }

    /**
     * Gets the value of the reopenDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getReopenDate() {
        return reopenDate;
    }

    /**
     * Sets the value of the reopenDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setReopenDate(XMLGregorianCalendar value) {
        this.reopenDate = value;
    }

    /**
     * Gets the value of the reopenReason property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReopenReason() {
        return reopenReason;
    }

    /**
     * Sets the value of the reopenReason property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReopenReason(String value) {
        this.reopenReason = value;
    }

    /**
     * Gets the value of the recloseDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getRecloseDate() {
        return recloseDate;
    }

    /**
     * Sets the value of the recloseDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setRecloseDate(XMLGregorianCalendar value) {
        this.recloseDate = value;
    }

    /**
     * Gets the value of the hoursSpend property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getHoursSpend() {
        return hoursSpend;
    }

    /**
     * Sets the value of the hoursSpend property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setHoursSpend(BigDecimal value) {
        this.hoursSpend = value;
    }

    /**
     * Gets the value of the workgroup property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWorkgroup() {
        return workgroup;
    }

    /**
     * Sets the value of the workgroup property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWorkgroup(String value) {
        this.workgroup = value;
    }

    /**
     * Gets the value of the handler property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHandler() {
        return handler;
    }

    /**
     * Sets the value of the handler property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHandler(String value) {
        this.handler = value;
    }

    /**
     * Gets the value of the claimClass property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClaimClass() {
        return claimClass;
    }

    /**
     * Sets the value of the claimClass property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClaimClass(String value) {
        this.claimClass = value;
    }

    /**
     * Gets the value of the questionClass property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQuestionClass() {
        return questionClass;
    }

    /**
     * Sets the value of the questionClass property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQuestionClass(String value) {
        this.questionClass = value;
    }

    /**
     * Gets the value of the questionStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQuestionStatus() {
        return questionStatus;
    }

    /**
     * Sets the value of the questionStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQuestionStatus(String value) {
        this.questionStatus = value;
    }

    /**
     * Gets the value of the tasklistClass property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTasklistClass() {
        return tasklistClass;
    }

    /**
     * Sets the value of the tasklistClass property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTasklistClass(String value) {
        this.tasklistClass = value;
    }

    /**
     * Gets the value of the itemClass property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItemClass() {
        return itemClass;
    }

    /**
     * Sets the value of the itemClass property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItemClass(String value) {
        this.itemClass = value;
    }

    /**
     * Gets the value of the referralCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReferralCode() {
        return referralCode;
    }

    /**
     * Sets the value of the referralCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReferralCode(String value) {
        this.referralCode = value;
    }

    /**
     * Gets the value of the scorecardScore property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getScorecardScore() {
        return scorecardScore;
    }

    /**
     * Sets the value of the scorecardScore property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setScorecardScore(BigDecimal value) {
        this.scorecardScore = value;
    }

    /**
     * Gets the value of the categoryResult property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCategoryResult() {
        return categoryResult;
    }

    /**
     * Sets the value of the categoryResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCategoryResult(String value) {
        this.categoryResult = value;
    }

    /**
     * Gets the value of the assignProfileId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAssignProfileId() {
        return assignProfileId;
    }

    /**
     * Sets the value of the assignProfileId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAssignProfileId(String value) {
        this.assignProfileId = value;
    }

    /**
     * Gets the value of the answerSetCollection property.
     * 
     * @return
     *     possible object is
     *     {@link AnswerSetCollection }
     *     
     */
    public AnswerSetCollection getAnswerSetCollection() {
        return answerSetCollection;
    }

    /**
     * Sets the value of the answerSetCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link AnswerSetCollection }
     *     
     */
    public void setAnswerSetCollection(AnswerSetCollection value) {
        this.answerSetCollection = value;
    }

    /**
     * Gets the value of the claimItemCollection property.
     * 
     * @return
     *     possible object is
     *     {@link ClaimItemCollection }
     *     
     */
    public ClaimItemCollection getClaimItemCollection() {
        return claimItemCollection;
    }

    /**
     * Sets the value of the claimItemCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link ClaimItemCollection }
     *     
     */
    public void setClaimItemCollection(ClaimItemCollection value) {
        this.claimItemCollection = value;
    }

    /**
     * Gets the value of the claimTaskCollection property.
     * 
     * @return
     *     possible object is
     *     {@link ClaimTaskCollection }
     *     
     */
    public ClaimTaskCollection getClaimTaskCollection() {
        return claimTaskCollection;
    }

    /**
     * Sets the value of the claimTaskCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link ClaimTaskCollection }
     *     
     */
    public void setClaimTaskCollection(ClaimTaskCollection value) {
        this.claimTaskCollection = value;
    }

    /**
     * Gets the value of the subclaimCollection property.
     * 
     * @return
     *     possible object is
     *     {@link SubclaimCollection }
     *     
     */
    public SubclaimCollection getSubclaimCollection() {
        return subclaimCollection;
    }

    /**
     * Sets the value of the subclaimCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link SubclaimCollection }
     *     
     */
    public void setSubclaimCollection(SubclaimCollection value) {
        this.subclaimCollection = value;
    }

    /**
     * Gets the value of the thirdPartyCollection property.
     * 
     * @return
     *     possible object is
     *     {@link ThirdPartyCollection }
     *     
     */
    public ThirdPartyCollection getThirdPartyCollection() {
        return thirdPartyCollection;
    }

    /**
     * Sets the value of the thirdPartyCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link ThirdPartyCollection }
     *     
     */
    public void setThirdPartyCollection(ThirdPartyCollection value) {
        this.thirdPartyCollection = value;
    }

    /**
     * Gets the value of the referralCollection property.
     * 
     * @return
     *     possible object is
     *     {@link ClaimReferralCollection }
     *     
     */
    public ClaimReferralCollection getReferralCollection() {
        return referralCollection;
    }

    /**
     * Sets the value of the referralCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link ClaimReferralCollection }
     *     
     */
    public void setReferralCollection(ClaimReferralCollection value) {
        this.referralCollection = value;
    }

}
