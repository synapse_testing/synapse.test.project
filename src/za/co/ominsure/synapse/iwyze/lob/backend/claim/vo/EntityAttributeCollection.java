
package za.co.ominsure.synapse.iwyze.lob.backend.claim.vo;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for EntityAttributeCollection complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="EntityAttributeCollection">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="varcharAttribute" type="{http://infrastructure.tia.dk/schema/common/v2/}VarcharAttribute" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="numberAttribute" type="{http://infrastructure.tia.dk/schema/common/v2/}NumberAttribute" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="dateAttribute" type="{http://infrastructure.tia.dk/schema/common/v2/}DateAttribute" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EntityAttributeCollection", namespace = "http://infrastructure.tia.dk/schema/common/v2/", propOrder = {
    "varcharAttributes",
    "numberAttributes",
    "dateAttributes"
})
public class EntityAttributeCollection {

    @XmlElement(name = "varcharAttribute")
    protected List<VarcharAttribute> varcharAttributes;
    @XmlElement(name = "numberAttribute")
    protected List<NumberAttribute> numberAttributes;
    @XmlElement(name = "dateAttribute")
    protected List<DateAttribute> dateAttributes;

    /**
     * Gets the value of the varcharAttributes property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the varcharAttributes property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getVarcharAttributes().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link VarcharAttribute }
     * 
     * 
     */
    public List<VarcharAttribute> getVarcharAttributes() {
        if (varcharAttributes == null) {
            varcharAttributes = new ArrayList<VarcharAttribute>();
        }
        return this.varcharAttributes;
    }

    /**
     * Gets the value of the numberAttributes property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the numberAttributes property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getNumberAttributes().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link NumberAttribute }
     * 
     * 
     */
    public List<NumberAttribute> getNumberAttributes() {
        if (numberAttributes == null) {
            numberAttributes = new ArrayList<NumberAttribute>();
        }
        return this.numberAttributes;
    }

    /**
     * Gets the value of the dateAttributes property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the dateAttributes property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDateAttributes().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DateAttribute }
     * 
     * 
     */
    public List<DateAttribute> getDateAttributes() {
        if (dateAttributes == null) {
            dateAttributes = new ArrayList<DateAttribute>();
        }
        return this.dateAttributes;
    }

}
