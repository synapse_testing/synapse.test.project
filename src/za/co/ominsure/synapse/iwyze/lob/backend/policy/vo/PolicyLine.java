
package za.co.ominsure.synapse.iwyze.lob.backend.policy.vo;

import java.math.BigDecimal;
import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Is mapped into TIA package P1100".
 * 
 * <p>Java class for PolicyLine complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PolicyLine">
 *   &lt;complexContent>
 *     &lt;extension base="{http://infrastructure.tia.dk/schema/common/v2/}TIAObject">
 *       &lt;sequence>
 *         &lt;element name="policyLineNo" type="{http://infrastructure.tia.dk/schema/common/v2/}policyLineNo" minOccurs="0"/>
 *         &lt;element name="policyLineSeqNo" type="{http://infrastructure.tia.dk/schema/common/v2/}policyLineSeqNo" minOccurs="0"/>
 *         &lt;element name="coHolderCollection" type="{http://infrastructure.tia.dk/schema/policy/v2/}PolicyCoHolderCollection" minOccurs="0"/>
 *         &lt;element name="productLineId" type="{http://infrastructure.tia.dk/schema/policy/v2/}productLineId" minOccurs="0"/>
 *         &lt;element name="productLineVerNo" type="{http://infrastructure.tia.dk/schema/common/v2/}decimal9Point3" minOccurs="0"/>
 *         &lt;element name="sectionNo" type="{http://infrastructure.tia.dk/schema/policy/v2/}sectionNo" minOccurs="0"/>
 *         &lt;element name="referToUnderwriter" type="{http://infrastructure.tia.dk/schema/policy/v2/}referToUnderwriter" minOccurs="0"/>
 *         &lt;element name="cancelCode" type="{http://infrastructure.tia.dk/schema/policy/v2/}cancelCode" minOccurs="0"/>
 *         &lt;element name="status" type="{http://infrastructure.tia.dk/schema/policy/v2/}status" minOccurs="0"/>
 *         &lt;element name="newest" type="{http://infrastructure.tia.dk/schema/policy/v2/}newest" minOccurs="0"/>
 *         &lt;element name="coverStartDate" type="{http://infrastructure.tia.dk/schema/common/v2/}date" minOccurs="0"/>
 *         &lt;element name="coverEndDate" type="{http://infrastructure.tia.dk/schema/common/v2/}date" minOccurs="0"/>
 *         &lt;element name="renewalDate" type="{http://infrastructure.tia.dk/schema/common/v2/}date" minOccurs="0"/>
 *         &lt;element name="policyYear" type="{http://infrastructure.tia.dk/schema/policy/v2/}policyYear" minOccurs="0"/>
 *         &lt;element name="firstStartDate" type="{http://infrastructure.tia.dk/schema/common/v2/}date" minOccurs="0"/>
 *         &lt;element name="yearStartDate" type="{http://infrastructure.tia.dk/schema/common/v2/}date" minOccurs="0"/>
 *         &lt;element name="expiryDate" type="{http://infrastructure.tia.dk/schema/common/v2/}date" minOccurs="0"/>
 *         &lt;element name="debitEndDate" type="{http://infrastructure.tia.dk/schema/common/v2/}date" minOccurs="0"/>
 *         &lt;element name="preSeqNo" type="{http://infrastructure.tia.dk/schema/policy/v2/}seqNo" minOccurs="0"/>
 *         &lt;element name="sucSeqNo" type="{http://infrastructure.tia.dk/schema/policy/v2/}seqNo" minOccurs="0"/>
 *         &lt;element name="transId" type="{http://infrastructure.tia.dk/schema/policy/v2/}seqNo" minOccurs="0"/>
 *         &lt;element name="mtaStartDate" type="{http://infrastructure.tia.dk/schema/common/v2/}date" minOccurs="0"/>
 *         &lt;element name="mtaEndDate" type="{http://infrastructure.tia.dk/schema/common/v2/}date" minOccurs="0"/>
 *         &lt;element name="discountType" type="{http://infrastructure.tia.dk/schema/common/v2/}string1" minOccurs="0"/>
 *         &lt;element name="discountPct" type="{http://infrastructure.tia.dk/schema/common/v2/}amountwithoutrestriction" minOccurs="0"/>
 *         &lt;element name="discountAmt" type="{http://infrastructure.tia.dk/schema/common/v2/}amountwithoutrestriction" minOccurs="0"/>
 *         &lt;element name="priceAgrDisc" type="{http://infrastructure.tia.dk/schema/policy/v2/}policyAmount" minOccurs="0"/>
 *         &lt;element name="priceIndivDisc" type="{http://infrastructure.tia.dk/schema/policy/v2/}policyAmount" minOccurs="0"/>
 *         &lt;element name="pricePaid" type="{http://infrastructure.tia.dk/schema/policy/v2/}policyAmount" minOccurs="0"/>
 *         &lt;element name="tariffPrice" type="{http://infrastructure.tia.dk/schema/policy/v2/}policyAmount" minOccurs="0"/>
 *         &lt;element name="priceFlatPaid" type="{http://infrastructure.tia.dk/schema/policy/v2/}policyAmount" minOccurs="0"/>
 *         &lt;element name="tariffFlat" type="{http://infrastructure.tia.dk/schema/policy/v2/}policyAmount" minOccurs="0"/>
 *         &lt;element name="transactionCost" type="{http://infrastructure.tia.dk/schema/policy/v2/}policyAmount" minOccurs="0"/>
 *         &lt;element name="stampDuty" type="{http://infrastructure.tia.dk/schema/policy/v2/}policyAmount" minOccurs="0"/>
 *         &lt;element name="stampDutyTransferred" type="{http://infrastructure.tia.dk/schema/policy/v2/}policyAmount" minOccurs="0"/>
 *         &lt;element name="transCode" type="{http://infrastructure.tia.dk/schema/policy/v2/}transCode" minOccurs="0"/>
 *         &lt;element name="proRataCalendar" type="{http://infrastructure.tia.dk/schema/policy/v2/}proRataCalendar" minOccurs="0"/>
 *         &lt;element name="currencyCode" type="{http://infrastructure.tia.dk/schema/policy/v2/}currencyCode" minOccurs="0"/>
 *         &lt;element name="temporaryInsurance" type="{http://infrastructure.tia.dk/schema/common/v2/}string1" minOccurs="0"/>
 *         &lt;element name="notes" type="{http://infrastructure.tia.dk/schema/policy/v2/}notes" minOccurs="0"/>
 *         &lt;element name="reiProgramNo" type="{http://infrastructure.tia.dk/schema/policy/v2/}reiProgramNo" minOccurs="0"/>
 *         &lt;element name="emlPct" type="{http://infrastructure.tia.dk/schema/policy/v2/}policyPct" minOccurs="0"/>
 *         &lt;element name="shortDesc" type="{http://infrastructure.tia.dk/schema/policy/v2/}shortDesc" minOccurs="0"/>
 *         &lt;element name="commissionPct" type="{http://infrastructure.tia.dk/schema/policy/v2/}policyPct" minOccurs="0"/>
 *         &lt;element name="transactionCause" type="{http://infrastructure.tia.dk/schema/policy/v2/}transactionCause" minOccurs="0"/>
 *         &lt;element name="transactionCauseNote" type="{http://infrastructure.tia.dk/schema/policy/v2/}transactionCauseNote" minOccurs="0"/>
 *         &lt;element name="affinityNo" type="{http://infrastructure.tia.dk/schema/policy/v2/}seqNo" minOccurs="0"/>
 *         &lt;element name="expiryCause" type="{http://infrastructure.tia.dk/schema/policy/v2/}expiryCause" minOccurs="0"/>
 *         &lt;element name="nextInsuranceCompany" type="{http://infrastructure.tia.dk/schema/policy/v2/}nextInsuranceCompany" minOccurs="0"/>
 *         &lt;element name="nextPolicyNo" type="{http://infrastructure.tia.dk/schema/policy/v2/}nextPolicyNo" minOccurs="0"/>
 *         &lt;element name="currentIndexDate" type="{http://infrastructure.tia.dk/schema/common/v2/}date" minOccurs="0"/>
 *         &lt;element name="nextIndexDate" type="{http://infrastructure.tia.dk/schema/common/v2/}date" minOccurs="0"/>
 *         &lt;element name="nextBonusDate" type="{http://infrastructure.tia.dk/schema/common/v2/}date" minOccurs="0"/>
 *         &lt;element name="prevInsuranceCompany" type="{http://infrastructure.tia.dk/schema/policy/v2/}prevInsuranceCompany" minOccurs="0"/>
 *         &lt;element name="prevPolicyNo" type="{http://infrastructure.tia.dk/schema/policy/v2/}prevPolicyNo" minOccurs="0"/>
 *         &lt;element name="createCause" type="{http://infrastructure.tia.dk/schema/policy/v2/}createCause" minOccurs="0"/>
 *         &lt;element name="referredAmount" type="{http://infrastructure.tia.dk/schema/policy/v2/}policyAmount" minOccurs="0"/>
 *         &lt;element name="bonusMalus" type="{http://infrastructure.tia.dk/schema/policy/v2/}bonusMalus" minOccurs="0"/>
 *         &lt;element name="bonusMalus2" type="{http://infrastructure.tia.dk/schema/policy/v2/}bonusMalus" minOccurs="0"/>
 *         &lt;element name="forceGlobalMta" type="{http://infrastructure.tia.dk/schema/common/v2/}string1" minOccurs="0"/>
 *         &lt;element name="peReserveTableType" type="{http://infrastructure.tia.dk/schema/policy/v2/}peReserveTableType" minOccurs="0"/>
 *         &lt;element name="ignoreBackdatedVersions" type="{http://infrastructure.tia.dk/schema/common/v2/}string1" minOccurs="0"/>
 *         &lt;element name="paymentFrequency" type="{http://infrastructure.tia.dk/schema/policy/v2/}paymentFrequency" minOccurs="0"/>
 *         &lt;element name="paymentMethod" type="{http://infrastructure.tia.dk/schema/common/v2/}paymentMethod" minOccurs="0"/>
 *         &lt;element name="paymentDetailsId" type="{http://infrastructure.tia.dk/schema/policy/v2/}seqNo" minOccurs="0"/>
 *         &lt;element name="instlPlanType" type="{http://infrastructure.tia.dk/schema/common/v2/}instlPlanType" minOccurs="0"/>
 *         &lt;element name="premiumRecovery" type="{http://infrastructure.tia.dk/schema/common/v2/}string1" minOccurs="0"/>
 *         &lt;element name="accountNo" type="{http://infrastructure.tia.dk/schema/common/v2/}seqNo" minOccurs="0"/>
 *         &lt;element name="centerCode" type="{http://infrastructure.tia.dk/schema/common/v2/}centerCode" minOccurs="0"/>
 *         &lt;element name="productTax1" type="{http://infrastructure.tia.dk/schema/policy/v2/}policyAmount" minOccurs="0"/>
 *         &lt;element name="productTax2" type="{http://infrastructure.tia.dk/schema/policy/v2/}policyAmount" minOccurs="0"/>
 *         &lt;element name="productTax3" type="{http://infrastructure.tia.dk/schema/policy/v2/}policyAmount" minOccurs="0"/>
 *         &lt;element name="productTax4" type="{http://infrastructure.tia.dk/schema/policy/v2/}policyAmount" minOccurs="0"/>
 *         &lt;element name="productTax5" type="{http://infrastructure.tia.dk/schema/policy/v2/}policyAmount" minOccurs="0"/>
 *         &lt;element name="productTax6" type="{http://infrastructure.tia.dk/schema/policy/v2/}policyAmount" minOccurs="0"/>
 *         &lt;element name="productTax7" type="{http://infrastructure.tia.dk/schema/policy/v2/}policyAmount" minOccurs="0"/>
 *         &lt;element name="seqNoQop" type="{http://infrastructure.tia.dk/schema/common/v2/}long10" minOccurs="0"/>
 *         &lt;element name="policyLineObjectCollection" type="{http://infrastructure.tia.dk/schema/policy/v4/}PolicyLineObjectCollection" minOccurs="0"/>
 *         &lt;element name="clauseCollection" type="{http://infrastructure.tia.dk/schema/policy/v4/}ClauseCollection" minOccurs="0"/>
 *         &lt;element name="premiumRiskSplitCollection" type="{http://infrastructure.tia.dk/schema/policy/v3/}PremiumRiskSplitCollection" minOccurs="0"/>
 *         &lt;element name="debitAmount" type="{http://infrastructure.tia.dk/schema/policy/v2/}policyAmount" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PolicyLine", namespace = "http://infrastructure.tia.dk/schema/policy/v4/", propOrder = {
    "policyLineNo",
    "policyLineSeqNo",
    "coHolderCollection",
    "productLineId",
    "productLineVerNo",
    "sectionNo",
    "referToUnderwriter",
    "cancelCode",
    "status",
    "newest",
    "coverStartDate",
    "coverEndDate",
    "renewalDate",
    "policyYear",
    "firstStartDate",
    "yearStartDate",
    "expiryDate",
    "debitEndDate",
    "preSeqNo",
    "sucSeqNo",
    "transId",
    "mtaStartDate",
    "mtaEndDate",
    "discountType",
    "discountPct",
    "discountAmt",
    "priceAgrDisc",
    "priceIndivDisc",
    "pricePaid",
    "tariffPrice",
    "priceFlatPaid",
    "tariffFlat",
    "transactionCost",
    "stampDuty",
    "stampDutyTransferred",
    "transCode",
    "proRataCalendar",
    "currencyCode",
    "temporaryInsurance",
    "notes",
    "reiProgramNo",
    "emlPct",
    "shortDesc",
    "commissionPct",
    "transactionCause",
    "transactionCauseNote",
    "affinityNo",
    "expiryCause",
    "nextInsuranceCompany",
    "nextPolicyNo",
    "currentIndexDate",
    "nextIndexDate",
    "nextBonusDate",
    "prevInsuranceCompany",
    "prevPolicyNo",
    "createCause",
    "referredAmount",
    "bonusMalus",
    "bonusMalus2",
    "forceGlobalMta",
    "peReserveTableType",
    "ignoreBackdatedVersions",
    "paymentFrequency",
    "paymentMethod",
    "paymentDetailsId",
    "instlPlanType",
    "premiumRecovery",
    "accountNo",
    "centerCode",
    "productTax1",
    "productTax2",
    "productTax3",
    "productTax4",
    "productTax5",
    "productTax6",
    "productTax7",
    "seqNoQop",
    "policyLineObjectCollection",
    "clauseCollection",
    "premiumRiskSplitCollection",
    "debitAmount"
})
public class PolicyLine
    extends TIAObject
{

    @XmlSchemaType(name = "unsignedLong")
    protected BigInteger policyLineNo;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "unsignedLong")
    protected BigInteger policyLineSeqNo;
    protected PolicyCoHolderCollection coHolderCollection;
    protected String productLineId;
    @XmlElement(nillable = true)
    protected BigDecimal productLineVerNo;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "unsignedInt")
    protected Long sectionNo;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "unsignedInt")
    protected Long referToUnderwriter;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "unsignedInt")
    protected Long cancelCode;
    @XmlElement(nillable = true)
    protected String status;
    @XmlElement(nillable = true)
    protected String newest;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar coverStartDate;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar coverEndDate;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar renewalDate;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "unsignedInt")
    protected Long policyYear;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar firstStartDate;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar yearStartDate;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar expiryDate;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar debitEndDate;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "unsignedLong")
    protected BigInteger preSeqNo;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "unsignedLong")
    protected BigInteger sucSeqNo;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "unsignedLong")
    protected BigInteger transId;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar mtaStartDate;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar mtaEndDate;
    protected String discountType;
    @XmlElement(nillable = true)
    protected BigDecimal discountPct;
    @XmlElement(nillable = true)
    protected BigDecimal discountAmt;
    @XmlElement(nillable = true)
    protected BigDecimal priceAgrDisc;
    @XmlElement(nillable = true)
    protected BigDecimal priceIndivDisc;
    @XmlElement(nillable = true)
    protected BigDecimal pricePaid;
    @XmlElement(nillable = true)
    protected BigDecimal tariffPrice;
    @XmlElement(nillable = true)
    protected BigDecimal priceFlatPaid;
    @XmlElement(nillable = true)
    protected BigDecimal tariffFlat;
    @XmlElement(nillable = true)
    protected BigDecimal transactionCost;
    @XmlElement(nillable = true)
    protected BigDecimal stampDuty;
    @XmlElement(nillable = true)
    protected BigDecimal stampDutyTransferred;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "unsignedInt")
    protected Long transCode;
    @XmlElement(nillable = true)
    protected String proRataCalendar;
    @XmlElement(nillable = true)
    protected String currencyCode;
    protected String temporaryInsurance;
    @XmlElement(nillable = true)
    protected String notes;
    @XmlElement(nillable = true)
    protected String reiProgramNo;
    @XmlElement(nillable = true)
    protected BigDecimal emlPct;
    @XmlElement(nillable = true)
    protected String shortDesc;
    @XmlElement(nillable = true)
    protected BigDecimal commissionPct;
    @XmlElement(nillable = true)
    protected String transactionCause;
    @XmlElement(nillable = true)
    protected String transactionCauseNote;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "unsignedLong")
    protected BigInteger affinityNo;
    @XmlElement(nillable = true)
    protected String expiryCause;
    @XmlElement(nillable = true)
    protected String nextInsuranceCompany;
    @XmlElement(nillable = true)
    protected String nextPolicyNo;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar currentIndexDate;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar nextIndexDate;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar nextBonusDate;
    @XmlElement(nillable = true)
    protected String prevInsuranceCompany;
    @XmlElement(nillable = true)
    protected String prevPolicyNo;
    @XmlElement(nillable = true)
    protected String createCause;
    @XmlElement(nillable = true)
    protected BigDecimal referredAmount;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "unsignedInt")
    protected Long bonusMalus;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "unsignedInt")
    protected Long bonusMalus2;
    @XmlElement(nillable = true)
    protected String forceGlobalMta;
    @XmlElement(nillable = true)
    protected String peReserveTableType;
    protected String ignoreBackdatedVersions;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "unsignedInt")
    protected Long paymentFrequency;
    @XmlElement(nillable = true)
    protected String paymentMethod;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "unsignedLong")
    protected BigInteger paymentDetailsId;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "unsignedInt")
    protected Long instlPlanType;
    @XmlElement(nillable = true)
    protected String premiumRecovery;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "unsignedLong")
    protected Long accountNo;
    @XmlElement(nillable = true)
    protected String centerCode;
    @XmlElement(nillable = true)
    protected BigDecimal productTax1;
    @XmlElement(nillable = true)
    protected BigDecimal productTax2;
    @XmlElement(nillable = true)
    protected BigDecimal productTax3;
    @XmlElement(nillable = true)
    protected BigDecimal productTax4;
    @XmlElement(nillable = true)
    protected BigDecimal productTax5;
    @XmlElement(nillable = true)
    protected BigDecimal productTax6;
    @XmlElement(nillable = true)
    protected BigDecimal productTax7;
    @XmlElement(nillable = true)
    protected Long seqNoQop;
    protected PolicyLineObjectCollection policyLineObjectCollection;
    protected ClauseCollection clauseCollection;
    protected PremiumRiskSplitCollection premiumRiskSplitCollection;
    @XmlElement(nillable = true)
    protected BigDecimal debitAmount;

    /**
     * Gets the value of the policyLineNo property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getPolicyLineNo() {
        return policyLineNo;
    }

    /**
     * Sets the value of the policyLineNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setPolicyLineNo(BigInteger value) {
        this.policyLineNo = value;
    }

    /**
     * Gets the value of the policyLineSeqNo property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getPolicyLineSeqNo() {
        return policyLineSeqNo;
    }

    /**
     * Sets the value of the policyLineSeqNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setPolicyLineSeqNo(BigInteger value) {
        this.policyLineSeqNo = value;
    }

    /**
     * Gets the value of the coHolderCollection property.
     * 
     * @return
     *     possible object is
     *     {@link PolicyCoHolderCollection }
     *     
     */
    public PolicyCoHolderCollection getCoHolderCollection() {
        return coHolderCollection;
    }

    /**
     * Sets the value of the coHolderCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link PolicyCoHolderCollection }
     *     
     */
    public void setCoHolderCollection(PolicyCoHolderCollection value) {
        this.coHolderCollection = value;
    }

    /**
     * Gets the value of the productLineId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductLineId() {
        return productLineId;
    }

    /**
     * Sets the value of the productLineId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductLineId(String value) {
        this.productLineId = value;
    }

    /**
     * Gets the value of the productLineVerNo property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getProductLineVerNo() {
        return productLineVerNo;
    }

    /**
     * Sets the value of the productLineVerNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setProductLineVerNo(BigDecimal value) {
        this.productLineVerNo = value;
    }

    /**
     * Gets the value of the sectionNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getSectionNo() {
        return sectionNo;
    }

    /**
     * Sets the value of the sectionNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setSectionNo(Long value) {
        this.sectionNo = value;
    }

    /**
     * Gets the value of the referToUnderwriter property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getReferToUnderwriter() {
        return referToUnderwriter;
    }

    /**
     * Sets the value of the referToUnderwriter property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setReferToUnderwriter(Long value) {
        this.referToUnderwriter = value;
    }

    /**
     * Gets the value of the cancelCode property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getCancelCode() {
        return cancelCode;
    }

    /**
     * Sets the value of the cancelCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setCancelCode(Long value) {
        this.cancelCode = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

    /**
     * Gets the value of the newest property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNewest() {
        return newest;
    }

    /**
     * Sets the value of the newest property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNewest(String value) {
        this.newest = value;
    }

    /**
     * Gets the value of the coverStartDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCoverStartDate() {
        return coverStartDate;
    }

    /**
     * Sets the value of the coverStartDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCoverStartDate(XMLGregorianCalendar value) {
        this.coverStartDate = value;
    }

    /**
     * Gets the value of the coverEndDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCoverEndDate() {
        return coverEndDate;
    }

    /**
     * Sets the value of the coverEndDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCoverEndDate(XMLGregorianCalendar value) {
        this.coverEndDate = value;
    }

    /**
     * Gets the value of the renewalDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getRenewalDate() {
        return renewalDate;
    }

    /**
     * Sets the value of the renewalDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setRenewalDate(XMLGregorianCalendar value) {
        this.renewalDate = value;
    }

    /**
     * Gets the value of the policyYear property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getPolicyYear() {
        return policyYear;
    }

    /**
     * Sets the value of the policyYear property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setPolicyYear(Long value) {
        this.policyYear = value;
    }

    /**
     * Gets the value of the firstStartDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFirstStartDate() {
        return firstStartDate;
    }

    /**
     * Sets the value of the firstStartDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFirstStartDate(XMLGregorianCalendar value) {
        this.firstStartDate = value;
    }

    /**
     * Gets the value of the yearStartDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getYearStartDate() {
        return yearStartDate;
    }

    /**
     * Sets the value of the yearStartDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setYearStartDate(XMLGregorianCalendar value) {
        this.yearStartDate = value;
    }

    /**
     * Gets the value of the expiryDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getExpiryDate() {
        return expiryDate;
    }

    /**
     * Sets the value of the expiryDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setExpiryDate(XMLGregorianCalendar value) {
        this.expiryDate = value;
    }

    /**
     * Gets the value of the debitEndDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDebitEndDate() {
        return debitEndDate;
    }

    /**
     * Sets the value of the debitEndDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDebitEndDate(XMLGregorianCalendar value) {
        this.debitEndDate = value;
    }

    /**
     * Gets the value of the preSeqNo property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getPreSeqNo() {
        return preSeqNo;
    }

    /**
     * Sets the value of the preSeqNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setPreSeqNo(BigInteger value) {
        this.preSeqNo = value;
    }

    /**
     * Gets the value of the sucSeqNo property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getSucSeqNo() {
        return sucSeqNo;
    }

    /**
     * Sets the value of the sucSeqNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setSucSeqNo(BigInteger value) {
        this.sucSeqNo = value;
    }

    /**
     * Gets the value of the transId property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getTransId() {
        return transId;
    }

    /**
     * Sets the value of the transId property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setTransId(BigInteger value) {
        this.transId = value;
    }

    /**
     * Gets the value of the mtaStartDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getMtaStartDate() {
        return mtaStartDate;
    }

    /**
     * Sets the value of the mtaStartDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setMtaStartDate(XMLGregorianCalendar value) {
        this.mtaStartDate = value;
    }

    /**
     * Gets the value of the mtaEndDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getMtaEndDate() {
        return mtaEndDate;
    }

    /**
     * Sets the value of the mtaEndDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setMtaEndDate(XMLGregorianCalendar value) {
        this.mtaEndDate = value;
    }

    /**
     * Gets the value of the discountType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDiscountType() {
        return discountType;
    }

    /**
     * Sets the value of the discountType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDiscountType(String value) {
        this.discountType = value;
    }

    /**
     * Gets the value of the discountPct property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDiscountPct() {
        return discountPct;
    }

    /**
     * Sets the value of the discountPct property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDiscountPct(BigDecimal value) {
        this.discountPct = value;
    }

    /**
     * Gets the value of the discountAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDiscountAmt() {
        return discountAmt;
    }

    /**
     * Sets the value of the discountAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDiscountAmt(BigDecimal value) {
        this.discountAmt = value;
    }

    /**
     * Gets the value of the priceAgrDisc property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPriceAgrDisc() {
        return priceAgrDisc;
    }

    /**
     * Sets the value of the priceAgrDisc property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPriceAgrDisc(BigDecimal value) {
        this.priceAgrDisc = value;
    }

    /**
     * Gets the value of the priceIndivDisc property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPriceIndivDisc() {
        return priceIndivDisc;
    }

    /**
     * Sets the value of the priceIndivDisc property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPriceIndivDisc(BigDecimal value) {
        this.priceIndivDisc = value;
    }

    /**
     * Gets the value of the pricePaid property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPricePaid() {
        return pricePaid;
    }

    /**
     * Sets the value of the pricePaid property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPricePaid(BigDecimal value) {
        this.pricePaid = value;
    }

    /**
     * Gets the value of the tariffPrice property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTariffPrice() {
        return tariffPrice;
    }

    /**
     * Sets the value of the tariffPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTariffPrice(BigDecimal value) {
        this.tariffPrice = value;
    }

    /**
     * Gets the value of the priceFlatPaid property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPriceFlatPaid() {
        return priceFlatPaid;
    }

    /**
     * Sets the value of the priceFlatPaid property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPriceFlatPaid(BigDecimal value) {
        this.priceFlatPaid = value;
    }

    /**
     * Gets the value of the tariffFlat property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTariffFlat() {
        return tariffFlat;
    }

    /**
     * Sets the value of the tariffFlat property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTariffFlat(BigDecimal value) {
        this.tariffFlat = value;
    }

    /**
     * Gets the value of the transactionCost property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTransactionCost() {
        return transactionCost;
    }

    /**
     * Sets the value of the transactionCost property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTransactionCost(BigDecimal value) {
        this.transactionCost = value;
    }

    /**
     * Gets the value of the stampDuty property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getStampDuty() {
        return stampDuty;
    }

    /**
     * Sets the value of the stampDuty property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setStampDuty(BigDecimal value) {
        this.stampDuty = value;
    }

    /**
     * Gets the value of the stampDutyTransferred property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getStampDutyTransferred() {
        return stampDutyTransferred;
    }

    /**
     * Sets the value of the stampDutyTransferred property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setStampDutyTransferred(BigDecimal value) {
        this.stampDutyTransferred = value;
    }

    /**
     * Gets the value of the transCode property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getTransCode() {
        return transCode;
    }

    /**
     * Sets the value of the transCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setTransCode(Long value) {
        this.transCode = value;
    }

    /**
     * Gets the value of the proRataCalendar property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProRataCalendar() {
        return proRataCalendar;
    }

    /**
     * Sets the value of the proRataCalendar property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProRataCalendar(String value) {
        this.proRataCalendar = value;
    }

    /**
     * Gets the value of the currencyCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrencyCode() {
        return currencyCode;
    }

    /**
     * Sets the value of the currencyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrencyCode(String value) {
        this.currencyCode = value;
    }

    /**
     * Gets the value of the temporaryInsurance property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTemporaryInsurance() {
        return temporaryInsurance;
    }

    /**
     * Sets the value of the temporaryInsurance property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTemporaryInsurance(String value) {
        this.temporaryInsurance = value;
    }

    /**
     * Gets the value of the notes property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNotes() {
        return notes;
    }

    /**
     * Sets the value of the notes property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNotes(String value) {
        this.notes = value;
    }

    /**
     * Gets the value of the reiProgramNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReiProgramNo() {
        return reiProgramNo;
    }

    /**
     * Sets the value of the reiProgramNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReiProgramNo(String value) {
        this.reiProgramNo = value;
    }

    /**
     * Gets the value of the emlPct property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getEmlPct() {
        return emlPct;
    }

    /**
     * Sets the value of the emlPct property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setEmlPct(BigDecimal value) {
        this.emlPct = value;
    }

    /**
     * Gets the value of the shortDesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShortDesc() {
        return shortDesc;
    }

    /**
     * Sets the value of the shortDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShortDesc(String value) {
        this.shortDesc = value;
    }

    /**
     * Gets the value of the commissionPct property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCommissionPct() {
        return commissionPct;
    }

    /**
     * Sets the value of the commissionPct property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCommissionPct(BigDecimal value) {
        this.commissionPct = value;
    }

    /**
     * Gets the value of the transactionCause property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionCause() {
        return transactionCause;
    }

    /**
     * Sets the value of the transactionCause property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionCause(String value) {
        this.transactionCause = value;
    }

    /**
     * Gets the value of the transactionCauseNote property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionCauseNote() {
        return transactionCauseNote;
    }

    /**
     * Sets the value of the transactionCauseNote property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionCauseNote(String value) {
        this.transactionCauseNote = value;
    }

    /**
     * Gets the value of the affinityNo property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getAffinityNo() {
        return affinityNo;
    }

    /**
     * Sets the value of the affinityNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setAffinityNo(BigInteger value) {
        this.affinityNo = value;
    }

    /**
     * Gets the value of the expiryCause property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExpiryCause() {
        return expiryCause;
    }

    /**
     * Sets the value of the expiryCause property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExpiryCause(String value) {
        this.expiryCause = value;
    }

    /**
     * Gets the value of the nextInsuranceCompany property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNextInsuranceCompany() {
        return nextInsuranceCompany;
    }

    /**
     * Sets the value of the nextInsuranceCompany property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNextInsuranceCompany(String value) {
        this.nextInsuranceCompany = value;
    }

    /**
     * Gets the value of the nextPolicyNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNextPolicyNo() {
        return nextPolicyNo;
    }

    /**
     * Sets the value of the nextPolicyNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNextPolicyNo(String value) {
        this.nextPolicyNo = value;
    }

    /**
     * Gets the value of the currentIndexDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCurrentIndexDate() {
        return currentIndexDate;
    }

    /**
     * Sets the value of the currentIndexDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCurrentIndexDate(XMLGregorianCalendar value) {
        this.currentIndexDate = value;
    }

    /**
     * Gets the value of the nextIndexDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getNextIndexDate() {
        return nextIndexDate;
    }

    /**
     * Sets the value of the nextIndexDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setNextIndexDate(XMLGregorianCalendar value) {
        this.nextIndexDate = value;
    }

    /**
     * Gets the value of the nextBonusDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getNextBonusDate() {
        return nextBonusDate;
    }

    /**
     * Sets the value of the nextBonusDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setNextBonusDate(XMLGregorianCalendar value) {
        this.nextBonusDate = value;
    }

    /**
     * Gets the value of the prevInsuranceCompany property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrevInsuranceCompany() {
        return prevInsuranceCompany;
    }

    /**
     * Sets the value of the prevInsuranceCompany property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrevInsuranceCompany(String value) {
        this.prevInsuranceCompany = value;
    }

    /**
     * Gets the value of the prevPolicyNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrevPolicyNo() {
        return prevPolicyNo;
    }

    /**
     * Sets the value of the prevPolicyNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrevPolicyNo(String value) {
        this.prevPolicyNo = value;
    }

    /**
     * Gets the value of the createCause property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCreateCause() {
        return createCause;
    }

    /**
     * Sets the value of the createCause property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCreateCause(String value) {
        this.createCause = value;
    }

    /**
     * Gets the value of the referredAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getReferredAmount() {
        return referredAmount;
    }

    /**
     * Sets the value of the referredAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setReferredAmount(BigDecimal value) {
        this.referredAmount = value;
    }

    /**
     * Gets the value of the bonusMalus property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getBonusMalus() {
        return bonusMalus;
    }

    /**
     * Sets the value of the bonusMalus property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setBonusMalus(Long value) {
        this.bonusMalus = value;
    }

    /**
     * Gets the value of the bonusMalus2 property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getBonusMalus2() {
        return bonusMalus2;
    }

    /**
     * Sets the value of the bonusMalus2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setBonusMalus2(Long value) {
        this.bonusMalus2 = value;
    }

    /**
     * Gets the value of the forceGlobalMta property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getForceGlobalMta() {
        return forceGlobalMta;
    }

    /**
     * Sets the value of the forceGlobalMta property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setForceGlobalMta(String value) {
        this.forceGlobalMta = value;
    }

    /**
     * Gets the value of the peReserveTableType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPeReserveTableType() {
        return peReserveTableType;
    }

    /**
     * Sets the value of the peReserveTableType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPeReserveTableType(String value) {
        this.peReserveTableType = value;
    }

    /**
     * Gets the value of the ignoreBackdatedVersions property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIgnoreBackdatedVersions() {
        return ignoreBackdatedVersions;
    }

    /**
     * Sets the value of the ignoreBackdatedVersions property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIgnoreBackdatedVersions(String value) {
        this.ignoreBackdatedVersions = value;
    }

    /**
     * Gets the value of the paymentFrequency property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getPaymentFrequency() {
        return paymentFrequency;
    }

    /**
     * Sets the value of the paymentFrequency property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setPaymentFrequency(Long value) {
        this.paymentFrequency = value;
    }

    /**
     * Gets the value of the paymentMethod property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaymentMethod() {
        return paymentMethod;
    }

    /**
     * Sets the value of the paymentMethod property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaymentMethod(String value) {
        this.paymentMethod = value;
    }

    /**
     * Gets the value of the paymentDetailsId property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getPaymentDetailsId() {
        return paymentDetailsId;
    }

    /**
     * Sets the value of the paymentDetailsId property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setPaymentDetailsId(BigInteger value) {
        this.paymentDetailsId = value;
    }

    /**
     * Gets the value of the instlPlanType property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getInstlPlanType() {
        return instlPlanType;
    }

    /**
     * Sets the value of the instlPlanType property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setInstlPlanType(Long value) {
        this.instlPlanType = value;
    }

    /**
     * Gets the value of the premiumRecovery property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPremiumRecovery() {
        return premiumRecovery;
    }

    /**
     * Sets the value of the premiumRecovery property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPremiumRecovery(String value) {
        this.premiumRecovery = value;
    }

    /**
     * Gets the value of the accountNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getAccountNo() {
        return accountNo;
    }

    /**
     * Sets the value of the accountNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setAccountNo(Long value) {
        this.accountNo = value;
    }

    /**
     * Gets the value of the centerCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCenterCode() {
        return centerCode;
    }

    /**
     * Sets the value of the centerCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCenterCode(String value) {
        this.centerCode = value;
    }

    /**
     * Gets the value of the productTax1 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getProductTax1() {
        return productTax1;
    }

    /**
     * Sets the value of the productTax1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setProductTax1(BigDecimal value) {
        this.productTax1 = value;
    }

    /**
     * Gets the value of the productTax2 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getProductTax2() {
        return productTax2;
    }

    /**
     * Sets the value of the productTax2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setProductTax2(BigDecimal value) {
        this.productTax2 = value;
    }

    /**
     * Gets the value of the productTax3 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getProductTax3() {
        return productTax3;
    }

    /**
     * Sets the value of the productTax3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setProductTax3(BigDecimal value) {
        this.productTax3 = value;
    }

    /**
     * Gets the value of the productTax4 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getProductTax4() {
        return productTax4;
    }

    /**
     * Sets the value of the productTax4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setProductTax4(BigDecimal value) {
        this.productTax4 = value;
    }

    /**
     * Gets the value of the productTax5 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getProductTax5() {
        return productTax5;
    }

    /**
     * Sets the value of the productTax5 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setProductTax5(BigDecimal value) {
        this.productTax5 = value;
    }

    /**
     * Gets the value of the productTax6 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getProductTax6() {
        return productTax6;
    }

    /**
     * Sets the value of the productTax6 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setProductTax6(BigDecimal value) {
        this.productTax6 = value;
    }

    /**
     * Gets the value of the productTax7 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getProductTax7() {
        return productTax7;
    }

    /**
     * Sets the value of the productTax7 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setProductTax7(BigDecimal value) {
        this.productTax7 = value;
    }

    /**
     * Gets the value of the seqNoQop property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getSeqNoQop() {
        return seqNoQop;
    }

    /**
     * Sets the value of the seqNoQop property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setSeqNoQop(Long value) {
        this.seqNoQop = value;
    }

    /**
     * Gets the value of the policyLineObjectCollection property.
     * 
     * @return
     *     possible object is
     *     {@link PolicyLineObjectCollection }
     *     
     */
    public PolicyLineObjectCollection getPolicyLineObjectCollection() {
        return policyLineObjectCollection;
    }

    /**
     * Sets the value of the policyLineObjectCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link PolicyLineObjectCollection }
     *     
     */
    public void setPolicyLineObjectCollection(PolicyLineObjectCollection value) {
        this.policyLineObjectCollection = value;
    }

    /**
     * Gets the value of the clauseCollection property.
     * 
     * @return
     *     possible object is
     *     {@link ClauseCollection }
     *     
     */
    public ClauseCollection getClauseCollection() {
        return clauseCollection;
    }

    /**
     * Sets the value of the clauseCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link ClauseCollection }
     *     
     */
    public void setClauseCollection(ClauseCollection value) {
        this.clauseCollection = value;
    }

    /**
     * Gets the value of the premiumRiskSplitCollection property.
     * 
     * @return
     *     possible object is
     *     {@link PremiumRiskSplitCollection }
     *     
     */
    public PremiumRiskSplitCollection getPremiumRiskSplitCollection() {
        return premiumRiskSplitCollection;
    }

    /**
     * Sets the value of the premiumRiskSplitCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link PremiumRiskSplitCollection }
     *     
     */
    public void setPremiumRiskSplitCollection(PremiumRiskSplitCollection value) {
        this.premiumRiskSplitCollection = value;
    }

    /**
     * Gets the value of the debitAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDebitAmount() {
        return debitAmount;
    }

    /**
     * Sets the value of the debitAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDebitAmount(BigDecimal value) {
        this.debitAmount = value;
    }

}
