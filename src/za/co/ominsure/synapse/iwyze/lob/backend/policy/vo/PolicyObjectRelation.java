
package za.co.ominsure.synapse.iwyze.lob.backend.policy.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Additional comments
 * 
 * <p>Java class for PolicyObjectRelation complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PolicyObjectRelation">
 *   &lt;complexContent>
 *     &lt;extension base="{http://infrastructure.tia.dk/schema/party/v2/}Relation">
 *       &lt;sequence>
 *         &lt;element name="policyObjectNo" type="{http://infrastructure.tia.dk/schema/common/v2/}long10" minOccurs="0"/>
 *         &lt;element name="memberNameIdNo" type="{http://infrastructure.tia.dk/schema/common/v2/}long10" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PolicyObjectRelation", namespace = "http://infrastructure.tia.dk/schema/party/v2/", propOrder = {
    "policyObjectNo",
    "memberNameIdNo"
})
public class PolicyObjectRelation
    extends Relation
{

    @XmlElement(nillable = true)
    protected Long policyObjectNo;
    @XmlElement(nillable = true)
    protected Long memberNameIdNo;

    /**
     * Gets the value of the policyObjectNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getPolicyObjectNo() {
        return policyObjectNo;
    }

    /**
     * Sets the value of the policyObjectNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setPolicyObjectNo(Long value) {
        this.policyObjectNo = value;
    }

    /**
     * Gets the value of the memberNameIdNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getMemberNameIdNo() {
        return memberNameIdNo;
    }

    /**
     * Sets the value of the memberNameIdNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setMemberNameIdNo(Long value) {
        this.memberNameIdNo = value;
    }

}
