
package za.co.ominsure.synapse.iwyze.lob.backend.search.payment.vo;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * Is mapped into TIA metadata type "obj_filter_token_acc_item".
 * 
 * <p>Java class for FilterTokenAccItem complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FilterTokenAccItem">
 *   &lt;complexContent>
 *     &lt;extension base="{http://infrastructure.tia.dk/schema/account/v2/}FilterTokenAccStatement">
 *       &lt;sequence>
 *         &lt;element name="dateType" type="{http://infrastructure.tia.dk/schema/account/v2/}dateType" minOccurs="0"/>
 *         &lt;element name="accountItemNo" type="{http://infrastructure.tia.dk/schema/common/v2/}seqNo" minOccurs="0"/>
 *         &lt;element name="centerCode" type="{http://infrastructure.tia.dk/schema/common/v2/}centerCode" minOccurs="0"/>
 *         &lt;element name="openItemsOnlyYN" type="{http://infrastructure.tia.dk/schema/common/v2/}tiaYN" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FilterTokenAccItem", propOrder = {
    "dateType",
    "accountItemNo",
    "centerCode",
    "openItemsOnlyYN"
})
public class FilterTokenAccItem
    extends FilterTokenAccStatement
{

    @XmlElementRef(name = "dateType", namespace = "http://infrastructure.tia.dk/schema/account/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> dateType;
    @XmlElementRef(name = "accountItemNo", namespace = "http://infrastructure.tia.dk/schema/account/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<Long> accountItemNo;
    @XmlElementRef(name = "centerCode", namespace = "http://infrastructure.tia.dk/schema/account/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> centerCode;
    @XmlElementRef(name = "openItemsOnlyYN", namespace = "http://infrastructure.tia.dk/schema/account/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> openItemsOnlyYN;

    /**
     * Gets the value of the dateType property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDateType() {
        return dateType;
    }

    /**
     * Sets the value of the dateType property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDateType(JAXBElement<String> value) {
        this.dateType = value;
    }

    /**
     * Gets the value of the accountItemNo property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Long }{@code >}
     *     
     */
    public JAXBElement<Long> getAccountItemNo() {
        return accountItemNo;
    }

    /**
     * Sets the value of the accountItemNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Long }{@code >}
     *     
     */
    public void setAccountItemNo(JAXBElement<Long> value) {
        this.accountItemNo = value;
    }

    /**
     * Gets the value of the centerCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCenterCode() {
        return centerCode;
    }

    /**
     * Sets the value of the centerCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCenterCode(JAXBElement<String> value) {
        this.centerCode = value;
    }

    /**
     * Gets the value of the openItemsOnlyYN property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOpenItemsOnlyYN() {
        return openItemsOnlyYN;
    }

    /**
     * Sets the value of the openItemsOnlyYN property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOpenItemsOnlyYN(JAXBElement<String> value) {
        this.openItemsOnlyYN = value;
    }

}
