
package za.co.ominsure.synapse.iwyze.lob.backend.policy.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Additional comments
 * 
 * <p>Java class for ProdLineDependency complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ProdLineDependency">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="dependencyType" type="{http://infrastructure.tia.dk/schema/common/v2/}string5" minOccurs="0"/>
 *         &lt;element name="dependentProdLineId" type="{http://infrastructure.tia.dk/schema/common/v2/}string5" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProdLineDependency", propOrder = {
    "dependencyType",
    "dependentProdLineId"
})
public class ProdLineDependency {

    @XmlElement(nillable = true)
    protected String dependencyType;
    @XmlElement(nillable = true)
    protected String dependentProdLineId;

    /**
     * Gets the value of the dependencyType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDependencyType() {
        return dependencyType;
    }

    /**
     * Sets the value of the dependencyType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDependencyType(String value) {
        this.dependencyType = value;
    }

    /**
     * Gets the value of the dependentProdLineId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDependentProdLineId() {
        return dependentProdLineId;
    }

    /**
     * Sets the value of the dependentProdLineId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDependentProdLineId(String value) {
        this.dependentProdLineId = value;
    }

}
