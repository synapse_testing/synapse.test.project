
package za.co.ominsure.synapse.iwyze.lob.backend.claim.vo;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Additional comments
 * 
 * <p>Java class for ClaimAccItem complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ClaimAccItem">
 *   &lt;complexContent>
 *     &lt;extension base="{http://infrastructure.tia.dk/schema/common/v2/}TIAObject">
 *       &lt;sequence>
 *         &lt;element name="accItemNo" type="{http://infrastructure.tia.dk/schema/common/v2/}long10" minOccurs="0"/>
 *         &lt;element name="accountNo" type="{http://infrastructure.tia.dk/schema/common/v2/}long10" minOccurs="0"/>
 *         &lt;element name="paymentMethod" type="{http://infrastructure.tia.dk/schema/common/v2/}string8" minOccurs="0"/>
 *         &lt;element name="meansPayNo" type="{http://infrastructure.tia.dk/schema/common/v2/}long10" minOccurs="0"/>
 *         &lt;element name="transDate" type="{http://infrastructure.tia.dk/schema/common/v2/}timestamp" minOccurs="0"/>
 *         &lt;element name="approvedBy" type="{http://infrastructure.tia.dk/schema/common/v2/}string8" minOccurs="0"/>
 *         &lt;element name="specification" type="{http://infrastructure.tia.dk/schema/common/v2/}string2000" minOccurs="0"/>
 *         &lt;element name="itemText" type="{http://infrastructure.tia.dk/schema/common/v2/}string60" minOccurs="0"/>
 *         &lt;element name="internalNote" type="{http://infrastructure.tia.dk/schema/common/v2/}string2000" minOccurs="0"/>
 *         &lt;element name="externalPaymentId" type="{http://infrastructure.tia.dk/schema/common/v2/}string60" minOccurs="0"/>
 *         &lt;element name="onHold" type="{http://infrastructure.tia.dk/schema/common/v2/}string1" minOccurs="0"/>
 *         &lt;element name="dueDate" type="{http://infrastructure.tia.dk/schema/common/v2/}timestamp" minOccurs="0"/>
 *         &lt;element name="receiverIdNo" type="{http://infrastructure.tia.dk/schema/common/v2/}long10" minOccurs="0"/>
 *         &lt;element name="transId" type="{http://infrastructure.tia.dk/schema/common/v2/}long10" minOccurs="0"/>
 *         &lt;element name="claimNo" type="{http://infrastructure.tia.dk/schema/common/v2/}long10" minOccurs="0"/>
 *         &lt;element name="paymentStatus" type="{http://infrastructure.tia.dk/schema/common/v2/}int3" minOccurs="0"/>
 *         &lt;element name="currencyCode" type="{http://infrastructure.tia.dk/schema/common/v2/}string3" minOccurs="0"/>
 *         &lt;element name="currencyAmount" type="{http://infrastructure.tia.dk/schema/common/v2/}amountwithoutrestriction" minOccurs="0"/>
 *         &lt;element name="itemReference" type="{http://infrastructure.tia.dk/schema/common/v2/}string60" minOccurs="0"/>
 *         &lt;element name="role" type="{http://infrastructure.tia.dk/schema/common/v2/}string3" minOccurs="0"/>
 *         &lt;element name="claimPaymentItemCollection" type="{http://infrastructure.tia.dk/schema/claim/v2/}ClaimPaymentItemCollection" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ClaimAccItem", propOrder = {
    "accItemNo",
    "accountNo",
    "paymentMethod",
    "meansPayNo",
    "transDate",
    "approvedBy",
    "specification",
    "itemText",
    "internalNote",
    "externalPaymentId",
    "onHold",
    "dueDate",
    "receiverIdNo",
    "transId",
    "claimNo",
    "paymentStatus",
    "currencyCode",
    "currencyAmount",
    "itemReference",
    "role",
    "claimPaymentItemCollection"
})
public class ClaimAccItem
    extends TIAObject
{

    @XmlElement(nillable = true)
    protected Long accItemNo;
    @XmlElement(nillable = true)
    protected Long accountNo;
    @XmlElement(nillable = true)
    protected String paymentMethod;
    @XmlElement(nillable = true)
    protected Long meansPayNo;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar transDate;
    @XmlElement(nillable = true)
    protected String approvedBy;
    @XmlElement(nillable = true)
    protected String specification;
    @XmlElement(nillable = true)
    protected String itemText;
    @XmlElement(nillable = true)
    protected String internalNote;
    @XmlElement(nillable = true)
    protected String externalPaymentId;
    @XmlElement(nillable = true)
    protected String onHold;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dueDate;
    @XmlElement(nillable = true)
    protected Long receiverIdNo;
    @XmlElement(nillable = true)
    protected Long transId;
    @XmlElement(nillable = true)
    protected Long claimNo;
    @XmlElement(nillable = true)
    protected Long paymentStatus;
    @XmlElement(nillable = true)
    protected String currencyCode;
    @XmlElement(nillable = true)
    protected BigDecimal currencyAmount;
    @XmlElement(nillable = true)
    protected String itemReference;
    @XmlElement(nillable = true)
    protected String role;
    protected ClaimPaymentItemCollection claimPaymentItemCollection;

    /**
     * Gets the value of the accItemNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getAccItemNo() {
        return accItemNo;
    }

    /**
     * Sets the value of the accItemNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setAccItemNo(Long value) {
        this.accItemNo = value;
    }

    /**
     * Gets the value of the accountNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getAccountNo() {
        return accountNo;
    }

    /**
     * Sets the value of the accountNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setAccountNo(Long value) {
        this.accountNo = value;
    }

    /**
     * Gets the value of the paymentMethod property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaymentMethod() {
        return paymentMethod;
    }

    /**
     * Sets the value of the paymentMethod property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaymentMethod(String value) {
        this.paymentMethod = value;
    }

    /**
     * Gets the value of the meansPayNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getMeansPayNo() {
        return meansPayNo;
    }

    /**
     * Sets the value of the meansPayNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setMeansPayNo(Long value) {
        this.meansPayNo = value;
    }

    /**
     * Gets the value of the transDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTransDate() {
        return transDate;
    }

    /**
     * Sets the value of the transDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTransDate(XMLGregorianCalendar value) {
        this.transDate = value;
    }

    /**
     * Gets the value of the approvedBy property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApprovedBy() {
        return approvedBy;
    }

    /**
     * Sets the value of the approvedBy property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApprovedBy(String value) {
        this.approvedBy = value;
    }

    /**
     * Gets the value of the specification property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSpecification() {
        return specification;
    }

    /**
     * Sets the value of the specification property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSpecification(String value) {
        this.specification = value;
    }

    /**
     * Gets the value of the itemText property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItemText() {
        return itemText;
    }

    /**
     * Sets the value of the itemText property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItemText(String value) {
        this.itemText = value;
    }

    /**
     * Gets the value of the internalNote property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInternalNote() {
        return internalNote;
    }

    /**
     * Sets the value of the internalNote property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInternalNote(String value) {
        this.internalNote = value;
    }

    /**
     * Gets the value of the externalPaymentId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExternalPaymentId() {
        return externalPaymentId;
    }

    /**
     * Sets the value of the externalPaymentId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExternalPaymentId(String value) {
        this.externalPaymentId = value;
    }

    /**
     * Gets the value of the onHold property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOnHold() {
        return onHold;
    }

    /**
     * Sets the value of the onHold property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOnHold(String value) {
        this.onHold = value;
    }

    /**
     * Gets the value of the dueDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDueDate() {
        return dueDate;
    }

    /**
     * Sets the value of the dueDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDueDate(XMLGregorianCalendar value) {
        this.dueDate = value;
    }

    /**
     * Gets the value of the receiverIdNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getReceiverIdNo() {
        return receiverIdNo;
    }

    /**
     * Sets the value of the receiverIdNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setReceiverIdNo(Long value) {
        this.receiverIdNo = value;
    }

    /**
     * Gets the value of the transId property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getTransId() {
        return transId;
    }

    /**
     * Sets the value of the transId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setTransId(Long value) {
        this.transId = value;
    }

    /**
     * Gets the value of the claimNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getClaimNo() {
        return claimNo;
    }

    /**
     * Sets the value of the claimNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setClaimNo(Long value) {
        this.claimNo = value;
    }

    /**
     * Gets the value of the paymentStatus property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getPaymentStatus() {
        return paymentStatus;
    }

    /**
     * Sets the value of the paymentStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setPaymentStatus(Long value) {
        this.paymentStatus = value;
    }

    /**
     * Gets the value of the currencyCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrencyCode() {
        return currencyCode;
    }

    /**
     * Sets the value of the currencyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrencyCode(String value) {
        this.currencyCode = value;
    }

    /**
     * Gets the value of the currencyAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCurrencyAmount() {
        return currencyAmount;
    }

    /**
     * Sets the value of the currencyAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCurrencyAmount(BigDecimal value) {
        this.currencyAmount = value;
    }

    /**
     * Gets the value of the itemReference property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItemReference() {
        return itemReference;
    }

    /**
     * Sets the value of the itemReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItemReference(String value) {
        this.itemReference = value;
    }

    /**
     * Gets the value of the role property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRole() {
        return role;
    }

    /**
     * Sets the value of the role property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRole(String value) {
        this.role = value;
    }

    /**
     * Gets the value of the claimPaymentItemCollection property.
     * 
     * @return
     *     possible object is
     *     {@link ClaimPaymentItemCollection }
     *     
     */
    public ClaimPaymentItemCollection getClaimPaymentItemCollection() {
        return claimPaymentItemCollection;
    }

    /**
     * Sets the value of the claimPaymentItemCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link ClaimPaymentItemCollection }
     *     
     */
    public void setClaimPaymentItemCollection(ClaimPaymentItemCollection value) {
        this.claimPaymentItemCollection = value;
    }

}
