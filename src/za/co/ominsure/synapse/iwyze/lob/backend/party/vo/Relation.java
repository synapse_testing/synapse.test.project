
package za.co.ominsure.synapse.iwyze.lob.backend.party.vo;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Is mapped into TIA metadata type "obj_relation".
 * 
 * <p>Java class for Relation complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Relation">
 *   &lt;complexContent>
 *     &lt;extension base="{http://infrastructure.tia.dk/schema/common/v2/}TIAObject">
 *       &lt;sequence>
 *         &lt;element name="relationType" type="{http://infrastructure.tia.dk/schema/party/v2/}relationType" minOccurs="0"/>
 *         &lt;element name="description" type="{http://infrastructure.tia.dk/schema/party/v2/}description" minOccurs="0"/>
 *         &lt;element name="startDate" type="{http://infrastructure.tia.dk/schema/common/v2/}date" minOccurs="0"/>
 *         &lt;element name="endDate" type="{http://infrastructure.tia.dk/schema/common/v2/}date" minOccurs="0"/>
 *         &lt;element name="ownerDesc" type="{http://infrastructure.tia.dk/schema/common/v2/}string2000" minOccurs="0"/>
 *         &lt;element name="transId" type="{http://infrastructure.tia.dk/schema/common/v2/}transId" minOccurs="0"/>
 *         &lt;element name="seqNo" type="{http://infrastructure.tia.dk/schema/common/v2/}seqNo" minOccurs="0"/>
 *         &lt;element name="newest" type="{http://infrastructure.tia.dk/schema/party/v2/}newest" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Relation", propOrder = {
    "relationType",
    "description",
    "startDate",
    "endDate",
    "ownerDesc",
    "transId",
    "seqNo",
    "newest"
})
@XmlSeeAlso({
    PolicyRelation.class,
    ClaimEventRelation.class,
    PolicyObjectRelation.class,
    AccountRelation.class,
    AffinityRelation.class,
    ClaimThirdPartyRelation.class,
    ClaimRelation.class,
    PolicyLineRelation.class,
    PartyRelation.class,
    SubclaimRelation.class,
    ClaimItemRelation.class
})
public abstract class Relation
    extends TIAObject
{

    @XmlElementRef(name = "relationType", namespace = "http://infrastructure.tia.dk/schema/party/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> relationType;
    @XmlElementRef(name = "description", namespace = "http://infrastructure.tia.dk/schema/party/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> description;
    @XmlElementRef(name = "startDate", namespace = "http://infrastructure.tia.dk/schema/party/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> startDate;
    @XmlElementRef(name = "endDate", namespace = "http://infrastructure.tia.dk/schema/party/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> endDate;
    @XmlElementRef(name = "ownerDesc", namespace = "http://infrastructure.tia.dk/schema/party/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> ownerDesc;
    @XmlElementRef(name = "transId", namespace = "http://infrastructure.tia.dk/schema/party/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<Long> transId;
    @XmlElementRef(name = "seqNo", namespace = "http://infrastructure.tia.dk/schema/party/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<Long> seqNo;
    @XmlElementRef(name = "newest", namespace = "http://infrastructure.tia.dk/schema/party/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> newest;

    /**
     * Gets the value of the relationType property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRelationType() {
        return relationType;
    }

    /**
     * Sets the value of the relationType property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRelationType(JAXBElement<String> value) {
        this.relationType = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDescription(JAXBElement<String> value) {
        this.description = value;
    }

    /**
     * Gets the value of the startDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getStartDate() {
        return startDate;
    }

    /**
     * Sets the value of the startDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setStartDate(JAXBElement<XMLGregorianCalendar> value) {
        this.startDate = value;
    }

    /**
     * Gets the value of the endDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getEndDate() {
        return endDate;
    }

    /**
     * Sets the value of the endDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setEndDate(JAXBElement<XMLGregorianCalendar> value) {
        this.endDate = value;
    }

    /**
     * Gets the value of the ownerDesc property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOwnerDesc() {
        return ownerDesc;
    }

    /**
     * Sets the value of the ownerDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOwnerDesc(JAXBElement<String> value) {
        this.ownerDesc = value;
    }

    /**
     * Gets the value of the transId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Long }{@code >}
     *     
     */
    public JAXBElement<Long> getTransId() {
        return transId;
    }

    /**
     * Sets the value of the transId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Long }{@code >}
     *     
     */
    public void setTransId(JAXBElement<Long> value) {
        this.transId = value;
    }

    /**
     * Gets the value of the seqNo property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Long }{@code >}
     *     
     */
    public JAXBElement<Long> getSeqNo() {
        return seqNo;
    }

    /**
     * Sets the value of the seqNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Long }{@code >}
     *     
     */
    public void setSeqNo(JAXBElement<Long> value) {
        this.seqNo = value;
    }

    /**
     * Gets the value of the newest property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNewest() {
        return newest;
    }

    /**
     * Sets the value of the newest property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNewest(JAXBElement<String> value) {
        this.newest = value;
    }

}
