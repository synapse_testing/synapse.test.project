
package za.co.ominsure.synapse.iwyze.lob.backend.search.payment.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="inputToken" type="{http://infrastructure.tia.dk/schema/common/v2/}InputToken"/>
 *         &lt;element name="collectionRequestConfirmation" type="{http://infrastructure.tia.dk/schema/account/v2/}CollectionRequestConfirm"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "inputToken",
    "collectionRequestConfirmation"
})
@XmlRootElement(name = "registerCollectionRequestRequest")
public class RegisterCollectionRequestRequest {

    @XmlElement(required = true)
    protected InputToken inputToken;
    @XmlElement(required = true)
    protected CollectionRequestConfirm collectionRequestConfirmation;

    /**
     * Gets the value of the inputToken property.
     * 
     * @return
     *     possible object is
     *     {@link InputToken }
     *     
     */
    public InputToken getInputToken() {
        return inputToken;
    }

    /**
     * Sets the value of the inputToken property.
     * 
     * @param value
     *     allowed object is
     *     {@link InputToken }
     *     
     */
    public void setInputToken(InputToken value) {
        this.inputToken = value;
    }

    /**
     * Gets the value of the collectionRequestConfirmation property.
     * 
     * @return
     *     possible object is
     *     {@link CollectionRequestConfirm }
     *     
     */
    public CollectionRequestConfirm getCollectionRequestConfirmation() {
        return collectionRequestConfirmation;
    }

    /**
     * Sets the value of the collectionRequestConfirmation property.
     * 
     * @param value
     *     allowed object is
     *     {@link CollectionRequestConfirm }
     *     
     */
    public void setCollectionRequestConfirmation(CollectionRequestConfirm value) {
        this.collectionRequestConfirmation = value;
    }

}
