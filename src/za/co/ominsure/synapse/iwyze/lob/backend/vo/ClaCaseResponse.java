package za.co.ominsure.synapse.iwyze.lob.backend.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ClaCaseResponse", propOrder = { "eventNumber", "claimNumber", "sequenceNumber" })
public class ClaCaseResponse {
	@XmlElement(name = "eventNumber")
	protected String eventNumber;
	@XmlElement(name = "claimNumber")
	protected String claimNumber;
	@XmlElement(name = "sequenceNumber")
	protected String sequenceNumber;

	public String getEventNumber() {
		return eventNumber;
	}

	public void setEventNumber(String eventNumber) {
		this.eventNumber = eventNumber;
	}

	public String getClaimNumber() {
		return claimNumber;
	}

	public void setClaimNumber(String claimNumber) {
		this.claimNumber = claimNumber;
	}

	public String getSequenceNumber() {
		return sequenceNumber;
	}

	public void setSequenceNumber(String sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}

}
