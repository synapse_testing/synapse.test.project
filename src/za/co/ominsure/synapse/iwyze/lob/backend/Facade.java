package za.co.ominsure.synapse.iwyze.lob.backend;

import java.sql.SQLException;

import javax.ejb.Local;

import za.co.ominsure.synapse.cdm.claim.Audits;
import za.co.ominsure.synapse.cdm.claim.Case;
import za.co.ominsure.synapse.cdm.claim.Claim;
import za.co.ominsure.synapse.cdm.common.content.SearchRequest;
import za.co.ominsure.synapse.cdm.common.content.SearchResult;
import za.co.ominsure.synapse.cdm.finance.Payments;
import za.co.ominsure.synapse.cdm.nonapproved.ThirdParties;
import za.co.ominsure.synapse.cdm.policy.PolicyOld;
import za.co.ominsure.synapse.cdm.workflow.ajs.CreateRequest;
import za.co.ominsure.synapse.cdm.workflow.ajs.RecoveryStatusChangeRequest;
import za.co.ominsure.synapse.common.exception.HttpException;
import za.co.ominsure.synapse.iwyze.lob.backend.caseItem.vo.CaseItemGenericResponse;
import za.co.ominsure.synapse.iwyze.lob.backend.servicesupplier.vo.GetServiceSupplierResponse;
import za.co.ominsure.synapse.iwyze.lob.backend.vo.AddClaimsLogRequest;
import za.co.ominsure.synapse.iwyze.lob.backend.vo.AddClaimsLogResponse;
import za.co.ominsure.synapse.iwyze.lob.backend.vo.GetClaimsLogResponse;
import za.co.ominsure.synapse.iwyze.lob.backend.vo.GetPolicyNumberResponse;

@Local
public interface Facade {

	public Audits getAudits(String claimEventNumber, int offset, int pageSize) throws HttpException;
	
	public GetClaimsLogResponse getClaimsLog(String claimNumber) throws HttpException, SQLException;
	
	public void addAudit(Audits audits) throws HttpException;
	
	public AddClaimsLogResponse addClaimLog(AddClaimsLogRequest request) throws HttpException,SQLException;
	
	public Case getCaseInfo(Long claimNumber, long caseNumber) throws HttpException;
	
	public GetPolicyNumberResponse getPolicyDetail(String policyHolderId, String secret) throws HttpException;
	
	public ThirdParties getThirdPartyInfo(String nameIdNumber, String siteName, int offset, int pageSize) throws HttpException;
	
	public PolicyOld getpolicyInformation(Long policyNumber,String caseNumber, String effectiveDate, Long lossRatioYears, String objectSequenceNumber) throws HttpException;
	
	public Payments getPayments(String claimNumber, int offset, int pagesize) throws HttpException, SQLException;
	
	public SearchResult getDocumentsForPolicy(SearchRequest searchRequest) throws HttpException;
	
	public Claim getClaim(String claimEventNumber, String siteName, int offset, int pageSize) throws HttpException;
	
	public CreateRequest getAJSData(String claimNumber) throws HttpException, SQLException;
	
	public PolicyOld searchPolicy(String nameIdNumber,String caseNumber) throws HttpException;
	
	public Claim searchParty(String idNumber, String siteName) throws HttpException;
	
	public void claimStatusUpdate(String claimNumber, RecoveryStatusChangeRequest request) throws HttpException;
	
	public ThirdParties searchSubClaim(String claimNumber, String siteName) throws HttpException;
	
	public GetServiceSupplierResponse getServiceSupplier(String receiverIdNumber, String sitename) throws SQLException, HttpException;
	
	public CaseItemGenericResponse searchCaseItem(String claimNumber,String siteName) throws HttpException;
	
}
