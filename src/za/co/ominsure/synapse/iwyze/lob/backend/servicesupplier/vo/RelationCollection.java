
package za.co.ominsure.synapse.iwyze.lob.backend.servicesupplier.vo;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * Is mapped into TIA metadata type "tab_relation".
 * 
 * <p>Java class for RelationCollection complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RelationCollection">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;sequence>
 *           &lt;element name="partyRelation" type="{http://infrastructure.tia.dk/schema/party/v2/}PartyRelation" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;/sequence>
 *         &lt;sequence>
 *           &lt;element name="affinityRelation" type="{http://infrastructure.tia.dk/schema/party/v2/}AffinityRelation" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;/sequence>
 *         &lt;sequence>
 *           &lt;element name="policyRelation" type="{http://infrastructure.tia.dk/schema/party/v2/}PolicyRelation" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;/sequence>
 *         &lt;sequence>
 *           &lt;element name="policyLineRelation" type="{http://infrastructure.tia.dk/schema/party/v2/}PolicyLineRelation" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;/sequence>
 *         &lt;sequence>
 *           &lt;element name="policyObjectRelation" type="{http://infrastructure.tia.dk/schema/party/v2/}PolicyObjectRelation" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;/sequence>
 *         &lt;sequence>
 *           &lt;element name="claimEventRelation" type="{http://infrastructure.tia.dk/schema/party/v2/}ClaimEventRelation" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;/sequence>
 *         &lt;sequence>
 *           &lt;element name="claimRelation" type="{http://infrastructure.tia.dk/schema/party/v2/}ClaimRelation" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;/sequence>
 *         &lt;sequence>
 *           &lt;element name="subclaimRelation" type="{http://infrastructure.tia.dk/schema/party/v2/}SubclaimRelation" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;/sequence>
 *         &lt;sequence>
 *           &lt;element name="claimItemRelation" type="{http://infrastructure.tia.dk/schema/party/v2/}ClaimItemRelation" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;/sequence>
 *         &lt;sequence>
 *           &lt;element name="claimThirdPartyRelation" type="{http://infrastructure.tia.dk/schema/party/v2/}ClaimThirdPartyRelation" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;/sequence>
 *         &lt;sequence>
 *           &lt;element name="accountRelation" type="{http://infrastructure.tia.dk/schema/party/v2/}AccountRelation" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;/sequence>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RelationCollection", propOrder = {
    "partyRelation",
    "affinityRelation",
    "policyRelation",
    "policyLineRelation",
    "policyObjectRelation",
    "claimEventRelation",
    "claimRelation",
    "subclaimRelation",
    "claimItemRelation",
    "claimThirdPartyRelation",
    "accountRelation"
})
public class RelationCollection {

    protected List<PartyRelation> partyRelation;
    protected List<AffinityRelation> affinityRelation;
    protected List<PolicyRelation> policyRelation;
    protected List<PolicyLineRelation> policyLineRelation;
    protected List<PolicyObjectRelation> policyObjectRelation;
    protected List<ClaimEventRelation> claimEventRelation;
    protected List<ClaimRelation> claimRelation;
    protected List<SubclaimRelation> subclaimRelation;
    protected List<ClaimItemRelation> claimItemRelation;
    protected List<ClaimThirdPartyRelation> claimThirdPartyRelation;
    protected List<AccountRelation> accountRelation;

    /**
     * Gets the value of the partyRelation property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the partyRelation property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPartyRelation().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PartyRelation }
     * 
     * 
     */
    public List<PartyRelation> getPartyRelation() {
        if (partyRelation == null) {
            partyRelation = new ArrayList<PartyRelation>();
        }
        return this.partyRelation;
    }

    /**
     * Gets the value of the affinityRelation property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the affinityRelation property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAffinityRelation().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AffinityRelation }
     * 
     * 
     */
    public List<AffinityRelation> getAffinityRelation() {
        if (affinityRelation == null) {
            affinityRelation = new ArrayList<AffinityRelation>();
        }
        return this.affinityRelation;
    }

    /**
     * Gets the value of the policyRelation property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the policyRelation property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPolicyRelation().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PolicyRelation }
     * 
     * 
     */
    public List<PolicyRelation> getPolicyRelation() {
        if (policyRelation == null) {
            policyRelation = new ArrayList<PolicyRelation>();
        }
        return this.policyRelation;
    }

    /**
     * Gets the value of the policyLineRelation property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the policyLineRelation property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPolicyLineRelation().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PolicyLineRelation }
     * 
     * 
     */
    public List<PolicyLineRelation> getPolicyLineRelation() {
        if (policyLineRelation == null) {
            policyLineRelation = new ArrayList<PolicyLineRelation>();
        }
        return this.policyLineRelation;
    }

    /**
     * Gets the value of the policyObjectRelation property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the policyObjectRelation property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPolicyObjectRelation().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PolicyObjectRelation }
     * 
     * 
     */
    public List<PolicyObjectRelation> getPolicyObjectRelation() {
        if (policyObjectRelation == null) {
            policyObjectRelation = new ArrayList<PolicyObjectRelation>();
        }
        return this.policyObjectRelation;
    }

    /**
     * Gets the value of the claimEventRelation property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the claimEventRelation property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getClaimEventRelation().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ClaimEventRelation }
     * 
     * 
     */
    public List<ClaimEventRelation> getClaimEventRelation() {
        if (claimEventRelation == null) {
            claimEventRelation = new ArrayList<ClaimEventRelation>();
        }
        return this.claimEventRelation;
    }

    /**
     * Gets the value of the claimRelation property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the claimRelation property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getClaimRelation().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ClaimRelation }
     * 
     * 
     */
    public List<ClaimRelation> getClaimRelation() {
        if (claimRelation == null) {
            claimRelation = new ArrayList<ClaimRelation>();
        }
        return this.claimRelation;
    }

    /**
     * Gets the value of the subclaimRelation property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the subclaimRelation property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSubclaimRelation().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SubclaimRelation }
     * 
     * 
     */
    public List<SubclaimRelation> getSubclaimRelation() {
        if (subclaimRelation == null) {
            subclaimRelation = new ArrayList<SubclaimRelation>();
        }
        return this.subclaimRelation;
    }

    /**
     * Gets the value of the claimItemRelation property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the claimItemRelation property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getClaimItemRelation().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ClaimItemRelation }
     * 
     * 
     */
    public List<ClaimItemRelation> getClaimItemRelation() {
        if (claimItemRelation == null) {
            claimItemRelation = new ArrayList<ClaimItemRelation>();
        }
        return this.claimItemRelation;
    }

    /**
     * Gets the value of the claimThirdPartyRelation property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the claimThirdPartyRelation property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getClaimThirdPartyRelation().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ClaimThirdPartyRelation }
     * 
     * 
     */
    public List<ClaimThirdPartyRelation> getClaimThirdPartyRelation() {
        if (claimThirdPartyRelation == null) {
            claimThirdPartyRelation = new ArrayList<ClaimThirdPartyRelation>();
        }
        return this.claimThirdPartyRelation;
    }

    /**
     * Gets the value of the accountRelation property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the accountRelation property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAccountRelation().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AccountRelation }
     * 
     * 
     */
    public List<AccountRelation> getAccountRelation() {
        if (accountRelation == null) {
            accountRelation = new ArrayList<AccountRelation>();
        }
        return this.accountRelation;
    }

}
