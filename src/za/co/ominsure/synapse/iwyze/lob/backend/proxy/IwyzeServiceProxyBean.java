package za.co.ominsure.synapse.iwyze.lob.backend.proxy;

import java.math.BigInteger;
import java.net.URL;
import java.net.URLEncoder;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.sql.DataSource;
import javax.ws.rs.core.Response;
import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;

import org.apache.commons.lang3.StringUtils;

import com.owlike.genson.Genson;
import com.owlike.genson.GensonBuilder;
import com.owlike.genson.ext.jaxb.JAXBBundle;

import za.co.ominsure.synapse.cdm.claim.Audit;
import za.co.ominsure.synapse.cdm.claim.Audits;
import za.co.ominsure.synapse.cdm.claim.Case;
import za.co.ominsure.synapse.cdm.claim.Claim;
import za.co.ominsure.synapse.cdm.common.content.SearchRequest;
import za.co.ominsure.synapse.cdm.common.content.SearchResult;
import za.co.ominsure.synapse.cdm.finance.Payment;
import za.co.ominsure.synapse.cdm.finance.Payments;
import za.co.ominsure.synapse.cdm.nonapproved.ThirdParties;
import za.co.ominsure.synapse.cdm.nonapproved.ThirdParty;
import za.co.ominsure.synapse.cdm.policy.PolicyOld;
import za.co.ominsure.synapse.cdm.workflow.ajs.CreateRequest;
import za.co.ominsure.synapse.cdm.workflow.ajs.RecoveryStatusChangeRequest;
import za.co.ominsure.synapse.common.config.ServiceConfig;
import za.co.ominsure.synapse.common.constants.SynapseConstants;
import za.co.ominsure.synapse.common.exception.HttpException;
import za.co.ominsure.synapse.common.interceptor.DebugInterceptor;
import za.co.ominsure.synapse.common.log.Synlog;
import za.co.ominsure.synapse.common.rest.ServiceBridge;
import za.co.ominsure.synapse.common.util.DatabaseUtil;
import za.co.ominsure.synapse.iwyze.lob.backend.caseItem.vo.CaseItem;
import za.co.ominsure.synapse.iwyze.lob.backend.caseItem.vo.CaseItemGenericResponse;
import za.co.ominsure.synapse.iwyze.lob.backend.caseItem.vo.PtCase;
import za.co.ominsure.synapse.iwyze.lob.backend.caseItem.vo.SearchCaseItemRequest;
import za.co.ominsure.synapse.iwyze.lob.backend.caseItem.vo.SearchCaseItemResponse;
import za.co.ominsure.synapse.iwyze.lob.backend.caseItem.vo.SearchTokenCase;
import za.co.ominsure.synapse.iwyze.lob.backend.caseItem.vo.SvCase;
import za.co.ominsure.synapse.iwyze.lob.backend.claim.payment.vo.ClaimAccItem;
import za.co.ominsure.synapse.iwyze.lob.backend.claim.payment.vo.ClaimPaymentItem;
import za.co.ominsure.synapse.iwyze.lob.backend.claim.payment.vo.GetClaimPaymentsRequest;
import za.co.ominsure.synapse.iwyze.lob.backend.claim.payment.vo.GetClaimPaymentsResponse;
import za.co.ominsure.synapse.iwyze.lob.backend.claim.payment.vo.PtClaimPayment;
import za.co.ominsure.synapse.iwyze.lob.backend.claim.payment.vo.SvClaimPayment;
import za.co.ominsure.synapse.iwyze.lob.backend.claim.vo.AddClaimResponse;
import za.co.ominsure.synapse.iwyze.lob.backend.claim.vo.Answer;
import za.co.ominsure.synapse.iwyze.lob.backend.claim.vo.AnswerCollection;
import za.co.ominsure.synapse.iwyze.lob.backend.claim.vo.AnswerSetCollection;
import za.co.ominsure.synapse.iwyze.lob.backend.claim.vo.ClaimItemCollection;
import za.co.ominsure.synapse.iwyze.lob.backend.claim.vo.GetClaimRequest;
import za.co.ominsure.synapse.iwyze.lob.backend.claim.vo.GetClaimResponse;
import za.co.ominsure.synapse.iwyze.lob.backend.claim.vo.InputToken;
import za.co.ominsure.synapse.iwyze.lob.backend.claim.vo.PtClaim;
import za.co.ominsure.synapse.iwyze.lob.backend.claim.vo.Subclaim;
import za.co.ominsure.synapse.iwyze.lob.backend.claim.vo.SubclaimCollection;
import za.co.ominsure.synapse.iwyze.lob.backend.claim.vo.SvClaim;
import za.co.ominsure.synapse.iwyze.lob.backend.interactive.claim.vo.ClaimEvent;
import za.co.ominsure.synapse.iwyze.lob.backend.interactive.claim.vo.ClaimItem;
import za.co.ominsure.synapse.iwyze.lob.backend.interactive.claim.vo.Flexfield;
import za.co.ominsure.synapse.iwyze.lob.backend.interactive.claim.vo.PtClaimInteractive;
import za.co.ominsure.synapse.iwyze.lob.backend.interactive.claim.vo.SearchSubclaimRequest;
import za.co.ominsure.synapse.iwyze.lob.backend.interactive.claim.vo.SearchSubclaimResponse;
import za.co.ominsure.synapse.iwyze.lob.backend.interactive.claim.vo.SearchTokenSubclaim;
import za.co.ominsure.synapse.iwyze.lob.backend.interactive.claim.vo.SvClaimInteractive;
import za.co.ominsure.synapse.iwyze.lob.backend.party.vo.Address;
import za.co.ominsure.synapse.iwyze.lob.backend.party.vo.ContactInfo;
import za.co.ominsure.synapse.iwyze.lob.backend.party.vo.GetPartyRequest;
import za.co.ominsure.synapse.iwyze.lob.backend.party.vo.GetPartyResponse;
import za.co.ominsure.synapse.iwyze.lob.backend.party.vo.MessageCollection;
import za.co.ominsure.synapse.iwyze.lob.backend.party.vo.MessageDetail;
import za.co.ominsure.synapse.iwyze.lob.backend.party.vo.PartyElement;
import za.co.ominsure.synapse.iwyze.lob.backend.party.vo.PtParty;
import za.co.ominsure.synapse.iwyze.lob.backend.party.vo.Result;
import za.co.ominsure.synapse.iwyze.lob.backend.party.vo.SearchPartyRequest;
import za.co.ominsure.synapse.iwyze.lob.backend.party.vo.SearchPartyResponse;
import za.co.ominsure.synapse.iwyze.lob.backend.party.vo.SearchTokenPar;
import za.co.ominsure.synapse.iwyze.lob.backend.party.vo.SvParty;
import za.co.ominsure.synapse.iwyze.lob.backend.policy.vo.GetPolicyRequest;
import za.co.ominsure.synapse.iwyze.lob.backend.policy.vo.GetPolicyResponse;
import za.co.ominsure.synapse.iwyze.lob.backend.policy.vo.PolicyLine;
import za.co.ominsure.synapse.iwyze.lob.backend.policy.vo.PolicyLineObject;
import za.co.ominsure.synapse.iwyze.lob.backend.policy.vo.PolicySearchResult;
import za.co.ominsure.synapse.iwyze.lob.backend.policy.vo.PtPolicyAtomic;
import za.co.ominsure.synapse.iwyze.lob.backend.policy.vo.SearchPolicyRequest;
import za.co.ominsure.synapse.iwyze.lob.backend.policy.vo.SearchPolicyResponse;
import za.co.ominsure.synapse.iwyze.lob.backend.policy.vo.SearchTokenPolicy;
import za.co.ominsure.synapse.iwyze.lob.backend.policy.vo.SvPolicyAtomic;
import za.co.ominsure.synapse.iwyze.lob.backend.servicesupplier.vo.GetServiceSupplierRequest;
import za.co.ominsure.synapse.iwyze.lob.backend.servicesupplier.vo.GetServiceSupplierResponse;
import za.co.ominsure.synapse.iwyze.lob.backend.servicesupplier.vo.PtServiceSupplier;
import za.co.ominsure.synapse.iwyze.lob.backend.servicesupplier.vo.SvServiceSupplier;
import za.co.ominsure.synapse.iwyze.lob.backend.vo.AddClaimsLogRequest;
import za.co.ominsure.synapse.iwyze.lob.backend.vo.AddClaimsLogResponse;
import za.co.ominsure.synapse.iwyze.lob.backend.vo.ClaCaseResponse;
import za.co.ominsure.synapse.iwyze.lob.backend.vo.GetClaimsLogResponse;
import za.co.ominsure.synapse.iwyze.lob.backend.vo.GetPolicyNumberResponse;
import za.co.ominsure.synapse.iwyze.lob.backend.vo.ServiceConfiguration;
import za.co.ominsure.synapse.iwyze.lob.backend.vo.SiteNameEnum;

@Stateless
@Interceptors({ DebugInterceptor.class })
public class IwyzeServiceProxyBean implements IwyzeProxy {

	private static final String CLAIM_MESSAGE = "getClaim call fail:";
	private static final String EMPTY_STRING = "";
	private static final String SEARCHPARTY_MESSAGE = "searchParty call fail:";
	private static final String SEARCH_CASE_ITEM_MESSAGE = "searchCaseItem call fail:";
	private static final String GET_POLICY_NUMBER_SERVICE_MESSAGE = "getPolicyNumberService Fail:";
	private static final String GET_SERVICE_SUPPLIER_SERVICE_MESSAGE = "getServiceSupplierDetails method fail:";
	private static final String SUB_CLAIM_MESSAGE = "searchSubClaim call fail:";
	private static final String ADD_CLAIMS_LOG_FAIL = "AddClaimsLog call fail:";
	private static final String GET_CLAIMS_LOG_FAIL = "getClaimsLog call fail:";
	private static final String POLICY_MESSAGE = "getPolicyInformation call fail:";
	private static final String THIRDPARTY_MESSAGE = "getThirdPartyInfo call fail:";
	private static final String THIRDPARTY_SOAP_MESSAGE = "opGetParty soap operation fail:";
	private static final String SEARCHPOLICY_MESSAGE = "searchPolicy call fail:";
	private static final String SEARCHSUBCLAIM_MESSAGE = "searchSubClaim call fail:";
	private static final String GET_CLAIM_PAYMENTS_MESSAGE = "getClaimPayments call fail:";
	private static final String UNKNOWN_ERROR = "Unknown Error";
	private static final String ADD_CLAIMS_LOG_REST_URI_FRAGMENT = "ADD_CLAIMS_LOG_URI_FRAGMENT";
	private static final String GET_CLAIMS_LOG_REST_URI_FRAGMENT = "GET_CLAIMS_LOG_URI_FRAGMENT";
	private static final String GET_POLICY_NUMBER_REST_URI_FRAGMENT = "GET_POLICY_NUMBER_REST_URI_FRAGMENT";
	private static final String SERVICE_SUPPLIER_ROUTER_SERVICE = "SERVICE_SUPPLIER_ROUTER_SERVICE";
	private static final String PATH = "path=";
	private static final String UTF8 = "UTF-8";
	private static final String IWYZE_SOURCE_PLATFORM = "IWYZE";
	private static final String API_KEY = "API_KEY";
	private static final String PARTY_PROXY_MESSAGE = "getPartyAutomicRouterServiceProxy method fail:";
	private static final String CASE_ITEM_PROXY_MESSAGE = "getCaseRouterSerciceProxy method fail:";
	private static final String CLAIM_INTERACTIVE_PROXY_MESSAGE = "getClaimInteractiveRouterServiceProxy method fail:";
	private static final String POLICY_PROXY_MESSAGE = "getPolicyAutomicRouterServiceProxy method fail:";
	private static final String CLAIM_PROXY_MESSAGE = "getClaimAutomicRouterServiceProxy method fail:";
	private static final String CLAIM_REQUEST_URI = "/synapse/iwyze/lob/rest/claim/";
	private static final String CASE_ITEM_SEARCH_URI = "/synapse/iwyze/lob/rest/caseitem/";
	private static final String UNKNOWN_SEQUENCE_NUMBER_ERROR = "Unknown sequence number received:";
	private static final String GET_POLICY_NUMBER_REST_URI = "/synapse/iwyze/lob/rest/policy/status/detail/";
	private static final String SEARCH_SUB_CLAIM_REQUEST_URI = "/synapse/iwyze/lob/rest/claim/subclaim/";
	private static final String GET_PARTY_URI = "/synapse/iwyze/lob/rest/claim/";
	private static final String SEARCH_SUB_CLAIM_URI = "/synapse/iwyze/lob/rest/claim/subclaim/";
	private static final String SEARCH_PARTY_URI = "/synapse/iwyze/lob/rest/party/";
	private static final String TOKEN_PAYLOAD_JSON = "iwyze soap token payload in json:";
	private static final String CLAIM_AUTOMIC_SERVICE_NAMESPACE = "http://infrastructure.tia.dk/contract/claim/v2/";
	private static final String CLAIM_PAYMENT_SERVICE_NAMESPACE = "http://infrastructure.tia.dk/contract/claim/v2/";
	private static final String CLAIM_INTERACTIVE_SERVICE_NAMESPACE = "http://infrastructure.tia.dk/contract/claim/v2/";
	private static final String SERVICE_SUPPLIER_ROUTER_SERVICE_NAMESPACE = "http://infrastructure.tia.dk/contract/party/v2/";
	private static final String SERVICE_SUPPLIER_PROXY_MESSAGE = "serviceSupplierRouterServiceProxy method fail:";

	private static final String SHARED_BASE64_CRED = "SHARED_BASE64_CRED";
	private static final String INTERNAL_BASE64_CRED = "INTERNAL_BASE64_CRED";
	private static final String AFFINITY_BASE64_CRED = "AFFINITY_BASE64_CRED";
	private static final String IWYZE_BASE64_CRED = "IWYZE_BASE64_CRED";
	private static final String SUREWISE_BASE64_CRED = "SUREWISE_BASE64_CRED";
	private static final String DA_BASE64_CRED = "DA_BASE64_CRED";
	private static final String FNB_BASE64_CRED = "FNB_BASE64_CRED";
	private static final String NEDBANK_BASE64_CRED = "NEDBANK_BASE64_CRED";
	private static final String TFG_BASE64_CRED = "TFG_BASE64_CRED";
	private static final String MOTORSURE_BASE64_CRED = "MOTORSURE_BASE64_CRED";
	private static final String OLDMUTUAL_BASE64_CRED = "OLDMUTUAL_BASE64_CRED";
	
	private static final String PINEAPPLE_BASE64_CRED = "PINEAPPLE_BASE64_CRED";
	private static final String IEMAS_BASE64_CRED = "IEMAS_BASE64_CRED";
	
	private static final String IWYZE_API_KEY = "IWYZE_API_KEY";
	private static final String SUREWISE_API_KEY = "SUREWISE_API_KEY";
	private static final String DA_API_KEY = "DA_API_KEY";
	private static final String FNB_API_KEY = "FNB_API_KEY";
	private static final String NEDBANK_API_KEY = "NEDBANK_API_KEY";
	private static final String MOTORSURE_API_KEY = "MOTORSURE_API_KEY";
	private static final String OLDMUTUAL_API_KEY = "OLDMUTUAL_API_KEY";
	
	private static final String IEMAS_API_KEY = "IEMAS_API_KEY";
	private static final String PINEAPPLE_API_KEY = "PINEAPPLE_API_KEY";
	
	

	private ServiceBridge serviceBridge = new ServiceBridge();

	@Resource(name = "jdbc/rmmDS", mappedName = "jdbc/rmmDS")
	private DataSource dataSource;

	@Override
	public SearchResult getDocumentsForPolicy(SearchRequest searchRequest) throws HttpException {
		return new SearchResult();
	}

	@Override
	public Claim getClaim(String claimEventNumber, String siteName, int offset, int pageSize) throws HttpException {
		Claim claim = null;
		GetClaimRequest request = new GetClaimRequest();
		PtClaim client = null;
		GetClaimResponse claimResponse = null;
		try {
			client = getClaimAutomicRouterServiceProxy();
			// building request to iwyze
			if (claimEventNumber != null)
				request.setClaimEventNo(Long.parseLong(claimEventNumber));
			request.setInputToken(getIwyzeClaimRouterServiceToken(siteName));

			if (client != null)
				claimResponse = client.opGetClaim(request);
			if (claimResponse != null && claimResponse.getResult() != null && claimResponse.getResult().getResultCode() != null && claimResponse.getResult().getResultCode() == 0) {
				claim = getGenericClaim(claimResponse, request);
				if (request.getInputToken() != null && !StringUtils.isBlank(request.getInputToken().getSiteName())) {
					claim.setBrokerInformation(request.getInputToken().getSiteName());
				}
				else {
					Synlog.info("broker name is empty or null");
				}
			}
			else if (claimResponse != null && claimResponse.getResult() != null && claimResponse.getResult().getResultCode() != null && claimResponse.getResult().getResultCode() != 0) {
				Object object = claimResponse;
				iwyzeErrorMessages(object);
			}

			// transforming specific response into generic response
			Synlog.info("getGenericClaim end");
		}
		catch (HttpException e) {
			Synlog.error(CLAIM_MESSAGE + e.getMessage());
			throw new HttpException(CLAIM_MESSAGE + e.getMessage(), 404);
		}
		catch (Exception e) {
			Synlog.error(CLAIM_MESSAGE + e.getMessage());
			throw new HttpException(CLAIM_MESSAGE + e.getMessage(), 500);
		}
		return claim;
	}

	@Override
	public PolicyOld getpolicyInformation(Long policyNumber, String caseNumber, String effectiveDate, Long lossRatioYears, String objectSequenceNumber) throws HttpException {
		Synlog.info("getpolicyInformation call started");
		Genson genson = new GensonBuilder().withBundle(new JAXBBundle()).setSkipNull(true).create();
		PolicyOld policyOld = null;
		GetPolicyRequest request = new GetPolicyRequest();
		PtPolicyAtomic client = null;
		GetPolicyResponse policyResponse = null;
		try {
			client = getPolicyAutomicRouterServiceProxy();
			// building request to iwyze
			request.setPolicyNo(BigInteger.valueOf(policyNumber));
			request.setInputToken(getIwyzePolicyRouterServiceToken(caseNumber));
			if (Synlog.isInfoEnabled()) {
				Synlog.info("GetPolicyRequest iwyze payload in json:", genson.serialize(request));
			}
			if (client != null)
				policyResponse = client.opGetPolicy(request);
			// transforming specific response into generic response
			Synlog.info("getGenericPolicy start");
			if (policyResponse != null && policyResponse.getResult() != null && policyResponse.getResult().getResultCode() != null && policyResponse.getResult().getResultCode() == 0)
				policyOld = getGenericPolicy(policyResponse, objectSequenceNumber);
			else if (policyResponse != null && policyResponse.getResult() != null && policyResponse.getResult().getResultCode() != null && policyResponse.getResult().getResultCode() != 0) {
				Object object = policyResponse;
				iwyzeErrorMessages(object);
			}
			Synlog.info("getGenericPolicy end");
		}
		catch (HttpException e) {
			Synlog.error(POLICY_MESSAGE + e.getMessage());
			throw new HttpException(POLICY_MESSAGE + e.getMessage(), 400);
		}
		catch (Exception e) {
			Synlog.error(POLICY_MESSAGE + e.getMessage());
			throw new HttpException(POLICY_MESSAGE + e.getMessage(), 500);
		}
		return policyOld;
	}

	@Override
	public ThirdParties getThirdPartyInfo(String nameIdNumber, String siteName, int offset, int pageSize) throws HttpException {
		Genson genson = new GensonBuilder().withBundle(new JAXBBundle()).setSkipNull(true).create();
		PtParty client = null;
		ThirdParties thirdParties = null;
		GetPartyRequest request = new GetPartyRequest();
		GetPartyResponse partyResponse = null;

		try {
			// calling opGetParty operaion
			client = getPartyAutomicRouterServiceProxy();
			request.setIdNo(Long.parseLong(nameIdNumber));
			request.setInputToken(getIwyzePartyRouterServiceToken(siteName));
			if(Synlog.isInfoEnabled()) {
			Synlog.info("GetPartyRequest iwyze payload in json:", genson.serialize(request));
			}
			if (client != null) {
				partyResponse = client.opGetParty(request);
			}
			// transforming specific response into generic response

			if (partyResponse != null && partyResponse.getResult() != null && partyResponse.getResult().getResultCode() != null && partyResponse.getResult().getResultCode() == 0)
				thirdParties = getGenericParty(partyResponse);
			else if (partyResponse != null && partyResponse.getResult() != null && partyResponse.getResult().getResultCode() != null && partyResponse.getResult().getResultCode() != 0) {
				Object object = partyResponse;
				iwyzeErrorMessages(object);
			}

		}
		catch (HttpException e) {
			Synlog.error(THIRDPARTY_MESSAGE + e.getMessage());
			throw new HttpException(THIRDPARTY_MESSAGE + e.getMessage(), 400);
		}
		catch (Exception e) {
			Synlog.error(THIRDPARTY_MESSAGE + e.getMessage());
			throw new HttpException(THIRDPARTY_MESSAGE + e.getMessage(), 500);
		}
		return thirdParties;
	}

	@Override
	public PolicyOld searchPolicy(String nameIdNumber, String caseNumber) throws HttpException {
		Synlog.info("searchPolicy call started");
		Genson genson = new GensonBuilder().withBundle(new JAXBBundle()).setSkipNull(true).create();
		PolicyOld policyOld = null;
		PtPolicyAtomic client = null;
		SearchPolicyRequest request = new SearchPolicyRequest();
		SearchPolicyResponse searchPolicyResponse = null;
		SearchTokenPolicy searchToken = new SearchTokenPolicy();

		try {
			client = getPolicyAutomicRouterServiceProxy();
			if (client == null)
				Synlog.info("client is null");
			// building request to iwyze
			request.setInputToken(getIwyzePolicyRouterServiceToken(caseNumber));
			searchToken.setPolicyHolderId(BigInteger.valueOf(Long.parseLong(nameIdNumber)));
			request.setSearchToken(searchToken);
			if(Synlog.isInfoEnabled()) {
			Synlog.info("SearchPolicyRequest iwyze payload in json:", genson.serialize(request));
			}
			if (client != null) {
				searchPolicyResponse = client.opSearchPolicy(request);
			}

			// transforming specific response into generic response
			Synlog.info("searchGenericPolicy start");
			if (searchPolicyResponse != null && searchPolicyResponse.getResult() != null && searchPolicyResponse.getResult().getResultCode() != null && searchPolicyResponse.getResult().getResultCode() == 0)
				policyOld = searchGenericPolicy(searchPolicyResponse);
			else if (searchPolicyResponse != null && searchPolicyResponse.getResult() != null && searchPolicyResponse.getResult().getResultCode() != null && searchPolicyResponse.getResult().getResultCode() != 0) {
				Object object = searchPolicyResponse;
				iwyzeErrorMessages(object);
			}
			Synlog.info("searchGenericPolicy end");
		}
		catch (HttpException e) {
			Synlog.error(SEARCHPOLICY_MESSAGE + e.getMessage());
			throw new HttpException(SEARCHPOLICY_MESSAGE + e.getMessage(), 400);
		}
		catch (Exception e) {
			Synlog.error(SEARCHPOLICY_MESSAGE + e.getMessage());
			throw new HttpException(SEARCHPOLICY_MESSAGE + e.getMessage(), 500);
		}
		return policyOld;
	}

	@Override
	public ThirdParties searchSubClaim(String claimNumber, String siteName) throws HttpException {
		Synlog.info("searchSubClaim call started");
		Genson genson = new GensonBuilder().withBundle(new JAXBBundle()).setSkipNull(true).create();
		PtClaimInteractive client = null;
		ThirdParties thirdParties = null;
		SearchSubclaimRequest request = new SearchSubclaimRequest();
		SearchSubclaimResponse searchSubclaimResponse = null;
		SearchTokenSubclaim searchToken = new SearchTokenSubclaim();
		try {
			// calling opSearchSubclaim operaion
			client = getClaimInteractiveRouterServiceProxy();
			request.setInputToken(getIwyzInteractiveClaimRouterServiceToken(siteName));
			searchToken.setClaimNo(Long.parseLong(claimNumber));
			request.setSearchToken(searchToken);
			if(Synlog.isInfoEnabled()) {
			Synlog.info("SearchSubclaimRequest iwyze payload in json:", genson.serialize(request));
			}
			if (client != null) {
				searchSubclaimResponse = client.opSearchSubclaim(request);
			}
			// transforming specific response into generic response

			if (searchSubclaimResponse != null && searchSubclaimResponse.getResult() != null && searchSubclaimResponse.getResult().getResultCode() != null && searchSubclaimResponse.getResult().getResultCode() == 0)
				thirdParties = searchGenericSubclaim(searchSubclaimResponse);
			else if (searchSubclaimResponse != null && searchSubclaimResponse.getResult() != null && searchSubclaimResponse.getResult().getResultCode() != null && searchSubclaimResponse.getResult().getResultCode() != 0) {
				Object object = searchSubclaimResponse;
				iwyzeErrorMessages(object);
			}
		}
		catch (HttpException e) {
			Synlog.error(SEARCHSUBCLAIM_MESSAGE + e.getMessage());
			throw new HttpException(SEARCHSUBCLAIM_MESSAGE + e.getMessage(), 400);
		}
		catch (Exception e) {
			Synlog.error(SEARCHSUBCLAIM_MESSAGE + e.getMessage());
			throw new HttpException(SEARCHSUBCLAIM_MESSAGE + e.getMessage(), 500);
		}
		return thirdParties;
	}

	@Override
	public Claim searchParty(String idNumber, String siteName) throws HttpException {
		Synlog.info("searchParty call started");
		SearchPartyRequest request = new SearchPartyRequest();
		Synlog.info("searchParty idNumber=" + idNumber);
		Synlog.info("searchParty siteName=" + siteName);
		Claim claim = null;
		PtParty client = null;
		SearchPartyResponse searchPartyResponse = null;
		SearchTokenPar searchToken = new SearchTokenPar();
		try {
			client = getPartyAutomicRouterServiceProxy();
			// building request to iwyze
			request.setInputToken(getIwyzePartyRouterServiceToken(siteName));
			searchToken.setCivilReg(idNumber);
			request.setSearchToken(searchToken);
			if (client != null) {
				searchPartyResponse = client.opSearchParty(request);
			}
			if (searchPartyResponse != null && searchPartyResponse.getResult() != null) {
				Synlog.info("result code=" + searchPartyResponse.getResult().getResultCode());
			}
			if (searchPartyResponse != null && searchPartyResponse.getResult() != null && searchPartyResponse.getResult().getResultCode() != null && searchPartyResponse.getResult().getResultCode() == 0)
				claim = searchGenericParty(searchPartyResponse);
			else if (searchPartyResponse != null && searchPartyResponse.getResult() != null && searchPartyResponse.getResult().getResultCode() != null && searchPartyResponse.getResult().getResultCode() != 0) {
				Object object = searchPartyResponse;
				iwyzeErrorMessages(object);
			}
			Synlog.info("searchParty end");
		}
		catch (HttpException e) {
			Synlog.error(SEARCHPARTY_MESSAGE + e.getMessage());
			throw new HttpException(SEARCHPARTY_MESSAGE + e.getMessage(), 400);
		}
		catch (Exception e) {
			Synlog.error(SEARCHPARTY_MESSAGE + e.getMessage());
			throw new HttpException(SEARCHPARTY_MESSAGE + e.getMessage(), 500);
		}
		return claim;
	}

	@Override
	public Payments getPayments(String iwyzeClaimNumber, int offset, int pagesize) throws HttpException, SQLException {
		Payments payments = new Payments();
		Genson genson = new GensonBuilder().withBundle(new JAXBBundle()).setSkipNull(true).create();
		GetClaimPaymentsResponse getClaimPaymentsResponse = null;
		ThirdParties thirdParties = null;
		String caseNumber = null;
		Claim claim = null;
		List<Case> caseList = null;
		List<ClaimAccItem> claimAccItemList = null;
		List<ClaimPaymentItem> claimPaymentItemList = null;
		int count = 0;
		int addedCount = 0;
		ClaCaseResponse response = null;
		SiteNameEnum siteName = null;
		// call to get site name
		response = getSequenceNumberServiceByIwyzeClaimNumber(iwyzeClaimNumber);
		try {
			if (response != null && !StringUtils.isBlank(response.getSequenceNumber())) {
				siteName = SiteNameEnum.fromValue(response.getSequenceNumber());
				Synlog.info(" SiteNameEnum in getPayments method is=" + siteName);
			}
		}
		catch (IllegalArgumentException ex) {
			throw new HttpException(ex.getMessage() + " - Invalid sequenceNumber  received. hence event did not exists sequenceNumber= " + response.getSequenceNumber(), Response.Status.BAD_REQUEST.getStatusCode());
		}
		// 1.calling getClaim service
		if (siteName != null && response != null && !StringUtils.isBlank(response.getEventNumber())) {
			claim = getClaimDetails(response.getEventNumber(), siteName.toString());
		}
		if (claim != null)
			caseList = claim.getCases().getClaimCase();
		if (caseList != null && !caseList.isEmpty() && caseList.get(0).getCaseNumber() != null) {
			caseNumber = caseList.get(0).getCaseNumber().toString();
			getClaimPaymentsResponse = getClaimPayments(Long.parseLong(caseList.get(0).getCaseNumber().toString()), siteName.toString());

		}
		// searchSubClaim
		if (!StringUtils.isBlank(caseNumber)) {
			thirdParties = searchSubClaimDetail(caseNumber, siteName.toString());
		}
		if (getClaimPaymentsResponse != null && getClaimPaymentsResponse.getResult() != null && getClaimPaymentsResponse.getResult().getResultCode() != null && getClaimPaymentsResponse.getResult().getResultCode() == 0) {
			Synlog.info("getClaimPaymentsResponse result code is 0");
			claimAccItemList = getClaimPaymentsResponse.getClaimAccItemCollection().getClaimAccItems();
			for (ClaimAccItem accItem : claimAccItemList) {
				if (count >= offset) {
					if (addedCount >= pagesize)
						break;
					claimPaymentItemList = accItem.getClaimPaymentItemCollection().getClaimPaymentItems();
					for (ClaimPaymentItem item : claimPaymentItemList) {
						Payment payment = new Payment();
						if (caseList != null && !caseList.isEmpty() && caseList.get(0).getCaseNumber() != null) {
							payment.setCaseNumber(caseList.get(0).getCaseNumber().toString());
						}
						if (accItem.getReceiverIdNo() != null && siteName != null)
							paymentCommonFieldSetter(claim, accItem, payment, thirdParties, siteName.name());
						else
							Synlog.info("claItemSeqNo is null");
						payment.setAmount(item.getAmount());
						if (item.getTaxPct() != null) {
							payment.setTaxPct(item.getTaxPct().toString());
						}
						payment.setPaymentType(item.getPaymentType());
						payment.setAmountAfterExcess(item.getAmount());
						payment.setTransactionAmount(item.getAmount());
						payment.setTimestamp(item.getVersionToken().getTimestamp());
						payment.setPaymentDescription(item.getDescription());
						if (item.getVersionToken() != null) {
							payment.setUserId(item.getVersionToken().getUserId());
						}
						if (item.getTaxPct() != null) {
							payment.setTaxPct(item.getTaxPct().toString());
						}
						payments.getPaymentList().add(payment);
					}
					addedCount++;
				}
				count++;
			}
		}
		// setting fields of payments from claim
		if(Synlog.isInfoEnabled()) {
		Synlog.info("getClaim response payload is:", genson.serialize(claim));
		}
		Synlog.info("getPaymentList size:" + payments.getPaymentList().size());
		return payments;
	}

	private void paymentCommonFieldSetter(Claim claim, ClaimAccItem accItem, Payment payment, ThirdParties thirdParties, String sitename) throws HttpException {
		Genson genson = new GensonBuilder().withBundle(new JAXBBundle()).setSkipNull(true).create();
		PtParty client = null;
		GetPartyRequest request = new GetPartyRequest();
		GetPartyResponse partyResponse = null;
		payment.setClaimNumber(claim.getClaimNumber());
		payment.setRiskNumber(claim.getRiskNumber());
		payment.setSubCaseNumber(claim.getSubcaseNumber());
		payment.setClaimItemNumber(claim.getItemNumber());
		payment.setCategoryCodeAndDescription(accItem.getPaymentMethod());
		payment.setRiskDescription(claim.getEventType());
		payment.setRiskNumber(claim.getRiskNumber());
		payment.setSectionCode(claim.getIsMotor());
		payment.setSubCaseNumber(claim.getSubcaseNumber());
		payment.setDueDateTime(accItem.getDueDate());
		payment.setHandler(claim.getHandler());
		if (accItem.getReceiverIdNo() != null) {
			Synlog.info("each ReceiverIdNo of accItem=" + accItem.getReceiverIdNo());
			GetServiceSupplierResponse getServiceSupplierResponse = null;
			getServiceSupplierResponse = getServiceSupplierDetails(accItem.getReceiverIdNo().toString(), sitename);
			if (getServiceSupplierResponse != null && getServiceSupplierResponse.getServiceSupplier() != null && getServiceSupplierResponse.getServiceSupplier().getInstitution() != null && !getServiceSupplierResponse.getServiceSupplier().getInstitution().isNil()) {
				payment.setPayee(getServiceSupplierResponse.getServiceSupplier().getInstitution().getValue().getName().getValue());
				Synlog.info("payee name is:" + getServiceSupplierResponse.getServiceSupplier().getInstitution().getValue().getName().getValue());
				if (getServiceSupplierResponse.getServiceSupplier().getInstitution().getValue().getPartyRoleCollection() != null && getServiceSupplierResponse.getServiceSupplier().getInstitution().getValue().getPartyRoleCollection().getServiceSupplier() != null
						&& !getServiceSupplierResponse.getServiceSupplier().getInstitution().getValue().getPartyRoleCollection().getServiceSupplier().isEmpty()) {
					payment.setDescription(getServiceSupplierResponse.getServiceSupplier().getInstitution().getValue().getPartyRoleCollection().getServiceSupplier().get(0).getRoleDescription().getValue());
					Synlog.info("role desecription is:" + getServiceSupplierResponse.getServiceSupplier().getInstitution().getValue().getPartyRoleCollection().getServiceSupplier().get(0).getRoleDescription().getValue());
				}
			}
			else if (getServiceSupplierResponse != null && getServiceSupplierResponse.getServiceSupplier() != null && getServiceSupplierResponse.getServiceSupplier().getPerson() != null && !getServiceSupplierResponse.getServiceSupplier().getPerson().isNil()) {
				payment.setPayee(getServiceSupplierResponse.getServiceSupplier().getPerson().getValue().getName().getValue());
				payment.setDescription(getServiceSupplierResponse.getServiceSupplier().getPerson().getValue().getName().getValue());
			}
			else if (getServiceSupplierResponse != null && getServiceSupplierResponse.getResult().getResultCode() != 0) {

				try {
					// calling opGetParty operaion
					client = getPartyAutomicRouterServiceProxy();
					request.setIdNo(accItem.getReceiverIdNo());
					request.setInputToken(getIwyzePartyRouterServiceToken(sitename));
					if(Synlog.isInfoEnabled()) {
					Synlog.info("GetPartyRequest iwyze payload in json:", genson.serialize(request));
					}
					if (client != null) {
						partyResponse = client.opGetParty(request);
					}
					Synlog.info("partyResponse in paymentCommonFieldSetter private method:" + partyResponse);
					// transforming specific response into generic response

					if (partyResponse != null && partyResponse.getResult() != null && partyResponse.getResult().getResultCode() != null && partyResponse.getResult().getResultCode() == 0) {
						if (partyResponse.getParty() != null && partyResponse.getParty().getInstitution() != null && !partyResponse.getParty().getInstitution().isNil() && partyResponse.getParty().getInstitution().getValue().getSurname() != null
								&& !partyResponse.getParty().getInstitution().getValue().getSurname().isNil()) {
							Synlog.info("instruction is not null:" + partyResponse.getParty().getInstitution().getValue().getSurname().getValue());
							payment.setPayee(partyResponse.getParty().getInstitution().getValue().getSurname().getValue());
							payment.setDescription(partyResponse.getParty().getInstitution().getValue().getSurname().getValue());
						}
						else if (partyResponse.getParty() != null && partyResponse.getParty().getPerson() != null && partyResponse.getParty().getPerson().getValue().getName() != null && !partyResponse.getParty().getPerson().getValue().getName().isNil()) {
							Synlog.info("party object is not null:" + partyResponse.getParty().getPerson().getValue().getName().getValue());
							payment.setPayee(partyResponse.getParty().getPerson().getValue().getName().getValue());
							payment.setDescription(partyResponse.getParty().getPerson().getValue().getName().getValue());
						}
					}
					else if (partyResponse != null && partyResponse.getResult() != null && partyResponse.getResult().getResultCode() != null && partyResponse.getResult().getResultCode() != 0) {
						Object object = partyResponse;
						iwyzeErrorMessages(object);
					}
				}
				catch (HttpException e) {
					Synlog.error(THIRDPARTY_SOAP_MESSAGE + e.getMessage());
					throw new HttpException(THIRDPARTY_SOAP_MESSAGE + e.getMessage(), 400);
				}
				catch (Exception e) {
					Synlog.error(THIRDPARTY_SOAP_MESSAGE + e.getMessage());
					throw new HttpException(THIRDPARTY_SOAP_MESSAGE + e.getMessage(), 500);
				}
			}
		}
		payment.setInvoiceNumber(accItem.getItemReference());
		payment.setItemReference(accItem.getItemReference());
		if (accItem.getPaymentStatus() != null) {
			payment.setPaymentStatus(accItem.getPaymentStatus().toString());
		}
		if (thirdParties != null && thirdParties.getExcessAmount() != null) {
			payment.setExcessAmount(thirdParties.getExcessAmount().doubleValue());
		}
		if (thirdParties != null) {
			payment.setQuestionStatus(thirdParties.getQuestionStatus());
		}
	}

	private GetClaimPaymentsResponse getClaimPayments(Long caseNumber, String siteName) throws HttpException {
		GetClaimPaymentsRequest request = new GetClaimPaymentsRequest();
		PtClaimPayment client = null;
		GetClaimPaymentsResponse getClaimPaymentsResponse = null;
		try {
			client = getClaimPaymentRouterServiceProxy();
			// building request to iwyze
			if (caseNumber != null)
				request.setInputToken(getIwyzeClaimPaymentRouterServiceToken(siteName));
			request.setClaimNo(caseNumber);
			if (client != null) {
				getClaimPaymentsResponse = client.opGetClaimPayments(request);
			}
			if (getClaimPaymentsResponse != null && getClaimPaymentsResponse.getResult() != null && getClaimPaymentsResponse.getResult().getResultCode() != null && getClaimPaymentsResponse.getResult().getResultCode() != 0) {
				Object object = getClaimPaymentsResponse;
				iwyzeErrorMessages(object);
			}
		}
		catch (HttpException e) {
			Synlog.error(GET_CLAIM_PAYMENTS_MESSAGE + e.getMessage());
			throw new HttpException(GET_CLAIM_PAYMENTS_MESSAGE + e.getMessage(), 400);
		}
		catch (Exception e) {
			Synlog.error(GET_CLAIM_PAYMENTS_MESSAGE + e.getMessage());
			throw new HttpException(GET_CLAIM_PAYMENTS_MESSAGE + e.getMessage(), 500);
		}
		return getClaimPaymentsResponse;
	}

	@Override
	public Audits getAudits(String claimEventNumber, int offset, int pageSize) throws HttpException {
		GetClaimsLogResponse response = null;
		response = getClaimsLogService(claimEventNumber);
		Audits audits = new Audits();
		Audit audit = null;
		int count = 0;
		int addedCount = 0;

		List<GetClaimsLogResponse.Content.GetClmLg.ClaEventLogArray> claimDetailList = response.getContent().getGetClmLg().getClaEventLogArray();
		for (GetClaimsLogResponse.Content.GetClmLg.ClaEventLogArray ce : claimDetailList) {
			if (count >= offset) {
				if (addedCount >= pageSize)
					break;
				audit = new Audit();
				audit.setClaimNumber(claimEventNumber);
				if (ce != null && ce.getLogType() != null) {
					audit.setAuditType(ce.getLogType().toString());
					audit.setLog(ce.getLogType().toString());
					audit.setAuditDate(ce.getTimestamp());
				}
				if (ce != null) {
					audit.setAuditUser(ce.getUserId());
					audit.setMessage(ce.getDescription());
					audit.setAuditDetails(ce.getDescription());
					audit.setUser(ce.getUserId());
				}
				audits.getAudit().add(audit);
				addedCount++;
			}
			count++;
		}
		audits.setCount(audits.getAudit().size());
		return audits;
	}

	@Override
	public void addAudit(Audits audits) throws HttpException {
		AddClaimsLogRequest addClaimsLogRequest = new AddClaimsLogRequest();
		// building request to iwyze
		List<Audit> auditList = audits.getAudit();
		for (Audit audit : auditList) {
			if (!StringUtils.isBlank(audit.getCaseNumber())) {
				addClaimsLogRequest.setEventNo(audit.getCaseNumber());
			}
			if (!StringUtils.isBlank(audit.getClaimNumber())) {
				addClaimsLogRequest.setClaimNo(audit.getClaimNumber());
			}
			addClaimsLogRequest.setDescription(audit.getMessage());
			addClaimsLogRequest.getMetadata().setUserId(audit.getUser());
			addClaimsLogService(addClaimsLogRequest);
		}
	}

	@Override
	public CreateRequest getAJSData(String claimNumber) throws HttpException, SQLException {
		CreateRequest request = new CreateRequest();
		Claim claim = null;
		Claim searchParty = null;
		ThirdParties getPartyResponse = null;
		ThirdParties searchSubClaimResponse = null;
		String caseNumber = null;
		ClaCaseResponse response = null;
		GetPolicyNumberResponse getPolicyNumberResponse = null;
		CaseItemGenericResponse caseItemGenericResponse = null;
		
		SiteNameEnum siteName = null;
		String secret = null;
		// call to get site name
		response = getSequenceNumberServiceByIwyzeClaimNumber(claimNumber);
		try {
			if (response != null) {
				Synlog.info("sequenceNumber before passing it to enum:" + response.getSequenceNumber());
				siteName = SiteNameEnum.fromValue(response.getSequenceNumber());
				Synlog.info(" SiteNameEnum in getAJSData method is=" + siteName);
				if (!StringUtils.isBlank(response.getSequenceNumber())) {
					secret = getSecretBySequenceNumber(response.getSequenceNumber());
				}
			}
		}
		catch (IllegalArgumentException ex) {
			throw new HttpException(ex.getMessage() + " - Invalid sequenceNumber  received. hence claim did not exists: sequenceNumber= " + response.getSequenceNumber(), Response.Status.BAD_REQUEST.getStatusCode());
		}
		if (response != null && !StringUtils.isBlank(response.getEventNumber())) {
			claimNumber = response.getEventNumber();
		}

		// 1.calling getClaim service
		if (siteName != null) {
			claim = getClaimDetails(claimNumber, siteName.toString());
		}
		
		// calling searchCaseType - to get clet type and requestId
		if (siteName != null && response != null && !StringUtils.isBlank(response.getClaimNumber())) {
			Synlog.info("calling searchCaseItem service");
			caseItemGenericResponse = getClegType(response.getClaimNumber(), siteName.toString());
			Synlog.info("cleg type is:"+caseItemGenericResponse.getClegType());
		}
		
		if(caseItemGenericResponse != null) {
			request.setClegType(caseItemGenericResponse.getClegType());
			request.setRequestId(caseItemGenericResponse.getRequestId());
		}
		

		// setting broker name
		if (siteName != null) {
			request.setBrokerName(siteName.toString());
		}
		// 3.calling getParty service
		if (claim != null && !StringUtils.isBlank(claim.getNameIdNumber()) && !claim.getCases().getClaimCase().isEmpty() && claim.getCases().getClaimCase().get(0).getCaseNumber() != null) {
			caseNumber = claim.getCases().getClaimCase().get(0).getCaseNumber().toString();
			Synlog.info("case number:" + caseNumber);
			Synlog.info("before getParty method");
			Synlog.info("name id number :" + claim.getNameIdNumber());
			getPartyResponse = getParty(claim.getNameIdNumber(), siteName.toString());
			Synlog.info("after getParty method");
		}

		// 4.calling searchSubCliam service
		if (claim != null && claim.getCases() != null && !claim.getCases().getClaimCase().isEmpty() && claim.getCases().getClaimCase().get(0).getCaseNumber() != null) {
			caseNumber = claim.getCases().getClaimCase().get(0).getCaseNumber().toString();
			searchSubClaimResponse = searchSubClaimService(caseNumber, siteName.toString());
		}
		// 6.calling searchParty service
		if (getPartyResponse != null && !StringUtils.isBlank(getPartyResponse.getInsuredIdNumber()) && !StringUtils.isBlank(caseNumber)) {
			searchParty = searchPartyService(getPartyResponse.getInsuredIdNumber(), siteName.toString());
		}
		if (claim != null) {
			request.setClaimNumber(caseNumber);// iwyze claim event number required for AJS
			request.setTiaStatus(claim.getStatus());
			request.setIncidentDate(claim.getDateOfLoss());
			request.setLossDescription(claim.getLossDescription());
			if (!StringUtils.isBlank(claim.getNameIdNumber()) && !StringUtils.isBlank(secret)) {
				getPolicyNumberResponse = getPolicyDetailService(claim.getNameIdNumber(), secret);
				if (getPolicyNumberResponse != null && !getPolicyNumberResponse.getContent().getSvcCheckPol().getRsExistingPoilyArray().isEmpty()) {
					List<GetPolicyNumberResponse.Content.SvcCheckPol.RsExistingPoilyArray> policyList = getPolicyNumberResponse.getContent().getSvcCheckPol().getRsExistingPoilyArray();
					for (GetPolicyNumberResponse.Content.SvcCheckPol.RsExistingPoilyArray p : policyList) {
						if (p.getCancelCode() != null && p.getPolicyNo() != null && p.getCancelCode() == 0 && p.getPolicyStatus().equalsIgnoreCase("p")) {
							Synlog.info("policy status is p");
							if (p.getProdId().equalsIgnoreCase("RM") || p.getProdId().equalsIgnoreCase("FN") || p.getProdId().equalsIgnoreCase("NB") || p.getProdId().equalsIgnoreCase("SW") || p.getProdId().equalsIgnoreCase("DA")) {
								request.setPolicyNumber(p.getPolicyNo().toString());// setting active policy number
								Synlog.info("active policy number is :" + p.getPolicyNo().toString());
							}
							else {
								Synlog.info("prod id not in creteria:" + p.getProdId());
							}
						}
					}
				}
				else {
					Synlog.info("condition not satified policy number null or empty");
					request.setPolicyNumber(null);
				}
			}
			request.setCaseNumber(claimNumber);// required for AJS
			request.setSubcaseNumber(claim.getSubcaseNumber());// required for AJS
			request.setIsMotor(claim.getIsMotor());
			request.setDriverName(claim.getDriverName());
			request.setSapCaseNumber(claim.getSapCaseNumber());
			request.setSapOffice(claim.getSapOffice());
			request.setCoverType(claim.getRiskNumber());
		}

		// building generic AJS request
		return buildingAJSRequest(request, searchParty, searchSubClaimResponse, getPartyResponse);
	}

	private InputToken getIwyzeClaimRouterServiceToken(String siteName) throws HttpException {
		InputToken token = new InputToken();
		Genson genson = new GensonBuilder().withBundle(new JAXBBundle()).setSkipNull(true).create();
		ServiceConfiguration config = null;
		config = getServiceConfig();
		token.setCommitOn(config.getCommitOn());
		token.setLanguage(config.getLanguage());
		token.setSiteName(siteName);
		token.setTraceOn(config.getTraceOn());
		token.setUserId(config.getUserId());
		if(Synlog.isInfoEnabled()) {
		Synlog.info(TOKEN_PAYLOAD_JSON, genson.serialize(token));
		}
		return token;
	}

	private za.co.ominsure.synapse.iwyze.lob.backend.policy.vo.InputToken getIwyzePolicyRouterServiceToken(String caseNumber) throws HttpException, SQLException {
		za.co.ominsure.synapse.iwyze.lob.backend.policy.vo.InputToken token = new za.co.ominsure.synapse.iwyze.lob.backend.policy.vo.InputToken();
		Genson genson = new GensonBuilder().withBundle(new JAXBBundle()).setSkipNull(true).create();
		ServiceConfiguration config = null;
		config = getServiceConfig();
		SiteNameEnum siteName = null;
		ClaCaseResponse response = null;
		response = getSequenceNumberServiceByIwyzeClaimNumber(caseNumber);
		try {
			if (response != null) {
				siteName = SiteNameEnum.fromValue(response.getSequenceNumber());
				Synlog.info(" SiteNameEnum  for get policy and search policy calls client=" + siteName);
			}
		}
		catch (IllegalArgumentException ex) {
			throw new HttpException(ex.getMessage() + " - Invalid sequenceNumber  received. sequenceNumber= " + response.getSequenceNumber(), Response.Status.BAD_REQUEST.getStatusCode());
		}
		if (siteName != null) {
			token.setSiteName(siteName.toString());
			Synlog.info("site name from for get policy and search policy calls token:" + token.getSiteName());
		}
		else {
			Synlog.info("site name is null  or empty");
			throw new HttpException("site name should  not be empty:", Response.Status.BAD_REQUEST.getStatusCode());
		}
		token.setCommitOn(config.getCommitOn());
		token.setLanguage(config.getLanguage());
		token.setTraceOn(config.getTraceOn());
		token.setUserId(config.getUserId());
		if(Synlog.isInfoEnabled()) {
		Synlog.info("search policy and get policy information:" + TOKEN_PAYLOAD_JSON, genson.serialize(token));
		}
		return token;
	}

	private za.co.ominsure.synapse.iwyze.lob.backend.party.vo.InputToken getIwyzePartyRouterServiceToken(String siteName) throws HttpException {
		za.co.ominsure.synapse.iwyze.lob.backend.party.vo.InputToken token = new za.co.ominsure.synapse.iwyze.lob.backend.party.vo.InputToken();
		Genson genson = new GensonBuilder().withBundle(new JAXBBundle()).setSkipNull(true).create();
		ServiceConfiguration config = null;
		config = getServiceConfig();
		token.setCommitOn(config.getCommitOn());
		token.setLanguage(config.getLanguage());
		token.setSiteName(siteName);
		token.setTraceOn(config.getTraceOn());
		token.setUserId(config.getUserId());
		if(Synlog.isInfoEnabled()) {
		Synlog.info("get party and search party:" + TOKEN_PAYLOAD_JSON, genson.serialize(token));
		}
		return token;
	}
	
	private za.co.ominsure.synapse.iwyze.lob.backend.caseItem.vo.InputToken getIwyzeCaseRouterServiceToken(String siteName) throws HttpException {
		za.co.ominsure.synapse.iwyze.lob.backend.caseItem.vo.InputToken token = new za.co.ominsure.synapse.iwyze.lob.backend.caseItem.vo.InputToken();
		Genson genson = new GensonBuilder().withBundle(new JAXBBundle()).setSkipNull(true).create();
		ServiceConfiguration config = null;
		config = getServiceConfig();
		token.setCommitOn(config.getCommitOn());
		token.setLanguage(config.getLanguage());
		token.setSiteName(siteName);
		token.setTraceOn(config.getTraceOn());
		token.setUserId(config.getUserId());
		if(Synlog.isInfoEnabled()) {
		Synlog.info("get case item token:" + TOKEN_PAYLOAD_JSON, genson.serialize(token));
		}
		return token;
	}

	private za.co.ominsure.synapse.iwyze.lob.backend.claim.payment.vo.InputToken getIwyzeClaimPaymentRouterServiceToken(String siteName) throws HttpException {
		za.co.ominsure.synapse.iwyze.lob.backend.claim.payment.vo.InputToken token = new za.co.ominsure.synapse.iwyze.lob.backend.claim.payment.vo.InputToken();
		Genson genson = new GensonBuilder().withBundle(new JAXBBundle()).setSkipNull(true).create();
		ServiceConfiguration config = null;
		config = getServiceConfig();
		token.setCommitOn(config.getCommitOn());
		token.setLanguage(config.getLanguage());
		token.setSiteName(siteName);
		token.setTraceOn(config.getTraceOn());
		token.setUserId(config.getUserId());
		if(Synlog.isInfoEnabled()) {
		Synlog.info("getIwyzeClaimPaymentRouterServiceToken: " + TOKEN_PAYLOAD_JSON, genson.serialize(token));
		}
		return token;
	}

	private za.co.ominsure.synapse.iwyze.lob.backend.servicesupplier.vo.InputToken getIwyzeServiceSupplierRouterServiceToken(String siteName) throws HttpException {
		za.co.ominsure.synapse.iwyze.lob.backend.servicesupplier.vo.InputToken token = new za.co.ominsure.synapse.iwyze.lob.backend.servicesupplier.vo.InputToken();
		Genson genson = new GensonBuilder().withBundle(new JAXBBundle()).setSkipNull(true).create();
		ServiceConfiguration config = null;
		config = getServiceConfig();
		token.setCommitOn(config.getCommitOn());
		token.setLanguage(config.getLanguage());
		token.setSiteName(siteName);
		token.setTraceOn(config.getTraceOn());
		token.setUserId(config.getUserId());
		if(Synlog.isInfoEnabled()) {
		Synlog.info("getIwyzeServiceSupplierRouterServiceToken: " + TOKEN_PAYLOAD_JSON, genson.serialize(token));
		}
		return token;
	}

	private za.co.ominsure.synapse.iwyze.lob.backend.interactive.claim.vo.InputToken getIwyzInteractiveClaimRouterServiceToken(String siteName) throws HttpException {
		za.co.ominsure.synapse.iwyze.lob.backend.interactive.claim.vo.InputToken token = new za.co.ominsure.synapse.iwyze.lob.backend.interactive.claim.vo.InputToken();
		Genson genson = new GensonBuilder().withBundle(new JAXBBundle()).setSkipNull(true).create();
		ServiceConfiguration config = null;
		config = getServiceConfig();
		token.setCommitOn(config.getCommitOn());
		token.setLanguage(config.getLanguage());
		token.setSiteName(siteName);
		token.setTraceOn(config.getTraceOn());
		token.setUserId(config.getUserId());
		if(Synlog.isInfoEnabled()) {
		Synlog.info("search sub claim :" + TOKEN_PAYLOAD_JSON, genson.serialize(token));
		}
		return token;
	}

	private Claim getGenericClaim(GetClaimResponse response, GetClaimRequest request) {
		Claim claim = new Claim();
		Genson genson = new GensonBuilder().withBundle(new JAXBBundle()).setSkipNull(true).create();
		claim.setClaimNumber(request.getClaimEventNo().toString());

		if (response != null && response.getClaimEvent() != null) {
			if(Synlog.isInfoEnabled()) {
			Synlog.info("GetClaimResponse iwyze specific payload in json:", genson.serialize(response));
			}
			claim.setEventType(response.getClaimEvent().getEventType());
			List<za.co.ominsure.synapse.iwyze.lob.backend.claim.vo.Claim> iwyzeClaimList = response.getClaimEvent().getClaimCollection().getClaims();
			SubclaimCollection subclaimCollection = null;
			AnswerSetCollection answerSetCollection = null;
			ClaimItemCollection claimItemCollection = null;
			if (iwyzeClaimList != null && !iwyzeClaimList.isEmpty()) {
				for (za.co.ominsure.synapse.iwyze.lob.backend.claim.vo.Claim c : iwyzeClaimList) {
					subclaimCollection = c.getSubclaimCollection();
					answerSetCollection = c.getAnswerSetCollection();
					claimItemCollection = c.getClaimItemCollection();

					claim.setInformerType(c.getInformerType());
					claim.setInformerName(c.getInformerName());
					claim.setInformerContactNumber(c.getInformerContact());
					claim.setStatus(c.getStatus());
					claim.setDeclineReason(c.getDeclineReason());
					claim.setLossDescription(c.getDescription());
					claim.setDateOfLoss(response.getClaimEvent().getIncidentDate());
					if (!StringUtils.isBlank(response.getClaimEvent().getIncidentDate()) && response.getClaimEvent().getIncidentDate().length() > 10) {
						Synlog.info("date of loss in get generic claim after substring 0,10:" + response.getClaimEvent().getIncidentDate().substring(0, 10));
						claim.setDateOfLoss(response.getClaimEvent().getIncidentDate().substring(0, 10));
					}
					if (c.getObjectSeqNo() != null)
						claim.setObjectSequenceNumber(c.getObjectSeqNo());// objectSeqNo
					if (c.getNameIdNo() != null)
						claim.setNameIdNumber(c.getNameIdNo().toString());

					if (c.getRiskNo() != null)
						claim.setRiskNumber(c.getRiskNo().toString());

					if (c.getNameIdNo() != null)
						claim.setIsMotor(c.getPolicyLineNo().toString());

					if (c.getClaimNo() != null) {
						Synlog.info("iwyze claimNo setting to cdm case number:" + c.getClaimNo());
						Case case1 = new Case();
						case1.setCaseNumber(c.getClaimNo());
						claim.getCases().getClaimCase().add(case1);
					}
				}
			}
			List<za.co.ominsure.synapse.iwyze.lob.backend.claim.vo.ClaimItem> claimItemList = null;
			if (claimItemCollection != null)
				claimItemList = claimItemCollection.getClaimItems();
			if (claimItemList != null && !claimItemList.isEmpty()) {
				for (za.co.ominsure.synapse.iwyze.lob.backend.claim.vo.ClaimItem item : claimItemList) {
					if (item != null && item.getItemType() != null && item.getNewest().equalsIgnoreCase("Y") && item.getItemType().equalsIgnoreCase("RE") && item.getSubitemType().equalsIgnoreCase("00008") && item.getCurrencyCode().equalsIgnoreCase("ZAR") && item.getCurrencyEstimate() != null) {
						claim.setLatestEstimate(item.getCurrencyEstimate().doubleValue());// map this to getPayments amount AJS field
						if (item.getItemNo() != null)
							claim.setItemNumber(item.getItemNo().toString());
					}
				}
			}
			List<Subclaim> subcList = null;
			if (subclaimCollection != null)
				subcList = subclaimCollection.getSubclaims();
			if (subcList != null && !subcList.isEmpty()) {
				for (Subclaim subc : subcList) {
					if (subc != null && subc.getSubclaimNo() != null) {
						claim.setSubcaseNumber(subc.getSubclaimNo().toString());
					}
				}
			}

			List<za.co.ominsure.synapse.iwyze.lob.backend.claim.vo.AnswerSet> answerSetList = null;
			List<Answer> answerList = null;
			AnswerCollection answerCollection = null;
			answerSetList = answerSetCollection.getAnswerSets();
			if (answerSetList != null && !answerSetList.isEmpty()) {
				for (za.co.ominsure.synapse.iwyze.lob.backend.claim.vo.AnswerSet answerSet : answerSetList) {
					answerCollection = answerSet.getAnswerCollection();
				}
			}
			if (answerCollection != null) {
				answerList = answerCollection.getAnswers();
			}
			if (answerList != null && !answerList.isEmpty()) {
				for (Answer answer : answerList) {
					if (answer != null && answer.getQuestionCode().equalsIgnoreCase("970")) {
						claim.setDriverName(answer.getAnswerValue());// need to look at later
					}
					if (answer != null && answer.getQuestionCode().equalsIgnoreCase("480")) {
						claim.setSapCaseNumber(answer.getAnswerValue());
					}
					if (answer != null && answer.getQuestionCode().equalsIgnoreCase("450")) {
						claim.setSapOffice(answer.getAnswerValue());// need to look at later
					}
				}
			}
			if(Synlog.isInfoEnabled()) {
			Synlog.info("generic claim payload in json:", genson.serialize(claim));
			}
		}
		return claim;
	}

	private PolicyOld getGenericPolicy(GetPolicyResponse response, String objectSequenceNumber) {
		PolicyOld policyOld = new PolicyOld();
		Genson genson = new GensonBuilder().withBundle(new JAXBBundle()).setSkipNull(true).create();
		BigInteger objectSeqNo = null;
		if (objectSequenceNumber != null)
			objectSeqNo = new BigInteger(objectSequenceNumber);

		if (response != null && response.getPolicy() != null) {

			if (response.getPolicy().getPolicySeqNo() != null)
				policyOld.setPolicySequenceNumber(Long.parseLong(response.getPolicy().getPolicySeqNo().toString()));
			if (response.getPolicy().getPolicyStatus() != null)
				policyOld.setPolicyStatus(response.getPolicy().getPolicyStatus());
			if (response.getPolicy().getCoverStartDate() != null)
				policyOld.setCoverStartDate(response.getPolicy().getCoverStartDate().toString());
			if (response.getPolicy().getCoverEndDate() != null)
				policyOld.setCoverEndDate(response.getPolicy().getCoverEndDate().toString());
			if (response.getPolicy().getCenterCode() != null)
				policyOld.setCenterCode(response.getPolicy().getCenterCode());
			if (response.getPolicy().getRenewalDate() != null)
				policyOld.setRenewalDate(response.getPolicy().getRenewalDate().toString());
			if (response.getPolicy().getHandler() != null)
				policyOld.setHandler(response.getPolicy().getHandler());
			if (response.getPolicy().getPolicyLineCollection() != null && !response.getPolicy().getPolicyLineCollection().getPolicyLines().isEmpty()) {
				if (response.getPolicy().getPolicyLineCollection().getPolicyLines().get(0).getFirstStartDate() != null)
					policyOld.setFirstStartDate(response.getPolicy().getPolicyLineCollection().getPolicyLines().get(0).getFirstStartDate().toString());
				List<PolicyLine> policyLineList = response.getPolicy().getPolicyLineCollection().getPolicyLines();
				for (PolicyLine policyLine : policyLineList) {
					if (policyLine.getPolicyLineObjectCollection() != null && !policyLine.getPolicyLineObjectCollection().getPolicyLineObjects().isEmpty()) {
						List<PolicyLineObject> lineObjectList = policyLine.getPolicyLineObjectCollection().getPolicyLineObjects();
						for (PolicyLineObject object : lineObjectList) {
							if (object != null && object.getSeqNo() != null && object.getSeqNo().equals(objectSeqNo)) {
								Synlog.info("passed object sequence number:" + objectSeqNo);
								Synlog.info("object sequence number from get policy stub call:" + object.getSeqNo());
								Synlog.info("both sequence numbers are equal");
								policyOld.setCoverLimit(object.getSumInsured());// suminsured
							}
						}
					}
				}
			}
			if(Synlog.isInfoEnabled()) {
			Synlog.info("generic policy payload in json:", genson.serialize(policyOld));
			}
		}
		return policyOld;
	}

	private PolicyOld searchGenericPolicy(SearchPolicyResponse response) {
		PolicyOld policyOld = new PolicyOld();
		Genson genson = new GensonBuilder().withBundle(new JAXBBundle()).setSkipNull(true).create();
		if(Synlog.isInfoEnabled()) {
		Synlog.info("SearchPolicyResponse iwyze specific paylaod in json:", genson.serialize(response));
		}

		if (response != null && response.getPolicySearchResultCollection() != null) {
			List<PolicySearchResult> policySearchResult = response.getPolicySearchResultCollection().getPolicySearchResults();
			if (policySearchResult != null && !policySearchResult.isEmpty() && policySearchResult.get(0).getPolicyDetails().getPolicyNo() != null) {
				policyOld.setPolicyNumber(policySearchResult.get(0).getPolicyDetails().getPolicyNo().toString());
			}
		}
		if(Synlog.isInfoEnabled()) {
		Synlog.info("searchGenericPolicy policyOld paylaod in json:", genson.serialize(policyOld));
		}
		return policyOld;
	}

	private ThirdParties searchGenericSubclaim(SearchSubclaimResponse response) {
		ThirdParties thirdParties = new ThirdParties();
		Genson genson = new GensonBuilder().withBundle(new JAXBBundle()).setSkipNull(true).create();
		Synlog.info("entered into method searchGenericSubclaim");
		if (response != null && response.getClaimEventCollection() != null) {
			if(Synlog.isInfoEnabled()) {
			Synlog.info("SearchSubclaimResponse iwyze specific payload in json:", genson.serialize(response));
			}
			Synlog.info("step1");
			List<ClaimEvent> claimEventList = response.getClaimEventCollection().getClaimEvents();
			Synlog.info("claimEventList size=" + claimEventList.size());
			if (claimEventList != null && !claimEventList.isEmpty()) {
				if (!StringUtils.isBlank(claimEventList.get(0).getIncidentDate()) && claimEventList.get(0).getIncidentDate().length() > 10) {
					Synlog.info("incident date of claim after substring 0,10:" + claimEventList.get(0).getIncidentDate().substring(0, 10));
					thirdParties.setIncidentDate(claimEventList.get(0).getIncidentDate().substring(0, 10));
				}
				Synlog.info("incident date of claim:" + claimEventList.get(0).getIncidentDate());
				thirdParties.setCreateDate(claimEventList.get(0).getCreateDate().toString());
				if (claimEventList.get(0).getClaimCollection() != null) {
					List<za.co.ominsure.synapse.iwyze.lob.backend.interactive.claim.vo.Claim> claimList = claimEventList.get(0).getClaimCollection().getClaims();
					for (za.co.ominsure.synapse.iwyze.lob.backend.interactive.claim.vo.Claim c : claimList) {
						thirdParties.setQuestionStatus(c.getQuestionStatus());
						if (c != null && !StringUtils.isBlank(c.getDescription())) {
							thirdParties.setLossDescription(c.getDescription());
						}
						if (c.getClaimItemCollection() != null && !c.getClaimItemCollection().getClaimItems().isEmpty()) {
							List<ClaimItem> itemList = c.getClaimItemCollection().getClaimItems();
							for (ClaimItem item : itemList) {
								if (item.getItemType() != null && item.getItemType().equalsIgnoreCase("RE") && item.getNewest() != null && item.getNewest().equalsIgnoreCase("Y")) {
									thirdParties.setCaseEstimateAmount(item.getCurrencyEstimate());// caseEstimateAmount check with iwyze the subItemType is 00002
								}
								if (item.getItemType() != null && item.getItemType().equalsIgnoreCase("EX") && item.getSubitemType().equalsIgnoreCase("00065") && item.getCurrencyCode().equalsIgnoreCase("ZAR") && item.getNewest() != null && item.getNewest().equalsIgnoreCase("Y")) {
									thirdParties.setExcessAmount(item.getEstimate());// excessAmount -- check with iwyze team why both estimate and excess is same
								}
							}
						}
						if (c != null && c.getThirdPartyCollection() != null && !c.getThirdPartyCollection().getThirdParties().isEmpty()) {
							List<za.co.ominsure.synapse.iwyze.lob.backend.interactive.claim.vo.ThirdParty> thrirdPartyList = c.getThirdPartyCollection().getThirdParties();
							for (za.co.ominsure.synapse.iwyze.lob.backend.interactive.claim.vo.ThirdParty party : thrirdPartyList) {
								ThirdParty thirdParty = new ThirdParty();
								thirdParty.setThirdPartyNumber(party.getThirdPartyNo());
								thirdParty.setThirdPartyType(party.getThirdPartyType());
								List<Flexfield> flexFieldList = null;
								if (party != null) {
									flexFieldList = party.getFlexfieldCollection().getFlexfields();
								}

								for (Flexfield field : flexFieldList) {
									if (field.getVarcharAttribute().getAttributeName() != null && field.getVarcharAttribute().getAttributeName().equalsIgnoreCase("SUPPLIER_ADDRESS")) {
										thirdParty.setThirdPartyAddressLine2(field.getVarcharAttribute().getValue());
										thirdParty.setThirdPartyAddressLine3(field.getVarcharAttribute().getValue());
										thirdParty.setThirdPartyAddressLine4(field.getVarcharAttribute().getValue());
									}

									if (field.getVarcharAttribute().getAttributeName() != null) {
										if (field.getVarcharAttribute().getAttributeName().equalsIgnoreCase("SUPPLIER_TEL_NUMBER")) {
											thirdParty.setThirdPartyContactNumber(field.getVarcharAttribute().getValue());
										}
										if (field.getVarcharAttribute().getAttributeName().equalsIgnoreCase("SUPPLIER_VEHICLE_OWNER")) {
											thirdParty.setThirdPartyDriverName(field.getVarcharAttribute().getValue());
										}
										if (field.getVarcharAttribute().getAttributeName().equalsIgnoreCase("SUPPLIER_ID_NUMBER")) {
											thirdParty.setThirdPartyIdNumber(field.getVarcharAttribute().getValue());
										}
										if (field.getVarcharAttribute().getAttributeName().equalsIgnoreCase("SUPPLIER_INSURANCE_CONTACT_NO")) {
											thirdParty.setThirdPartyInsuranceContactNumber(field.getVarcharAttribute().getValue());
										}
										if (field.getVarcharAttribute().getAttributeName().equalsIgnoreCase("SUPPLIER_INSURANCE")) {
											thirdParty.setThirdPartyInsuranceName(field.getVarcharAttribute().getValue());
										}
										if (field.getVarcharAttribute().getAttributeName().equalsIgnoreCase("SUPPLIER_VEHICLE_OWNER")) {
											thirdParty.setThirdPartyName(field.getVarcharAttribute().getValue());
										}
										if (field.getVarcharAttribute().getAttributeName().equalsIgnoreCase("SUPPLIER_VEHICLE_REG_NO")) {
											thirdParty.setRegistrationNumber(field.getVarcharAttribute().getValue());
										}
									}
								}
								thirdParties.getThirdParty().add(thirdParty);
							}
						}
					}
				}
			}
		}
		if(Synlog.isInfoEnabled()) {
		Synlog.info("searchGenericSubclaim ThirdParties payload in json:", genson.serialize(thirdParties));
		}
		return thirdParties;
	}

	private ThirdParties getGenericParty(GetPartyResponse response) {
		ThirdParties thirdParties = new ThirdParties();
		Genson genson = new GensonBuilder().withBundle(new JAXBBundle()).setSkipNull(true).create();
		Synlog.info("in  getGenericParty");
		if (response != null && response.getParty() != null) {
			if (response.getParty().getPerson() != null && !response.getParty().getPerson().isNil()) {
				Synlog.info("person object exists");
			}
			else
				Synlog.info("person object does not exist");
			List<Address> addressList = null;
			if (response.getParty().getPerson() != null && !response.getParty().getPerson().isNil())
				addressList = response.getParty().getPerson().getValue().getAddressCollection().getAddress();
			if (addressList != null && !addressList.isEmpty()) {
				Synlog.info("addressList not empty");
				for (Address address : addressList) {
					if (address.getAddressType() != null && !address.getAddressType().isNil() && address.getAddressType().getValue().equalsIgnoreCase("address")) {
						if (!address.getStreetNo().isNil())
							thirdParties.setInsuredAddressLine1(address.getStreetNo().getValue());
						Synlog.info("street no:" + address.getStreetNo().getValue());
						if (!address.getStreet().isNil())
							thirdParties.setInsuredAddressLine2(address.getStreet().getValue());
						Synlog.info("street :" + address.getStreet().getValue());
						if (!address.getCity().isNil())
							thirdParties.setInsuredAddressLine3(address.getCity().getValue());
						if (!address.getCountry().isNil())
							thirdParties.setInsuredAddressLine4(address.getCountry().getValue());
					}
				}
			}
			if (response.getParty().getPerson() != null && !response.getParty().getPerson().isNil() && response.getParty().getPerson().getValue().getFlexfieldCollection() != null && !response.getParty().getPerson().getValue().getFlexfieldCollection().isNil()
					&& !response.getParty().getPerson().getValue().getFlexfieldCollection().getValue().getFlexfield().isEmpty()) {
				
				String viaSMS = null;
				String viaEmail = null;
				String viaPhone = null;
				StringBuilder messages = new StringBuilder();

				List<za.co.ominsure.synapse.iwyze.lob.backend.party.vo.Flexfield> fieldList = response.getParty().getPerson().getValue().getFlexfieldCollection().getValue().getFlexfield();
				if (fieldList != null && !fieldList.isEmpty()) {
					for (za.co.ominsure.synapse.iwyze.lob.backend.party.vo.Flexfield field : fieldList) {
						if (field != null && field.getVarcharAttribute() != null && field.getVarcharAttribute().getAttributeName().equalsIgnoreCase("CONTACT_VIA_SMS")) {
							viaSMS = field.getVarcharAttribute().getValue();
						}
						if (field != null && field.getVarcharAttribute() != null && field.getVarcharAttribute().getAttributeName().equalsIgnoreCase("CONTACT_VIA_EMAIL")) {
							viaEmail = field.getVarcharAttribute().getValue();
						}
						if (field != null && field.getVarcharAttribute() != null && field.getVarcharAttribute().getAttributeName().equalsIgnoreCase("CONTACT_VIA_PHONE")) {
							viaPhone = field.getVarcharAttribute().getValue();
						}
					}
				}
				if (!StringUtils.isBlank(viaSMS) && viaSMS.equalsIgnoreCase("1"))
					messages.append("viaSMS");
				if (!StringUtils.isBlank(viaEmail) && viaEmail.equalsIgnoreCase("1"))
					messages.append("viaEmail");
				if (!StringUtils.isBlank(viaPhone) && viaPhone.equalsIgnoreCase("1"))
					messages.append("viaPhone");
				thirdParties.setInsuredCommunicationMethod(messages.toString());
			}
			List<ContactInfo> contactInfoList = null;
			if (response.getParty().getPerson() != null && !response.getParty().getPerson().isNil())
				contactInfoList = response.getParty().getPerson().getValue().getContactInfoCollection().getContactInfo();
			if (contactInfoList != null && !contactInfoList.isEmpty()) {
				for (ContactInfo contactInfo : contactInfoList) {
					if (!contactInfo.getContactInfoDetail().isNil()) {
						thirdParties.setInsuredContactNumber(contactInfo.getContactInfoDetail().getValue());
						Synlog.info("ContactInfoDetail :" + contactInfo.getContactInfoDetail().getValue());
					}
				}
			}
			if (response.getParty().getPerson() != null && !response.getParty().getPerson().getValue().getCivilRegCode().isNil())
				thirdParties.setInsuredIdNumber(response.getParty().getPerson().getValue().getCivilRegCode().getValue());
			Synlog.info("InsuredIdNumber :" + response.getParty().getPerson().getValue().getCivilRegCode().getValue());
			if (response.getParty().getPerson() != null && !response.getParty().getPerson().getValue().getLanguage().isNil())
				thirdParties.setInsuredLanguage(response.getParty().getPerson().getValue().getLanguage().getValue());
			Synlog.info("InsuredLanguage :" + response.getParty().getPerson().getValue().getLanguage().getValue());
		}
		if (Synlog.isInfoEnabled()) {
			Synlog.info("thirdParties generic payload in json:", genson.serialize(thirdParties));
		}
		return thirdParties;
	}

	private Claim searchGenericParty(SearchPartyResponse response) {
		Claim claim = new Claim();
		Genson genson = new GensonBuilder().withBundle(new JAXBBundle()).setSkipNull(true).create();
		if (response != null && response.getPartyCollection() != null && !response.getPartyCollection().getParty().isEmpty()) {
			Synlog.info("party size:" + response.getPartyCollection().getParty().size());
			List<PartyElement> partyElementList = response.getPartyCollection().getParty();
			if (partyElementList != null && !partyElementList.isEmpty()) {
				for (PartyElement element : partyElementList) {
					if (element.getPerson() != null && !element.getPerson().isNil() && !element.getPerson().getValue().getName().isNil()) {
						claim.setPolicyHolderName(element.getPerson().getValue().getName().getValue());// policy holder name(name in AJS)
					}
				}
			}
		}
		if(Synlog.isInfoEnabled()) {
		Synlog.info("claim generic payload in searchGenericParty method:", genson.serialize(claim));
		}
		return claim;
	}

	/*************************************************************************************************************************************
	 * Helper to create the proxy to Iwyze
	 *
	 * @return The iwyze claimAutomicRouterService proxy
	 * @throws HttpException
	 *             Thrown if the URL cannot be retrieved from the Config table.
	 */

	private PtClaim getClaimAutomicRouterServiceProxy() throws HttpException {
		Synlog.info("getCliam call started");
		String serviceUrl = ServiceConfig.getInstance().getProperty("CLAIM_AUTOMIC_ROUTER_SERVICE");
		Synlog.info("CLAIM_AUTOMIC_ROUTER_SERVICE =" + serviceUrl);
		PtClaim client = null;
		BindingProvider bp = null;
		try {
			SvClaim service = new SvClaim(new URL(serviceUrl), new QName(CLAIM_AUTOMIC_SERVICE_NAMESPACE, "svClaim"));
			client = service.getPtClaimPt();
			if (client != null)
				bp = (BindingProvider) client;
			if (bp != null)
				bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, serviceUrl);
		}
		catch (Exception e) {
			Synlog.error(CLAIM_PROXY_MESSAGE + e.getMessage());
			throw new HttpException(CLAIM_PROXY_MESSAGE + e.getMessage(), 500);
		}
		return client;
	}

	/*************************************************************************************************************************************
	 * Helper to create the proxy to Iwyze
	 *
	 * @return The iwyze claimPaymentRouterService proxy
	 * @throws HttpException
	 *             Thrown if the URL cannot be retrieved from the Config table.
	 */

	private PtClaimPayment getClaimPaymentRouterServiceProxy() throws HttpException {
		String serviceUrl = ServiceConfig.getInstance().getProperty("CLAIM_PAYMENT_ROUTER_SERVICE");
		Synlog.info("CLAIM_PAYMENT_ROUTER_SERVICE =" + serviceUrl);
		PtClaimPayment client = null;
		BindingProvider bp = null;
		try {
			SvClaimPayment service = new SvClaimPayment(new URL(serviceUrl), new QName(CLAIM_PAYMENT_SERVICE_NAMESPACE, "svClaimPayment"));
			client = service.getPtClaimPaymentPt();
			if (client != null)
				bp = (BindingProvider) client;
			if (bp != null)
				bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, serviceUrl);
		}
		catch (Exception e) {
			Synlog.error(CLAIM_PROXY_MESSAGE + e.getMessage());
			throw new HttpException(CLAIM_PROXY_MESSAGE + e.getMessage(), 500);
		}
		return client;
	}

	/*************************************************************************************************************************************
	 * Helper to create the proxy to Iwyze
	 *
	 * @return The iwyze polciyAutomicRouterService proxy
	 * @throws HttpException
	 *             Thrown if the URL cannot be retrieved from the Config table.
	 */

	private PtPolicyAtomic getPolicyAutomicRouterServiceProxy() throws HttpException {
		Synlog.info("getCliam call started");
		String serviceUrl = ServiceConfig.getInstance().getProperty("POLICY_AUTOMIC_ROUTER_SERVICE");
		Synlog.info("POLICY_AUTOMIC_ROUTER_SERVICE =" + serviceUrl);
		PtPolicyAtomic client = null;
		BindingProvider bp = null;
		try {
			SvPolicyAtomic service = new SvPolicyAtomic(new URL(serviceUrl), new QName("http://infrastructure.tia.dk/contract/policy/v4/", "svPolicyAtomic"));
			client = service.getPtPolicyAtomicPt();
			if (client != null)
				bp = (BindingProvider) client;
			if (bp != null)
				bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, serviceUrl);
		}
		catch (Exception e) {
			Synlog.error(POLICY_PROXY_MESSAGE + e.getMessage());
			throw new HttpException(POLICY_PROXY_MESSAGE + e.getMessage(), 500);
		}
		return client;
	}

	/*************************************************************************************************************************************
	 * Helper to create the proxy to Iwyze
	 *
	 * @return The iwyze partyAutomicRouterService proxy
	 * @throws HttpException
	 *             Thrown if the URL cannot be retrieved from the Config table.
	 */

	private PtParty getPartyAutomicRouterServiceProxy() throws HttpException {
		Synlog.info("entered into method getPartyAutomicRouterServiceProxy....");
		String serviceUrl = ServiceConfig.getInstance().getProperty("PARTY_AUTOMIC_ROUTER_SERVICE");
		Synlog.info("PARTY_AUTOMIC_ROUTER_SERVICE =" + serviceUrl);
		PtParty client = null;
		BindingProvider bp = null;
		try {
			SvParty service = new SvParty(new URL(serviceUrl), new QName("http://infrastructure.tia.dk/contract/party/v3/", "svParty"));
			client = service.getPtPartyPt();
			if (client != null)
				bp = (BindingProvider) client;
			if (bp != null)
				bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, serviceUrl);
		}
		catch (Exception e) {
			Synlog.error(PARTY_PROXY_MESSAGE + e.getMessage());
			throw new HttpException(PARTY_PROXY_MESSAGE + e.getMessage(), 500);
		}
		return client;
	}
	
	
	/*************************************************************************************************************************************
	 * Helper to create the proxy to Iwyze
	 *
	 * @return The iwyze caseRouterSercice proxy
	 * @throws HttpException
	 *             Thrown if the URL cannot be retrieved from the Config table.
	 */

	private PtCase getCaseRouterSerciceProxy() throws HttpException {
		Synlog.info("entered into method getCaseRouterSerciceProxy....");
		String serviceUrl = ServiceConfig.getInstance().getProperty("CASE_ROUTER_SERVICE");
		Synlog.info("CASE_ROUTER_SERVICE =" + serviceUrl);
		PtCase client = null;
		BindingProvider bp = null;
		try {
			SvCase service = new SvCase(new URL(serviceUrl), new QName("http://infrastructure.tia.dk/contract/case/v3/", "svCase"));
			client = service.getPtCasePt();
			if (client != null)
				bp = (BindingProvider) client;
			if (bp != null)
				bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, serviceUrl);
		}
		catch (Exception e) {
			Synlog.error(CASE_ITEM_PROXY_MESSAGE + e.getMessage());
			throw new HttpException(CASE_ITEM_PROXY_MESSAGE + e.getMessage(), 500);
		}
		return client;
	}

	/*************************************************************************************************************************************
	 * Helper to create the proxy to Iwyze
	 *
	 * @return The iwyze claimInteractiveRouterServiceProxy proxy
	 * @throws HttpException
	 *             Thrown if the URL cannot be retrieved from the Config table.
	 */

	private PtClaimInteractive getClaimInteractiveRouterServiceProxy() throws HttpException {
		Synlog.info("entered into method getClaimInteractiveRouterServiceProxy....");
		String serviceUrl = ServiceConfig.getInstance().getProperty("CLAIM_INTERACTIVE_ROUTER_SERVICE");
		Synlog.info("CLAIM_INTERACTIVE_ROUTER_SERVICE =" + serviceUrl);
		PtClaimInteractive client = null;
		BindingProvider bp = null;
		try {
			SvClaimInteractive service = new SvClaimInteractive(new URL(serviceUrl), new QName(CLAIM_INTERACTIVE_SERVICE_NAMESPACE, "svClaimInteractive"));
			client = service.getPtClaimInteractivePt();
			if (client != null)
				bp = (BindingProvider) client;
			if (bp != null)
				bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, serviceUrl);
		}
		catch (Exception e) {
			Synlog.error(CLAIM_INTERACTIVE_PROXY_MESSAGE + e.getMessage());
			throw new HttpException(CLAIM_INTERACTIVE_PROXY_MESSAGE + e.getMessage(), 500);
		}
		return client;
	}

	private void iwyzeErrorMessages(Object object) throws HttpException {

		StringBuilder messages = new StringBuilder();

		if (object instanceof GetPartyResponse) {
			Result result = null;
			MessageCollection messageCollection = null;
			List<MessageDetail> messageDetailList = null;
			GetPartyResponse partyResponse = null;
			Synlog.info("object is instaceOf GetPartyResponse is true");
			partyResponse = (GetPartyResponse) object;
			result = partyResponse.getResult();
			messageCollection = result.getMessageCollection();
			if (messageCollection != null)
				messageDetailList = messageCollection.getMessageDetail();
			if (messageDetailList != null) {
				for (MessageDetail detail : messageDetailList) {
					messages.append(detail.getMessageText());
				}
			}
			else
				messages.append(UNKNOWN_ERROR);
			throw new HttpException(messages.toString(), 400);
		}
		else if (object instanceof GetClaimResponse) {
			za.co.ominsure.synapse.iwyze.lob.backend.claim.vo.Result result = null;
			za.co.ominsure.synapse.iwyze.lob.backend.claim.vo.MessageCollection messageCollection = null;
			List<za.co.ominsure.synapse.iwyze.lob.backend.claim.vo.MessageDetail> messageDetailList = null;
			GetClaimResponse claimResponse = null;
			Synlog.info("object is instaceOf GetClaimResponse is true");
			claimResponse = (GetClaimResponse) object;
			result = claimResponse.getResult();
			messageCollection = result.getMessageCollection();
			if (messageCollection != null)
				messageDetailList = messageCollection.getMessageDetails();
			if (messageDetailList != null) {
				for (za.co.ominsure.synapse.iwyze.lob.backend.claim.vo.MessageDetail detail : messageDetailList) {
					messages.append(detail.getMessageText());
				}
			}
			else
				messages.append(UNKNOWN_ERROR);
			throw new HttpException(messages.toString(), 400);
		}
		else if (object instanceof GetPolicyResponse) {
			za.co.ominsure.synapse.iwyze.lob.backend.policy.vo.Result result = null;
			za.co.ominsure.synapse.iwyze.lob.backend.policy.vo.MessageCollection messageCollection = null;
			List<za.co.ominsure.synapse.iwyze.lob.backend.policy.vo.MessageDetail> messageDetailList = null;
			GetPolicyResponse policyResponse = null;
			Synlog.info("object is instaceOf GetPolicyResponse is true");
			policyResponse = (GetPolicyResponse) object;
			result = policyResponse.getResult();
			messageCollection = result.getMessageCollection();
			if (messageCollection != null)
				messageDetailList = messageCollection.getMessageDetails();
			if (messageDetailList != null) {
				for (za.co.ominsure.synapse.iwyze.lob.backend.policy.vo.MessageDetail detail : messageDetailList) {
					messages.append(detail.getMessageText());
				}
			}
			else
				messages.append(UNKNOWN_ERROR);
			throw new HttpException(messages.toString(), 400);
		}
		else if (object instanceof SearchPolicyResponse) {
			za.co.ominsure.synapse.iwyze.lob.backend.policy.vo.Result result = null;
			za.co.ominsure.synapse.iwyze.lob.backend.policy.vo.MessageCollection messageCollection = null;
			List<za.co.ominsure.synapse.iwyze.lob.backend.policy.vo.MessageDetail> messageDetailList = null;
			GetPolicyResponse policyResponse = null;
			Synlog.info("object is instaceOf SearchPolicyResponse is true");
			policyResponse = (GetPolicyResponse) object;
			result = policyResponse.getResult();
			messageCollection = result.getMessageCollection();
			if (messageCollection != null)
				messageDetailList = messageCollection.getMessageDetails();
			if (messageDetailList != null) {
				for (za.co.ominsure.synapse.iwyze.lob.backend.policy.vo.MessageDetail detail : messageDetailList) {
					messages.append(detail.getMessageText());
				}
			}
			else
				messages.append(UNKNOWN_ERROR);
			throw new HttpException(messages.toString(), 400);
		}
		else if (object instanceof SearchSubclaimResponse) {
			za.co.ominsure.synapse.iwyze.lob.backend.interactive.claim.vo.Result result = null;
			za.co.ominsure.synapse.iwyze.lob.backend.interactive.claim.vo.MessageCollection messageCollection = null;
			List<za.co.ominsure.synapse.iwyze.lob.backend.interactive.claim.vo.MessageDetail> messageDetailList = null;
			SearchSubclaimResponse searchSubClaimResponse = null;
			Synlog.info("object is instaceOf SearchSubclaimResponse is true");
			searchSubClaimResponse = (SearchSubclaimResponse) object;
			result = searchSubClaimResponse.getResult();
			messageCollection = result.getMessageCollection();
			if (messageCollection != null)
				messageDetailList = messageCollection.getMessageDetails();
			if (messageDetailList != null) {
				for (za.co.ominsure.synapse.iwyze.lob.backend.interactive.claim.vo.MessageDetail detail : messageDetailList) {
					messages.append(detail.getMessageText());
				}
			}
			else
				messages.append(UNKNOWN_ERROR);
			throw new HttpException(messages.toString(), 400);
		}
		else if (object instanceof SearchPartyResponse) {
			Result result = null;
			MessageCollection messageCollection = null;
			List<MessageDetail> messageDetailList = null;
			SearchPartyResponse searchResponse = null;
			Synlog.info("object is instaceOf SearchPartyResponse is true");
			searchResponse = (SearchPartyResponse) object;
			result = searchResponse.getResult();
			messageCollection = result.getMessageCollection();
			if (messageCollection != null)
				messageDetailList = messageCollection.getMessageDetail();
			if (messageDetailList != null) {
				for (MessageDetail detail : messageDetailList) {
					messages.append(detail.getMessageText());
				}
			}
			else
				messages.append(UNKNOWN_ERROR);
			throw new HttpException(messages.toString(), 400);
		}
		else if (object instanceof AddClaimResponse) {
			za.co.ominsure.synapse.iwyze.lob.backend.claim.vo.Result result = null;
			za.co.ominsure.synapse.iwyze.lob.backend.claim.vo.MessageCollection messageCollection = null;
			List<za.co.ominsure.synapse.iwyze.lob.backend.claim.vo.MessageDetail> messageDetailList = null;
			AddClaimResponse addClaimResponse = null;
			Synlog.info("object is instaceOf AddClaimResponse is true");
			addClaimResponse = (AddClaimResponse) object;
			result = addClaimResponse.getResult();
			messageCollection = result.getMessageCollection();
			if (messageCollection != null)
				messageDetailList = messageCollection.getMessageDetails();
			if (messageDetailList != null) {
				for (za.co.ominsure.synapse.iwyze.lob.backend.claim.vo.MessageDetail detail : messageDetailList) {
					messages.append(detail.getMessageText());
				}
			}
			else
				messages.append(UNKNOWN_ERROR);
			throw new HttpException(messages.toString(), 400);
		}
		else if (object instanceof GetClaimPaymentsResponse) {
			za.co.ominsure.synapse.iwyze.lob.backend.claim.payment.vo.Result result = null;
			za.co.ominsure.synapse.iwyze.lob.backend.claim.payment.vo.MessageCollection messageCollection = null;
			List<za.co.ominsure.synapse.iwyze.lob.backend.claim.payment.vo.MessageDetail> messageDetailList = null;
			GetClaimPaymentsResponse getClaimPaymentsResponse = null;
			Synlog.info("object is instaceOf GetClaimPaymentsResponse is true");
			getClaimPaymentsResponse = (GetClaimPaymentsResponse) object;
			result = getClaimPaymentsResponse.getResult();
			messageCollection = result.getMessageCollection();
			if (messageCollection != null)
				messageDetailList = messageCollection.getMessageDetails();
			if (messageDetailList != null) {
				for (za.co.ominsure.synapse.iwyze.lob.backend.claim.payment.vo.MessageDetail detail : messageDetailList) {
					messages.append(detail.getMessageText());
				}
			}
			else
				messages.append(UNKNOWN_ERROR);
			throw new HttpException(messages.toString(), 400);
		}
		else if (object instanceof GetServiceSupplierResponse) {
			za.co.ominsure.synapse.iwyze.lob.backend.servicesupplier.vo.Result result = null;
			za.co.ominsure.synapse.iwyze.lob.backend.servicesupplier.vo.MessageCollection messageCollection = null;
			List<za.co.ominsure.synapse.iwyze.lob.backend.servicesupplier.vo.MessageDetail> messageDetailList = null;
			GetServiceSupplierResponse getServiceSupplierResponse = null;
			Synlog.info("object is instaceOf GetServiceSupplierResponse is true");
			getServiceSupplierResponse = (GetServiceSupplierResponse) object;
			result = getServiceSupplierResponse.getResult();
			messageCollection = result.getMessageCollection();
			if (messageCollection != null)
				messageDetailList = messageCollection.getMessageDetail();
			if (messageDetailList != null) {
				for (za.co.ominsure.synapse.iwyze.lob.backend.servicesupplier.vo.MessageDetail detail : messageDetailList) {
					messages.append(detail.getMessageText());
				}
			}
			else
				messages.append(UNKNOWN_ERROR);
			throw new HttpException(messages.toString(), 400);
		}
		else if (object instanceof SearchCaseItemResponse) {
			za.co.ominsure.synapse.iwyze.lob.backend.caseItem.vo.Result result = null;
			za.co.ominsure.synapse.iwyze.lob.backend.caseItem.vo.MessageCollection messageCollection = null;
			List<za.co.ominsure.synapse.iwyze.lob.backend.caseItem.vo.MessageDetail> messageDetailList = null;
			SearchCaseItemResponse searchCaseItemResponse = null;
			Synlog.info("object is instaceOf SearchCaseItemResponse is true");
			searchCaseItemResponse = (SearchCaseItemResponse) object;
			result = searchCaseItemResponse.getResult();
			messageCollection = result.getMessageCollection();
			if (messageCollection != null)
				messageDetailList = messageCollection.getMessageDetails();
			if (messageDetailList != null) {
				for (za.co.ominsure.synapse.iwyze.lob.backend.caseItem.vo.MessageDetail detail : messageDetailList) {
					messages.append(detail.getMessageText());
				}
			}
			else
				messages.append(UNKNOWN_ERROR);
			throw new HttpException(messages.toString(), 400);
		}
	}

	private ServiceConfiguration getServiceConfig() throws HttpException {
		ServiceConfiguration config = new ServiceConfiguration();
		String userId = ServiceConfig.getInstance().getProperty("USERID");
		String language = ServiceConfig.getInstance().getProperty("LANGUAGE");
		String traceOn = ServiceConfig.getInstance().getProperty("TRACEON");
		String commitOn = ServiceConfig.getInstance().getProperty("COMMITON");
		String addClaimCommitOn = ServiceConfig.getInstance().getProperty("ADD_CLAIM_COMMITON");
		config.setUserId(userId);
		config.setLanguage(language);
		config.setTraceOn(traceOn);
		config.setCommitOn(commitOn);
		config.setAddClaimCommitOn(addClaimCommitOn);
		return config;
	}

	@Override
	public Case getCaseInfo(Long claimNumber, long caseNumber) throws HttpException {

		return null;
	}

	private Claim getClaimDetails(String claimNumber, String siteName) throws HttpException {
		Claim claim = null;
		try {
			claim = (Claim) serviceBridge.callService(CLAIM_REQUEST_URI + claimNumber + "/" + siteName, "GET", null, Claim.class);
		}
		catch (HttpException e) {
			Synlog.error(CLAIM_MESSAGE + e.getMessage());
			throw new HttpException(CLAIM_MESSAGE + e.getMessage(), 404);
		}
		catch (Exception e) {
			Synlog.error(CLAIM_MESSAGE, e.getMessage());
			throw new HttpException(CLAIM_MESSAGE + e.getMessage(), 500);
		}
		return claim;
	}
	
	private CaseItemGenericResponse getClegType(String claimNumber, String siteName) throws HttpException {
		CaseItemGenericResponse caseItemGenericResponse = null;
		try {
			caseItemGenericResponse = (CaseItemGenericResponse) serviceBridge.callService(CASE_ITEM_SEARCH_URI + claimNumber + "/" + siteName, "POST", null, CaseItemGenericResponse.class);
		}
		catch (HttpException e) {
			Synlog.error(SEARCH_CASE_ITEM_MESSAGE + e.getMessage());
			throw new HttpException(SEARCH_CASE_ITEM_MESSAGE + e.getMessage(), 404);
		}
		catch (Exception e) {
			Synlog.error(SEARCH_CASE_ITEM_MESSAGE, e.getMessage());
			throw new HttpException(SEARCH_CASE_ITEM_MESSAGE + e.getMessage(), 500);
		}
		return caseItemGenericResponse;
	}

	private ThirdParties searchSubClaimDetail(String iwyzeCaseNumber, String siteName) throws HttpException {
		ThirdParties thirdParties = null;
		try {
			thirdParties = (ThirdParties) serviceBridge.callService(SEARCH_SUB_CLAIM_REQUEST_URI + iwyzeCaseNumber + "/" + siteName, "POST", null, ThirdParties.class);
		}
		catch (HttpException e) {
			Synlog.error(SUB_CLAIM_MESSAGE + e.getMessage());
			throw new HttpException(SUB_CLAIM_MESSAGE + e.getMessage(), 400);
		}
		catch (Exception e) {
			Synlog.error(SUB_CLAIM_MESSAGE, e.getMessage());
			throw new HttpException(SUB_CLAIM_MESSAGE + e.getMessage(), 500);
		}
		return thirdParties;
	}

	private ThirdParties getParty(String nameIdNumber, String siteName) throws HttpException {
		ThirdParties getPartyResponse = null;
		try {
			getPartyResponse = (ThirdParties) serviceBridge.callService(GET_PARTY_URI + nameIdNumber + "/case/" + siteName + "/thirdparty", "GET", null, ThirdParties.class);
		}
		catch (HttpException e) {
			Synlog.error(THIRDPARTY_MESSAGE + e.getMessage());
			throw new HttpException(THIRDPARTY_MESSAGE + e.getMessage(), 400);
		}
		catch (Exception e) {
			Synlog.error(THIRDPARTY_MESSAGE, e.getMessage());
			throw new HttpException(THIRDPARTY_MESSAGE + e.getMessage(), 500);
		}
		return getPartyResponse;
	}

	private ThirdParties searchSubClaimService(String caseNumber, String siteName) throws HttpException {
		ThirdParties searchSubClaimResponse = null;
		try {
			searchSubClaimResponse = (ThirdParties) serviceBridge.callService(SEARCH_SUB_CLAIM_URI + caseNumber + "/" + siteName, "POST", null, ThirdParties.class);
		}
		catch (HttpException e) {
			Synlog.error(SEARCHSUBCLAIM_MESSAGE + e.getMessage());
			throw new HttpException(SEARCHSUBCLAIM_MESSAGE + e.getMessage(), 400);
		}
		catch (Exception e) {
			Synlog.error(SEARCHSUBCLAIM_MESSAGE, e.getMessage());
			throw new HttpException(SEARCHSUBCLAIM_MESSAGE + e.getMessage(), 500);
		}
		return searchSubClaimResponse;
	}

	private Claim searchPartyService(String insuredIdNumber, String siteName) throws HttpException {
		Claim searchParty = null;
		try {
			searchParty = (Claim) serviceBridge.callService(SEARCH_PARTY_URI + insuredIdNumber + "/" + siteName, "POST", null, Claim.class);
		}
		catch (HttpException e) {
			Synlog.error(SEARCHPARTY_MESSAGE + e.getMessage());
			throw new HttpException(SEARCHPARTY_MESSAGE + e.getMessage(), 400);
		}
		catch (Exception e) {
			Synlog.error(SEARCHPARTY_MESSAGE, e.getMessage());
			throw new HttpException(SEARCHPARTY_MESSAGE + e.getMessage(), 500);
		}
		return searchParty;
	}

	private CreateRequest buildingAJSRequest(CreateRequest request, Claim searchParty, ThirdParties searchSubClaimResponse, ThirdParties getPartyResponse) {

		Genson genson = new GensonBuilder().withBundle(new JAXBBundle()).setSkipNull(true).create();
		if(Synlog.isInfoEnabled()) {
		Synlog.info("searchSubClaimResponse payload is:", genson.serialize(searchSubClaimResponse));
		}
		if(Synlog.isInfoEnabled()) {
		Synlog.info("getPartyResponse in buildingAJSRequest payload is:", genson.serialize(getPartyResponse));
		}

		if (searchParty != null && !StringUtils.isBlank(searchParty.getPolicyHolderName())) {
			request.setPolicyHolderName(searchParty.getPolicyHolderName());// required for AJS if not provided AJS call will fail
		}
		request.setSourcePlatform(IWYZE_SOURCE_PLATFORM);
		if (searchSubClaimResponse != null && !searchSubClaimResponse.getThirdParty().isEmpty()) {
			Synlog.info("entered into buildingAJSRequest searchSubClaimResponse not null");
			Synlog.info("size of third party list in searchSubClaimResponse:" + searchSubClaimResponse.getThirdParty().size());
			if (searchSubClaimResponse.getThirdParty().get(0).getThirdPartyNumber() != 0)
				request.setThirdpartyNumber(searchSubClaimResponse.getThirdParty().get(0).getThirdPartyNumber() + "");
			Synlog.info("third party number is:" + searchSubClaimResponse.getThirdParty().get(0).getThirdPartyNumber());
			request.setThirdpartyType(searchSubClaimResponse.getThirdParty().get(0).getThirdPartyType());
			request.setIncidentDate(searchSubClaimResponse.getIncidentDate());
			request.setClaimRegistrationDate(searchSubClaimResponse.getCreateDate());
			request.setThirdpartyAddressLine2(searchSubClaimResponse.getThirdParty().get(0).getThirdPartyAddressLine2());
			request.setThirdpartyAddressLine3(searchSubClaimResponse.getThirdParty().get(0).getThirdPartyAddressLine3());
			request.setThirdpartyAddressLine4(searchSubClaimResponse.getThirdParty().get(0).getThirdPartyAddressLine4());
			request.setThirdpartyContactNumber(searchSubClaimResponse.getThirdParty().get(0).getThirdPartyContactNumber());

			request.setThirdpartyDriverName(searchSubClaimResponse.getThirdParty().get(0).getThirdPartyDriverName());
			request.setThirdpartyContactNumber(searchSubClaimResponse.getThirdParty().get(0).getThirdPartyContactNumber());
			request.setThirdpartyIdNumber(searchSubClaimResponse.getThirdParty().get(0).getThirdPartyIdNumber());
			if (StringUtils.isBlank(request.getThirdpartyContactNumber()))
				request.setThirdpartyContactNumber(searchSubClaimResponse.getThirdParty().get(0).getThirdPartyInsuranceContactNumber());
			request.setThirdpartyInsuranceContactNumber(searchSubClaimResponse.getThirdParty().get(0).getThirdPartyInsuranceContactNumber());
			request.setThirdpartyInsuranceName(searchSubClaimResponse.getThirdParty().get(0).getThirdPartyInsuranceName());
			request.setThirdpartyName(searchSubClaimResponse.getThirdParty().get(0).getThirdPartyName());
			request.setVehicleRegistrationNumber(searchSubClaimResponse.getThirdParty().get(0).getRegistrationNumber());
			request.setVehicleMake(searchSubClaimResponse.getThirdParty().get(0).getMakeAndModel());
			request.setVehicleModel(searchSubClaimResponse.getThirdParty().get(0).getMakeAndModel());
			if (searchSubClaimResponse.getCaseEstimateAmount() != null)
				request.setCaseEstimateAmount(searchSubClaimResponse.getCaseEstimateAmount().toString());// caseEstimateAmount
			if (searchSubClaimResponse.getExcessAmount() != null)
				request.setExcessAmount(searchSubClaimResponse.getExcessAmount().toString());// excessAmount
		}
		if (getPartyResponse != null) {
			Synlog.info("befoe setting insured address details");
			request.setInsuredAddressLine1(getPartyResponse.getInsuredAddressLine1());
			request.setInsuredAddressLine2(getPartyResponse.getInsuredAddressLine2());
			request.setInsuredAddressLine3(getPartyResponse.getInsuredAddressLine3());
			request.setInsuredAddressLine4(getPartyResponse.getInsuredAddressLine4());
			request.setInsuredContactNumber(getPartyResponse.getInsuredContactNumber());
			request.setInsuredIdNumber(getPartyResponse.getInsuredIdNumber());
			request.setInsuredCommunicationMethod(getPartyResponse.getInsuredCommunicationMethod());
		}
		if (StringUtils.isBlank(request.getSubcaseNumber())) {
			request.setSubcaseNumber(request.getThirdpartyNumber());// check with iwyze if have multiple subclaimNo
		}

		return request;
	}

	@Override
	public AddClaimsLogResponse addClaimLog(AddClaimsLogRequest request) throws HttpException, SQLException {
		Map<String, String> requestHeaders = new HashMap<>();
		String apiKey = null;
		Genson genson = new GensonBuilder().withBundle(new JAXBBundle()).setSkipNull(true).create();
		String restURIFragment = ServiceConfig.getInstance().getProperty(ADD_CLAIMS_LOG_REST_URI_FRAGMENT);
		requestHeaders.put(SynapseConstants.ACCEPT, SynapseConstants.APPLICATION_JSON);
		requestHeaders.put(SynapseConstants.CONTENT_TYPE, SynapseConstants.APPLICATION_JSON);
		ClaCaseResponse claCaseResponse = null;
		AddClaimsLogResponse addClaimsLogResponse = null;
		if (request != null && !StringUtils.isBlank(request.getClaimNo())) {
			claCaseResponse = getSequenceNumberServiceByIwyzeClaimNumber(request.getClaimNo());
		}
		if (claCaseResponse != null && !StringUtils.isBlank(claCaseResponse.getSequenceNumber())) {
			apiKey = getAPIKeyBySequenceNumber(claCaseResponse.getSequenceNumber());
		}
		if (!StringUtils.isBlank(apiKey)) {
			Synlog.info("apiKey is=" + apiKey);
			requestHeaders.put("apiKey", apiKey);
		}

		try {
			addClaimsLogResponse = (AddClaimsLogResponse) serviceBridge.callExternalService(restURIFragment + "/wtf-web/claimLog", "POST", request, AddClaimsLogResponse.class, requestHeaders);
			if(Synlog.isInfoEnabled()) {
			Synlog.info("AddClaimsLogResponse iwyze response payload in json:", genson.serialize(addClaimsLogResponse));
			}
		}
		catch (HttpException e) {
			Synlog.error(ADD_CLAIMS_LOG_FAIL + e.getMessage());
			throw new HttpException(ADD_CLAIMS_LOG_FAIL + e.getMessage(), 400);
		}
		catch (Exception e) {
			Synlog.error(ADD_CLAIMS_LOG_FAIL, e.getMessage());
			throw new HttpException(ADD_CLAIMS_LOG_FAIL + e.getMessage(), 500);
		}
		return addClaimsLogResponse;
	}

	private AddClaimsLogResponse addClaimsLogService(AddClaimsLogRequest request) throws HttpException {
		AddClaimsLogResponse addClaimsLogResponse = null;
		try {
			addClaimsLogResponse = (AddClaimsLogResponse) serviceBridge.callService(CLAIM_REQUEST_URI + request.getEventNo() + "/audit", "POST", request, AddClaimsLogResponse.class);
		}
		catch (HttpException e) {
			Synlog.error(ADD_CLAIMS_LOG_FAIL + e.getMessage());
			throw new HttpException(ADD_CLAIMS_LOG_FAIL + e.getMessage(), 400);
		}
		catch (Exception e) {
			Synlog.error(ADD_CLAIMS_LOG_FAIL, e.getMessage());
			throw new HttpException(ADD_CLAIMS_LOG_FAIL + e.getMessage(), 500);
		}
		return addClaimsLogResponse;
	}

	@Override
	public GetClaimsLogResponse getClaimsLog(String claimNumber) throws HttpException, SQLException {
		Map<String, String> requestHeaders = new HashMap<>();
		Genson genson = new GensonBuilder().withBundle(new JAXBBundle()).setSkipNull(true).create();
		String restURIFragment = ServiceConfig.getInstance().getProperty(GET_CLAIMS_LOG_REST_URI_FRAGMENT);
		ClaCaseResponse claCaseResponse = null;
		String secret = null;
		claCaseResponse = getSequenceNumberServiceByIwyzeClaimNumber(claimNumber);
		if (claCaseResponse != null && !StringUtils.isBlank(claCaseResponse.getSequenceNumber())) {
			secret = getSecretBySequenceNumber(claCaseResponse.getSequenceNumber());
		}
		Synlog.info("secret value is :" + secret);
		final String IWYZE_GET_CLAIM_DETAIL_REST_URI = restURIFragment + "/common/api/v3/dynamicOutput/GET_CLM_LG?size=";
		requestHeaders.put(SynapseConstants.ACCEPT, SynapseConstants.APPLICATION_JSON);
		requestHeaders.put(SynapseConstants.CONTENT_TYPE, SynapseConstants.APPLICATION_JSON);
		requestHeaders.put(SynapseConstants.AUTHORIZATION, SynapseConstants.BASIC + secret);
		Synlog.info("secret=" + secret);
		GetClaimsLogResponse response = null;
		try {
			StringBuilder path = new StringBuilder();
			path.append(IWYZE_GET_CLAIM_DETAIL_REST_URI).append(URLEncoder.encode("2000", UTF8));
			path.append("&param1=" + URLEncoder.encode(claimNumber, UTF8));
			Synlog.info(PATH + path.toString());
			response = (GetClaimsLogResponse) serviceBridge.callExternalService(path.toString(), "GET", null, GetClaimsLogResponse.class, requestHeaders);
			if(Synlog.isInfoEnabled()) {
			Synlog.info("getClaimsLog Content iwyze response payload in json:", genson.serialize(response));
			}
		}
		catch (HttpException e) {
			Synlog.error(GET_CLAIMS_LOG_FAIL + e.getMessage());
			throw new HttpException(GET_CLAIMS_LOG_FAIL + e.getMessage(), 404);
		}
		catch (Exception e) {
			Synlog.error(GET_CLAIMS_LOG_FAIL, e.getMessage());
			throw new HttpException(GET_CLAIMS_LOG_FAIL + e.getMessage(), 500);
		}
		return response;
	}

	private GetClaimsLogResponse getClaimsLogService(String claimNumber) throws HttpException {
		GetClaimsLogResponse response = null;
		try {
			response = (GetClaimsLogResponse) serviceBridge.callService(CLAIM_REQUEST_URI + claimNumber + "/detail", "GET", null, GetClaimsLogResponse.class);
		}
		catch (HttpException e) {
			Synlog.error(GET_CLAIMS_LOG_FAIL + e.getMessage());
			throw new HttpException(GET_CLAIMS_LOG_FAIL + e.getMessage(), 404);
		}
		catch (Exception e) {
			Synlog.error(GET_CLAIMS_LOG_FAIL, e.getMessage());
			throw new HttpException(GET_CLAIMS_LOG_FAIL + e.getMessage(), 500);
		}
		return response;
	}

	@Override
	public void claimStatusUpdate(String claimNumber, RecoveryStatusChangeRequest request) throws HttpException {
		Synlog.info("has not been implemented yet need to discuss this with Iwyze");
	}

	@Override
	public GetPolicyNumberResponse getPolicyDetail(String policyHolderId, String secret) throws HttpException {
		Map<String, String> requestHeaders = new HashMap<>();
		Genson genson = new GensonBuilder().withBundle(new JAXBBundle()).setSkipNull(true).create();
		String restURIFragment = ServiceConfig.getInstance().getProperty(GET_POLICY_NUMBER_REST_URI_FRAGMENT);
		Synlog.info("secret value is :" + secret);
		final String IWYZE_GET_POLCY_NUMBER_DETAIL_REST_URI = restURIFragment + "/common/api/v3/dynamicOutput/SV_CHK_POL?";
		requestHeaders.put(SynapseConstants.ACCEPT, SynapseConstants.APPLICATION_JSON);
		requestHeaders.put(SynapseConstants.CONTENT_TYPE, SynapseConstants.APPLICATION_JSON);
		requestHeaders.put(SynapseConstants.AUTHORIZATION, SynapseConstants.BASIC + secret);
		Synlog.info("secret=" + secret);
		GetPolicyNumberResponse response = null;
		try {
			StringBuilder path = new StringBuilder();
			path.append(IWYZE_GET_POLCY_NUMBER_DETAIL_REST_URI).append("param1=" + URLEncoder.encode(policyHolderId, UTF8));
			Synlog.info(PATH + path.toString());
			response = (GetPolicyNumberResponse) serviceBridge.callExternalService(path.toString(), "GET", null, GetPolicyNumberResponse.class, requestHeaders);
			if(Synlog.isInfoEnabled()) {
			Synlog.info("getPolicyDetail Content iwyze response payload in json:", genson.serialize(response));
			}
		}
		catch (HttpException e) {
			Synlog.error(GET_POLICY_NUMBER_SERVICE_MESSAGE + e.getMessage());
			throw new HttpException(GET_POLICY_NUMBER_SERVICE_MESSAGE + e.getMessage(), 404);
		}
		catch (Exception e) {
			Synlog.error(GET_POLICY_NUMBER_SERVICE_MESSAGE, e.getMessage());
			throw new HttpException(GET_POLICY_NUMBER_SERVICE_MESSAGE + e.getMessage(), 500);
		}
		return response;
	}

	private GetPolicyNumberResponse getPolicyDetailService(String policyHolderId, String secret) throws HttpException {
		GetPolicyNumberResponse response = null;
		try {
			response = (GetPolicyNumberResponse) serviceBridge.callService(GET_POLICY_NUMBER_REST_URI + policyHolderId + "/" + secret, "GET", null, GetPolicyNumberResponse.class);
		}
		catch (Exception e) {
			Synlog.error(GET_POLICY_NUMBER_SERVICE_MESSAGE, e.getMessage());
			throw new HttpException(GET_POLICY_NUMBER_SERVICE_MESSAGE + e.getMessage(), 500);
		}
		return response;
	}

	private void setUser(String user, Connection con) throws SQLException {
		CallableStatement cs = null;
		try {
			cs = con.prepareCall("{ call p8160.setup_user(?) }");
			cs.setString(1, user);
			Synlog.info("before execute setUser");
			cs.execute();
			Synlog.info("after execute  setUser");
		}
		finally {
			DatabaseUtil.closeResources(con, null, null, cs);
		}
	}

	private void setSite(String site, String access, Connection con) throws SQLException {
		CallableStatement cs = null;
		try {
			cs = con.prepareCall("{ call p0000.set_site(?,?) }");
			cs.setLong(1, Long.parseLong(site));
			cs.setString(2, access);
			Synlog.info("before  execute setSite");
			cs.execute();
			Synlog.info("after  execute setSite");
		}
		finally {
			DatabaseUtil.closeResources(con, null, null, cs);
		}
	}

	private void setSearchMethod(String method, Connection con) throws SQLException {
		CallableStatement cs = null;
		try {
			cs = con.prepareCall("{ call p0000.set_search_method(?) }");
			cs.setString(1, method);
			Synlog.info("before execute setSearchMethod");
			cs.execute();
			Synlog.info("after execute setSearchMethod");
		}
		finally {
			DatabaseUtil.closeResources(con, null, null, cs);
		}
	}

	private ClaCaseResponse getSequenceNumberServiceByIwyzeClaimNumber(String claimNumber) throws HttpException, SQLException {
		String user = ServiceConfig.getInstance().getProperty("USER");
		String setUpSiteFlag = ServiceConfig.getInstance().getProperty("SET_UP_SITE_FLAG");
		ClaCaseResponse response = null;
		Synlog.info("user from config:" + user);
		String access = ServiceConfig.getInstance().getProperty("ACCESS");
		Synlog.info("ACCESS from config:" + access);
		String method = ServiceConfig.getInstance().getProperty("METHOD");
		Synlog.info("METHOD from config:" + method);
		String site = ServiceConfig.getInstance().getProperty("SITE");
		Synlog.info("SITE from config:" + site);
		Synlog.info("claimNumber in getSequenceNumberServiceByClaimNumber private method:" + claimNumber);
		if (!StringUtils.isBlank(claimNumber)) {
			Long sequence = null;
			Long iwyzeClaimEventNumber = null;
			try (Connection con = dataSource.getConnection(); PreparedStatement ps = con.prepareStatement("select * from CLA_CASE where CLA_CASE_NO=?")) {
				ps.setLong(1, Long.parseLong(claimNumber));
				Synlog.info("start getSiteSequence");
				if (!StringUtils.isBlank(setUpSiteFlag) && setUpSiteFlag.equalsIgnoreCase("Y")) {
					Synlog.info("set site is true");
					setUser(user, con);
					setSite(site, access, con);
					setSearchMethod(method, con);
				}
				try (ResultSet rs = ps.executeQuery()) {
					if (rs.next()) {
						Synlog.info("entered into rs.next() block");
						response = new ClaCaseResponse();
						sequence = rs.getLong("SITE_SEQ_NO");
						iwyzeClaimEventNumber = rs.getLong("CLA_EVENT_NO");
						response.setSequenceNumber(sequence == null ? EMPTY_STRING : sequence.toString());
						response.setEventNumber(iwyzeClaimEventNumber == null ? EMPTY_STRING : iwyzeClaimEventNumber.toString());
						response.setClaimNumber(claimNumber);
						Synlog.info("SITE_SEQ_NO by claimNumber=" + response.getSequenceNumber());
						Synlog.info("cla_event_no =" + response.getEventNumber());
						if (StringUtils.isBlank(response.getEventNumber()) || StringUtils.isBlank(response.getSequenceNumber())) {
							Synlog.info("event number or sequence number is null");
							throw new HttpException("claim not found", Response.Status.BAD_REQUEST.getStatusCode());
						}
					}
					else {
						Synlog.info("claimNo not found");
						throw new HttpException("claimNo not found", Response.Status.BAD_REQUEST.getStatusCode());
					}
				}
			}
		}
		return response;
	}

	private String getSecretBySequenceNumber(String sequenceNumber) throws HttpException {
		String secret = null;
		switch (sequenceNumber) {
		case "2":
			secret = ServiceConfig.getInstance().getProperty(SHARED_BASE64_CRED);
			break;
		case "3":
			secret = ServiceConfig.getInstance().getProperty(INTERNAL_BASE64_CRED);
			break;
		case "4":
			secret = ServiceConfig.getInstance().getProperty(AFFINITY_BASE64_CRED);
			break;
		case "5":
			secret = ServiceConfig.getInstance().getProperty(IWYZE_BASE64_CRED);
			break;
		case "6":
			secret = ServiceConfig.getInstance().getProperty(SUREWISE_BASE64_CRED);
			break;
		case "7":
			secret = ServiceConfig.getInstance().getProperty(DA_BASE64_CRED);
			break;
		case "8":
			secret = ServiceConfig.getInstance().getProperty(FNB_BASE64_CRED);
			break;
		case "9":
			secret = ServiceConfig.getInstance().getProperty(NEDBANK_BASE64_CRED);
			break;
		case "10":
			secret = ServiceConfig.getInstance().getProperty(TFG_BASE64_CRED);
			break;
		case "11":
			secret = ServiceConfig.getInstance().getProperty(MOTORSURE_BASE64_CRED);
			break;
		case "12":
			secret = ServiceConfig.getInstance().getProperty(OLDMUTUAL_BASE64_CRED);
			break;
		case "13":
			secret = ServiceConfig.getInstance().getProperty(PINEAPPLE_BASE64_CRED);
			break;
		case "14":
			secret = ServiceConfig.getInstance().getProperty(IEMAS_BASE64_CRED);
			break;
		default:
			Synlog.info("unknown sequence number received: " + sequenceNumber);
			throw new HttpException(UNKNOWN_SEQUENCE_NUMBER_ERROR, 400);
		}
		return secret;
	}
	
	private String getAPIKeyBySequenceNumber(String sequenceNumber) throws HttpException {
		String apiKey = null;
		switch (sequenceNumber) {
		case "2":
			apiKey = ServiceConfig.getInstance().getProperty(API_KEY);
			break;
		case "3":
			apiKey = ServiceConfig.getInstance().getProperty(API_KEY);
			break;
		case "4":
			apiKey = ServiceConfig.getInstance().getProperty(API_KEY);
			break;
		case "5":
			apiKey = ServiceConfig.getInstance().getProperty(IWYZE_API_KEY);
			break;
		case "6":
			apiKey = ServiceConfig.getInstance().getProperty(SUREWISE_API_KEY);
			break;
		case "7":
			apiKey = ServiceConfig.getInstance().getProperty(DA_API_KEY);
			break;
		case "8":
			apiKey = ServiceConfig.getInstance().getProperty(FNB_API_KEY);
			break;
		case "9":
			apiKey = ServiceConfig.getInstance().getProperty(NEDBANK_API_KEY);
			break;
		case "10":
			apiKey = ServiceConfig.getInstance().getProperty(API_KEY);
			break;
		case "11":
			apiKey = ServiceConfig.getInstance().getProperty(MOTORSURE_API_KEY);
			break;
		case "12":
			apiKey = ServiceConfig.getInstance().getProperty(OLDMUTUAL_API_KEY);
			break;
		case "13":
			apiKey = ServiceConfig.getInstance().getProperty(PINEAPPLE_API_KEY);
			break;
		case "14":
			apiKey = ServiceConfig.getInstance().getProperty(IEMAS_API_KEY);
			break;
		default:
			Synlog.info("unknown sequence number received: " + sequenceNumber);
			throw new HttpException(UNKNOWN_SEQUENCE_NUMBER_ERROR, 400);
		}
		return apiKey;
	}

	@Override
	public GetServiceSupplierResponse getServiceSupplier(String receiverIdNumber, String sitename) throws HttpException {
		GetServiceSupplierRequest request = new GetServiceSupplierRequest();
		PtServiceSupplier client = null;
		GetServiceSupplierResponse getServiceSupplierResponse = null;
		try {
			client = getServiceSupplierRouterServiceProxy();
			// building request to iwyze
			if (receiverIdNumber != null)
				request.setInputToken(getIwyzeServiceSupplierRouterServiceToken(sitename));
			request.setAlias(receiverIdNumber);
			if (client != null) {
				getServiceSupplierResponse = client.opGetServiceSupplier(request);
			}
			if (getServiceSupplierResponse != null && getServiceSupplierResponse.getResult() != null && getServiceSupplierResponse.getResult().getResultCode() != null && getServiceSupplierResponse.getResult().getResultCode() != 0) {
				Object object = getServiceSupplierResponse;
				iwyzeErrorMessages(object);
			}
		}
		catch (HttpException e) {
			Synlog.error(GET_CLAIM_PAYMENTS_MESSAGE + e.getMessage());
			throw new HttpException(GET_CLAIM_PAYMENTS_MESSAGE + e.getMessage(), 400);
		}
		catch (Exception e) {
			Synlog.error(GET_CLAIM_PAYMENTS_MESSAGE + e.getMessage());
			throw new HttpException(GET_CLAIM_PAYMENTS_MESSAGE + e.getMessage(), 500);
		}
		return getServiceSupplierResponse;
	}

	private GetServiceSupplierResponse getServiceSupplierDetails(String receiverIdNumber, String sitename) throws HttpException {
		GetServiceSupplierRequest request = new GetServiceSupplierRequest();
		PtServiceSupplier client = null;
		GetServiceSupplierResponse getServiceSupplierResponse = null;
		try {
			client = getServiceSupplierRouterServiceProxy();
			// building request to iwyze
			if (receiverIdNumber != null)
				request.setInputToken(getIwyzeServiceSupplierRouterServiceToken(sitename));
			request.setAlias(receiverIdNumber);
			if (client != null) {
				getServiceSupplierResponse = client.opGetServiceSupplier(request);
			}
		}
		catch (HttpException e) {
			Synlog.error(GET_SERVICE_SUPPLIER_SERVICE_MESSAGE + e.getMessage());
			throw new HttpException(GET_SERVICE_SUPPLIER_SERVICE_MESSAGE + e.getMessage(), 400);
		}
		catch (Exception e) {
			Synlog.error(GET_CLAIM_PAYMENTS_MESSAGE + e.getMessage());
			throw new HttpException(GET_CLAIM_PAYMENTS_MESSAGE + e.getMessage(), 500);
		}
		return getServiceSupplierResponse;
	}

	/*************************************************************************************************************************************
	 * Helper to create the proxy to Iwyze
	 *
	 * @return The iwyze serviceSupplierRouterService proxy
	 * @throws HttpException
	 *             Thrown if the URL cannot be retrieved from the Config table.
	 */

	private PtServiceSupplier getServiceSupplierRouterServiceProxy() throws HttpException {
		String serviceUrl = ServiceConfig.getInstance().getProperty(SERVICE_SUPPLIER_ROUTER_SERVICE);
		Synlog.info("SERVICE_SUPPLIER_ROUTER_SERVICE =" + serviceUrl);
		PtServiceSupplier client = null;
		BindingProvider bp = null;
		try {
			SvServiceSupplier service = new SvServiceSupplier(new URL(serviceUrl), new QName(SERVICE_SUPPLIER_ROUTER_SERVICE_NAMESPACE, "svServiceSupplier"));
			client = service.getPtServiceSupplierPt();
			if (client != null)
				bp = (BindingProvider) client;
			if (bp != null)
				bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, serviceUrl);
		}
		catch (Exception e) {
			Synlog.error(SERVICE_SUPPLIER_PROXY_MESSAGE + e.getMessage());
			throw new HttpException(SERVICE_SUPPLIER_PROXY_MESSAGE + e.getMessage(), 500);
		}
		return client;
	}

	@Override
	public CaseItemGenericResponse searchCaseItem(String claimNumber, String siteName) throws HttpException {
		Synlog.info("searchCaseItem call started");
		SearchCaseItemRequest request = new SearchCaseItemRequest();
		Synlog.info("searchCaseItem claimNumber=" + claimNumber);
		Synlog.info("searchCaseItem siteName=" + siteName);
		CaseItemGenericResponse response = null;
		PtCase client = null;
		SearchCaseItemResponse searchCaseItemResponse = null;
		SearchTokenCase searchToken = new SearchTokenCase();
		try {
			client = getCaseRouterSerciceProxy();
			// building request to iwyze
			request.setInputToken(getIwyzeCaseRouterServiceToken(siteName));
			searchToken.setClaimNo(Long.parseLong(claimNumber));
			request.setSearchTokenCase(searchToken);
			if (client != null) {
				searchCaseItemResponse = client.opSearchCaseItem(request);
			}
			if (searchCaseItemResponse != null && searchCaseItemResponse.getResult() != null) {
				Synlog.info("result code=" + searchCaseItemResponse.getResult().getResultCode());
			}
			if (searchCaseItemResponse != null && searchCaseItemResponse.getResult() != null && searchCaseItemResponse.getResult().getResultCode() != null && searchCaseItemResponse.getResult().getResultCode() == 0) {
				if (!searchCaseItemResponse.getCaseItemCollection().getCaseItems().isEmpty()) {
					for (CaseItem caseItem : searchCaseItemResponse.getCaseItemCollection().getCaseItems()) {
						if (!StringUtils.isBlank(caseItem.getCaseType()) && "CLEG".equals(caseItem.getCaseType())) {
							Synlog.info("found cleg type");
							response = new CaseItemGenericResponse();
							response.setClegType(caseItem.getCaseType());
							if (caseItem.getRequestId() != null) {
								response.setRequestId(caseItem.getRequestId().toString());
							}
						}
					}
				}
				else {
					Synlog.info("case item collection is empty");
					throw new HttpException("CLEG could not be found - case items are empty. ", 400);
				}
			}
			else if (searchCaseItemResponse != null && searchCaseItemResponse.getResult() != null && searchCaseItemResponse.getResult().getResultCode() != null && searchCaseItemResponse.getResult().getResultCode() != 0) {
				Object object = searchCaseItemResponse;
				iwyzeErrorMessages(object);
			}
			Synlog.info("searchCaseItem end");
		}
		catch (HttpException e) {
			Synlog.error(SEARCH_CASE_ITEM_MESSAGE + e.getMessage());
			throw new HttpException(SEARCH_CASE_ITEM_MESSAGE + e.getMessage(), 400);
		}
		catch (Exception e) {
			Synlog.error(SEARCH_CASE_ITEM_MESSAGE + e.getMessage());
			throw new HttpException(SEARCH_CASE_ITEM_MESSAGE + e.getMessage(), 500);
		}
		return response;
	}
}
