
package za.co.ominsure.synapse.iwyze.lob.backend.exists.policy.vo;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for TIA.OBJ_POLICY_DETAILS complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TIA.OBJ_POLICY_DETAILS">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="VERSION_TOKEN" type="{http://xmlns.oracle.com/pcbpel/adapter/db/sp/checkExistingPolicyReference}TIA.OBJ_VERSION_TOKEN" minOccurs="0"/>
 *         &lt;element name="COUNTRY_SPECIFICS" type="{http://xmlns.oracle.com/pcbpel/adapter/db/sp/checkExistingPolicyReference}TIA.OBJ_COUNTRY_SPECIFICS" minOccurs="0"/>
 *         &lt;element name="SITE_SPECIFICS" type="{http://xmlns.oracle.com/pcbpel/adapter/db/sp/checkExistingPolicyReference}TIA.OBJ_SITE_SPECIFICS" minOccurs="0"/>
 *         &lt;element name="FLEX_ATTRIBUTES" type="{http://xmlns.oracle.com/pcbpel/adapter/db/sp/checkExistingPolicyReference}TIA.TAB_ATTRIBUTE" minOccurs="0"/>
 *         &lt;element name="EXTERNAL_ID" type="{http://xmlns.oracle.com/pcbpel/adapter/db/sp/checkExistingPolicyReference}string100" minOccurs="0"/>
 *         &lt;element name="POLICY_HOLDER_ID" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="POLICY_NO" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="PROD_ID" type="{http://xmlns.oracle.com/pcbpel/adapter/db/sp/checkExistingPolicyReference}string100" minOccurs="0"/>
 *         &lt;element name="POLICY_STATUS" type="{http://xmlns.oracle.com/pcbpel/adapter/db/sp/checkExistingPolicyReference}string100" minOccurs="0"/>
 *         &lt;element name="COVER_START_DATE" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="COVER_END_DATE" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="CANCEL_CODE" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="CANCEL_DATE" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TIA.OBJ_POLICY_DETAILS", propOrder = {
    "versiontoken",
    "countryspecifics",
    "sitespecifics",
    "flexattributes",
    "externalid",
    "policyholderid",
    "policyno",
    "prodid",
    "policystatus",
    "coverstartdate",
    "coverenddate",
    "cancelcode",
    "canceldate"
})
public class TIAOBJPOLICYDETAILS {

    @XmlElement(name = "VERSION_TOKEN", nillable = true)
    protected TIAOBJVERSIONTOKEN versiontoken;
    @XmlElement(name = "COUNTRY_SPECIFICS", nillable = true)
    protected TIAOBJCOUNTRYSPECIFICS countryspecifics;
    @XmlElement(name = "SITE_SPECIFICS", nillable = true)
    protected TIAOBJSITESPECIFICS sitespecifics;
    @XmlElement(name = "FLEX_ATTRIBUTES", nillable = true)
    protected TIATABATTRIBUTE flexattributes;
    @XmlElement(name = "EXTERNAL_ID", nillable = true)
    protected String externalid;
    @XmlElement(name = "POLICY_HOLDER_ID", nillable = true)
    protected BigDecimal policyholderid;
    @XmlElement(name = "POLICY_NO", nillable = true)
    protected BigDecimal policyno;
    @XmlElement(name = "PROD_ID", nillable = true)
    protected String prodid;
    @XmlElement(name = "POLICY_STATUS", nillable = true)
    protected String policystatus;
    @XmlElement(name = "COVER_START_DATE", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar coverstartdate;
    @XmlElement(name = "COVER_END_DATE", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar coverenddate;
    @XmlElement(name = "CANCEL_CODE", nillable = true)
    protected Integer cancelcode;
    @XmlElement(name = "CANCEL_DATE", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar canceldate;

    /**
     * Gets the value of the versiontoken property.
     * 
     * @return
     *     possible object is
     *     {@link TIAOBJVERSIONTOKEN }
     *     
     */
    public TIAOBJVERSIONTOKEN getVERSIONTOKEN() {
        return versiontoken;
    }

    /**
     * Sets the value of the versiontoken property.
     * 
     * @param value
     *     allowed object is
     *     {@link TIAOBJVERSIONTOKEN }
     *     
     */
    public void setVERSIONTOKEN(TIAOBJVERSIONTOKEN value) {
        this.versiontoken = value;
    }

    /**
     * Gets the value of the countryspecifics property.
     * 
     * @return
     *     possible object is
     *     {@link TIAOBJCOUNTRYSPECIFICS }
     *     
     */
    public TIAOBJCOUNTRYSPECIFICS getCOUNTRYSPECIFICS() {
        return countryspecifics;
    }

    /**
     * Sets the value of the countryspecifics property.
     * 
     * @param value
     *     allowed object is
     *     {@link TIAOBJCOUNTRYSPECIFICS }
     *     
     */
    public void setCOUNTRYSPECIFICS(TIAOBJCOUNTRYSPECIFICS value) {
        this.countryspecifics = value;
    }

    /**
     * Gets the value of the sitespecifics property.
     * 
     * @return
     *     possible object is
     *     {@link TIAOBJSITESPECIFICS }
     *     
     */
    public TIAOBJSITESPECIFICS getSITESPECIFICS() {
        return sitespecifics;
    }

    /**
     * Sets the value of the sitespecifics property.
     * 
     * @param value
     *     allowed object is
     *     {@link TIAOBJSITESPECIFICS }
     *     
     */
    public void setSITESPECIFICS(TIAOBJSITESPECIFICS value) {
        this.sitespecifics = value;
    }

    /**
     * Gets the value of the flexattributes property.
     * 
     * @return
     *     possible object is
     *     {@link TIATABATTRIBUTE }
     *     
     */
    public TIATABATTRIBUTE getFLEXATTRIBUTES() {
        return flexattributes;
    }

    /**
     * Sets the value of the flexattributes property.
     * 
     * @param value
     *     allowed object is
     *     {@link TIATABATTRIBUTE }
     *     
     */
    public void setFLEXATTRIBUTES(TIATABATTRIBUTE value) {
        this.flexattributes = value;
    }

    /**
     * Gets the value of the externalid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEXTERNALID() {
        return externalid;
    }

    /**
     * Sets the value of the externalid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEXTERNALID(String value) {
        this.externalid = value;
    }

    /**
     * Gets the value of the policyholderid property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPOLICYHOLDERID() {
        return policyholderid;
    }

    /**
     * Sets the value of the policyholderid property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPOLICYHOLDERID(BigDecimal value) {
        this.policyholderid = value;
    }

    /**
     * Gets the value of the policyno property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPOLICYNO() {
        return policyno;
    }

    /**
     * Sets the value of the policyno property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPOLICYNO(BigDecimal value) {
        this.policyno = value;
    }

    /**
     * Gets the value of the prodid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPRODID() {
        return prodid;
    }

    /**
     * Sets the value of the prodid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPRODID(String value) {
        this.prodid = value;
    }

    /**
     * Gets the value of the policystatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPOLICYSTATUS() {
        return policystatus;
    }

    /**
     * Sets the value of the policystatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPOLICYSTATUS(String value) {
        this.policystatus = value;
    }

    /**
     * Gets the value of the coverstartdate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCOVERSTARTDATE() {
        return coverstartdate;
    }

    /**
     * Sets the value of the coverstartdate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCOVERSTARTDATE(XMLGregorianCalendar value) {
        this.coverstartdate = value;
    }

    /**
     * Gets the value of the coverenddate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCOVERENDDATE() {
        return coverenddate;
    }

    /**
     * Sets the value of the coverenddate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCOVERENDDATE(XMLGregorianCalendar value) {
        this.coverenddate = value;
    }

    /**
     * Gets the value of the cancelcode property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public Integer getCANCELCODE() {
        return cancelcode;
    }

    /**
     * Sets the value of the cancelcode property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCANCELCODE(Integer value) {
        this.cancelcode = value;
    }

    /**
     * Gets the value of the canceldate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCANCELDATE() {
        return canceldate;
    }

    /**
     * Sets the value of the canceldate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCANCELDATE(XMLGregorianCalendar value) {
        this.canceldate = value;
    }

}
