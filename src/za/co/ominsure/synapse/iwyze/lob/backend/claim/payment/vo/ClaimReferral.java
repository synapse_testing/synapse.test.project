
package za.co.ominsure.synapse.iwyze.lob.backend.claim.payment.vo;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Additional comments
 * 
 * <p>Java class for ClaimReferral complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ClaimReferral">
 *   &lt;complexContent>
 *     &lt;extension base="{http://infrastructure.tia.dk/schema/common/v2/}TIAObject">
 *       &lt;sequence>
 *         &lt;element name="referralNo" type="{http://infrastructure.tia.dk/schema/common/v2/}long10" minOccurs="0"/>
 *         &lt;element name="referralSource" type="{http://infrastructure.tia.dk/schema/common/v2/}int3" minOccurs="0"/>
 *         &lt;element name="referralName" type="{http://infrastructure.tia.dk/schema/common/v2/}string120" minOccurs="0"/>
 *         &lt;element name="referralCode" type="{http://infrastructure.tia.dk/schema/common/v2/}int4" minOccurs="0"/>
 *         &lt;element name="referralAmt" type="{http://infrastructure.tia.dk/schema/common/v2/}amount" minOccurs="0"/>
 *         &lt;element name="currencyCode" type="{http://infrastructure.tia.dk/schema/common/v2/}currencyCode" minOccurs="0"/>
 *         &lt;element name="referralValue" type="{http://infrastructure.tia.dk/schema/common/v2/}string120" minOccurs="0"/>
 *         &lt;element name="referralDesc" type="{http://infrastructure.tia.dk/schema/common/v2/}string120" minOccurs="0"/>
 *         &lt;element name="raisedByUserid" type="{http://infrastructure.tia.dk/schema/common/v2/}userId" minOccurs="0"/>
 *         &lt;element name="clearedByUserid" type="{http://infrastructure.tia.dk/schema/common/v2/}userId" minOccurs="0"/>
 *         &lt;element name="clearedDate" type="{http://infrastructure.tia.dk/schema/common/v2/}timestamp" minOccurs="0"/>
 *         &lt;element name="clearCondition" type="{http://infrastructure.tia.dk/schema/common/v2/}string2000" minOccurs="0"/>
 *         &lt;element name="grantedAmt" type="{http://infrastructure.tia.dk/schema/common/v2/}amount" minOccurs="0"/>
 *         &lt;element name="status" type="{http://infrastructure.tia.dk/schema/common/v2/}numberType" minOccurs="0"/>
 *         &lt;element name="raisedDate" type="{http://infrastructure.tia.dk/schema/common/v2/}timestamp" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ClaimReferral", propOrder = {
    "referralNo",
    "referralSource",
    "referralName",
    "referralCode",
    "referralAmt",
    "currencyCode",
    "referralValue",
    "referralDesc",
    "raisedByUserid",
    "clearedByUserid",
    "clearedDate",
    "clearCondition",
    "grantedAmt",
    "status",
    "raisedDate"
})
public class ClaimReferral
    extends TIAObject
{

    @XmlElement(nillable = true)
    protected Long referralNo;
    @XmlElement(nillable = true)
    protected Long referralSource;
    @XmlElement(nillable = true)
    protected String referralName;
    @XmlElement(nillable = true)
    protected Long referralCode;
    @XmlElement(nillable = true)
    protected BigDecimal referralAmt;
    @XmlElement(nillable = true)
    protected String currencyCode;
    @XmlElement(nillable = true)
    protected String referralValue;
    @XmlElement(nillable = true)
    protected String referralDesc;
    @XmlElement(nillable = true)
    protected String raisedByUserid;
    @XmlElement(nillable = true)
    protected String clearedByUserid;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar clearedDate;
    @XmlElement(nillable = true)
    protected String clearCondition;
    @XmlElement(nillable = true)
    protected BigDecimal grantedAmt;
    @XmlElement(nillable = true)
    protected BigDecimal status;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar raisedDate;

    /**
     * Gets the value of the referralNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getReferralNo() {
        return referralNo;
    }

    /**
     * Sets the value of the referralNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setReferralNo(Long value) {
        this.referralNo = value;
    }

    /**
     * Gets the value of the referralSource property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getReferralSource() {
        return referralSource;
    }

    /**
     * Sets the value of the referralSource property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setReferralSource(Long value) {
        this.referralSource = value;
    }

    /**
     * Gets the value of the referralName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReferralName() {
        return referralName;
    }

    /**
     * Sets the value of the referralName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReferralName(String value) {
        this.referralName = value;
    }

    /**
     * Gets the value of the referralCode property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getReferralCode() {
        return referralCode;
    }

    /**
     * Sets the value of the referralCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setReferralCode(Long value) {
        this.referralCode = value;
    }

    /**
     * Gets the value of the referralAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getReferralAmt() {
        return referralAmt;
    }

    /**
     * Sets the value of the referralAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setReferralAmt(BigDecimal value) {
        this.referralAmt = value;
    }

    /**
     * Gets the value of the currencyCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrencyCode() {
        return currencyCode;
    }

    /**
     * Sets the value of the currencyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrencyCode(String value) {
        this.currencyCode = value;
    }

    /**
     * Gets the value of the referralValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReferralValue() {
        return referralValue;
    }

    /**
     * Sets the value of the referralValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReferralValue(String value) {
        this.referralValue = value;
    }

    /**
     * Gets the value of the referralDesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReferralDesc() {
        return referralDesc;
    }

    /**
     * Sets the value of the referralDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReferralDesc(String value) {
        this.referralDesc = value;
    }

    /**
     * Gets the value of the raisedByUserid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRaisedByUserid() {
        return raisedByUserid;
    }

    /**
     * Sets the value of the raisedByUserid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRaisedByUserid(String value) {
        this.raisedByUserid = value;
    }

    /**
     * Gets the value of the clearedByUserid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClearedByUserid() {
        return clearedByUserid;
    }

    /**
     * Sets the value of the clearedByUserid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClearedByUserid(String value) {
        this.clearedByUserid = value;
    }

    /**
     * Gets the value of the clearedDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getClearedDate() {
        return clearedDate;
    }

    /**
     * Sets the value of the clearedDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setClearedDate(XMLGregorianCalendar value) {
        this.clearedDate = value;
    }

    /**
     * Gets the value of the clearCondition property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClearCondition() {
        return clearCondition;
    }

    /**
     * Sets the value of the clearCondition property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClearCondition(String value) {
        this.clearCondition = value;
    }

    /**
     * Gets the value of the grantedAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getGrantedAmt() {
        return grantedAmt;
    }

    /**
     * Sets the value of the grantedAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setGrantedAmt(BigDecimal value) {
        this.grantedAmt = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setStatus(BigDecimal value) {
        this.status = value;
    }

    /**
     * Gets the value of the raisedDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getRaisedDate() {
        return raisedDate;
    }

    /**
     * Sets the value of the raisedDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setRaisedDate(XMLGregorianCalendar value) {
        this.raisedDate = value;
    }

}
