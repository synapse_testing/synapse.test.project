
package za.co.ominsure.synapse.iwyze.lob.backend.policy.vo;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Is mapped into ???".
 * 
 * <p>Java class for PendingPolicyTransShort complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PendingPolicyTransShort">
 *   &lt;complexContent>
 *     &lt;extension base="{http://infrastructure.tia.dk/schema/common/v2/}TIAObject">
 *       &lt;sequence>
 *         &lt;element name="transactionNo" type="{http://infrastructure.tia.dk/schema/policy/v2/}seqNo"/>
 *         &lt;element name="coverStartDate" type="{http://infrastructure.tia.dk/schema/common/v2/}date"/>
 *         &lt;element name="transactionType" type="{http://infrastructure.tia.dk/schema/policy/v2/}transactionType"/>
 *         &lt;element name="transactionStatus" type="{http://infrastructure.tia.dk/schema/policy/v2/}transactionStatus"/>
 *         &lt;element name="description" type="{http://infrastructure.tia.dk/schema/policy/v2/}description"/>
 *         &lt;element name="transactionOwner" type="{http://infrastructure.tia.dk/schema/policy/v2/}owner"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PendingPolicyTransShort", propOrder = {
    "transactionNo",
    "coverStartDate",
    "transactionType",
    "transactionStatus",
    "description",
    "transactionOwner"
})
public class PendingPolicyTransShort
    extends TIAObject
{

    @XmlElement(required = true, nillable = true)
    @XmlSchemaType(name = "unsignedLong")
    protected BigInteger transactionNo;
    @XmlElement(required = true, nillable = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar coverStartDate;
    @XmlElement(required = true, nillable = true)
    protected String transactionType;
    @XmlElement(required = true, type = Long.class, nillable = true)
    @XmlSchemaType(name = "unsignedInt")
    protected Long transactionStatus;
    @XmlElement(required = true, nillable = true)
    protected String description;
    @XmlElement(required = true, nillable = true)
    protected String transactionOwner;

    /**
     * Gets the value of the transactionNo property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getTransactionNo() {
        return transactionNo;
    }

    /**
     * Sets the value of the transactionNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setTransactionNo(BigInteger value) {
        this.transactionNo = value;
    }

    /**
     * Gets the value of the coverStartDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCoverStartDate() {
        return coverStartDate;
    }

    /**
     * Sets the value of the coverStartDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCoverStartDate(XMLGregorianCalendar value) {
        this.coverStartDate = value;
    }

    /**
     * Gets the value of the transactionType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionType() {
        return transactionType;
    }

    /**
     * Sets the value of the transactionType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionType(String value) {
        this.transactionType = value;
    }

    /**
     * Gets the value of the transactionStatus property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getTransactionStatus() {
        return transactionStatus;
    }

    /**
     * Sets the value of the transactionStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setTransactionStatus(Long value) {
        this.transactionStatus = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the transactionOwner property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionOwner() {
        return transactionOwner;
    }

    /**
     * Sets the value of the transactionOwner property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionOwner(String value) {
        this.transactionOwner = value;
    }

}
