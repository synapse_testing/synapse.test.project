
package za.co.ominsure.synapse.iwyze.lob.backend.policy.vo;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for UIConfigObject complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="UIConfigObject">
 *   &lt;complexContent>
 *     &lt;extension base="{http://infrastructure.tia.dk/schema/common/v2/}TIAObject">
 *       &lt;sequence>
 *         &lt;element name="columnName" type="{http://infrastructure.tia.dk/schema/common/v2/}string10" minOccurs="0"/>
 *         &lt;element name="itemName" type="{http://infrastructure.tia.dk/schema/common/v2/}string30" minOccurs="0"/>
 *         &lt;element name="dataType" type="{http://infrastructure.tia.dk/schema/common/v2/}string3" minOccurs="0"/>
 *         &lt;element name="itemLength" type="{http://infrastructure.tia.dk/schema/common/v2/}numberType" minOccurs="0"/>
 *         &lt;element name="description" type="{http://infrastructure.tia.dk/schema/common/v2/}string60" minOccurs="0"/>
 *         &lt;element name="width" type="{http://infrastructure.tia.dk/schema/common/v2/}string10" minOccurs="0"/>
 *         &lt;element name="height" type="{http://infrastructure.tia.dk/schema/common/v2/}numberType" minOccurs="0"/>
 *         &lt;element name="displayCondition" type="{http://infrastructure.tia.dk/schema/common/v2/}string2000" minOccurs="0"/>
 *         &lt;element name="lovReference" type="{http://infrastructure.tia.dk/schema/common/v2/}string30" minOccurs="0"/>
 *         &lt;element name="referenceName" type="{http://infrastructure.tia.dk/schema/common/v2/}string30" minOccurs="0"/>
 *         &lt;element name="sortNo" type="{http://infrastructure.tia.dk/schema/common/v2/}numberType" minOccurs="0"/>
 *         &lt;element name="defaultValueType" type="{http://infrastructure.tia.dk/schema/common/v2/}string10" minOccurs="0"/>
 *         &lt;element name="defaultValue" type="{http://infrastructure.tia.dk/schema/common/v2/}string2000" minOccurs="0"/>
 *         &lt;element name="mandatory" type="{http://infrastructure.tia.dk/schema/common/v2/}tiaYN" minOccurs="0"/>
 *         &lt;element name="readOnly" type="{http://infrastructure.tia.dk/schema/common/v2/}tiaYN" minOccurs="0"/>
 *         &lt;element name="ruleCode" type="{http://infrastructure.tia.dk/schema/common/v2/}string30" minOccurs="0"/>
 *         &lt;element name="configFunc" type="{http://infrastructure.tia.dk/schema/common/v2/}string30" minOccurs="0"/>
 *         &lt;element name="fieldType" type="{http://infrastructure.tia.dk/schema/common/v2/}string10" minOccurs="0"/>
 *         &lt;element name="labelTranslationCollection" type="{http://infrastructure.tia.dk/schema/common/v2/}ValueTranslationPolicyCollection" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UIConfigObject", namespace = "http://infrastructure.tia.dk/schema/policy/v4/", propOrder = {
    "columnName",
    "itemName",
    "dataType",
    "itemLength",
    "description",
    "width",
    "height",
    "displayCondition",
    "lovReference",
    "referenceName",
    "sortNo",
    "defaultValueType",
    "defaultValue",
    "mandatory",
    "readOnly",
    "ruleCode",
    "configFunc",
    "fieldType",
    "labelTranslationCollection"
})
public class UIConfigObject
    extends TIAObject
{

    @XmlElement(nillable = true)
    protected String columnName;
    @XmlElement(nillable = true)
    protected String itemName;
    @XmlElement(nillable = true)
    protected String dataType;
    @XmlElement(nillable = true)
    protected BigDecimal itemLength;
    @XmlElement(nillable = true)
    protected String description;
    @XmlElement(nillable = true)
    protected String width;
    @XmlElement(nillable = true)
    protected BigDecimal height;
    @XmlElement(nillable = true)
    protected String displayCondition;
    @XmlElement(nillable = true)
    protected String lovReference;
    @XmlElement(nillable = true)
    protected String referenceName;
    @XmlElement(nillable = true)
    protected BigDecimal sortNo;
    @XmlElement(nillable = true)
    protected String defaultValueType;
    @XmlElement(nillable = true)
    protected String defaultValue;
    @XmlElement(nillable = true)
    protected String mandatory;
    @XmlElement(nillable = true)
    protected String readOnly;
    @XmlElement(nillable = true)
    protected String ruleCode;
    @XmlElement(nillable = true)
    protected String configFunc;
    @XmlElement(nillable = true)
    protected String fieldType;
    protected ValueTranslationPolicyCollection labelTranslationCollection;

    /**
     * Gets the value of the columnName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getColumnName() {
        return columnName;
    }

    /**
     * Sets the value of the columnName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setColumnName(String value) {
        this.columnName = value;
    }

    /**
     * Gets the value of the itemName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItemName() {
        return itemName;
    }

    /**
     * Sets the value of the itemName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItemName(String value) {
        this.itemName = value;
    }

    /**
     * Gets the value of the dataType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataType() {
        return dataType;
    }

    /**
     * Sets the value of the dataType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataType(String value) {
        this.dataType = value;
    }

    /**
     * Gets the value of the itemLength property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getItemLength() {
        return itemLength;
    }

    /**
     * Sets the value of the itemLength property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setItemLength(BigDecimal value) {
        this.itemLength = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the width property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWidth() {
        return width;
    }

    /**
     * Sets the value of the width property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWidth(String value) {
        this.width = value;
    }

    /**
     * Gets the value of the height property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getHeight() {
        return height;
    }

    /**
     * Sets the value of the height property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setHeight(BigDecimal value) {
        this.height = value;
    }

    /**
     * Gets the value of the displayCondition property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDisplayCondition() {
        return displayCondition;
    }

    /**
     * Sets the value of the displayCondition property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDisplayCondition(String value) {
        this.displayCondition = value;
    }

    /**
     * Gets the value of the lovReference property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLovReference() {
        return lovReference;
    }

    /**
     * Sets the value of the lovReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLovReference(String value) {
        this.lovReference = value;
    }

    /**
     * Gets the value of the referenceName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReferenceName() {
        return referenceName;
    }

    /**
     * Sets the value of the referenceName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReferenceName(String value) {
        this.referenceName = value;
    }

    /**
     * Gets the value of the sortNo property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSortNo() {
        return sortNo;
    }

    /**
     * Sets the value of the sortNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSortNo(BigDecimal value) {
        this.sortNo = value;
    }

    /**
     * Gets the value of the defaultValueType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefaultValueType() {
        return defaultValueType;
    }

    /**
     * Sets the value of the defaultValueType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefaultValueType(String value) {
        this.defaultValueType = value;
    }

    /**
     * Gets the value of the defaultValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefaultValue() {
        return defaultValue;
    }

    /**
     * Sets the value of the defaultValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefaultValue(String value) {
        this.defaultValue = value;
    }

    /**
     * Gets the value of the mandatory property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMandatory() {
        return mandatory;
    }

    /**
     * Sets the value of the mandatory property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMandatory(String value) {
        this.mandatory = value;
    }

    /**
     * Gets the value of the readOnly property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReadOnly() {
        return readOnly;
    }

    /**
     * Sets the value of the readOnly property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReadOnly(String value) {
        this.readOnly = value;
    }

    /**
     * Gets the value of the ruleCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRuleCode() {
        return ruleCode;
    }

    /**
     * Sets the value of the ruleCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRuleCode(String value) {
        this.ruleCode = value;
    }

    /**
     * Gets the value of the configFunc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConfigFunc() {
        return configFunc;
    }

    /**
     * Sets the value of the configFunc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConfigFunc(String value) {
        this.configFunc = value;
    }

    /**
     * Gets the value of the fieldType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFieldType() {
        return fieldType;
    }

    /**
     * Sets the value of the fieldType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFieldType(String value) {
        this.fieldType = value;
    }

    /**
     * Gets the value of the labelTranslationCollection property.
     * 
     * @return
     *     possible object is
     *     {@link ValueTranslationPolicyCollection }
     *     
     */
    public ValueTranslationPolicyCollection getLabelTranslationCollection() {
        return labelTranslationCollection;
    }

    /**
     * Sets the value of the labelTranslationCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link ValueTranslationPolicyCollection }
     *     
     */
    public void setLabelTranslationCollection(ValueTranslationPolicyCollection value) {
        this.labelTranslationCollection = value;
    }

}
