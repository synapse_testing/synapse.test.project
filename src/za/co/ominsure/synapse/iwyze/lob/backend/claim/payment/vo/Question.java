
package za.co.ominsure.synapse.iwyze.lob.backend.claim.payment.vo;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Additional comments
 * 
 * <p>Java class for Question complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Question">
 *   &lt;complexContent>
 *     &lt;extension base="{http://infrastructure.tia.dk/schema/common/v2/}TIAObject">
 *       &lt;sequence>
 *         &lt;element name="sortNo" type="{http://infrastructure.tia.dk/schema/common/v2/}decimal38Point0" minOccurs="0"/>
 *         &lt;element name="questionCode" type="{http://infrastructure.tia.dk/schema/common/v2/}string10" minOccurs="0"/>
 *         &lt;element name="dataType" type="{http://infrastructure.tia.dk/schema/common/v2/}string30" minOccurs="0"/>
 *         &lt;element name="mandatory" type="{http://infrastructure.tia.dk/schema/common/v2/}string1" minOccurs="0"/>
 *         &lt;element name="lovTable" type="{http://infrastructure.tia.dk/schema/common/v2/}string30" minOccurs="0"/>
 *         &lt;element name="defaultValue" type="{http://infrastructure.tia.dk/schema/common/v2/}string30" minOccurs="0"/>
 *         &lt;element name="skipCondition" type="{http://infrastructure.tia.dk/schema/common/v2/}string2000" minOccurs="0"/>
 *         &lt;element name="skipTo" type="{http://infrastructure.tia.dk/schema/common/v2/}long10" minOccurs="0"/>
 *         &lt;element name="columnName" type="{http://infrastructure.tia.dk/schema/common/v2/}string61" minOccurs="0"/>
 *         &lt;element name="userFunc" type="{http://infrastructure.tia.dk/schema/common/v2/}string1" minOccurs="0"/>
 *         &lt;element name="caseRestriction" type="{http://infrastructure.tia.dk/schema/common/v2/}string3" minOccurs="0"/>
 *         &lt;element name="valueTranslationCollection" type="{http://infrastructure.tia.dk/schema/common/v2/}ValueTranslationCollection" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Question", propOrder = {
    "sortNo",
    "questionCode",
    "dataType",
    "mandatory",
    "lovTable",
    "defaultValue",
    "skipCondition",
    "skipTo",
    "columnName",
    "userFunc",
    "caseRestriction",
    "valueTranslationCollection"
})
public class Question
    extends TIAObject
{

    @XmlElement(nillable = true)
    protected BigDecimal sortNo;
    @XmlElement(nillable = true)
    protected String questionCode;
    @XmlElement(nillable = true)
    protected String dataType;
    @XmlElement(nillable = true)
    protected String mandatory;
    @XmlElement(nillable = true)
    protected String lovTable;
    @XmlElement(nillable = true)
    protected String defaultValue;
    @XmlElement(nillable = true)
    protected String skipCondition;
    @XmlElement(nillable = true)
    protected Long skipTo;
    @XmlElement(nillable = true)
    protected String columnName;
    @XmlElement(nillable = true)
    protected String userFunc;
    @XmlElement(nillable = true)
    protected String caseRestriction;
    protected ValueTranslationCollection2 valueTranslationCollection;

    /**
     * Gets the value of the sortNo property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSortNo() {
        return sortNo;
    }

    /**
     * Sets the value of the sortNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSortNo(BigDecimal value) {
        this.sortNo = value;
    }

    /**
     * Gets the value of the questionCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQuestionCode() {
        return questionCode;
    }

    /**
     * Sets the value of the questionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQuestionCode(String value) {
        this.questionCode = value;
    }

    /**
     * Gets the value of the dataType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataType() {
        return dataType;
    }

    /**
     * Sets the value of the dataType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataType(String value) {
        this.dataType = value;
    }

    /**
     * Gets the value of the mandatory property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMandatory() {
        return mandatory;
    }

    /**
     * Sets the value of the mandatory property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMandatory(String value) {
        this.mandatory = value;
    }

    /**
     * Gets the value of the lovTable property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLovTable() {
        return lovTable;
    }

    /**
     * Sets the value of the lovTable property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLovTable(String value) {
        this.lovTable = value;
    }

    /**
     * Gets the value of the defaultValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefaultValue() {
        return defaultValue;
    }

    /**
     * Sets the value of the defaultValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefaultValue(String value) {
        this.defaultValue = value;
    }

    /**
     * Gets the value of the skipCondition property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSkipCondition() {
        return skipCondition;
    }

    /**
     * Sets the value of the skipCondition property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSkipCondition(String value) {
        this.skipCondition = value;
    }

    /**
     * Gets the value of the skipTo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getSkipTo() {
        return skipTo;
    }

    /**
     * Sets the value of the skipTo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setSkipTo(Long value) {
        this.skipTo = value;
    }

    /**
     * Gets the value of the columnName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getColumnName() {
        return columnName;
    }

    /**
     * Sets the value of the columnName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setColumnName(String value) {
        this.columnName = value;
    }

    /**
     * Gets the value of the userFunc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserFunc() {
        return userFunc;
    }

    /**
     * Sets the value of the userFunc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserFunc(String value) {
        this.userFunc = value;
    }

    /**
     * Gets the value of the caseRestriction property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCaseRestriction() {
        return caseRestriction;
    }

    /**
     * Sets the value of the caseRestriction property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCaseRestriction(String value) {
        this.caseRestriction = value;
    }

    /**
     * Gets the value of the valueTranslationCollection property.
     * 
     * @return
     *     possible object is
     *     {@link ValueTranslationCollection2 }
     *     
     */
    public ValueTranslationCollection2 getValueTranslationCollection() {
        return valueTranslationCollection;
    }

    /**
     * Sets the value of the valueTranslationCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link ValueTranslationCollection2 }
     *     
     */
    public void setValueTranslationCollection(ValueTranslationCollection2 value) {
        this.valueTranslationCollection = value;
    }

}
