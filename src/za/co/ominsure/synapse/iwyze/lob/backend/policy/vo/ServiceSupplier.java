
package za.co.ominsure.synapse.iwyze.lob.backend.policy.vo;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Additional comments
 * 
 * <p>Java class for ServiceSupplier complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ServiceSupplier">
 *   &lt;complexContent>
 *     &lt;extension base="{http://infrastructure.tia.dk/schema/party/v2/}PartyRole">
 *       &lt;sequence>
 *         &lt;element name="serviceSupplierType" type="{http://infrastructure.tia.dk/schema/common/v2/}string3" minOccurs="0"/>
 *         &lt;element name="alias" type="{http://infrastructure.tia.dk/schema/common/v2/}string10" minOccurs="0"/>
 *         &lt;element name="description" type="{http://infrastructure.tia.dk/schema/common/v2/}string2000" minOccurs="0"/>
 *         &lt;element name="specialityCollection" type="{http://infrastructure.tia.dk/schema/party/v2/}SpecialityCollection" minOccurs="0"/>
 *         &lt;element name="workAreaCollection" type="{http://infrastructure.tia.dk/schema/party/v2/}WorkAreaCollection" minOccurs="0"/>
 *         &lt;element name="ssuContactPerson" type="{http://infrastructure.tia.dk/schema/common/v2/}string32" minOccurs="0"/>
 *         &lt;element name="startDate" type="{http://infrastructure.tia.dk/schema/common/v2/}date" minOccurs="0"/>
 *         &lt;element name="endDate" type="{http://infrastructure.tia.dk/schema/common/v2/}date" minOccurs="0"/>
 *         &lt;element name="blacklisted" type="{http://infrastructure.tia.dk/schema/common/v2/}string1" minOccurs="0"/>
 *         &lt;element name="blacklistedReason" type="{http://infrastructure.tia.dk/schema/common/v2/}string2000" minOccurs="0"/>
 *         &lt;element name="priority" type="{http://infrastructure.tia.dk/schema/common/v2/}decimal38Point0" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceSupplier", namespace = "http://infrastructure.tia.dk/schema/party/v2/", propOrder = {
    "serviceSupplierType",
    "alias",
    "description",
    "specialityCollection",
    "workAreaCollection",
    "ssuContactPerson",
    "startDate",
    "endDate",
    "blacklisted",
    "blacklistedReason",
    "priority"
})
public class ServiceSupplier
    extends PartyRole
{

    @XmlElement(nillable = true)
    protected String serviceSupplierType;
    @XmlElement(nillable = true)
    protected String alias;
    @XmlElement(nillable = true)
    protected String description;
    protected SpecialityCollection specialityCollection;
    protected WorkAreaCollection workAreaCollection;
    @XmlElement(nillable = true)
    protected String ssuContactPerson;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar startDate;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar endDate;
    @XmlElement(nillable = true)
    protected String blacklisted;
    @XmlElement(nillable = true)
    protected String blacklistedReason;
    @XmlElement(nillable = true)
    protected BigDecimal priority;

    /**
     * Gets the value of the serviceSupplierType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceSupplierType() {
        return serviceSupplierType;
    }

    /**
     * Sets the value of the serviceSupplierType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceSupplierType(String value) {
        this.serviceSupplierType = value;
    }

    /**
     * Gets the value of the alias property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAlias() {
        return alias;
    }

    /**
     * Sets the value of the alias property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAlias(String value) {
        this.alias = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the specialityCollection property.
     * 
     * @return
     *     possible object is
     *     {@link SpecialityCollection }
     *     
     */
    public SpecialityCollection getSpecialityCollection() {
        return specialityCollection;
    }

    /**
     * Sets the value of the specialityCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link SpecialityCollection }
     *     
     */
    public void setSpecialityCollection(SpecialityCollection value) {
        this.specialityCollection = value;
    }

    /**
     * Gets the value of the workAreaCollection property.
     * 
     * @return
     *     possible object is
     *     {@link WorkAreaCollection }
     *     
     */
    public WorkAreaCollection getWorkAreaCollection() {
        return workAreaCollection;
    }

    /**
     * Sets the value of the workAreaCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link WorkAreaCollection }
     *     
     */
    public void setWorkAreaCollection(WorkAreaCollection value) {
        this.workAreaCollection = value;
    }

    /**
     * Gets the value of the ssuContactPerson property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSsuContactPerson() {
        return ssuContactPerson;
    }

    /**
     * Sets the value of the ssuContactPerson property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSsuContactPerson(String value) {
        this.ssuContactPerson = value;
    }

    /**
     * Gets the value of the startDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getStartDate() {
        return startDate;
    }

    /**
     * Sets the value of the startDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setStartDate(XMLGregorianCalendar value) {
        this.startDate = value;
    }

    /**
     * Gets the value of the endDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEndDate() {
        return endDate;
    }

    /**
     * Sets the value of the endDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEndDate(XMLGregorianCalendar value) {
        this.endDate = value;
    }

    /**
     * Gets the value of the blacklisted property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBlacklisted() {
        return blacklisted;
    }

    /**
     * Sets the value of the blacklisted property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBlacklisted(String value) {
        this.blacklisted = value;
    }

    /**
     * Gets the value of the blacklistedReason property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBlacklistedReason() {
        return blacklistedReason;
    }

    /**
     * Sets the value of the blacklistedReason property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBlacklistedReason(String value) {
        this.blacklistedReason = value;
    }

    /**
     * Gets the value of the priority property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPriority() {
        return priority;
    }

    /**
     * Sets the value of the priority property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPriority(BigDecimal value) {
        this.priority = value;
    }

}
