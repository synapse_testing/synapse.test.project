
package za.co.ominsure.synapse.iwyze.lob.backend.policy.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Level made in order to ensure only one collections is made".
 * 
 * <p>Java class for PartyElement complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PartyElement">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element name="person" type="{http://infrastructure.tia.dk/schema/party/v2/}Person" minOccurs="0"/>
 *         &lt;element name="institution" type="{http://infrastructure.tia.dk/schema/party/v2/}Institution" minOccurs="0"/>
 *         &lt;element name="partyOther" type="{http://infrastructure.tia.dk/schema/party/v2/}PartyOther" minOccurs="0"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PartyElement", namespace = "http://infrastructure.tia.dk/schema/party/v2/", propOrder = {
    "partyOther",
    "institution",
    "person"
})
public class PartyElement {

    @XmlElement(nillable = true)
    protected PartyOther partyOther;
    @XmlElement(nillable = true)
    protected Institution institution;
    @XmlElement(nillable = true)
    protected Person person;

    /**
     * Gets the value of the partyOther property.
     * 
     * @return
     *     possible object is
     *     {@link PartyOther }
     *     
     */
    public PartyOther getPartyOther() {
        return partyOther;
    }

    /**
     * Sets the value of the partyOther property.
     * 
     * @param value
     *     allowed object is
     *     {@link PartyOther }
     *     
     */
    public void setPartyOther(PartyOther value) {
        this.partyOther = value;
    }

    /**
     * Gets the value of the institution property.
     * 
     * @return
     *     possible object is
     *     {@link Institution }
     *     
     */
    public Institution getInstitution() {
        return institution;
    }

    /**
     * Sets the value of the institution property.
     * 
     * @param value
     *     allowed object is
     *     {@link Institution }
     *     
     */
    public void setInstitution(Institution value) {
        this.institution = value;
    }

    /**
     * Gets the value of the person property.
     * 
     * @return
     *     possible object is
     *     {@link Person }
     *     
     */
    public Person getPerson() {
        return person;
    }

    /**
     * Sets the value of the person property.
     * 
     * @param value
     *     allowed object is
     *     {@link Person }
     *     
     */
    public void setPerson(Person value) {
        this.person = value;
    }

}
