
package za.co.ominsure.synapse.iwyze.lob.backend.detail.payment.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="remindingHistoryCollection" type="{http://infrastructure.tia.dk/schema/account/v2/}RemindingHistoryCollection"/>
 *         &lt;element name="result" type="{http://infrastructure.tia.dk/schema/common/v2/}Result"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "remindingHistoryCollection",
    "result"
})
@XmlRootElement(name = "getRemindingHistoryResponse")
public class GetRemindingHistoryResponse {

    @XmlElement(required = true)
    protected RemindingHistoryCollection remindingHistoryCollection;
    @XmlElement(required = true)
    protected Result result;

    /**
     * Gets the value of the remindingHistoryCollection property.
     * 
     * @return
     *     possible object is
     *     {@link RemindingHistoryCollection }
     *     
     */
    public RemindingHistoryCollection getRemindingHistoryCollection() {
        return remindingHistoryCollection;
    }

    /**
     * Sets the value of the remindingHistoryCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link RemindingHistoryCollection }
     *     
     */
    public void setRemindingHistoryCollection(RemindingHistoryCollection value) {
        this.remindingHistoryCollection = value;
    }

    /**
     * Gets the value of the result property.
     * 
     * @return
     *     possible object is
     *     {@link Result }
     *     
     */
    public Result getResult() {
        return result;
    }

    /**
     * Sets the value of the result property.
     * 
     * @param value
     *     allowed object is
     *     {@link Result }
     *     
     */
    public void setResult(Result value) {
        this.result = value;
    }

}
