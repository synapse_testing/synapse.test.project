
package za.co.ominsure.synapse.iwyze.lob.backend.party.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * Additional comments
 * 
 * <p>Java class for SearchTokenIntParty complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SearchTokenIntParty">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="name" type="{http://infrastructure.tia.dk/schema/party/v2/}shortName" minOccurs="0"/>
 *         &lt;element name="companyReg" type="{http://infrastructure.tia.dk/schema/party/v2/}companyReg" minOccurs="0"/>
 *         &lt;element name="civilReg" type="{http://infrastructure.tia.dk/schema/party/v2/}civilReg" minOccurs="0"/>
 *         &lt;element name="ipType" type="{http://infrastructure.tia.dk/schema/common/v2/}int2" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SearchTokenIntParty", namespace = "http://infrastructure.tia.dk/schema/party/v3/", propOrder = {
    "name",
    "companyReg",
    "civilReg",
    "ipType"
})
public class SearchTokenIntParty {

    protected String name;
    protected String companyReg;
    protected String civilReg;
    protected Long ipType;

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the companyReg property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCompanyReg() {
        return companyReg;
    }

    /**
     * Sets the value of the companyReg property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCompanyReg(String value) {
        this.companyReg = value;
    }

    /**
     * Gets the value of the civilReg property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCivilReg() {
        return civilReg;
    }

    /**
     * Sets the value of the civilReg property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCivilReg(String value) {
        this.civilReg = value;
    }

    /**
     * Gets the value of the ipType property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getIpType() {
        return ipType;
    }

    /**
     * Sets the value of the ipType property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setIpType(Long value) {
        this.ipType = value;
    }

}
