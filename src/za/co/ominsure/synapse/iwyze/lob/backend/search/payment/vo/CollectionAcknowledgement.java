
package za.co.ominsure.synapse.iwyze.lob.backend.search.payment.vo;

import java.math.BigDecimal;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Is mapped into TIA metadata type "obj_collection_acknowledgement".
 * 
 * <p>Java class for CollectionAcknowledgement complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CollectionAcknowledgement">
 *   &lt;complexContent>
 *     &lt;extension base="{http://infrastructure.tia.dk/schema/common/v2/}TIAObject">
 *       &lt;sequence>
 *         &lt;element name="paymentNo" type="{http://infrastructure.tia.dk/schema/common/v2/}seqNo"/>
 *         &lt;element name="amountIn" type="{http://infrastructure.tia.dk/schema/common/v2/}amount"/>
 *         &lt;element name="valueDateIn" type="{http://infrastructure.tia.dk/schema/common/v2/}date"/>
 *         &lt;element name="paymentStatus" type="{http://infrastructure.tia.dk/schema/account/v2/}paymentStatus"/>
 *         &lt;element name="externalCodeIn" type="{http://infrastructure.tia.dk/schema/account/v2/}externalCode" minOccurs="0"/>
 *         &lt;element name="externalRefIn" type="{http://infrastructure.tia.dk/schema/account/v2/}externalRef" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CollectionAcknowledgement", propOrder = {
    "paymentNo",
    "amountIn",
    "valueDateIn",
    "paymentStatus",
    "externalCodeIn",
    "externalRefIn"
})
public class CollectionAcknowledgement
    extends TIAObject
{

    @XmlSchemaType(name = "unsignedLong")
    protected long paymentNo;
    @XmlElement(required = true)
    protected BigDecimal amountIn;
    @XmlElement(required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar valueDateIn;
    @XmlSchemaType(name = "integer")
    protected int paymentStatus;
    @XmlElementRef(name = "externalCodeIn", namespace = "http://infrastructure.tia.dk/schema/account/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> externalCodeIn;
    @XmlElementRef(name = "externalRefIn", namespace = "http://infrastructure.tia.dk/schema/account/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> externalRefIn;

    /**
     * Gets the value of the paymentNo property.
     * 
     */
    public long getPaymentNo() {
        return paymentNo;
    }

    /**
     * Sets the value of the paymentNo property.
     * 
     */
    public void setPaymentNo(long value) {
        this.paymentNo = value;
    }

    /**
     * Gets the value of the amountIn property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAmountIn() {
        return amountIn;
    }

    /**
     * Sets the value of the amountIn property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAmountIn(BigDecimal value) {
        this.amountIn = value;
    }

    /**
     * Gets the value of the valueDateIn property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getValueDateIn() {
        return valueDateIn;
    }

    /**
     * Sets the value of the valueDateIn property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setValueDateIn(XMLGregorianCalendar value) {
        this.valueDateIn = value;
    }

    /**
     * Gets the value of the paymentStatus property.
     * 
     */
    public int getPaymentStatus() {
        return paymentStatus;
    }

    /**
     * Sets the value of the paymentStatus property.
     * 
     */
    public void setPaymentStatus(int value) {
        this.paymentStatus = value;
    }

    /**
     * Gets the value of the externalCodeIn property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getExternalCodeIn() {
        return externalCodeIn;
    }

    /**
     * Sets the value of the externalCodeIn property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setExternalCodeIn(JAXBElement<String> value) {
        this.externalCodeIn = value;
    }

    /**
     * Gets the value of the externalRefIn property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getExternalRefIn() {
        return externalRefIn;
    }

    /**
     * Sets the value of the externalRefIn property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setExternalRefIn(JAXBElement<String> value) {
        this.externalRefIn = value;
    }

}
