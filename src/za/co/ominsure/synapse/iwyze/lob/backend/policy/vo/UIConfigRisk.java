
package za.co.ominsure.synapse.iwyze.lob.backend.policy.vo;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for UIConfigRisk complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="UIConfigRisk">
 *   &lt;complexContent>
 *     &lt;extension base="{http://infrastructure.tia.dk/schema/common/v2/}TIAObject">
 *       &lt;sequence>
 *         &lt;element name="riskNo" type="{http://infrastructure.tia.dk/schema/common/v2/}numberType" minOccurs="0"/>
 *         &lt;element name="selected" type="{http://infrastructure.tia.dk/schema/common/v2/}tiaYN" minOccurs="0"/>
 *         &lt;element name="expanded" type="{http://infrastructure.tia.dk/schema/common/v2/}tiaYN" minOccurs="0"/>
 *         &lt;element name="expandOnSelect" type="{http://infrastructure.tia.dk/schema/common/v2/}tiaYN" minOccurs="0"/>
 *         &lt;element name="mandatory" type="{http://infrastructure.tia.dk/schema/common/v2/}tiaYN" minOccurs="0"/>
 *         &lt;element name="uiConfigRiskItems" type="{http://infrastructure.tia.dk/schema/policy/v4/}UIConfigRiskItemCollection" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UIConfigRisk", namespace = "http://infrastructure.tia.dk/schema/policy/v4/", propOrder = {
    "riskNo",
    "selected",
    "expanded",
    "expandOnSelect",
    "mandatory",
    "uiConfigRiskItems"
})
public class UIConfigRisk
    extends TIAObject
{

    @XmlElement(nillable = true)
    protected BigDecimal riskNo;
    @XmlElement(nillable = true)
    protected String selected;
    @XmlElement(nillable = true)
    protected String expanded;
    @XmlElement(nillable = true)
    protected String expandOnSelect;
    @XmlElement(nillable = true)
    protected String mandatory;
    @XmlElement(nillable = true)
    protected UIConfigRiskItemCollection uiConfigRiskItems;

    /**
     * Gets the value of the riskNo property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRiskNo() {
        return riskNo;
    }

    /**
     * Sets the value of the riskNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRiskNo(BigDecimal value) {
        this.riskNo = value;
    }

    /**
     * Gets the value of the selected property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSelected() {
        return selected;
    }

    /**
     * Sets the value of the selected property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSelected(String value) {
        this.selected = value;
    }

    /**
     * Gets the value of the expanded property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExpanded() {
        return expanded;
    }

    /**
     * Sets the value of the expanded property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExpanded(String value) {
        this.expanded = value;
    }

    /**
     * Gets the value of the expandOnSelect property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExpandOnSelect() {
        return expandOnSelect;
    }

    /**
     * Sets the value of the expandOnSelect property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExpandOnSelect(String value) {
        this.expandOnSelect = value;
    }

    /**
     * Gets the value of the mandatory property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMandatory() {
        return mandatory;
    }

    /**
     * Sets the value of the mandatory property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMandatory(String value) {
        this.mandatory = value;
    }

    /**
     * Gets the value of the uiConfigRiskItems property.
     * 
     * @return
     *     possible object is
     *     {@link UIConfigRiskItemCollection }
     *     
     */
    public UIConfigRiskItemCollection getUiConfigRiskItems() {
        return uiConfigRiskItems;
    }

    /**
     * Sets the value of the uiConfigRiskItems property.
     * 
     * @param value
     *     allowed object is
     *     {@link UIConfigRiskItemCollection }
     *     
     */
    public void setUiConfigRiskItems(UIConfigRiskItemCollection value) {
        this.uiConfigRiskItems = value;
    }

}
