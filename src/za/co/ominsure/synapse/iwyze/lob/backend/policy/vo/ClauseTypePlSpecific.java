
package za.co.ominsure.synapse.iwyze.lob.backend.policy.vo;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Additional comments
 * 
 * <p>Java class for ClauseTypePlSpecific complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ClauseTypePlSpecific">
 *   &lt;complexContent>
 *     &lt;extension base="{http://infrastructure.tia.dk/schema/common/v2/}TIAObject">
 *       &lt;sequence>
 *         &lt;element name="clauseTypeId" type="{http://infrastructure.tia.dk/schema/common/v2/}long10" minOccurs="0"/>
 *         &lt;element name="clauseTypeVerNo" type="{http://infrastructure.tia.dk/schema/common/v2/}decimal9Point3" minOccurs="0"/>
 *         &lt;element name="title" type="{http://infrastructure.tia.dk/schema/common/v2/}string60" minOccurs="0"/>
 *         &lt;element name="riskNo" type="{http://infrastructure.tia.dk/schema/common/v2/}int3" minOccurs="0"/>
 *         &lt;element name="subriskNo" type="{http://infrastructure.tia.dk/schema/common/v2/}int3" minOccurs="0"/>
 *         &lt;element name="optionalYN" type="{http://infrastructure.tia.dk/schema/common/v2/}string1" minOccurs="0"/>
 *         &lt;element name="defaultYN" type="{http://infrastructure.tia.dk/schema/common/v2/}string1" minOccurs="0"/>
 *         &lt;element name="variables" type="{http://infrastructure.tia.dk/schema/common/v2/}KeyPairCollection" minOccurs="0"/>
 *         &lt;element name="seqNo" type="{http://infrastructure.tia.dk/schema/common/v2/}long10" minOccurs="0"/>
 *         &lt;element name="systemOnly" type="{http://infrastructure.tia.dk/schema/common/v2/}long10" minOccurs="0"/>
 *         &lt;element name="displayFuncAuth" type="{http://infrastructure.tia.dk/schema/common/v2/}int4" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ClauseTypePlSpecific", propOrder = {
    "clauseTypeId",
    "clauseTypeVerNo",
    "title",
    "riskNo",
    "subriskNo",
    "optionalYN",
    "defaultYN",
    "variables",
    "seqNo",
    "systemOnly",
    "displayFuncAuth"
})
public class ClauseTypePlSpecific
    extends TIAObject
{

    @XmlElement(nillable = true)
    protected Long clauseTypeId;
    @XmlElement(nillable = true)
    protected BigDecimal clauseTypeVerNo;
    @XmlElement(nillable = true)
    protected String title;
    @XmlElement(nillable = true)
    protected Long riskNo;
    @XmlElement(nillable = true)
    protected Long subriskNo;
    protected String optionalYN;
    protected String defaultYN;
    protected KeyPairCollection variables;
    @XmlElement(nillable = true)
    protected Long seqNo;
    @XmlElement(nillable = true)
    protected Long systemOnly;
    @XmlElement(nillable = true)
    protected Long displayFuncAuth;

    /**
     * Gets the value of the clauseTypeId property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getClauseTypeId() {
        return clauseTypeId;
    }

    /**
     * Sets the value of the clauseTypeId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setClauseTypeId(Long value) {
        this.clauseTypeId = value;
    }

    /**
     * Gets the value of the clauseTypeVerNo property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getClauseTypeVerNo() {
        return clauseTypeVerNo;
    }

    /**
     * Sets the value of the clauseTypeVerNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setClauseTypeVerNo(BigDecimal value) {
        this.clauseTypeVerNo = value;
    }

    /**
     * Gets the value of the title property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets the value of the title property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTitle(String value) {
        this.title = value;
    }

    /**
     * Gets the value of the riskNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getRiskNo() {
        return riskNo;
    }

    /**
     * Sets the value of the riskNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setRiskNo(Long value) {
        this.riskNo = value;
    }

    /**
     * Gets the value of the subriskNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getSubriskNo() {
        return subriskNo;
    }

    /**
     * Sets the value of the subriskNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setSubriskNo(Long value) {
        this.subriskNo = value;
    }

    /**
     * Gets the value of the optionalYN property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOptionalYN() {
        return optionalYN;
    }

    /**
     * Sets the value of the optionalYN property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOptionalYN(String value) {
        this.optionalYN = value;
    }

    /**
     * Gets the value of the defaultYN property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefaultYN() {
        return defaultYN;
    }

    /**
     * Sets the value of the defaultYN property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefaultYN(String value) {
        this.defaultYN = value;
    }

    /**
     * Gets the value of the variables property.
     * 
     * @return
     *     possible object is
     *     {@link KeyPairCollection }
     *     
     */
    public KeyPairCollection getVariables() {
        return variables;
    }

    /**
     * Sets the value of the variables property.
     * 
     * @param value
     *     allowed object is
     *     {@link KeyPairCollection }
     *     
     */
    public void setVariables(KeyPairCollection value) {
        this.variables = value;
    }

    /**
     * Gets the value of the seqNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getSeqNo() {
        return seqNo;
    }

    /**
     * Sets the value of the seqNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setSeqNo(Long value) {
        this.seqNo = value;
    }

    /**
     * Gets the value of the systemOnly property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getSystemOnly() {
        return systemOnly;
    }

    /**
     * Sets the value of the systemOnly property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setSystemOnly(Long value) {
        this.systemOnly = value;
    }

    /**
     * Gets the value of the displayFuncAuth property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getDisplayFuncAuth() {
        return displayFuncAuth;
    }

    /**
     * Sets the value of the displayFuncAuth property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setDisplayFuncAuth(Long value) {
        this.displayFuncAuth = value;
    }

}
