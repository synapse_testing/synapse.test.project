
package za.co.ominsure.synapse.iwyze.lob.backend.search.payment.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * Is mapped into TIA metadata type "obj_payment_terms".
 * 
 * <p>Java class for PaymentTerms complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PaymentTerms">
 *   &lt;complexContent>
 *     &lt;extension base="{http://infrastructure.tia.dk/schema/common/v2/}TIAObject">
 *       &lt;sequence>
 *         &lt;element name="paymentTerms" type="{http://infrastructure.tia.dk/schema/account/v2/}paymentTermsType"/>
 *         &lt;element name="paymentTermsName" type="{http://infrastructure.tia.dk/schema/account/v2/}paymentTermsName"/>
 *         &lt;element name="calendarOffset" type="{http://infrastructure.tia.dk/schema/account/v2/}calendarOffset"/>
 *         &lt;element name="days" type="{http://infrastructure.tia.dk/schema/account/v2/}paymentTermsDays"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PaymentTerms", propOrder = {
    "paymentTerms",
    "paymentTermsName",
    "calendarOffset",
    "days"
})
public class PaymentTerms
    extends TIAObject
{

    @XmlElement(required = true)
    protected String paymentTerms;
    @XmlElement(required = true)
    protected String paymentTermsName;
    @XmlSchemaType(name = "integer")
    protected int calendarOffset;
    @XmlSchemaType(name = "integer")
    protected int days;

    /**
     * Gets the value of the paymentTerms property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaymentTerms() {
        return paymentTerms;
    }

    /**
     * Sets the value of the paymentTerms property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaymentTerms(String value) {
        this.paymentTerms = value;
    }

    /**
     * Gets the value of the paymentTermsName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaymentTermsName() {
        return paymentTermsName;
    }

    /**
     * Sets the value of the paymentTermsName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaymentTermsName(String value) {
        this.paymentTermsName = value;
    }

    /**
     * Gets the value of the calendarOffset property.
     * 
     */
    public int getCalendarOffset() {
        return calendarOffset;
    }

    /**
     * Sets the value of the calendarOffset property.
     * 
     */
    public void setCalendarOffset(int value) {
        this.calendarOffset = value;
    }

    /**
     * Gets the value of the days property.
     * 
     */
    public int getDays() {
        return days;
    }

    /**
     * Sets the value of the days property.
     * 
     */
    public void setDays(int value) {
        this.days = value;
    }

}
