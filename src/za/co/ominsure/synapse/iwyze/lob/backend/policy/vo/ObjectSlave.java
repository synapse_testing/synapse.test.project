
package za.co.ominsure.synapse.iwyze.lob.backend.policy.vo;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * Is mapped into obj_policy_line
 * 
 * <p>Java class for ObjectSlave complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ObjectSlave">
 *   &lt;complexContent>
 *     &lt;extension base="{http://infrastructure.tia.dk/schema/common/v2/}TIAObject">
 *       &lt;sequence>
 *         &lt;element name="uixSeqNo" type="{http://infrastructure.tia.dk/schema/common/v2/}numberType" minOccurs="0"/>
 *         &lt;element name="objSlaveTypeId" type="{http://infrastructure.tia.dk/schema/common/v2/}string4000" minOccurs="0"/>
 *         &lt;element name="objectSlaveNo" type="{http://infrastructure.tia.dk/schema/common/v2/}numberType" minOccurs="0"/>
 *         &lt;element name="deleteYn" type="{http://infrastructure.tia.dk/schema/common/v2/}string1" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ObjectSlave", namespace = "http://infrastructure.tia.dk/schema/policy/v4/", propOrder = {
    "uixSeqNo",
    "objSlaveTypeId",
    "objectSlaveNo",
    "deleteYn"
})
public class ObjectSlave
    extends TIAObject
{

    protected BigDecimal uixSeqNo;
    protected String objSlaveTypeId;
    protected BigDecimal objectSlaveNo;
    protected String deleteYn;

    /**
     * Gets the value of the uixSeqNo property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getUixSeqNo() {
        return uixSeqNo;
    }

    /**
     * Sets the value of the uixSeqNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setUixSeqNo(BigDecimal value) {
        this.uixSeqNo = value;
    }

    /**
     * Gets the value of the objSlaveTypeId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getObjSlaveTypeId() {
        return objSlaveTypeId;
    }

    /**
     * Sets the value of the objSlaveTypeId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setObjSlaveTypeId(String value) {
        this.objSlaveTypeId = value;
    }

    /**
     * Gets the value of the objectSlaveNo property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getObjectSlaveNo() {
        return objectSlaveNo;
    }

    /**
     * Sets the value of the objectSlaveNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setObjectSlaveNo(BigDecimal value) {
        this.objectSlaveNo = value;
    }

    /**
     * Gets the value of the deleteYn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeleteYn() {
        return deleteYn;
    }

    /**
     * Sets the value of the deleteYn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeleteYn(String value) {
        this.deleteYn = value;
    }

}
