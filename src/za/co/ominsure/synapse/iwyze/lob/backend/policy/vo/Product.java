
package za.co.ominsure.synapse.iwyze.lob.backend.policy.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Additional comments
 * 
 * <p>Java class for Product complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Product">
 *   &lt;complexContent>
 *     &lt;extension base="{http://infrastructure.tia.dk/schema/common/v2/}TIAObject">
 *       &lt;sequence>
 *         &lt;element name="prodId" type="{http://infrastructure.tia.dk/schema/common/v2/}string2" minOccurs="0"/>
 *         &lt;element name="productClass" type="{http://infrastructure.tia.dk/schema/common/v2/}int3" minOccurs="0"/>
 *         &lt;element name="productNameTranslationCollection" type="{http://infrastructure.tia.dk/schema/common/v2/}ValueTranslationPolicyCollection" minOccurs="0"/>
 *         &lt;element name="productVersionCollection" type="{http://infrastructure.tia.dk/schema/policy/v2/}ProductVersionCollection" minOccurs="0"/>
 *         &lt;element name="sortNo" type="{http://infrastructure.tia.dk/schema/common/v2/}int3" minOccurs="0"/>
 *         &lt;element name="mainProdId" type="{http://infrastructure.tia.dk/schema/common/v2/}string2" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Product", propOrder = {
    "prodId",
    "productClass",
    "productNameTranslationCollection",
    "productVersionCollection",
    "sortNo",
    "mainProdId"
})
public class Product
    extends TIAObject
{

    @XmlElement(nillable = true)
    protected String prodId;
    @XmlElement(nillable = true)
    protected Long productClass;
    protected ValueTranslationPolicyCollection productNameTranslationCollection;
    protected ProductVersionCollection productVersionCollection;
    @XmlElement(nillable = true)
    protected Long sortNo;
    @XmlElement(nillable = true)
    protected String mainProdId;

    /**
     * Gets the value of the prodId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProdId() {
        return prodId;
    }

    /**
     * Sets the value of the prodId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProdId(String value) {
        this.prodId = value;
    }

    /**
     * Gets the value of the productClass property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getProductClass() {
        return productClass;
    }

    /**
     * Sets the value of the productClass property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setProductClass(Long value) {
        this.productClass = value;
    }

    /**
     * Gets the value of the productNameTranslationCollection property.
     * 
     * @return
     *     possible object is
     *     {@link ValueTranslationPolicyCollection }
     *     
     */
    public ValueTranslationPolicyCollection getProductNameTranslationCollection() {
        return productNameTranslationCollection;
    }

    /**
     * Sets the value of the productNameTranslationCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link ValueTranslationPolicyCollection }
     *     
     */
    public void setProductNameTranslationCollection(ValueTranslationPolicyCollection value) {
        this.productNameTranslationCollection = value;
    }

    /**
     * Gets the value of the productVersionCollection property.
     * 
     * @return
     *     possible object is
     *     {@link ProductVersionCollection }
     *     
     */
    public ProductVersionCollection getProductVersionCollection() {
        return productVersionCollection;
    }

    /**
     * Sets the value of the productVersionCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProductVersionCollection }
     *     
     */
    public void setProductVersionCollection(ProductVersionCollection value) {
        this.productVersionCollection = value;
    }

    /**
     * Gets the value of the sortNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getSortNo() {
        return sortNo;
    }

    /**
     * Sets the value of the sortNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setSortNo(Long value) {
        this.sortNo = value;
    }

    /**
     * Gets the value of the mainProdId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMainProdId() {
        return mainProdId;
    }

    /**
     * Sets the value of the mainProdId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMainProdId(String value) {
        this.mainProdId = value;
    }

}
