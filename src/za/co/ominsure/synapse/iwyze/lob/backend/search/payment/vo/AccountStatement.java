
package za.co.ominsure.synapse.iwyze.lob.backend.search.payment.vo;

import java.math.BigInteger;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Is mapped into TIA metadata type "obj_account_statement".
 * 
 * <p>Java class for AccountStatement complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AccountStatement">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="accountNo" type="{http://infrastructure.tia.dk/schema/common/v2/}seqNo"/>
 *         &lt;element name="mpPolicyNo" type="{http://infrastructure.tia.dk/schema/common/v2/}long10" minOccurs="0"/>
 *         &lt;element name="policyNo" type="{http://infrastructure.tia.dk/schema/common/v2/}policyNo" minOccurs="0"/>
 *         &lt;element name="policyLineNo" type="{http://infrastructure.tia.dk/schema/common/v2/}policyLineNo" minOccurs="0"/>
 *         &lt;element name="claimNo" type="{http://infrastructure.tia.dk/schema/common/v2/}seqNo" minOccurs="0"/>
 *         &lt;element name="fromDate" type="{http://infrastructure.tia.dk/schema/common/v2/}date" minOccurs="0"/>
 *         &lt;element name="toDate" type="{http://infrastructure.tia.dk/schema/common/v2/}date" minOccurs="0"/>
 *         &lt;element name="statementItemCollection" type="{http://infrastructure.tia.dk/schema/account/v2/}AccountStatementItemCollection" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AccountStatement", propOrder = {
    "accountNo",
    "mpPolicyNo",
    "policyNo",
    "policyLineNo",
    "claimNo",
    "fromDate",
    "toDate",
    "statementItemCollection"
})
public class AccountStatement {

    @XmlSchemaType(name = "unsignedLong")
    protected long accountNo;
    @XmlElementRef(name = "mpPolicyNo", namespace = "http://infrastructure.tia.dk/schema/account/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<Long> mpPolicyNo;
    @XmlElementRef(name = "policyNo", namespace = "http://infrastructure.tia.dk/schema/account/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<BigInteger> policyNo;
    @XmlElementRef(name = "policyLineNo", namespace = "http://infrastructure.tia.dk/schema/account/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<BigInteger> policyLineNo;
    @XmlElementRef(name = "claimNo", namespace = "http://infrastructure.tia.dk/schema/account/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<Long> claimNo;
    @XmlElementRef(name = "fromDate", namespace = "http://infrastructure.tia.dk/schema/account/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> fromDate;
    @XmlElementRef(name = "toDate", namespace = "http://infrastructure.tia.dk/schema/account/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> toDate;
    protected AccountStatementItemCollection statementItemCollection;

    /**
     * Gets the value of the accountNo property.
     * 
     */
    public long getAccountNo() {
        return accountNo;
    }

    /**
     * Sets the value of the accountNo property.
     * 
     */
    public void setAccountNo(long value) {
        this.accountNo = value;
    }

    /**
     * Gets the value of the mpPolicyNo property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Long }{@code >}
     *     
     */
    public JAXBElement<Long> getMpPolicyNo() {
        return mpPolicyNo;
    }

    /**
     * Sets the value of the mpPolicyNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Long }{@code >}
     *     
     */
    public void setMpPolicyNo(JAXBElement<Long> value) {
        this.mpPolicyNo = value;
    }

    /**
     * Gets the value of the policyNo property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
     *     
     */
    public JAXBElement<BigInteger> getPolicyNo() {
        return policyNo;
    }

    /**
     * Sets the value of the policyNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
     *     
     */
    public void setPolicyNo(JAXBElement<BigInteger> value) {
        this.policyNo = value;
    }

    /**
     * Gets the value of the policyLineNo property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
     *     
     */
    public JAXBElement<BigInteger> getPolicyLineNo() {
        return policyLineNo;
    }

    /**
     * Sets the value of the policyLineNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
     *     
     */
    public void setPolicyLineNo(JAXBElement<BigInteger> value) {
        this.policyLineNo = value;
    }

    /**
     * Gets the value of the claimNo property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Long }{@code >}
     *     
     */
    public JAXBElement<Long> getClaimNo() {
        return claimNo;
    }

    /**
     * Sets the value of the claimNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Long }{@code >}
     *     
     */
    public void setClaimNo(JAXBElement<Long> value) {
        this.claimNo = value;
    }

    /**
     * Gets the value of the fromDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getFromDate() {
        return fromDate;
    }

    /**
     * Sets the value of the fromDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setFromDate(JAXBElement<XMLGregorianCalendar> value) {
        this.fromDate = value;
    }

    /**
     * Gets the value of the toDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getToDate() {
        return toDate;
    }

    /**
     * Sets the value of the toDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setToDate(JAXBElement<XMLGregorianCalendar> value) {
        this.toDate = value;
    }

    /**
     * Gets the value of the statementItemCollection property.
     * 
     * @return
     *     possible object is
     *     {@link AccountStatementItemCollection }
     *     
     */
    public AccountStatementItemCollection getStatementItemCollection() {
        return statementItemCollection;
    }

    /**
     * Sets the value of the statementItemCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link AccountStatementItemCollection }
     *     
     */
    public void setStatementItemCollection(AccountStatementItemCollection value) {
        this.statementItemCollection = value;
    }

}
