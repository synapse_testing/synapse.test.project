
package za.co.ominsure.synapse.iwyze.lob.backend.policy.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Additional comments
 * 
 * <p>Java class for ProductVersion complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ProductVersion">
 *   &lt;complexContent>
 *     &lt;extension base="{http://infrastructure.tia.dk/schema/common/v2/}TIAObject">
 *       &lt;sequence>
 *         &lt;element name="startDate" type="{http://infrastructure.tia.dk/schema/common/v2/}date" minOccurs="0"/>
 *         &lt;element name="endDate" type="{http://infrastructure.tia.dk/schema/common/v2/}date" minOccurs="0"/>
 *         &lt;element name="guaranteeDays" type="{http://infrastructure.tia.dk/schema/common/v2/}int3" minOccurs="0"/>
 *         &lt;element name="productLineVersionCollection" type="{http://infrastructure.tia.dk/schema/policy/v2/}ProdLineVerInProductCollection" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProductVersion", propOrder = {
    "startDate",
    "endDate",
    "guaranteeDays",
    "productLineVersionCollection"
})
public class ProductVersion
    extends TIAObject
{

    @XmlElement(nillable = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar startDate;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar endDate;
    @XmlElement(nillable = true)
    protected Long guaranteeDays;
    protected ProdLineVerInProductCollection productLineVersionCollection;

    /**
     * Gets the value of the startDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getStartDate() {
        return startDate;
    }

    /**
     * Sets the value of the startDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setStartDate(XMLGregorianCalendar value) {
        this.startDate = value;
    }

    /**
     * Gets the value of the endDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEndDate() {
        return endDate;
    }

    /**
     * Sets the value of the endDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEndDate(XMLGregorianCalendar value) {
        this.endDate = value;
    }

    /**
     * Gets the value of the guaranteeDays property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getGuaranteeDays() {
        return guaranteeDays;
    }

    /**
     * Sets the value of the guaranteeDays property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setGuaranteeDays(Long value) {
        this.guaranteeDays = value;
    }

    /**
     * Gets the value of the productLineVersionCollection property.
     * 
     * @return
     *     possible object is
     *     {@link ProdLineVerInProductCollection }
     *     
     */
    public ProdLineVerInProductCollection getProductLineVersionCollection() {
        return productLineVersionCollection;
    }

    /**
     * Sets the value of the productLineVersionCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProdLineVerInProductCollection }
     *     
     */
    public void setProductLineVersionCollection(ProdLineVerInProductCollection value) {
        this.productLineVersionCollection = value;
    }

}
