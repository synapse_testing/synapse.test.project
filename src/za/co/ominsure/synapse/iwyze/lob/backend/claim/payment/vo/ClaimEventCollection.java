
package za.co.ominsure.synapse.iwyze.lob.backend.claim.payment.vo;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ClaimEventCollection complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ClaimEventCollection">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="claimEvent" type="{http://infrastructure.tia.dk/schema/claim/v2/}ClaimEvent" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ClaimEventCollection", propOrder = {
    "claimEvents"
})
public class ClaimEventCollection {

    @XmlElement(name = "claimEvent")
    protected List<ClaimEvent> claimEvents;

    /**
     * Gets the value of the claimEvents property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the claimEvents property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getClaimEvents().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ClaimEvent }
     * 
     * 
     */
    public List<ClaimEvent> getClaimEvents() {
        if (claimEvents == null) {
            claimEvents = new ArrayList<ClaimEvent>();
        }
        return this.claimEvents;
    }

}
