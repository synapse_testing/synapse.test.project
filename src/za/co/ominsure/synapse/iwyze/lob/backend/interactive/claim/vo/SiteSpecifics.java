
package za.co.ominsure.synapse.iwyze.lob.backend.interactive.claim.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SiteSpecifics complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SiteSpecifics">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="entityAttributeCollection" type="{http://infrastructure.tia.dk/schema/common/v2/}EntityAttributeCollection"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SiteSpecifics", namespace = "http://infrastructure.tia.dk/schema/common/v2/", propOrder = {
    "entityAttributeCollection"
})
public class SiteSpecifics {

    @XmlElement(required = true, nillable = true)
    protected EntityAttributeCollection entityAttributeCollection;

    /**
     * Gets the value of the entityAttributeCollection property.
     * 
     * @return
     *     possible object is
     *     {@link EntityAttributeCollection }
     *     
     */
    public EntityAttributeCollection getEntityAttributeCollection() {
        return entityAttributeCollection;
    }

    /**
     * Sets the value of the entityAttributeCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link EntityAttributeCollection }
     *     
     */
    public void setEntityAttributeCollection(EntityAttributeCollection value) {
        this.entityAttributeCollection = value;
    }

}
