
package za.co.ominsure.synapse.iwyze.lob.backend.search.payment.vo;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * Is mapped into TIA metadata type "obj_payment_channel".
 * 
 * <p>Java class for PaymentChannel complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PaymentChannel">
 *   &lt;complexContent>
 *     &lt;extension base="{http://infrastructure.tia.dk/schema/common/v2/}TIAObject">
 *       &lt;sequence>
 *         &lt;element name="paymentChannel" type="{http://infrastructure.tia.dk/schema/account/v2/}paymentChannelType"/>
 *         &lt;element name="title" type="{http://infrastructure.tia.dk/schema/account/v2/}paymentChannelTitle" minOccurs="0"/>
 *         &lt;element name="paymentMethod" type="{http://infrastructure.tia.dk/schema/common/v2/}paymentMethod"/>
 *         &lt;element name="currencyCode" type="{http://infrastructure.tia.dk/schema/common/v2/}currencyCode"/>
 *         &lt;element name="bankIdNo" type="{http://infrastructure.tia.dk/schema/account/v2/}bankIdNo" minOccurs="0"/>
 *         &lt;element name="centerCode" type="{http://infrastructure.tia.dk/schema/common/v2/}centerCode" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PaymentChannel", propOrder = {
    "paymentChannel",
    "title",
    "paymentMethod",
    "currencyCode",
    "bankIdNo",
    "centerCode"
})
public class PaymentChannel
    extends TIAObject
{

    @XmlElement(required = true)
    protected String paymentChannel;
    @XmlElementRef(name = "title", namespace = "http://infrastructure.tia.dk/schema/account/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> title;
    @XmlElement(required = true)
    protected String paymentMethod;
    @XmlElement(required = true)
    protected String currencyCode;
    @XmlElementRef(name = "bankIdNo", namespace = "http://infrastructure.tia.dk/schema/account/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<Long> bankIdNo;
    @XmlElementRef(name = "centerCode", namespace = "http://infrastructure.tia.dk/schema/account/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> centerCode;

    /**
     * Gets the value of the paymentChannel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaymentChannel() {
        return paymentChannel;
    }

    /**
     * Sets the value of the paymentChannel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaymentChannel(String value) {
        this.paymentChannel = value;
    }

    /**
     * Gets the value of the title property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTitle() {
        return title;
    }

    /**
     * Sets the value of the title property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTitle(JAXBElement<String> value) {
        this.title = value;
    }

    /**
     * Gets the value of the paymentMethod property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaymentMethod() {
        return paymentMethod;
    }

    /**
     * Sets the value of the paymentMethod property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaymentMethod(String value) {
        this.paymentMethod = value;
    }

    /**
     * Gets the value of the currencyCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrencyCode() {
        return currencyCode;
    }

    /**
     * Sets the value of the currencyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrencyCode(String value) {
        this.currencyCode = value;
    }

    /**
     * Gets the value of the bankIdNo property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Long }{@code >}
     *     
     */
    public JAXBElement<Long> getBankIdNo() {
        return bankIdNo;
    }

    /**
     * Sets the value of the bankIdNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Long }{@code >}
     *     
     */
    public void setBankIdNo(JAXBElement<Long> value) {
        this.bankIdNo = value;
    }

    /**
     * Gets the value of the centerCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCenterCode() {
        return centerCode;
    }

    /**
     * Sets the value of the centerCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCenterCode(JAXBElement<String> value) {
        this.centerCode = value;
    }

}
