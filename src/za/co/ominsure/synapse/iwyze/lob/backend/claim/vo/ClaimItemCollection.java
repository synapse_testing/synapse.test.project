
package za.co.ominsure.synapse.iwyze.lob.backend.claim.vo;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ClaimItemCollection complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ClaimItemCollection">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="claimItem" type="{http://infrastructure.tia.dk/schema/claim/v2/}ClaimItem" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ClaimItemCollection", propOrder = {
    "claimItems"
})
public class ClaimItemCollection {

    @XmlElement(name = "claimItem")
    protected List<ClaimItem> claimItems;

    /**
     * Gets the value of the claimItems property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the claimItems property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getClaimItems().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ClaimItem }
     * 
     * 
     */
    public List<ClaimItem> getClaimItems() {
        if (claimItems == null) {
            claimItems = new ArrayList<ClaimItem>();
        }
        return this.claimItems;
    }

}
