
package za.co.ominsure.synapse.iwyze.lob.backend.search.payment.vo;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * Definition for type Email
 * 
 * <p>Java class for Email complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Email">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="emailTo" type="{http://infrastructure.tia.dk/schema/common/v2/}emailAddress"/>
 *         &lt;element name="emailFrom" type="{http://infrastructure.tia.dk/schema/common/v2/}emailAddress"/>
 *         &lt;element name="emailCC" type="{http://infrastructure.tia.dk/schema/common/v2/}emailAddress" minOccurs="0"/>
 *         &lt;element name="emailBCC" type="{http://infrastructure.tia.dk/schema/common/v2/}emailAddress" minOccurs="0"/>
 *         &lt;element name="emailSubject" type="{http://infrastructure.tia.dk/schema/common/v2/}emailSubject"/>
 *         &lt;element name="emailMessage" type="{http://infrastructure.tia.dk/schema/common/v2/}emailMessage"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Email", namespace = "http://infrastructure.tia.dk/schema/common/v2/", propOrder = {
    "emailTo",
    "emailFrom",
    "emailCC",
    "emailBCC",
    "emailSubject",
    "emailMessage"
})
public class Email {

    @XmlElement(required = true, nillable = true)
    protected String emailTo;
    @XmlElement(required = true, nillable = true)
    protected String emailFrom;
    @XmlElementRef(name = "emailCC", namespace = "http://infrastructure.tia.dk/schema/common/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> emailCC;
    @XmlElementRef(name = "emailBCC", namespace = "http://infrastructure.tia.dk/schema/common/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> emailBCC;
    @XmlElement(required = true, nillable = true)
    protected String emailSubject;
    @XmlElement(required = true, nillable = true)
    protected String emailMessage;

    /**
     * Gets the value of the emailTo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmailTo() {
        return emailTo;
    }

    /**
     * Sets the value of the emailTo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmailTo(String value) {
        this.emailTo = value;
    }

    /**
     * Gets the value of the emailFrom property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmailFrom() {
        return emailFrom;
    }

    /**
     * Sets the value of the emailFrom property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmailFrom(String value) {
        this.emailFrom = value;
    }

    /**
     * Gets the value of the emailCC property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getEmailCC() {
        return emailCC;
    }

    /**
     * Sets the value of the emailCC property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setEmailCC(JAXBElement<String> value) {
        this.emailCC = value;
    }

    /**
     * Gets the value of the emailBCC property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getEmailBCC() {
        return emailBCC;
    }

    /**
     * Sets the value of the emailBCC property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setEmailBCC(JAXBElement<String> value) {
        this.emailBCC = value;
    }

    /**
     * Gets the value of the emailSubject property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmailSubject() {
        return emailSubject;
    }

    /**
     * Sets the value of the emailSubject property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmailSubject(String value) {
        this.emailSubject = value;
    }

    /**
     * Gets the value of the emailMessage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmailMessage() {
        return emailMessage;
    }

    /**
     * Sets the value of the emailMessage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmailMessage(String value) {
        this.emailMessage = value;
    }

}
