
package za.co.ominsure.synapse.iwyze.lob.backend.exists.policy.vo;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for TIA.OBJ_VERSION_TOKEN complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TIA.OBJ_VERSION_TOKEN">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TIMESTAMP" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="USERID" type="{http://xmlns.oracle.com/pcbpel/adapter/db/sp/checkExistingPolicyReference}string8" minOccurs="0"/>
 *         &lt;element name="RECORD_VERSION" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="RECORD_TIMESTAMP" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="RECORD_USERID" type="{http://xmlns.oracle.com/pcbpel/adapter/db/sp/checkExistingPolicyReference}string8" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TIA.OBJ_VERSION_TOKEN", propOrder = {
    "timestamp",
    "userid",
    "recordversion",
    "recordtimestamp",
    "recorduserid"
})
public class TIAOBJVERSIONTOKEN {

    @XmlElement(name = "TIMESTAMP", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar timestamp;
    @XmlElement(name = "USERID", nillable = true)
    protected String userid;
    @XmlElement(name = "RECORD_VERSION", nillable = true)
    protected BigDecimal recordversion;
    @XmlElement(name = "RECORD_TIMESTAMP", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar recordtimestamp;
    @XmlElement(name = "RECORD_USERID", nillable = true)
    protected String recorduserid;

    /**
     * Gets the value of the timestamp property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTIMESTAMP() {
        return timestamp;
    }

    /**
     * Sets the value of the timestamp property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTIMESTAMP(XMLGregorianCalendar value) {
        this.timestamp = value;
    }

    /**
     * Gets the value of the userid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUSERID() {
        return userid;
    }

    /**
     * Sets the value of the userid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUSERID(String value) {
        this.userid = value;
    }

    /**
     * Gets the value of the recordversion property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRECORDVERSION() {
        return recordversion;
    }

    /**
     * Sets the value of the recordversion property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRECORDVERSION(BigDecimal value) {
        this.recordversion = value;
    }

    /**
     * Gets the value of the recordtimestamp property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getRECORDTIMESTAMP() {
        return recordtimestamp;
    }

    /**
     * Sets the value of the recordtimestamp property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setRECORDTIMESTAMP(XMLGregorianCalendar value) {
        this.recordtimestamp = value;
    }

    /**
     * Gets the value of the recorduserid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRECORDUSERID() {
        return recorduserid;
    }

    /**
     * Sets the value of the recorduserid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRECORDUSERID(String value) {
        this.recorduserid = value;
    }

}
