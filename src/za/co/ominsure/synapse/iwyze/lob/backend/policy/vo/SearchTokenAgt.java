
package za.co.ominsure.synapse.iwyze.lob.backend.policy.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Additional comments
 * 
 * <p>Java class for SearchTokenAgt complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SearchTokenAgt">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="agentName" type="{http://infrastructure.tia.dk/schema/common/v2/}string32" minOccurs="0"/>
 *         &lt;element name="comGroup" type="{http://infrastructure.tia.dk/schema/common/v2/}string15" minOccurs="0"/>
 *         &lt;element name="comCategory" type="{http://infrastructure.tia.dk/schema/common/v2/}string15" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SearchTokenAgt", namespace = "http://infrastructure.tia.dk/schema/party/v2/", propOrder = {
    "agentName",
    "comGroup",
    "comCategory"
})
public class SearchTokenAgt {

    @XmlElement(nillable = true)
    protected String agentName;
    @XmlElement(nillable = true)
    protected String comGroup;
    @XmlElement(nillable = true)
    protected String comCategory;

    /**
     * Gets the value of the agentName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgentName() {
        return agentName;
    }

    /**
     * Sets the value of the agentName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgentName(String value) {
        this.agentName = value;
    }

    /**
     * Gets the value of the comGroup property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComGroup() {
        return comGroup;
    }

    /**
     * Sets the value of the comGroup property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComGroup(String value) {
        this.comGroup = value;
    }

    /**
     * Gets the value of the comCategory property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCategory() {
        return comCategory;
    }

    /**
     * Sets the value of the comCategory property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCategory(String value) {
        this.comCategory = value;
    }

}
