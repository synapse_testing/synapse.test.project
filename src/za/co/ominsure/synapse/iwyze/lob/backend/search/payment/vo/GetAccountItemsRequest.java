
package za.co.ominsure.synapse.iwyze.lob.backend.search.payment.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="inputToken" type="{http://infrastructure.tia.dk/schema/common/v2/}InputToken"/>
 *         &lt;element name="accountNo" type="{http://infrastructure.tia.dk/schema/common/v2/}seqNo" minOccurs="0"/>
 *         &lt;element name="filterToken" type="{http://infrastructure.tia.dk/schema/account/v2/}FilterTokenAccItem" minOccurs="0"/>
 *         &lt;element name="getDepth" type="{http://infrastructure.tia.dk/schema/account/v2/}getDepth" minOccurs="0"/>
 *         &lt;element name="pageSort" type="{http://infrastructure.tia.dk/schema/common/v2/}PageSort" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "inputToken",
    "accountNo",
    "filterToken",
    "getDepth",
    "pageSort"
})
@XmlRootElement(name = "getAccountItemsRequest")
public class GetAccountItemsRequest {

    @XmlElement(required = true)
    protected InputToken inputToken;
    @XmlSchemaType(name = "unsignedLong")
    protected Long accountNo;
    protected FilterTokenAccItem filterToken;
    protected String getDepth;
    protected PageSort pageSort;

    /**
     * Gets the value of the inputToken property.
     * 
     * @return
     *     possible object is
     *     {@link InputToken }
     *     
     */
    public InputToken getInputToken() {
        return inputToken;
    }

    /**
     * Sets the value of the inputToken property.
     * 
     * @param value
     *     allowed object is
     *     {@link InputToken }
     *     
     */
    public void setInputToken(InputToken value) {
        this.inputToken = value;
    }

    /**
     * Gets the value of the accountNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getAccountNo() {
        return accountNo;
    }

    /**
     * Sets the value of the accountNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setAccountNo(Long value) {
        this.accountNo = value;
    }

    /**
     * Gets the value of the filterToken property.
     * 
     * @return
     *     possible object is
     *     {@link FilterTokenAccItem }
     *     
     */
    public FilterTokenAccItem getFilterToken() {
        return filterToken;
    }

    /**
     * Sets the value of the filterToken property.
     * 
     * @param value
     *     allowed object is
     *     {@link FilterTokenAccItem }
     *     
     */
    public void setFilterToken(FilterTokenAccItem value) {
        this.filterToken = value;
    }

    /**
     * Gets the value of the getDepth property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGetDepth() {
        return getDepth;
    }

    /**
     * Sets the value of the getDepth property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGetDepth(String value) {
        this.getDepth = value;
    }

    /**
     * Gets the value of the pageSort property.
     * 
     * @return
     *     possible object is
     *     {@link PageSort }
     *     
     */
    public PageSort getPageSort() {
        return pageSort;
    }

    /**
     * Sets the value of the pageSort property.
     * 
     * @param value
     *     allowed object is
     *     {@link PageSort }
     *     
     */
    public void setPageSort(PageSort value) {
        this.pageSort = value;
    }

}
