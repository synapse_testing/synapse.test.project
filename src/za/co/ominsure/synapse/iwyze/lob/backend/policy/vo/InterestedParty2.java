
package za.co.ominsure.synapse.iwyze.lob.backend.policy.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Additional comments
 * 
 * <p>Java class for InterestedParty complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="InterestedParty">
 *   &lt;complexContent>
 *     &lt;extension base="{http://infrastructure.tia.dk/schema/party/v2/}PartyRole">
 *       &lt;sequence>
 *         &lt;element name="ipType" type="{http://infrastructure.tia.dk/schema/common/v2/}int2" minOccurs="0"/>
 *         &lt;element name="typeOther" type="{http://infrastructure.tia.dk/schema/common/v2/}string30" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InterestedParty", namespace = "http://infrastructure.tia.dk/schema/party/v2/", propOrder = {
    "ipType",
    "typeOther"
})
public class InterestedParty2
    extends PartyRole
{

    @XmlElement(nillable = true)
    protected Long ipType;
    @XmlElement(nillable = true)
    protected String typeOther;

    /**
     * Gets the value of the ipType property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getIpType() {
        return ipType;
    }

    /**
     * Sets the value of the ipType property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setIpType(Long value) {
        this.ipType = value;
    }

    /**
     * Gets the value of the typeOther property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTypeOther() {
        return typeOther;
    }

    /**
     * Sets the value of the typeOther property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTypeOther(String value) {
        this.typeOther = value;
    }

}
