
package za.co.ominsure.synapse.iwyze.lob.backend.policy.vo;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getDepth.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="getDepth">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="POLICY_ONLY"/>
 *     &lt;enumeration value="POLICY_LINES"/>
 *     &lt;enumeration value="ALL"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "getDepth")
@XmlEnum
public enum GetDepth {

    POLICY_ONLY,
    POLICY_LINES,
    ALL;

    public String value() {
        return name();
    }

    public static GetDepth fromValue(String v) {
        return valueOf(v);
    }

}
