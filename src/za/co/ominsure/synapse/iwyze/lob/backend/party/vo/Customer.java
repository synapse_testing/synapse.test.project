
package za.co.ominsure.synapse.iwyze.lob.backend.party.vo;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Is mapped into TIA metadata type "obj_customer".
 * 
 * <p>Java class for Customer complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Customer">
 *   &lt;complexContent>
 *     &lt;extension base="{http://infrastructure.tia.dk/schema/party/v2/}Customer">
 *       &lt;sequence>
 *         &lt;element name="handler" type="{http://infrastructure.tia.dk/schema/common/v2/}string8" minOccurs="0"/>
 *         &lt;element name="customerGroup" type="{http://infrastructure.tia.dk/schema/common/v2/}string3" minOccurs="0"/>
 *         &lt;element name="customerSegment" type="{http://infrastructure.tia.dk/schema/common/v2/}string3" minOccurs="0"/>
 *         &lt;element name="nextContactWay" type="{http://infrastructure.tia.dk/schema/common/v2/}string10" minOccurs="0"/>
 *         &lt;element name="nextContact" type="{http://infrastructure.tia.dk/schema/common/v2/}date" minOccurs="0"/>
 *         &lt;element name="advertising" type="{http://infrastructure.tia.dk/schema/common/v2/}string3" minOccurs="0"/>
 *         &lt;element name="advertisingDate" type="{http://infrastructure.tia.dk/schema/common/v2/}date" minOccurs="0"/>
 *         &lt;element name="agentNo" type="{http://infrastructure.tia.dk/schema/common/v2/}long10" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Customer", namespace = "http://infrastructure.tia.dk/schema/party/v3/", propOrder = {
    "handler",
    "customerGroup",
    "customerSegment",
    "nextContactWay",
    "nextContact",
    "advertising",
    "advertisingDate",
    "agentNo"
})
public class Customer
    extends Customer2
{

    @XmlElementRef(name = "handler", namespace = "http://infrastructure.tia.dk/schema/party/v3/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> handler;
    @XmlElementRef(name = "customerGroup", namespace = "http://infrastructure.tia.dk/schema/party/v3/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> customerGroup;
    @XmlElementRef(name = "customerSegment", namespace = "http://infrastructure.tia.dk/schema/party/v3/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> customerSegment;
    @XmlElementRef(name = "nextContactWay", namespace = "http://infrastructure.tia.dk/schema/party/v3/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> nextContactWay;
    @XmlElementRef(name = "nextContact", namespace = "http://infrastructure.tia.dk/schema/party/v3/", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> nextContact;
    @XmlElementRef(name = "advertising", namespace = "http://infrastructure.tia.dk/schema/party/v3/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> advertising;
    @XmlElementRef(name = "advertisingDate", namespace = "http://infrastructure.tia.dk/schema/party/v3/", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> advertisingDate;
    @XmlElementRef(name = "agentNo", namespace = "http://infrastructure.tia.dk/schema/party/v3/", type = JAXBElement.class, required = false)
    protected JAXBElement<Long> agentNo;

    /**
     * Gets the value of the handler property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getHandler() {
        return handler;
    }

    /**
     * Sets the value of the handler property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setHandler(JAXBElement<String> value) {
        this.handler = value;
    }

    /**
     * Gets the value of the customerGroup property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCustomerGroup() {
        return customerGroup;
    }

    /**
     * Sets the value of the customerGroup property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCustomerGroup(JAXBElement<String> value) {
        this.customerGroup = value;
    }

    /**
     * Gets the value of the customerSegment property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCustomerSegment() {
        return customerSegment;
    }

    /**
     * Sets the value of the customerSegment property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCustomerSegment(JAXBElement<String> value) {
        this.customerSegment = value;
    }

    /**
     * Gets the value of the nextContactWay property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNextContactWay() {
        return nextContactWay;
    }

    /**
     * Sets the value of the nextContactWay property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNextContactWay(JAXBElement<String> value) {
        this.nextContactWay = value;
    }

    /**
     * Gets the value of the nextContact property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getNextContact() {
        return nextContact;
    }

    /**
     * Sets the value of the nextContact property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setNextContact(JAXBElement<XMLGregorianCalendar> value) {
        this.nextContact = value;
    }

    /**
     * Gets the value of the advertising property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAdvertising() {
        return advertising;
    }

    /**
     * Sets the value of the advertising property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAdvertising(JAXBElement<String> value) {
        this.advertising = value;
    }

    /**
     * Gets the value of the advertisingDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getAdvertisingDate() {
        return advertisingDate;
    }

    /**
     * Sets the value of the advertisingDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setAdvertisingDate(JAXBElement<XMLGregorianCalendar> value) {
        this.advertisingDate = value;
    }

    /**
     * Gets the value of the agentNo property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Long }{@code >}
     *     
     */
    public JAXBElement<Long> getAgentNo() {
        return agentNo;
    }

    /**
     * Sets the value of the agentNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Long }{@code >}
     *     
     */
    public void setAgentNo(JAXBElement<Long> value) {
        this.agentNo = value;
    }

}
