
package za.co.ominsure.synapse.iwyze.lob.backend.policy.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Additional comments
 * 
 * <p>Java class for ObjectType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ObjectType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="objectTypeId" type="{http://infrastructure.tia.dk/schema/common/v2/}string12" minOccurs="0"/>
 *         &lt;element name="objectTypeVersionCollection" type="{http://infrastructure.tia.dk/schema/policy/v2/}ObjectTypeVersionCollection" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ObjectType", propOrder = {
    "objectTypeId",
    "objectTypeVersionCollection"
})
public class ObjectType {

    @XmlElement(nillable = true)
    protected String objectTypeId;
    protected ObjectTypeVersionCollection objectTypeVersionCollection;

    /**
     * Gets the value of the objectTypeId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getObjectTypeId() {
        return objectTypeId;
    }

    /**
     * Sets the value of the objectTypeId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setObjectTypeId(String value) {
        this.objectTypeId = value;
    }

    /**
     * Gets the value of the objectTypeVersionCollection property.
     * 
     * @return
     *     possible object is
     *     {@link ObjectTypeVersionCollection }
     *     
     */
    public ObjectTypeVersionCollection getObjectTypeVersionCollection() {
        return objectTypeVersionCollection;
    }

    /**
     * Sets the value of the objectTypeVersionCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjectTypeVersionCollection }
     *     
     */
    public void setObjectTypeVersionCollection(ObjectTypeVersionCollection value) {
        this.objectTypeVersionCollection = value;
    }

}
