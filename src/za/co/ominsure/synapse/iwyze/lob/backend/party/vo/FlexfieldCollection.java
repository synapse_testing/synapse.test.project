
package za.co.ominsure.synapse.iwyze.lob.backend.party.vo;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FlexfieldCollection complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FlexfieldCollection">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="flexfield" type="{http://infrastructure.tia.dk/schema/common/v2/}Flexfield" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FlexfieldCollection", namespace = "http://infrastructure.tia.dk/schema/common/v2/", propOrder = {
    "flexfield"
})
public class FlexfieldCollection {

    protected List<Flexfield> flexfield;

    /**
     * Gets the value of the flexfield property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the flexfield property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFlexfield().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Flexfield }
     * 
     * 
     */
    public List<Flexfield> getFlexfield() {
        if (flexfield == null) {
            flexfield = new ArrayList<>();
        }
        return this.flexfield;
    }

}
