
package za.co.ominsure.synapse.iwyze.lob.backend.interactive.claim.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Additional comments
 * 
 * <p>Java class for ClaimPaymentPlan complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ClaimPaymentPlan">
 *   &lt;complexContent>
 *     &lt;extension base="{http://infrastructure.tia.dk/schema/common/v2/}TIAObject">
 *       &lt;sequence>
 *         &lt;element name="claItemNo" type="{http://infrastructure.tia.dk/schema/common/v2/}long10" minOccurs="0"/>
 *         &lt;element name="receiverIdNo" type="{http://infrastructure.tia.dk/schema/common/v2/}long10" minOccurs="0"/>
 *         &lt;element name="accountNo" type="{http://infrastructure.tia.dk/schema/common/v2/}long10" minOccurs="0"/>
 *         &lt;element name="paymentMethod" type="{http://infrastructure.tia.dk/schema/common/v2/}string8" minOccurs="0"/>
 *         &lt;element name="meansPayNo" type="{http://infrastructure.tia.dk/schema/common/v2/}long10" minOccurs="0"/>
 *         &lt;element name="startDate" type="{http://infrastructure.tia.dk/schema/common/v2/}timestamp" minOccurs="0"/>
 *         &lt;element name="endDate" type="{http://infrastructure.tia.dk/schema/common/v2/}timestamp" minOccurs="0"/>
 *         &lt;element name="paymentAmount" type="{http://infrastructure.tia.dk/schema/common/v2/}doublewithoutrestriction" minOccurs="0"/>
 *         &lt;element name="franchisePeriod" type="{http://infrastructure.tia.dk/schema/common/v2/}int3" minOccurs="0"/>
 *         &lt;element name="paymentFrequency" type="{http://infrastructure.tia.dk/schema/common/v2/}int2" minOccurs="0"/>
 *         &lt;element name="paymentDate" type="{http://infrastructure.tia.dk/schema/common/v2/}string10" minOccurs="0"/>
 *         &lt;element name="paymentPeriodBasis" type="{http://infrastructure.tia.dk/schema/common/v2/}int2" minOccurs="0"/>
 *         &lt;element name="claimPayPlanItemCollection" type="{http://infrastructure.tia.dk/schema/claim/v2/}ClaimPaymentPlanItemCollection" minOccurs="0"/>
 *         &lt;element name="claimPayPlanExceptionCollection" type="{http://infrastructure.tia.dk/schema/claim/v2/}ClaimPayPlanExceptionCollection" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ClaimPaymentPlan", propOrder = {
    "claItemNo",
    "receiverIdNo",
    "accountNo",
    "paymentMethod",
    "meansPayNo",
    "startDate",
    "endDate",
    "paymentAmount",
    "franchisePeriod",
    "paymentFrequency",
    "paymentDate",
    "paymentPeriodBasis",
    "claimPayPlanItemCollection",
    "claimPayPlanExceptionCollection"
})
public class ClaimPaymentPlan
    extends TIAObject
{

    @XmlElement(nillable = true)
    protected Long claItemNo;
    @XmlElement(nillable = true)
    protected Long receiverIdNo;
    @XmlElement(nillable = true)
    protected Long accountNo;
    @XmlElement(nillable = true)
    protected String paymentMethod;
    @XmlElement(nillable = true)
    protected Long meansPayNo;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar startDate;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar endDate;
    @XmlElement(nillable = true)
    protected Double paymentAmount;
    @XmlElement(nillable = true)
    protected Long franchisePeriod;
    @XmlElement(nillable = true)
    protected Long paymentFrequency;
    @XmlElement(nillable = true)
    protected String paymentDate;
    @XmlElement(nillable = true)
    protected Long paymentPeriodBasis;
    protected ClaimPaymentPlanItemCollection claimPayPlanItemCollection;
    protected ClaimPayPlanExceptionCollection claimPayPlanExceptionCollection;

    /**
     * Gets the value of the claItemNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getClaItemNo() {
        return claItemNo;
    }

    /**
     * Sets the value of the claItemNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setClaItemNo(Long value) {
        this.claItemNo = value;
    }

    /**
     * Gets the value of the receiverIdNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getReceiverIdNo() {
        return receiverIdNo;
    }

    /**
     * Sets the value of the receiverIdNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setReceiverIdNo(Long value) {
        this.receiverIdNo = value;
    }

    /**
     * Gets the value of the accountNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getAccountNo() {
        return accountNo;
    }

    /**
     * Sets the value of the accountNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setAccountNo(Long value) {
        this.accountNo = value;
    }

    /**
     * Gets the value of the paymentMethod property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaymentMethod() {
        return paymentMethod;
    }

    /**
     * Sets the value of the paymentMethod property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaymentMethod(String value) {
        this.paymentMethod = value;
    }

    /**
     * Gets the value of the meansPayNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getMeansPayNo() {
        return meansPayNo;
    }

    /**
     * Sets the value of the meansPayNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setMeansPayNo(Long value) {
        this.meansPayNo = value;
    }

    /**
     * Gets the value of the startDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getStartDate() {
        return startDate;
    }

    /**
     * Sets the value of the startDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setStartDate(XMLGregorianCalendar value) {
        this.startDate = value;
    }

    /**
     * Gets the value of the endDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEndDate() {
        return endDate;
    }

    /**
     * Sets the value of the endDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEndDate(XMLGregorianCalendar value) {
        this.endDate = value;
    }

    /**
     * Gets the value of the paymentAmount property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getPaymentAmount() {
        return paymentAmount;
    }

    /**
     * Sets the value of the paymentAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setPaymentAmount(Double value) {
        this.paymentAmount = value;
    }

    /**
     * Gets the value of the franchisePeriod property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getFranchisePeriod() {
        return franchisePeriod;
    }

    /**
     * Sets the value of the franchisePeriod property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setFranchisePeriod(Long value) {
        this.franchisePeriod = value;
    }

    /**
     * Gets the value of the paymentFrequency property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getPaymentFrequency() {
        return paymentFrequency;
    }

    /**
     * Sets the value of the paymentFrequency property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setPaymentFrequency(Long value) {
        this.paymentFrequency = value;
    }

    /**
     * Gets the value of the paymentDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaymentDate() {
        return paymentDate;
    }

    /**
     * Sets the value of the paymentDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaymentDate(String value) {
        this.paymentDate = value;
    }

    /**
     * Gets the value of the paymentPeriodBasis property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getPaymentPeriodBasis() {
        return paymentPeriodBasis;
    }

    /**
     * Sets the value of the paymentPeriodBasis property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setPaymentPeriodBasis(Long value) {
        this.paymentPeriodBasis = value;
    }

    /**
     * Gets the value of the claimPayPlanItemCollection property.
     * 
     * @return
     *     possible object is
     *     {@link ClaimPaymentPlanItemCollection }
     *     
     */
    public ClaimPaymentPlanItemCollection getClaimPayPlanItemCollection() {
        return claimPayPlanItemCollection;
    }

    /**
     * Sets the value of the claimPayPlanItemCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link ClaimPaymentPlanItemCollection }
     *     
     */
    public void setClaimPayPlanItemCollection(ClaimPaymentPlanItemCollection value) {
        this.claimPayPlanItemCollection = value;
    }

    /**
     * Gets the value of the claimPayPlanExceptionCollection property.
     * 
     * @return
     *     possible object is
     *     {@link ClaimPayPlanExceptionCollection }
     *     
     */
    public ClaimPayPlanExceptionCollection getClaimPayPlanExceptionCollection() {
        return claimPayPlanExceptionCollection;
    }

    /**
     * Sets the value of the claimPayPlanExceptionCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link ClaimPayPlanExceptionCollection }
     *     
     */
    public void setClaimPayPlanExceptionCollection(ClaimPayPlanExceptionCollection value) {
        this.claimPayPlanExceptionCollection = value;
    }

}
