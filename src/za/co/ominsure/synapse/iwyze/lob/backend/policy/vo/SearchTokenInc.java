
package za.co.ominsure.synapse.iwyze.lob.backend.policy.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Additional comments
 * 
 * <p>Java class for SearchTokenInc complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SearchTokenInc">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="insurerCode" type="{http://infrastructure.tia.dk/schema/common/v2/}string3" minOccurs="0"/>
 *         &lt;element name="insuranceCompanyName" type="{http://infrastructure.tia.dk/schema/common/v2/}string60" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SearchTokenInc", namespace = "http://infrastructure.tia.dk/schema/party/v2/", propOrder = {
    "insurerCode",
    "insuranceCompanyName"
})
public class SearchTokenInc {

    @XmlElement(nillable = true)
    protected String insurerCode;
    @XmlElement(nillable = true)
    protected String insuranceCompanyName;

    /**
     * Gets the value of the insurerCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInsurerCode() {
        return insurerCode;
    }

    /**
     * Sets the value of the insurerCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInsurerCode(String value) {
        this.insurerCode = value;
    }

    /**
     * Gets the value of the insuranceCompanyName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInsuranceCompanyName() {
        return insuranceCompanyName;
    }

    /**
     * Sets the value of the insuranceCompanyName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInsuranceCompanyName(String value) {
        this.insuranceCompanyName = value;
    }

}
