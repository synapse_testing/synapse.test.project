
package za.co.ominsure.synapse.iwyze.lob.backend.policy.vo;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Additional comments
 * 
 * <p>Java class for ObjectTypeVersion complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ObjectTypeVersion">
 *   &lt;complexContent>
 *     &lt;extension base="{http://infrastructure.tia.dk/schema/common/v2/}TIAObject">
 *       &lt;sequence>
 *         &lt;element name="objectTypeVerNo" type="{http://infrastructure.tia.dk/schema/common/v2/}decimal9Point3" minOccurs="0"/>
 *         &lt;element name="objectTypeName" type="{http://infrastructure.tia.dk/schema/common/v2/}string30" minOccurs="0"/>
 *         &lt;element name="objectTypeDesc" type="{http://infrastructure.tia.dk/schema/common/v2/}string2000" minOccurs="0"/>
 *         &lt;element name="objectAttributeCollection" type="{http://infrastructure.tia.dk/schema/policy/v2/}ObjectTypeAttributeCollection" minOccurs="0"/>
 *         &lt;element name="mainObjectTypeId" type="{http://infrastructure.tia.dk/schema/common/v2/}string12" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ObjectTypeVersion", propOrder = {
    "objectTypeVerNo",
    "objectTypeName",
    "objectTypeDesc",
    "objectAttributeCollection",
    "mainObjectTypeId"
})
public class ObjectTypeVersion
    extends TIAObject
{

    @XmlElement(nillable = true)
    protected BigDecimal objectTypeVerNo;
    @XmlElement(nillable = true)
    protected String objectTypeName;
    @XmlElement(nillable = true)
    protected String objectTypeDesc;
    protected ObjectTypeAttributeCollection objectAttributeCollection;
    @XmlElement(nillable = true)
    protected String mainObjectTypeId;

    /**
     * Gets the value of the objectTypeVerNo property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getObjectTypeVerNo() {
        return objectTypeVerNo;
    }

    /**
     * Sets the value of the objectTypeVerNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setObjectTypeVerNo(BigDecimal value) {
        this.objectTypeVerNo = value;
    }

    /**
     * Gets the value of the objectTypeName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getObjectTypeName() {
        return objectTypeName;
    }

    /**
     * Sets the value of the objectTypeName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setObjectTypeName(String value) {
        this.objectTypeName = value;
    }

    /**
     * Gets the value of the objectTypeDesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getObjectTypeDesc() {
        return objectTypeDesc;
    }

    /**
     * Sets the value of the objectTypeDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setObjectTypeDesc(String value) {
        this.objectTypeDesc = value;
    }

    /**
     * Gets the value of the objectAttributeCollection property.
     * 
     * @return
     *     possible object is
     *     {@link ObjectTypeAttributeCollection }
     *     
     */
    public ObjectTypeAttributeCollection getObjectAttributeCollection() {
        return objectAttributeCollection;
    }

    /**
     * Sets the value of the objectAttributeCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjectTypeAttributeCollection }
     *     
     */
    public void setObjectAttributeCollection(ObjectTypeAttributeCollection value) {
        this.objectAttributeCollection = value;
    }

    /**
     * Gets the value of the mainObjectTypeId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMainObjectTypeId() {
        return mainObjectTypeId;
    }

    /**
     * Sets the value of the mainObjectTypeId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMainObjectTypeId(String value) {
        this.mainObjectTypeId = value;
    }

}
