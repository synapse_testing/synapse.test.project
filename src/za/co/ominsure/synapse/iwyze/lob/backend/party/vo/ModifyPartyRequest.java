
package za.co.ominsure.synapse.iwyze.lob.backend.party.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="inputToken" type="{http://infrastructure.tia.dk/schema/common/v2/}InputToken"/>
 *         &lt;element name="lockingMode" type="{http://infrastructure.tia.dk/schema/common/v2/}lockingMode" minOccurs="0"/>
 *         &lt;element name="party" type="{http://infrastructure.tia.dk/schema/party/v3/}PartyElement"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "inputToken",
    "lockingMode",
    "party"
})
@XmlRootElement(name = "modifyPartyRequest", namespace = "http://infrastructure.tia.dk/schema/party/v3/")
public class ModifyPartyRequest {

    @XmlElement(namespace = "http://infrastructure.tia.dk/schema/party/v3/", required = true)
    protected InputToken inputToken;
    @XmlElement(namespace = "http://infrastructure.tia.dk/schema/party/v3/")
    protected String lockingMode;
    @XmlElement(namespace = "http://infrastructure.tia.dk/schema/party/v3/", required = true, nillable = true)
    protected PartyElement party;

    /**
     * Gets the value of the inputToken property.
     * 
     * @return
     *     possible object is
     *     {@link InputToken }
     *     
     */
    public InputToken getInputToken() {
        return inputToken;
    }

    /**
     * Sets the value of the inputToken property.
     * 
     * @param value
     *     allowed object is
     *     {@link InputToken }
     *     
     */
    public void setInputToken(InputToken value) {
        this.inputToken = value;
    }

    /**
     * Gets the value of the lockingMode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLockingMode() {
        return lockingMode;
    }

    /**
     * Sets the value of the lockingMode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLockingMode(String value) {
        this.lockingMode = value;
    }

    /**
     * Gets the value of the party property.
     * 
     * @return
     *     possible object is
     *     {@link PartyElement }
     *     
     */
    public PartyElement getParty() {
        return party;
    }

    /**
     * Sets the value of the party property.
     * 
     * @param value
     *     allowed object is
     *     {@link PartyElement }
     *     
     */
    public void setParty(PartyElement value) {
        this.party = value;
    }

}
