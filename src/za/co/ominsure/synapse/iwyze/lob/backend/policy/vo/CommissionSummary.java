
package za.co.ominsure.synapse.iwyze.lob.backend.policy.vo;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Additional comments
 * 
 * <p>Java class for CommissionSummary complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CommissionSummary">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="agentNo" type="{http://infrastructure.tia.dk/schema/common/v2/}long10" minOccurs="0"/>
 *         &lt;element name="dateFrom" type="{http://infrastructure.tia.dk/schema/common/v2/}date" minOccurs="0"/>
 *         &lt;element name="dateTo" type="{http://infrastructure.tia.dk/schema/common/v2/}date" minOccurs="0"/>
 *         &lt;element name="commissionGroup" type="{http://infrastructure.tia.dk/schema/common/v2/}string15" minOccurs="0"/>
 *         &lt;element name="currencyCode" type="{http://infrastructure.tia.dk/schema/common/v2/}string3" minOccurs="0"/>
 *         &lt;element name="premium" type="{http://infrastructure.tia.dk/schema/common/v2/}decimal30Point2" minOccurs="0"/>
 *         &lt;element name="pctCommission" type="{http://infrastructure.tia.dk/schema/common/v2/}decimal30Point2" minOccurs="0"/>
 *         &lt;element name="flatCommission" type="{http://infrastructure.tia.dk/schema/common/v2/}decimal30Point2" minOccurs="0"/>
 *         &lt;element name="commissionItemCollection" type="{http://infrastructure.tia.dk/schema/policy/v2/}CommissionItemCollection" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CommissionSummary", propOrder = {
    "agentNo",
    "dateFrom",
    "dateTo",
    "commissionGroup",
    "currencyCode",
    "premium",
    "pctCommission",
    "flatCommission",
    "commissionItemCollection"
})
public class CommissionSummary {

    @XmlElement(nillable = true)
    protected Long agentNo;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dateFrom;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dateTo;
    @XmlElement(nillable = true)
    protected String commissionGroup;
    @XmlElement(nillable = true)
    protected String currencyCode;
    @XmlElement(nillable = true)
    protected BigDecimal premium;
    @XmlElement(nillable = true)
    protected BigDecimal pctCommission;
    @XmlElement(nillable = true)
    protected BigDecimal flatCommission;
    protected CommissionItemCollection commissionItemCollection;

    /**
     * Gets the value of the agentNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getAgentNo() {
        return agentNo;
    }

    /**
     * Sets the value of the agentNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setAgentNo(Long value) {
        this.agentNo = value;
    }

    /**
     * Gets the value of the dateFrom property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDateFrom() {
        return dateFrom;
    }

    /**
     * Sets the value of the dateFrom property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDateFrom(XMLGregorianCalendar value) {
        this.dateFrom = value;
    }

    /**
     * Gets the value of the dateTo property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDateTo() {
        return dateTo;
    }

    /**
     * Sets the value of the dateTo property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDateTo(XMLGregorianCalendar value) {
        this.dateTo = value;
    }

    /**
     * Gets the value of the commissionGroup property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCommissionGroup() {
        return commissionGroup;
    }

    /**
     * Sets the value of the commissionGroup property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCommissionGroup(String value) {
        this.commissionGroup = value;
    }

    /**
     * Gets the value of the currencyCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrencyCode() {
        return currencyCode;
    }

    /**
     * Sets the value of the currencyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrencyCode(String value) {
        this.currencyCode = value;
    }

    /**
     * Gets the value of the premium property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPremium() {
        return premium;
    }

    /**
     * Sets the value of the premium property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPremium(BigDecimal value) {
        this.premium = value;
    }

    /**
     * Gets the value of the pctCommission property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPctCommission() {
        return pctCommission;
    }

    /**
     * Sets the value of the pctCommission property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPctCommission(BigDecimal value) {
        this.pctCommission = value;
    }

    /**
     * Gets the value of the flatCommission property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getFlatCommission() {
        return flatCommission;
    }

    /**
     * Sets the value of the flatCommission property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setFlatCommission(BigDecimal value) {
        this.flatCommission = value;
    }

    /**
     * Gets the value of the commissionItemCollection property.
     * 
     * @return
     *     possible object is
     *     {@link CommissionItemCollection }
     *     
     */
    public CommissionItemCollection getCommissionItemCollection() {
        return commissionItemCollection;
    }

    /**
     * Sets the value of the commissionItemCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link CommissionItemCollection }
     *     
     */
    public void setCommissionItemCollection(CommissionItemCollection value) {
        this.commissionItemCollection = value;
    }

}
