
package za.co.ominsure.synapse.iwyze.lob.backend.interactive.claim.vo;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for modelType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="modelType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="A4"/>
 *     &lt;enumeration value="A6"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "modelType", namespace = "http://infrastructure.tia.dk/schema/common/v2/")
@XmlEnum
public enum ModelType {

    @XmlEnumValue("A4")
    A_4("A4"),
    @XmlEnumValue("A6")
    A_6("A6");
    private final String value;

    ModelType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ModelType fromValue(String v) {
        for (ModelType c: ModelType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
