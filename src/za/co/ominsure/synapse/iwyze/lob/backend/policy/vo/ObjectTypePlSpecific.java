
package za.co.ominsure.synapse.iwyze.lob.backend.policy.vo;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Additional comments
 * 
 * <p>Java class for ObjectTypePlSpecific complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ObjectTypePlSpecific">
 *   &lt;complexContent>
 *     &lt;extension base="{http://infrastructure.tia.dk/schema/common/v2/}TIAObject">
 *       &lt;sequence>
 *         &lt;element name="objectTypeId" type="{http://infrastructure.tia.dk/schema/common/v2/}string12" minOccurs="0"/>
 *         &lt;element name="objectTypeVerNo" type="{http://infrastructure.tia.dk/schema/common/v2/}decimal9Point3" minOccurs="0"/>
 *         &lt;element name="sectionNo" type="{http://infrastructure.tia.dk/schema/common/v2/}int3" minOccurs="0"/>
 *         &lt;element name="objectSlaveType" type="{http://infrastructure.tia.dk/schema/policy/v2/}ObjectSlaveTypePlSpecificCollection" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ObjectTypePlSpecific", propOrder = {
    "objectTypeId",
    "objectTypeVerNo",
    "sectionNo",
    "objectSlaveType"
})
public class ObjectTypePlSpecific
    extends TIAObject
{

    @XmlElement(nillable = true)
    protected String objectTypeId;
    @XmlElement(nillable = true)
    protected BigDecimal objectTypeVerNo;
    @XmlElement(nillable = true)
    protected Long sectionNo;
    protected ObjectSlaveTypePlSpecificCollection objectSlaveType;

    /**
     * Gets the value of the objectTypeId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getObjectTypeId() {
        return objectTypeId;
    }

    /**
     * Sets the value of the objectTypeId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setObjectTypeId(String value) {
        this.objectTypeId = value;
    }

    /**
     * Gets the value of the objectTypeVerNo property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getObjectTypeVerNo() {
        return objectTypeVerNo;
    }

    /**
     * Sets the value of the objectTypeVerNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setObjectTypeVerNo(BigDecimal value) {
        this.objectTypeVerNo = value;
    }

    /**
     * Gets the value of the sectionNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getSectionNo() {
        return sectionNo;
    }

    /**
     * Sets the value of the sectionNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setSectionNo(Long value) {
        this.sectionNo = value;
    }

    /**
     * Gets the value of the objectSlaveType property.
     * 
     * @return
     *     possible object is
     *     {@link ObjectSlaveTypePlSpecificCollection }
     *     
     */
    public ObjectSlaveTypePlSpecificCollection getObjectSlaveType() {
        return objectSlaveType;
    }

    /**
     * Sets the value of the objectSlaveType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjectSlaveTypePlSpecificCollection }
     *     
     */
    public void setObjectSlaveType(ObjectSlaveTypePlSpecificCollection value) {
        this.objectSlaveType = value;
    }

}
