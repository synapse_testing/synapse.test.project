
package za.co.ominsure.synapse.iwyze.lob.backend.policy.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * Is mapped into obj_policy_line
 * 
 * <p>Java class for PolicyLine complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PolicyLine">
 *   &lt;complexContent>
 *     &lt;extension base="{http://infrastructure.tia.dk/schema/policy/v2/}PolicyLine">
 *       &lt;sequence>
 *         &lt;element name="premiumRiskSplitCollection" type="{http://infrastructure.tia.dk/schema/policy/v3/}PremiumRiskSplitCollection" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PolicyLine", namespace = "http://infrastructure.tia.dk/schema/policy/v3/", propOrder = {
    "premiumRiskSplitCollection"
})
public class PolicyLine3
    extends PolicyLine2
{

    protected PremiumRiskSplitCollection premiumRiskSplitCollection;

    /**
     * Gets the value of the premiumRiskSplitCollection property.
     * 
     * @return
     *     possible object is
     *     {@link PremiumRiskSplitCollection }
     *     
     */
    public PremiumRiskSplitCollection getPremiumRiskSplitCollection() {
        return premiumRiskSplitCollection;
    }

    /**
     * Sets the value of the premiumRiskSplitCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link PremiumRiskSplitCollection }
     *     
     */
    public void setPremiumRiskSplitCollection(PremiumRiskSplitCollection value) {
        this.premiumRiskSplitCollection = value;
    }

}
