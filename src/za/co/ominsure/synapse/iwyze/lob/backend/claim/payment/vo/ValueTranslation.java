
package za.co.ominsure.synapse.iwyze.lob.backend.claim.payment.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ValueTranslation complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ValueTranslation">
 *   &lt;complexContent>
 *     &lt;extension base="{http://infrastructure.tia.dk/schema/common/v2/}TranslatedCode">
 *       &lt;sequence>
 *         &lt;element name="miscValue" type="{http://infrastructure.tia.dk/schema/common/v2/}string60"/>
 *         &lt;element name="sortNo" type="{http://infrastructure.tia.dk/schema/common/v2/}int3"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ValueTranslation", namespace = "http://infrastructure.tia.dk/schema/common/v2/", propOrder = {
    "miscValue",
    "sortNo"
})
public class ValueTranslation
    extends TranslatedCode
{

    @XmlElement(required = true, nillable = true)
    protected String miscValue;
    @XmlElement(required = true, type = Long.class, nillable = true)
    protected Long sortNo;

    /**
     * Gets the value of the miscValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMiscValue() {
        return miscValue;
    }

    /**
     * Sets the value of the miscValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMiscValue(String value) {
        this.miscValue = value;
    }

    /**
     * Gets the value of the sortNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getSortNo() {
        return sortNo;
    }

    /**
     * Sets the value of the sortNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setSortNo(Long value) {
        this.sortNo = value;
    }

}
