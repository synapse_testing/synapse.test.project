
package za.co.ominsure.synapse.iwyze.lob.backend.detail.payment.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Is mapped into TIA metadata type "obj_reminder_specification".
 * 
 * <p>Java class for ReminderSpecification complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ReminderSpecification">
 *   &lt;complexContent>
 *     &lt;extension base="{http://infrastructure.tia.dk/schema/common/v2/}TIAObject">
 *       &lt;sequence>
 *         &lt;element name="specificationText" type="{http://infrastructure.tia.dk/schema/account/v2/}specificationText" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReminderSpecification", propOrder = {
    "specificationText"
})
public class ReminderSpecification
    extends TIAObject
{

    @XmlElement(nillable = true)
    protected String specificationText;

    /**
     * Gets the value of the specificationText property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSpecificationText() {
        return specificationText;
    }

    /**
     * Sets the value of the specificationText property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSpecificationText(String value) {
        this.specificationText = value;
    }

}
