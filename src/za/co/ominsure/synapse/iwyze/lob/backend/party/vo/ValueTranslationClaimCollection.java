
package za.co.ominsure.synapse.iwyze.lob.backend.party.vo;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * tab_value_translation_claim
 * 
 * <p>Java class for ValueTranslationClaimCollection complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ValueTranslationClaimCollection">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ValueTranslationClaim" type="{http://infrastructure.tia.dk/schema/common/v2/}ValueTranslationClaim" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ValueTranslationClaimCollection", namespace = "http://infrastructure.tia.dk/schema/common/v2/", propOrder = {
    "valueTranslationClaim"
})
public class ValueTranslationClaimCollection {

    @XmlElement(name = "ValueTranslationClaim", nillable = true)
    protected List<ValueTranslationClaim> valueTranslationClaim;

    /**
     * Gets the value of the valueTranslationClaim property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the valueTranslationClaim property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getValueTranslationClaim().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ValueTranslationClaim }
     * 
     * 
     */
    public List<ValueTranslationClaim> getValueTranslationClaim() {
        if (valueTranslationClaim == null) {
            valueTranslationClaim = new ArrayList<ValueTranslationClaim>();
        }
        return this.valueTranslationClaim;
    }

}
