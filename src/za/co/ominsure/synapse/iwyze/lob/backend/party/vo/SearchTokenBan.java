
package za.co.ominsure.synapse.iwyze.lob.backend.party.vo;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * Additional comments
 * 
 * <p>Java class for SearchTokenBan complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SearchTokenBan">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="bankName" type="{http://infrastructure.tia.dk/schema/common/v2/}string32" minOccurs="0"/>
 *         &lt;element name="bankCode" type="{http://infrastructure.tia.dk/schema/common/v2/}string20" minOccurs="0"/>
 *         &lt;element name="bankCodeTypeCollection" type="{http://infrastructure.tia.dk/schema/common/v2/}StringCollection" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SearchTokenBan", propOrder = {
    "bankName",
    "bankCode",
    "bankCodeTypeCollection"
})
public class SearchTokenBan {

    @XmlElementRef(name = "bankName", namespace = "http://infrastructure.tia.dk/schema/party/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> bankName;
    @XmlElementRef(name = "bankCode", namespace = "http://infrastructure.tia.dk/schema/party/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> bankCode;
    protected StringCollection bankCodeTypeCollection;

    /**
     * Gets the value of the bankName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getBankName() {
        return bankName;
    }

    /**
     * Sets the value of the bankName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setBankName(JAXBElement<String> value) {
        this.bankName = value;
    }

    /**
     * Gets the value of the bankCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getBankCode() {
        return bankCode;
    }

    /**
     * Sets the value of the bankCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setBankCode(JAXBElement<String> value) {
        this.bankCode = value;
    }

    /**
     * Gets the value of the bankCodeTypeCollection property.
     * 
     * @return
     *     possible object is
     *     {@link StringCollection }
     *     
     */
    public StringCollection getBankCodeTypeCollection() {
        return bankCodeTypeCollection;
    }

    /**
     * Sets the value of the bankCodeTypeCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link StringCollection }
     *     
     */
    public void setBankCodeTypeCollection(StringCollection value) {
        this.bankCodeTypeCollection = value;
    }

}
