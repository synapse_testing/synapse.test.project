
package za.co.ominsure.synapse.iwyze.lob.backend.servicesupplier.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="serviceSupplier" type="{http://infrastructure.tia.dk/schema/party/v2/}PartyElement"/>
 *         &lt;element name="result" type="{http://infrastructure.tia.dk/schema/common/v2/}Result"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "serviceSupplier",
    "result"
})
@XmlRootElement(name = "getServiceSupplierResponse")
public class GetServiceSupplierResponse {

    @XmlElement(required = true, nillable = true)
    protected PartyElement serviceSupplier;
    @XmlElement(required = true)
    protected Result result;

    /**
     * Gets the value of the serviceSupplier property.
     * 
     * @return
     *     possible object is
     *     {@link PartyElement }
     *     
     */
    public PartyElement getServiceSupplier() {
        return serviceSupplier;
    }

    /**
     * Sets the value of the serviceSupplier property.
     * 
     * @param value
     *     allowed object is
     *     {@link PartyElement }
     *     
     */
    public void setServiceSupplier(PartyElement value) {
        this.serviceSupplier = value;
    }

    /**
     * Gets the value of the result property.
     * 
     * @return
     *     possible object is
     *     {@link Result }
     *     
     */
    public Result getResult() {
        return result;
    }

    /**
     * Sets the value of the result property.
     * 
     * @param value
     *     allowed object is
     *     {@link Result }
     *     
     */
    public void setResult(Result value) {
        this.result = value;
    }

}
