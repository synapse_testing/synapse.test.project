
package za.co.ominsure.synapse.iwyze.lob.backend.policy.vo;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="inputToken" type="{http://infrastructure.tia.dk/schema/common/v2/}InputToken"/>
 *         &lt;element name="customerNo" type="{http://infrastructure.tia.dk/schema/common/v2/}policyNo"/>
 *         &lt;element name="asOfDate" type="{http://infrastructure.tia.dk/schema/common/v2/}date" minOccurs="0"/>
 *         &lt;element name="getDepth" type="{http://infrastructure.tia.dk/schema/policy/v2/}getDepth" minOccurs="0"/>
 *         &lt;element name="requestedStateCollection" type="{http://infrastructure.tia.dk/schema/policy/v2/}RequestedStateCollection" minOccurs="0"/>
 *         &lt;element name="pageSort" type="{http://infrastructure.tia.dk/schema/common/v2/}PageSort" minOccurs="0"/>
 *         &lt;element name="newestOnlyYN" type="{http://infrastructure.tia.dk/schema/common/v2/}tiaYN" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "inputToken",
    "customerNo",
    "asOfDate",
    "getDepth",
    "requestedStateCollection",
    "pageSort",
    "newestOnlyYN"
})
@XmlRootElement(name = "getCustomerPoliciesRequest", namespace = "http://infrastructure.tia.dk/schema/policy/v4/")
public class GetCustomerPoliciesRequest {

    @XmlElement(namespace = "http://infrastructure.tia.dk/schema/policy/v4/", required = true)
    protected InputToken inputToken;
    @XmlElement(namespace = "http://infrastructure.tia.dk/schema/policy/v4/", required = true)
    @XmlSchemaType(name = "unsignedLong")
    protected BigInteger customerNo;
    @XmlElement(namespace = "http://infrastructure.tia.dk/schema/policy/v4/")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar asOfDate;
    @XmlElement(namespace = "http://infrastructure.tia.dk/schema/policy/v4/")
    @XmlSchemaType(name = "string")
    protected GetDepth getDepth;
    @XmlElement(namespace = "http://infrastructure.tia.dk/schema/policy/v4/")
    protected RequestedStateCollection requestedStateCollection;
    @XmlElement(namespace = "http://infrastructure.tia.dk/schema/policy/v4/")
    protected PageSort pageSort;
    @XmlElement(namespace = "http://infrastructure.tia.dk/schema/policy/v4/")
    protected String newestOnlyYN;

    /**
     * Gets the value of the inputToken property.
     * 
     * @return
     *     possible object is
     *     {@link InputToken }
     *     
     */
    public InputToken getInputToken() {
        return inputToken;
    }

    /**
     * Sets the value of the inputToken property.
     * 
     * @param value
     *     allowed object is
     *     {@link InputToken }
     *     
     */
    public void setInputToken(InputToken value) {
        this.inputToken = value;
    }

    /**
     * Gets the value of the customerNo property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getCustomerNo() {
        return customerNo;
    }

    /**
     * Sets the value of the customerNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setCustomerNo(BigInteger value) {
        this.customerNo = value;
    }

    /**
     * Gets the value of the asOfDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getAsOfDate() {
        return asOfDate;
    }

    /**
     * Sets the value of the asOfDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setAsOfDate(XMLGregorianCalendar value) {
        this.asOfDate = value;
    }

    /**
     * Gets the value of the getDepth property.
     * 
     * @return
     *     possible object is
     *     {@link GetDepth }
     *     
     */
    public GetDepth getGetDepth() {
        return getDepth;
    }

    /**
     * Sets the value of the getDepth property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetDepth }
     *     
     */
    public void setGetDepth(GetDepth value) {
        this.getDepth = value;
    }

    /**
     * Gets the value of the requestedStateCollection property.
     * 
     * @return
     *     possible object is
     *     {@link RequestedStateCollection }
     *     
     */
    public RequestedStateCollection getRequestedStateCollection() {
        return requestedStateCollection;
    }

    /**
     * Sets the value of the requestedStateCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link RequestedStateCollection }
     *     
     */
    public void setRequestedStateCollection(RequestedStateCollection value) {
        this.requestedStateCollection = value;
    }

    /**
     * Gets the value of the pageSort property.
     * 
     * @return
     *     possible object is
     *     {@link PageSort }
     *     
     */
    public PageSort getPageSort() {
        return pageSort;
    }

    /**
     * Sets the value of the pageSort property.
     * 
     * @param value
     *     allowed object is
     *     {@link PageSort }
     *     
     */
    public void setPageSort(PageSort value) {
        this.pageSort = value;
    }

    /**
     * Gets the value of the newestOnlyYN property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNewestOnlyYN() {
        return newestOnlyYN;
    }

    /**
     * Sets the value of the newestOnlyYN property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNewestOnlyYN(String value) {
        this.newestOnlyYN = value;
    }

}
