
package za.co.ominsure.synapse.iwyze.lob.backend.policy.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Additional comments
 * 
 * <p>Java class for ObjectSlaveTypeAttribute complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ObjectSlaveTypeAttribute">
 *   &lt;complexContent>
 *     &lt;extension base="{http://infrastructure.tia.dk/schema/common/v2/}TIAObject">
 *       &lt;sequence>
 *         &lt;element name="attributeName" type="{http://infrastructure.tia.dk/schema/common/v2/}string30" minOccurs="0"/>
 *         &lt;element name="dataType" type="{http://infrastructure.tia.dk/schema/common/v2/}string3" minOccurs="0"/>
 *         &lt;element name="length" type="{http://infrastructure.tia.dk/schema/common/v2/}int5" minOccurs="0"/>
 *         &lt;element name="defaultValue" type="{http://infrastructure.tia.dk/schema/common/v2/}string30" minOccurs="0"/>
 *         &lt;element name="codeTableName" type="{http://infrastructure.tia.dk/schema/common/v2/}string12" minOccurs="0"/>
 *         &lt;element name="regExpPattern" type="{http://infrastructure.tia.dk/schema/common/v2/}string2000" minOccurs="0"/>
 *         &lt;element name="nullAllowedYN" type="{http://infrastructure.tia.dk/schema/common/v2/}string1" minOccurs="0"/>
 *         &lt;element name="displayFuncAuth" type="{http://infrastructure.tia.dk/schema/common/v2/}int4" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ObjectSlaveTypeAttribute", propOrder = {
    "attributeName",
    "dataType",
    "length",
    "defaultValue",
    "codeTableName",
    "regExpPattern",
    "nullAllowedYN",
    "displayFuncAuth"
})
public class ObjectSlaveTypeAttribute
    extends TIAObject
{

    @XmlElement(nillable = true)
    protected String attributeName;
    @XmlElement(nillable = true)
    protected String dataType;
    @XmlElement(nillable = true)
    protected Long length;
    @XmlElement(nillable = true)
    protected String defaultValue;
    @XmlElement(nillable = true)
    protected String codeTableName;
    @XmlElement(nillable = true)
    protected String regExpPattern;
    protected String nullAllowedYN;
    @XmlElement(nillable = true)
    protected Long displayFuncAuth;

    /**
     * Gets the value of the attributeName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAttributeName() {
        return attributeName;
    }

    /**
     * Sets the value of the attributeName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAttributeName(String value) {
        this.attributeName = value;
    }

    /**
     * Gets the value of the dataType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataType() {
        return dataType;
    }

    /**
     * Sets the value of the dataType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataType(String value) {
        this.dataType = value;
    }

    /**
     * Gets the value of the length property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getLength() {
        return length;
    }

    /**
     * Sets the value of the length property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setLength(Long value) {
        this.length = value;
    }

    /**
     * Gets the value of the defaultValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefaultValue() {
        return defaultValue;
    }

    /**
     * Sets the value of the defaultValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefaultValue(String value) {
        this.defaultValue = value;
    }

    /**
     * Gets the value of the codeTableName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodeTableName() {
        return codeTableName;
    }

    /**
     * Sets the value of the codeTableName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodeTableName(String value) {
        this.codeTableName = value;
    }

    /**
     * Gets the value of the regExpPattern property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegExpPattern() {
        return regExpPattern;
    }

    /**
     * Sets the value of the regExpPattern property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegExpPattern(String value) {
        this.regExpPattern = value;
    }

    /**
     * Gets the value of the nullAllowedYN property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNullAllowedYN() {
        return nullAllowedYN;
    }

    /**
     * Sets the value of the nullAllowedYN property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNullAllowedYN(String value) {
        this.nullAllowedYN = value;
    }

    /**
     * Gets the value of the displayFuncAuth property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getDisplayFuncAuth() {
        return displayFuncAuth;
    }

    /**
     * Sets the value of the displayFuncAuth property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setDisplayFuncAuth(Long value) {
        this.displayFuncAuth = value;
    }

}
