
package za.co.ominsure.synapse.iwyze.lob.backend.detail.payment.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * Is mapped into TIA metadata type "obj_payment_method".
 * 
 * <p>Java class for PaymentMethod complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PaymentMethod">
 *   &lt;complexContent>
 *     &lt;extension base="{http://infrastructure.tia.dk/schema/common/v2/}TIAObject">
 *       &lt;sequence>
 *         &lt;element name="paymentMethod" type="{http://infrastructure.tia.dk/schema/common/v2/}paymentMethod"/>
 *         &lt;element name="paymentDetailsNeededYN" type="{http://infrastructure.tia.dk/schema/common/v2/}tiaYN"/>
 *         &lt;element name="paymentInYN" type="{http://infrastructure.tia.dk/schema/common/v2/}tiaYN" minOccurs="0"/>
 *         &lt;element name="paymentOutYN" type="{http://infrastructure.tia.dk/schema/common/v2/}tiaYN" minOccurs="0"/>
 *         &lt;element name="payPer" type="{http://infrastructure.tia.dk/schema/account/v2/}payPer"/>
 *         &lt;element name="daysBeforeDuedate" type="{http://infrastructure.tia.dk/schema/account/v2/}daysBeforeDuedate" minOccurs="0"/>
 *         &lt;element name="itemText" type="{http://infrastructure.tia.dk/schema/account/v2/}itemText" minOccurs="0"/>
 *         &lt;element name="approveCollectionsYN" type="{http://infrastructure.tia.dk/schema/common/v2/}tiaYN" minOccurs="0"/>
 *         &lt;element name="approvePaymentsYN" type="{http://infrastructure.tia.dk/schema/common/v2/}tiaYN" minOccurs="0"/>
 *         &lt;element name="deferredAllocationYN" type="{http://infrastructure.tia.dk/schema/common/v2/}tiaYN" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PaymentMethod", propOrder = {
    "paymentMethod",
    "paymentDetailsNeededYN",
    "paymentInYN",
    "paymentOutYN",
    "payPer",
    "daysBeforeDuedate",
    "itemText",
    "approveCollectionsYN",
    "approvePaymentsYN",
    "deferredAllocationYN"
})
public class PaymentMethod
    extends TIAObject
{

    @XmlElement(required = true)
    protected String paymentMethod;
    @XmlElement(required = true)
    protected String paymentDetailsNeededYN;
    @XmlElement(nillable = true)
    protected String paymentInYN;
    @XmlElement(nillable = true)
    protected String paymentOutYN;
    @XmlSchemaType(name = "integer")
    protected int payPer;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "integer")
    protected Integer daysBeforeDuedate;
    @XmlElement(nillable = true)
    protected String itemText;
    @XmlElement(nillable = true)
    protected String approveCollectionsYN;
    @XmlElement(nillable = true)
    protected String approvePaymentsYN;
    @XmlElement(nillable = true)
    protected String deferredAllocationYN;

    /**
     * Gets the value of the paymentMethod property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaymentMethod() {
        return paymentMethod;
    }

    /**
     * Sets the value of the paymentMethod property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaymentMethod(String value) {
        this.paymentMethod = value;
    }

    /**
     * Gets the value of the paymentDetailsNeededYN property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaymentDetailsNeededYN() {
        return paymentDetailsNeededYN;
    }

    /**
     * Sets the value of the paymentDetailsNeededYN property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaymentDetailsNeededYN(String value) {
        this.paymentDetailsNeededYN = value;
    }

    /**
     * Gets the value of the paymentInYN property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaymentInYN() {
        return paymentInYN;
    }

    /**
     * Sets the value of the paymentInYN property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaymentInYN(String value) {
        this.paymentInYN = value;
    }

    /**
     * Gets the value of the paymentOutYN property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaymentOutYN() {
        return paymentOutYN;
    }

    /**
     * Sets the value of the paymentOutYN property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaymentOutYN(String value) {
        this.paymentOutYN = value;
    }

    /**
     * Gets the value of the payPer property.
     * 
     */
    public int getPayPer() {
        return payPer;
    }

    /**
     * Sets the value of the payPer property.
     * 
     */
    public void setPayPer(int value) {
        this.payPer = value;
    }

    /**
     * Gets the value of the daysBeforeDuedate property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDaysBeforeDuedate() {
        return daysBeforeDuedate;
    }

    /**
     * Sets the value of the daysBeforeDuedate property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDaysBeforeDuedate(Integer value) {
        this.daysBeforeDuedate = value;
    }

    /**
     * Gets the value of the itemText property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItemText() {
        return itemText;
    }

    /**
     * Sets the value of the itemText property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItemText(String value) {
        this.itemText = value;
    }

    /**
     * Gets the value of the approveCollectionsYN property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApproveCollectionsYN() {
        return approveCollectionsYN;
    }

    /**
     * Sets the value of the approveCollectionsYN property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApproveCollectionsYN(String value) {
        this.approveCollectionsYN = value;
    }

    /**
     * Gets the value of the approvePaymentsYN property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApprovePaymentsYN() {
        return approvePaymentsYN;
    }

    /**
     * Sets the value of the approvePaymentsYN property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApprovePaymentsYN(String value) {
        this.approvePaymentsYN = value;
    }

    /**
     * Gets the value of the deferredAllocationYN property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeferredAllocationYN() {
        return deferredAllocationYN;
    }

    /**
     * Sets the value of the deferredAllocationYN property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeferredAllocationYN(String value) {
        this.deferredAllocationYN = value;
    }

}
