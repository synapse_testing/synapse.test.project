
package za.co.ominsure.synapse.iwyze.lob.backend.search.payment.vo;

import java.math.BigDecimal;
import java.math.BigInteger;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the za.co.ominsure.synapse.iwyze.lob.backend.search.payment.vo package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _AccountStatementPolicyNo_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "policyNo");
    private final static QName _AccountStatementMpPolicyNo_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "mpPolicyNo");
    private final static QName _AccountStatementFromDate_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "fromDate");
    private final static QName _AccountStatementClaimNo_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "claimNo");
    private final static QName _AccountStatementPolicyLineNo_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "policyLineNo");
    private final static QName _AccountStatementToDate_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "toDate");
    private final static QName _SearchTokenPaymentDueDateFrom_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "dueDateFrom");
    private final static QName _SearchTokenPaymentPaymentMethod_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "paymentMethod");
    private final static QName _SearchTokenPaymentCurrencyCode_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "currencyCode");
    private final static QName _SearchTokenPaymentTransactionDateTo_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "transactionDateTo");
    private final static QName _SearchTokenPaymentPaymentStatus_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "paymentStatus");
    private final static QName _SearchTokenPaymentPaymentNo_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "paymentNo");
    private final static QName _SearchTokenPaymentAmountTo_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "amountTo");
    private final static QName _SearchTokenPaymentAccountNo_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "accountNo");
    private final static QName _SearchTokenPaymentAmountFrom_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "amountFrom");
    private final static QName _SearchTokenPaymentExternalRefIn_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "externalRefIn");
    private final static QName _SearchTokenPaymentDueDateTo_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "dueDateTo");
    private final static QName _SearchTokenPaymentTransactionDateFrom_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "transactionDateFrom");
    private final static QName _PaymentDetailsExternalPaymentId_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "externalPaymentId");
    private final static QName _PaymentDetailsPaymentDetailsId_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "paymentDetailsId");
    private final static QName _PaymentDetailsStartDate_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "startDate");
    private final static QName _PaymentDetailsBankIdNo_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "bankIdNo");
    private final static QName _PaymentDetailsBankCodeType_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "bankCodeType");
    private final static QName _PaymentDetailsIdentification_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "identification");
    private final static QName _PaymentDetailsCreditorNo_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "creditorNo");
    private final static QName _PaymentDetailsNameIdNo_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "nameIdNo");
    private final static QName _PaymentDetailsBankAccountNo_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "bankAccountNo");
    private final static QName _PaymentDetailsCcContinuousDate_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "ccContinuousDate");
    private final static QName _PaymentDetailsAccountHolderName_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "accountHolderName");
    private final static QName _PaymentDetailsGiroType_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "giroType");
    private final static QName _PaymentDetailsPaymentChannel_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "paymentChannel");
    private final static QName _PaymentDetailsCcNo_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "ccNo");
    private final static QName _PaymentDetailsBankCode_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "bankCode");
    private final static QName _PaymentDetailsCcContinuous_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "ccContinuous");
    private final static QName _PaymentDetailsCcExpiryDate_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "ccExpiryDate");
    private final static QName _RemindedItemRemindedAmount_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "remindedAmount");
    private final static QName _RemindedItemAccountItemNo_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "accountItemNo");
    private final static QName _InstalmentPlanPlanStartDate_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "planStartDate");
    private final static QName _InstalmentPlanPlanEndDate_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "planEndDate");
    private final static QName _EmailEmailBCC_QNAME = new QName("http://infrastructure.tia.dk/schema/common/v2/", "emailBCC");
    private final static QName _EmailEmailCC_QNAME = new QName("http://infrastructure.tia.dk/schema/common/v2/", "emailCC");
    private final static QName _PaymentItemApprovedBy_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "approvedBy");
    private final static QName _PaymentItemAmount_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "amount");
    private final static QName _PaymentSpecificationSpecificationText_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "specificationText");
    private final static QName _PaymentSpecificationSeqNo_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "seqNo");
    private final static QName _AccountStatementItemPaidAmount_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "paidAmount");
    private final static QName _AccountStatementItemItemId_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "itemId");
    private final static QName _AccountStatementItemItemReference_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "itemReference");
    private final static QName _AccountStatementItemSpecification_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "specification");
    private final static QName _AccountStatementItemSource_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "source");
    private final static QName _AccountStatementItemTransactionDate_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "transactionDate");
    private final static QName _AccountStatementItemItemText_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "itemText");
    private final static QName _AccountStatementItemPaidAccountItemNo_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "paidAccountItemNo");
    private final static QName _AccountStatementItemPaidCurrencyCode_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "paidCurrencyCode");
    private final static QName _AccountStatementItemEndDate_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "endDate");
    private final static QName _AccountCenterCode_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "centerCode");
    private final static QName _AccountPaymentMethodIn_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "paymentMethodIn");
    private final static QName _AccountLocked_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "locked");
    private final static QName _AccountCollectionBy_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "collectionBy");
    private final static QName _AccountPaymentMethodOut_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "paymentMethodOut");
    private final static QName _AccountReceiverId_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "receiverId");
    private final static QName _AccountMeansPayNoOut_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "meansPayNoOut");
    private final static QName _AccountPaymentTermsOut_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "paymentTermsOut");
    private final static QName _AccountReminderGroup_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "reminderGroup");
    private final static QName _AccountPaymentTermsIn_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "paymentTermsIn");
    private final static QName _AccountTitle_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "title");
    private final static QName _AccountHandler_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "handler");
    private final static QName _AccountNote_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "note");
    private final static QName _AccountMeansPayNoIn_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "meansPayNoIn");
    private final static QName _CollectionRequestConfirmExternalCode_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "externalCode");
    private final static QName _InputTokenCorrelationId_QNAME = new QName("http://infrastructure.tia.dk/schema/common/v2/", "correlationId");
    private final static QName _TranslatedCodeDescription_QNAME = new QName("http://infrastructure.tia.dk/schema/common/v2/", "description");
    private final static QName _TranslatedCodeDiplayCode_QNAME = new QName("http://infrastructure.tia.dk/schema/common/v2/", "diplayCode");
    private final static QName _TranslatedCodeHelpText_QNAME = new QName("http://infrastructure.tia.dk/schema/common/v2/", "helpText");
    private final static QName _ValueVarchar2Value_QNAME = new QName("http://infrastructure.tia.dk/schema/common/v2/", "varchar2Value");
    private final static QName _ValueNumberValue_QNAME = new QName("http://infrastructure.tia.dk/schema/common/v2/", "numberValue");
    private final static QName _ValueDateValue_QNAME = new QName("http://infrastructure.tia.dk/schema/common/v2/", "dateValue");
    private final static QName _CollectionAcknowledgementExternalCodeIn_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "externalCodeIn");
    private final static QName _ReminderReminderStepNo_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "reminderStepNo");
    private final static QName _ReminderDueDate_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "dueDate");
    private final static QName _TIAObjectSiteSpecifics_QNAME = new QName("http://infrastructure.tia.dk/schema/common/v2/", "siteSpecifics");
    private final static QName _TIAObjectExternalId_QNAME = new QName("http://infrastructure.tia.dk/schema/common/v2/", "externalId");
    private final static QName _TIAObjectCountrySpecifics_QNAME = new QName("http://infrastructure.tia.dk/schema/common/v2/", "countrySpecifics");
    private final static QName _TIAObjectFlexfieldCollection_QNAME = new QName("http://infrastructure.tia.dk/schema/common/v2/", "flexfieldCollection");
    private final static QName _TIAObjectVersionToken_QNAME = new QName("http://infrastructure.tia.dk/schema/common/v2/", "versionToken");
    private final static QName _SearchTokenAccountNameId_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "nameId");
    private final static QName _SearchTokenAccountAccountType_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "accountType");
    private final static QName _PlannedInstalmentDescription_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "description");
    private final static QName _PlannedInstalmentAccItemNo_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "accItemNo");
    private final static QName _PlannedInstalmentFromAccItemNo_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "fromAccItemNo");
    private final static QName _PlannedInstalmentCollectionGroup_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "collectionGroup");
    private final static QName _PlannedInstalmentTransId_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "transId");
    private final static QName _PaymentAmountIn_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "amountIn");
    private final static QName _PaymentTransactionDateOut_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "transactionDateOut");
    private final static QName _PaymentBatchOut_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "batchOut");
    private final static QName _PaymentPaymentDetailsText_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "paymentDetailsText");
    private final static QName _PaymentBatchRun_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "batchRun");
    private final static QName _PaymentPaymentInstruction_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "paymentInstruction");
    private final static QName _PaymentValueDateIn_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "valueDateIn");
    private final static QName _PaymentPaymentStatusOut_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "paymentStatusOut");
    private final static QName _PaymentTransactionDateIn_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "transactionDateIn");
    private final static QName _PaymentRecollectionTransId_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "recollectionTransId");
    private final static QName _PaymentReceiverNameAddress_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "receiverNameAddress");
    private final static QName _PaymentBatchIn_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "batchIn");
    private final static QName _AccountItemMatchCurrencyAmount_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "currencyAmount");
    private final static QName _AccountItemMatchLossProfit_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "lossProfit");
    private final static QName _AccountItemMatchOtherCurrencyAmount_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "otherCurrencyAmount");
    private final static QName _AccountItemMatchOtherAccountItemNo_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "otherAccountItemNo");
    private final static QName _AccountItemMatchOtherAmount_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "otherAmount");
    private final static QName _AccountItemMatchMatchDate_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "matchDate");
    private final static QName _AccountItemMatchBaseCurrencyCode_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "baseCurrencyCode");
    private final static QName _AccountItemMatchOtherCurrencyCode_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "otherCurrencyCode");
    private final static QName _GetTokenInstalmentPlanPostedYN_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "postedYN");
    private final static QName _ExchangeRateRateType_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "rateType");
    private final static QName _ExchangeRateExchangeRate_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "exchangeRate");
    private final static QName _PaymentMethodPaymentInYN_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "paymentInYN");
    private final static QName _PaymentMethodPaymentOutYN_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "paymentOutYN");
    private final static QName _PaymentMethodApproveCollectionsYN_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "approveCollectionsYN");
    private final static QName _PaymentMethodApprovePaymentsYN_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "approvePaymentsYN");
    private final static QName _PaymentMethodDeferredAllocationYN_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "deferredAllocationYN");
    private final static QName _PaymentMethodDaysBeforeDuedate_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "daysBeforeDuedate");
    private final static QName _FilterTokenAccItemOpenItemsOnlyYN_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "openItemsOnlyYN");
    private final static QName _FilterTokenAccItemDateType_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "dateType");
    private final static QName _AccountItemComplaintNo_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "complaintNo");
    private final static QName _AccountItemCreateDate_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "createDate");
    private final static QName _AccountItemCurrencyBalance_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "currencyBalance");
    private final static QName _AccountItemReferralCode_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "referralCode");
    private final static QName _AccountItemBankCostCurrencyAmt_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "bankCostCurrencyAmt");
    private final static QName _AccountItemYearOfOrigin_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "yearOfOrigin");
    private final static QName _AccountItemTransferredFromAccItemNo_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "transferredFromAccItemNo");
    private final static QName _AccountItemOrigTransId_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "origTransId");
    private final static QName _AccountItemReiContractSeqNo_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "reiContractSeqNo");
    private final static QName _AccountItemCurrencyRate_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "currencyRate");
    private final static QName _AccountItemMoneyClass_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "moneyClass");
    private final static QName _AccountItemPolicyYear_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "policyYear");
    private final static QName _AccountItemReminderTransDate_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "reminderTransDate");
    private final static QName _AccountItemInternalNote_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "internalNote");
    private final static QName _AccountItemReleaseBy_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "releaseBy");
    private final static QName _AccountItemReiContractNo_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "reiContractNo");
    private final static QName _AccountItemValueDate_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "valueDate");
    private final static QName _AccountItemBrokerId_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "brokerId");
    private final static QName _AccountItemActualCurrencyCode_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "actualCurrencyCode");
    private final static QName _AccountItemVatCode_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "vatCode");
    private final static QName _AccountItemPayee_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "payee");
    private final static QName _AccountItemBalance_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "balance");
    private final static QName _AccountItemActualCurrencyAmt_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "actualCurrencyAmt");
    private final static QName _AccountItemReversedToAccItemNo_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "reversedToAccItemNo");
    private final static QName _AccountItemCreatedByUserid_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "createdByUserid");
    private final static QName _AccountItemSubClaimNo_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "subClaimNo");
    private final static QName _AccountItemVatCurrencyAmt_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "vatCurrencyAmt");
    private final static QName _AccountItemPaymentComment_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "paymentComment");
    private final static QName _AccountItemHoldUntilPaid_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "holdUntilPaid");
    private final static QName _AccountItemOnHold_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "onHold");
    private final static QName _ReminderPostponementPostponementNo_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "postponementNo");
    private final static QName _ReminderPostponementCreator_QNAME = new QName("http://infrastructure.tia.dk/schema/account/v2/", "creator");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: za.co.ominsure.synapse.iwyze.lob.backend.search.payment.vo
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetRemindingHistoryRequest }
     * 
     */
    public GetRemindingHistoryRequest createGetRemindingHistoryRequest() {
        return new GetRemindingHistoryRequest();
    }

    /**
     * Create an instance of {@link InputToken }
     * 
     */
    public InputToken createInputToken() {
        return new InputToken();
    }

    /**
     * Create an instance of {@link FilterTokenRemHistory }
     * 
     */
    public FilterTokenRemHistory createFilterTokenRemHistory() {
        return new FilterTokenRemHistory();
    }

    /**
     * Create an instance of {@link ModifyReminderPostponementRequest }
     * 
     */
    public ModifyReminderPostponementRequest createModifyReminderPostponementRequest() {
        return new ModifyReminderPostponementRequest();
    }

    /**
     * Create an instance of {@link ReminderPostponement }
     * 
     */
    public ReminderPostponement createReminderPostponement() {
        return new ReminderPostponement();
    }

    /**
     * Create an instance of {@link RemoveReminderPostponementRequest }
     * 
     */
    public RemoveReminderPostponementRequest createRemoveReminderPostponementRequest() {
        return new RemoveReminderPostponementRequest();
    }

    /**
     * Create an instance of {@link GetAccountItemsResponse }
     * 
     */
    public GetAccountItemsResponse createGetAccountItemsResponse() {
        return new GetAccountItemsResponse();
    }

    /**
     * Create an instance of {@link AccountItemCollection }
     * 
     */
    public AccountItemCollection createAccountItemCollection() {
        return new AccountItemCollection();
    }

    /**
     * Create an instance of {@link Result }
     * 
     */
    public Result createResult() {
        return new Result();
    }

    /**
     * Create an instance of {@link GetPaymentMethodsResponse }
     * 
     */
    public GetPaymentMethodsResponse createGetPaymentMethodsResponse() {
        return new GetPaymentMethodsResponse();
    }

    /**
     * Create an instance of {@link PaymentMethodCollection }
     * 
     */
    public PaymentMethodCollection createPaymentMethodCollection() {
        return new PaymentMethodCollection();
    }

    /**
     * Create an instance of {@link GetRemindingHistoryResponse }
     * 
     */
    public GetRemindingHistoryResponse createGetRemindingHistoryResponse() {
        return new GetRemindingHistoryResponse();
    }

    /**
     * Create an instance of {@link RemindingHistoryCollection }
     * 
     */
    public RemindingHistoryCollection createRemindingHistoryCollection() {
        return new RemindingHistoryCollection();
    }

    /**
     * Create an instance of {@link GetPaymentDetailsRequest }
     * 
     */
    public GetPaymentDetailsRequest createGetPaymentDetailsRequest() {
        return new GetPaymentDetailsRequest();
    }

    /**
     * Create an instance of {@link RegisterCollectionRequestResponse }
     * 
     */
    public RegisterCollectionRequestResponse createRegisterCollectionRequestResponse() {
        return new RegisterCollectionRequestResponse();
    }

    /**
     * Create an instance of {@link ModifyReminderPostponementResponse }
     * 
     */
    public ModifyReminderPostponementResponse createModifyReminderPostponementResponse() {
        return new ModifyReminderPostponementResponse();
    }

    /**
     * Create an instance of {@link RegisterCollectionRequestRequest }
     * 
     */
    public RegisterCollectionRequestRequest createRegisterCollectionRequestRequest() {
        return new RegisterCollectionRequestRequest();
    }

    /**
     * Create an instance of {@link CollectionRequestConfirm }
     * 
     */
    public CollectionRequestConfirm createCollectionRequestConfirm() {
        return new CollectionRequestConfirm();
    }

    /**
     * Create an instance of {@link SearchPaymentResponse }
     * 
     */
    public SearchPaymentResponse createSearchPaymentResponse() {
        return new SearchPaymentResponse();
    }

    /**
     * Create an instance of {@link PaymentCollection }
     * 
     */
    public PaymentCollection createPaymentCollection() {
        return new PaymentCollection();
    }

    /**
     * Create an instance of {@link GetPaymentMethodsRequest }
     * 
     */
    public GetPaymentMethodsRequest createGetPaymentMethodsRequest() {
        return new GetPaymentMethodsRequest();
    }

    /**
     * Create an instance of {@link SearchPaymentRequest }
     * 
     */
    public SearchPaymentRequest createSearchPaymentRequest() {
        return new SearchPaymentRequest();
    }

    /**
     * Create an instance of {@link SearchTokenPayment }
     * 
     */
    public SearchTokenPayment createSearchTokenPayment() {
        return new SearchTokenPayment();
    }

    /**
     * Create an instance of {@link GetPaymentChannelsResponse }
     * 
     */
    public GetPaymentChannelsResponse createGetPaymentChannelsResponse() {
        return new GetPaymentChannelsResponse();
    }

    /**
     * Create an instance of {@link PaymentChannelCollection }
     * 
     */
    public PaymentChannelCollection createPaymentChannelCollection() {
        return new PaymentChannelCollection();
    }

    /**
     * Create an instance of {@link GetAccountStatementRequest }
     * 
     */
    public GetAccountStatementRequest createGetAccountStatementRequest() {
        return new GetAccountStatementRequest();
    }

    /**
     * Create an instance of {@link FilterTokenAccStatement }
     * 
     */
    public FilterTokenAccStatement createFilterTokenAccStatement() {
        return new FilterTokenAccStatement();
    }

    /**
     * Create an instance of {@link PageSort }
     * 
     */
    public PageSort createPageSort() {
        return new PageSort();
    }

    /**
     * Create an instance of {@link GetPaymentTermsRequest }
     * 
     */
    public GetPaymentTermsRequest createGetPaymentTermsRequest() {
        return new GetPaymentTermsRequest();
    }

    /**
     * Create an instance of {@link SearchAccountResponse }
     * 
     */
    public SearchAccountResponse createSearchAccountResponse() {
        return new SearchAccountResponse();
    }

    /**
     * Create an instance of {@link AccountCollection }
     * 
     */
    public AccountCollection createAccountCollection() {
        return new AccountCollection();
    }

    /**
     * Create an instance of {@link CreateAccountItemRequest }
     * 
     */
    public CreateAccountItemRequest createCreateAccountItemRequest() {
        return new CreateAccountItemRequest();
    }

    /**
     * Create an instance of {@link AccountItem }
     * 
     */
    public AccountItem createAccountItem() {
        return new AccountItem();
    }

    /**
     * Create an instance of {@link CreatePaymentDetailsRequest }
     * 
     */
    public CreatePaymentDetailsRequest createCreatePaymentDetailsRequest() {
        return new CreatePaymentDetailsRequest();
    }

    /**
     * Create an instance of {@link PaymentDetails }
     * 
     */
    public PaymentDetails createPaymentDetails() {
        return new PaymentDetails();
    }

    /**
     * Create an instance of {@link CreatePaymentDetailsResponse }
     * 
     */
    public CreatePaymentDetailsResponse createCreatePaymentDetailsResponse() {
        return new CreatePaymentDetailsResponse();
    }

    /**
     * Create an instance of {@link GetAccountItemsRequest }
     * 
     */
    public GetAccountItemsRequest createGetAccountItemsRequest() {
        return new GetAccountItemsRequest();
    }

    /**
     * Create an instance of {@link FilterTokenAccItem }
     * 
     */
    public FilterTokenAccItem createFilterTokenAccItem() {
        return new FilterTokenAccItem();
    }

    /**
     * Create an instance of {@link CreateAccountItemResponse }
     * 
     */
    public CreateAccountItemResponse createCreateAccountItemResponse() {
        return new CreateAccountItemResponse();
    }

    /**
     * Create an instance of {@link SearchAccountRequest }
     * 
     */
    public SearchAccountRequest createSearchAccountRequest() {
        return new SearchAccountRequest();
    }

    /**
     * Create an instance of {@link SearchTokenAccount }
     * 
     */
    public SearchTokenAccount createSearchTokenAccount() {
        return new SearchTokenAccount();
    }

    /**
     * Create an instance of {@link FaultMessageDetail }
     * 
     */
    public FaultMessageDetail createFaultMessageDetail() {
        return new FaultMessageDetail();
    }

    /**
     * Create an instance of {@link AcknowledgeIncomingPaymentRequest }
     * 
     */
    public AcknowledgeIncomingPaymentRequest createAcknowledgeIncomingPaymentRequest() {
        return new AcknowledgeIncomingPaymentRequest();
    }

    /**
     * Create an instance of {@link CollectionAcknowledgement }
     * 
     */
    public CollectionAcknowledgement createCollectionAcknowledgement() {
        return new CollectionAcknowledgement();
    }

    /**
     * Create an instance of {@link GetReminderPostponementsRequest }
     * 
     */
    public GetReminderPostponementsRequest createGetReminderPostponementsRequest() {
        return new GetReminderPostponementsRequest();
    }

    /**
     * Create an instance of {@link GetPaymentChannelsRequest }
     * 
     */
    public GetPaymentChannelsRequest createGetPaymentChannelsRequest() {
        return new GetPaymentChannelsRequest();
    }

    /**
     * Create an instance of {@link AcknowledgeIncomingPaymentResponse }
     * 
     */
    public AcknowledgeIncomingPaymentResponse createAcknowledgeIncomingPaymentResponse() {
        return new AcknowledgeIncomingPaymentResponse();
    }

    /**
     * Create an instance of {@link ModifyPaymentDetailsResponse }
     * 
     */
    public ModifyPaymentDetailsResponse createModifyPaymentDetailsResponse() {
        return new ModifyPaymentDetailsResponse();
    }

    /**
     * Create an instance of {@link GetPaymentDetailsResponse }
     * 
     */
    public GetPaymentDetailsResponse createGetPaymentDetailsResponse() {
        return new GetPaymentDetailsResponse();
    }

    /**
     * Create an instance of {@link PaymentDetailsCollection }
     * 
     */
    public PaymentDetailsCollection createPaymentDetailsCollection() {
        return new PaymentDetailsCollection();
    }

    /**
     * Create an instance of {@link CreateReminderPostponementRequest }
     * 
     */
    public CreateReminderPostponementRequest createCreateReminderPostponementRequest() {
        return new CreateReminderPostponementRequest();
    }

    /**
     * Create an instance of {@link ModifyPaymentDetailsRequest }
     * 
     */
    public ModifyPaymentDetailsRequest createModifyPaymentDetailsRequest() {
        return new ModifyPaymentDetailsRequest();
    }

    /**
     * Create an instance of {@link PreparePolicyPremiumPaymentResponse }
     * 
     */
    public PreparePolicyPremiumPaymentResponse createPreparePolicyPremiumPaymentResponse() {
        return new PreparePolicyPremiumPaymentResponse();
    }

    /**
     * Create an instance of {@link Payment }
     * 
     */
    public Payment createPayment() {
        return new Payment();
    }

    /**
     * Create an instance of {@link CreateReminderPostponementResponse }
     * 
     */
    public CreateReminderPostponementResponse createCreateReminderPostponementResponse() {
        return new CreateReminderPostponementResponse();
    }

    /**
     * Create an instance of {@link RemoveReminderPostponementResponse }
     * 
     */
    public RemoveReminderPostponementResponse createRemoveReminderPostponementResponse() {
        return new RemoveReminderPostponementResponse();
    }

    /**
     * Create an instance of {@link GetReminderPostponementsResponse }
     * 
     */
    public GetReminderPostponementsResponse createGetReminderPostponementsResponse() {
        return new GetReminderPostponementsResponse();
    }

    /**
     * Create an instance of {@link ReminderPostponementCollection }
     * 
     */
    public ReminderPostponementCollection createReminderPostponementCollection() {
        return new ReminderPostponementCollection();
    }

    /**
     * Create an instance of {@link GetAccountStatementResponse }
     * 
     */
    public GetAccountStatementResponse createGetAccountStatementResponse() {
        return new GetAccountStatementResponse();
    }

    /**
     * Create an instance of {@link AccountStatement }
     * 
     */
    public AccountStatement createAccountStatement() {
        return new AccountStatement();
    }

    /**
     * Create an instance of {@link GetPaymentTermsResponse }
     * 
     */
    public GetPaymentTermsResponse createGetPaymentTermsResponse() {
        return new GetPaymentTermsResponse();
    }

    /**
     * Create an instance of {@link PaymentTermsCollection }
     * 
     */
    public PaymentTermsCollection createPaymentTermsCollection() {
        return new PaymentTermsCollection();
    }

    /**
     * Create an instance of {@link PreparePolicyPremiumPaymentRequest }
     * 
     */
    public PreparePolicyPremiumPaymentRequest createPreparePolicyPremiumPaymentRequest() {
        return new PreparePolicyPremiumPaymentRequest();
    }

    /**
     * Create an instance of {@link Account }
     * 
     */
    public Account createAccount() {
        return new Account();
    }

    /**
     * Create an instance of {@link InstalmentPlan }
     * 
     */
    public InstalmentPlan createInstalmentPlan() {
        return new InstalmentPlan();
    }

    /**
     * Create an instance of {@link PlannedInstalmentCollection }
     * 
     */
    public PlannedInstalmentCollection createPlannedInstalmentCollection() {
        return new PlannedInstalmentCollection();
    }

    /**
     * Create an instance of {@link PaymentSpecificationCollection }
     * 
     */
    public PaymentSpecificationCollection createPaymentSpecificationCollection() {
        return new PaymentSpecificationCollection();
    }

    /**
     * Create an instance of {@link ReminderSpecificationCollection }
     * 
     */
    public ReminderSpecificationCollection createReminderSpecificationCollection() {
        return new ReminderSpecificationCollection();
    }

    /**
     * Create an instance of {@link AccountStatementItem }
     * 
     */
    public AccountStatementItem createAccountStatementItem() {
        return new AccountStatementItem();
    }

    /**
     * Create an instance of {@link ExchangeRateCollection }
     * 
     */
    public ExchangeRateCollection createExchangeRateCollection() {
        return new ExchangeRateCollection();
    }

    /**
     * Create an instance of {@link Reminder }
     * 
     */
    public Reminder createReminder() {
        return new Reminder();
    }

    /**
     * Create an instance of {@link PaymentItemCollection }
     * 
     */
    public PaymentItemCollection createPaymentItemCollection() {
        return new PaymentItemCollection();
    }

    /**
     * Create an instance of {@link RemindingHistory }
     * 
     */
    public RemindingHistory createRemindingHistory() {
        return new RemindingHistory();
    }

    /**
     * Create an instance of {@link PaymentItem }
     * 
     */
    public PaymentItem createPaymentItem() {
        return new PaymentItem();
    }

    /**
     * Create an instance of {@link RemindedItem }
     * 
     */
    public RemindedItem createRemindedItem() {
        return new RemindedItem();
    }

    /**
     * Create an instance of {@link ReminderCollection }
     * 
     */
    public ReminderCollection createReminderCollection() {
        return new ReminderCollection();
    }

    /**
     * Create an instance of {@link AccountStatementItemCollection }
     * 
     */
    public AccountStatementItemCollection createAccountStatementItemCollection() {
        return new AccountStatementItemCollection();
    }

    /**
     * Create an instance of {@link AccountItemMatchCollection }
     * 
     */
    public AccountItemMatchCollection createAccountItemMatchCollection() {
        return new AccountItemMatchCollection();
    }

    /**
     * Create an instance of {@link ReminderSpecification }
     * 
     */
    public ReminderSpecification createReminderSpecification() {
        return new ReminderSpecification();
    }

    /**
     * Create an instance of {@link InstalmentPlanCollection }
     * 
     */
    public InstalmentPlanCollection createInstalmentPlanCollection() {
        return new InstalmentPlanCollection();
    }

    /**
     * Create an instance of {@link CurrencyCollection }
     * 
     */
    public CurrencyCollection createCurrencyCollection() {
        return new CurrencyCollection();
    }

    /**
     * Create an instance of {@link PaymentMethod }
     * 
     */
    public PaymentMethod createPaymentMethod() {
        return new PaymentMethod();
    }

    /**
     * Create an instance of {@link GetTokenInstalmentPlan }
     * 
     */
    public GetTokenInstalmentPlan createGetTokenInstalmentPlan() {
        return new GetTokenInstalmentPlan();
    }

    /**
     * Create an instance of {@link ExchangeRate }
     * 
     */
    public ExchangeRate createExchangeRate() {
        return new ExchangeRate();
    }

    /**
     * Create an instance of {@link PaymentChannel }
     * 
     */
    public PaymentChannel createPaymentChannel() {
        return new PaymentChannel();
    }

    /**
     * Create an instance of {@link PaymentTerms }
     * 
     */
    public PaymentTerms createPaymentTerms() {
        return new PaymentTerms();
    }

    /**
     * Create an instance of {@link RemindedItemCollection }
     * 
     */
    public RemindedItemCollection createRemindedItemCollection() {
        return new RemindedItemCollection();
    }

    /**
     * Create an instance of {@link PlannedInstalment }
     * 
     */
    public PlannedInstalment createPlannedInstalment() {
        return new PlannedInstalment();
    }

    /**
     * Create an instance of {@link AccountItemMatch }
     * 
     */
    public AccountItemMatch createAccountItemMatch() {
        return new AccountItemMatch();
    }

    /**
     * Create an instance of {@link PaymentSpecification }
     * 
     */
    public PaymentSpecification createPaymentSpecification() {
        return new PaymentSpecification();
    }

    /**
     * Create an instance of {@link FaultMessageDetail2 }
     * 
     */
    public FaultMessageDetail2 createFaultMessageDetail2() {
        return new FaultMessageDetail2();
    }

    /**
     * Create an instance of {@link FlexfieldCollection }
     * 
     */
    public FlexfieldCollection createFlexfieldCollection() {
        return new FlexfieldCollection();
    }

    /**
     * Create an instance of {@link ValueTranslationPolicy }
     * 
     */
    public ValueTranslationPolicy createValueTranslationPolicy() {
        return new ValueTranslationPolicy();
    }

    /**
     * Create an instance of {@link NumberAttribute }
     * 
     */
    public NumberAttribute createNumberAttribute() {
        return new NumberAttribute();
    }

    /**
     * Create an instance of {@link KeyPair }
     * 
     */
    public KeyPair createKeyPair() {
        return new KeyPair();
    }

    /**
     * Create an instance of {@link TranslatedCode }
     * 
     */
    public TranslatedCode createTranslatedCode() {
        return new TranslatedCode();
    }

    /**
     * Create an instance of {@link ValueTranslationCollection }
     * 
     */
    public ValueTranslationCollection createValueTranslationCollection() {
        return new ValueTranslationCollection();
    }

    /**
     * Create an instance of {@link VersionToken }
     * 
     */
    public VersionToken createVersionToken() {
        return new VersionToken();
    }

    /**
     * Create an instance of {@link StringCollection }
     * 
     */
    public StringCollection createStringCollection() {
        return new StringCollection();
    }

    /**
     * Create an instance of {@link Attribute }
     * 
     */
    public Attribute createAttribute() {
        return new Attribute();
    }

    /**
     * Create an instance of {@link ValueTranslationClaim }
     * 
     */
    public ValueTranslationClaim createValueTranslationClaim() {
        return new ValueTranslationClaim();
    }

    /**
     * Create an instance of {@link VarcharAttribute }
     * 
     */
    public VarcharAttribute createVarcharAttribute() {
        return new VarcharAttribute();
    }

    /**
     * Create an instance of {@link MessageDetail }
     * 
     */
    public MessageDetail createMessageDetail() {
        return new MessageDetail();
    }

    /**
     * Create an instance of {@link KeyPairCollection }
     * 
     */
    public KeyPairCollection createKeyPairCollection() {
        return new KeyPairCollection();
    }

    /**
     * Create an instance of {@link Email }
     * 
     */
    public Email createEmail() {
        return new Email();
    }

    /**
     * Create an instance of {@link TranslatedCodeCollection }
     * 
     */
    public TranslatedCodeCollection createTranslatedCodeCollection() {
        return new TranslatedCodeCollection();
    }

    /**
     * Create an instance of {@link Flexfield }
     * 
     */
    public Flexfield createFlexfield() {
        return new Flexfield();
    }

    /**
     * Create an instance of {@link DateAttribute }
     * 
     */
    public DateAttribute createDateAttribute() {
        return new DateAttribute();
    }

    /**
     * Create an instance of {@link NumberCollection }
     * 
     */
    public NumberCollection createNumberCollection() {
        return new NumberCollection();
    }

    /**
     * Create an instance of {@link Value }
     * 
     */
    public Value createValue() {
        return new Value();
    }

    /**
     * Create an instance of {@link SiteSpecifics }
     * 
     */
    public SiteSpecifics createSiteSpecifics() {
        return new SiteSpecifics();
    }

    /**
     * Create an instance of {@link ValueTranslationPolicyCollection }
     * 
     */
    public ValueTranslationPolicyCollection createValueTranslationPolicyCollection() {
        return new ValueTranslationPolicyCollection();
    }

    /**
     * Create an instance of {@link ValueTranslation }
     * 
     */
    public ValueTranslation createValueTranslation() {
        return new ValueTranslation();
    }

    /**
     * Create an instance of {@link MessageCollection }
     * 
     */
    public MessageCollection createMessageCollection() {
        return new MessageCollection();
    }

    /**
     * Create an instance of {@link Number }
     * 
     */
    public Number createNumber() {
        return new Number();
    }

    /**
     * Create an instance of {@link CountrySpecifics }
     * 
     */
    public CountrySpecifics createCountrySpecifics() {
        return new CountrySpecifics();
    }

    /**
     * Create an instance of {@link EntityAttributeCollection }
     * 
     */
    public EntityAttributeCollection createEntityAttributeCollection() {
        return new EntityAttributeCollection();
    }

    /**
     * Create an instance of {@link ValueTranslationClaimCollection }
     * 
     */
    public ValueTranslationClaimCollection createValueTranslationClaimCollection() {
        return new ValueTranslationClaimCollection();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "policyNo", scope = AccountStatement.class)
    public JAXBElement<BigInteger> createAccountStatementPolicyNo(BigInteger value) {
        return new JAXBElement<BigInteger>(_AccountStatementPolicyNo_QNAME, BigInteger.class, AccountStatement.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "mpPolicyNo", scope = AccountStatement.class)
    public JAXBElement<Long> createAccountStatementMpPolicyNo(Long value) {
        return new JAXBElement<Long>(_AccountStatementMpPolicyNo_QNAME, Long.class, AccountStatement.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "fromDate", scope = AccountStatement.class)
    public JAXBElement<XMLGregorianCalendar> createAccountStatementFromDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_AccountStatementFromDate_QNAME, XMLGregorianCalendar.class, AccountStatement.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "claimNo", scope = AccountStatement.class)
    public JAXBElement<Long> createAccountStatementClaimNo(Long value) {
        return new JAXBElement<Long>(_AccountStatementClaimNo_QNAME, Long.class, AccountStatement.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "policyLineNo", scope = AccountStatement.class)
    public JAXBElement<BigInteger> createAccountStatementPolicyLineNo(BigInteger value) {
        return new JAXBElement<BigInteger>(_AccountStatementPolicyLineNo_QNAME, BigInteger.class, AccountStatement.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "toDate", scope = AccountStatement.class)
    public JAXBElement<XMLGregorianCalendar> createAccountStatementToDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_AccountStatementToDate_QNAME, XMLGregorianCalendar.class, AccountStatement.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "dueDateFrom", scope = SearchTokenPayment.class)
    public JAXBElement<XMLGregorianCalendar> createSearchTokenPaymentDueDateFrom(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_SearchTokenPaymentDueDateFrom_QNAME, XMLGregorianCalendar.class, SearchTokenPayment.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "paymentMethod", scope = SearchTokenPayment.class)
    public JAXBElement<String> createSearchTokenPaymentPaymentMethod(String value) {
        return new JAXBElement<String>(_SearchTokenPaymentPaymentMethod_QNAME, String.class, SearchTokenPayment.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "currencyCode", scope = SearchTokenPayment.class)
    public JAXBElement<String> createSearchTokenPaymentCurrencyCode(String value) {
        return new JAXBElement<String>(_SearchTokenPaymentCurrencyCode_QNAME, String.class, SearchTokenPayment.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "transactionDateTo", scope = SearchTokenPayment.class)
    public JAXBElement<XMLGregorianCalendar> createSearchTokenPaymentTransactionDateTo(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_SearchTokenPaymentTransactionDateTo_QNAME, XMLGregorianCalendar.class, SearchTokenPayment.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "paymentStatus", scope = SearchTokenPayment.class)
    public JAXBElement<Integer> createSearchTokenPaymentPaymentStatus(Integer value) {
        return new JAXBElement<Integer>(_SearchTokenPaymentPaymentStatus_QNAME, Integer.class, SearchTokenPayment.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "paymentNo", scope = SearchTokenPayment.class)
    public JAXBElement<Long> createSearchTokenPaymentPaymentNo(Long value) {
        return new JAXBElement<Long>(_SearchTokenPaymentPaymentNo_QNAME, Long.class, SearchTokenPayment.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "amountTo", scope = SearchTokenPayment.class)
    public JAXBElement<BigDecimal> createSearchTokenPaymentAmountTo(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_SearchTokenPaymentAmountTo_QNAME, BigDecimal.class, SearchTokenPayment.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "accountNo", scope = SearchTokenPayment.class)
    public JAXBElement<Long> createSearchTokenPaymentAccountNo(Long value) {
        return new JAXBElement<Long>(_SearchTokenPaymentAccountNo_QNAME, Long.class, SearchTokenPayment.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "amountFrom", scope = SearchTokenPayment.class)
    public JAXBElement<BigDecimal> createSearchTokenPaymentAmountFrom(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_SearchTokenPaymentAmountFrom_QNAME, BigDecimal.class, SearchTokenPayment.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "externalRefIn", scope = SearchTokenPayment.class)
    public JAXBElement<String> createSearchTokenPaymentExternalRefIn(String value) {
        return new JAXBElement<String>(_SearchTokenPaymentExternalRefIn_QNAME, String.class, SearchTokenPayment.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "policyNo", scope = SearchTokenPayment.class)
    public JAXBElement<Long> createSearchTokenPaymentPolicyNo(Long value) {
        return new JAXBElement<Long>(_AccountStatementPolicyNo_QNAME, Long.class, SearchTokenPayment.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "dueDateTo", scope = SearchTokenPayment.class)
    public JAXBElement<XMLGregorianCalendar> createSearchTokenPaymentDueDateTo(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_SearchTokenPaymentDueDateTo_QNAME, XMLGregorianCalendar.class, SearchTokenPayment.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "transactionDateFrom", scope = SearchTokenPayment.class)
    public JAXBElement<XMLGregorianCalendar> createSearchTokenPaymentTransactionDateFrom(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_SearchTokenPaymentTransactionDateFrom_QNAME, XMLGregorianCalendar.class, SearchTokenPayment.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "claimNo", scope = SearchTokenPayment.class)
    public JAXBElement<Long> createSearchTokenPaymentClaimNo(Long value) {
        return new JAXBElement<Long>(_AccountStatementClaimNo_QNAME, Long.class, SearchTokenPayment.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "paymentMethod", scope = PaymentDetails.class)
    public JAXBElement<String> createPaymentDetailsPaymentMethod(String value) {
        return new JAXBElement<String>(_SearchTokenPaymentPaymentMethod_QNAME, String.class, PaymentDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "externalPaymentId", scope = PaymentDetails.class)
    public JAXBElement<String> createPaymentDetailsExternalPaymentId(String value) {
        return new JAXBElement<String>(_PaymentDetailsExternalPaymentId_QNAME, String.class, PaymentDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "paymentDetailsId", scope = PaymentDetails.class)
    public JAXBElement<Long> createPaymentDetailsPaymentDetailsId(Long value) {
        return new JAXBElement<Long>(_PaymentDetailsPaymentDetailsId_QNAME, Long.class, PaymentDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "startDate", scope = PaymentDetails.class)
    public JAXBElement<XMLGregorianCalendar> createPaymentDetailsStartDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_PaymentDetailsStartDate_QNAME, XMLGregorianCalendar.class, PaymentDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "bankIdNo", scope = PaymentDetails.class)
    public JAXBElement<Long> createPaymentDetailsBankIdNo(Long value) {
        return new JAXBElement<Long>(_PaymentDetailsBankIdNo_QNAME, Long.class, PaymentDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "bankCodeType", scope = PaymentDetails.class)
    public JAXBElement<String> createPaymentDetailsBankCodeType(String value) {
        return new JAXBElement<String>(_PaymentDetailsBankCodeType_QNAME, String.class, PaymentDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "identification", scope = PaymentDetails.class)
    public JAXBElement<String> createPaymentDetailsIdentification(String value) {
        return new JAXBElement<String>(_PaymentDetailsIdentification_QNAME, String.class, PaymentDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "creditorNo", scope = PaymentDetails.class)
    public JAXBElement<String> createPaymentDetailsCreditorNo(String value) {
        return new JAXBElement<String>(_PaymentDetailsCreditorNo_QNAME, String.class, PaymentDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "nameIdNo", scope = PaymentDetails.class)
    public JAXBElement<Long> createPaymentDetailsNameIdNo(Long value) {
        return new JAXBElement<Long>(_PaymentDetailsNameIdNo_QNAME, Long.class, PaymentDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "bankAccountNo", scope = PaymentDetails.class)
    public JAXBElement<String> createPaymentDetailsBankAccountNo(String value) {
        return new JAXBElement<String>(_PaymentDetailsBankAccountNo_QNAME, String.class, PaymentDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "ccContinuousDate", scope = PaymentDetails.class)
    public JAXBElement<XMLGregorianCalendar> createPaymentDetailsCcContinuousDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_PaymentDetailsCcContinuousDate_QNAME, XMLGregorianCalendar.class, PaymentDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "accountHolderName", scope = PaymentDetails.class)
    public JAXBElement<String> createPaymentDetailsAccountHolderName(String value) {
        return new JAXBElement<String>(_PaymentDetailsAccountHolderName_QNAME, String.class, PaymentDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "giroType", scope = PaymentDetails.class)
    public JAXBElement<String> createPaymentDetailsGiroType(String value) {
        return new JAXBElement<String>(_PaymentDetailsGiroType_QNAME, String.class, PaymentDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "paymentChannel", scope = PaymentDetails.class)
    public JAXBElement<String> createPaymentDetailsPaymentChannel(String value) {
        return new JAXBElement<String>(_PaymentDetailsPaymentChannel_QNAME, String.class, PaymentDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "ccNo", scope = PaymentDetails.class)
    public JAXBElement<Long> createPaymentDetailsCcNo(Long value) {
        return new JAXBElement<Long>(_PaymentDetailsCcNo_QNAME, Long.class, PaymentDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "bankCode", scope = PaymentDetails.class)
    public JAXBElement<String> createPaymentDetailsBankCode(String value) {
        return new JAXBElement<String>(_PaymentDetailsBankCode_QNAME, String.class, PaymentDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "ccContinuous", scope = PaymentDetails.class)
    public JAXBElement<String> createPaymentDetailsCcContinuous(String value) {
        return new JAXBElement<String>(_PaymentDetailsCcContinuous_QNAME, String.class, PaymentDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "ccExpiryDate", scope = PaymentDetails.class)
    public JAXBElement<XMLGregorianCalendar> createPaymentDetailsCcExpiryDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_PaymentDetailsCcExpiryDate_QNAME, XMLGregorianCalendar.class, PaymentDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "remindedAmount", scope = RemindedItem.class)
    public JAXBElement<BigDecimal> createRemindedItemRemindedAmount(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_RemindedItemRemindedAmount_QNAME, BigDecimal.class, RemindedItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "accountItemNo", scope = RemindedItem.class)
    public JAXBElement<Long> createRemindedItemAccountItemNo(Long value) {
        return new JAXBElement<Long>(_RemindedItemAccountItemNo_QNAME, Long.class, RemindedItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "policyNo", scope = InstalmentPlan.class)
    public JAXBElement<Long> createInstalmentPlanPolicyNo(Long value) {
        return new JAXBElement<Long>(_AccountStatementPolicyNo_QNAME, Long.class, InstalmentPlan.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "planStartDate", scope = InstalmentPlan.class)
    public JAXBElement<XMLGregorianCalendar> createInstalmentPlanPlanStartDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_InstalmentPlanPlanStartDate_QNAME, XMLGregorianCalendar.class, InstalmentPlan.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "currencyCode", scope = InstalmentPlan.class)
    public JAXBElement<String> createInstalmentPlanCurrencyCode(String value) {
        return new JAXBElement<String>(_SearchTokenPaymentCurrencyCode_QNAME, String.class, InstalmentPlan.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "planEndDate", scope = InstalmentPlan.class)
    public JAXBElement<XMLGregorianCalendar> createInstalmentPlanPlanEndDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_InstalmentPlanPlanEndDate_QNAME, XMLGregorianCalendar.class, InstalmentPlan.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "mpPolicyNo", scope = InstalmentPlan.class)
    public JAXBElement<Long> createInstalmentPlanMpPolicyNo(Long value) {
        return new JAXBElement<Long>(_AccountStatementMpPolicyNo_QNAME, Long.class, InstalmentPlan.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "accountNo", scope = InstalmentPlan.class)
    public JAXBElement<Long> createInstalmentPlanAccountNo(Long value) {
        return new JAXBElement<Long>(_SearchTokenPaymentAccountNo_QNAME, Long.class, InstalmentPlan.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "policyLineNo", scope = InstalmentPlan.class)
    public JAXBElement<Long> createInstalmentPlanPolicyLineNo(Long value) {
        return new JAXBElement<Long>(_AccountStatementPolicyLineNo_QNAME, Long.class, InstalmentPlan.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/common/v2/", name = "emailBCC", scope = Email.class)
    public JAXBElement<String> createEmailEmailBCC(String value) {
        return new JAXBElement<String>(_EmailEmailBCC_QNAME, String.class, Email.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/common/v2/", name = "emailCC", scope = Email.class)
    public JAXBElement<String> createEmailEmailCC(String value) {
        return new JAXBElement<String>(_EmailEmailCC_QNAME, String.class, Email.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "approvedBy", scope = PaymentItem.class)
    public JAXBElement<String> createPaymentItemApprovedBy(String value) {
        return new JAXBElement<String>(_PaymentItemApprovedBy_QNAME, String.class, PaymentItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "accountItemNo", scope = PaymentItem.class)
    public JAXBElement<Long> createPaymentItemAccountItemNo(Long value) {
        return new JAXBElement<Long>(_RemindedItemAccountItemNo_QNAME, Long.class, PaymentItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "amount", scope = PaymentItem.class)
    public JAXBElement<BigDecimal> createPaymentItemAmount(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_PaymentItemAmount_QNAME, BigDecimal.class, PaymentItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "specificationText", scope = PaymentSpecification.class)
    public JAXBElement<String> createPaymentSpecificationSpecificationText(String value) {
        return new JAXBElement<String>(_PaymentSpecificationSpecificationText_QNAME, String.class, PaymentSpecification.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "seqNo", scope = PaymentSpecification.class)
    public JAXBElement<Long> createPaymentSpecificationSeqNo(Long value) {
        return new JAXBElement<Long>(_PaymentSpecificationSeqNo_QNAME, Long.class, PaymentSpecification.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "currencyCode", scope = AccountStatementItem.class)
    public JAXBElement<String> createAccountStatementItemCurrencyCode(String value) {
        return new JAXBElement<String>(_SearchTokenPaymentCurrencyCode_QNAME, String.class, AccountStatementItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "paidAmount", scope = AccountStatementItem.class)
    public JAXBElement<BigDecimal> createAccountStatementItemPaidAmount(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_AccountStatementItemPaidAmount_QNAME, BigDecimal.class, AccountStatementItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "startDate", scope = AccountStatementItem.class)
    public JAXBElement<XMLGregorianCalendar> createAccountStatementItemStartDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_PaymentDetailsStartDate_QNAME, XMLGregorianCalendar.class, AccountStatementItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "paymentStatus", scope = AccountStatementItem.class)
    public JAXBElement<Integer> createAccountStatementItemPaymentStatus(Integer value) {
        return new JAXBElement<Integer>(_SearchTokenPaymentPaymentStatus_QNAME, Integer.class, AccountStatementItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "mpPolicyNo", scope = AccountStatementItem.class)
    public JAXBElement<Long> createAccountStatementItemMpPolicyNo(Long value) {
        return new JAXBElement<Long>(_AccountStatementMpPolicyNo_QNAME, Long.class, AccountStatementItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "itemId", scope = AccountStatementItem.class)
    public JAXBElement<Long> createAccountStatementItemItemId(Long value) {
        return new JAXBElement<Long>(_AccountStatementItemItemId_QNAME, Long.class, AccountStatementItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "itemReference", scope = AccountStatementItem.class)
    public JAXBElement<String> createAccountStatementItemItemReference(String value) {
        return new JAXBElement<String>(_AccountStatementItemItemReference_QNAME, String.class, AccountStatementItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "specification", scope = AccountStatementItem.class)
    public JAXBElement<String> createAccountStatementItemSpecification(String value) {
        return new JAXBElement<String>(_AccountStatementItemSpecification_QNAME, String.class, AccountStatementItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "policyNo", scope = AccountStatementItem.class)
    public JAXBElement<BigInteger> createAccountStatementItemPolicyNo(BigInteger value) {
        return new JAXBElement<BigInteger>(_AccountStatementPolicyNo_QNAME, BigInteger.class, AccountStatementItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "source", scope = AccountStatementItem.class)
    public JAXBElement<Integer> createAccountStatementItemSource(Integer value) {
        return new JAXBElement<Integer>(_AccountStatementItemSource_QNAME, Integer.class, AccountStatementItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "transactionDate", scope = AccountStatementItem.class)
    public JAXBElement<XMLGregorianCalendar> createAccountStatementItemTransactionDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_AccountStatementItemTransactionDate_QNAME, XMLGregorianCalendar.class, AccountStatementItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "itemText", scope = AccountStatementItem.class)
    public JAXBElement<String> createAccountStatementItemItemText(String value) {
        return new JAXBElement<String>(_AccountStatementItemItemText_QNAME, String.class, AccountStatementItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "accountItemNo", scope = AccountStatementItem.class)
    public JAXBElement<Long> createAccountStatementItemAccountItemNo(Long value) {
        return new JAXBElement<Long>(_RemindedItemAccountItemNo_QNAME, Long.class, AccountStatementItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "claimNo", scope = AccountStatementItem.class)
    public JAXBElement<Long> createAccountStatementItemClaimNo(Long value) {
        return new JAXBElement<Long>(_AccountStatementClaimNo_QNAME, Long.class, AccountStatementItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "amount", scope = AccountStatementItem.class)
    public JAXBElement<BigDecimal> createAccountStatementItemAmount(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_PaymentItemAmount_QNAME, BigDecimal.class, AccountStatementItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "paidAccountItemNo", scope = AccountStatementItem.class)
    public JAXBElement<Long> createAccountStatementItemPaidAccountItemNo(Long value) {
        return new JAXBElement<Long>(_AccountStatementItemPaidAccountItemNo_QNAME, Long.class, AccountStatementItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "paidCurrencyCode", scope = AccountStatementItem.class)
    public JAXBElement<String> createAccountStatementItemPaidCurrencyCode(String value) {
        return new JAXBElement<String>(_AccountStatementItemPaidCurrencyCode_QNAME, String.class, AccountStatementItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "endDate", scope = AccountStatementItem.class)
    public JAXBElement<XMLGregorianCalendar> createAccountStatementItemEndDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_AccountStatementItemEndDate_QNAME, XMLGregorianCalendar.class, AccountStatementItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "policyLineNo", scope = AccountStatementItem.class)
    public JAXBElement<BigInteger> createAccountStatementItemPolicyLineNo(BigInteger value) {
        return new JAXBElement<BigInteger>(_AccountStatementPolicyLineNo_QNAME, BigInteger.class, AccountStatementItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "centerCode", scope = Account.class)
    public JAXBElement<String> createAccountCenterCode(String value) {
        return new JAXBElement<String>(_AccountCenterCode_QNAME, String.class, Account.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "paymentMethodIn", scope = Account.class)
    public JAXBElement<String> createAccountPaymentMethodIn(String value) {
        return new JAXBElement<String>(_AccountPaymentMethodIn_QNAME, String.class, Account.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "locked", scope = Account.class)
    public JAXBElement<String> createAccountLocked(String value) {
        return new JAXBElement<String>(_AccountLocked_QNAME, String.class, Account.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "currencyCode", scope = Account.class)
    public JAXBElement<String> createAccountCurrencyCode(String value) {
        return new JAXBElement<String>(_SearchTokenPaymentCurrencyCode_QNAME, String.class, Account.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "collectionBy", scope = Account.class)
    public JAXBElement<Integer> createAccountCollectionBy(Integer value) {
        return new JAXBElement<Integer>(_AccountCollectionBy_QNAME, Integer.class, Account.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "paymentMethodOut", scope = Account.class)
    public JAXBElement<String> createAccountPaymentMethodOut(String value) {
        return new JAXBElement<String>(_AccountPaymentMethodOut_QNAME, String.class, Account.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "receiverId", scope = Account.class)
    public JAXBElement<Long> createAccountReceiverId(Long value) {
        return new JAXBElement<Long>(_AccountReceiverId_QNAME, Long.class, Account.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "meansPayNoOut", scope = Account.class)
    public JAXBElement<Long> createAccountMeansPayNoOut(Long value) {
        return new JAXBElement<Long>(_AccountMeansPayNoOut_QNAME, Long.class, Account.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "paymentTermsOut", scope = Account.class)
    public JAXBElement<String> createAccountPaymentTermsOut(String value) {
        return new JAXBElement<String>(_AccountPaymentTermsOut_QNAME, String.class, Account.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "reminderGroup", scope = Account.class)
    public JAXBElement<String> createAccountReminderGroup(String value) {
        return new JAXBElement<String>(_AccountReminderGroup_QNAME, String.class, Account.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "paymentTermsIn", scope = Account.class)
    public JAXBElement<String> createAccountPaymentTermsIn(String value) {
        return new JAXBElement<String>(_AccountPaymentTermsIn_QNAME, String.class, Account.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "title", scope = Account.class)
    public JAXBElement<String> createAccountTitle(String value) {
        return new JAXBElement<String>(_AccountTitle_QNAME, String.class, Account.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "handler", scope = Account.class)
    public JAXBElement<String> createAccountHandler(String value) {
        return new JAXBElement<String>(_AccountHandler_QNAME, String.class, Account.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "note", scope = Account.class)
    public JAXBElement<String> createAccountNote(String value) {
        return new JAXBElement<String>(_AccountNote_QNAME, String.class, Account.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "meansPayNoIn", scope = Account.class)
    public JAXBElement<Long> createAccountMeansPayNoIn(Long value) {
        return new JAXBElement<Long>(_AccountMeansPayNoIn_QNAME, Long.class, Account.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "externalCode", scope = CollectionRequestConfirm.class)
    public JAXBElement<String> createCollectionRequestConfirmExternalCode(String value) {
        return new JAXBElement<String>(_CollectionRequestConfirmExternalCode_QNAME, String.class, CollectionRequestConfirm.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/common/v2/", name = "correlationId", scope = InputToken.class)
    public JAXBElement<String> createInputTokenCorrelationId(String value) {
        return new JAXBElement<String>(_InputTokenCorrelationId_QNAME, String.class, InputToken.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "centerCode", scope = PaymentChannel.class)
    public JAXBElement<String> createPaymentChannelCenterCode(String value) {
        return new JAXBElement<String>(_AccountCenterCode_QNAME, String.class, PaymentChannel.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "title", scope = PaymentChannel.class)
    public JAXBElement<String> createPaymentChannelTitle(String value) {
        return new JAXBElement<String>(_AccountTitle_QNAME, String.class, PaymentChannel.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "bankIdNo", scope = PaymentChannel.class)
    public JAXBElement<Long> createPaymentChannelBankIdNo(Long value) {
        return new JAXBElement<Long>(_PaymentDetailsBankIdNo_QNAME, Long.class, PaymentChannel.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/common/v2/", name = "description", scope = TranslatedCode.class)
    public JAXBElement<String> createTranslatedCodeDescription(String value) {
        return new JAXBElement<String>(_TranslatedCodeDescription_QNAME, String.class, TranslatedCode.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/common/v2/", name = "diplayCode", scope = TranslatedCode.class)
    public JAXBElement<String> createTranslatedCodeDiplayCode(String value) {
        return new JAXBElement<String>(_TranslatedCodeDiplayCode_QNAME, String.class, TranslatedCode.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/common/v2/", name = "helpText", scope = TranslatedCode.class)
    public JAXBElement<String> createTranslatedCodeHelpText(String value) {
        return new JAXBElement<String>(_TranslatedCodeHelpText_QNAME, String.class, TranslatedCode.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/common/v2/", name = "varchar2Value", scope = Value.class)
    public JAXBElement<String> createValueVarchar2Value(String value) {
        return new JAXBElement<String>(_ValueVarchar2Value_QNAME, String.class, Value.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/common/v2/", name = "numberValue", scope = Value.class)
    public JAXBElement<BigDecimal> createValueNumberValue(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_ValueNumberValue_QNAME, BigDecimal.class, Value.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/common/v2/", name = "dateValue", scope = Value.class)
    public JAXBElement<XMLGregorianCalendar> createValueDateValue(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_ValueDateValue_QNAME, XMLGregorianCalendar.class, Value.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "externalRefIn", scope = CollectionAcknowledgement.class)
    public JAXBElement<String> createCollectionAcknowledgementExternalRefIn(String value) {
        return new JAXBElement<String>(_SearchTokenPaymentExternalRefIn_QNAME, String.class, CollectionAcknowledgement.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "externalCodeIn", scope = CollectionAcknowledgement.class)
    public JAXBElement<String> createCollectionAcknowledgementExternalCodeIn(String value) {
        return new JAXBElement<String>(_CollectionAcknowledgementExternalCodeIn_QNAME, String.class, CollectionAcknowledgement.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "reminderStepNo", scope = Reminder.class)
    public JAXBElement<Integer> createReminderReminderStepNo(Integer value) {
        return new JAXBElement<Integer>(_ReminderReminderStepNo_QNAME, Integer.class, Reminder.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "dueDate", scope = Reminder.class)
    public JAXBElement<XMLGregorianCalendar> createReminderDueDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_ReminderDueDate_QNAME, XMLGregorianCalendar.class, Reminder.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SiteSpecifics }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/common/v2/", name = "siteSpecifics", scope = TIAObject.class)
    public JAXBElement<SiteSpecifics> createTIAObjectSiteSpecifics(SiteSpecifics value) {
        return new JAXBElement<SiteSpecifics>(_TIAObjectSiteSpecifics_QNAME, SiteSpecifics.class, TIAObject.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/common/v2/", name = "externalId", scope = TIAObject.class)
    public JAXBElement<String> createTIAObjectExternalId(String value) {
        return new JAXBElement<String>(_TIAObjectExternalId_QNAME, String.class, TIAObject.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CountrySpecifics }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/common/v2/", name = "countrySpecifics", scope = TIAObject.class)
    public JAXBElement<CountrySpecifics> createTIAObjectCountrySpecifics(CountrySpecifics value) {
        return new JAXBElement<CountrySpecifics>(_TIAObjectCountrySpecifics_QNAME, CountrySpecifics.class, TIAObject.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FlexfieldCollection }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/common/v2/", name = "flexfieldCollection", scope = TIAObject.class)
    public JAXBElement<FlexfieldCollection> createTIAObjectFlexfieldCollection(FlexfieldCollection value) {
        return new JAXBElement<FlexfieldCollection>(_TIAObjectFlexfieldCollection_QNAME, FlexfieldCollection.class, TIAObject.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VersionToken }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/common/v2/", name = "versionToken", scope = TIAObject.class)
    public JAXBElement<VersionToken> createTIAObjectVersionToken(VersionToken value) {
        return new JAXBElement<VersionToken>(_TIAObjectVersionToken_QNAME, VersionToken.class, TIAObject.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "centerCode", scope = SearchTokenAccount.class)
    public JAXBElement<String> createSearchTokenAccountCenterCode(String value) {
        return new JAXBElement<String>(_AccountCenterCode_QNAME, String.class, SearchTokenAccount.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "nameId", scope = SearchTokenAccount.class)
    public JAXBElement<Long> createSearchTokenAccountNameId(Long value) {
        return new JAXBElement<Long>(_SearchTokenAccountNameId_QNAME, Long.class, SearchTokenAccount.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "policyNo", scope = SearchTokenAccount.class)
    public JAXBElement<BigInteger> createSearchTokenAccountPolicyNo(BigInteger value) {
        return new JAXBElement<BigInteger>(_AccountStatementPolicyNo_QNAME, BigInteger.class, SearchTokenAccount.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "mpPolicyNo", scope = SearchTokenAccount.class)
    public JAXBElement<Long> createSearchTokenAccountMpPolicyNo(Long value) {
        return new JAXBElement<Long>(_AccountStatementMpPolicyNo_QNAME, Long.class, SearchTokenAccount.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "claimNo", scope = SearchTokenAccount.class)
    public JAXBElement<Long> createSearchTokenAccountClaimNo(Long value) {
        return new JAXBElement<Long>(_AccountStatementClaimNo_QNAME, Long.class, SearchTokenAccount.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "accountNo", scope = SearchTokenAccount.class)
    public JAXBElement<Long> createSearchTokenAccountAccountNo(Long value) {
        return new JAXBElement<Long>(_SearchTokenPaymentAccountNo_QNAME, Long.class, SearchTokenAccount.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "accountType", scope = SearchTokenAccount.class)
    public JAXBElement<String> createSearchTokenAccountAccountType(String value) {
        return new JAXBElement<String>(_SearchTokenAccountAccountType_QNAME, String.class, SearchTokenAccount.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "policyLineNo", scope = SearchTokenAccount.class)
    public JAXBElement<BigInteger> createSearchTokenAccountPolicyLineNo(BigInteger value) {
        return new JAXBElement<BigInteger>(_AccountStatementPolicyLineNo_QNAME, BigInteger.class, SearchTokenAccount.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "centerCode", scope = PlannedInstalment.class)
    public JAXBElement<String> createPlannedInstalmentCenterCode(String value) {
        return new JAXBElement<String>(_AccountCenterCode_QNAME, String.class, PlannedInstalment.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "paymentMethod", scope = PlannedInstalment.class)
    public JAXBElement<String> createPlannedInstalmentPaymentMethod(String value) {
        return new JAXBElement<String>(_SearchTokenPaymentPaymentMethod_QNAME, String.class, PlannedInstalment.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "description", scope = PlannedInstalment.class)
    public JAXBElement<String> createPlannedInstalmentDescription(String value) {
        return new JAXBElement<String>(_PlannedInstalmentDescription_QNAME, String.class, PlannedInstalment.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "accItemNo", scope = PlannedInstalment.class)
    public JAXBElement<Long> createPlannedInstalmentAccItemNo(Long value) {
        return new JAXBElement<Long>(_PlannedInstalmentAccItemNo_QNAME, Long.class, PlannedInstalment.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "paymentDetailsId", scope = PlannedInstalment.class)
    public JAXBElement<Long> createPlannedInstalmentPaymentDetailsId(Long value) {
        return new JAXBElement<Long>(_PaymentDetailsPaymentDetailsId_QNAME, Long.class, PlannedInstalment.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "fromAccItemNo", scope = PlannedInstalment.class)
    public JAXBElement<Long> createPlannedInstalmentFromAccItemNo(Long value) {
        return new JAXBElement<Long>(_PlannedInstalmentFromAccItemNo_QNAME, Long.class, PlannedInstalment.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "amount", scope = PlannedInstalment.class)
    public JAXBElement<Double> createPlannedInstalmentAmount(Double value) {
        return new JAXBElement<Double>(_PaymentItemAmount_QNAME, Double.class, PlannedInstalment.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "collectionGroup", scope = PlannedInstalment.class)
    public JAXBElement<Long> createPlannedInstalmentCollectionGroup(Long value) {
        return new JAXBElement<Long>(_PlannedInstalmentCollectionGroup_QNAME, Long.class, PlannedInstalment.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "transId", scope = PlannedInstalment.class)
    public JAXBElement<Long> createPlannedInstalmentTransId(Long value) {
        return new JAXBElement<Long>(_PlannedInstalmentTransId_QNAME, Long.class, PlannedInstalment.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "dueDate", scope = PlannedInstalment.class)
    public JAXBElement<XMLGregorianCalendar> createPlannedInstalmentDueDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_ReminderDueDate_QNAME, XMLGregorianCalendar.class, PlannedInstalment.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "fromDate", scope = FilterTokenRemHistory.class)
    public JAXBElement<XMLGregorianCalendar> createFilterTokenRemHistoryFromDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_AccountStatementFromDate_QNAME, XMLGregorianCalendar.class, FilterTokenRemHistory.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "accountNo", scope = FilterTokenRemHistory.class)
    public JAXBElement<Long> createFilterTokenRemHistoryAccountNo(Long value) {
        return new JAXBElement<Long>(_SearchTokenPaymentAccountNo_QNAME, Long.class, FilterTokenRemHistory.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "toDate", scope = FilterTokenRemHistory.class)
    public JAXBElement<XMLGregorianCalendar> createFilterTokenRemHistoryToDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_AccountStatementToDate_QNAME, XMLGregorianCalendar.class, FilterTokenRemHistory.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "paymentMethod", scope = Payment.class)
    public JAXBElement<String> createPaymentPaymentMethod(String value) {
        return new JAXBElement<String>(_SearchTokenPaymentPaymentMethod_QNAME, String.class, Payment.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "amountIn", scope = Payment.class)
    public JAXBElement<BigDecimal> createPaymentAmountIn(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_PaymentAmountIn_QNAME, BigDecimal.class, Payment.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "paymentDetailsId", scope = Payment.class)
    public JAXBElement<Long> createPaymentPaymentDetailsId(Long value) {
        return new JAXBElement<Long>(_PaymentDetailsPaymentDetailsId_QNAME, Long.class, Payment.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "currencyCode", scope = Payment.class)
    public JAXBElement<String> createPaymentCurrencyCode(String value) {
        return new JAXBElement<String>(_SearchTokenPaymentCurrencyCode_QNAME, String.class, Payment.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "paymentStatus", scope = Payment.class)
    public JAXBElement<Integer> createPaymentPaymentStatus(Integer value) {
        return new JAXBElement<Integer>(_SearchTokenPaymentPaymentStatus_QNAME, Integer.class, Payment.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "paymentNo", scope = Payment.class)
    public JAXBElement<Long> createPaymentPaymentNo(Long value) {
        return new JAXBElement<Long>(_SearchTokenPaymentPaymentNo_QNAME, Long.class, Payment.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "transactionDateOut", scope = Payment.class)
    public JAXBElement<XMLGregorianCalendar> createPaymentTransactionDateOut(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_PaymentTransactionDateOut_QNAME, XMLGregorianCalendar.class, Payment.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "batchOut", scope = Payment.class)
    public JAXBElement<Long> createPaymentBatchOut(Long value) {
        return new JAXBElement<Long>(_PaymentBatchOut_QNAME, Long.class, Payment.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "receiverId", scope = Payment.class)
    public JAXBElement<Long> createPaymentReceiverId(Long value) {
        return new JAXBElement<Long>(_AccountReceiverId_QNAME, Long.class, Payment.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "paymentDetailsText", scope = Payment.class)
    public JAXBElement<String> createPaymentPaymentDetailsText(String value) {
        return new JAXBElement<String>(_PaymentPaymentDetailsText_QNAME, String.class, Payment.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "batchRun", scope = Payment.class)
    public JAXBElement<Long> createPaymentBatchRun(Long value) {
        return new JAXBElement<Long>(_PaymentBatchRun_QNAME, Long.class, Payment.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "paymentInstruction", scope = Payment.class)
    public JAXBElement<String> createPaymentPaymentInstruction(String value) {
        return new JAXBElement<String>(_PaymentPaymentInstruction_QNAME, String.class, Payment.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "valueDateIn", scope = Payment.class)
    public JAXBElement<XMLGregorianCalendar> createPaymentValueDateIn(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_PaymentValueDateIn_QNAME, XMLGregorianCalendar.class, Payment.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "paymentStatusOut", scope = Payment.class)
    public JAXBElement<Integer> createPaymentPaymentStatusOut(Integer value) {
        return new JAXBElement<Integer>(_PaymentPaymentStatusOut_QNAME, Integer.class, Payment.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "transactionDateIn", scope = Payment.class)
    public JAXBElement<XMLGregorianCalendar> createPaymentTransactionDateIn(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_PaymentTransactionDateIn_QNAME, XMLGregorianCalendar.class, Payment.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "externalRefIn", scope = Payment.class)
    public JAXBElement<String> createPaymentExternalRefIn(String value) {
        return new JAXBElement<String>(_SearchTokenPaymentExternalRefIn_QNAME, String.class, Payment.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "externalCodeIn", scope = Payment.class)
    public JAXBElement<String> createPaymentExternalCodeIn(String value) {
        return new JAXBElement<String>(_CollectionAcknowledgementExternalCodeIn_QNAME, String.class, Payment.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "paymentChannel", scope = Payment.class)
    public JAXBElement<String> createPaymentPaymentChannel(String value) {
        return new JAXBElement<String>(_PaymentDetailsPaymentChannel_QNAME, String.class, Payment.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "amount", scope = Payment.class)
    public JAXBElement<BigDecimal> createPaymentAmount(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_PaymentItemAmount_QNAME, BigDecimal.class, Payment.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "recollectionTransId", scope = Payment.class)
    public JAXBElement<Long> createPaymentRecollectionTransId(Long value) {
        return new JAXBElement<Long>(_PaymentRecollectionTransId_QNAME, Long.class, Payment.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "externalCode", scope = Payment.class)
    public JAXBElement<String> createPaymentExternalCode(String value) {
        return new JAXBElement<String>(_CollectionRequestConfirmExternalCode_QNAME, String.class, Payment.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "receiverNameAddress", scope = Payment.class)
    public JAXBElement<String> createPaymentReceiverNameAddress(String value) {
        return new JAXBElement<String>(_PaymentReceiverNameAddress_QNAME, String.class, Payment.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "batchIn", scope = Payment.class)
    public JAXBElement<Long> createPaymentBatchIn(Long value) {
        return new JAXBElement<Long>(_PaymentBatchIn_QNAME, Long.class, Payment.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "dueDate", scope = Payment.class)
    public JAXBElement<XMLGregorianCalendar> createPaymentDueDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_ReminderDueDate_QNAME, XMLGregorianCalendar.class, Payment.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "currencyAmount", scope = AccountItemMatch.class)
    public JAXBElement<BigDecimal> createAccountItemMatchCurrencyAmount(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_AccountItemMatchCurrencyAmount_QNAME, BigDecimal.class, AccountItemMatch.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "currencyCode", scope = AccountItemMatch.class)
    public JAXBElement<String> createAccountItemMatchCurrencyCode(String value) {
        return new JAXBElement<String>(_SearchTokenPaymentCurrencyCode_QNAME, String.class, AccountItemMatch.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "lossProfit", scope = AccountItemMatch.class)
    public JAXBElement<BigDecimal> createAccountItemMatchLossProfit(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_AccountItemMatchLossProfit_QNAME, BigDecimal.class, AccountItemMatch.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "otherCurrencyAmount", scope = AccountItemMatch.class)
    public JAXBElement<BigDecimal> createAccountItemMatchOtherCurrencyAmount(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_AccountItemMatchOtherCurrencyAmount_QNAME, BigDecimal.class, AccountItemMatch.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "otherAccountItemNo", scope = AccountItemMatch.class)
    public JAXBElement<Long> createAccountItemMatchOtherAccountItemNo(Long value) {
        return new JAXBElement<Long>(_AccountItemMatchOtherAccountItemNo_QNAME, Long.class, AccountItemMatch.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "amount", scope = AccountItemMatch.class)
    public JAXBElement<BigDecimal> createAccountItemMatchAmount(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_PaymentItemAmount_QNAME, BigDecimal.class, AccountItemMatch.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "otherAmount", scope = AccountItemMatch.class)
    public JAXBElement<BigDecimal> createAccountItemMatchOtherAmount(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_AccountItemMatchOtherAmount_QNAME, BigDecimal.class, AccountItemMatch.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "matchDate", scope = AccountItemMatch.class)
    public JAXBElement<XMLGregorianCalendar> createAccountItemMatchMatchDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_AccountItemMatchMatchDate_QNAME, XMLGregorianCalendar.class, AccountItemMatch.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "baseCurrencyCode", scope = AccountItemMatch.class)
    public JAXBElement<String> createAccountItemMatchBaseCurrencyCode(String value) {
        return new JAXBElement<String>(_AccountItemMatchBaseCurrencyCode_QNAME, String.class, AccountItemMatch.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "otherCurrencyCode", scope = AccountItemMatch.class)
    public JAXBElement<String> createAccountItemMatchOtherCurrencyCode(String value) {
        return new JAXBElement<String>(_AccountItemMatchOtherCurrencyCode_QNAME, String.class, AccountItemMatch.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "policyNo", scope = GetTokenInstalmentPlan.class)
    public JAXBElement<Long> createGetTokenInstalmentPlanPolicyNo(Long value) {
        return new JAXBElement<Long>(_AccountStatementPolicyNo_QNAME, Long.class, GetTokenInstalmentPlan.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "planStartDate", scope = GetTokenInstalmentPlan.class)
    public JAXBElement<XMLGregorianCalendar> createGetTokenInstalmentPlanPlanStartDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_InstalmentPlanPlanStartDate_QNAME, XMLGregorianCalendar.class, GetTokenInstalmentPlan.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "mpPolicyNo", scope = GetTokenInstalmentPlan.class)
    public JAXBElement<Long> createGetTokenInstalmentPlanMpPolicyNo(Long value) {
        return new JAXBElement<Long>(_AccountStatementMpPolicyNo_QNAME, Long.class, GetTokenInstalmentPlan.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "postedYN", scope = GetTokenInstalmentPlan.class)
    public JAXBElement<String> createGetTokenInstalmentPlanPostedYN(String value) {
        return new JAXBElement<String>(_GetTokenInstalmentPlanPostedYN_QNAME, String.class, GetTokenInstalmentPlan.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "policyLineNo", scope = GetTokenInstalmentPlan.class)
    public JAXBElement<Long> createGetTokenInstalmentPlanPolicyLineNo(Long value) {
        return new JAXBElement<Long>(_AccountStatementPolicyLineNo_QNAME, Long.class, GetTokenInstalmentPlan.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "policyNo", scope = FilterTokenAccStatement.class)
    public JAXBElement<BigInteger> createFilterTokenAccStatementPolicyNo(BigInteger value) {
        return new JAXBElement<BigInteger>(_AccountStatementPolicyNo_QNAME, BigInteger.class, FilterTokenAccStatement.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "mpPolicyNo", scope = FilterTokenAccStatement.class)
    public JAXBElement<Long> createFilterTokenAccStatementMpPolicyNo(Long value) {
        return new JAXBElement<Long>(_AccountStatementMpPolicyNo_QNAME, Long.class, FilterTokenAccStatement.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "fromDate", scope = FilterTokenAccStatement.class)
    public JAXBElement<XMLGregorianCalendar> createFilterTokenAccStatementFromDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_AccountStatementFromDate_QNAME, XMLGregorianCalendar.class, FilterTokenAccStatement.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "claimNo", scope = FilterTokenAccStatement.class)
    public JAXBElement<Long> createFilterTokenAccStatementClaimNo(Long value) {
        return new JAXBElement<Long>(_AccountStatementClaimNo_QNAME, Long.class, FilterTokenAccStatement.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "policyLineNo", scope = FilterTokenAccStatement.class)
    public JAXBElement<BigInteger> createFilterTokenAccStatementPolicyLineNo(BigInteger value) {
        return new JAXBElement<BigInteger>(_AccountStatementPolicyLineNo_QNAME, BigInteger.class, FilterTokenAccStatement.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "toDate", scope = FilterTokenAccStatement.class)
    public JAXBElement<XMLGregorianCalendar> createFilterTokenAccStatementToDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_AccountStatementToDate_QNAME, XMLGregorianCalendar.class, FilterTokenAccStatement.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "currencyCode", scope = ExchangeRate.class)
    public JAXBElement<String> createExchangeRateCurrencyCode(String value) {
        return new JAXBElement<String>(_SearchTokenPaymentCurrencyCode_QNAME, String.class, ExchangeRate.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "startDate", scope = ExchangeRate.class)
    public JAXBElement<XMLGregorianCalendar> createExchangeRateStartDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_PaymentDetailsStartDate_QNAME, XMLGregorianCalendar.class, ExchangeRate.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "rateType", scope = ExchangeRate.class)
    public JAXBElement<Long> createExchangeRateRateType(Long value) {
        return new JAXBElement<Long>(_ExchangeRateRateType_QNAME, Long.class, ExchangeRate.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "seqNo", scope = ExchangeRate.class)
    public JAXBElement<Long> createExchangeRateSeqNo(Long value) {
        return new JAXBElement<Long>(_PaymentSpecificationSeqNo_QNAME, Long.class, ExchangeRate.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "exchangeRate", scope = ExchangeRate.class)
    public JAXBElement<BigDecimal> createExchangeRateExchangeRate(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_ExchangeRateExchangeRate_QNAME, BigDecimal.class, ExchangeRate.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "endDate", scope = ExchangeRate.class)
    public JAXBElement<XMLGregorianCalendar> createExchangeRateEndDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_AccountStatementItemEndDate_QNAME, XMLGregorianCalendar.class, ExchangeRate.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "paymentInYN", scope = PaymentMethod.class)
    public JAXBElement<String> createPaymentMethodPaymentInYN(String value) {
        return new JAXBElement<String>(_PaymentMethodPaymentInYN_QNAME, String.class, PaymentMethod.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "paymentOutYN", scope = PaymentMethod.class)
    public JAXBElement<String> createPaymentMethodPaymentOutYN(String value) {
        return new JAXBElement<String>(_PaymentMethodPaymentOutYN_QNAME, String.class, PaymentMethod.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "approveCollectionsYN", scope = PaymentMethod.class)
    public JAXBElement<String> createPaymentMethodApproveCollectionsYN(String value) {
        return new JAXBElement<String>(_PaymentMethodApproveCollectionsYN_QNAME, String.class, PaymentMethod.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "approvePaymentsYN", scope = PaymentMethod.class)
    public JAXBElement<String> createPaymentMethodApprovePaymentsYN(String value) {
        return new JAXBElement<String>(_PaymentMethodApprovePaymentsYN_QNAME, String.class, PaymentMethod.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "itemText", scope = PaymentMethod.class)
    public JAXBElement<String> createPaymentMethodItemText(String value) {
        return new JAXBElement<String>(_AccountStatementItemItemText_QNAME, String.class, PaymentMethod.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "deferredAllocationYN", scope = PaymentMethod.class)
    public JAXBElement<String> createPaymentMethodDeferredAllocationYN(String value) {
        return new JAXBElement<String>(_PaymentMethodDeferredAllocationYN_QNAME, String.class, PaymentMethod.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "daysBeforeDuedate", scope = PaymentMethod.class)
    public JAXBElement<Integer> createPaymentMethodDaysBeforeDuedate(Integer value) {
        return new JAXBElement<Integer>(_PaymentMethodDaysBeforeDuedate_QNAME, Integer.class, PaymentMethod.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "centerCode", scope = FilterTokenAccItem.class)
    public JAXBElement<String> createFilterTokenAccItemCenterCode(String value) {
        return new JAXBElement<String>(_AccountCenterCode_QNAME, String.class, FilterTokenAccItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "openItemsOnlyYN", scope = FilterTokenAccItem.class)
    public JAXBElement<String> createFilterTokenAccItemOpenItemsOnlyYN(String value) {
        return new JAXBElement<String>(_FilterTokenAccItemOpenItemsOnlyYN_QNAME, String.class, FilterTokenAccItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "accountItemNo", scope = FilterTokenAccItem.class)
    public JAXBElement<Long> createFilterTokenAccItemAccountItemNo(Long value) {
        return new JAXBElement<Long>(_RemindedItemAccountItemNo_QNAME, Long.class, FilterTokenAccItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "dateType", scope = FilterTokenAccItem.class)
    public JAXBElement<String> createFilterTokenAccItemDateType(String value) {
        return new JAXBElement<String>(_FilterTokenAccItemDateType_QNAME, String.class, FilterTokenAccItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "specificationText", scope = ReminderSpecification.class)
    public JAXBElement<String> createReminderSpecificationSpecificationText(String value) {
        return new JAXBElement<String>(_PaymentSpecificationSpecificationText_QNAME, String.class, ReminderSpecification.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "centerCode", scope = AccountItem.class)
    public JAXBElement<String> createAccountItemCenterCode(String value) {
        return new JAXBElement<String>(_AccountCenterCode_QNAME, String.class, AccountItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "externalPaymentId", scope = AccountItem.class)
    public JAXBElement<String> createAccountItemExternalPaymentId(String value) {
        return new JAXBElement<String>(_PaymentDetailsExternalPaymentId_QNAME, String.class, AccountItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "paymentStatus", scope = AccountItem.class)
    public JAXBElement<Integer> createAccountItemPaymentStatus(Integer value) {
        return new JAXBElement<Integer>(_SearchTokenPaymentPaymentStatus_QNAME, Integer.class, AccountItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "complaintNo", scope = AccountItem.class)
    public JAXBElement<Long> createAccountItemComplaintNo(Long value) {
        return new JAXBElement<Long>(_AccountItemComplaintNo_QNAME, Long.class, AccountItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "createDate", scope = AccountItem.class)
    public JAXBElement<XMLGregorianCalendar> createAccountItemCreateDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_AccountItemCreateDate_QNAME, XMLGregorianCalendar.class, AccountItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "currencyBalance", scope = AccountItem.class)
    public JAXBElement<BigDecimal> createAccountItemCurrencyBalance(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_AccountItemCurrencyBalance_QNAME, BigDecimal.class, AccountItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "referralCode", scope = AccountItem.class)
    public JAXBElement<Integer> createAccountItemReferralCode(Integer value) {
        return new JAXBElement<Integer>(_AccountItemReferralCode_QNAME, Integer.class, AccountItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "currencyAmount", scope = AccountItem.class)
    public JAXBElement<BigDecimal> createAccountItemCurrencyAmount(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_AccountItemMatchCurrencyAmount_QNAME, BigDecimal.class, AccountItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "bankCostCurrencyAmt", scope = AccountItem.class)
    public JAXBElement<BigDecimal> createAccountItemBankCostCurrencyAmt(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_AccountItemBankCostCurrencyAmt_QNAME, BigDecimal.class, AccountItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "itemText", scope = AccountItem.class)
    public JAXBElement<String> createAccountItemItemText(String value) {
        return new JAXBElement<String>(_AccountStatementItemItemText_QNAME, String.class, AccountItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "claimNo", scope = AccountItem.class)
    public JAXBElement<Long> createAccountItemClaimNo(Long value) {
        return new JAXBElement<Long>(_AccountStatementClaimNo_QNAME, Long.class, AccountItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "yearOfOrigin", scope = AccountItem.class)
    public JAXBElement<Integer> createAccountItemYearOfOrigin(Integer value) {
        return new JAXBElement<Integer>(_AccountItemYearOfOrigin_QNAME, Integer.class, AccountItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "transferredFromAccItemNo", scope = AccountItem.class)
    public JAXBElement<Long> createAccountItemTransferredFromAccItemNo(Long value) {
        return new JAXBElement<Long>(_AccountItemTransferredFromAccItemNo_QNAME, Long.class, AccountItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "origTransId", scope = AccountItem.class)
    public JAXBElement<Long> createAccountItemOrigTransId(Long value) {
        return new JAXBElement<Long>(_AccountItemOrigTransId_QNAME, Long.class, AccountItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "reiContractSeqNo", scope = AccountItem.class)
    public JAXBElement<Long> createAccountItemReiContractSeqNo(Long value) {
        return new JAXBElement<Long>(_AccountItemReiContractSeqNo_QNAME, Long.class, AccountItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "currencyRate", scope = AccountItem.class)
    public JAXBElement<BigDecimal> createAccountItemCurrencyRate(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_AccountItemCurrencyRate_QNAME, BigDecimal.class, AccountItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "moneyClass", scope = AccountItem.class)
    public JAXBElement<Integer> createAccountItemMoneyClass(Integer value) {
        return new JAXBElement<Integer>(_AccountItemMoneyClass_QNAME, Integer.class, AccountItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "mpPolicyNo", scope = AccountItem.class)
    public JAXBElement<Long> createAccountItemMpPolicyNo(Long value) {
        return new JAXBElement<Long>(_AccountStatementMpPolicyNo_QNAME, Long.class, AccountItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "itemReference", scope = AccountItem.class)
    public JAXBElement<String> createAccountItemItemReference(String value) {
        return new JAXBElement<String>(_AccountStatementItemItemReference_QNAME, String.class, AccountItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "policyYear", scope = AccountItem.class)
    public JAXBElement<Integer> createAccountItemPolicyYear(Integer value) {
        return new JAXBElement<Integer>(_AccountItemPolicyYear_QNAME, Integer.class, AccountItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "reminderTransDate", scope = AccountItem.class)
    public JAXBElement<XMLGregorianCalendar> createAccountItemReminderTransDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_AccountItemReminderTransDate_QNAME, XMLGregorianCalendar.class, AccountItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "baseCurrencyCode", scope = AccountItem.class)
    public JAXBElement<String> createAccountItemBaseCurrencyCode(String value) {
        return new JAXBElement<String>(_AccountItemMatchBaseCurrencyCode_QNAME, String.class, AccountItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "internalNote", scope = AccountItem.class)
    public JAXBElement<String> createAccountItemInternalNote(String value) {
        return new JAXBElement<String>(_AccountItemInternalNote_QNAME, String.class, AccountItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "releaseBy", scope = AccountItem.class)
    public JAXBElement<String> createAccountItemReleaseBy(String value) {
        return new JAXBElement<String>(_AccountItemReleaseBy_QNAME, String.class, AccountItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "specification", scope = AccountItem.class)
    public JAXBElement<String> createAccountItemSpecification(String value) {
        return new JAXBElement<String>(_AccountStatementItemSpecification_QNAME, String.class, AccountItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "reiContractNo", scope = AccountItem.class)
    public JAXBElement<String> createAccountItemReiContractNo(String value) {
        return new JAXBElement<String>(_AccountItemReiContractNo_QNAME, String.class, AccountItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "valueDate", scope = AccountItem.class)
    public JAXBElement<XMLGregorianCalendar> createAccountItemValueDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_AccountItemValueDate_QNAME, XMLGregorianCalendar.class, AccountItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "transactionDate", scope = AccountItem.class)
    public JAXBElement<XMLGregorianCalendar> createAccountItemTransactionDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_AccountStatementItemTransactionDate_QNAME, XMLGregorianCalendar.class, AccountItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "accountItemNo", scope = AccountItem.class)
    public JAXBElement<Long> createAccountItemAccountItemNo(Long value) {
        return new JAXBElement<Long>(_RemindedItemAccountItemNo_QNAME, Long.class, AccountItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "amount", scope = AccountItem.class)
    public JAXBElement<BigDecimal> createAccountItemAmount(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_PaymentItemAmount_QNAME, BigDecimal.class, AccountItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "brokerId", scope = AccountItem.class)
    public JAXBElement<Long> createAccountItemBrokerId(Long value) {
        return new JAXBElement<Long>(_AccountItemBrokerId_QNAME, Long.class, AccountItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "actualCurrencyCode", scope = AccountItem.class)
    public JAXBElement<String> createAccountItemActualCurrencyCode(String value) {
        return new JAXBElement<String>(_AccountItemActualCurrencyCode_QNAME, String.class, AccountItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "paymentDetailsId", scope = AccountItem.class)
    public JAXBElement<Long> createAccountItemPaymentDetailsId(Long value) {
        return new JAXBElement<Long>(_PaymentDetailsPaymentDetailsId_QNAME, Long.class, AccountItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "vatCode", scope = AccountItem.class)
    public JAXBElement<String> createAccountItemVatCode(String value) {
        return new JAXBElement<String>(_AccountItemVatCode_QNAME, String.class, AccountItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "payee", scope = AccountItem.class)
    public JAXBElement<String> createAccountItemPayee(String value) {
        return new JAXBElement<String>(_AccountItemPayee_QNAME, String.class, AccountItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "balance", scope = AccountItem.class)
    public JAXBElement<BigDecimal> createAccountItemBalance(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_AccountItemBalance_QNAME, BigDecimal.class, AccountItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "approvedBy", scope = AccountItem.class)
    public JAXBElement<String> createAccountItemApprovedBy(String value) {
        return new JAXBElement<String>(_PaymentItemApprovedBy_QNAME, String.class, AccountItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "policyNo", scope = AccountItem.class)
    public JAXBElement<BigInteger> createAccountItemPolicyNo(BigInteger value) {
        return new JAXBElement<BigInteger>(_AccountStatementPolicyNo_QNAME, BigInteger.class, AccountItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "actualCurrencyAmt", scope = AccountItem.class)
    public JAXBElement<BigDecimal> createAccountItemActualCurrencyAmt(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_AccountItemActualCurrencyAmt_QNAME, BigDecimal.class, AccountItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "reversedToAccItemNo", scope = AccountItem.class)
    public JAXBElement<Long> createAccountItemReversedToAccItemNo(Long value) {
        return new JAXBElement<Long>(_AccountItemReversedToAccItemNo_QNAME, Long.class, AccountItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "createdByUserid", scope = AccountItem.class)
    public JAXBElement<String> createAccountItemCreatedByUserid(String value) {
        return new JAXBElement<String>(_AccountItemCreatedByUserid_QNAME, String.class, AccountItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "endDate", scope = AccountItem.class)
    public JAXBElement<XMLGregorianCalendar> createAccountItemEndDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_AccountStatementItemEndDate_QNAME, XMLGregorianCalendar.class, AccountItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "dueDate", scope = AccountItem.class)
    public JAXBElement<XMLGregorianCalendar> createAccountItemDueDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_ReminderDueDate_QNAME, XMLGregorianCalendar.class, AccountItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "paymentMethod", scope = AccountItem.class)
    public JAXBElement<String> createAccountItemPaymentMethod(String value) {
        return new JAXBElement<String>(_SearchTokenPaymentPaymentMethod_QNAME, String.class, AccountItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "nameId", scope = AccountItem.class)
    public JAXBElement<Long> createAccountItemNameId(Long value) {
        return new JAXBElement<Long>(_SearchTokenAccountNameId_QNAME, Long.class, AccountItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "startDate", scope = AccountItem.class)
    public JAXBElement<XMLGregorianCalendar> createAccountItemStartDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_PaymentDetailsStartDate_QNAME, XMLGregorianCalendar.class, AccountItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "subClaimNo", scope = AccountItem.class)
    public JAXBElement<Long> createAccountItemSubClaimNo(Long value) {
        return new JAXBElement<Long>(_AccountItemSubClaimNo_QNAME, Long.class, AccountItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "vatCurrencyAmt", scope = AccountItem.class)
    public JAXBElement<BigDecimal> createAccountItemVatCurrencyAmt(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_AccountItemVatCurrencyAmt_QNAME, BigDecimal.class, AccountItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "paymentComment", scope = AccountItem.class)
    public JAXBElement<String> createAccountItemPaymentComment(String value) {
        return new JAXBElement<String>(_AccountItemPaymentComment_QNAME, String.class, AccountItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "holdUntilPaid", scope = AccountItem.class)
    public JAXBElement<String> createAccountItemHoldUntilPaid(String value) {
        return new JAXBElement<String>(_AccountItemHoldUntilPaid_QNAME, String.class, AccountItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "paymentChannel", scope = AccountItem.class)
    public JAXBElement<String> createAccountItemPaymentChannel(String value) {
        return new JAXBElement<String>(_PaymentDetailsPaymentChannel_QNAME, String.class, AccountItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "onHold", scope = AccountItem.class)
    public JAXBElement<String> createAccountItemOnHold(String value) {
        return new JAXBElement<String>(_AccountItemOnHold_QNAME, String.class, AccountItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "policyLineNo", scope = AccountItem.class)
    public JAXBElement<BigInteger> createAccountItemPolicyLineNo(BigInteger value) {
        return new JAXBElement<BigInteger>(_AccountStatementPolicyLineNo_QNAME, BigInteger.class, AccountItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "postponementNo", scope = ReminderPostponement.class)
    public JAXBElement<Long> createReminderPostponementPostponementNo(Long value) {
        return new JAXBElement<Long>(_ReminderPostponementPostponementNo_QNAME, Long.class, ReminderPostponement.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "policyNo", scope = ReminderPostponement.class)
    public JAXBElement<BigInteger> createReminderPostponementPolicyNo(BigInteger value) {
        return new JAXBElement<BigInteger>(_AccountStatementPolicyNo_QNAME, BigInteger.class, ReminderPostponement.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "mpPolicyNo", scope = ReminderPostponement.class)
    public JAXBElement<Long> createReminderPostponementMpPolicyNo(Long value) {
        return new JAXBElement<Long>(_AccountStatementMpPolicyNo_QNAME, Long.class, ReminderPostponement.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "createDate", scope = ReminderPostponement.class)
    public JAXBElement<XMLGregorianCalendar> createReminderPostponementCreateDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_AccountItemCreateDate_QNAME, XMLGregorianCalendar.class, ReminderPostponement.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "claimNo", scope = ReminderPostponement.class)
    public JAXBElement<Long> createReminderPostponementClaimNo(Long value) {
        return new JAXBElement<Long>(_AccountStatementClaimNo_QNAME, Long.class, ReminderPostponement.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "accountItemNo", scope = ReminderPostponement.class)
    public JAXBElement<Long> createReminderPostponementAccountItemNo(Long value) {
        return new JAXBElement<Long>(_RemindedItemAccountItemNo_QNAME, Long.class, ReminderPostponement.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "note", scope = ReminderPostponement.class)
    public JAXBElement<String> createReminderPostponementNote(String value) {
        return new JAXBElement<String>(_AccountNote_QNAME, String.class, ReminderPostponement.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "creator", scope = ReminderPostponement.class)
    public JAXBElement<String> createReminderPostponementCreator(String value) {
        return new JAXBElement<String>(_ReminderPostponementCreator_QNAME, String.class, ReminderPostponement.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "receiverId", scope = ReminderPostponement.class)
    public JAXBElement<Long> createReminderPostponementReceiverId(Long value) {
        return new JAXBElement<Long>(_AccountReceiverId_QNAME, Long.class, ReminderPostponement.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "policyLineNo", scope = ReminderPostponement.class)
    public JAXBElement<BigInteger> createReminderPostponementPolicyLineNo(BigInteger value) {
        return new JAXBElement<BigInteger>(_AccountStatementPolicyLineNo_QNAME, BigInteger.class, ReminderPostponement.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/account/v2/", name = "accountNo", scope = ReminderPostponement.class)
    public JAXBElement<Long> createReminderPostponementAccountNo(Long value) {
        return new JAXBElement<Long>(_SearchTokenPaymentAccountNo_QNAME, Long.class, ReminderPostponement.class, value);
    }

}
