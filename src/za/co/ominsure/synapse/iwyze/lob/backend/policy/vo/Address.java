
package za.co.ominsure.synapse.iwyze.lob.backend.policy.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Is mapped into TIA metadata type "obj_address".
 * 
 * <p>Java class for Address complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Address">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="addressType" type="{http://infrastructure.tia.dk/schema/party/v2/}addressType" minOccurs="0"/>
 *         &lt;element name="houseCoName" type="{http://infrastructure.tia.dk/schema/party/v2/}longName" minOccurs="0"/>
 *         &lt;element name="street" type="{http://infrastructure.tia.dk/schema/party/v2/}street" minOccurs="0"/>
 *         &lt;element name="streetNo" type="{http://infrastructure.tia.dk/schema/party/v2/}streetNo" minOccurs="0"/>
 *         &lt;element name="floor" type="{http://infrastructure.tia.dk/schema/party/v2/}floor" minOccurs="0"/>
 *         &lt;element name="floorExt" type="{http://infrastructure.tia.dk/schema/party/v2/}floorExt" minOccurs="0"/>
 *         &lt;element name="postArea" type="{http://infrastructure.tia.dk/schema/party/v2/}postArea" minOccurs="0"/>
 *         &lt;element name="postStreet" type="{http://infrastructure.tia.dk/schema/party/v2/}postStreet" minOccurs="0"/>
 *         &lt;element name="postalRegion" type="{http://infrastructure.tia.dk/schema/party/v2/}postalRegion" minOccurs="0"/>
 *         &lt;element name="city" type="{http://infrastructure.tia.dk/schema/party/v2/}city" minOccurs="0"/>
 *         &lt;element name="country" type="{http://infrastructure.tia.dk/schema/party/v2/}country" minOccurs="0"/>
 *         &lt;element name="countryCode" type="{http://infrastructure.tia.dk/schema/party/v2/}countryCode" minOccurs="0"/>
 *         &lt;element name="county" type="{http://infrastructure.tia.dk/schema/party/v2/}county" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Address", namespace = "http://infrastructure.tia.dk/schema/party/v2/", propOrder = {
    "addressType",
    "houseCoName",
    "street",
    "streetNo",
    "floor",
    "floorExt",
    "postArea",
    "postStreet",
    "postalRegion",
    "city",
    "country",
    "countryCode",
    "county"
})
public class Address {

    @XmlElement(nillable = true)
    protected String addressType;
    @XmlElement(nillable = true)
    protected String houseCoName;
    @XmlElement(nillable = true)
    protected String street;
    @XmlElement(nillable = true)
    protected String streetNo;
    @XmlElement(nillable = true)
    protected String floor;
    @XmlElement(nillable = true)
    protected String floorExt;
    @XmlElement(nillable = true)
    protected String postArea;
    @XmlElement(nillable = true)
    protected String postStreet;
    @XmlElement(nillable = true)
    protected String postalRegion;
    @XmlElement(nillable = true)
    protected String city;
    @XmlElement(nillable = true)
    protected String country;
    @XmlElement(nillable = true)
    protected String countryCode;
    @XmlElement(nillable = true)
    protected String county;

    /**
     * Gets the value of the addressType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddressType() {
        return addressType;
    }

    /**
     * Sets the value of the addressType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddressType(String value) {
        this.addressType = value;
    }

    /**
     * Gets the value of the houseCoName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHouseCoName() {
        return houseCoName;
    }

    /**
     * Sets the value of the houseCoName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHouseCoName(String value) {
        this.houseCoName = value;
    }

    /**
     * Gets the value of the street property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStreet() {
        return street;
    }

    /**
     * Sets the value of the street property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStreet(String value) {
        this.street = value;
    }

    /**
     * Gets the value of the streetNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStreetNo() {
        return streetNo;
    }

    /**
     * Sets the value of the streetNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStreetNo(String value) {
        this.streetNo = value;
    }

    /**
     * Gets the value of the floor property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFloor() {
        return floor;
    }

    /**
     * Sets the value of the floor property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFloor(String value) {
        this.floor = value;
    }

    /**
     * Gets the value of the floorExt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFloorExt() {
        return floorExt;
    }

    /**
     * Sets the value of the floorExt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFloorExt(String value) {
        this.floorExt = value;
    }

    /**
     * Gets the value of the postArea property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPostArea() {
        return postArea;
    }

    /**
     * Sets the value of the postArea property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPostArea(String value) {
        this.postArea = value;
    }

    /**
     * Gets the value of the postStreet property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPostStreet() {
        return postStreet;
    }

    /**
     * Sets the value of the postStreet property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPostStreet(String value) {
        this.postStreet = value;
    }

    /**
     * Gets the value of the postalRegion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPostalRegion() {
        return postalRegion;
    }

    /**
     * Sets the value of the postalRegion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPostalRegion(String value) {
        this.postalRegion = value;
    }

    /**
     * Gets the value of the city property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCity() {
        return city;
    }

    /**
     * Sets the value of the city property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCity(String value) {
        this.city = value;
    }

    /**
     * Gets the value of the country property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCountry() {
        return country;
    }

    /**
     * Sets the value of the country property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCountry(String value) {
        this.country = value;
    }

    /**
     * Gets the value of the countryCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCountryCode() {
        return countryCode;
    }

    /**
     * Sets the value of the countryCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCountryCode(String value) {
        this.countryCode = value;
    }

    /**
     * Gets the value of the county property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCounty() {
        return county;
    }

    /**
     * Sets the value of the county property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCounty(String value) {
        this.county = value;
    }

}
