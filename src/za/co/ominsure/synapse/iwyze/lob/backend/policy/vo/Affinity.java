
package za.co.ominsure.synapse.iwyze.lob.backend.policy.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Additional comments
 * 
 * <p>Java class for Affinity complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Affinity">
 *   &lt;complexContent>
 *     &lt;extension base="{http://infrastructure.tia.dk/schema/party/v2/}PartyRole">
 *       &lt;sequence>
 *         &lt;element name="affinityNo" type="{http://infrastructure.tia.dk/schema/common/v2/}long10" minOccurs="0"/>
 *         &lt;element name="descriptionNote" type="{http://infrastructure.tia.dk/schema/common/v2/}string2000" minOccurs="0"/>
 *         &lt;element name="documentPile" type="{http://infrastructure.tia.dk/schema/common/v2/}string30" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Affinity", namespace = "http://infrastructure.tia.dk/schema/party/v2/", propOrder = {
    "affinityNo",
    "descriptionNote",
    "documentPile"
})
public class Affinity
    extends PartyRole
{

    @XmlElement(nillable = true)
    protected Long affinityNo;
    @XmlElement(nillable = true)
    protected String descriptionNote;
    @XmlElement(nillable = true)
    protected String documentPile;

    /**
     * Gets the value of the affinityNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getAffinityNo() {
        return affinityNo;
    }

    /**
     * Sets the value of the affinityNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setAffinityNo(Long value) {
        this.affinityNo = value;
    }

    /**
     * Gets the value of the descriptionNote property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescriptionNote() {
        return descriptionNote;
    }

    /**
     * Sets the value of the descriptionNote property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescriptionNote(String value) {
        this.descriptionNote = value;
    }

    /**
     * Gets the value of the documentPile property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDocumentPile() {
        return documentPile;
    }

    /**
     * Sets the value of the documentPile property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDocumentPile(String value) {
        this.documentPile = value;
    }

}
