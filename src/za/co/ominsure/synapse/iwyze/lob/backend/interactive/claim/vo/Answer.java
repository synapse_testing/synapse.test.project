
package za.co.ominsure.synapse.iwyze.lob.backend.interactive.claim.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Additional comments
 * 
 * <p>Java class for Answer complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Answer">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="questionCode" type="{http://infrastructure.tia.dk/schema/common/v2/}string10" minOccurs="0"/>
 *         &lt;element name="answerValue" type="{http://infrastructure.tia.dk/schema/common/v2/}string2000" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Answer", propOrder = {
    "questionCode",
    "answerValue"
})
public class Answer {

    @XmlElement(nillable = true)
    protected String questionCode;
    @XmlElement(nillable = true)
    protected String answerValue;

    /**
     * Gets the value of the questionCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQuestionCode() {
        return questionCode;
    }

    /**
     * Sets the value of the questionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQuestionCode(String value) {
        this.questionCode = value;
    }

    /**
     * Gets the value of the answerValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAnswerValue() {
        return answerValue;
    }

    /**
     * Sets the value of the answerValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAnswerValue(String value) {
        this.answerValue = value;
    }

}
