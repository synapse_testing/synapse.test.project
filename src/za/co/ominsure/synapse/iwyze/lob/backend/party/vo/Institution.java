
package za.co.ominsure.synapse.iwyze.lob.backend.party.vo;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * Is mapped into TIA metadata type "obj_institution".
 * 
 * <p>Java class for Institution complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Institution">
 *   &lt;complexContent>
 *     &lt;extension base="{http://infrastructure.tia.dk/schema/party/v3/}AbstractParty">
 *       &lt;sequence>
 *         &lt;element name="companyRegNo" type="{http://infrastructure.tia.dk/schema/party/v2/}companyRegNo" minOccurs="0"/>
 *         &lt;element name="companyVatNo" type="{http://infrastructure.tia.dk/schema/party/v2/}companyVatNo" minOccurs="0"/>
 *         &lt;element name="contactPerson" type="{http://infrastructure.tia.dk/schema/party/v2/}contactPerson" minOccurs="0"/>
 *         &lt;element name="institutionCode" type="{http://infrastructure.tia.dk/schema/common/v2/}string10" minOccurs="0"/>
 *         &lt;element name="employees" type="{http://infrastructure.tia.dk/schema/common/v2/}long12" minOccurs="0"/>
 *         &lt;element name="industryCode" type="{http://infrastructure.tia.dk/schema/common/v2/}string10" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Institution", namespace = "http://infrastructure.tia.dk/schema/party/v3/", propOrder = {
    "companyRegNo",
    "companyVatNo",
    "contactPerson",
    "institutionCode",
    "employees",
    "industryCode"
})
public class Institution
    extends AbstractParty
{

    @XmlElementRef(name = "companyRegNo", namespace = "http://infrastructure.tia.dk/schema/party/v3/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> companyRegNo;
    @XmlElementRef(name = "companyVatNo", namespace = "http://infrastructure.tia.dk/schema/party/v3/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> companyVatNo;
    @XmlElementRef(name = "contactPerson", namespace = "http://infrastructure.tia.dk/schema/party/v3/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> contactPerson;
    @XmlElementRef(name = "institutionCode", namespace = "http://infrastructure.tia.dk/schema/party/v3/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> institutionCode;
    @XmlElementRef(name = "employees", namespace = "http://infrastructure.tia.dk/schema/party/v3/", type = JAXBElement.class, required = false)
    protected JAXBElement<Long> employees;
    @XmlElementRef(name = "industryCode", namespace = "http://infrastructure.tia.dk/schema/party/v3/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> industryCode;

    /**
     * Gets the value of the companyRegNo property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCompanyRegNo() {
        return companyRegNo;
    }

    /**
     * Sets the value of the companyRegNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCompanyRegNo(JAXBElement<String> value) {
        this.companyRegNo = value;
    }

    /**
     * Gets the value of the companyVatNo property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCompanyVatNo() {
        return companyVatNo;
    }

    /**
     * Sets the value of the companyVatNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCompanyVatNo(JAXBElement<String> value) {
        this.companyVatNo = value;
    }

    /**
     * Gets the value of the contactPerson property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getContactPerson() {
        return contactPerson;
    }

    /**
     * Sets the value of the contactPerson property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setContactPerson(JAXBElement<String> value) {
        this.contactPerson = value;
    }

    /**
     * Gets the value of the institutionCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getInstitutionCode() {
        return institutionCode;
    }

    /**
     * Sets the value of the institutionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setInstitutionCode(JAXBElement<String> value) {
        this.institutionCode = value;
    }

    /**
     * Gets the value of the employees property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Long }{@code >}
     *     
     */
    public JAXBElement<Long> getEmployees() {
        return employees;
    }

    /**
     * Sets the value of the employees property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Long }{@code >}
     *     
     */
    public void setEmployees(JAXBElement<Long> value) {
        this.employees = value;
    }

    /**
     * Gets the value of the industryCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getIndustryCode() {
        return industryCode;
    }

    /**
     * Sets the value of the industryCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setIndustryCode(JAXBElement<String> value) {
        this.industryCode = value;
    }

}
