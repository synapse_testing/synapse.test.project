
package za.co.ominsure.synapse.iwyze.lob.backend.policy.vo;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="inputToken" type="{http://infrastructure.tia.dk/schema/common/v2/}InputToken"/>
 *         &lt;element name="policyNoAlt" type="{http://infrastructure.tia.dk/schema/common/v2/}string30" minOccurs="0"/>
 *         &lt;element name="policySeqNo" type="{http://infrastructure.tia.dk/schema/policy/v2/}seqNo" minOccurs="0"/>
 *         &lt;element name="getDepth" type="{http://infrastructure.tia.dk/schema/policy/v2/}getDepth" minOccurs="0"/>
 *         &lt;element name="policyLineNoCollection" type="{http://infrastructure.tia.dk/schema/policy/v2/}PolicyLineNoCollection" minOccurs="0"/>
 *         &lt;element name="pageSort" type="{http://infrastructure.tia.dk/schema/common/v2/}PageSort" minOccurs="0"/>
 *         &lt;element name="coverDate" type="{http://infrastructure.tia.dk/schema/common/v2/}date" minOccurs="0"/>
 *         &lt;element name="filterOption" type="{http://infrastructure.tia.dk/schema/policy/v2/}FilterOptionCollection" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "inputToken",
    "policyNoAlt",
    "policySeqNo",
    "getDepth",
    "policyLineNoCollection",
    "pageSort",
    "coverDate",
    "filterOption"
})
@XmlRootElement(name = "getPolicyByAltIdRequest", namespace = "http://infrastructure.tia.dk/schema/policy/v4/")
public class GetPolicyByAltIdRequest {

    @XmlElement(namespace = "http://infrastructure.tia.dk/schema/policy/v4/", required = true)
    protected InputToken inputToken;
    @XmlElement(namespace = "http://infrastructure.tia.dk/schema/policy/v4/")
    protected String policyNoAlt;
    @XmlElement(namespace = "http://infrastructure.tia.dk/schema/policy/v4/")
    @XmlSchemaType(name = "unsignedLong")
    protected BigInteger policySeqNo;
    @XmlElement(namespace = "http://infrastructure.tia.dk/schema/policy/v4/")
    @XmlSchemaType(name = "string")
    protected GetDepth getDepth;
    @XmlElement(namespace = "http://infrastructure.tia.dk/schema/policy/v4/")
    protected PolicyLineNoCollection policyLineNoCollection;
    @XmlElement(namespace = "http://infrastructure.tia.dk/schema/policy/v4/")
    protected PageSort pageSort;
    @XmlElement(namespace = "http://infrastructure.tia.dk/schema/policy/v4/")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar coverDate;
    @XmlElement(namespace = "http://infrastructure.tia.dk/schema/policy/v4/")
    protected FilterOptionCollection filterOption;

    /**
     * Gets the value of the inputToken property.
     * 
     * @return
     *     possible object is
     *     {@link InputToken }
     *     
     */
    public InputToken getInputToken() {
        return inputToken;
    }

    /**
     * Sets the value of the inputToken property.
     * 
     * @param value
     *     allowed object is
     *     {@link InputToken }
     *     
     */
    public void setInputToken(InputToken value) {
        this.inputToken = value;
    }

    /**
     * Gets the value of the policyNoAlt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPolicyNoAlt() {
        return policyNoAlt;
    }

    /**
     * Sets the value of the policyNoAlt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPolicyNoAlt(String value) {
        this.policyNoAlt = value;
    }

    /**
     * Gets the value of the policySeqNo property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getPolicySeqNo() {
        return policySeqNo;
    }

    /**
     * Sets the value of the policySeqNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setPolicySeqNo(BigInteger value) {
        this.policySeqNo = value;
    }

    /**
     * Gets the value of the getDepth property.
     * 
     * @return
     *     possible object is
     *     {@link GetDepth }
     *     
     */
    public GetDepth getGetDepth() {
        return getDepth;
    }

    /**
     * Sets the value of the getDepth property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetDepth }
     *     
     */
    public void setGetDepth(GetDepth value) {
        this.getDepth = value;
    }

    /**
     * Gets the value of the policyLineNoCollection property.
     * 
     * @return
     *     possible object is
     *     {@link PolicyLineNoCollection }
     *     
     */
    public PolicyLineNoCollection getPolicyLineNoCollection() {
        return policyLineNoCollection;
    }

    /**
     * Sets the value of the policyLineNoCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link PolicyLineNoCollection }
     *     
     */
    public void setPolicyLineNoCollection(PolicyLineNoCollection value) {
        this.policyLineNoCollection = value;
    }

    /**
     * Gets the value of the pageSort property.
     * 
     * @return
     *     possible object is
     *     {@link PageSort }
     *     
     */
    public PageSort getPageSort() {
        return pageSort;
    }

    /**
     * Sets the value of the pageSort property.
     * 
     * @param value
     *     allowed object is
     *     {@link PageSort }
     *     
     */
    public void setPageSort(PageSort value) {
        this.pageSort = value;
    }

    /**
     * Gets the value of the coverDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCoverDate() {
        return coverDate;
    }

    /**
     * Sets the value of the coverDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCoverDate(XMLGregorianCalendar value) {
        this.coverDate = value;
    }

    /**
     * Gets the value of the filterOption property.
     * 
     * @return
     *     possible object is
     *     {@link FilterOptionCollection }
     *     
     */
    public FilterOptionCollection getFilterOption() {
        return filterOption;
    }

    /**
     * Sets the value of the filterOption property.
     * 
     * @param value
     *     allowed object is
     *     {@link FilterOptionCollection }
     *     
     */
    public void setFilterOption(FilterOptionCollection value) {
        this.filterOption = value;
    }

}
