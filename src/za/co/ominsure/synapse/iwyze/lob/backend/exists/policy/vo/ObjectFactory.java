
package za.co.ominsure.synapse.iwyze.lob.backend.exists.policy.vo;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the za.co.ominsure.synapse.iwyze.lob.backend.exists.policy.vo package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: za.co.ominsure.synapse.iwyze.lob.backend.exists.policy.vo
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OutputParameters }
     * 
     */
    public OutputParameters createOutputParameters() {
        return new OutputParameters();
    }

    /**
     * Create an instance of {@link TIATABPOLICYDETAILS }
     * 
     */
    public TIATABPOLICYDETAILS createTIATABPOLICYDETAILS() {
        return new TIATABPOLICYDETAILS();
    }

    /**
     * Create an instance of {@link TIAOBJRESULT }
     * 
     */
    public TIAOBJRESULT createTIAOBJRESULT() {
        return new TIAOBJRESULT();
    }

    /**
     * Create an instance of {@link InputParameters }
     * 
     */
    public InputParameters createInputParameters() {
        return new InputParameters();
    }

    /**
     * Create an instance of {@link TIAOBJINPUTTOKEN }
     * 
     */
    public TIAOBJINPUTTOKEN createTIAOBJINPUTTOKEN() {
        return new TIAOBJINPUTTOKEN();
    }

    /**
     * Create an instance of {@link TIAOBJMESSAGE }
     * 
     */
    public TIAOBJMESSAGE createTIAOBJMESSAGE() {
        return new TIAOBJMESSAGE();
    }

    /**
     * Create an instance of {@link TIAOBJATTRIBUTE }
     * 
     */
    public TIAOBJATTRIBUTE createTIAOBJATTRIBUTE() {
        return new TIAOBJATTRIBUTE();
    }

    /**
     * Create an instance of {@link TIATABMESSAGE }
     * 
     */
    public TIATABMESSAGE createTIATABMESSAGE() {
        return new TIATABMESSAGE();
    }

    /**
     * Create an instance of {@link TIATABVARCHAR2 }
     * 
     */
    public TIATABVARCHAR2 createTIATABVARCHAR2() {
        return new TIATABVARCHAR2();
    }

    /**
     * Create an instance of {@link TIAOBJVARCHAR2 }
     * 
     */
    public TIAOBJVARCHAR2 createTIAOBJVARCHAR2() {
        return new TIAOBJVARCHAR2();
    }

    /**
     * Create an instance of {@link TIATABATTRIBUTE }
     * 
     */
    public TIATABATTRIBUTE createTIATABATTRIBUTE() {
        return new TIATABATTRIBUTE();
    }

    /**
     * Create an instance of {@link TIAOBJVERSIONTOKEN }
     * 
     */
    public TIAOBJVERSIONTOKEN createTIAOBJVERSIONTOKEN() {
        return new TIAOBJVERSIONTOKEN();
    }

    /**
     * Create an instance of {@link TIAOBJCOUNTRYSPECIFICS }
     * 
     */
    public TIAOBJCOUNTRYSPECIFICS createTIAOBJCOUNTRYSPECIFICS() {
        return new TIAOBJCOUNTRYSPECIFICS();
    }

    /**
     * Create an instance of {@link TIAOBJSITESPECIFICS }
     * 
     */
    public TIAOBJSITESPECIFICS createTIAOBJSITESPECIFICS() {
        return new TIAOBJSITESPECIFICS();
    }

    /**
     * Create an instance of {@link TIAOBJPOLICYDETAILS }
     * 
     */
    public TIAOBJPOLICYDETAILS createTIAOBJPOLICYDETAILS() {
        return new TIAOBJPOLICYDETAILS();
    }

}
