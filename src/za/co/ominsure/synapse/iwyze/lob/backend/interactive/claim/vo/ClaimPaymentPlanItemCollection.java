
package za.co.ominsure.synapse.iwyze.lob.backend.interactive.claim.vo;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ClaimPaymentPlanItemCollection complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ClaimPaymentPlanItemCollection">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="claimPaymentPlanItem" type="{http://infrastructure.tia.dk/schema/claim/v2/}ClaimPaymentPlanItem" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ClaimPaymentPlanItemCollection", propOrder = {
    "claimPaymentPlanItems"
})
public class ClaimPaymentPlanItemCollection {

    @XmlElement(name = "claimPaymentPlanItem")
    protected List<ClaimPaymentPlanItem> claimPaymentPlanItems;

    /**
     * Gets the value of the claimPaymentPlanItems property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the claimPaymentPlanItems property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getClaimPaymentPlanItems().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ClaimPaymentPlanItem }
     * 
     * 
     */
    public List<ClaimPaymentPlanItem> getClaimPaymentPlanItems() {
        if (claimPaymentPlanItems == null) {
            claimPaymentPlanItems = new ArrayList<ClaimPaymentPlanItem>();
        }
        return this.claimPaymentPlanItems;
    }

}
