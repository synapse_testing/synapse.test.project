
package za.co.ominsure.synapse.iwyze.lob.backend.claim.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Additional comments
 * 
 * <p>Java class for ClaimNrcEvent complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ClaimNrcEvent">
 *   &lt;complexContent>
 *     &lt;extension base="{http://infrastructure.tia.dk/schema/claim/v2/}ClaimCatastrophe">
 *       &lt;sequence>
 *         &lt;element name="nrcEventNo" type="{http://infrastructure.tia.dk/schema/common/v2/}long10" minOccurs="0"/>
 *         &lt;element name="nrcEventType" type="{http://infrastructure.tia.dk/schema/common/v2/}string3" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ClaimNrcEvent", propOrder = {
    "nrcEventNo",
    "nrcEventType"
})
public class ClaimNrcEvent
    extends ClaimCatastrophe
{

    @XmlElement(nillable = true)
    protected Long nrcEventNo;
    @XmlElement(nillable = true)
    protected String nrcEventType;

    /**
     * Gets the value of the nrcEventNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getNrcEventNo() {
        return nrcEventNo;
    }

    /**
     * Sets the value of the nrcEventNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setNrcEventNo(Long value) {
        this.nrcEventNo = value;
    }

    /**
     * Gets the value of the nrcEventType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNrcEventType() {
        return nrcEventType;
    }

    /**
     * Sets the value of the nrcEventType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNrcEventType(String value) {
        this.nrcEventType = value;
    }

}
