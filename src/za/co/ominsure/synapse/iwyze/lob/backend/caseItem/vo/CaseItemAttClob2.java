
package za.co.ominsure.synapse.iwyze.lob.backend.caseItem.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CaseItemAttClob complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CaseItemAttClob">
 *   &lt;complexContent>
 *     &lt;extension base="{http://infrastructure.tia.dk/schema/case/v2/}CaseItemAttachment">
 *       &lt;sequence>
 *         &lt;element name="clob" type="{http://infrastructure.tia.dk/schema/common/v2/}CLOB"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CaseItemAttClob", namespace = "http://infrastructure.tia.dk/schema/case/v2/", propOrder = {
    "clob"
})
public class CaseItemAttClob2
    extends CaseItemAttachment2
{

    @XmlElement(required = true)
    protected String clob;

    /**
     * Gets the value of the clob property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClob() {
        return clob;
    }

    /**
     * Sets the value of the clob property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClob(String value) {
        this.clob = value;
    }

}
