
package za.co.ominsure.synapse.iwyze.lob.backend.caseItem.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="caseItemCollection" type="{http://infrastructure.tia.dk/schema/case/v3/}CaseItemCollection"/>
 *         &lt;element name="result" type="{http://infrastructure.tia.dk/schema/common/v2/}Result"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "caseItemCollection",
    "result"
})
@XmlRootElement(name = "searchCaseItemResponse", namespace = "http://infrastructure.tia.dk/schema/case/v3/")
public class SearchCaseItemResponse {

    @XmlElement(namespace = "http://infrastructure.tia.dk/schema/case/v3/", required = true)
    protected CaseItemCollection caseItemCollection;
    @XmlElement(namespace = "http://infrastructure.tia.dk/schema/case/v3/", required = true)
    protected Result result;

    /**
     * Gets the value of the caseItemCollection property.
     * 
     * @return
     *     possible object is
     *     {@link CaseItemCollection }
     *     
     */
    public CaseItemCollection getCaseItemCollection() {
        if(caseItemCollection ==  null) {
        	caseItemCollection = new CaseItemCollection();
        }
    	return caseItemCollection;
    }

    /**
     * Sets the value of the caseItemCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link CaseItemCollection }
     *     
     */
    public void setCaseItemCollection(CaseItemCollection value) {
        this.caseItemCollection = value;
    }

    /**
     * Gets the value of the result property.
     * 
     * @return
     *     possible object is
     *     {@link Result }
     *     
     */
    public Result getResult() {
        return result;
    }

    /**
     * Sets the value of the result property.
     * 
     * @param value
     *     allowed object is
     *     {@link Result }
     *     
     */
    public void setResult(Result value) {
        this.result = value;
    }

}
