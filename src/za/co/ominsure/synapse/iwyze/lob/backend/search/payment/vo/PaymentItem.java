
package za.co.ominsure.synapse.iwyze.lob.backend.search.payment.vo;

import java.math.BigDecimal;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * Is mapped into TIA metadata type "obj_payment_item".
 * 
 * <p>Java class for PaymentItem complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PaymentItem">
 *   &lt;complexContent>
 *     &lt;extension base="{http://infrastructure.tia.dk/schema/common/v2/}TIAObject">
 *       &lt;sequence>
 *         &lt;element name="accountItemNo" type="{http://infrastructure.tia.dk/schema/common/v2/}seqNo" minOccurs="0"/>
 *         &lt;element name="amount" type="{http://infrastructure.tia.dk/schema/common/v2/}amount" minOccurs="0"/>
 *         &lt;element name="approvedBy" type="{http://infrastructure.tia.dk/schema/common/v2/}userId" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PaymentItem", propOrder = {
    "accountItemNo",
    "amount",
    "approvedBy"
})
public class PaymentItem
    extends TIAObject
{

    @XmlElementRef(name = "accountItemNo", namespace = "http://infrastructure.tia.dk/schema/account/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<Long> accountItemNo;
    @XmlElementRef(name = "amount", namespace = "http://infrastructure.tia.dk/schema/account/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<BigDecimal> amount;
    @XmlElementRef(name = "approvedBy", namespace = "http://infrastructure.tia.dk/schema/account/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> approvedBy;

    /**
     * Gets the value of the accountItemNo property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Long }{@code >}
     *     
     */
    public JAXBElement<Long> getAccountItemNo() {
        return accountItemNo;
    }

    /**
     * Sets the value of the accountItemNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Long }{@code >}
     *     
     */
    public void setAccountItemNo(JAXBElement<Long> value) {
        this.accountItemNo = value;
    }

    /**
     * Gets the value of the amount property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public JAXBElement<BigDecimal> getAmount() {
        return amount;
    }

    /**
     * Sets the value of the amount property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public void setAmount(JAXBElement<BigDecimal> value) {
        this.amount = value;
    }

    /**
     * Gets the value of the approvedBy property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getApprovedBy() {
        return approvedBy;
    }

    /**
     * Sets the value of the approvedBy property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setApprovedBy(JAXBElement<String> value) {
        this.approvedBy = value;
    }

}
