
package za.co.ominsure.synapse.iwyze.lob.backend.policy.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Additional comments
 * 
 * <p>Java class for SearchTokenSsu complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SearchTokenSsu">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="alias" type="{http://infrastructure.tia.dk/schema/common/v2/}string10" minOccurs="0"/>
 *         &lt;element name="serviceSupplierName" type="{http://infrastructure.tia.dk/schema/common/v2/}string32" minOccurs="0"/>
 *         &lt;element name="specialityCollection" type="{http://infrastructure.tia.dk/schema/party/v2/}SpecialityCollection" minOccurs="0"/>
 *         &lt;element name="workAreaCollection" type="{http://infrastructure.tia.dk/schema/party/v2/}WorkAreaCollection" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SearchTokenSsu", namespace = "http://infrastructure.tia.dk/schema/party/v2/", propOrder = {
    "alias",
    "serviceSupplierName",
    "specialityCollection",
    "workAreaCollection"
})
public class SearchTokenSsu {

    @XmlElement(nillable = true)
    protected String alias;
    @XmlElement(nillable = true)
    protected String serviceSupplierName;
    protected SpecialityCollection specialityCollection;
    protected WorkAreaCollection workAreaCollection;

    /**
     * Gets the value of the alias property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAlias() {
        return alias;
    }

    /**
     * Sets the value of the alias property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAlias(String value) {
        this.alias = value;
    }

    /**
     * Gets the value of the serviceSupplierName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceSupplierName() {
        return serviceSupplierName;
    }

    /**
     * Sets the value of the serviceSupplierName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceSupplierName(String value) {
        this.serviceSupplierName = value;
    }

    /**
     * Gets the value of the specialityCollection property.
     * 
     * @return
     *     possible object is
     *     {@link SpecialityCollection }
     *     
     */
    public SpecialityCollection getSpecialityCollection() {
        return specialityCollection;
    }

    /**
     * Sets the value of the specialityCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link SpecialityCollection }
     *     
     */
    public void setSpecialityCollection(SpecialityCollection value) {
        this.specialityCollection = value;
    }

    /**
     * Gets the value of the workAreaCollection property.
     * 
     * @return
     *     possible object is
     *     {@link WorkAreaCollection }
     *     
     */
    public WorkAreaCollection getWorkAreaCollection() {
        return workAreaCollection;
    }

    /**
     * Sets the value of the workAreaCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link WorkAreaCollection }
     *     
     */
    public void setWorkAreaCollection(WorkAreaCollection value) {
        this.workAreaCollection = value;
    }

}
