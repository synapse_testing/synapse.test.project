
package za.co.ominsure.synapse.iwyze.lob.backend.policy.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Additional comments
 * 
 * <p>Java class for RiskTypeGroup complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RiskTypeGroup">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="riskGroupNo" type="{http://infrastructure.tia.dk/schema/common/v2/}int2" minOccurs="0"/>
 *         &lt;element name="riskGroupDescription" type="{http://infrastructure.tia.dk/schema/common/v2/}string60" minOccurs="0"/>
 *         &lt;element name="riskTypeCollection" type="{http://infrastructure.tia.dk/schema/policy/v2/}RiskTypeCollection" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RiskTypeGroup", propOrder = {
    "riskGroupNo",
    "riskGroupDescription",
    "riskTypeCollection"
})
public class RiskTypeGroup {

    @XmlElement(nillable = true)
    protected Long riskGroupNo;
    @XmlElement(nillable = true)
    protected String riskGroupDescription;
    protected RiskTypeCollection riskTypeCollection;

    /**
     * Gets the value of the riskGroupNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getRiskGroupNo() {
        return riskGroupNo;
    }

    /**
     * Sets the value of the riskGroupNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setRiskGroupNo(Long value) {
        this.riskGroupNo = value;
    }

    /**
     * Gets the value of the riskGroupDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRiskGroupDescription() {
        return riskGroupDescription;
    }

    /**
     * Sets the value of the riskGroupDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRiskGroupDescription(String value) {
        this.riskGroupDescription = value;
    }

    /**
     * Gets the value of the riskTypeCollection property.
     * 
     * @return
     *     possible object is
     *     {@link RiskTypeCollection }
     *     
     */
    public RiskTypeCollection getRiskTypeCollection() {
        return riskTypeCollection;
    }

    /**
     * Sets the value of the riskTypeCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link RiskTypeCollection }
     *     
     */
    public void setRiskTypeCollection(RiskTypeCollection value) {
        this.riskTypeCollection = value;
    }

}
