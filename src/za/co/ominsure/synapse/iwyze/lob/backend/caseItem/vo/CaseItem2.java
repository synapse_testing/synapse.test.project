
package za.co.ominsure.synapse.iwyze.lob.backend.caseItem.vo;

import java.math.BigDecimal;
import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;



/**
 * Is mapped into TIA metadata type "obj_code_table".
 * 
 * <p>Java class for CaseItem complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CaseItem">
 *   &lt;complexContent>
 *     &lt;extension base="{http://infrastructure.tia.dk/schema/common/v2/}TIAObject">
 *       &lt;sequence>
 *         &lt;element name="requestId" type="{http://infrastructure.tia.dk/schema/common/v2/}requestId" minOccurs="0"/>
 *         &lt;element name="caseType" type="{http://infrastructure.tia.dk/schema/case/v2/}caseType" minOccurs="0"/>
 *         &lt;element name="caseSubType" type="{http://infrastructure.tia.dk/schema/case/v2/}caseSubType" minOccurs="0"/>
 *         &lt;element name="requestUserId" type="{http://infrastructure.tia.dk/schema/common/v2/}userId" minOccurs="0"/>
 *         &lt;element name="requestUserName" type="{http://infrastructure.tia.dk/schema/common/v2/}userName" minOccurs="0"/>
 *         &lt;element name="assignedUserId" type="{http://infrastructure.tia.dk/schema/common/v2/}userId" minOccurs="0"/>
 *         &lt;element name="assignedUserName" type="{http://infrastructure.tia.dk/schema/common/v2/}userName" minOccurs="0"/>
 *         &lt;element name="writingDate" type="{http://infrastructure.tia.dk/schema/common/v2/}date" minOccurs="0"/>
 *         &lt;element name="actionDate" type="{http://infrastructure.tia.dk/schema/common/v2/}date" minOccurs="0"/>
 *         &lt;element name="completionDate" type="{http://infrastructure.tia.dk/schema/common/v2/}date" minOccurs="0"/>
 *         &lt;element name="actionId" type="{http://infrastructure.tia.dk/schema/common/v2/}actionId" minOccurs="0"/>
 *         &lt;element name="userComm" type="{http://infrastructure.tia.dk/schema/common/v2/}userComm" minOccurs="0"/>
 *         &lt;element name="letterDesc" type="{http://infrastructure.tia.dk/schema/common/v2/}letterDesc" minOccurs="0"/>
 *         &lt;element name="undeletableYN" type="{http://infrastructure.tia.dk/schema/common/v2/}tiaYN" minOccurs="0"/>
 *         &lt;element name="visibleYN" type="{http://infrastructure.tia.dk/schema/common/v2/}tiaYN" minOccurs="0"/>
 *         &lt;element name="receiverIdNo" type="{http://infrastructure.tia.dk/schema/common/v2/}receiverIdNo" minOccurs="0"/>
 *         &lt;element name="receiverName" type="{http://infrastructure.tia.dk/schema/common/v2/}receiverName" minOccurs="0"/>
 *         &lt;element name="emailDetails" type="{http://infrastructure.tia.dk/schema/common/v2/}Email" minOccurs="0"/>
 *         &lt;element name="companyNo" type="{http://infrastructure.tia.dk/schema/common/v2/}companyNo" minOccurs="0"/>
 *         &lt;element name="companyName" type="{http://infrastructure.tia.dk/schema/common/v2/}companyName" minOccurs="0"/>
 *         &lt;element name="departmentNo" type="{http://infrastructure.tia.dk/schema/case/v2/}departmentNo" minOccurs="0"/>
 *         &lt;element name="departmentName" type="{http://infrastructure.tia.dk/schema/common/v2/}departmentName" minOccurs="0"/>
 *         &lt;element name="nameIdNo" type="{http://infrastructure.tia.dk/schema/common/v2/}nameIdNo" minOccurs="0"/>
 *         &lt;element name="customerName" type="{http://infrastructure.tia.dk/schema/common/v2/}customerName" minOccurs="0"/>
 *         &lt;element name="policyNo" type="{http://infrastructure.tia.dk/schema/common/v2/}policyNo" minOccurs="0"/>
 *         &lt;element name="policyLineNo" type="{http://infrastructure.tia.dk/schema/common/v2/}policyNo" minOccurs="0"/>
 *         &lt;element name="claimNo" type="{http://infrastructure.tia.dk/schema/common/v2/}policyNo" minOccurs="0"/>
 *         &lt;element name="subclaimNo" type="{http://infrastructure.tia.dk/schema/common/v2/}policyNo" minOccurs="0"/>
 *         &lt;element name="accountNo" type="{http://infrastructure.tia.dk/schema/common/v2/}policyNo" minOccurs="0"/>
 *         &lt;element name="reminderNo" type="{http://infrastructure.tia.dk/schema/common/v2/}policyNo" minOccurs="0"/>
 *         &lt;element name="complaintNo" type="{http://infrastructure.tia.dk/schema/common/v2/}policyNo" minOccurs="0"/>
 *         &lt;element name="etransNo" type="{http://infrastructure.tia.dk/schema/common/v2/}etransNo" minOccurs="0"/>
 *         &lt;element name="documentTypeId" type="{http://infrastructure.tia.dk/schema/case/v2/}documentTypeId" minOccurs="0"/>
 *         &lt;element name="fileName" type="{http://infrastructure.tia.dk/schema/common/v2/}fileName" minOccurs="0"/>
 *         &lt;element name="letterLocation" type="{http://infrastructure.tia.dk/schema/case/v2/}letterLocation" minOccurs="0"/>
 *         &lt;element name="ssuSerSupCaseNo" type="{http://infrastructure.tia.dk/schema/case/v2/}ssuCaseNo" minOccurs="0"/>
 *         &lt;element name="claItemNo" type="{http://infrastructure.tia.dk/schema/case/v2/}claItemNo" minOccurs="0"/>
 *         &lt;element name="longitude" type="{http://infrastructure.tia.dk/schema/case/v2/}longitude" minOccurs="0"/>
 *         &lt;element name="latitude" type="{http://infrastructure.tia.dk/schema/case/v2/}latitude" minOccurs="0"/>
 *         &lt;element name="objectNo" type="{http://infrastructure.tia.dk/schema/common/v2/}long10" minOccurs="0"/>
 *         &lt;element name="attachment" type="{http://infrastructure.tia.dk/schema/case/v2/}Attachment" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CaseItem", namespace = "http://infrastructure.tia.dk/schema/case/v2/", propOrder = {
    "requestId",
    "caseType",
    "caseSubType",
    "requestUserId",
    "requestUserName",
    "assignedUserId",
    "assignedUserName",
    "writingDate",
    "actionDate",
    "completionDate",
    "actionId",
    "userComm",
    "letterDesc",
    "undeletableYN",
    "visibleYN",
    "receiverIdNo",
    "receiverName",
    "emailDetails",
    "companyNo",
    "companyName",
    "departmentNo",
    "departmentName",
    "nameIdNo",
    "customerName",
    "policyNo",
    "policyLineNo",
    "claimNo",
    "subclaimNo",
    "accountNo",
    "reminderNo",
    "complaintNo",
    "etransNo",
    "documentTypeId",
    "fileName",
    "letterLocation",
    "ssuSerSupCaseNo",
    "claItemNo",
    "longitude",
    "latitude",
    "objectNo",
    "attachment"
})
public class CaseItem2
    extends TIAObject
{

    @XmlElement(nillable = true)
    @XmlSchemaType(name = "unsignedLong")
    protected Long requestId;
    @XmlElement(nillable = true)
    protected String caseType;
    @XmlElement(nillable = true)
    protected String caseSubType;
    @XmlElement(nillable = true)
    protected String requestUserId;
    @XmlElement(nillable = true)
    protected String requestUserName;
    @XmlElement(nillable = true)
    protected String assignedUserId;
    @XmlElement(nillable = true)
    protected String assignedUserName;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "date")
    protected String writingDate;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "date")
    protected String actionDate;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "date")
    protected String completionDate;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "integer")
    protected Integer actionId;
    @XmlElement(nillable = true)
    protected String userComm;
    @XmlElement(nillable = true)
    protected String letterDesc;
    @XmlElement(nillable = true)
    protected String undeletableYN;
    @XmlElement(nillable = true)
    protected String visibleYN;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "unsignedLong")
    protected Long receiverIdNo;
    @XmlElement(nillable = true)
    protected String receiverName;
    @XmlElement(nillable = true)
    protected Email emailDetails;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "unsignedLong")
    protected Integer companyNo;
    @XmlElement(nillable = true)
    protected String companyName;
    @XmlElement(nillable = true)
    protected String departmentNo;
    @XmlElement(nillable = true)
    protected String departmentName;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "unsignedLong")
    protected Long nameIdNo;
    @XmlElement(nillable = true)
    protected String customerName;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "unsignedLong")
    protected BigInteger policyNo;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "unsignedLong")
    protected BigInteger policyLineNo;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "unsignedLong")
    protected BigInteger claimNo;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "unsignedLong")
    protected BigInteger subclaimNo;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "unsignedLong")
    protected BigInteger accountNo;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "unsignedLong")
    protected BigInteger reminderNo;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "unsignedLong")
    protected BigInteger complaintNo;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "integer")
    protected Integer etransNo;
    @XmlElement(nillable = true)
    protected String documentTypeId;
    @XmlElement(nillable = true)
    protected String fileName;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "string")
    protected LetterLocation letterLocation;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "unsignedLong")
    protected Long ssuSerSupCaseNo;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "unsignedLong")
    protected Long claItemNo;
    @XmlElement(nillable = true)
    protected BigDecimal longitude;
    @XmlElement(nillable = true)
    protected BigDecimal latitude;
    @XmlElement(nillable = true)
    protected Long objectNo;
    @XmlElement(nillable = true)
    protected Attachment2 attachment;

    /**
     * Gets the value of the requestId property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getRequestId() {
        return requestId;
    }

    /**
     * Sets the value of the requestId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setRequestId(Long value) {
        this.requestId = value;
    }

    /**
     * Gets the value of the caseType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCaseType() {
        return caseType;
    }

    /**
     * Sets the value of the caseType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCaseType(String value) {
        this.caseType = value;
    }

    /**
     * Gets the value of the caseSubType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCaseSubType() {
        return caseSubType;
    }

    /**
     * Sets the value of the caseSubType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCaseSubType(String value) {
        this.caseSubType = value;
    }

    /**
     * Gets the value of the requestUserId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRequestUserId() {
        return requestUserId;
    }

    /**
     * Sets the value of the requestUserId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRequestUserId(String value) {
        this.requestUserId = value;
    }

    /**
     * Gets the value of the requestUserName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRequestUserName() {
        return requestUserName;
    }

    /**
     * Sets the value of the requestUserName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRequestUserName(String value) {
        this.requestUserName = value;
    }

    /**
     * Gets the value of the assignedUserId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAssignedUserId() {
        return assignedUserId;
    }

    /**
     * Sets the value of the assignedUserId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAssignedUserId(String value) {
        this.assignedUserId = value;
    }

    /**
     * Gets the value of the assignedUserName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAssignedUserName() {
        return assignedUserName;
    }

    /**
     * Sets the value of the assignedUserName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAssignedUserName(String value) {
        this.assignedUserName = value;
    }

    /**
     * Gets the value of the writingDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWritingDate() {
        return writingDate;
    }

    /**
     * Sets the value of the writingDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWritingDate(String value) {
        this.writingDate = value;
    }

    /**
     * Gets the value of the actionDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActionDate() {
        return actionDate;
    }

    /**
     * Sets the value of the actionDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActionDate(String value) {
        this.actionDate = value;
    }

    /**
     * Gets the value of the completionDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCompletionDate() {
        return completionDate;
    }

    /**
     * Sets the value of the completionDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCompletionDate(String value) {
        this.completionDate = value;
    }

    /**
     * Gets the value of the actionId property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getActionId() {
        return actionId;
    }

    /**
     * Sets the value of the actionId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setActionId(Integer value) {
        this.actionId = value;
    }

    /**
     * Gets the value of the userComm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserComm() {
        return userComm;
    }

    /**
     * Sets the value of the userComm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserComm(String value) {
        this.userComm = value;
    }

    /**
     * Gets the value of the letterDesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLetterDesc() {
        return letterDesc;
    }

    /**
     * Sets the value of the letterDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLetterDesc(String value) {
        this.letterDesc = value;
    }

    /**
     * Gets the value of the undeletableYN property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUndeletableYN() {
        return undeletableYN;
    }

    /**
     * Sets the value of the undeletableYN property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUndeletableYN(String value) {
        this.undeletableYN = value;
    }

    /**
     * Gets the value of the visibleYN property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVisibleYN() {
        return visibleYN;
    }

    /**
     * Sets the value of the visibleYN property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVisibleYN(String value) {
        this.visibleYN = value;
    }

    /**
     * Gets the value of the receiverIdNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getReceiverIdNo() {
        return receiverIdNo;
    }

    /**
     * Sets the value of the receiverIdNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setReceiverIdNo(Long value) {
        this.receiverIdNo = value;
    }

    /**
     * Gets the value of the receiverName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReceiverName() {
        return receiverName;
    }

    /**
     * Sets the value of the receiverName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReceiverName(String value) {
        this.receiverName = value;
    }

    /**
     * Gets the value of the emailDetails property.
     * 
     * @return
     *     possible object is
     *     {@link Email }
     *     
     */
    public Email getEmailDetails() {
        return emailDetails;
    }

    /**
     * Sets the value of the emailDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link Email }
     *     
     */
    public void setEmailDetails(Email value) {
        this.emailDetails = value;
    }

    /**
     * Gets the value of the companyNo property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCompanyNo() {
        return companyNo;
    }

    /**
     * Sets the value of the companyNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCompanyNo(Integer value) {
        this.companyNo = value;
    }

    /**
     * Gets the value of the companyName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCompanyName() {
        return companyName;
    }

    /**
     * Sets the value of the companyName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCompanyName(String value) {
        this.companyName = value;
    }

    /**
     * Gets the value of the departmentNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDepartmentNo() {
        return departmentNo;
    }

    /**
     * Sets the value of the departmentNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDepartmentNo(String value) {
        this.departmentNo = value;
    }

    /**
     * Gets the value of the departmentName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDepartmentName() {
        return departmentName;
    }

    /**
     * Sets the value of the departmentName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDepartmentName(String value) {
        this.departmentName = value;
    }

    /**
     * Gets the value of the nameIdNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getNameIdNo() {
        return nameIdNo;
    }

    /**
     * Sets the value of the nameIdNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setNameIdNo(Long value) {
        this.nameIdNo = value;
    }

    /**
     * Gets the value of the customerName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomerName() {
        return customerName;
    }

    /**
     * Sets the value of the customerName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomerName(String value) {
        this.customerName = value;
    }

    /**
     * Gets the value of the policyNo property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getPolicyNo() {
        return policyNo;
    }

    /**
     * Sets the value of the policyNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setPolicyNo(BigInteger value) {
        this.policyNo = value;
    }

    /**
     * Gets the value of the policyLineNo property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getPolicyLineNo() {
        return policyLineNo;
    }

    /**
     * Sets the value of the policyLineNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setPolicyLineNo(BigInteger value) {
        this.policyLineNo = value;
    }

    /**
     * Gets the value of the claimNo property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getClaimNo() {
        return claimNo;
    }

    /**
     * Sets the value of the claimNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setClaimNo(BigInteger value) {
        this.claimNo = value;
    }

    /**
     * Gets the value of the subclaimNo property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getSubclaimNo() {
        return subclaimNo;
    }

    /**
     * Sets the value of the subclaimNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setSubclaimNo(BigInteger value) {
        this.subclaimNo = value;
    }

    /**
     * Gets the value of the accountNo property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getAccountNo() {
        return accountNo;
    }

    /**
     * Sets the value of the accountNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setAccountNo(BigInteger value) {
        this.accountNo = value;
    }

    /**
     * Gets the value of the reminderNo property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getReminderNo() {
        return reminderNo;
    }

    /**
     * Sets the value of the reminderNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setReminderNo(BigInteger value) {
        this.reminderNo = value;
    }

    /**
     * Gets the value of the complaintNo property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getComplaintNo() {
        return complaintNo;
    }

    /**
     * Sets the value of the complaintNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setComplaintNo(BigInteger value) {
        this.complaintNo = value;
    }

    /**
     * Gets the value of the etransNo property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getEtransNo() {
        return etransNo;
    }

    /**
     * Sets the value of the etransNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setEtransNo(Integer value) {
        this.etransNo = value;
    }

    /**
     * Gets the value of the documentTypeId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDocumentTypeId() {
        return documentTypeId;
    }

    /**
     * Sets the value of the documentTypeId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDocumentTypeId(String value) {
        this.documentTypeId = value;
    }

    /**
     * Gets the value of the fileName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * Sets the value of the fileName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFileName(String value) {
        this.fileName = value;
    }

    /**
     * Gets the value of the letterLocation property.
     * 
     * @return
     *     possible object is
     *     {@link LetterLocation }
     *     
     */
    public LetterLocation getLetterLocation() {
        return letterLocation;
    }

    /**
     * Sets the value of the letterLocation property.
     * 
     * @param value
     *     allowed object is
     *     {@link LetterLocation }
     *     
     */
    public void setLetterLocation(LetterLocation value) {
        this.letterLocation = value;
    }

    /**
     * Gets the value of the ssuSerSupCaseNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getSsuSerSupCaseNo() {
        return ssuSerSupCaseNo;
    }

    /**
     * Sets the value of the ssuSerSupCaseNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setSsuSerSupCaseNo(Long value) {
        this.ssuSerSupCaseNo = value;
    }

    /**
     * Gets the value of the claItemNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getClaItemNo() {
        return claItemNo;
    }

    /**
     * Sets the value of the claItemNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setClaItemNo(Long value) {
        this.claItemNo = value;
    }

    /**
     * Gets the value of the longitude property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getLongitude() {
        return longitude;
    }

    /**
     * Sets the value of the longitude property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setLongitude(BigDecimal value) {
        this.longitude = value;
    }

    /**
     * Gets the value of the latitude property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getLatitude() {
        return latitude;
    }

    /**
     * Sets the value of the latitude property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setLatitude(BigDecimal value) {
        this.latitude = value;
    }

    /**
     * Gets the value of the objectNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getObjectNo() {
        return objectNo;
    }

    /**
     * Sets the value of the objectNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setObjectNo(Long value) {
        this.objectNo = value;
    }

    /**
     * Gets the value of the attachment property.
     * 
     * @return
     *     possible object is
     *     {@link Attachment2 }
     *     
     */
    public Attachment2 getAttachment() {
        return attachment;
    }

    /**
     * Sets the value of the attachment property.
     * 
     * @param value
     *     allowed object is
     *     {@link Attachment2 }
     *     
     */
    public void setAttachment(Attachment2 value) {
        this.attachment = value;
    }

}
