
package za.co.ominsure.synapse.iwyze.lob.backend.claim.payment.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="inputToken" type="{http://infrastructure.tia.dk/schema/common/v2/}InputToken"/>
 *         &lt;element name="lockingMode" type="{http://infrastructure.tia.dk/schema/common/v2/}lockingMode" minOccurs="0"/>
 *         &lt;element name="claimPaymentPlan" type="{http://infrastructure.tia.dk/schema/claim/v2/}ClaimPaymentPlan"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "inputToken",
    "lockingMode",
    "claimPaymentPlan"
})
@XmlRootElement(name = "modifyClaimPaymentPlanRequest")
public class ModifyClaimPaymentPlanRequest {

    @XmlElement(required = true)
    protected InputToken inputToken;
    protected String lockingMode;
    @XmlElement(required = true)
    protected ClaimPaymentPlan claimPaymentPlan;

    /**
     * Gets the value of the inputToken property.
     * 
     * @return
     *     possible object is
     *     {@link InputToken }
     *     
     */
    public InputToken getInputToken() {
        return inputToken;
    }

    /**
     * Sets the value of the inputToken property.
     * 
     * @param value
     *     allowed object is
     *     {@link InputToken }
     *     
     */
    public void setInputToken(InputToken value) {
        this.inputToken = value;
    }

    /**
     * Gets the value of the lockingMode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLockingMode() {
        return lockingMode;
    }

    /**
     * Sets the value of the lockingMode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLockingMode(String value) {
        this.lockingMode = value;
    }

    /**
     * Gets the value of the claimPaymentPlan property.
     * 
     * @return
     *     possible object is
     *     {@link ClaimPaymentPlan }
     *     
     */
    public ClaimPaymentPlan getClaimPaymentPlan() {
        return claimPaymentPlan;
    }

    /**
     * Sets the value of the claimPaymentPlan property.
     * 
     * @param value
     *     allowed object is
     *     {@link ClaimPaymentPlan }
     *     
     */
    public void setClaimPaymentPlan(ClaimPaymentPlan value) {
        this.claimPaymentPlan = value;
    }

}
