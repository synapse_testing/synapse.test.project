
package za.co.ominsure.synapse.iwyze.lob.backend.party.vo;

import java.math.BigDecimal;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the za.co.ominsure.synapse.iwyze.lob.backend.party.vo package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _AffinityRelationAffinityNo_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v2/", "affinityNo");
    private final static QName _AffinityRelationOwnerNameIdNo_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v2/", "ownerNameIdNo");
    private final static QName _Institution2CompanyVatNo_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v2/", "companyVatNo");
    private final static QName _Institution2ContactPerson_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v2/", "contactPerson");
    private final static QName _Institution2CompanyRegNo_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v2/", "companyRegNo");
    private final static QName _SearchTokenBanBankName_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v2/", "bankName");
    private final static QName _SearchTokenBanBankCode_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v2/", "bankCode");
    private final static QName _PartyOther2CivilReg_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v2/", "civilReg");
    private final static QName _PartyOther2Title_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v2/", "title");
    private final static QName _PartyOther2BirthDate_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v2/", "birthDate");
    private final static QName _PartyOther2Forename_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v2/", "forename");
    private final static QName _ClaimEventRelationClaimEventNo_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v2/", "claimEventNo");
    private final static QName _ClaimEventRelationMemberNameIdNo_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v2/", "memberNameIdNo");
    private final static QName _SearchTokenLocAddressCode_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v3/", "addressCode");
    private final static QName _SearchTokenLocSiteShortName_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v3/", "siteShortName");
    private final static QName _SearchTokenLocSearchMethod_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v3/", "searchMethod");
    private final static QName _TIAObjectSiteSpecifics_QNAME = new QName("http://infrastructure.tia.dk/schema/common/v2/", "siteSpecifics");
    private final static QName _TIAObjectExternalId_QNAME = new QName("http://infrastructure.tia.dk/schema/common/v2/", "externalId");
    private final static QName _TIAObjectCountrySpecifics_QNAME = new QName("http://infrastructure.tia.dk/schema/common/v2/", "countrySpecifics");
    private final static QName _TIAObjectFlexfieldCollection_QNAME = new QName("http://infrastructure.tia.dk/schema/common/v2/", "flexfieldCollection");
    private final static QName _TIAObjectVersionToken_QNAME = new QName("http://infrastructure.tia.dk/schema/common/v2/", "versionToken");
    private final static QName _RelationDescription_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v2/", "description");
    private final static QName _RelationStartDate_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v2/", "startDate");
    private final static QName _RelationNewest_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v2/", "newest");
    private final static QName _RelationRelationType_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v2/", "relationType");
    private final static QName _RelationOwnerDesc_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v2/", "ownerDesc");
    private final static QName _RelationTransId_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v2/", "transId");
    private final static QName _RelationSeqNo_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v2/", "seqNo");
    private final static QName _RelationEndDate_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v2/", "endDate");
    private final static QName _ClaimThirdPartyRelationThirdPartyNo_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v2/", "thirdPartyNo");
    private final static QName _Customer2SpouseName_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v2/", "spouseName");
    private final static QName _Customer2ProfileCode_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v2/", "profileCode");
    private final static QName _Customer2SpouseBirthDate_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v2/", "spouseBirthDate");
    private final static QName _Customer2Household_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v2/", "household");
    private final static QName _Customer2StatusCode_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v2/", "statusCode");
    private final static QName _Customer2Potential_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v2/", "potential");
    private final static QName _Customer2OccupationCode_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v2/", "occupationCode");
    private final static QName _Customer2MaritalState_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v2/", "maritalState");
    private final static QName _Customer2Children_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v2/", "children");
    private final static QName _Customer2ReminderGroup_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v2/", "reminderGroup");
    private final static QName _Customer2HowToContact_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v2/", "howToContact");
    private final static QName _Customer2LastContactWay_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v2/", "lastContactWay");
    private final static QName _Customer2SpouseSex_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v2/", "spouseSex");
    private final static QName _Customer2HowNotToContact_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v2/", "howNotToContact");
    private final static QName _Customer2FirstContact_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v2/", "firstContact");
    private final static QName _Customer2LastContact_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v2/", "lastContact");
    private final static QName _Customer2IncomeGroup_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v2/", "incomeGroup");
    private final static QName _Customer2Education_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v2/", "education");
    private final static QName _Customer2Occupation_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v2/", "occupation");
    private final static QName _Customer2YourRef_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v2/", "yourRef");
    private final static QName _Customer2Sex_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v2/", "sex");
    private final static QName _Customer2ServiceCode_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v2/", "serviceCode");
    private final static QName _BankLocked_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v2/", "locked");
    private final static QName _BankBankIdNo_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v2/", "bankIdNo");
    private final static QName _BankBankCodeType_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v2/", "bankCodeType");
    private final static QName _BankNotes_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v2/", "notes");
    private final static QName _AbstractParty2Language_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v2/", "language");
    private final static QName _AbstractParty2Name_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v2/", "name");
    private final static QName _AbstractParty2CustomerInfo_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v2/", "customerInfo");
    private final static QName _AbstractParty2IdNoAlt_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v2/", "idNoAlt");
    private final static QName _AbstractParty2IdNo_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v2/", "idNo");
    private final static QName _AbstractParty2CurrencyCode_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v2/", "currencyCode");
    private final static QName _AbstractParty2NameSort_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v2/", "nameSort");
    private final static QName _AbstractParty2IdNoSort_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v2/", "idNoSort");
    private final static QName _AbstractParty2NameType_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v2/", "nameType");
    private final static QName _AbstractParty2Surname_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v2/", "surname");
    private final static QName _AbstractParty2ObsoleteCode_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v2/", "obsoleteCode");
    private final static QName _CustomerAdvertising_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v3/", "advertising");
    private final static QName _CustomerAdvertisingDate_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v3/", "advertisingDate");
    private final static QName _CustomerNextContactWay_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v3/", "nextContactWay");
    private final static QName _CustomerHandler_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v3/", "handler");
    private final static QName _CustomerNextContact_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v3/", "nextContact");
    private final static QName _CustomerCustomerGroup_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v3/", "customerGroup");
    private final static QName _CustomerCustomerSegment_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v3/", "customerSegment");
    private final static QName _CustomerAgentNo_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v3/", "agentNo");
    private final static QName _SearchTokenSsuAlias_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v2/", "alias");
    private final static QName _SearchTokenSsuServiceSupplierName_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v2/", "serviceSupplierName");
    private final static QName _AccountRelationAccountNo_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v2/", "accountNo");
    private final static QName _InstitutionContactPerson_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v3/", "contactPerson");
    private final static QName _InstitutionCompanyVatNo_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v3/", "companyVatNo");
    private final static QName _InstitutionIndustryCode_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v3/", "industryCode");
    private final static QName _InstitutionEmployees_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v3/", "employees");
    private final static QName _InstitutionCompanyRegNo_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v3/", "companyRegNo");
    private final static QName _InstitutionInstitutionCode_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v3/", "institutionCode");
    private final static QName _EmailEmailBCC_QNAME = new QName("http://infrastructure.tia.dk/schema/common/v2/", "emailBCC");
    private final static QName _EmailEmailCC_QNAME = new QName("http://infrastructure.tia.dk/schema/common/v2/", "emailCC");
    private final static QName _InsuranceCompanyInsuranceCompanyName_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v2/", "insuranceCompanyName");
    private final static QName _InsuranceCompanyInsurerCode_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v2/", "insurerCode");
    private final static QName _InsuranceCompanyParentInsurerCode_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v2/", "parentInsurerCode");
    private final static QName _InsuranceCompanyPeriodOfCancellation_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v2/", "periodOfCancellation");
    private final static QName _PartyElementPartyOther_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v3/", "partyOther");
    private final static QName _PartyElementInstitution_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v3/", "institution");
    private final static QName _PartyElementPerson_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v3/", "person");
    private final static QName _PolicyObjectRelationPolicyObjectNo_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v2/", "policyObjectNo");
    private final static QName _Person2CivilRegCode_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v2/", "civilRegCode");
    private final static QName _SubclaimRelationSubclaimNo_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v2/", "subclaimNo");
    private final static QName _LocationLanguage_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v3/", "language");
    private final static QName _LocationName_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v3/", "name");
    private final static QName _LocationSiteSeqNo_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v3/", "siteSeqNo");
    private final static QName _LocationIdNo_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v3/", "idNo");
    private final static QName _LocationCurrencyCode_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v3/", "currencyCode");
    private final static QName _LocationNameSort_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v3/", "nameSort");
    private final static QName _LocationIdNoAlt_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v3/", "idNoAlt");
    private final static QName _LocationIdNoSort_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v3/", "idNoSort");
    private final static QName _LocationSurname_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v3/", "surname");
    private final static QName _LocationObsoleteCode_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v3/", "obsoleteCode");
    private final static QName _PolicyLineRelationPolicyLineNo_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v2/", "policyLineNo");
    private final static QName _ClaimRelationClaimNo_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v2/", "claimNo");
    private final static QName _InputTokenCorrelationId_QNAME = new QName("http://infrastructure.tia.dk/schema/common/v2/", "correlationId");
    private final static QName _Address2PostArea_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v2/", "postArea");
    private final static QName _Address2Floor_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v2/", "floor");
    private final static QName _Address2PostalRegion_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v2/", "postalRegion");
    private final static QName _Address2Street_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v2/", "street");
    private final static QName _Address2CountryCode_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v2/", "countryCode");
    private final static QName _Address2County_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v2/", "county");
    private final static QName _Address2HouseCoName_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v2/", "houseCoName");
    private final static QName _Address2PostStreet_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v2/", "postStreet");
    private final static QName _Address2Country_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v2/", "country");
    private final static QName _Address2AddressType_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v2/", "addressType");
    private final static QName _Address2FloorExt_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v2/", "floorExt");
    private final static QName _Address2StreetNo_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v2/", "streetNo");
    private final static QName _Address2City_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v2/", "city");
    private final static QName _SearchTokenAgtComGroup_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v2/", "comGroup");
    private final static QName _SearchTokenAgtAgentName_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v2/", "agentName");
    private final static QName _SearchTokenAgtComCategory_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v2/", "comCategory");
    private final static QName _ContactInfoContactInfoDetail_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v2/", "contactInfoDetail");
    private final static QName _ContactInfoContactInfoType_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v2/", "contactInfoType");
    private final static QName _PartyOtherMiddleName_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v3/", "middleName");
    private final static QName _PartyOtherCivilReg_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v3/", "civilReg");
    private final static QName _PartyOtherHiddenIdentity_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v3/", "hiddenIdentity");
    private final static QName _PartyOtherDeceasedDate_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v3/", "deceasedDate");
    private final static QName _PartyOtherForename_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v3/", "forename");
    private final static QName _PartyOtherEmigrationDate_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v3/", "emigrationDate");
    private final static QName _PartyOtherBirthDate_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v3/", "birthDate");
    private final static QName _PartyOtherTitle_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v3/", "title");
    private final static QName _AbstractPartyNameType_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v3/", "nameType");
    private final static QName _AbstractPartyCustomerInfo_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v3/", "customerInfo");
    private final static QName _AbstractPartyNote_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v3/", "note");
    private final static QName _AddressPostArea_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v3/", "postArea");
    private final static QName _AddressDependStreetDesc_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v3/", "dependStreetDesc");
    private final static QName _AddressBuildingNumber_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v3/", "buildingNumber");
    private final static QName _AddressDependLocality_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v3/", "dependLocality");
    private final static QName _AddressPostalRegion_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v3/", "postalRegion");
    private final static QName _AddressFloor_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v3/", "floor");
    private final static QName _AddressBuildingName_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v3/", "buildingName");
    private final static QName _AddressPoBox_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v3/", "poBox");
    private final static QName _AddressStreet_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v3/", "street");
    private final static QName _AddressCountryCode_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v3/", "countryCode");
    private final static QName _AddressCounty_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v3/", "county");
    private final static QName _AddressHouseCoName_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v3/", "houseCoName");
    private final static QName _AddressPostStreet_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v3/", "postStreet");
    private final static QName _AddressUnknownAddress_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v3/", "unknownAddress");
    private final static QName _AddressCountry_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v3/", "country");
    private final static QName _AddressDepartmentName_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v3/", "departmentName");
    private final static QName _AddressSubBuilding_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v3/", "subBuilding");
    private final static QName _AddressAddressType_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v3/", "addressType");
    private final static QName _AddressFloorExt_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v3/", "floorExt");
    private final static QName _AddressStreetNo_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v3/", "streetNo");
    private final static QName _AddressCity_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v3/", "city");
    private final static QName _AddressDependStreet_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v3/", "dependStreet");
    private final static QName _AddressStreetDescriptor_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v3/", "streetDescriptor");
    private final static QName _ServiceSupplierServiceSupplierType_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v2/", "serviceSupplierType");
    private final static QName _ServiceSupplierSsuContactPerson_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v2/", "ssuContactPerson");
    private final static QName _ServiceSupplierBlacklistedReason_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v2/", "blacklistedReason");
    private final static QName _ServiceSupplierPriority_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v2/", "priority");
    private final static QName _ServiceSupplierBlacklisted_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v2/", "blacklisted");
    private final static QName _PersonCivilRegCode_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v3/", "civilRegCode");
    private final static QName _ClaimItemRelationClaimItemNo_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v2/", "claimItemNo");
    private final static QName _SearchTokenAffAffinityGroupName_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v2/", "affinityGroupName");
    private final static QName _SearchTokenAffAffinityGroupDesc_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v2/", "affinityGroupDesc");
    private final static QName _TranslatedCodeDescription_QNAME = new QName("http://infrastructure.tia.dk/schema/common/v2/", "description");
    private final static QName _TranslatedCodeDiplayCode_QNAME = new QName("http://infrastructure.tia.dk/schema/common/v2/", "diplayCode");
    private final static QName _TranslatedCodeHelpText_QNAME = new QName("http://infrastructure.tia.dk/schema/common/v2/", "helpText");
    private final static QName _PolicyRelationPolicyNo_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v2/", "policyNo");
    private final static QName _ValueVarchar2Value_QNAME = new QName("http://infrastructure.tia.dk/schema/common/v2/", "varchar2Value");
    private final static QName _ValueNumberValue_QNAME = new QName("http://infrastructure.tia.dk/schema/common/v2/", "numberValue");
    private final static QName _ValueDateValue_QNAME = new QName("http://infrastructure.tia.dk/schema/common/v2/", "dateValue");
    private final static QName _PartyElement2PartyOther_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v2/", "partyOther");
    private final static QName _PartyElement2Institution_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v2/", "institution");
    private final static QName _PartyElement2Person_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v2/", "person");
    private final static QName _PartyRoleRoleDescription_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v2/", "roleDescription");
    private final static QName _PartyRoleRole_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v2/", "role");
    private final static QName _InterestedPartyIpType_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v2/", "ipType");
    private final static QName _InterestedPartyTypeOther_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v2/", "typeOther");
    private final static QName _AffinityDescriptionNote_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v2/", "descriptionNote");
    private final static QName _AffinityDocumentPile_QNAME = new QName("http://infrastructure.tia.dk/schema/party/v2/", "documentPile");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: za.co.ominsure.synapse.iwyze.lob.backend.party.vo
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetPartyRequest }
     * 
     */
    public GetPartyRequest createGetPartyRequest() {
        return new GetPartyRequest();
    }

    /**
     * Create an instance of {@link InputToken }
     * 
     */
    public InputToken createInputToken() {
        return new InputToken();
    }

    /**
     * Create an instance of {@link CreatePartyResponse }
     * 
     */
    public CreatePartyResponse createCreatePartyResponse() {
        return new CreatePartyResponse();
    }

    /**
     * Create an instance of {@link PartyElement }
     * 
     */
    public PartyElement createPartyElement() {
        return new PartyElement();
    }

    /**
     * Create an instance of {@link Result }
     * 
     */
    public Result createResult() {
        return new Result();
    }

    /**
     * Create an instance of {@link ModifyPartyRequest }
     * 
     */
    public ModifyPartyRequest createModifyPartyRequest() {
        return new ModifyPartyRequest();
    }

    /**
     * Create an instance of {@link CreatePartyRequest }
     * 
     */
    public CreatePartyRequest createCreatePartyRequest() {
        return new CreatePartyRequest();
    }

    /**
     * Create an instance of {@link SearchPartyResponse }
     * 
     */
    public SearchPartyResponse createSearchPartyResponse() {
        return new SearchPartyResponse();
    }

    /**
     * Create an instance of {@link PartyCollection }
     * 
     */
    public PartyCollection createPartyCollection() {
        return new PartyCollection();
    }

    /**
     * Create an instance of {@link GetPartyResponse }
     * 
     */
    public GetPartyResponse createGetPartyResponse() {
        return new GetPartyResponse();
    }

    /**
     * Create an instance of {@link ModifyPartyResponse }
     * 
     */
    public ModifyPartyResponse createModifyPartyResponse() {
        return new ModifyPartyResponse();
    }

    /**
     * Create an instance of {@link GetPartyByAltIdRequest }
     * 
     */
    public GetPartyByAltIdRequest createGetPartyByAltIdRequest() {
        return new GetPartyByAltIdRequest();
    }

    /**
     * Create an instance of {@link SearchPartyRequest }
     * 
     */
    public SearchPartyRequest createSearchPartyRequest() {
        return new SearchPartyRequest();
    }

    /**
     * Create an instance of {@link SearchTokenPar }
     * 
     */
    public SearchTokenPar createSearchTokenPar() {
        return new SearchTokenPar();
    }

    /**
     * Create an instance of {@link PageSort }
     * 
     */
    public PageSort createPageSort() {
        return new PageSort();
    }

    /**
     * Create an instance of {@link LocationCollection }
     * 
     */
    public LocationCollection createLocationCollection() {
        return new LocationCollection();
    }

    /**
     * Create an instance of {@link Address }
     * 
     */
    public Address createAddress() {
        return new Address();
    }

    /**
     * Create an instance of {@link Customer }
     * 
     */
    public Customer createCustomer() {
        return new Customer();
    }

    /**
     * Create an instance of {@link AddressCollection }
     * 
     */
    public AddressCollection createAddressCollection() {
        return new AddressCollection();
    }

    /**
     * Create an instance of {@link Institution }
     * 
     */
    public Institution createInstitution() {
        return new Institution();
    }

    /**
     * Create an instance of {@link SearchTokenLoc }
     * 
     */
    public SearchTokenLoc createSearchTokenLoc() {
        return new SearchTokenLoc();
    }

    /**
     * Create an instance of {@link PartyOther }
     * 
     */
    public PartyOther createPartyOther() {
        return new PartyOther();
    }

    /**
     * Create an instance of {@link SearchTokenIntParty }
     * 
     */
    public SearchTokenIntParty createSearchTokenIntParty() {
        return new SearchTokenIntParty();
    }

    /**
     * Create an instance of {@link Person }
     * 
     */
    public Person createPerson() {
        return new Person();
    }

    /**
     * Create an instance of {@link Location }
     * 
     */
    public Location createLocation() {
        return new Location();
    }

    /**
     * Create an instance of {@link InsuranceCompany }
     * 
     */
    public InsuranceCompany createInsuranceCompany() {
        return new InsuranceCompany();
    }

    /**
     * Create an instance of {@link Address2 }
     * 
     */
    public Address2 createAddress2() {
        return new Address2();
    }

    /**
     * Create an instance of {@link InterestedParty }
     * 
     */
    public InterestedParty createInterestedParty() {
        return new InterestedParty();
    }

    /**
     * Create an instance of {@link Customer2 }
     * 
     */
    public Customer2 createCustomer2() {
        return new Customer2();
    }

    /**
     * Create an instance of {@link PolicyRelation }
     * 
     */
    public PolicyRelation createPolicyRelation() {
        return new PolicyRelation();
    }

    /**
     * Create an instance of {@link SearchTokenInc }
     * 
     */
    public SearchTokenInc createSearchTokenInc() {
        return new SearchTokenInc();
    }

    /**
     * Create an instance of {@link Institution2 }
     * 
     */
    public Institution2 createInstitution2() {
        return new Institution2();
    }

    /**
     * Create an instance of {@link SearchTokenAff }
     * 
     */
    public SearchTokenAff createSearchTokenAff() {
        return new SearchTokenAff();
    }

    /**
     * Create an instance of {@link PartyCollection2 }
     * 
     */
    public PartyCollection2 createPartyCollection2() {
        return new PartyCollection2();
    }

    /**
     * Create an instance of {@link ContactInfo }
     * 
     */
    public ContactInfo createContactInfo() {
        return new ContactInfo();
    }

    /**
     * Create an instance of {@link PartyRoleCollection }
     * 
     */
    public PartyRoleCollection createPartyRoleCollection() {
        return new PartyRoleCollection();
    }

    /**
     * Create an instance of {@link PartyRole }
     * 
     */
    public PartyRole createPartyRole() {
        return new PartyRole();
    }

    /**
     * Create an instance of {@link RelationCollection }
     * 
     */
    public RelationCollection createRelationCollection() {
        return new RelationCollection();
    }

    /**
     * Create an instance of {@link Agent }
     * 
     */
    public Agent createAgent() {
        return new Agent();
    }

    /**
     * Create an instance of {@link ClaimEventRelation }
     * 
     */
    public ClaimEventRelation createClaimEventRelation() {
        return new ClaimEventRelation();
    }

    /**
     * Create an instance of {@link PolicyObjectRelation }
     * 
     */
    public PolicyObjectRelation createPolicyObjectRelation() {
        return new PolicyObjectRelation();
    }

    /**
     * Create an instance of {@link ContactInfoCollection }
     * 
     */
    public ContactInfoCollection createContactInfoCollection() {
        return new ContactInfoCollection();
    }

    /**
     * Create an instance of {@link SearchTokenAgt }
     * 
     */
    public SearchTokenAgt createSearchTokenAgt() {
        return new SearchTokenAgt();
    }

    /**
     * Create an instance of {@link Affinity }
     * 
     */
    public Affinity createAffinity() {
        return new Affinity();
    }

    /**
     * Create an instance of {@link PartyElement2 }
     * 
     */
    public PartyElement2 createPartyElement2() {
        return new PartyElement2();
    }

    /**
     * Create an instance of {@link SearchTokenBan }
     * 
     */
    public SearchTokenBan createSearchTokenBan() {
        return new SearchTokenBan();
    }

    /**
     * Create an instance of {@link AccountRelation }
     * 
     */
    public AccountRelation createAccountRelation() {
        return new AccountRelation();
    }

    /**
     * Create an instance of {@link Person2 }
     * 
     */
    public Person2 createPerson2() {
        return new Person2();
    }

    /**
     * Create an instance of {@link SearchTokenSsu }
     * 
     */
    public SearchTokenSsu createSearchTokenSsu() {
        return new SearchTokenSsu();
    }

    /**
     * Create an instance of {@link LocationCollection2 }
     * 
     */
    public LocationCollection2 createLocationCollection2() {
        return new LocationCollection2();
    }

    /**
     * Create an instance of {@link Bank }
     * 
     */
    public Bank createBank() {
        return new Bank();
    }

    /**
     * Create an instance of {@link AffinityRelation }
     * 
     */
    public AffinityRelation createAffinityRelation() {
        return new AffinityRelation();
    }

    /**
     * Create an instance of {@link RelationWrapperType }
     * 
     */
    public RelationWrapperType createRelationWrapperType() {
        return new RelationWrapperType();
    }

    /**
     * Create an instance of {@link ServiceSupplier }
     * 
     */
    public ServiceSupplier createServiceSupplier() {
        return new ServiceSupplier();
    }

    /**
     * Create an instance of {@link AddressCollection2 }
     * 
     */
    public AddressCollection2 createAddressCollection2() {
        return new AddressCollection2();
    }

    /**
     * Create an instance of {@link ClaimThirdPartyRelation }
     * 
     */
    public ClaimThirdPartyRelation createClaimThirdPartyRelation() {
        return new ClaimThirdPartyRelation();
    }

    /**
     * Create an instance of {@link ClaimRelation }
     * 
     */
    public ClaimRelation createClaimRelation() {
        return new ClaimRelation();
    }

    /**
     * Create an instance of {@link WorkAreaCollection }
     * 
     */
    public WorkAreaCollection createWorkAreaCollection() {
        return new WorkAreaCollection();
    }

    /**
     * Create an instance of {@link SearchTokenPar2 }
     * 
     */
    public SearchTokenPar2 createSearchTokenPar2() {
        return new SearchTokenPar2();
    }

    /**
     * Create an instance of {@link SpecialityCollection }
     * 
     */
    public SpecialityCollection createSpecialityCollection() {
        return new SpecialityCollection();
    }

    /**
     * Create an instance of {@link PolicyLineRelation }
     * 
     */
    public PolicyLineRelation createPolicyLineRelation() {
        return new PolicyLineRelation();
    }

    /**
     * Create an instance of {@link PartyRelation }
     * 
     */
    public PartyRelation createPartyRelation() {
        return new PartyRelation();
    }

    /**
     * Create an instance of {@link SearchTokenLoc2 }
     * 
     */
    public SearchTokenLoc2 createSearchTokenLoc2() {
        return new SearchTokenLoc2();
    }

    /**
     * Create an instance of {@link PartyOther2 }
     * 
     */
    public PartyOther2 createPartyOther2() {
        return new PartyOther2();
    }

    /**
     * Create an instance of {@link SubclaimRelation }
     * 
     */
    public SubclaimRelation createSubclaimRelation() {
        return new SubclaimRelation();
    }

    /**
     * Create an instance of {@link ClaimItemRelation }
     * 
     */
    public ClaimItemRelation createClaimItemRelation() {
        return new ClaimItemRelation();
    }

    /**
     * Create an instance of {@link Location2 }
     * 
     */
    public Location2 createLocation2() {
        return new Location2();
    }

    /**
     * Create an instance of {@link FaultMessageDetail }
     * 
     */
    public FaultMessageDetail createFaultMessageDetail() {
        return new FaultMessageDetail();
    }

    /**
     * Create an instance of {@link FlexfieldCollection }
     * 
     */
    public FlexfieldCollection createFlexfieldCollection() {
        return new FlexfieldCollection();
    }

    /**
     * Create an instance of {@link ValueTranslationPolicy }
     * 
     */
    public ValueTranslationPolicy createValueTranslationPolicy() {
        return new ValueTranslationPolicy();
    }

    /**
     * Create an instance of {@link NumberAttribute }
     * 
     */
    public NumberAttribute createNumberAttribute() {
        return new NumberAttribute();
    }

    /**
     * Create an instance of {@link KeyPair }
     * 
     */
    public KeyPair createKeyPair() {
        return new KeyPair();
    }

    /**
     * Create an instance of {@link TranslatedCode }
     * 
     */
    public TranslatedCode createTranslatedCode() {
        return new TranslatedCode();
    }

    /**
     * Create an instance of {@link ValueTranslationCollection }
     * 
     */
    public ValueTranslationCollection createValueTranslationCollection() {
        return new ValueTranslationCollection();
    }

    /**
     * Create an instance of {@link VersionToken }
     * 
     */
    public VersionToken createVersionToken() {
        return new VersionToken();
    }

    /**
     * Create an instance of {@link StringCollection }
     * 
     */
    public StringCollection createStringCollection() {
        return new StringCollection();
    }

    /**
     * Create an instance of {@link Attribute }
     * 
     */
    public Attribute createAttribute() {
        return new Attribute();
    }

    /**
     * Create an instance of {@link ValueTranslationClaim }
     * 
     */
    public ValueTranslationClaim createValueTranslationClaim() {
        return new ValueTranslationClaim();
    }

    /**
     * Create an instance of {@link VarcharAttribute }
     * 
     */
    public VarcharAttribute createVarcharAttribute() {
        return new VarcharAttribute();
    }

    /**
     * Create an instance of {@link MessageDetail }
     * 
     */
    public MessageDetail createMessageDetail() {
        return new MessageDetail();
    }

    /**
     * Create an instance of {@link KeyPairCollection }
     * 
     */
    public KeyPairCollection createKeyPairCollection() {
        return new KeyPairCollection();
    }

    /**
     * Create an instance of {@link Email }
     * 
     */
    public Email createEmail() {
        return new Email();
    }

    /**
     * Create an instance of {@link TranslatedCodeCollection }
     * 
     */
    public TranslatedCodeCollection createTranslatedCodeCollection() {
        return new TranslatedCodeCollection();
    }

    /**
     * Create an instance of {@link Flexfield }
     * 
     */
    public Flexfield createFlexfield() {
        return new Flexfield();
    }

    /**
     * Create an instance of {@link DateAttribute }
     * 
     */
    public DateAttribute createDateAttribute() {
        return new DateAttribute();
    }

    /**
     * Create an instance of {@link NumberCollection }
     * 
     */
    public NumberCollection createNumberCollection() {
        return new NumberCollection();
    }

    /**
     * Create an instance of {@link Value }
     * 
     */
    public Value createValue() {
        return new Value();
    }

    /**
     * Create an instance of {@link SiteSpecifics }
     * 
     */
    public SiteSpecifics createSiteSpecifics() {
        return new SiteSpecifics();
    }

    /**
     * Create an instance of {@link ValueTranslationPolicyCollection }
     * 
     */
    public ValueTranslationPolicyCollection createValueTranslationPolicyCollection() {
        return new ValueTranslationPolicyCollection();
    }

    /**
     * Create an instance of {@link ValueTranslation }
     * 
     */
    public ValueTranslation createValueTranslation() {
        return new ValueTranslation();
    }

    /**
     * Create an instance of {@link MessageCollection }
     * 
     */
    public MessageCollection createMessageCollection() {
        return new MessageCollection();
    }

    /**
     * Create an instance of {@link Number }
     * 
     */
    public Number createNumber() {
        return new Number();
    }

    /**
     * Create an instance of {@link CountrySpecifics }
     * 
     */
    public CountrySpecifics createCountrySpecifics() {
        return new CountrySpecifics();
    }

    /**
     * Create an instance of {@link EntityAttributeCollection }
     * 
     */
    public EntityAttributeCollection createEntityAttributeCollection() {
        return new EntityAttributeCollection();
    }

    /**
     * Create an instance of {@link ValueTranslationClaimCollection }
     * 
     */
    public ValueTranslationClaimCollection createValueTranslationClaimCollection() {
        return new ValueTranslationClaimCollection();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "affinityNo", scope = AffinityRelation.class)
    public JAXBElement<Long> createAffinityRelationAffinityNo(Long value) {
        return new JAXBElement<Long>(_AffinityRelationAffinityNo_QNAME, Long.class, AffinityRelation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "ownerNameIdNo", scope = AffinityRelation.class)
    public JAXBElement<Long> createAffinityRelationOwnerNameIdNo(Long value) {
        return new JAXBElement<Long>(_AffinityRelationOwnerNameIdNo_QNAME, Long.class, AffinityRelation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "companyVatNo", scope = Institution2 .class)
    public JAXBElement<String> createInstitution2CompanyVatNo(String value) {
        return new JAXBElement<String>(_Institution2CompanyVatNo_QNAME, String.class, Institution2 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "contactPerson", scope = Institution2 .class)
    public JAXBElement<String> createInstitution2ContactPerson(String value) {
        return new JAXBElement<String>(_Institution2ContactPerson_QNAME, String.class, Institution2 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "companyRegNo", scope = Institution2 .class)
    public JAXBElement<String> createInstitution2CompanyRegNo(String value) {
        return new JAXBElement<String>(_Institution2CompanyRegNo_QNAME, String.class, Institution2 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "bankName", scope = SearchTokenBan.class)
    public JAXBElement<String> createSearchTokenBanBankName(String value) {
        return new JAXBElement<String>(_SearchTokenBanBankName_QNAME, String.class, SearchTokenBan.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "bankCode", scope = SearchTokenBan.class)
    public JAXBElement<String> createSearchTokenBanBankCode(String value) {
        return new JAXBElement<String>(_SearchTokenBanBankCode_QNAME, String.class, SearchTokenBan.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "companyVatNo", scope = PartyOther2 .class)
    public JAXBElement<String> createPartyOther2CompanyVatNo(String value) {
        return new JAXBElement<String>(_Institution2CompanyVatNo_QNAME, String.class, PartyOther2 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "civilReg", scope = PartyOther2 .class)
    public JAXBElement<String> createPartyOther2CivilReg(String value) {
        return new JAXBElement<String>(_PartyOther2CivilReg_QNAME, String.class, PartyOther2 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "contactPerson", scope = PartyOther2 .class)
    public JAXBElement<String> createPartyOther2ContactPerson(String value) {
        return new JAXBElement<String>(_Institution2ContactPerson_QNAME, String.class, PartyOther2 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "title", scope = PartyOther2 .class)
    public JAXBElement<String> createPartyOther2Title(String value) {
        return new JAXBElement<String>(_PartyOther2Title_QNAME, String.class, PartyOther2 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "birthDate", scope = PartyOther2 .class)
    public JAXBElement<XMLGregorianCalendar> createPartyOther2BirthDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_PartyOther2BirthDate_QNAME, XMLGregorianCalendar.class, PartyOther2 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "forename", scope = PartyOther2 .class)
    public JAXBElement<String> createPartyOther2Forename(String value) {
        return new JAXBElement<String>(_PartyOther2Forename_QNAME, String.class, PartyOther2 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "companyRegNo", scope = PartyOther2 .class)
    public JAXBElement<String> createPartyOther2CompanyRegNo(String value) {
        return new JAXBElement<String>(_Institution2CompanyRegNo_QNAME, String.class, PartyOther2 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "claimEventNo", scope = ClaimEventRelation.class)
    public JAXBElement<Long> createClaimEventRelationClaimEventNo(Long value) {
        return new JAXBElement<Long>(_ClaimEventRelationClaimEventNo_QNAME, Long.class, ClaimEventRelation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "memberNameIdNo", scope = ClaimEventRelation.class)
    public JAXBElement<Long> createClaimEventRelationMemberNameIdNo(Long value) {
        return new JAXBElement<Long>(_ClaimEventRelationMemberNameIdNo_QNAME, Long.class, ClaimEventRelation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v3/", name = "addressCode", scope = SearchTokenLoc.class)
    public JAXBElement<String> createSearchTokenLocAddressCode(String value) {
        return new JAXBElement<String>(_SearchTokenLocAddressCode_QNAME, String.class, SearchTokenLoc.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v3/", name = "siteShortName", scope = SearchTokenLoc.class)
    public JAXBElement<String> createSearchTokenLocSiteShortName(String value) {
        return new JAXBElement<String>(_SearchTokenLocSiteShortName_QNAME, String.class, SearchTokenLoc.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v3/", name = "searchMethod", scope = SearchTokenLoc.class)
    public JAXBElement<String> createSearchTokenLocSearchMethod(String value) {
        return new JAXBElement<String>(_SearchTokenLocSearchMethod_QNAME, String.class, SearchTokenLoc.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SiteSpecifics }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/common/v2/", name = "siteSpecifics", scope = TIAObject.class)
    public JAXBElement<SiteSpecifics> createTIAObjectSiteSpecifics(SiteSpecifics value) {
        return new JAXBElement<SiteSpecifics>(_TIAObjectSiteSpecifics_QNAME, SiteSpecifics.class, TIAObject.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/common/v2/", name = "externalId", scope = TIAObject.class)
    public JAXBElement<String> createTIAObjectExternalId(String value) {
        return new JAXBElement<String>(_TIAObjectExternalId_QNAME, String.class, TIAObject.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CountrySpecifics }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/common/v2/", name = "countrySpecifics", scope = TIAObject.class)
    public JAXBElement<CountrySpecifics> createTIAObjectCountrySpecifics(CountrySpecifics value) {
        return new JAXBElement<CountrySpecifics>(_TIAObjectCountrySpecifics_QNAME, CountrySpecifics.class, TIAObject.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FlexfieldCollection }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/common/v2/", name = "flexfieldCollection", scope = TIAObject.class)
    public JAXBElement<FlexfieldCollection> createTIAObjectFlexfieldCollection(FlexfieldCollection value) {
        return new JAXBElement<FlexfieldCollection>(_TIAObjectFlexfieldCollection_QNAME, FlexfieldCollection.class, TIAObject.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VersionToken }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/common/v2/", name = "versionToken", scope = TIAObject.class)
    public JAXBElement<VersionToken> createTIAObjectVersionToken(VersionToken value) {
        return new JAXBElement<VersionToken>(_TIAObjectVersionToken_QNAME, VersionToken.class, TIAObject.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "description", scope = Relation.class)
    public JAXBElement<String> createRelationDescription(String value) {
        return new JAXBElement<String>(_RelationDescription_QNAME, String.class, Relation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "startDate", scope = Relation.class)
    public JAXBElement<XMLGregorianCalendar> createRelationStartDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_RelationStartDate_QNAME, XMLGregorianCalendar.class, Relation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "newest", scope = Relation.class)
    public JAXBElement<String> createRelationNewest(String value) {
        return new JAXBElement<String>(_RelationNewest_QNAME, String.class, Relation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "relationType", scope = Relation.class)
    public JAXBElement<String> createRelationRelationType(String value) {
        return new JAXBElement<String>(_RelationRelationType_QNAME, String.class, Relation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "ownerDesc", scope = Relation.class)
    public JAXBElement<String> createRelationOwnerDesc(String value) {
        return new JAXBElement<String>(_RelationOwnerDesc_QNAME, String.class, Relation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "transId", scope = Relation.class)
    public JAXBElement<Long> createRelationTransId(Long value) {
        return new JAXBElement<Long>(_RelationTransId_QNAME, Long.class, Relation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "seqNo", scope = Relation.class)
    public JAXBElement<Long> createRelationSeqNo(Long value) {
        return new JAXBElement<Long>(_RelationSeqNo_QNAME, Long.class, Relation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "endDate", scope = Relation.class)
    public JAXBElement<XMLGregorianCalendar> createRelationEndDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_RelationEndDate_QNAME, XMLGregorianCalendar.class, Relation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "thirdPartyNo", scope = ClaimThirdPartyRelation.class)
    public JAXBElement<Long> createClaimThirdPartyRelationThirdPartyNo(Long value) {
        return new JAXBElement<Long>(_ClaimThirdPartyRelationThirdPartyNo_QNAME, Long.class, ClaimThirdPartyRelation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "memberNameIdNo", scope = ClaimThirdPartyRelation.class)
    public JAXBElement<Long> createClaimThirdPartyRelationMemberNameIdNo(Long value) {
        return new JAXBElement<Long>(_ClaimEventRelationMemberNameIdNo_QNAME, Long.class, ClaimThirdPartyRelation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "spouseName", scope = Customer2 .class)
    public JAXBElement<String> createCustomer2SpouseName(String value) {
        return new JAXBElement<String>(_Customer2SpouseName_QNAME, String.class, Customer2 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "profileCode", scope = Customer2 .class)
    public JAXBElement<String> createCustomer2ProfileCode(String value) {
        return new JAXBElement<String>(_Customer2ProfileCode_QNAME, String.class, Customer2 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "spouseBirthDate", scope = Customer2 .class)
    public JAXBElement<XMLGregorianCalendar> createCustomer2SpouseBirthDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_Customer2SpouseBirthDate_QNAME, XMLGregorianCalendar.class, Customer2 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "household", scope = Customer2 .class)
    public JAXBElement<Integer> createCustomer2Household(Integer value) {
        return new JAXBElement<Integer>(_Customer2Household_QNAME, Integer.class, Customer2 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "statusCode", scope = Customer2 .class)
    public JAXBElement<String> createCustomer2StatusCode(String value) {
        return new JAXBElement<String>(_Customer2StatusCode_QNAME, String.class, Customer2 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "potential", scope = Customer2 .class)
    public JAXBElement<String> createCustomer2Potential(String value) {
        return new JAXBElement<String>(_Customer2Potential_QNAME, String.class, Customer2 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "occupationCode", scope = Customer2 .class)
    public JAXBElement<String> createCustomer2OccupationCode(String value) {
        return new JAXBElement<String>(_Customer2OccupationCode_QNAME, String.class, Customer2 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "maritalState", scope = Customer2 .class)
    public JAXBElement<Integer> createCustomer2MaritalState(Integer value) {
        return new JAXBElement<Integer>(_Customer2MaritalState_QNAME, Integer.class, Customer2 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "children", scope = Customer2 .class)
    public JAXBElement<Integer> createCustomer2Children(Integer value) {
        return new JAXBElement<Integer>(_Customer2Children_QNAME, Integer.class, Customer2 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "reminderGroup", scope = Customer2 .class)
    public JAXBElement<String> createCustomer2ReminderGroup(String value) {
        return new JAXBElement<String>(_Customer2ReminderGroup_QNAME, String.class, Customer2 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "howToContact", scope = Customer2 .class)
    public JAXBElement<Integer> createCustomer2HowToContact(Integer value) {
        return new JAXBElement<Integer>(_Customer2HowToContact_QNAME, Integer.class, Customer2 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "lastContactWay", scope = Customer2 .class)
    public JAXBElement<Integer> createCustomer2LastContactWay(Integer value) {
        return new JAXBElement<Integer>(_Customer2LastContactWay_QNAME, Integer.class, Customer2 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "spouseSex", scope = Customer2 .class)
    public JAXBElement<String> createCustomer2SpouseSex(String value) {
        return new JAXBElement<String>(_Customer2SpouseSex_QNAME, String.class, Customer2 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "howNotToContact", scope = Customer2 .class)
    public JAXBElement<Integer> createCustomer2HowNotToContact(Integer value) {
        return new JAXBElement<Integer>(_Customer2HowNotToContact_QNAME, Integer.class, Customer2 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "firstContact", scope = Customer2 .class)
    public JAXBElement<XMLGregorianCalendar> createCustomer2FirstContact(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_Customer2FirstContact_QNAME, XMLGregorianCalendar.class, Customer2 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "lastContact", scope = Customer2 .class)
    public JAXBElement<XMLGregorianCalendar> createCustomer2LastContact(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_Customer2LastContact_QNAME, XMLGregorianCalendar.class, Customer2 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "incomeGroup", scope = Customer2 .class)
    public JAXBElement<String> createCustomer2IncomeGroup(String value) {
        return new JAXBElement<String>(_Customer2IncomeGroup_QNAME, String.class, Customer2 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "education", scope = Customer2 .class)
    public JAXBElement<String> createCustomer2Education(String value) {
        return new JAXBElement<String>(_Customer2Education_QNAME, String.class, Customer2 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "occupation", scope = Customer2 .class)
    public JAXBElement<String> createCustomer2Occupation(String value) {
        return new JAXBElement<String>(_Customer2Occupation_QNAME, String.class, Customer2 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "yourRef", scope = Customer2 .class)
    public JAXBElement<String> createCustomer2YourRef(String value) {
        return new JAXBElement<String>(_Customer2YourRef_QNAME, String.class, Customer2 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "sex", scope = Customer2 .class)
    public JAXBElement<String> createCustomer2Sex(String value) {
        return new JAXBElement<String>(_Customer2Sex_QNAME, String.class, Customer2 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "serviceCode", scope = Customer2 .class)
    public JAXBElement<String> createCustomer2ServiceCode(String value) {
        return new JAXBElement<String>(_Customer2ServiceCode_QNAME, String.class, Customer2 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "locked", scope = Bank.class)
    public JAXBElement<String> createBankLocked(String value) {
        return new JAXBElement<String>(_BankLocked_QNAME, String.class, Bank.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "bankIdNo", scope = Bank.class)
    public JAXBElement<Long> createBankBankIdNo(Long value) {
        return new JAXBElement<Long>(_BankBankIdNo_QNAME, Long.class, Bank.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "bankCodeType", scope = Bank.class)
    public JAXBElement<String> createBankBankCodeType(String value) {
        return new JAXBElement<String>(_BankBankCodeType_QNAME, String.class, Bank.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "notes", scope = Bank.class)
    public JAXBElement<String> createBankNotes(String value) {
        return new JAXBElement<String>(_BankNotes_QNAME, String.class, Bank.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "bankCode", scope = Bank.class)
    public JAXBElement<String> createBankBankCode(String value) {
        return new JAXBElement<String>(_SearchTokenBanBankCode_QNAME, String.class, Bank.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "language", scope = AbstractParty2 .class)
    public JAXBElement<String> createAbstractParty2Language(String value) {
        return new JAXBElement<String>(_AbstractParty2Language_QNAME, String.class, AbstractParty2 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "name", scope = AbstractParty2 .class)
    public JAXBElement<String> createAbstractParty2Name(String value) {
        return new JAXBElement<String>(_AbstractParty2Name_QNAME, String.class, AbstractParty2 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Customer2 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "customerInfo", scope = AbstractParty2 .class)
    public JAXBElement<Customer2> createAbstractParty2CustomerInfo(Customer2 value) {
        return new JAXBElement<Customer2>(_AbstractParty2CustomerInfo_QNAME, Customer2 .class, AbstractParty2 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "idNoAlt", scope = AbstractParty2 .class)
    public JAXBElement<String> createAbstractParty2IdNoAlt(String value) {
        return new JAXBElement<String>(_AbstractParty2IdNoAlt_QNAME, String.class, AbstractParty2 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "idNo", scope = AbstractParty2 .class)
    public JAXBElement<Long> createAbstractParty2IdNo(Long value) {
        return new JAXBElement<Long>(_AbstractParty2IdNo_QNAME, Long.class, AbstractParty2 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "currencyCode", scope = AbstractParty2 .class)
    public JAXBElement<String> createAbstractParty2CurrencyCode(String value) {
        return new JAXBElement<String>(_AbstractParty2CurrencyCode_QNAME, String.class, AbstractParty2 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "nameSort", scope = AbstractParty2 .class)
    public JAXBElement<String> createAbstractParty2NameSort(String value) {
        return new JAXBElement<String>(_AbstractParty2NameSort_QNAME, String.class, AbstractParty2 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "idNoSort", scope = AbstractParty2 .class)
    public JAXBElement<String> createAbstractParty2IdNoSort(String value) {
        return new JAXBElement<String>(_AbstractParty2IdNoSort_QNAME, String.class, AbstractParty2 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "nameType", scope = AbstractParty2 .class)
    public JAXBElement<Integer> createAbstractParty2NameType(Integer value) {
        return new JAXBElement<Integer>(_AbstractParty2NameType_QNAME, Integer.class, AbstractParty2 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "surname", scope = AbstractParty2 .class)
    public JAXBElement<String> createAbstractParty2Surname(String value) {
        return new JAXBElement<String>(_AbstractParty2Surname_QNAME, String.class, AbstractParty2 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "obsoleteCode", scope = AbstractParty2 .class)
    public JAXBElement<Integer> createAbstractParty2ObsoleteCode(Integer value) {
        return new JAXBElement<Integer>(_AbstractParty2ObsoleteCode_QNAME, Integer.class, AbstractParty2 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v3/", name = "advertising", scope = Customer.class)
    public JAXBElement<String> createCustomerAdvertising(String value) {
        return new JAXBElement<String>(_CustomerAdvertising_QNAME, String.class, Customer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v3/", name = "advertisingDate", scope = Customer.class)
    public JAXBElement<XMLGregorianCalendar> createCustomerAdvertisingDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_CustomerAdvertisingDate_QNAME, XMLGregorianCalendar.class, Customer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v3/", name = "nextContactWay", scope = Customer.class)
    public JAXBElement<String> createCustomerNextContactWay(String value) {
        return new JAXBElement<String>(_CustomerNextContactWay_QNAME, String.class, Customer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v3/", name = "handler", scope = Customer.class)
    public JAXBElement<String> createCustomerHandler(String value) {
        return new JAXBElement<String>(_CustomerHandler_QNAME, String.class, Customer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v3/", name = "nextContact", scope = Customer.class)
    public JAXBElement<XMLGregorianCalendar> createCustomerNextContact(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_CustomerNextContact_QNAME, XMLGregorianCalendar.class, Customer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v3/", name = "customerGroup", scope = Customer.class)
    public JAXBElement<String> createCustomerCustomerGroup(String value) {
        return new JAXBElement<String>(_CustomerCustomerGroup_QNAME, String.class, Customer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v3/", name = "customerSegment", scope = Customer.class)
    public JAXBElement<String> createCustomerCustomerSegment(String value) {
        return new JAXBElement<String>(_CustomerCustomerSegment_QNAME, String.class, Customer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v3/", name = "agentNo", scope = Customer.class)
    public JAXBElement<Long> createCustomerAgentNo(Long value) {
        return new JAXBElement<Long>(_CustomerAgentNo_QNAME, Long.class, Customer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "alias", scope = SearchTokenSsu.class)
    public JAXBElement<String> createSearchTokenSsuAlias(String value) {
        return new JAXBElement<String>(_SearchTokenSsuAlias_QNAME, String.class, SearchTokenSsu.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "serviceSupplierName", scope = SearchTokenSsu.class)
    public JAXBElement<String> createSearchTokenSsuServiceSupplierName(String value) {
        return new JAXBElement<String>(_SearchTokenSsuServiceSupplierName_QNAME, String.class, SearchTokenSsu.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "memberNameIdNo", scope = AccountRelation.class)
    public JAXBElement<Long> createAccountRelationMemberNameIdNo(Long value) {
        return new JAXBElement<Long>(_ClaimEventRelationMemberNameIdNo_QNAME, Long.class, AccountRelation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "accountNo", scope = AccountRelation.class)
    public JAXBElement<Long> createAccountRelationAccountNo(Long value) {
        return new JAXBElement<Long>(_AccountRelationAccountNo_QNAME, Long.class, AccountRelation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v3/", name = "contactPerson", scope = Institution.class)
    public JAXBElement<String> createInstitutionContactPerson(String value) {
        return new JAXBElement<String>(_InstitutionContactPerson_QNAME, String.class, Institution.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v3/", name = "companyVatNo", scope = Institution.class)
    public JAXBElement<String> createInstitutionCompanyVatNo(String value) {
        return new JAXBElement<String>(_InstitutionCompanyVatNo_QNAME, String.class, Institution.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v3/", name = "industryCode", scope = Institution.class)
    public JAXBElement<String> createInstitutionIndustryCode(String value) {
        return new JAXBElement<String>(_InstitutionIndustryCode_QNAME, String.class, Institution.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v3/", name = "employees", scope = Institution.class)
    public JAXBElement<Long> createInstitutionEmployees(Long value) {
        return new JAXBElement<Long>(_InstitutionEmployees_QNAME, Long.class, Institution.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v3/", name = "companyRegNo", scope = Institution.class)
    public JAXBElement<String> createInstitutionCompanyRegNo(String value) {
        return new JAXBElement<String>(_InstitutionCompanyRegNo_QNAME, String.class, Institution.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v3/", name = "institutionCode", scope = Institution.class)
    public JAXBElement<String> createInstitutionInstitutionCode(String value) {
        return new JAXBElement<String>(_InstitutionInstitutionCode_QNAME, String.class, Institution.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/common/v2/", name = "emailBCC", scope = Email.class)
    public JAXBElement<String> createEmailEmailBCC(String value) {
        return new JAXBElement<String>(_EmailEmailBCC_QNAME, String.class, Email.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/common/v2/", name = "emailCC", scope = Email.class)
    public JAXBElement<String> createEmailEmailCC(String value) {
        return new JAXBElement<String>(_EmailEmailCC_QNAME, String.class, Email.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v3/", name = "addressCode", scope = SearchTokenPar.class)
    public JAXBElement<String> createSearchTokenParAddressCode(String value) {
        return new JAXBElement<String>(_SearchTokenLocAddressCode_QNAME, String.class, SearchTokenPar.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v3/", name = "handler", scope = SearchTokenPar.class)
    public JAXBElement<String> createSearchTokenParHandler(String value) {
        return new JAXBElement<String>(_CustomerHandler_QNAME, String.class, SearchTokenPar.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v3/", name = "siteShortName", scope = SearchTokenPar.class)
    public JAXBElement<String> createSearchTokenParSiteShortName(String value) {
        return new JAXBElement<String>(_SearchTokenLocSiteShortName_QNAME, String.class, SearchTokenPar.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v3/", name = "searchMethod", scope = SearchTokenPar.class)
    public JAXBElement<String> createSearchTokenParSearchMethod(String value) {
        return new JAXBElement<String>(_SearchTokenLocSearchMethod_QNAME, String.class, SearchTokenPar.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v3/", name = "agentNo", scope = SearchTokenPar.class)
    public JAXBElement<Long> createSearchTokenParAgentNo(Long value) {
        return new JAXBElement<Long>(_CustomerAgentNo_QNAME, Long.class, SearchTokenPar.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "insuranceCompanyName", scope = InsuranceCompany.class)
    public JAXBElement<String> createInsuranceCompanyInsuranceCompanyName(String value) {
        return new JAXBElement<String>(_InsuranceCompanyInsuranceCompanyName_QNAME, String.class, InsuranceCompany.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "insurerCode", scope = InsuranceCompany.class)
    public JAXBElement<String> createInsuranceCompanyInsurerCode(String value) {
        return new JAXBElement<String>(_InsuranceCompanyInsurerCode_QNAME, String.class, InsuranceCompany.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "parentInsurerCode", scope = InsuranceCompany.class)
    public JAXBElement<String> createInsuranceCompanyParentInsurerCode(String value) {
        return new JAXBElement<String>(_InsuranceCompanyParentInsurerCode_QNAME, String.class, InsuranceCompany.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "periodOfCancellation", scope = InsuranceCompany.class)
    public JAXBElement<Long> createInsuranceCompanyPeriodOfCancellation(Long value) {
        return new JAXBElement<Long>(_InsuranceCompanyPeriodOfCancellation_QNAME, Long.class, InsuranceCompany.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PartyOther }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v3/", name = "partyOther", scope = PartyElement.class)
    public JAXBElement<PartyOther> createPartyElementPartyOther(PartyOther value) {
        return new JAXBElement<PartyOther>(_PartyElementPartyOther_QNAME, PartyOther.class, PartyElement.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Institution }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v3/", name = "institution", scope = PartyElement.class)
    public JAXBElement<Institution> createPartyElementInstitution(Institution value) {
        return new JAXBElement<Institution>(_PartyElementInstitution_QNAME, Institution.class, PartyElement.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Person }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v3/", name = "person", scope = PartyElement.class)
    public JAXBElement<Person> createPartyElementPerson(Person value) {
        return new JAXBElement<Person>(_PartyElementPerson_QNAME, Person.class, PartyElement.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "memberNameIdNo", scope = PolicyObjectRelation.class)
    public JAXBElement<Long> createPolicyObjectRelationMemberNameIdNo(Long value) {
        return new JAXBElement<Long>(_ClaimEventRelationMemberNameIdNo_QNAME, Long.class, PolicyObjectRelation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "policyObjectNo", scope = PolicyObjectRelation.class)
    public JAXBElement<Long> createPolicyObjectRelationPolicyObjectNo(Long value) {
        return new JAXBElement<Long>(_PolicyObjectRelationPolicyObjectNo_QNAME, Long.class, PolicyObjectRelation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "title", scope = Person2 .class)
    public JAXBElement<String> createPerson2Title(String value) {
        return new JAXBElement<String>(_PartyOther2Title_QNAME, String.class, Person2 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "birthDate", scope = Person2 .class)
    public JAXBElement<XMLGregorianCalendar> createPerson2BirthDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_PartyOther2BirthDate_QNAME, XMLGregorianCalendar.class, Person2 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "civilRegCode", scope = Person2 .class)
    public JAXBElement<String> createPerson2CivilRegCode(String value) {
        return new JAXBElement<String>(_Person2CivilRegCode_QNAME, String.class, Person2 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "forename", scope = Person2 .class)
    public JAXBElement<String> createPerson2Forename(String value) {
        return new JAXBElement<String>(_PartyOther2Forename_QNAME, String.class, Person2 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "subclaimNo", scope = SubclaimRelation.class)
    public JAXBElement<Long> createSubclaimRelationSubclaimNo(Long value) {
        return new JAXBElement<Long>(_SubclaimRelationSubclaimNo_QNAME, Long.class, SubclaimRelation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "memberNameIdNo", scope = SubclaimRelation.class)
    public JAXBElement<Long> createSubclaimRelationMemberNameIdNo(Long value) {
        return new JAXBElement<Long>(_ClaimEventRelationMemberNameIdNo_QNAME, Long.class, SubclaimRelation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v3/", name = "language", scope = Location.class)
    public JAXBElement<String> createLocationLanguage(String value) {
        return new JAXBElement<String>(_LocationLanguage_QNAME, String.class, Location.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v3/", name = "name", scope = Location.class)
    public JAXBElement<String> createLocationName(String value) {
        return new JAXBElement<String>(_LocationName_QNAME, String.class, Location.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v3/", name = "siteSeqNo", scope = Location.class)
    public JAXBElement<Long> createLocationSiteSeqNo(Long value) {
        return new JAXBElement<Long>(_LocationSiteSeqNo_QNAME, Long.class, Location.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v3/", name = "idNo", scope = Location.class)
    public JAXBElement<Long> createLocationIdNo(Long value) {
        return new JAXBElement<Long>(_LocationIdNo_QNAME, Long.class, Location.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v3/", name = "currencyCode", scope = Location.class)
    public JAXBElement<String> createLocationCurrencyCode(String value) {
        return new JAXBElement<String>(_LocationCurrencyCode_QNAME, String.class, Location.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v3/", name = "nameSort", scope = Location.class)
    public JAXBElement<String> createLocationNameSort(String value) {
        return new JAXBElement<String>(_LocationNameSort_QNAME, String.class, Location.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v3/", name = "idNoAlt", scope = Location.class)
    public JAXBElement<String> createLocationIdNoAlt(String value) {
        return new JAXBElement<String>(_LocationIdNoAlt_QNAME, String.class, Location.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v3/", name = "idNoSort", scope = Location.class)
    public JAXBElement<String> createLocationIdNoSort(String value) {
        return new JAXBElement<String>(_LocationIdNoSort_QNAME, String.class, Location.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v3/", name = "siteShortName", scope = Location.class)
    public JAXBElement<String> createLocationSiteShortName(String value) {
        return new JAXBElement<String>(_SearchTokenLocSiteShortName_QNAME, String.class, Location.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v3/", name = "surname", scope = Location.class)
    public JAXBElement<String> createLocationSurname(String value) {
        return new JAXBElement<String>(_LocationSurname_QNAME, String.class, Location.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v3/", name = "obsoleteCode", scope = Location.class)
    public JAXBElement<Long> createLocationObsoleteCode(Long value) {
        return new JAXBElement<Long>(_LocationObsoleteCode_QNAME, Long.class, Location.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "memberNameIdNo", scope = PolicyLineRelation.class)
    public JAXBElement<Long> createPolicyLineRelationMemberNameIdNo(Long value) {
        return new JAXBElement<Long>(_ClaimEventRelationMemberNameIdNo_QNAME, Long.class, PolicyLineRelation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "policyLineNo", scope = PolicyLineRelation.class)
    public JAXBElement<Long> createPolicyLineRelationPolicyLineNo(Long value) {
        return new JAXBElement<Long>(_PolicyLineRelationPolicyLineNo_QNAME, Long.class, PolicyLineRelation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "insuranceCompanyName", scope = SearchTokenInc.class)
    public JAXBElement<String> createSearchTokenIncInsuranceCompanyName(String value) {
        return new JAXBElement<String>(_InsuranceCompanyInsuranceCompanyName_QNAME, String.class, SearchTokenInc.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "insurerCode", scope = SearchTokenInc.class)
    public JAXBElement<String> createSearchTokenIncInsurerCode(String value) {
        return new JAXBElement<String>(_InsuranceCompanyInsurerCode_QNAME, String.class, SearchTokenInc.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "memberNameIdNo", scope = ClaimRelation.class)
    public JAXBElement<Long> createClaimRelationMemberNameIdNo(Long value) {
        return new JAXBElement<Long>(_ClaimEventRelationMemberNameIdNo_QNAME, Long.class, ClaimRelation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "claimNo", scope = ClaimRelation.class)
    public JAXBElement<Long> createClaimRelationClaimNo(Long value) {
        return new JAXBElement<Long>(_ClaimRelationClaimNo_QNAME, Long.class, ClaimRelation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/common/v2/", name = "correlationId", scope = InputToken.class)
    public JAXBElement<String> createInputTokenCorrelationId(String value) {
        return new JAXBElement<String>(_InputTokenCorrelationId_QNAME, String.class, InputToken.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "language", scope = Location2 .class)
    public JAXBElement<String> createLocation2Language(String value) {
        return new JAXBElement<String>(_AbstractParty2Language_QNAME, String.class, Location2 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "name", scope = Location2 .class)
    public JAXBElement<String> createLocation2Name(String value) {
        return new JAXBElement<String>(_AbstractParty2Name_QNAME, String.class, Location2 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "idNoAlt", scope = Location2 .class)
    public JAXBElement<String> createLocation2IdNoAlt(String value) {
        return new JAXBElement<String>(_AbstractParty2IdNoAlt_QNAME, String.class, Location2 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "idNo", scope = Location2 .class)
    public JAXBElement<Long> createLocation2IdNo(Long value) {
        return new JAXBElement<Long>(_AbstractParty2IdNo_QNAME, Long.class, Location2 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "currencyCode", scope = Location2 .class)
    public JAXBElement<String> createLocation2CurrencyCode(String value) {
        return new JAXBElement<String>(_AbstractParty2CurrencyCode_QNAME, String.class, Location2 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "nameSort", scope = Location2 .class)
    public JAXBElement<String> createLocation2NameSort(String value) {
        return new JAXBElement<String>(_AbstractParty2NameSort_QNAME, String.class, Location2 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "idNoSort", scope = Location2 .class)
    public JAXBElement<String> createLocation2IdNoSort(String value) {
        return new JAXBElement<String>(_AbstractParty2IdNoSort_QNAME, String.class, Location2 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "surname", scope = Location2 .class)
    public JAXBElement<String> createLocation2Surname(String value) {
        return new JAXBElement<String>(_AbstractParty2Surname_QNAME, String.class, Location2 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "obsoleteCode", scope = Location2 .class)
    public JAXBElement<Long> createLocation2ObsoleteCode(Long value) {
        return new JAXBElement<Long>(_AbstractParty2ObsoleteCode_QNAME, Long.class, Location2 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "postArea", scope = Address2 .class)
    public JAXBElement<String> createAddress2PostArea(String value) {
        return new JAXBElement<String>(_Address2PostArea_QNAME, String.class, Address2 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "floor", scope = Address2 .class)
    public JAXBElement<String> createAddress2Floor(String value) {
        return new JAXBElement<String>(_Address2Floor_QNAME, String.class, Address2 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "postalRegion", scope = Address2 .class)
    public JAXBElement<String> createAddress2PostalRegion(String value) {
        return new JAXBElement<String>(_Address2PostalRegion_QNAME, String.class, Address2 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "street", scope = Address2 .class)
    public JAXBElement<String> createAddress2Street(String value) {
        return new JAXBElement<String>(_Address2Street_QNAME, String.class, Address2 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "countryCode", scope = Address2 .class)
    public JAXBElement<String> createAddress2CountryCode(String value) {
        return new JAXBElement<String>(_Address2CountryCode_QNAME, String.class, Address2 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "county", scope = Address2 .class)
    public JAXBElement<String> createAddress2County(String value) {
        return new JAXBElement<String>(_Address2County_QNAME, String.class, Address2 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "houseCoName", scope = Address2 .class)
    public JAXBElement<String> createAddress2HouseCoName(String value) {
        return new JAXBElement<String>(_Address2HouseCoName_QNAME, String.class, Address2 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "postStreet", scope = Address2 .class)
    public JAXBElement<String> createAddress2PostStreet(String value) {
        return new JAXBElement<String>(_Address2PostStreet_QNAME, String.class, Address2 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "country", scope = Address2 .class)
    public JAXBElement<String> createAddress2Country(String value) {
        return new JAXBElement<String>(_Address2Country_QNAME, String.class, Address2 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "addressType", scope = Address2 .class)
    public JAXBElement<String> createAddress2AddressType(String value) {
        return new JAXBElement<String>(_Address2AddressType_QNAME, String.class, Address2 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "floorExt", scope = Address2 .class)
    public JAXBElement<String> createAddress2FloorExt(String value) {
        return new JAXBElement<String>(_Address2FloorExt_QNAME, String.class, Address2 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "streetNo", scope = Address2 .class)
    public JAXBElement<String> createAddress2StreetNo(String value) {
        return new JAXBElement<String>(_Address2StreetNo_QNAME, String.class, Address2 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "city", scope = Address2 .class)
    public JAXBElement<String> createAddress2City(String value) {
        return new JAXBElement<String>(_Address2City_QNAME, String.class, Address2 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "comGroup", scope = SearchTokenAgt.class)
    public JAXBElement<String> createSearchTokenAgtComGroup(String value) {
        return new JAXBElement<String>(_SearchTokenAgtComGroup_QNAME, String.class, SearchTokenAgt.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "agentName", scope = SearchTokenAgt.class)
    public JAXBElement<String> createSearchTokenAgtAgentName(String value) {
        return new JAXBElement<String>(_SearchTokenAgtAgentName_QNAME, String.class, SearchTokenAgt.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "comCategory", scope = SearchTokenAgt.class)
    public JAXBElement<String> createSearchTokenAgtComCategory(String value) {
        return new JAXBElement<String>(_SearchTokenAgtComCategory_QNAME, String.class, SearchTokenAgt.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "contactInfoDetail", scope = ContactInfo.class)
    public JAXBElement<String> createContactInfoContactInfoDetail(String value) {
        return new JAXBElement<String>(_ContactInfoContactInfoDetail_QNAME, String.class, ContactInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "contactInfoType", scope = ContactInfo.class)
    public JAXBElement<String> createContactInfoContactInfoType(String value) {
        return new JAXBElement<String>(_ContactInfoContactInfoType_QNAME, String.class, ContactInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v3/", name = "middleName", scope = PartyOther.class)
    public JAXBElement<String> createPartyOtherMiddleName(String value) {
        return new JAXBElement<String>(_PartyOtherMiddleName_QNAME, String.class, PartyOther.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v3/", name = "civilReg", scope = PartyOther.class)
    public JAXBElement<String> createPartyOtherCivilReg(String value) {
        return new JAXBElement<String>(_PartyOtherCivilReg_QNAME, String.class, PartyOther.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v3/", name = "companyVatNo", scope = PartyOther.class)
    public JAXBElement<String> createPartyOtherCompanyVatNo(String value) {
        return new JAXBElement<String>(_InstitutionCompanyVatNo_QNAME, String.class, PartyOther.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v3/", name = "hiddenIdentity", scope = PartyOther.class)
    public JAXBElement<String> createPartyOtherHiddenIdentity(String value) {
        return new JAXBElement<String>(_PartyOtherHiddenIdentity_QNAME, String.class, PartyOther.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v3/", name = "deceasedDate", scope = PartyOther.class)
    public JAXBElement<XMLGregorianCalendar> createPartyOtherDeceasedDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_PartyOtherDeceasedDate_QNAME, XMLGregorianCalendar.class, PartyOther.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v3/", name = "employees", scope = PartyOther.class)
    public JAXBElement<Long> createPartyOtherEmployees(Long value) {
        return new JAXBElement<Long>(_InstitutionEmployees_QNAME, Long.class, PartyOther.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v3/", name = "forename", scope = PartyOther.class)
    public JAXBElement<String> createPartyOtherForename(String value) {
        return new JAXBElement<String>(_PartyOtherForename_QNAME, String.class, PartyOther.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v3/", name = "institutionCode", scope = PartyOther.class)
    public JAXBElement<String> createPartyOtherInstitutionCode(String value) {
        return new JAXBElement<String>(_InstitutionInstitutionCode_QNAME, String.class, PartyOther.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v3/", name = "emigrationDate", scope = PartyOther.class)
    public JAXBElement<XMLGregorianCalendar> createPartyOtherEmigrationDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_PartyOtherEmigrationDate_QNAME, XMLGregorianCalendar.class, PartyOther.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v3/", name = "contactPerson", scope = PartyOther.class)
    public JAXBElement<String> createPartyOtherContactPerson(String value) {
        return new JAXBElement<String>(_InstitutionContactPerson_QNAME, String.class, PartyOther.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v3/", name = "industryCode", scope = PartyOther.class)
    public JAXBElement<String> createPartyOtherIndustryCode(String value) {
        return new JAXBElement<String>(_InstitutionIndustryCode_QNAME, String.class, PartyOther.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v3/", name = "birthDate", scope = PartyOther.class)
    public JAXBElement<XMLGregorianCalendar> createPartyOtherBirthDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_PartyOtherBirthDate_QNAME, XMLGregorianCalendar.class, PartyOther.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v3/", name = "title", scope = PartyOther.class)
    public JAXBElement<String> createPartyOtherTitle(String value) {
        return new JAXBElement<String>(_PartyOtherTitle_QNAME, String.class, PartyOther.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v3/", name = "companyRegNo", scope = PartyOther.class)
    public JAXBElement<String> createPartyOtherCompanyRegNo(String value) {
        return new JAXBElement<String>(_InstitutionCompanyRegNo_QNAME, String.class, PartyOther.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v3/", name = "name", scope = AbstractParty.class)
    public JAXBElement<String> createAbstractPartyName(String value) {
        return new JAXBElement<String>(_LocationName_QNAME, String.class, AbstractParty.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v3/", name = "siteSeqNo", scope = AbstractParty.class)
    public JAXBElement<Long> createAbstractPartySiteSeqNo(Long value) {
        return new JAXBElement<Long>(_LocationSiteSeqNo_QNAME, Long.class, AbstractParty.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v3/", name = "currencyCode", scope = AbstractParty.class)
    public JAXBElement<String> createAbstractPartyCurrencyCode(String value) {
        return new JAXBElement<String>(_LocationCurrencyCode_QNAME, String.class, AbstractParty.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v3/", name = "nameSort", scope = AbstractParty.class)
    public JAXBElement<String> createAbstractPartyNameSort(String value) {
        return new JAXBElement<String>(_LocationNameSort_QNAME, String.class, AbstractParty.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v3/", name = "idNoAlt", scope = AbstractParty.class)
    public JAXBElement<String> createAbstractPartyIdNoAlt(String value) {
        return new JAXBElement<String>(_LocationIdNoAlt_QNAME, String.class, AbstractParty.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v3/", name = "idNoSort", scope = AbstractParty.class)
    public JAXBElement<String> createAbstractPartyIdNoSort(String value) {
        return new JAXBElement<String>(_LocationIdNoSort_QNAME, String.class, AbstractParty.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v3/", name = "nameType", scope = AbstractParty.class)
    public JAXBElement<Integer> createAbstractPartyNameType(Integer value) {
        return new JAXBElement<Integer>(_AbstractPartyNameType_QNAME, Integer.class, AbstractParty.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v3/", name = "surname", scope = AbstractParty.class)
    public JAXBElement<String> createAbstractPartySurname(String value) {
        return new JAXBElement<String>(_LocationSurname_QNAME, String.class, AbstractParty.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v3/", name = "obsoleteCode", scope = AbstractParty.class)
    public JAXBElement<Integer> createAbstractPartyObsoleteCode(Integer value) {
        return new JAXBElement<Integer>(_LocationObsoleteCode_QNAME, Integer.class, AbstractParty.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v3/", name = "language", scope = AbstractParty.class)
    public JAXBElement<String> createAbstractPartyLanguage(String value) {
        return new JAXBElement<String>(_LocationLanguage_QNAME, String.class, AbstractParty.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Customer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v3/", name = "customerInfo", scope = AbstractParty.class)
    public JAXBElement<Customer> createAbstractPartyCustomerInfo(Customer value) {
        return new JAXBElement<Customer>(_AbstractPartyCustomerInfo_QNAME, Customer.class, AbstractParty.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v3/", name = "idNo", scope = AbstractParty.class)
    public JAXBElement<Long> createAbstractPartyIdNo(Long value) {
        return new JAXBElement<Long>(_LocationIdNo_QNAME, Long.class, AbstractParty.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v3/", name = "siteShortName", scope = AbstractParty.class)
    public JAXBElement<String> createAbstractPartySiteShortName(String value) {
        return new JAXBElement<String>(_SearchTokenLocSiteShortName_QNAME, String.class, AbstractParty.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v3/", name = "note", scope = AbstractParty.class)
    public JAXBElement<String> createAbstractPartyNote(String value) {
        return new JAXBElement<String>(_AbstractPartyNote_QNAME, String.class, AbstractParty.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v3/", name = "postArea", scope = Address.class)
    public JAXBElement<String> createAddressPostArea(String value) {
        return new JAXBElement<String>(_AddressPostArea_QNAME, String.class, Address.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v3/", name = "dependStreetDesc", scope = Address.class)
    public JAXBElement<String> createAddressDependStreetDesc(String value) {
        return new JAXBElement<String>(_AddressDependStreetDesc_QNAME, String.class, Address.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v3/", name = "buildingNumber", scope = Address.class)
    public JAXBElement<String> createAddressBuildingNumber(String value) {
        return new JAXBElement<String>(_AddressBuildingNumber_QNAME, String.class, Address.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v3/", name = "dependLocality", scope = Address.class)
    public JAXBElement<String> createAddressDependLocality(String value) {
        return new JAXBElement<String>(_AddressDependLocality_QNAME, String.class, Address.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v3/", name = "postalRegion", scope = Address.class)
    public JAXBElement<String> createAddressPostalRegion(String value) {
        return new JAXBElement<String>(_AddressPostalRegion_QNAME, String.class, Address.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v3/", name = "floor", scope = Address.class)
    public JAXBElement<String> createAddressFloor(String value) {
        return new JAXBElement<String>(_AddressFloor_QNAME, String.class, Address.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v3/", name = "buildingName", scope = Address.class)
    public JAXBElement<String> createAddressBuildingName(String value) {
        return new JAXBElement<String>(_AddressBuildingName_QNAME, String.class, Address.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v3/", name = "poBox", scope = Address.class)
    public JAXBElement<String> createAddressPoBox(String value) {
        return new JAXBElement<String>(_AddressPoBox_QNAME, String.class, Address.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v3/", name = "street", scope = Address.class)
    public JAXBElement<String> createAddressStreet(String value) {
        return new JAXBElement<String>(_AddressStreet_QNAME, String.class, Address.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v3/", name = "countryCode", scope = Address.class)
    public JAXBElement<String> createAddressCountryCode(String value) {
        return new JAXBElement<String>(_AddressCountryCode_QNAME, String.class, Address.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v3/", name = "county", scope = Address.class)
    public JAXBElement<String> createAddressCounty(String value) {
        return new JAXBElement<String>(_AddressCounty_QNAME, String.class, Address.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v3/", name = "houseCoName", scope = Address.class)
    public JAXBElement<String> createAddressHouseCoName(String value) {
        return new JAXBElement<String>(_AddressHouseCoName_QNAME, String.class, Address.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v3/", name = "postStreet", scope = Address.class)
    public JAXBElement<String> createAddressPostStreet(String value) {
        return new JAXBElement<String>(_AddressPostStreet_QNAME, String.class, Address.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v3/", name = "unknownAddress", scope = Address.class)
    public JAXBElement<String> createAddressUnknownAddress(String value) {
        return new JAXBElement<String>(_AddressUnknownAddress_QNAME, String.class, Address.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v3/", name = "country", scope = Address.class)
    public JAXBElement<String> createAddressCountry(String value) {
        return new JAXBElement<String>(_AddressCountry_QNAME, String.class, Address.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v3/", name = "addressCode", scope = Address.class)
    public JAXBElement<String> createAddressAddressCode(String value) {
        return new JAXBElement<String>(_SearchTokenLocAddressCode_QNAME, String.class, Address.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v3/", name = "departmentName", scope = Address.class)
    public JAXBElement<String> createAddressDepartmentName(String value) {
        return new JAXBElement<String>(_AddressDepartmentName_QNAME, String.class, Address.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v3/", name = "subBuilding", scope = Address.class)
    public JAXBElement<String> createAddressSubBuilding(String value) {
        return new JAXBElement<String>(_AddressSubBuilding_QNAME, String.class, Address.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v3/", name = "addressType", scope = Address.class)
    public JAXBElement<String> createAddressAddressType(String value) {
        return new JAXBElement<String>(_AddressAddressType_QNAME, String.class, Address.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v3/", name = "floorExt", scope = Address.class)
    public JAXBElement<String> createAddressFloorExt(String value) {
        return new JAXBElement<String>(_AddressFloorExt_QNAME, String.class, Address.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v3/", name = "streetNo", scope = Address.class)
    public JAXBElement<String> createAddressStreetNo(String value) {
        return new JAXBElement<String>(_AddressStreetNo_QNAME, String.class, Address.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v3/", name = "city", scope = Address.class)
    public JAXBElement<String> createAddressCity(String value) {
        return new JAXBElement<String>(_AddressCity_QNAME, String.class, Address.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v3/", name = "dependStreet", scope = Address.class)
    public JAXBElement<String> createAddressDependStreet(String value) {
        return new JAXBElement<String>(_AddressDependStreet_QNAME, String.class, Address.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v3/", name = "streetDescriptor", scope = Address.class)
    public JAXBElement<String> createAddressStreetDescriptor(String value) {
        return new JAXBElement<String>(_AddressStreetDescriptor_QNAME, String.class, Address.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "serviceSupplierType", scope = ServiceSupplier.class)
    public JAXBElement<String> createServiceSupplierServiceSupplierType(String value) {
        return new JAXBElement<String>(_ServiceSupplierServiceSupplierType_QNAME, String.class, ServiceSupplier.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "alias", scope = ServiceSupplier.class)
    public JAXBElement<String> createServiceSupplierAlias(String value) {
        return new JAXBElement<String>(_SearchTokenSsuAlias_QNAME, String.class, ServiceSupplier.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "description", scope = ServiceSupplier.class)
    public JAXBElement<String> createServiceSupplierDescription(String value) {
        return new JAXBElement<String>(_RelationDescription_QNAME, String.class, ServiceSupplier.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "startDate", scope = ServiceSupplier.class)
    public JAXBElement<XMLGregorianCalendar> createServiceSupplierStartDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_RelationStartDate_QNAME, XMLGregorianCalendar.class, ServiceSupplier.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "ssuContactPerson", scope = ServiceSupplier.class)
    public JAXBElement<String> createServiceSupplierSsuContactPerson(String value) {
        return new JAXBElement<String>(_ServiceSupplierSsuContactPerson_QNAME, String.class, ServiceSupplier.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "blacklistedReason", scope = ServiceSupplier.class)
    public JAXBElement<String> createServiceSupplierBlacklistedReason(String value) {
        return new JAXBElement<String>(_ServiceSupplierBlacklistedReason_QNAME, String.class, ServiceSupplier.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "priority", scope = ServiceSupplier.class)
    public JAXBElement<BigDecimal> createServiceSupplierPriority(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_ServiceSupplierPriority_QNAME, BigDecimal.class, ServiceSupplier.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "blacklisted", scope = ServiceSupplier.class)
    public JAXBElement<String> createServiceSupplierBlacklisted(String value) {
        return new JAXBElement<String>(_ServiceSupplierBlacklisted_QNAME, String.class, ServiceSupplier.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "endDate", scope = ServiceSupplier.class)
    public JAXBElement<XMLGregorianCalendar> createServiceSupplierEndDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_RelationEndDate_QNAME, XMLGregorianCalendar.class, ServiceSupplier.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v3/", name = "emigrationDate", scope = Person.class)
    public JAXBElement<XMLGregorianCalendar> createPersonEmigrationDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_PartyOtherEmigrationDate_QNAME, XMLGregorianCalendar.class, Person.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v3/", name = "middleName", scope = Person.class)
    public JAXBElement<String> createPersonMiddleName(String value) {
        return new JAXBElement<String>(_PartyOtherMiddleName_QNAME, String.class, Person.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v3/", name = "hiddenIdentity", scope = Person.class)
    public JAXBElement<String> createPersonHiddenIdentity(String value) {
        return new JAXBElement<String>(_PartyOtherHiddenIdentity_QNAME, String.class, Person.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v3/", name = "birthDate", scope = Person.class)
    public JAXBElement<XMLGregorianCalendar> createPersonBirthDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_PartyOtherBirthDate_QNAME, XMLGregorianCalendar.class, Person.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v3/", name = "deceasedDate", scope = Person.class)
    public JAXBElement<XMLGregorianCalendar> createPersonDeceasedDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_PartyOtherDeceasedDate_QNAME, XMLGregorianCalendar.class, Person.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v3/", name = "title", scope = Person.class)
    public JAXBElement<String> createPersonTitle(String value) {
        return new JAXBElement<String>(_PartyOtherTitle_QNAME, String.class, Person.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v3/", name = "forename", scope = Person.class)
    public JAXBElement<String> createPersonForename(String value) {
        return new JAXBElement<String>(_PartyOtherForename_QNAME, String.class, Person.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v3/", name = "civilRegCode", scope = Person.class)
    public JAXBElement<String> createPersonCivilRegCode(String value) {
        return new JAXBElement<String>(_PersonCivilRegCode_QNAME, String.class, Person.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "postArea", scope = SearchTokenLoc2 .class)
    public JAXBElement<String> createSearchTokenLoc2PostArea(String value) {
        return new JAXBElement<String>(_Address2PostArea_QNAME, String.class, SearchTokenLoc2 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "postStreet", scope = SearchTokenLoc2 .class)
    public JAXBElement<String> createSearchTokenLoc2PostStreet(String value) {
        return new JAXBElement<String>(_Address2PostStreet_QNAME, String.class, SearchTokenLoc2 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "street", scope = SearchTokenLoc2 .class)
    public JAXBElement<String> createSearchTokenLoc2Street(String value) {
        return new JAXBElement<String>(_Address2Street_QNAME, String.class, SearchTokenLoc2 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "streetNo", scope = SearchTokenLoc2 .class)
    public JAXBElement<String> createSearchTokenLoc2StreetNo(String value) {
        return new JAXBElement<String>(_Address2StreetNo_QNAME, String.class, SearchTokenLoc2 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "city", scope = SearchTokenLoc2 .class)
    public JAXBElement<String> createSearchTokenLoc2City(String value) {
        return new JAXBElement<String>(_Address2City_QNAME, String.class, SearchTokenLoc2 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "memberNameIdNo", scope = ClaimItemRelation.class)
    public JAXBElement<Long> createClaimItemRelationMemberNameIdNo(Long value) {
        return new JAXBElement<Long>(_ClaimEventRelationMemberNameIdNo_QNAME, Long.class, ClaimItemRelation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "claimItemNo", scope = ClaimItemRelation.class)
    public JAXBElement<Long> createClaimItemRelationClaimItemNo(Long value) {
        return new JAXBElement<Long>(_ClaimItemRelationClaimItemNo_QNAME, Long.class, ClaimItemRelation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "affinityGroupName", scope = SearchTokenAff.class)
    public JAXBElement<String> createSearchTokenAffAffinityGroupName(String value) {
        return new JAXBElement<String>(_SearchTokenAffAffinityGroupName_QNAME, String.class, SearchTokenAff.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "affinityGroupDesc", scope = SearchTokenAff.class)
    public JAXBElement<String> createSearchTokenAffAffinityGroupDesc(String value) {
        return new JAXBElement<String>(_SearchTokenAffAffinityGroupDesc_QNAME, String.class, SearchTokenAff.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/common/v2/", name = "description", scope = TranslatedCode.class)
    public JAXBElement<String> createTranslatedCodeDescription(String value) {
        return new JAXBElement<String>(_TranslatedCodeDescription_QNAME, String.class, TranslatedCode.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/common/v2/", name = "diplayCode", scope = TranslatedCode.class)
    public JAXBElement<String> createTranslatedCodeDiplayCode(String value) {
        return new JAXBElement<String>(_TranslatedCodeDiplayCode_QNAME, String.class, TranslatedCode.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/common/v2/", name = "helpText", scope = TranslatedCode.class)
    public JAXBElement<String> createTranslatedCodeHelpText(String value) {
        return new JAXBElement<String>(_TranslatedCodeHelpText_QNAME, String.class, TranslatedCode.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "policyNo", scope = PolicyRelation.class)
    public JAXBElement<Long> createPolicyRelationPolicyNo(Long value) {
        return new JAXBElement<Long>(_PolicyRelationPolicyNo_QNAME, Long.class, PolicyRelation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "memberNameIdNo", scope = PolicyRelation.class)
    public JAXBElement<Long> createPolicyRelationMemberNameIdNo(Long value) {
        return new JAXBElement<Long>(_ClaimEventRelationMemberNameIdNo_QNAME, Long.class, PolicyRelation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/common/v2/", name = "varchar2Value", scope = Value.class)
    public JAXBElement<String> createValueVarchar2Value(String value) {
        return new JAXBElement<String>(_ValueVarchar2Value_QNAME, String.class, Value.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/common/v2/", name = "numberValue", scope = Value.class)
    public JAXBElement<BigDecimal> createValueNumberValue(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_ValueNumberValue_QNAME, BigDecimal.class, Value.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/common/v2/", name = "dateValue", scope = Value.class)
    public JAXBElement<XMLGregorianCalendar> createValueDateValue(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_ValueDateValue_QNAME, XMLGregorianCalendar.class, Value.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PartyOther2 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "partyOther", scope = PartyElement2 .class)
    public JAXBElement<PartyOther2> createPartyElement2PartyOther(PartyOther2 value) {
        return new JAXBElement<PartyOther2>(_PartyElement2PartyOther_QNAME, PartyOther2 .class, PartyElement2 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Institution2 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "institution", scope = PartyElement2 .class)
    public JAXBElement<Institution2> createPartyElement2Institution(Institution2 value) {
        return new JAXBElement<Institution2>(_PartyElement2Institution_QNAME, Institution2 .class, PartyElement2 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Person2 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "person", scope = PartyElement2 .class)
    public JAXBElement<Person2> createPartyElement2Person(Person2 value) {
        return new JAXBElement<Person2>(_PartyElement2Person_QNAME, Person2 .class, PartyElement2 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "ownerNameIdNo", scope = PartyRelation.class)
    public JAXBElement<Long> createPartyRelationOwnerNameIdNo(Long value) {
        return new JAXBElement<Long>(_AffinityRelationOwnerNameIdNo_QNAME, Long.class, PartyRelation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "memberNameIdNo", scope = PartyRelation.class)
    public JAXBElement<Long> createPartyRelationMemberNameIdNo(Long value) {
        return new JAXBElement<Long>(_ClaimEventRelationMemberNameIdNo_QNAME, Long.class, PartyRelation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "roleDescription", scope = PartyRole.class)
    public JAXBElement<String> createPartyRoleRoleDescription(String value) {
        return new JAXBElement<String>(_PartyRoleRoleDescription_QNAME, String.class, PartyRole.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "role", scope = PartyRole.class)
    public JAXBElement<String> createPartyRoleRole(String value) {
        return new JAXBElement<String>(_PartyRoleRole_QNAME, String.class, PartyRole.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "comGroup", scope = Agent.class)
    public JAXBElement<String> createAgentComGroup(String value) {
        return new JAXBElement<String>(_SearchTokenAgtComGroup_QNAME, String.class, Agent.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "startDate", scope = Agent.class)
    public JAXBElement<XMLGregorianCalendar> createAgentStartDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_RelationStartDate_QNAME, XMLGregorianCalendar.class, Agent.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "comCategory", scope = Agent.class)
    public JAXBElement<String> createAgentComCategory(String value) {
        return new JAXBElement<String>(_SearchTokenAgtComCategory_QNAME, String.class, Agent.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "seqNo", scope = Agent.class)
    public JAXBElement<BigDecimal> createAgentSeqNo(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_RelationSeqNo_QNAME, BigDecimal.class, Agent.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "endDate", scope = Agent.class)
    public JAXBElement<XMLGregorianCalendar> createAgentEndDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_RelationEndDate_QNAME, XMLGregorianCalendar.class, Agent.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "ipType", scope = InterestedParty.class)
    public JAXBElement<Long> createInterestedPartyIpType(Long value) {
        return new JAXBElement<Long>(_InterestedPartyIpType_QNAME, Long.class, InterestedParty.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "typeOther", scope = InterestedParty.class)
    public JAXBElement<String> createInterestedPartyTypeOther(String value) {
        return new JAXBElement<String>(_InterestedPartyTypeOther_QNAME, String.class, InterestedParty.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "affinityNo", scope = Affinity.class)
    public JAXBElement<Long> createAffinityAffinityNo(Long value) {
        return new JAXBElement<Long>(_AffinityRelationAffinityNo_QNAME, Long.class, Affinity.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "descriptionNote", scope = Affinity.class)
    public JAXBElement<String> createAffinityDescriptionNote(String value) {
        return new JAXBElement<String>(_AffinityDescriptionNote_QNAME, String.class, Affinity.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://infrastructure.tia.dk/schema/party/v2/", name = "documentPile", scope = Affinity.class)
    public JAXBElement<String> createAffinityDocumentPile(String value) {
        return new JAXBElement<String>(_AffinityDocumentPile_QNAME, String.class, Affinity.class, value);
    }

}
