
package za.co.ominsure.synapse.iwyze.lob.backend.claim.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * Mappes into TIA Commons type "obj_page_sort"
 * 
 * <p>Java class for PageSort complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PageSort">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="fromRow" type="{http://infrastructure.tia.dk/schema/common/v2/}fromRow" minOccurs="0"/>
 *         &lt;element name="numberOfRows" type="{http://infrastructure.tia.dk/schema/common/v2/}numberOfRows" minOccurs="0"/>
 *         &lt;element name="scnNumber" type="{http://infrastructure.tia.dk/schema/common/v2/}SCNNumber" minOccurs="0"/>
 *         &lt;element name="orderByCollection" type="{http://infrastructure.tia.dk/schema/common/v2/}StringCollection" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PageSort", namespace = "http://infrastructure.tia.dk/schema/common/v2/", propOrder = {
    "fromRow",
    "numberOfRows",
    "scnNumber",
    "orderByCollection"
})
public class PageSort {

    @XmlSchemaType(name = "unsignedLong")
    protected Long fromRow;
    @XmlSchemaType(name = "unsignedLong")
    protected Long numberOfRows;
    protected String scnNumber;
    protected StringCollection orderByCollection;

    /**
     * Gets the value of the fromRow property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getFromRow() {
        return fromRow;
    }

    /**
     * Sets the value of the fromRow property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setFromRow(Long value) {
        this.fromRow = value;
    }

    /**
     * Gets the value of the numberOfRows property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getNumberOfRows() {
        return numberOfRows;
    }

    /**
     * Sets the value of the numberOfRows property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setNumberOfRows(Long value) {
        this.numberOfRows = value;
    }

    /**
     * Gets the value of the scnNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getScnNumber() {
        return scnNumber;
    }

    /**
     * Sets the value of the scnNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setScnNumber(String value) {
        this.scnNumber = value;
    }

    /**
     * Gets the value of the orderByCollection property.
     * 
     * @return
     *     possible object is
     *     {@link StringCollection }
     *     
     */
    public StringCollection getOrderByCollection() {
        return orderByCollection;
    }

    /**
     * Sets the value of the orderByCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link StringCollection }
     *     
     */
    public void setOrderByCollection(StringCollection value) {
        this.orderByCollection = value;
    }

}
