
package za.co.ominsure.synapse.iwyze.lob.backend.interactive.claim.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Additional comments
 * 
 * <p>Java class for AnswerSet complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AnswerSet">
 *   &lt;complexContent>
 *     &lt;extension base="{http://infrastructure.tia.dk/schema/common/v2/}TIAObject">
 *       &lt;sequence>
 *         &lt;element name="historySeqNo" type="{http://infrastructure.tia.dk/schema/common/v2/}long10" minOccurs="0"/>
 *         &lt;element name="questionClass" type="{http://infrastructure.tia.dk/schema/common/v2/}string10" minOccurs="0"/>
 *         &lt;element name="questionDate" type="{http://infrastructure.tia.dk/schema/common/v2/}timestamp" minOccurs="0"/>
 *         &lt;element name="answerCollection" type="{http://infrastructure.tia.dk/schema/claim/v2/}AnswerCollection" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AnswerSet", propOrder = {
    "historySeqNo",
    "questionClass",
    "questionDate",
    "answerCollection"
})
public class AnswerSet
    extends TIAObject
{

    @XmlElement(nillable = true)
    protected Long historySeqNo;
    @XmlElement(nillable = true)
    protected String questionClass;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar questionDate;
    protected AnswerCollection answerCollection;

    /**
     * Gets the value of the historySeqNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getHistorySeqNo() {
        return historySeqNo;
    }

    /**
     * Sets the value of the historySeqNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setHistorySeqNo(Long value) {
        this.historySeqNo = value;
    }

    /**
     * Gets the value of the questionClass property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQuestionClass() {
        return questionClass;
    }

    /**
     * Sets the value of the questionClass property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQuestionClass(String value) {
        this.questionClass = value;
    }

    /**
     * Gets the value of the questionDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getQuestionDate() {
        return questionDate;
    }

    /**
     * Sets the value of the questionDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setQuestionDate(XMLGregorianCalendar value) {
        this.questionDate = value;
    }

    /**
     * Gets the value of the answerCollection property.
     * 
     * @return
     *     possible object is
     *     {@link AnswerCollection }
     *     
     */
    public AnswerCollection getAnswerCollection() {
        return answerCollection;
    }

    /**
     * Sets the value of the answerCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link AnswerCollection }
     *     
     */
    public void setAnswerCollection(AnswerCollection value) {
        this.answerCollection = value;
    }

}
