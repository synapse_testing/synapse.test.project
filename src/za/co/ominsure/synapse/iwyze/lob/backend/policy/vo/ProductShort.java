
package za.co.ominsure.synapse.iwyze.lob.backend.policy.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Additional comments
 * 
 * <p>Java class for ProductShort complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ProductShort">
 *   &lt;complexContent>
 *     &lt;extension base="{http://infrastructure.tia.dk/schema/common/v2/}TIAObject">
 *       &lt;sequence>
 *         &lt;element name="prodId" type="{http://infrastructure.tia.dk/schema/common/v2/}string2" minOccurs="0"/>
 *         &lt;element name="productNameTranslationCollection" type="{http://infrastructure.tia.dk/schema/common/v2/}ValueTranslationPolicyCollection" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProductShort", propOrder = {
    "prodId",
    "productNameTranslationCollection"
})
public class ProductShort
    extends TIAObject
{

    @XmlElement(nillable = true)
    protected String prodId;
    protected ValueTranslationPolicyCollection productNameTranslationCollection;

    /**
     * Gets the value of the prodId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProdId() {
        return prodId;
    }

    /**
     * Sets the value of the prodId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProdId(String value) {
        this.prodId = value;
    }

    /**
     * Gets the value of the productNameTranslationCollection property.
     * 
     * @return
     *     possible object is
     *     {@link ValueTranslationPolicyCollection }
     *     
     */
    public ValueTranslationPolicyCollection getProductNameTranslationCollection() {
        return productNameTranslationCollection;
    }

    /**
     * Sets the value of the productNameTranslationCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link ValueTranslationPolicyCollection }
     *     
     */
    public void setProductNameTranslationCollection(ValueTranslationPolicyCollection value) {
        this.productNameTranslationCollection = value;
    }

}
