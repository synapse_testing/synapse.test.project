
package za.co.ominsure.synapse.iwyze.lob.backend.party.vo;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * Additional comments
 * 
 * <p>Java class for SearchTokenPar complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SearchTokenPar">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="name" type="{http://infrastructure.tia.dk/schema/party/v2/}shortName" minOccurs="0"/>
 *         &lt;element name="companyReg" type="{http://infrastructure.tia.dk/schema/party/v2/}companyReg" minOccurs="0"/>
 *         &lt;element name="civilReg" type="{http://infrastructure.tia.dk/schema/party/v2/}civilReg" minOccurs="0"/>
 *         &lt;element name="street" type="{http://infrastructure.tia.dk/schema/party/v2/}street" minOccurs="0"/>
 *         &lt;element name="postArea" type="{http://infrastructure.tia.dk/schema/party/v2/}postArea" minOccurs="0"/>
 *         &lt;element name="postStreet" type="{http://infrastructure.tia.dk/schema/party/v2/}postStreet" minOccurs="0"/>
 *         &lt;element name="title" type="{http://infrastructure.tia.dk/schema/party/v2/}title" minOccurs="0"/>
 *         &lt;element name="forename" type="{http://infrastructure.tia.dk/schema/common/v2/}string70" minOccurs="0"/>
 *         &lt;element name="coName" type="{http://infrastructure.tia.dk/schema/common/v2/}string70" minOccurs="0"/>
 *         &lt;element name="streetNo" type="{http://infrastructure.tia.dk/schema/party/v2/}streetNo" minOccurs="0"/>
 *         &lt;element name="floor" type="{http://infrastructure.tia.dk/schema/party/v2/}floor" minOccurs="0"/>
 *         &lt;element name="floorExt" type="{http://infrastructure.tia.dk/schema/party/v2/}floorExt" minOccurs="0"/>
 *         &lt;element name="city" type="{http://infrastructure.tia.dk/schema/party/v2/}city" minOccurs="0"/>
 *         &lt;element name="countryCode" type="{http://infrastructure.tia.dk/schema/party/v2/}countryCode" minOccurs="0"/>
 *         &lt;element name="contactDetail" type="{http://infrastructure.tia.dk/schema/party/v2/}contactInfoDetail" minOccurs="0"/>
 *         &lt;element name="contactDetailType" type="{http://infrastructure.tia.dk/schema/party/v2/}contactInfoType" minOccurs="0"/>
 *         &lt;element name="roleCode" type="{http://infrastructure.tia.dk/schema/party/v2/}roleCode" minOccurs="0"/>
 *         &lt;element name="addressCode" type="{http://infrastructure.tia.dk/schema/common/v2/}string100" minOccurs="0"/>
 *         &lt;element name="handler" type="{http://infrastructure.tia.dk/schema/common/v2/}string8" minOccurs="0"/>
 *         &lt;element name="agentNo" type="{http://infrastructure.tia.dk/schema/common/v2/}long10" minOccurs="0"/>
 *         &lt;element name="siteShortName" type="{http://infrastructure.tia.dk/schema/common/v2/}string10" minOccurs="0"/>
 *         &lt;element name="searchMethod" type="{http://infrastructure.tia.dk/schema/common/v2/}string20" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SearchTokenPar", namespace = "http://infrastructure.tia.dk/schema/party/v3/", propOrder = {
    "name",
    "companyReg",
    "civilReg",
    "street",
    "postArea",
    "postStreet",
    "title",
    "forename",
    "coName",
    "streetNo",
    "floor",
    "floorExt",
    "city",
    "countryCode",
    "contactDetail",
    "contactDetailType",
    "roleCode",
    "addressCode",
    "handler",
    "agentNo",
    "siteShortName",
    "searchMethod"
})
public class SearchTokenPar {

    protected String name;
    protected String companyReg;
    protected String civilReg;
    protected String street;
    protected String postArea;
    protected String postStreet;
    protected String title;
    protected String forename;
    protected String coName;
    protected String streetNo;
    protected String floor;
    protected String floorExt;
    protected String city;
    protected String countryCode;
    protected String contactDetail;
    protected String contactDetailType;
    protected String roleCode;
    @XmlElementRef(name = "addressCode", namespace = "http://infrastructure.tia.dk/schema/party/v3/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> addressCode;
    @XmlElementRef(name = "handler", namespace = "http://infrastructure.tia.dk/schema/party/v3/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> handler;
    @XmlElementRef(name = "agentNo", namespace = "http://infrastructure.tia.dk/schema/party/v3/", type = JAXBElement.class, required = false)
    protected JAXBElement<Long> agentNo;
    @XmlElementRef(name = "siteShortName", namespace = "http://infrastructure.tia.dk/schema/party/v3/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> siteShortName;
    @XmlElementRef(name = "searchMethod", namespace = "http://infrastructure.tia.dk/schema/party/v3/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> searchMethod;

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the companyReg property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCompanyReg() {
        return companyReg;
    }

    /**
     * Sets the value of the companyReg property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCompanyReg(String value) {
        this.companyReg = value;
    }

    /**
     * Gets the value of the civilReg property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCivilReg() {
        return civilReg;
    }

    /**
     * Sets the value of the civilReg property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCivilReg(String value) {
        this.civilReg = value;
    }

    /**
     * Gets the value of the street property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStreet() {
        return street;
    }

    /**
     * Sets the value of the street property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStreet(String value) {
        this.street = value;
    }

    /**
     * Gets the value of the postArea property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPostArea() {
        return postArea;
    }

    /**
     * Sets the value of the postArea property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPostArea(String value) {
        this.postArea = value;
    }

    /**
     * Gets the value of the postStreet property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPostStreet() {
        return postStreet;
    }

    /**
     * Sets the value of the postStreet property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPostStreet(String value) {
        this.postStreet = value;
    }

    /**
     * Gets the value of the title property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets the value of the title property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTitle(String value) {
        this.title = value;
    }

    /**
     * Gets the value of the forename property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getForename() {
        return forename;
    }

    /**
     * Sets the value of the forename property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setForename(String value) {
        this.forename = value;
    }

    /**
     * Gets the value of the coName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCoName() {
        return coName;
    }

    /**
     * Sets the value of the coName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCoName(String value) {
        this.coName = value;
    }

    /**
     * Gets the value of the streetNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStreetNo() {
        return streetNo;
    }

    /**
     * Sets the value of the streetNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStreetNo(String value) {
        this.streetNo = value;
    }

    /**
     * Gets the value of the floor property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFloor() {
        return floor;
    }

    /**
     * Sets the value of the floor property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFloor(String value) {
        this.floor = value;
    }

    /**
     * Gets the value of the floorExt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFloorExt() {
        return floorExt;
    }

    /**
     * Sets the value of the floorExt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFloorExt(String value) {
        this.floorExt = value;
    }

    /**
     * Gets the value of the city property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCity() {
        return city;
    }

    /**
     * Sets the value of the city property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCity(String value) {
        this.city = value;
    }

    /**
     * Gets the value of the countryCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCountryCode() {
        return countryCode;
    }

    /**
     * Sets the value of the countryCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCountryCode(String value) {
        this.countryCode = value;
    }

    /**
     * Gets the value of the contactDetail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContactDetail() {
        return contactDetail;
    }

    /**
     * Sets the value of the contactDetail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContactDetail(String value) {
        this.contactDetail = value;
    }

    /**
     * Gets the value of the contactDetailType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContactDetailType() {
        return contactDetailType;
    }

    /**
     * Sets the value of the contactDetailType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContactDetailType(String value) {
        this.contactDetailType = value;
    }

    /**
     * Gets the value of the roleCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRoleCode() {
        return roleCode;
    }

    /**
     * Sets the value of the roleCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRoleCode(String value) {
        this.roleCode = value;
    }

    /**
     * Gets the value of the addressCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAddressCode() {
        return addressCode;
    }

    /**
     * Sets the value of the addressCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAddressCode(JAXBElement<String> value) {
        this.addressCode = value;
    }

    /**
     * Gets the value of the handler property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getHandler() {
        return handler;
    }

    /**
     * Sets the value of the handler property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setHandler(JAXBElement<String> value) {
        this.handler = value;
    }

    /**
     * Gets the value of the agentNo property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Long }{@code >}
     *     
     */
    public JAXBElement<Long> getAgentNo() {
        return agentNo;
    }

    /**
     * Sets the value of the agentNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Long }{@code >}
     *     
     */
    public void setAgentNo(JAXBElement<Long> value) {
        this.agentNo = value;
    }

    /**
     * Gets the value of the siteShortName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSiteShortName() {
        return siteShortName;
    }

    /**
     * Sets the value of the siteShortName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSiteShortName(JAXBElement<String> value) {
        this.siteShortName = value;
    }

    /**
     * Gets the value of the searchMethod property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSearchMethod() {
        return searchMethod;
    }

    /**
     * Sets the value of the searchMethod property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSearchMethod(JAXBElement<String> value) {
        this.searchMethod = value;
    }

}
