
package za.co.ominsure.synapse.iwyze.lob.backend.claim.vo;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for Value complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Value">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element name="varchar2Value" type="{http://infrastructure.tia.dk/schema/common/v2/}varchar2"/>
 *         &lt;element name="numberValue" type="{http://infrastructure.tia.dk/schema/common/v2/}number"/>
 *         &lt;element name="dateValue" type="{http://infrastructure.tia.dk/schema/common/v2/}timestamp"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Value", namespace = "http://infrastructure.tia.dk/schema/common/v2/", propOrder = {
    "dateValue",
    "numberValue",
    "varchar2Value"
})
public class Value {

    @XmlElement(nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dateValue;
    @XmlElement(nillable = true)
    protected BigDecimal numberValue;
    @XmlElement(nillable = true)
    protected String varchar2Value;

    /**
     * Gets the value of the dateValue property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDateValue() {
        return dateValue;
    }

    /**
     * Sets the value of the dateValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDateValue(XMLGregorianCalendar value) {
        this.dateValue = value;
    }

    /**
     * Gets the value of the numberValue property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getNumberValue() {
        return numberValue;
    }

    /**
     * Sets the value of the numberValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setNumberValue(BigDecimal value) {
        this.numberValue = value;
    }

    /**
     * Gets the value of the varchar2Value property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVarchar2Value() {
        return varchar2Value;
    }

    /**
     * Sets the value of the varchar2Value property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVarchar2Value(String value) {
        this.varchar2Value = value;
    }

}
