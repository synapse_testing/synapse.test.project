
package za.co.ominsure.synapse.iwyze.lob.backend.policy.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * Additional comments
 * 
 * <p>Java class for RiskDefinition complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RiskDefinition">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="groupedRiskTypeCollection" type="{http://infrastructure.tia.dk/schema/policy/v2/}RiskTypeGroupCollection" minOccurs="0"/>
 *         &lt;element name="standaloneRiskTypeCollection" type="{http://infrastructure.tia.dk/schema/policy/v2/}RiskTypeCollection" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RiskDefinition", propOrder = {
    "groupedRiskTypeCollection",
    "standaloneRiskTypeCollection"
})
public class RiskDefinition {

    protected RiskTypeGroupCollection groupedRiskTypeCollection;
    protected RiskTypeCollection standaloneRiskTypeCollection;

    /**
     * Gets the value of the groupedRiskTypeCollection property.
     * 
     * @return
     *     possible object is
     *     {@link RiskTypeGroupCollection }
     *     
     */
    public RiskTypeGroupCollection getGroupedRiskTypeCollection() {
        return groupedRiskTypeCollection;
    }

    /**
     * Sets the value of the groupedRiskTypeCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link RiskTypeGroupCollection }
     *     
     */
    public void setGroupedRiskTypeCollection(RiskTypeGroupCollection value) {
        this.groupedRiskTypeCollection = value;
    }

    /**
     * Gets the value of the standaloneRiskTypeCollection property.
     * 
     * @return
     *     possible object is
     *     {@link RiskTypeCollection }
     *     
     */
    public RiskTypeCollection getStandaloneRiskTypeCollection() {
        return standaloneRiskTypeCollection;
    }

    /**
     * Sets the value of the standaloneRiskTypeCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link RiskTypeCollection }
     *     
     */
    public void setStandaloneRiskTypeCollection(RiskTypeCollection value) {
        this.standaloneRiskTypeCollection = value;
    }

}
