
package za.co.ominsure.synapse.iwyze.lob.backend.detail.payment.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="inputToken" type="{http://infrastructure.tia.dk/schema/common/v2/}InputToken"/>
 *         &lt;element name="searchToken" type="{http://infrastructure.tia.dk/schema/account/v2/}searchTokenAccount"/>
 *         &lt;element name="pageSort" type="{http://infrastructure.tia.dk/schema/common/v2/}PageSort" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "inputToken",
    "searchToken",
    "pageSort"
})
@XmlRootElement(name = "searchAccountRequest")
public class SearchAccountRequest {

    @XmlElement(required = true)
    protected InputToken inputToken;
    @XmlElement(required = true)
    protected SearchTokenAccount searchToken;
    protected PageSort pageSort;

    /**
     * Gets the value of the inputToken property.
     * 
     * @return
     *     possible object is
     *     {@link InputToken }
     *     
     */
    public InputToken getInputToken() {
        return inputToken;
    }

    /**
     * Sets the value of the inputToken property.
     * 
     * @param value
     *     allowed object is
     *     {@link InputToken }
     *     
     */
    public void setInputToken(InputToken value) {
        this.inputToken = value;
    }

    /**
     * Gets the value of the searchToken property.
     * 
     * @return
     *     possible object is
     *     {@link SearchTokenAccount }
     *     
     */
    public SearchTokenAccount getSearchToken() {
        return searchToken;
    }

    /**
     * Sets the value of the searchToken property.
     * 
     * @param value
     *     allowed object is
     *     {@link SearchTokenAccount }
     *     
     */
    public void setSearchToken(SearchTokenAccount value) {
        this.searchToken = value;
    }

    /**
     * Gets the value of the pageSort property.
     * 
     * @return
     *     possible object is
     *     {@link PageSort }
     *     
     */
    public PageSort getPageSort() {
        return pageSort;
    }

    /**
     * Sets the value of the pageSort property.
     * 
     * @param value
     *     allowed object is
     *     {@link PageSort }
     *     
     */
    public void setPageSort(PageSort value) {
        this.pageSort = value;
    }

}
