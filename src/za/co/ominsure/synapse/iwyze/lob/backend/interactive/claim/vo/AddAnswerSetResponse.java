
package za.co.ominsure.synapse.iwyze.lob.backend.interactive.claim.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="answerSet" type="{http://infrastructure.tia.dk/schema/claim/v2/}AnswerSet"/>
 *         &lt;element name="result" type="{http://infrastructure.tia.dk/schema/common/v2/}Result"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "answerSet",
    "result"
})
@XmlRootElement(name = "addAnswerSetResponse")
public class AddAnswerSetResponse {

    @XmlElement(required = true, nillable = true)
    protected AnswerSet answerSet;
    @XmlElement(required = true)
    protected Result result;

    /**
     * Gets the value of the answerSet property.
     * 
     * @return
     *     possible object is
     *     {@link AnswerSet }
     *     
     */
    public AnswerSet getAnswerSet() {
        return answerSet;
    }

    /**
     * Sets the value of the answerSet property.
     * 
     * @param value
     *     allowed object is
     *     {@link AnswerSet }
     *     
     */
    public void setAnswerSet(AnswerSet value) {
        this.answerSet = value;
    }

    /**
     * Gets the value of the result property.
     * 
     * @return
     *     possible object is
     *     {@link Result }
     *     
     */
    public Result getResult() {
        return result;
    }

    /**
     * Sets the value of the result property.
     * 
     * @param value
     *     allowed object is
     *     {@link Result }
     *     
     */
    public void setResult(Result value) {
        this.result = value;
    }

}
