
package za.co.ominsure.synapse.iwyze.lob.backend.interactive.claim.vo;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Additional comments
 * 
 * <p>Java class for Subclaim complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Subclaim">
 *   &lt;complexContent>
 *     &lt;extension base="{http://infrastructure.tia.dk/schema/common/v2/}TIAObject">
 *       &lt;sequence>
 *         &lt;element name="subclaimNo" type="{http://infrastructure.tia.dk/schema/common/v2/}long10" minOccurs="0"/>
 *         &lt;element name="thirdPartyNo" type="{http://infrastructure.tia.dk/schema/common/v2/}long10" minOccurs="0"/>
 *         &lt;element name="subclaimType" type="{http://infrastructure.tia.dk/schema/common/v2/}string3" minOccurs="0"/>
 *         &lt;element name="coverDecision" type="{http://infrastructure.tia.dk/schema/common/v2/}string3" minOccurs="0"/>
 *         &lt;element name="coverDecisionDesc" type="{http://infrastructure.tia.dk/schema/common/v2/}string2000" minOccurs="0"/>
 *         &lt;element name="description" type="{http://infrastructure.tia.dk/schema/common/v2/}string2000" minOccurs="0"/>
 *         &lt;element name="objectNo" type="{http://infrastructure.tia.dk/schema/common/v2/}long10" minOccurs="0"/>
 *         &lt;element name="objectSeqNo" type="{http://infrastructure.tia.dk/schema/common/v2/}long10" minOccurs="0"/>
 *         &lt;element name="objectId" type="{http://infrastructure.tia.dk/schema/common/v2/}string30" minOccurs="0"/>
 *         &lt;element name="statCode1" type="{http://infrastructure.tia.dk/schema/common/v2/}string3" minOccurs="0"/>
 *         &lt;element name="statCode2" type="{http://infrastructure.tia.dk/schema/common/v2/}string3" minOccurs="0"/>
 *         &lt;element name="statCode3" type="{http://infrastructure.tia.dk/schema/common/v2/}string3" minOccurs="0"/>
 *         &lt;element name="status" type="{http://infrastructure.tia.dk/schema/common/v2/}string3" minOccurs="0"/>
 *         &lt;element name="firstOpenDate" type="{http://infrastructure.tia.dk/schema/common/v2/}timestamp" minOccurs="0"/>
 *         &lt;element name="firstCloseDate" type="{http://infrastructure.tia.dk/schema/common/v2/}timestamp" minOccurs="0"/>
 *         &lt;element name="reopenDate" type="{http://infrastructure.tia.dk/schema/common/v2/}timestamp" minOccurs="0"/>
 *         &lt;element name="reopenReason" type="{http://infrastructure.tia.dk/schema/common/v2/}string2000" minOccurs="0"/>
 *         &lt;element name="recloseDate" type="{http://infrastructure.tia.dk/schema/common/v2/}timestamp" minOccurs="0"/>
 *         &lt;element name="hoursSpend" type="{http://infrastructure.tia.dk/schema/common/v2/}decimal38Point2" minOccurs="0"/>
 *         &lt;element name="workgroup" type="{http://infrastructure.tia.dk/schema/common/v2/}string10" minOccurs="0"/>
 *         &lt;element name="handler" type="{http://infrastructure.tia.dk/schema/common/v2/}string8" minOccurs="0"/>
 *         &lt;element name="claimClass" type="{http://infrastructure.tia.dk/schema/common/v2/}string10" minOccurs="0"/>
 *         &lt;element name="questionClass" type="{http://infrastructure.tia.dk/schema/common/v2/}string10" minOccurs="0"/>
 *         &lt;element name="questionStatus" type="{http://infrastructure.tia.dk/schema/common/v2/}string3" minOccurs="0"/>
 *         &lt;element name="tasklistClass" type="{http://infrastructure.tia.dk/schema/common/v2/}string10" minOccurs="0"/>
 *         &lt;element name="itemClass" type="{http://infrastructure.tia.dk/schema/common/v2/}string10" minOccurs="0"/>
 *         &lt;element name="referralCode" type="{http://infrastructure.tia.dk/schema/common/v2/}string3" minOccurs="0"/>
 *         &lt;element name="scorecardScore" type="{http://infrastructure.tia.dk/schema/common/v2/}decimal38Point0" minOccurs="0"/>
 *         &lt;element name="categoryResult" type="{http://infrastructure.tia.dk/schema/common/v2/}string10" minOccurs="0"/>
 *         &lt;element name="assignProfileId" type="{http://infrastructure.tia.dk/schema/common/v2/}string6" minOccurs="0"/>
 *         &lt;element name="answerSetCollection" type="{http://infrastructure.tia.dk/schema/claim/v2/}AnswerSetCollection" minOccurs="0"/>
 *         &lt;element name="claimItemCollection" type="{http://infrastructure.tia.dk/schema/claim/v2/}ClaimItemCollection" minOccurs="0"/>
 *         &lt;element name="claimTaskCollection" type="{http://infrastructure.tia.dk/schema/claim/v2/}ClaimTaskCollection" minOccurs="0"/>
 *         &lt;element name="referralCollection" type="{http://infrastructure.tia.dk/schema/claim/v2/}ClaimReferralCollection" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Subclaim", propOrder = {
    "subclaimNo",
    "thirdPartyNo",
    "subclaimType",
    "coverDecision",
    "coverDecisionDesc",
    "description",
    "objectNo",
    "objectSeqNo",
    "objectId",
    "statCode1",
    "statCode2",
    "statCode3",
    "status",
    "firstOpenDate",
    "firstCloseDate",
    "reopenDate",
    "reopenReason",
    "recloseDate",
    "hoursSpend",
    "workgroup",
    "handler",
    "claimClass",
    "questionClass",
    "questionStatus",
    "tasklistClass",
    "itemClass",
    "referralCode",
    "scorecardScore",
    "categoryResult",
    "assignProfileId",
    "answerSetCollection",
    "claimItemCollection",
    "claimTaskCollection",
    "referralCollection"
})
public class Subclaim
    extends TIAObject
{

    @XmlElement(nillable = true)
    protected Long subclaimNo;
    @XmlElement(nillable = true)
    protected Long thirdPartyNo;
    @XmlElement(nillable = true)
    protected String subclaimType;
    @XmlElement(nillable = true)
    protected String coverDecision;
    @XmlElement(nillable = true)
    protected String coverDecisionDesc;
    @XmlElement(nillable = true)
    protected String description;
    @XmlElement(nillable = true)
    protected Long objectNo;
    @XmlElement(nillable = true)
    protected Long objectSeqNo;
    @XmlElement(nillable = true)
    protected String objectId;
    @XmlElement(nillable = true)
    protected String statCode1;
    @XmlElement(nillable = true)
    protected String statCode2;
    @XmlElement(nillable = true)
    protected String statCode3;
    @XmlElement(nillable = true)
    protected String status;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar firstOpenDate;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar firstCloseDate;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar reopenDate;
    @XmlElement(nillable = true)
    protected String reopenReason;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar recloseDate;
    @XmlElement(nillable = true)
    protected BigDecimal hoursSpend;
    @XmlElement(nillable = true)
    protected String workgroup;
    @XmlElement(nillable = true)
    protected String handler;
    @XmlElement(nillable = true)
    protected String claimClass;
    @XmlElement(nillable = true)
    protected String questionClass;
    @XmlElement(nillable = true)
    protected String questionStatus;
    @XmlElement(nillable = true)
    protected String tasklistClass;
    @XmlElement(nillable = true)
    protected String itemClass;
    @XmlElement(nillable = true)
    protected String referralCode;
    @XmlElement(nillable = true)
    protected BigDecimal scorecardScore;
    @XmlElement(nillable = true)
    protected String categoryResult;
    @XmlElement(nillable = true)
    protected String assignProfileId;
    protected AnswerSetCollection answerSetCollection;
    protected ClaimItemCollection claimItemCollection;
    protected ClaimTaskCollection claimTaskCollection;
    protected ClaimReferralCollection referralCollection;

    /**
     * Gets the value of the subclaimNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getSubclaimNo() {
        return subclaimNo;
    }

    /**
     * Sets the value of the subclaimNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setSubclaimNo(Long value) {
        this.subclaimNo = value;
    }

    /**
     * Gets the value of the thirdPartyNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getThirdPartyNo() {
        return thirdPartyNo;
    }

    /**
     * Sets the value of the thirdPartyNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setThirdPartyNo(Long value) {
        this.thirdPartyNo = value;
    }

    /**
     * Gets the value of the subclaimType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubclaimType() {
        return subclaimType;
    }

    /**
     * Sets the value of the subclaimType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubclaimType(String value) {
        this.subclaimType = value;
    }

    /**
     * Gets the value of the coverDecision property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCoverDecision() {
        return coverDecision;
    }

    /**
     * Sets the value of the coverDecision property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCoverDecision(String value) {
        this.coverDecision = value;
    }

    /**
     * Gets the value of the coverDecisionDesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCoverDecisionDesc() {
        return coverDecisionDesc;
    }

    /**
     * Sets the value of the coverDecisionDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCoverDecisionDesc(String value) {
        this.coverDecisionDesc = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the objectNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getObjectNo() {
        return objectNo;
    }

    /**
     * Sets the value of the objectNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setObjectNo(Long value) {
        this.objectNo = value;
    }

    /**
     * Gets the value of the objectSeqNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getObjectSeqNo() {
        return objectSeqNo;
    }

    /**
     * Sets the value of the objectSeqNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setObjectSeqNo(Long value) {
        this.objectSeqNo = value;
    }

    /**
     * Gets the value of the objectId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getObjectId() {
        return objectId;
    }

    /**
     * Sets the value of the objectId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setObjectId(String value) {
        this.objectId = value;
    }

    /**
     * Gets the value of the statCode1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatCode1() {
        return statCode1;
    }

    /**
     * Sets the value of the statCode1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatCode1(String value) {
        this.statCode1 = value;
    }

    /**
     * Gets the value of the statCode2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatCode2() {
        return statCode2;
    }

    /**
     * Sets the value of the statCode2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatCode2(String value) {
        this.statCode2 = value;
    }

    /**
     * Gets the value of the statCode3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatCode3() {
        return statCode3;
    }

    /**
     * Sets the value of the statCode3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatCode3(String value) {
        this.statCode3 = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

    /**
     * Gets the value of the firstOpenDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFirstOpenDate() {
        return firstOpenDate;
    }

    /**
     * Sets the value of the firstOpenDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFirstOpenDate(XMLGregorianCalendar value) {
        this.firstOpenDate = value;
    }

    /**
     * Gets the value of the firstCloseDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFirstCloseDate() {
        return firstCloseDate;
    }

    /**
     * Sets the value of the firstCloseDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFirstCloseDate(XMLGregorianCalendar value) {
        this.firstCloseDate = value;
    }

    /**
     * Gets the value of the reopenDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getReopenDate() {
        return reopenDate;
    }

    /**
     * Sets the value of the reopenDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setReopenDate(XMLGregorianCalendar value) {
        this.reopenDate = value;
    }

    /**
     * Gets the value of the reopenReason property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReopenReason() {
        return reopenReason;
    }

    /**
     * Sets the value of the reopenReason property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReopenReason(String value) {
        this.reopenReason = value;
    }

    /**
     * Gets the value of the recloseDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getRecloseDate() {
        return recloseDate;
    }

    /**
     * Sets the value of the recloseDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setRecloseDate(XMLGregorianCalendar value) {
        this.recloseDate = value;
    }

    /**
     * Gets the value of the hoursSpend property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getHoursSpend() {
        return hoursSpend;
    }

    /**
     * Sets the value of the hoursSpend property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setHoursSpend(BigDecimal value) {
        this.hoursSpend = value;
    }

    /**
     * Gets the value of the workgroup property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWorkgroup() {
        return workgroup;
    }

    /**
     * Sets the value of the workgroup property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWorkgroup(String value) {
        this.workgroup = value;
    }

    /**
     * Gets the value of the handler property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHandler() {
        return handler;
    }

    /**
     * Sets the value of the handler property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHandler(String value) {
        this.handler = value;
    }

    /**
     * Gets the value of the claimClass property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClaimClass() {
        return claimClass;
    }

    /**
     * Sets the value of the claimClass property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClaimClass(String value) {
        this.claimClass = value;
    }

    /**
     * Gets the value of the questionClass property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQuestionClass() {
        return questionClass;
    }

    /**
     * Sets the value of the questionClass property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQuestionClass(String value) {
        this.questionClass = value;
    }

    /**
     * Gets the value of the questionStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQuestionStatus() {
        return questionStatus;
    }

    /**
     * Sets the value of the questionStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQuestionStatus(String value) {
        this.questionStatus = value;
    }

    /**
     * Gets the value of the tasklistClass property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTasklistClass() {
        return tasklistClass;
    }

    /**
     * Sets the value of the tasklistClass property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTasklistClass(String value) {
        this.tasklistClass = value;
    }

    /**
     * Gets the value of the itemClass property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItemClass() {
        return itemClass;
    }

    /**
     * Sets the value of the itemClass property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItemClass(String value) {
        this.itemClass = value;
    }

    /**
     * Gets the value of the referralCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReferralCode() {
        return referralCode;
    }

    /**
     * Sets the value of the referralCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReferralCode(String value) {
        this.referralCode = value;
    }

    /**
     * Gets the value of the scorecardScore property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getScorecardScore() {
        return scorecardScore;
    }

    /**
     * Sets the value of the scorecardScore property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setScorecardScore(BigDecimal value) {
        this.scorecardScore = value;
    }

    /**
     * Gets the value of the categoryResult property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCategoryResult() {
        return categoryResult;
    }

    /**
     * Sets the value of the categoryResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCategoryResult(String value) {
        this.categoryResult = value;
    }

    /**
     * Gets the value of the assignProfileId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAssignProfileId() {
        return assignProfileId;
    }

    /**
     * Sets the value of the assignProfileId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAssignProfileId(String value) {
        this.assignProfileId = value;
    }

    /**
     * Gets the value of the answerSetCollection property.
     * 
     * @return
     *     possible object is
     *     {@link AnswerSetCollection }
     *     
     */
    public AnswerSetCollection getAnswerSetCollection() {
        return answerSetCollection;
    }

    /**
     * Sets the value of the answerSetCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link AnswerSetCollection }
     *     
     */
    public void setAnswerSetCollection(AnswerSetCollection value) {
        this.answerSetCollection = value;
    }

    /**
     * Gets the value of the claimItemCollection property.
     * 
     * @return
     *     possible object is
     *     {@link ClaimItemCollection }
     *     
     */
    public ClaimItemCollection getClaimItemCollection() {
        return claimItemCollection;
    }

    /**
     * Sets the value of the claimItemCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link ClaimItemCollection }
     *     
     */
    public void setClaimItemCollection(ClaimItemCollection value) {
        this.claimItemCollection = value;
    }

    /**
     * Gets the value of the claimTaskCollection property.
     * 
     * @return
     *     possible object is
     *     {@link ClaimTaskCollection }
     *     
     */
    public ClaimTaskCollection getClaimTaskCollection() {
        return claimTaskCollection;
    }

    /**
     * Sets the value of the claimTaskCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link ClaimTaskCollection }
     *     
     */
    public void setClaimTaskCollection(ClaimTaskCollection value) {
        this.claimTaskCollection = value;
    }

    /**
     * Gets the value of the referralCollection property.
     * 
     * @return
     *     possible object is
     *     {@link ClaimReferralCollection }
     *     
     */
    public ClaimReferralCollection getReferralCollection() {
        return referralCollection;
    }

    /**
     * Sets the value of the referralCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link ClaimReferralCollection }
     *     
     */
    public void setReferralCollection(ClaimReferralCollection value) {
        this.referralCollection = value;
    }

}
