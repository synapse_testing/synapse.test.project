
package za.co.ominsure.synapse.iwyze.lob.backend.policy.vo;

import java.math.BigDecimal;
import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Is mapped into TIA package P1100".
 * 
 * <p>Java class for Policy complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Policy">
 *   &lt;complexContent>
 *     &lt;extension base="{http://infrastructure.tia.dk/schema/common/v2/}TIAObject">
 *       &lt;sequence>
 *         &lt;element name="policyHolderId" type="{http://infrastructure.tia.dk/schema/policy/v2/}seqNo" minOccurs="0"/>
 *         &lt;element name="policyNo" type="{http://infrastructure.tia.dk/schema/common/v2/}policyNo" minOccurs="0"/>
 *         &lt;element name="policySeqNo" type="{http://infrastructure.tia.dk/schema/policy/v2/}seqNo" minOccurs="0"/>
 *         &lt;element name="prodId" type="{http://infrastructure.tia.dk/schema/policy/v2/}prodId" minOccurs="0"/>
 *         &lt;element name="policyStatus" type="{http://infrastructure.tia.dk/schema/policy/v2/}policyStatus" minOccurs="0"/>
 *         &lt;element name="cancelCode" type="{http://infrastructure.tia.dk/schema/policy/v2/}cancelCode" minOccurs="0"/>
 *         &lt;element name="referToUnderwriter" type="{http://infrastructure.tia.dk/schema/policy/v2/}referToUnderwriter" minOccurs="0"/>
 *         &lt;element name="transactionType" type="{http://infrastructure.tia.dk/schema/policy/v2/}transactionType" minOccurs="0"/>
 *         &lt;element name="newest" type="{http://infrastructure.tia.dk/schema/policy/v2/}newest" minOccurs="0"/>
 *         &lt;element name="coverStartDate" type="{http://infrastructure.tia.dk/schema/common/v2/}date" minOccurs="0"/>
 *         &lt;element name="coverEndDate" type="{http://infrastructure.tia.dk/schema/common/v2/}date" minOccurs="0"/>
 *         &lt;element name="renewalDate" type="{http://infrastructure.tia.dk/schema/common/v2/}date" minOccurs="0"/>
 *         &lt;element name="policyYear" type="{http://infrastructure.tia.dk/schema/policy/v2/}policyYear" minOccurs="0"/>
 *         &lt;element name="firstStartDate" type="{http://infrastructure.tia.dk/schema/common/v2/}date" minOccurs="0"/>
 *         &lt;element name="yearStartDate" type="{http://infrastructure.tia.dk/schema/common/v2/}date" minOccurs="0"/>
 *         &lt;element name="cancelDate" type="{http://infrastructure.tia.dk/schema/common/v2/}date" minOccurs="0"/>
 *         &lt;element name="quoteExpiryDate" type="{http://infrastructure.tia.dk/schema/common/v2/}date" minOccurs="0"/>
 *         &lt;element name="policyNoAlt" type="{http://infrastructure.tia.dk/schema/policy/v2/}policyNoAlt" minOccurs="0"/>
 *         &lt;element name="preSeqNo" type="{http://infrastructure.tia.dk/schema/policy/v2/}seqNo" minOccurs="0"/>
 *         &lt;element name="sucSeqNo" type="{http://infrastructure.tia.dk/schema/policy/v2/}seqNo" minOccurs="0"/>
 *         &lt;element name="transId" type="{http://infrastructure.tia.dk/schema/policy/v2/}seqNo" minOccurs="0"/>
 *         &lt;element name="mtaPreSeqNo" type="{http://infrastructure.tia.dk/schema/policy/v2/}seqNo" minOccurs="0"/>
 *         &lt;element name="oldPolEndDate" type="{http://infrastructure.tia.dk/schema/common/v2/}date" minOccurs="0"/>
 *         &lt;element name="prevInsurer" type="{http://infrastructure.tia.dk/schema/policy/v2/}prevInsurer" minOccurs="0"/>
 *         &lt;element name="discountType" type="{http://infrastructure.tia.dk/schema/common/v2/}tiaYN" minOccurs="0"/>
 *         &lt;element name="discountAmount" type="{http://infrastructure.tia.dk/schema/policy/v2/}discountAmount" minOccurs="0"/>
 *         &lt;element name="discountPct" type="{http://infrastructure.tia.dk/schema/policy/v2/}policyPct" minOccurs="0"/>
 *         &lt;element name="expiryCode" type="{http://infrastructure.tia.dk/schema/policy/v2/}expiryCode" minOccurs="0"/>
 *         &lt;element name="lapseCode" type="{http://infrastructure.tia.dk/schema/policy/v2/}lapseCode" minOccurs="0"/>
 *         &lt;element name="policyAutRenewal" type="{http://infrastructure.tia.dk/schema/policy/v2/}policyAutRenewal" minOccurs="0"/>
 *         &lt;element name="paymentFrequency" type="{http://infrastructure.tia.dk/schema/common/v2/}paymentFrequency" minOccurs="0"/>
 *         &lt;element name="paymentMethod" type="{http://infrastructure.tia.dk/schema/common/v2/}paymentMethod" minOccurs="0"/>
 *         &lt;element name="paymentDetailsId" type="{http://infrastructure.tia.dk/schema/common/v2/}seqNo" minOccurs="0"/>
 *         &lt;element name="instalmentMethod" type="{http://infrastructure.tia.dk/schema/common/v2/}instalmentMethod" minOccurs="0"/>
 *         &lt;element name="instlPlanType" type="{http://infrastructure.tia.dk/schema/common/v2/}instlPlanType" minOccurs="0"/>
 *         &lt;element name="accountNo" type="{http://infrastructure.tia.dk/schema/common/v2/}seqNo" minOccurs="0"/>
 *         &lt;element name="centerCode" type="{http://infrastructure.tia.dk/schema/common/v2/}centerCode" minOccurs="0"/>
 *         &lt;element name="policyNoSort" type="{http://infrastructure.tia.dk/schema/policy/v2/}policyNoSort" minOccurs="0"/>
 *         &lt;element name="campaign" type="{http://infrastructure.tia.dk/schema/policy/v2/}campaign" minOccurs="0"/>
 *         &lt;element name="responseType" type="{http://infrastructure.tia.dk/schema/policy/v2/}responseType" minOccurs="0"/>
 *         &lt;element name="markNo" type="{http://infrastructure.tia.dk/schema/policy/v2/}markNo" minOccurs="0"/>
 *         &lt;element name="handler" type="{http://infrastructure.tia.dk/schema/policy/v2/}owner" minOccurs="0"/>
 *         &lt;element name="transferPremiumFlag" type="{http://infrastructure.tia.dk/schema/policy/v2/}transferPremiumFlag" minOccurs="0"/>
 *         &lt;element name="referredAmount" type="{http://infrastructure.tia.dk/schema/policy/v2/}policyAmount" minOccurs="0"/>
 *         &lt;element name="takenByUw" type="{http://infrastructure.tia.dk/schema/policy/v2/}owner" minOccurs="0"/>
 *         &lt;element name="documentDate" type="{http://infrastructure.tia.dk/schema/common/v2/}date" minOccurs="0"/>
 *         &lt;element name="forceGlobalMta" type="{http://infrastructure.tia.dk/schema/common/v2/}tiaYN" minOccurs="0"/>
 *         &lt;element name="notes" type="{http://infrastructure.tia.dk/schema/policy/v2/}notes" minOccurs="0"/>
 *         &lt;element name="mp" type="{http://infrastructure.tia.dk/schema/common/v2/}string1" minOccurs="0"/>
 *         &lt;element name="mpPolicyNo" type="{http://infrastructure.tia.dk/schema/common/v2/}long10" minOccurs="0"/>
 *         &lt;element name="mpTransId" type="{http://infrastructure.tia.dk/schema/common/v2/}long10" minOccurs="0"/>
 *         &lt;element name="policyNoQop" type="{http://infrastructure.tia.dk/schema/common/v2/}long10" minOccurs="0"/>
 *         &lt;element name="seqNoQop" type="{http://infrastructure.tia.dk/schema/common/v2/}long10" minOccurs="0"/>
 *         &lt;element name="maxTransId" type="{http://infrastructure.tia.dk/schema/common/v2/}long10" minOccurs="0"/>
 *         &lt;element name="policyAgentCollection" type="{http://infrastructure.tia.dk/schema/policy/v2/}PolicyAgentCollection" minOccurs="0"/>
 *         &lt;element name="policyLineCollection" type="{http://infrastructure.tia.dk/schema/policy/v3/}PolicyLineCollection" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Policy", namespace = "http://infrastructure.tia.dk/schema/policy/v3/", propOrder = {
    "policyHolderId",
    "policyNo",
    "policySeqNo",
    "prodId",
    "policyStatus",
    "cancelCode",
    "referToUnderwriter",
    "transactionType",
    "newest",
    "coverStartDate",
    "coverEndDate",
    "renewalDate",
    "policyYear",
    "firstStartDate",
    "yearStartDate",
    "cancelDate",
    "quoteExpiryDate",
    "policyNoAlt",
    "preSeqNo",
    "sucSeqNo",
    "transId",
    "mtaPreSeqNo",
    "oldPolEndDate",
    "prevInsurer",
    "discountType",
    "discountAmount",
    "discountPct",
    "expiryCode",
    "lapseCode",
    "policyAutRenewal",
    "paymentFrequency",
    "paymentMethod",
    "paymentDetailsId",
    "instalmentMethod",
    "instlPlanType",
    "accountNo",
    "centerCode",
    "policyNoSort",
    "campaign",
    "responseType",
    "markNo",
    "handler",
    "transferPremiumFlag",
    "referredAmount",
    "takenByUw",
    "documentDate",
    "forceGlobalMta",
    "notes",
    "mp",
    "mpPolicyNo",
    "mpTransId",
    "policyNoQop",
    "seqNoQop",
    "maxTransId",
    "policyAgentCollection",
    "policyLineCollection"
})
public class Policy3
    extends TIAObject
{

    @XmlElement(nillable = true)
    @XmlSchemaType(name = "unsignedLong")
    protected BigInteger policyHolderId;
    @XmlSchemaType(name = "unsignedLong")
    protected BigInteger policyNo;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "unsignedLong")
    protected BigInteger policySeqNo;
    protected String prodId;
    @XmlElement(nillable = true)
    protected String policyStatus;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "unsignedInt")
    protected Long cancelCode;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "unsignedInt")
    protected Long referToUnderwriter;
    @XmlElement(nillable = true)
    protected String transactionType;
    @XmlElement(nillable = true)
    protected String newest;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar coverStartDate;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar coverEndDate;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar renewalDate;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "unsignedInt")
    protected Long policyYear;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar firstStartDate;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar yearStartDate;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar cancelDate;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar quoteExpiryDate;
    @XmlElement(nillable = true)
    protected String policyNoAlt;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "unsignedLong")
    protected BigInteger preSeqNo;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "unsignedLong")
    protected BigInteger sucSeqNo;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "unsignedLong")
    protected BigInteger transId;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "unsignedLong")
    protected BigInteger mtaPreSeqNo;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar oldPolEndDate;
    @XmlElement(nillable = true)
    protected String prevInsurer;
    protected String discountType;
    @XmlElement(nillable = true)
    protected BigDecimal discountAmount;
    @XmlElement(nillable = true)
    protected BigDecimal discountPct;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "unsignedInt")
    protected Long expiryCode;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "unsignedInt")
    protected Long lapseCode;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "unsignedInt")
    protected Long policyAutRenewal;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "unsignedInt")
    protected Long paymentFrequency;
    @XmlElement(nillable = true)
    protected String paymentMethod;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "unsignedLong")
    protected Long paymentDetailsId;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "unsignedInt")
    protected Long instalmentMethod;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "unsignedInt")
    protected Long instlPlanType;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "unsignedLong")
    protected Long accountNo;
    @XmlElement(nillable = true)
    protected String centerCode;
    @XmlElement(nillable = true)
    protected String policyNoSort;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "unsignedInt")
    protected Long campaign;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "unsignedInt")
    protected Long responseType;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "unsignedLong")
    protected BigInteger markNo;
    @XmlElement(nillable = true)
    protected String handler;
    @XmlElement(nillable = true)
    protected String transferPremiumFlag;
    @XmlElement(nillable = true)
    protected BigDecimal referredAmount;
    @XmlElement(nillable = true)
    protected String takenByUw;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar documentDate;
    @XmlElement(nillable = true)
    protected String forceGlobalMta;
    @XmlElement(nillable = true)
    protected String notes;
    @XmlElement(nillable = true)
    protected String mp;
    @XmlElement(nillable = true)
    protected Long mpPolicyNo;
    @XmlElement(nillable = true)
    protected Long mpTransId;
    @XmlElement(nillable = true)
    protected Long policyNoQop;
    @XmlElement(nillable = true)
    protected Long seqNoQop;
    @XmlElement(nillable = true)
    protected Long maxTransId;
    protected PolicyAgentCollection2 policyAgentCollection;
    protected PolicyLineCollection3 policyLineCollection;

    /**
     * Gets the value of the policyHolderId property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getPolicyHolderId() {
        return policyHolderId;
    }

    /**
     * Sets the value of the policyHolderId property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setPolicyHolderId(BigInteger value) {
        this.policyHolderId = value;
    }

    /**
     * Gets the value of the policyNo property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getPolicyNo() {
        return policyNo;
    }

    /**
     * Sets the value of the policyNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setPolicyNo(BigInteger value) {
        this.policyNo = value;
    }

    /**
     * Gets the value of the policySeqNo property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getPolicySeqNo() {
        return policySeqNo;
    }

    /**
     * Sets the value of the policySeqNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setPolicySeqNo(BigInteger value) {
        this.policySeqNo = value;
    }

    /**
     * Gets the value of the prodId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProdId() {
        return prodId;
    }

    /**
     * Sets the value of the prodId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProdId(String value) {
        this.prodId = value;
    }

    /**
     * Gets the value of the policyStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPolicyStatus() {
        return policyStatus;
    }

    /**
     * Sets the value of the policyStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPolicyStatus(String value) {
        this.policyStatus = value;
    }

    /**
     * Gets the value of the cancelCode property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getCancelCode() {
        return cancelCode;
    }

    /**
     * Sets the value of the cancelCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setCancelCode(Long value) {
        this.cancelCode = value;
    }

    /**
     * Gets the value of the referToUnderwriter property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getReferToUnderwriter() {
        return referToUnderwriter;
    }

    /**
     * Sets the value of the referToUnderwriter property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setReferToUnderwriter(Long value) {
        this.referToUnderwriter = value;
    }

    /**
     * Gets the value of the transactionType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionType() {
        return transactionType;
    }

    /**
     * Sets the value of the transactionType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionType(String value) {
        this.transactionType = value;
    }

    /**
     * Gets the value of the newest property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNewest() {
        return newest;
    }

    /**
     * Sets the value of the newest property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNewest(String value) {
        this.newest = value;
    }

    /**
     * Gets the value of the coverStartDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCoverStartDate() {
        return coverStartDate;
    }

    /**
     * Sets the value of the coverStartDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCoverStartDate(XMLGregorianCalendar value) {
        this.coverStartDate = value;
    }

    /**
     * Gets the value of the coverEndDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCoverEndDate() {
        return coverEndDate;
    }

    /**
     * Sets the value of the coverEndDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCoverEndDate(XMLGregorianCalendar value) {
        this.coverEndDate = value;
    }

    /**
     * Gets the value of the renewalDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getRenewalDate() {
        return renewalDate;
    }

    /**
     * Sets the value of the renewalDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setRenewalDate(XMLGregorianCalendar value) {
        this.renewalDate = value;
    }

    /**
     * Gets the value of the policyYear property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getPolicyYear() {
        return policyYear;
    }

    /**
     * Sets the value of the policyYear property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setPolicyYear(Long value) {
        this.policyYear = value;
    }

    /**
     * Gets the value of the firstStartDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFirstStartDate() {
        return firstStartDate;
    }

    /**
     * Sets the value of the firstStartDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFirstStartDate(XMLGregorianCalendar value) {
        this.firstStartDate = value;
    }

    /**
     * Gets the value of the yearStartDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getYearStartDate() {
        return yearStartDate;
    }

    /**
     * Sets the value of the yearStartDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setYearStartDate(XMLGregorianCalendar value) {
        this.yearStartDate = value;
    }

    /**
     * Gets the value of the cancelDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCancelDate() {
        return cancelDate;
    }

    /**
     * Sets the value of the cancelDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCancelDate(XMLGregorianCalendar value) {
        this.cancelDate = value;
    }

    /**
     * Gets the value of the quoteExpiryDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getQuoteExpiryDate() {
        return quoteExpiryDate;
    }

    /**
     * Sets the value of the quoteExpiryDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setQuoteExpiryDate(XMLGregorianCalendar value) {
        this.quoteExpiryDate = value;
    }

    /**
     * Gets the value of the policyNoAlt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPolicyNoAlt() {
        return policyNoAlt;
    }

    /**
     * Sets the value of the policyNoAlt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPolicyNoAlt(String value) {
        this.policyNoAlt = value;
    }

    /**
     * Gets the value of the preSeqNo property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getPreSeqNo() {
        return preSeqNo;
    }

    /**
     * Sets the value of the preSeqNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setPreSeqNo(BigInteger value) {
        this.preSeqNo = value;
    }

    /**
     * Gets the value of the sucSeqNo property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getSucSeqNo() {
        return sucSeqNo;
    }

    /**
     * Sets the value of the sucSeqNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setSucSeqNo(BigInteger value) {
        this.sucSeqNo = value;
    }

    /**
     * Gets the value of the transId property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getTransId() {
        return transId;
    }

    /**
     * Sets the value of the transId property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setTransId(BigInteger value) {
        this.transId = value;
    }

    /**
     * Gets the value of the mtaPreSeqNo property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getMtaPreSeqNo() {
        return mtaPreSeqNo;
    }

    /**
     * Sets the value of the mtaPreSeqNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setMtaPreSeqNo(BigInteger value) {
        this.mtaPreSeqNo = value;
    }

    /**
     * Gets the value of the oldPolEndDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getOldPolEndDate() {
        return oldPolEndDate;
    }

    /**
     * Sets the value of the oldPolEndDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setOldPolEndDate(XMLGregorianCalendar value) {
        this.oldPolEndDate = value;
    }

    /**
     * Gets the value of the prevInsurer property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrevInsurer() {
        return prevInsurer;
    }

    /**
     * Sets the value of the prevInsurer property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrevInsurer(String value) {
        this.prevInsurer = value;
    }

    /**
     * Gets the value of the discountType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDiscountType() {
        return discountType;
    }

    /**
     * Sets the value of the discountType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDiscountType(String value) {
        this.discountType = value;
    }

    /**
     * Gets the value of the discountAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDiscountAmount() {
        return discountAmount;
    }

    /**
     * Sets the value of the discountAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDiscountAmount(BigDecimal value) {
        this.discountAmount = value;
    }

    /**
     * Gets the value of the discountPct property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDiscountPct() {
        return discountPct;
    }

    /**
     * Sets the value of the discountPct property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDiscountPct(BigDecimal value) {
        this.discountPct = value;
    }

    /**
     * Gets the value of the expiryCode property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getExpiryCode() {
        return expiryCode;
    }

    /**
     * Sets the value of the expiryCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setExpiryCode(Long value) {
        this.expiryCode = value;
    }

    /**
     * Gets the value of the lapseCode property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getLapseCode() {
        return lapseCode;
    }

    /**
     * Sets the value of the lapseCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setLapseCode(Long value) {
        this.lapseCode = value;
    }

    /**
     * Gets the value of the policyAutRenewal property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getPolicyAutRenewal() {
        return policyAutRenewal;
    }

    /**
     * Sets the value of the policyAutRenewal property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setPolicyAutRenewal(Long value) {
        this.policyAutRenewal = value;
    }

    /**
     * Gets the value of the paymentFrequency property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getPaymentFrequency() {
        return paymentFrequency;
    }

    /**
     * Sets the value of the paymentFrequency property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setPaymentFrequency(Long value) {
        this.paymentFrequency = value;
    }

    /**
     * Gets the value of the paymentMethod property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaymentMethod() {
        return paymentMethod;
    }

    /**
     * Sets the value of the paymentMethod property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaymentMethod(String value) {
        this.paymentMethod = value;
    }

    /**
     * Gets the value of the paymentDetailsId property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getPaymentDetailsId() {
        return paymentDetailsId;
    }

    /**
     * Sets the value of the paymentDetailsId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setPaymentDetailsId(Long value) {
        this.paymentDetailsId = value;
    }

    /**
     * Gets the value of the instalmentMethod property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getInstalmentMethod() {
        return instalmentMethod;
    }

    /**
     * Sets the value of the instalmentMethod property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setInstalmentMethod(Long value) {
        this.instalmentMethod = value;
    }

    /**
     * Gets the value of the instlPlanType property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getInstlPlanType() {
        return instlPlanType;
    }

    /**
     * Sets the value of the instlPlanType property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setInstlPlanType(Long value) {
        this.instlPlanType = value;
    }

    /**
     * Gets the value of the accountNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getAccountNo() {
        return accountNo;
    }

    /**
     * Sets the value of the accountNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setAccountNo(Long value) {
        this.accountNo = value;
    }

    /**
     * Gets the value of the centerCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCenterCode() {
        return centerCode;
    }

    /**
     * Sets the value of the centerCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCenterCode(String value) {
        this.centerCode = value;
    }

    /**
     * Gets the value of the policyNoSort property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPolicyNoSort() {
        return policyNoSort;
    }

    /**
     * Sets the value of the policyNoSort property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPolicyNoSort(String value) {
        this.policyNoSort = value;
    }

    /**
     * Gets the value of the campaign property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getCampaign() {
        return campaign;
    }

    /**
     * Sets the value of the campaign property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setCampaign(Long value) {
        this.campaign = value;
    }

    /**
     * Gets the value of the responseType property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getResponseType() {
        return responseType;
    }

    /**
     * Sets the value of the responseType property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setResponseType(Long value) {
        this.responseType = value;
    }

    /**
     * Gets the value of the markNo property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getMarkNo() {
        return markNo;
    }

    /**
     * Sets the value of the markNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setMarkNo(BigInteger value) {
        this.markNo = value;
    }

    /**
     * Gets the value of the handler property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHandler() {
        return handler;
    }

    /**
     * Sets the value of the handler property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHandler(String value) {
        this.handler = value;
    }

    /**
     * Gets the value of the transferPremiumFlag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransferPremiumFlag() {
        return transferPremiumFlag;
    }

    /**
     * Sets the value of the transferPremiumFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransferPremiumFlag(String value) {
        this.transferPremiumFlag = value;
    }

    /**
     * Gets the value of the referredAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getReferredAmount() {
        return referredAmount;
    }

    /**
     * Sets the value of the referredAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setReferredAmount(BigDecimal value) {
        this.referredAmount = value;
    }

    /**
     * Gets the value of the takenByUw property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTakenByUw() {
        return takenByUw;
    }

    /**
     * Sets the value of the takenByUw property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTakenByUw(String value) {
        this.takenByUw = value;
    }

    /**
     * Gets the value of the documentDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDocumentDate() {
        return documentDate;
    }

    /**
     * Sets the value of the documentDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDocumentDate(XMLGregorianCalendar value) {
        this.documentDate = value;
    }

    /**
     * Gets the value of the forceGlobalMta property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getForceGlobalMta() {
        return forceGlobalMta;
    }

    /**
     * Sets the value of the forceGlobalMta property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setForceGlobalMta(String value) {
        this.forceGlobalMta = value;
    }

    /**
     * Gets the value of the notes property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNotes() {
        return notes;
    }

    /**
     * Sets the value of the notes property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNotes(String value) {
        this.notes = value;
    }

    /**
     * Gets the value of the mp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMp() {
        return mp;
    }

    /**
     * Sets the value of the mp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMp(String value) {
        this.mp = value;
    }

    /**
     * Gets the value of the mpPolicyNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getMpPolicyNo() {
        return mpPolicyNo;
    }

    /**
     * Sets the value of the mpPolicyNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setMpPolicyNo(Long value) {
        this.mpPolicyNo = value;
    }

    /**
     * Gets the value of the mpTransId property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getMpTransId() {
        return mpTransId;
    }

    /**
     * Sets the value of the mpTransId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setMpTransId(Long value) {
        this.mpTransId = value;
    }

    /**
     * Gets the value of the policyNoQop property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getPolicyNoQop() {
        return policyNoQop;
    }

    /**
     * Sets the value of the policyNoQop property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setPolicyNoQop(Long value) {
        this.policyNoQop = value;
    }

    /**
     * Gets the value of the seqNoQop property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getSeqNoQop() {
        return seqNoQop;
    }

    /**
     * Sets the value of the seqNoQop property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setSeqNoQop(Long value) {
        this.seqNoQop = value;
    }

    /**
     * Gets the value of the maxTransId property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getMaxTransId() {
        return maxTransId;
    }

    /**
     * Sets the value of the maxTransId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setMaxTransId(Long value) {
        this.maxTransId = value;
    }

    /**
     * Gets the value of the policyAgentCollection property.
     * 
     * @return
     *     possible object is
     *     {@link PolicyAgentCollection2 }
     *     
     */
    public PolicyAgentCollection2 getPolicyAgentCollection() {
        return policyAgentCollection;
    }

    /**
     * Sets the value of the policyAgentCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link PolicyAgentCollection2 }
     *     
     */
    public void setPolicyAgentCollection(PolicyAgentCollection2 value) {
        this.policyAgentCollection = value;
    }

    /**
     * Gets the value of the policyLineCollection property.
     * 
     * @return
     *     possible object is
     *     {@link PolicyLineCollection3 }
     *     
     */
    public PolicyLineCollection3 getPolicyLineCollection() {
        return policyLineCollection;
    }

    /**
     * Sets the value of the policyLineCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link PolicyLineCollection3 }
     *     
     */
    public void setPolicyLineCollection(PolicyLineCollection3 value) {
        this.policyLineCollection = value;
    }

}
