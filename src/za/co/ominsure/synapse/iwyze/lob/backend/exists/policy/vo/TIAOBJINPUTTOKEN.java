
package za.co.ominsure.synapse.iwyze.lob.backend.exists.policy.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TIA.OBJ_INPUT_TOKEN complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TIA.OBJ_INPUT_TOKEN">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="USER_ID" type="{http://xmlns.oracle.com/pcbpel/adapter/db/sp/checkExistingPolicyReference}string8" minOccurs="0"/>
 *         &lt;element name="LANGUAGE" type="{http://xmlns.oracle.com/pcbpel/adapter/db/sp/checkExistingPolicyReference}string3" minOccurs="0"/>
 *         &lt;element name="SITE_NAME" type="{http://xmlns.oracle.com/pcbpel/adapter/db/sp/checkExistingPolicyReference}string10" minOccurs="0"/>
 *         &lt;element name="TRACE_YN" type="{http://xmlns.oracle.com/pcbpel/adapter/db/sp/checkExistingPolicyReference}string1" minOccurs="0"/>
 *         &lt;element name="COMMIT_YN" type="{http://xmlns.oracle.com/pcbpel/adapter/db/sp/checkExistingPolicyReference}string1" minOccurs="0"/>
 *         &lt;element name="CORRELATION_ID" type="{http://xmlns.oracle.com/pcbpel/adapter/db/sp/checkExistingPolicyReference}string32" minOccurs="0"/>
 *         &lt;element name="TRACE_PROGRAMS" type="{http://xmlns.oracle.com/pcbpel/adapter/db/sp/checkExistingPolicyReference}TIA.TAB_VARCHAR2" minOccurs="0"/>
 *         &lt;element name="CLIENT_TYPE" type="{http://xmlns.oracle.com/pcbpel/adapter/db/sp/checkExistingPolicyReference}string30" minOccurs="0"/>
 *         &lt;element name="OPTIONS" type="{http://xmlns.oracle.com/pcbpel/adapter/db/sp/checkExistingPolicyReference}TIA.TAB_VARCHAR2" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TIA.OBJ_INPUT_TOKEN", propOrder = {
    "userid",
    "language",
    "sitename",
    "traceyn",
    "commityn",
    "correlationid",
    "traceprograms",
    "clienttype",
    "options"
})
public class TIAOBJINPUTTOKEN {

    @XmlElement(name = "USER_ID", nillable = true)
    protected String userid;
    @XmlElement(name = "LANGUAGE", nillable = true)
    protected String language;
    @XmlElement(name = "SITE_NAME", nillable = true)
    protected String sitename;
    @XmlElement(name = "TRACE_YN", nillable = true)
    protected String traceyn;
    @XmlElement(name = "COMMIT_YN", nillable = true)
    protected String commityn;
    @XmlElement(name = "CORRELATION_ID", nillable = true)
    protected String correlationid;
    @XmlElement(name = "TRACE_PROGRAMS", nillable = true)
    protected TIATABVARCHAR2 traceprograms;
    @XmlElement(name = "CLIENT_TYPE", nillable = true)
    protected String clienttype;
    @XmlElement(name = "OPTIONS", nillable = true)
    protected TIATABVARCHAR2 options;

    /**
     * Gets the value of the userid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUSERID() {
        return userid;
    }

    /**
     * Sets the value of the userid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUSERID(String value) {
        this.userid = value;
    }

    /**
     * Gets the value of the language property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLANGUAGE() {
        return language;
    }

    /**
     * Sets the value of the language property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLANGUAGE(String value) {
        this.language = value;
    }

    /**
     * Gets the value of the sitename property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSITENAME() {
        return sitename;
    }

    /**
     * Sets the value of the sitename property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSITENAME(String value) {
        this.sitename = value;
    }

    /**
     * Gets the value of the traceyn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTRACEYN() {
        return traceyn;
    }

    /**
     * Sets the value of the traceyn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTRACEYN(String value) {
        this.traceyn = value;
    }

    /**
     * Gets the value of the commityn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCOMMITYN() {
        return commityn;
    }

    /**
     * Sets the value of the commityn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCOMMITYN(String value) {
        this.commityn = value;
    }

    /**
     * Gets the value of the correlationid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCORRELATIONID() {
        return correlationid;
    }

    /**
     * Sets the value of the correlationid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCORRELATIONID(String value) {
        this.correlationid = value;
    }

    /**
     * Gets the value of the traceprograms property.
     * 
     * @return
     *     possible object is
     *     {@link TIATABVARCHAR2 }
     *     
     */
    public TIATABVARCHAR2 getTRACEPROGRAMS() {
        return traceprograms;
    }

    /**
     * Sets the value of the traceprograms property.
     * 
     * @param value
     *     allowed object is
     *     {@link TIATABVARCHAR2 }
     *     
     */
    public void setTRACEPROGRAMS(TIATABVARCHAR2 value) {
        this.traceprograms = value;
    }

    /**
     * Gets the value of the clienttype property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCLIENTTYPE() {
        return clienttype;
    }

    /**
     * Sets the value of the clienttype property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCLIENTTYPE(String value) {
        this.clienttype = value;
    }

    /**
     * Gets the value of the options property.
     * 
     * @return
     *     possible object is
     *     {@link TIATABVARCHAR2 }
     *     
     */
    public TIATABVARCHAR2 getOPTIONS() {
        return options;
    }

    /**
     * Sets the value of the options property.
     * 
     * @param value
     *     allowed object is
     *     {@link TIATABVARCHAR2 }
     *     
     */
    public void setOPTIONS(TIATABVARCHAR2 value) {
        this.options = value;
    }

}
