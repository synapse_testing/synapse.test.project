
package za.co.ominsure.synapse.iwyze.lob.backend.policy.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Can be mapped into TIA Commons type "obj_input_token ".
 * 
 * <p>Java class for InputToken complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="InputToken">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="userId" type="{http://infrastructure.tia.dk/schema/common/v2/}userId"/>
 *         &lt;element name="language" type="{http://infrastructure.tia.dk/schema/common/v2/}language" minOccurs="0"/>
 *         &lt;element name="siteName" type="{http://infrastructure.tia.dk/schema/common/v2/}string10" minOccurs="0"/>
 *         &lt;element name="traceOn" type="{http://infrastructure.tia.dk/schema/common/v2/}tiaYN"/>
 *         &lt;element name="commitOn" type="{http://infrastructure.tia.dk/schema/common/v2/}tiaYN"/>
 *         &lt;element name="correlationId" type="{http://infrastructure.tia.dk/schema/common/v2/}correlationId" minOccurs="0"/>
 *         &lt;element name="traceProgramCollection" type="{http://infrastructure.tia.dk/schema/common/v2/}StringCollection" minOccurs="0"/>
 *         &lt;element name="optionsCollection" type="{http://infrastructure.tia.dk/schema/common/v2/}StringCollection" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InputToken", namespace = "http://infrastructure.tia.dk/schema/common/v2/", propOrder = {
    "userId",
    "language",
    "siteName",
    "traceOn",
    "commitOn",
    "correlationId",
    "traceProgramCollection",
    "optionsCollection"
})
public class InputToken {

    @XmlElement(required = true)
    protected String userId;
    protected String language;
    protected String siteName;
    @XmlElement(required = true)
    protected String traceOn;
    @XmlElement(required = true)
    protected String commitOn;
    @XmlElement(nillable = true)
    protected String correlationId;
    protected StringCollection traceProgramCollection;
    protected StringCollection optionsCollection;

    /**
     * Gets the value of the userId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Sets the value of the userId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserId(String value) {
        this.userId = value;
    }

    /**
     * Gets the value of the language property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLanguage() {
        return language;
    }

    /**
     * Sets the value of the language property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLanguage(String value) {
        this.language = value;
    }

    /**
     * Gets the value of the siteName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSiteName() {
        return siteName;
    }

    /**
     * Sets the value of the siteName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSiteName(String value) {
        this.siteName = value;
    }

    /**
     * Gets the value of the traceOn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTraceOn() {
        return traceOn;
    }

    /**
     * Sets the value of the traceOn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTraceOn(String value) {
        this.traceOn = value;
    }

    /**
     * Gets the value of the commitOn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCommitOn() {
        return commitOn;
    }

    /**
     * Sets the value of the commitOn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCommitOn(String value) {
        this.commitOn = value;
    }

    /**
     * Gets the value of the correlationId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorrelationId() {
        return correlationId;
    }

    /**
     * Sets the value of the correlationId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorrelationId(String value) {
        this.correlationId = value;
    }

    /**
     * Gets the value of the traceProgramCollection property.
     * 
     * @return
     *     possible object is
     *     {@link StringCollection }
     *     
     */
    public StringCollection getTraceProgramCollection() {
        return traceProgramCollection;
    }

    /**
     * Sets the value of the traceProgramCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link StringCollection }
     *     
     */
    public void setTraceProgramCollection(StringCollection value) {
        this.traceProgramCollection = value;
    }

    /**
     * Gets the value of the optionsCollection property.
     * 
     * @return
     *     possible object is
     *     {@link StringCollection }
     *     
     */
    public StringCollection getOptionsCollection() {
        return optionsCollection;
    }

    /**
     * Sets the value of the optionsCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link StringCollection }
     *     
     */
    public void setOptionsCollection(StringCollection value) {
        this.optionsCollection = value;
    }

}
