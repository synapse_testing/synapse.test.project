
package za.co.ominsure.synapse.iwyze.lob.backend.claim.payment.vo;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TranslatedCodeCollection complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TranslatedCodeCollection">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="translatedCode" type="{http://infrastructure.tia.dk/schema/common/v2/}TranslatedCode" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TranslatedCodeCollection", namespace = "http://infrastructure.tia.dk/schema/common/v2/", propOrder = {
    "translatedCodes"
})
public class TranslatedCodeCollection {

    @XmlElement(name = "translatedCode")
    protected List<TranslatedCode> translatedCodes;

    /**
     * Gets the value of the translatedCodes property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the translatedCodes property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTranslatedCodes().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TranslatedCode }
     * 
     * 
     */
    public List<TranslatedCode> getTranslatedCodes() {
        if (translatedCodes == null) {
            translatedCodes = new ArrayList<TranslatedCode>();
        }
        return this.translatedCodes;
    }

}
