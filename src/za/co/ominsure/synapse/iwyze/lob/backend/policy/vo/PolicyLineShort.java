
package za.co.ominsure.synapse.iwyze.lob.backend.policy.vo;

import java.math.BigDecimal;
import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Is mapped into TIA package P1100".
 * 
 * <p>Java class for PolicyLineShort complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PolicyLineShort">
 *   &lt;complexContent>
 *     &lt;extension base="{http://infrastructure.tia.dk/schema/common/v2/}TIAObject">
 *       &lt;sequence>
 *         &lt;element name="policyLineSeqNo" type="{http://infrastructure.tia.dk/schema/common/v2/}policyLineSeqNo"/>
 *         &lt;element name="coverStartDate" type="{http://infrastructure.tia.dk/schema/common/v2/}date"/>
 *         &lt;element name="coverEndDate" type="{http://infrastructure.tia.dk/schema/common/v2/}date"/>
 *         &lt;element name="renewalDate" type="{http://infrastructure.tia.dk/schema/common/v2/}date"/>
 *         &lt;element name="transCode" type="{http://infrastructure.tia.dk/schema/policy/v2/}transCode"/>
 *         &lt;element name="policyYear" type="{http://infrastructure.tia.dk/schema/policy/v2/}policyYear"/>
 *         &lt;element name="cancelCode" type="{http://infrastructure.tia.dk/schema/policy/v2/}cancelCode"/>
 *         &lt;element name="newest" type="{http://infrastructure.tia.dk/schema/policy/v2/}newest"/>
 *         &lt;element name="pricePaid" type="{http://infrastructure.tia.dk/schema/policy/v2/}policyAmount"/>
 *         &lt;element name="currencyCode" type="{http://infrastructure.tia.dk/schema/policy/v2/}currencyCode"/>
 *         &lt;element name="preSeqNo" type="{http://infrastructure.tia.dk/schema/policy/v2/}seqNo"/>
 *         &lt;element name="sucSeqNo" type="{http://infrastructure.tia.dk/schema/policy/v2/}seqNo"/>
 *         &lt;element name="transId" type="{http://infrastructure.tia.dk/schema/policy/v2/}seqNo" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PolicyLineShort", propOrder = {
    "policyLineSeqNo",
    "coverStartDate",
    "coverEndDate",
    "renewalDate",
    "transCode",
    "policyYear",
    "cancelCode",
    "newest",
    "pricePaid",
    "currencyCode",
    "preSeqNo",
    "sucSeqNo",
    "transId"
})
public class PolicyLineShort
    extends TIAObject
{

    @XmlElement(required = true, nillable = true)
    @XmlSchemaType(name = "unsignedLong")
    protected BigInteger policyLineSeqNo;
    @XmlElement(required = true, nillable = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar coverStartDate;
    @XmlElement(required = true, nillable = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar coverEndDate;
    @XmlElement(required = true, nillable = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar renewalDate;
    @XmlElement(required = true, type = Long.class, nillable = true)
    @XmlSchemaType(name = "unsignedInt")
    protected Long transCode;
    @XmlElement(required = true, type = Long.class, nillable = true)
    @XmlSchemaType(name = "unsignedInt")
    protected Long policyYear;
    @XmlElement(required = true, type = Long.class, nillable = true)
    @XmlSchemaType(name = "unsignedInt")
    protected Long cancelCode;
    @XmlElement(required = true, nillable = true)
    protected String newest;
    @XmlElement(required = true, nillable = true)
    protected BigDecimal pricePaid;
    @XmlElement(required = true, nillable = true)
    protected String currencyCode;
    @XmlElement(required = true, nillable = true)
    @XmlSchemaType(name = "unsignedLong")
    protected BigInteger preSeqNo;
    @XmlElement(required = true, nillable = true)
    @XmlSchemaType(name = "unsignedLong")
    protected BigInteger sucSeqNo;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "unsignedLong")
    protected BigInteger transId;

    /**
     * Gets the value of the policyLineSeqNo property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getPolicyLineSeqNo() {
        return policyLineSeqNo;
    }

    /**
     * Sets the value of the policyLineSeqNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setPolicyLineSeqNo(BigInteger value) {
        this.policyLineSeqNo = value;
    }

    /**
     * Gets the value of the coverStartDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCoverStartDate() {
        return coverStartDate;
    }

    /**
     * Sets the value of the coverStartDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCoverStartDate(XMLGregorianCalendar value) {
        this.coverStartDate = value;
    }

    /**
     * Gets the value of the coverEndDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCoverEndDate() {
        return coverEndDate;
    }

    /**
     * Sets the value of the coverEndDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCoverEndDate(XMLGregorianCalendar value) {
        this.coverEndDate = value;
    }

    /**
     * Gets the value of the renewalDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getRenewalDate() {
        return renewalDate;
    }

    /**
     * Sets the value of the renewalDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setRenewalDate(XMLGregorianCalendar value) {
        this.renewalDate = value;
    }

    /**
     * Gets the value of the transCode property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getTransCode() {
        return transCode;
    }

    /**
     * Sets the value of the transCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setTransCode(Long value) {
        this.transCode = value;
    }

    /**
     * Gets the value of the policyYear property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getPolicyYear() {
        return policyYear;
    }

    /**
     * Sets the value of the policyYear property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setPolicyYear(Long value) {
        this.policyYear = value;
    }

    /**
     * Gets the value of the cancelCode property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getCancelCode() {
        return cancelCode;
    }

    /**
     * Sets the value of the cancelCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setCancelCode(Long value) {
        this.cancelCode = value;
    }

    /**
     * Gets the value of the newest property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNewest() {
        return newest;
    }

    /**
     * Sets the value of the newest property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNewest(String value) {
        this.newest = value;
    }

    /**
     * Gets the value of the pricePaid property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPricePaid() {
        return pricePaid;
    }

    /**
     * Sets the value of the pricePaid property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPricePaid(BigDecimal value) {
        this.pricePaid = value;
    }

    /**
     * Gets the value of the currencyCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrencyCode() {
        return currencyCode;
    }

    /**
     * Sets the value of the currencyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrencyCode(String value) {
        this.currencyCode = value;
    }

    /**
     * Gets the value of the preSeqNo property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getPreSeqNo() {
        return preSeqNo;
    }

    /**
     * Sets the value of the preSeqNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setPreSeqNo(BigInteger value) {
        this.preSeqNo = value;
    }

    /**
     * Gets the value of the sucSeqNo property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getSucSeqNo() {
        return sucSeqNo;
    }

    /**
     * Sets the value of the sucSeqNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setSucSeqNo(BigInteger value) {
        this.sucSeqNo = value;
    }

    /**
     * Gets the value of the transId property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getTransId() {
        return transId;
    }

    /**
     * Sets the value of the transId property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setTransId(BigInteger value) {
        this.transId = value;
    }

}
