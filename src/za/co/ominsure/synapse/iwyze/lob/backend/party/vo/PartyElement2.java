
package za.co.ominsure.synapse.iwyze.lob.backend.party.vo;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * Level made in order to ensure only one collections is made".
 * 
 * <p>Java class for PartyElement complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PartyElement">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element name="person" type="{http://infrastructure.tia.dk/schema/party/v2/}Person" minOccurs="0"/>
 *         &lt;element name="institution" type="{http://infrastructure.tia.dk/schema/party/v2/}Institution" minOccurs="0"/>
 *         &lt;element name="partyOther" type="{http://infrastructure.tia.dk/schema/party/v2/}PartyOther" minOccurs="0"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PartyElement", propOrder = {
    "person",
    "institution",
    "partyOther"
})
public class PartyElement2 {

    @XmlElementRef(name = "person", namespace = "http://infrastructure.tia.dk/schema/party/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<Person2> person;
    @XmlElementRef(name = "institution", namespace = "http://infrastructure.tia.dk/schema/party/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<Institution2> institution;
    @XmlElementRef(name = "partyOther", namespace = "http://infrastructure.tia.dk/schema/party/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<PartyOther2> partyOther;

    /**
     * Gets the value of the person property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Person2 }{@code >}
     *     
     */
    public JAXBElement<Person2> getPerson() {
        return person;
    }

    /**
     * Sets the value of the person property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Person2 }{@code >}
     *     
     */
    public void setPerson(JAXBElement<Person2> value) {
        this.person = value;
    }

    /**
     * Gets the value of the institution property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Institution2 }{@code >}
     *     
     */
    public JAXBElement<Institution2> getInstitution() {
        return institution;
    }

    /**
     * Sets the value of the institution property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Institution2 }{@code >}
     *     
     */
    public void setInstitution(JAXBElement<Institution2> value) {
        this.institution = value;
    }

    /**
     * Gets the value of the partyOther property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link PartyOther2 }{@code >}
     *     
     */
    public JAXBElement<PartyOther2> getPartyOther() {
        return partyOther;
    }

    /**
     * Sets the value of the partyOther property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link PartyOther2 }{@code >}
     *     
     */
    public void setPartyOther(JAXBElement<PartyOther2> value) {
        this.partyOther = value;
    }

}
