
package za.co.ominsure.synapse.iwyze.lob.backend.detail.payment.vo;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Is mapped into TIA metadata type "obj_reminder_postponement".
 * 
 * <p>Java class for ReminderPostponement complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ReminderPostponement">
 *   &lt;complexContent>
 *     &lt;extension base="{http://infrastructure.tia.dk/schema/common/v2/}TIAObject">
 *       &lt;sequence>
 *         &lt;element name="postponementNo" type="{http://infrastructure.tia.dk/schema/common/v2/}seqNo" minOccurs="0"/>
 *         &lt;element name="postponeCode" type="{http://infrastructure.tia.dk/schema/account/v2/}postponeCode"/>
 *         &lt;element name="nameId" type="{http://infrastructure.tia.dk/schema/common/v2/}seqNo"/>
 *         &lt;element name="expiryDate" type="{http://infrastructure.tia.dk/schema/common/v2/}date"/>
 *         &lt;element name="receiverId" type="{http://infrastructure.tia.dk/schema/common/v2/}seqNo" minOccurs="0"/>
 *         &lt;element name="mpPolicyNo" type="{http://infrastructure.tia.dk/schema/common/v2/}long10" minOccurs="0"/>
 *         &lt;element name="policyNo" type="{http://infrastructure.tia.dk/schema/common/v2/}policyNo" minOccurs="0"/>
 *         &lt;element name="policyLineNo" type="{http://infrastructure.tia.dk/schema/common/v2/}policyLineNo" minOccurs="0"/>
 *         &lt;element name="claimNo" type="{http://infrastructure.tia.dk/schema/common/v2/}seqNo" minOccurs="0"/>
 *         &lt;element name="accountNo" type="{http://infrastructure.tia.dk/schema/common/v2/}seqNo" minOccurs="0"/>
 *         &lt;element name="accountItemNo" type="{http://infrastructure.tia.dk/schema/common/v2/}seqNo" minOccurs="0"/>
 *         &lt;element name="creator" type="{http://infrastructure.tia.dk/schema/common/v2/}userId" minOccurs="0"/>
 *         &lt;element name="createDate" type="{http://infrastructure.tia.dk/schema/common/v2/}timestamp" minOccurs="0"/>
 *         &lt;element name="note" type="{http://infrastructure.tia.dk/schema/account/v2/}note" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReminderPostponement", propOrder = {
    "postponementNo",
    "postponeCode",
    "nameId",
    "expiryDate",
    "receiverId",
    "mpPolicyNo",
    "policyNo",
    "policyLineNo",
    "claimNo",
    "accountNo",
    "accountItemNo",
    "creator",
    "createDate",
    "note"
})
public class ReminderPostponement
    extends TIAObject
{

    @XmlElement(nillable = true)
    @XmlSchemaType(name = "unsignedLong")
    protected Long postponementNo;
    @XmlSchemaType(name = "integer")
    protected int postponeCode;
    @XmlSchemaType(name = "unsignedLong")
    protected long nameId;
    @XmlElement(required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar expiryDate;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "unsignedLong")
    protected Long receiverId;
    @XmlElement(nillable = true)
    protected Long mpPolicyNo;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "unsignedLong")
    protected BigInteger policyNo;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "unsignedLong")
    protected BigInteger policyLineNo;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "unsignedLong")
    protected Long claimNo;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "unsignedLong")
    protected Long accountNo;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "unsignedLong")
    protected Long accountItemNo;
    @XmlElement(nillable = true)
    protected String creator;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar createDate;
    @XmlElement(nillable = true)
    protected String note;

    /**
     * Gets the value of the postponementNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getPostponementNo() {
        return postponementNo;
    }

    /**
     * Sets the value of the postponementNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setPostponementNo(Long value) {
        this.postponementNo = value;
    }

    /**
     * Gets the value of the postponeCode property.
     * 
     */
    public int getPostponeCode() {
        return postponeCode;
    }

    /**
     * Sets the value of the postponeCode property.
     * 
     */
    public void setPostponeCode(int value) {
        this.postponeCode = value;
    }

    /**
     * Gets the value of the nameId property.
     * 
     */
    public long getNameId() {
        return nameId;
    }

    /**
     * Sets the value of the nameId property.
     * 
     */
    public void setNameId(long value) {
        this.nameId = value;
    }

    /**
     * Gets the value of the expiryDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getExpiryDate() {
        return expiryDate;
    }

    /**
     * Sets the value of the expiryDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setExpiryDate(XMLGregorianCalendar value) {
        this.expiryDate = value;
    }

    /**
     * Gets the value of the receiverId property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getReceiverId() {
        return receiverId;
    }

    /**
     * Sets the value of the receiverId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setReceiverId(Long value) {
        this.receiverId = value;
    }

    /**
     * Gets the value of the mpPolicyNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getMpPolicyNo() {
        return mpPolicyNo;
    }

    /**
     * Sets the value of the mpPolicyNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setMpPolicyNo(Long value) {
        this.mpPolicyNo = value;
    }

    /**
     * Gets the value of the policyNo property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getPolicyNo() {
        return policyNo;
    }

    /**
     * Sets the value of the policyNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setPolicyNo(BigInteger value) {
        this.policyNo = value;
    }

    /**
     * Gets the value of the policyLineNo property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getPolicyLineNo() {
        return policyLineNo;
    }

    /**
     * Sets the value of the policyLineNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setPolicyLineNo(BigInteger value) {
        this.policyLineNo = value;
    }

    /**
     * Gets the value of the claimNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getClaimNo() {
        return claimNo;
    }

    /**
     * Sets the value of the claimNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setClaimNo(Long value) {
        this.claimNo = value;
    }

    /**
     * Gets the value of the accountNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getAccountNo() {
        return accountNo;
    }

    /**
     * Sets the value of the accountNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setAccountNo(Long value) {
        this.accountNo = value;
    }

    /**
     * Gets the value of the accountItemNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getAccountItemNo() {
        return accountItemNo;
    }

    /**
     * Sets the value of the accountItemNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setAccountItemNo(Long value) {
        this.accountItemNo = value;
    }

    /**
     * Gets the value of the creator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCreator() {
        return creator;
    }

    /**
     * Sets the value of the creator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCreator(String value) {
        this.creator = value;
    }

    /**
     * Gets the value of the createDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCreateDate() {
        return createDate;
    }

    /**
     * Sets the value of the createDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCreateDate(XMLGregorianCalendar value) {
        this.createDate = value;
    }

    /**
     * Gets the value of the note property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNote() {
        return note;
    }

    /**
     * Sets the value of the note property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNote(String value) {
        this.note = value;
    }

}
