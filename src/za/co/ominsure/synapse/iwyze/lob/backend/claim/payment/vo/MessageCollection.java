
package za.co.ominsure.synapse.iwyze.lob.backend.claim.payment.vo;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Definition for type tab_message which consists of obj_message
 * 
 * <p>Java class for MessageCollection complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MessageCollection">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="messageDetail" type="{http://infrastructure.tia.dk/schema/common/v2/}MessageDetail" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MessageCollection", namespace = "http://infrastructure.tia.dk/schema/common/v2/", propOrder = {
    "messageDetails"
})
public class MessageCollection {

    @XmlElement(name = "messageDetail")
    protected List<MessageDetail> messageDetails;

    /**
     * Gets the value of the messageDetails property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the messageDetails property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMessageDetails().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MessageDetail }
     * 
     * 
     */
    public List<MessageDetail> getMessageDetails() {
        if (messageDetails == null) {
            messageDetails = new ArrayList<MessageDetail>();
        }
        return this.messageDetails;
    }

}
