
package za.co.ominsure.synapse.iwyze.lob.backend.search.payment.vo;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ValueTranslationCollection complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ValueTranslationCollection">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="valueTranslation" type="{http://infrastructure.tia.dk/schema/common/v2/}ValueTranslation" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ValueTranslationCollection", namespace = "http://infrastructure.tia.dk/schema/common/v2/", propOrder = {
    "valueTranslation"
})
public class ValueTranslationCollection {

    @XmlElement(nillable = true)
    protected List<ValueTranslation> valueTranslation;

    /**
     * Gets the value of the valueTranslation property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the valueTranslation property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getValueTranslation().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ValueTranslation }
     * 
     * 
     */
    public List<ValueTranslation> getValueTranslation() {
        if (valueTranslation == null) {
            valueTranslation = new ArrayList<ValueTranslation>();
        }
        return this.valueTranslation;
    }

}
