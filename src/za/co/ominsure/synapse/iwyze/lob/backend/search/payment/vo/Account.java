
package za.co.ominsure.synapse.iwyze.lob.backend.search.payment.vo;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * Is mapped into TIA metadata type "obj_account".
 * 
 * <p>Java class for Account complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Account">
 *   &lt;complexContent>
 *     &lt;extension base="{http://infrastructure.tia.dk/schema/common/v2/}TIAObject">
 *       &lt;sequence>
 *         &lt;element name="accountNo" type="{http://infrastructure.tia.dk/schema/common/v2/}seqNo"/>
 *         &lt;element name="nameId" type="{http://infrastructure.tia.dk/schema/common/v2/}seqNo"/>
 *         &lt;element name="accountType" type="{http://infrastructure.tia.dk/schema/account/v2/}accountType"/>
 *         &lt;element name="locked" type="{http://infrastructure.tia.dk/schema/common/v2/}tiaYN" minOccurs="0"/>
 *         &lt;element name="currencyCode" type="{http://infrastructure.tia.dk/schema/common/v2/}currencyCode" minOccurs="0"/>
 *         &lt;element name="centerCode" type="{http://infrastructure.tia.dk/schema/common/v2/}centerCode" minOccurs="0"/>
 *         &lt;element name="receiverId" type="{http://infrastructure.tia.dk/schema/common/v2/}seqNo" minOccurs="0"/>
 *         &lt;element name="handler" type="{http://infrastructure.tia.dk/schema/common/v2/}userId" minOccurs="0"/>
 *         &lt;element name="reminderGroup" type="{http://infrastructure.tia.dk/schema/account/v2/}reminderGroup" minOccurs="0"/>
 *         &lt;element name="title" type="{http://infrastructure.tia.dk/schema/account/v2/}title" minOccurs="0"/>
 *         &lt;element name="collectionBy" type="{http://infrastructure.tia.dk/schema/account/v2/}collectionBy" minOccurs="0"/>
 *         &lt;element name="paymentTermsIn" type="{http://infrastructure.tia.dk/schema/account/v2/}paymentTermsType" minOccurs="0"/>
 *         &lt;element name="paymentMethodIn" type="{http://infrastructure.tia.dk/schema/common/v2/}paymentMethod" minOccurs="0"/>
 *         &lt;element name="meansPayNoIn" type="{http://infrastructure.tia.dk/schema/common/v2/}seqNo" minOccurs="0"/>
 *         &lt;element name="paymentTermsOut" type="{http://infrastructure.tia.dk/schema/account/v2/}paymentTermsType" minOccurs="0"/>
 *         &lt;element name="paymentMethodOut" type="{http://infrastructure.tia.dk/schema/common/v2/}paymentMethod" minOccurs="0"/>
 *         &lt;element name="meansPayNoOut" type="{http://infrastructure.tia.dk/schema/common/v2/}seqNo" minOccurs="0"/>
 *         &lt;element name="note" type="{http://infrastructure.tia.dk/schema/account/v2/}note" minOccurs="0"/>
 *         &lt;element name="accountItems" type="{http://infrastructure.tia.dk/schema/account/v2/}AccountItemCollection" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Account", propOrder = {
    "accountNo",
    "nameId",
    "accountType",
    "locked",
    "currencyCode",
    "centerCode",
    "receiverId",
    "handler",
    "reminderGroup",
    "title",
    "collectionBy",
    "paymentTermsIn",
    "paymentMethodIn",
    "meansPayNoIn",
    "paymentTermsOut",
    "paymentMethodOut",
    "meansPayNoOut",
    "note",
    "accountItems"
})
public class Account
    extends TIAObject
{

    @XmlSchemaType(name = "unsignedLong")
    protected long accountNo;
    @XmlSchemaType(name = "unsignedLong")
    protected long nameId;
    @XmlElement(required = true)
    protected String accountType;
    @XmlElementRef(name = "locked", namespace = "http://infrastructure.tia.dk/schema/account/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> locked;
    @XmlElementRef(name = "currencyCode", namespace = "http://infrastructure.tia.dk/schema/account/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> currencyCode;
    @XmlElementRef(name = "centerCode", namespace = "http://infrastructure.tia.dk/schema/account/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> centerCode;
    @XmlElementRef(name = "receiverId", namespace = "http://infrastructure.tia.dk/schema/account/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<Long> receiverId;
    @XmlElementRef(name = "handler", namespace = "http://infrastructure.tia.dk/schema/account/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> handler;
    @XmlElementRef(name = "reminderGroup", namespace = "http://infrastructure.tia.dk/schema/account/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> reminderGroup;
    @XmlElementRef(name = "title", namespace = "http://infrastructure.tia.dk/schema/account/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> title;
    @XmlElementRef(name = "collectionBy", namespace = "http://infrastructure.tia.dk/schema/account/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> collectionBy;
    @XmlElementRef(name = "paymentTermsIn", namespace = "http://infrastructure.tia.dk/schema/account/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> paymentTermsIn;
    @XmlElementRef(name = "paymentMethodIn", namespace = "http://infrastructure.tia.dk/schema/account/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> paymentMethodIn;
    @XmlElementRef(name = "meansPayNoIn", namespace = "http://infrastructure.tia.dk/schema/account/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<Long> meansPayNoIn;
    @XmlElementRef(name = "paymentTermsOut", namespace = "http://infrastructure.tia.dk/schema/account/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> paymentTermsOut;
    @XmlElementRef(name = "paymentMethodOut", namespace = "http://infrastructure.tia.dk/schema/account/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> paymentMethodOut;
    @XmlElementRef(name = "meansPayNoOut", namespace = "http://infrastructure.tia.dk/schema/account/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<Long> meansPayNoOut;
    @XmlElementRef(name = "note", namespace = "http://infrastructure.tia.dk/schema/account/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> note;
    protected AccountItemCollection accountItems;

    /**
     * Gets the value of the accountNo property.
     * 
     */
    public long getAccountNo() {
        return accountNo;
    }

    /**
     * Sets the value of the accountNo property.
     * 
     */
    public void setAccountNo(long value) {
        this.accountNo = value;
    }

    /**
     * Gets the value of the nameId property.
     * 
     */
    public long getNameId() {
        return nameId;
    }

    /**
     * Sets the value of the nameId property.
     * 
     */
    public void setNameId(long value) {
        this.nameId = value;
    }

    /**
     * Gets the value of the accountType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountType() {
        return accountType;
    }

    /**
     * Sets the value of the accountType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountType(String value) {
        this.accountType = value;
    }

    /**
     * Gets the value of the locked property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLocked() {
        return locked;
    }

    /**
     * Sets the value of the locked property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLocked(JAXBElement<String> value) {
        this.locked = value;
    }

    /**
     * Gets the value of the currencyCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCurrencyCode() {
        return currencyCode;
    }

    /**
     * Sets the value of the currencyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCurrencyCode(JAXBElement<String> value) {
        this.currencyCode = value;
    }

    /**
     * Gets the value of the centerCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCenterCode() {
        return centerCode;
    }

    /**
     * Sets the value of the centerCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCenterCode(JAXBElement<String> value) {
        this.centerCode = value;
    }

    /**
     * Gets the value of the receiverId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Long }{@code >}
     *     
     */
    public JAXBElement<Long> getReceiverId() {
        return receiverId;
    }

    /**
     * Sets the value of the receiverId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Long }{@code >}
     *     
     */
    public void setReceiverId(JAXBElement<Long> value) {
        this.receiverId = value;
    }

    /**
     * Gets the value of the handler property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getHandler() {
        return handler;
    }

    /**
     * Sets the value of the handler property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setHandler(JAXBElement<String> value) {
        this.handler = value;
    }

    /**
     * Gets the value of the reminderGroup property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getReminderGroup() {
        return reminderGroup;
    }

    /**
     * Sets the value of the reminderGroup property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setReminderGroup(JAXBElement<String> value) {
        this.reminderGroup = value;
    }

    /**
     * Gets the value of the title property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTitle() {
        return title;
    }

    /**
     * Sets the value of the title property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTitle(JAXBElement<String> value) {
        this.title = value;
    }

    /**
     * Gets the value of the collectionBy property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getCollectionBy() {
        return collectionBy;
    }

    /**
     * Sets the value of the collectionBy property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setCollectionBy(JAXBElement<Integer> value) {
        this.collectionBy = value;
    }

    /**
     * Gets the value of the paymentTermsIn property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPaymentTermsIn() {
        return paymentTermsIn;
    }

    /**
     * Sets the value of the paymentTermsIn property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPaymentTermsIn(JAXBElement<String> value) {
        this.paymentTermsIn = value;
    }

    /**
     * Gets the value of the paymentMethodIn property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPaymentMethodIn() {
        return paymentMethodIn;
    }

    /**
     * Sets the value of the paymentMethodIn property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPaymentMethodIn(JAXBElement<String> value) {
        this.paymentMethodIn = value;
    }

    /**
     * Gets the value of the meansPayNoIn property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Long }{@code >}
     *     
     */
    public JAXBElement<Long> getMeansPayNoIn() {
        return meansPayNoIn;
    }

    /**
     * Sets the value of the meansPayNoIn property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Long }{@code >}
     *     
     */
    public void setMeansPayNoIn(JAXBElement<Long> value) {
        this.meansPayNoIn = value;
    }

    /**
     * Gets the value of the paymentTermsOut property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPaymentTermsOut() {
        return paymentTermsOut;
    }

    /**
     * Sets the value of the paymentTermsOut property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPaymentTermsOut(JAXBElement<String> value) {
        this.paymentTermsOut = value;
    }

    /**
     * Gets the value of the paymentMethodOut property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPaymentMethodOut() {
        return paymentMethodOut;
    }

    /**
     * Sets the value of the paymentMethodOut property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPaymentMethodOut(JAXBElement<String> value) {
        this.paymentMethodOut = value;
    }

    /**
     * Gets the value of the meansPayNoOut property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Long }{@code >}
     *     
     */
    public JAXBElement<Long> getMeansPayNoOut() {
        return meansPayNoOut;
    }

    /**
     * Sets the value of the meansPayNoOut property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Long }{@code >}
     *     
     */
    public void setMeansPayNoOut(JAXBElement<Long> value) {
        this.meansPayNoOut = value;
    }

    /**
     * Gets the value of the note property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNote() {
        return note;
    }

    /**
     * Sets the value of the note property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNote(JAXBElement<String> value) {
        this.note = value;
    }

    /**
     * Gets the value of the accountItems property.
     * 
     * @return
     *     possible object is
     *     {@link AccountItemCollection }
     *     
     */
    public AccountItemCollection getAccountItems() {
        return accountItems;
    }

    /**
     * Sets the value of the accountItems property.
     * 
     * @param value
     *     allowed object is
     *     {@link AccountItemCollection }
     *     
     */
    public void setAccountItems(AccountItemCollection value) {
        this.accountItems = value;
    }

}
