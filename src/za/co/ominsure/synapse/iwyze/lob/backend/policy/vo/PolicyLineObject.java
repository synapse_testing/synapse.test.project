
package za.co.ominsure.synapse.iwyze.lob.backend.policy.vo;

import java.math.BigDecimal;
import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Is mapped into xxxx".
 * 
 * <p>Java class for PolicyLineObject complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PolicyLineObject">
 *   &lt;complexContent>
 *     &lt;extension base="{http://infrastructure.tia.dk/schema/common/v2/}TIAObject">
 *       &lt;sequence>
 *         &lt;element name="objectNo" type="{http://infrastructure.tia.dk/schema/policy/v2/}seqNo" minOccurs="0"/>
 *         &lt;element name="seqNo" type="{http://infrastructure.tia.dk/schema/policy/v2/}seqNo" minOccurs="0"/>
 *         &lt;element name="objectType" type="{http://infrastructure.tia.dk/schema/common/v2/}string12" minOccurs="0"/>
 *         &lt;element name="typeId" type="{http://infrastructure.tia.dk/schema/policy/v2/}typeId" minOccurs="0"/>
 *         &lt;element name="status" type="{http://infrastructure.tia.dk/schema/policy/v2/}status" minOccurs="0"/>
 *         &lt;element name="newest" type="{http://infrastructure.tia.dk/schema/policy/v2/}newest" minOccurs="0"/>
 *         &lt;element name="cancelCode" type="{http://infrastructure.tia.dk/schema/policy/v2/}cancelCode" minOccurs="0"/>
 *         &lt;element name="coverStartDate" type="{http://infrastructure.tia.dk/schema/common/v2/}date" minOccurs="0"/>
 *         &lt;element name="coverEndDate" type="{http://infrastructure.tia.dk/schema/common/v2/}date" minOccurs="0"/>
 *         &lt;element name="preSeqNo" type="{http://infrastructure.tia.dk/schema/policy/v2/}seqNo" minOccurs="0"/>
 *         &lt;element name="sucSeqNo" type="{http://infrastructure.tia.dk/schema/policy/v2/}seqNo" minOccurs="0"/>
 *         &lt;element name="transId" type="{http://infrastructure.tia.dk/schema/policy/v2/}seqNo" minOccurs="0"/>
 *         &lt;element name="objectId" type="{http://infrastructure.tia.dk/schema/policy/v2/}objectId" minOccurs="0"/>
 *         &lt;element name="objectNameId" type="{http://infrastructure.tia.dk/schema/policy/v2/}seqNo" minOccurs="0"/>
 *         &lt;element name="shortDesc" type="{http://infrastructure.tia.dk/schema/policy/v2/}objectShortDesc" minOccurs="0"/>
 *         &lt;element name="sumInsured" type="{http://infrastructure.tia.dk/schema/policy/v2/}policyAmount" minOccurs="0"/>
 *         &lt;element name="custNo" type="{http://infrastructure.tia.dk/schema/policy/v2/}seqNo" minOccurs="0"/>
 *         &lt;element name="locationId" type="{http://infrastructure.tia.dk/schema/policy/v2/}seqNo" minOccurs="0"/>
 *         &lt;element name="seqNoQop" type="{http://infrastructure.tia.dk/schema/common/v2/}long10" minOccurs="0"/>
 *         &lt;element name="parentObjectNo" type="{http://infrastructure.tia.dk/schema/policy/v2/}seqNo" minOccurs="0"/>
 *         &lt;element name="informationOnly" type="{http://infrastructure.tia.dk/schema/common/v2/}tiaYN" minOccurs="0"/>
 *         &lt;element name="interestedPartyCollection" type="{http://infrastructure.tia.dk/schema/policy/v2/}InterestedPartyCollection" minOccurs="0"/>
 *         &lt;element name="riskCollection" type="{http://infrastructure.tia.dk/schema/policy/v4/}RiskCollection" minOccurs="0"/>
 *         &lt;element name="clauseCollection" type="{http://infrastructure.tia.dk/schema/policy/v4/}ClauseCollection" minOccurs="0"/>
 *         &lt;element name="objectSlaveCollection" type="{http://infrastructure.tia.dk/schema/policy/v4/}ObjectSlaveCollection" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PolicyLineObject", namespace = "http://infrastructure.tia.dk/schema/policy/v4/", propOrder = {
    "objectNo",
    "seqNo",
    "objectType",
    "typeId",
    "status",
    "newest",
    "cancelCode",
    "coverStartDate",
    "coverEndDate",
    "preSeqNo",
    "sucSeqNo",
    "transId",
    "objectId",
    "objectNameId",
    "shortDesc",
    "sumInsured",
    "custNo",
    "locationId",
    "seqNoQop",
    "parentObjectNo",
    "informationOnly",
    "interestedPartyCollection",
    "riskCollection",
    "clauseCollection",
    "objectSlaveCollection"
})
public class PolicyLineObject
    extends TIAObject
{

    @XmlElement(nillable = true)
    @XmlSchemaType(name = "unsignedLong")
    protected BigInteger objectNo;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "unsignedLong")
    protected BigInteger seqNo;
    protected String objectType;
    protected String typeId;
    @XmlElement(nillable = true)
    protected String status;
    @XmlElement(nillable = true)
    protected String newest;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "unsignedInt")
    protected Long cancelCode;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar coverStartDate;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar coverEndDate;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "unsignedLong")
    protected BigInteger preSeqNo;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "unsignedLong")
    protected BigInteger sucSeqNo;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "unsignedLong")
    protected BigInteger transId;
    @XmlElement(nillable = true)
    protected String objectId;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "unsignedLong")
    protected BigInteger objectNameId;
    @XmlElement(nillable = true)
    protected String shortDesc;
    @XmlElement(nillable = true)
    protected BigDecimal sumInsured;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "unsignedLong")
    protected BigInteger custNo;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "unsignedLong")
    protected BigInteger locationId;
    @XmlElement(nillable = true)
    protected Long seqNoQop;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "unsignedLong")
    protected BigInteger parentObjectNo;
    @XmlElement(nillable = true)
    protected String informationOnly;
    protected InterestedPartyCollection interestedPartyCollection;
    protected RiskCollection riskCollection;
    protected ClauseCollection clauseCollection;
    protected ObjectSlaveCollection objectSlaveCollection;

    /**
     * Gets the value of the objectNo property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getObjectNo() {
        return objectNo;
    }

    /**
     * Sets the value of the objectNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setObjectNo(BigInteger value) {
        this.objectNo = value;
    }

    /**
     * Gets the value of the seqNo property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getSeqNo() {
        return seqNo;
    }

    /**
     * Sets the value of the seqNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setSeqNo(BigInteger value) {
        this.seqNo = value;
    }

    /**
     * Gets the value of the objectType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getObjectType() {
        return objectType;
    }

    /**
     * Sets the value of the objectType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setObjectType(String value) {
        this.objectType = value;
    }

    /**
     * Gets the value of the typeId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTypeId() {
        return typeId;
    }

    /**
     * Sets the value of the typeId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTypeId(String value) {
        this.typeId = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

    /**
     * Gets the value of the newest property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNewest() {
        return newest;
    }

    /**
     * Sets the value of the newest property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNewest(String value) {
        this.newest = value;
    }

    /**
     * Gets the value of the cancelCode property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getCancelCode() {
        return cancelCode;
    }

    /**
     * Sets the value of the cancelCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setCancelCode(Long value) {
        this.cancelCode = value;
    }

    /**
     * Gets the value of the coverStartDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCoverStartDate() {
        return coverStartDate;
    }

    /**
     * Sets the value of the coverStartDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCoverStartDate(XMLGregorianCalendar value) {
        this.coverStartDate = value;
    }

    /**
     * Gets the value of the coverEndDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCoverEndDate() {
        return coverEndDate;
    }

    /**
     * Sets the value of the coverEndDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCoverEndDate(XMLGregorianCalendar value) {
        this.coverEndDate = value;
    }

    /**
     * Gets the value of the preSeqNo property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getPreSeqNo() {
        return preSeqNo;
    }

    /**
     * Sets the value of the preSeqNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setPreSeqNo(BigInteger value) {
        this.preSeqNo = value;
    }

    /**
     * Gets the value of the sucSeqNo property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getSucSeqNo() {
        return sucSeqNo;
    }

    /**
     * Sets the value of the sucSeqNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setSucSeqNo(BigInteger value) {
        this.sucSeqNo = value;
    }

    /**
     * Gets the value of the transId property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getTransId() {
        return transId;
    }

    /**
     * Sets the value of the transId property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setTransId(BigInteger value) {
        this.transId = value;
    }

    /**
     * Gets the value of the objectId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getObjectId() {
        return objectId;
    }

    /**
     * Sets the value of the objectId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setObjectId(String value) {
        this.objectId = value;
    }

    /**
     * Gets the value of the objectNameId property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getObjectNameId() {
        return objectNameId;
    }

    /**
     * Sets the value of the objectNameId property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setObjectNameId(BigInteger value) {
        this.objectNameId = value;
    }

    /**
     * Gets the value of the shortDesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShortDesc() {
        return shortDesc;
    }

    /**
     * Sets the value of the shortDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShortDesc(String value) {
        this.shortDesc = value;
    }

    /**
     * Gets the value of the sumInsured property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSumInsured() {
        return sumInsured;
    }

    /**
     * Sets the value of the sumInsured property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSumInsured(BigDecimal value) {
        this.sumInsured = value;
    }

    /**
     * Gets the value of the custNo property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getCustNo() {
        return custNo;
    }

    /**
     * Sets the value of the custNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setCustNo(BigInteger value) {
        this.custNo = value;
    }

    /**
     * Gets the value of the locationId property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getLocationId() {
        return locationId;
    }

    /**
     * Sets the value of the locationId property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setLocationId(BigInteger value) {
        this.locationId = value;
    }

    /**
     * Gets the value of the seqNoQop property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getSeqNoQop() {
        return seqNoQop;
    }

    /**
     * Sets the value of the seqNoQop property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setSeqNoQop(Long value) {
        this.seqNoQop = value;
    }

    /**
     * Gets the value of the parentObjectNo property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getParentObjectNo() {
        return parentObjectNo;
    }

    /**
     * Sets the value of the parentObjectNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setParentObjectNo(BigInteger value) {
        this.parentObjectNo = value;
    }

    /**
     * Gets the value of the informationOnly property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInformationOnly() {
        return informationOnly;
    }

    /**
     * Sets the value of the informationOnly property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInformationOnly(String value) {
        this.informationOnly = value;
    }

    /**
     * Gets the value of the interestedPartyCollection property.
     * 
     * @return
     *     possible object is
     *     {@link InterestedPartyCollection }
     *     
     */
    public InterestedPartyCollection getInterestedPartyCollection() {
        return interestedPartyCollection;
    }

    /**
     * Sets the value of the interestedPartyCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link InterestedPartyCollection }
     *     
     */
    public void setInterestedPartyCollection(InterestedPartyCollection value) {
        this.interestedPartyCollection = value;
    }

    /**
     * Gets the value of the riskCollection property.
     * 
     * @return
     *     possible object is
     *     {@link RiskCollection }
     *     
     */
    public RiskCollection getRiskCollection() {
        return riskCollection;
    }

    /**
     * Sets the value of the riskCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link RiskCollection }
     *     
     */
    public void setRiskCollection(RiskCollection value) {
        this.riskCollection = value;
    }

    /**
     * Gets the value of the clauseCollection property.
     * 
     * @return
     *     possible object is
     *     {@link ClauseCollection }
     *     
     */
    public ClauseCollection getClauseCollection() {
        return clauseCollection;
    }

    /**
     * Sets the value of the clauseCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link ClauseCollection }
     *     
     */
    public void setClauseCollection(ClauseCollection value) {
        this.clauseCollection = value;
    }

    /**
     * Gets the value of the objectSlaveCollection property.
     * 
     * @return
     *     possible object is
     *     {@link ObjectSlaveCollection }
     *     
     */
    public ObjectSlaveCollection getObjectSlaveCollection() {
        return objectSlaveCollection;
    }

    /**
     * Sets the value of the objectSlaveCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjectSlaveCollection }
     *     
     */
    public void setObjectSlaveCollection(ObjectSlaveCollection value) {
        this.objectSlaveCollection = value;
    }

}
