
package za.co.ominsure.synapse.iwyze.lob.backend.search.payment.vo;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for InstalmentPlanCollection complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="InstalmentPlanCollection">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="instalmentPlan" type="{http://infrastructure.tia.dk/schema/account/v2/}InstalmentPlan" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InstalmentPlanCollection", propOrder = {
    "instalmentPlan"
})
public class InstalmentPlanCollection {

    protected List<InstalmentPlan> instalmentPlan;

    /**
     * Gets the value of the instalmentPlan property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the instalmentPlan property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInstalmentPlan().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link InstalmentPlan }
     * 
     * 
     */
    public List<InstalmentPlan> getInstalmentPlan() {
        if (instalmentPlan == null) {
            instalmentPlan = new ArrayList<InstalmentPlan>();
        }
        return this.instalmentPlan;
    }

}
