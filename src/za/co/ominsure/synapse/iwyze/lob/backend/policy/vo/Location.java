
package za.co.ominsure.synapse.iwyze.lob.backend.policy.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Additional comments
 * 
 * <p>Java class for Location complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Location">
 *   &lt;complexContent>
 *     &lt;extension base="{http://infrastructure.tia.dk/schema/common/v2/}TIAObject">
 *       &lt;sequence>
 *         &lt;element name="idNo" type="{http://infrastructure.tia.dk/schema/common/v2/}long10" minOccurs="0"/>
 *         &lt;element name="surname" type="{http://infrastructure.tia.dk/schema/common/v2/}string200" minOccurs="0"/>
 *         &lt;element name="name" type="{http://infrastructure.tia.dk/schema/common/v2/}string32" minOccurs="0"/>
 *         &lt;element name="language" type="{http://infrastructure.tia.dk/schema/common/v2/}string3" minOccurs="0"/>
 *         &lt;element name="currencyCode" type="{http://infrastructure.tia.dk/schema/common/v2/}string3" minOccurs="0"/>
 *         &lt;element name="obsoleteCode" type="{http://infrastructure.tia.dk/schema/common/v2/}int3" minOccurs="0"/>
 *         &lt;element name="addressCollection" type="{http://infrastructure.tia.dk/schema/party/v2/}AddressCollection" minOccurs="0"/>
 *         &lt;element name="nameSort" type="{http://infrastructure.tia.dk/schema/common/v2/}string15" minOccurs="0"/>
 *         &lt;element name="idNoAlt" type="{http://infrastructure.tia.dk/schema/common/v2/}string30" minOccurs="0"/>
 *         &lt;element name="idNoSort" type="{http://infrastructure.tia.dk/schema/common/v2/}string30" minOccurs="0"/>
 *         &lt;element name="contactInfoCollection" type="{http://infrastructure.tia.dk/schema/party/v2/}ContactInfoCollection" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Location", namespace = "http://infrastructure.tia.dk/schema/party/v2/", propOrder = {
    "idNo",
    "surname",
    "name",
    "language",
    "currencyCode",
    "obsoleteCode",
    "addressCollection",
    "nameSort",
    "idNoAlt",
    "idNoSort",
    "contactInfoCollection"
})
public class Location
    extends TIAObject
{

    @XmlElement(nillable = true)
    protected Long idNo;
    @XmlElement(nillable = true)
    protected String surname;
    @XmlElement(nillable = true)
    protected String name;
    @XmlElement(nillable = true)
    protected String language;
    @XmlElement(nillable = true)
    protected String currencyCode;
    @XmlElement(nillable = true)
    protected Long obsoleteCode;
    protected AddressCollection addressCollection;
    @XmlElement(nillable = true)
    protected String nameSort;
    @XmlElement(nillable = true)
    protected String idNoAlt;
    @XmlElement(nillable = true)
    protected String idNoSort;
    protected ContactInfoCollection contactInfoCollection;

    /**
     * Gets the value of the idNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getIdNo() {
        return idNo;
    }

    /**
     * Sets the value of the idNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setIdNo(Long value) {
        this.idNo = value;
    }

    /**
     * Gets the value of the surname property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSurname() {
        return surname;
    }

    /**
     * Sets the value of the surname property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSurname(String value) {
        this.surname = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the language property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLanguage() {
        return language;
    }

    /**
     * Sets the value of the language property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLanguage(String value) {
        this.language = value;
    }

    /**
     * Gets the value of the currencyCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrencyCode() {
        return currencyCode;
    }

    /**
     * Sets the value of the currencyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrencyCode(String value) {
        this.currencyCode = value;
    }

    /**
     * Gets the value of the obsoleteCode property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getObsoleteCode() {
        return obsoleteCode;
    }

    /**
     * Sets the value of the obsoleteCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setObsoleteCode(Long value) {
        this.obsoleteCode = value;
    }

    /**
     * Gets the value of the addressCollection property.
     * 
     * @return
     *     possible object is
     *     {@link AddressCollection }
     *     
     */
    public AddressCollection getAddressCollection() {
        return addressCollection;
    }

    /**
     * Sets the value of the addressCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link AddressCollection }
     *     
     */
    public void setAddressCollection(AddressCollection value) {
        this.addressCollection = value;
    }

    /**
     * Gets the value of the nameSort property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNameSort() {
        return nameSort;
    }

    /**
     * Sets the value of the nameSort property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNameSort(String value) {
        this.nameSort = value;
    }

    /**
     * Gets the value of the idNoAlt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdNoAlt() {
        return idNoAlt;
    }

    /**
     * Sets the value of the idNoAlt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdNoAlt(String value) {
        this.idNoAlt = value;
    }

    /**
     * Gets the value of the idNoSort property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdNoSort() {
        return idNoSort;
    }

    /**
     * Sets the value of the idNoSort property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdNoSort(String value) {
        this.idNoSort = value;
    }

    /**
     * Gets the value of the contactInfoCollection property.
     * 
     * @return
     *     possible object is
     *     {@link ContactInfoCollection }
     *     
     */
    public ContactInfoCollection getContactInfoCollection() {
        return contactInfoCollection;
    }

    /**
     * Sets the value of the contactInfoCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link ContactInfoCollection }
     *     
     */
    public void setContactInfoCollection(ContactInfoCollection value) {
        this.contactInfoCollection = value;
    }

}
