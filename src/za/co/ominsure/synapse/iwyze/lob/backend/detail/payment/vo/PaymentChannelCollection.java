
package za.co.ominsure.synapse.iwyze.lob.backend.detail.payment.vo;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Is mapped into TIA metadata type "tab_payment_channel".
 * 
 * <p>Java class for PaymentChannelCollection complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PaymentChannelCollection">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="paymentChannel" type="{http://infrastructure.tia.dk/schema/account/v2/}PaymentChannel" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PaymentChannelCollection", propOrder = {
    "paymentChannels"
})
public class PaymentChannelCollection {

    @XmlElement(name = "paymentChannel")
    protected List<PaymentChannel> paymentChannels;

    /**
     * Gets the value of the paymentChannels property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the paymentChannels property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPaymentChannels().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PaymentChannel }
     * 
     * 
     */
    public List<PaymentChannel> getPaymentChannels() {
        if (paymentChannels == null) {
            paymentChannels = new ArrayList<PaymentChannel>();
        }
        return this.paymentChannels;
    }

}
