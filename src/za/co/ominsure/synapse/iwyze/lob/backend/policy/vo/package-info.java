/**
 * Version 2.0
 * 
 */
@javax.xml.bind.annotation.XmlSchema(namespace = "http://infrastructure.tia.dk/schema/policy/v2/", elementFormDefault = javax.xml.bind.annotation.XmlNsForm.QUALIFIED)
package za.co.ominsure.synapse.iwyze.lob.backend.policy.vo;
