
package za.co.ominsure.synapse.iwyze.lob.backend.exists.policy.vo;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TIA.OBJ_RESULT complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TIA.OBJ_RESULT">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RESULT_CODE" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="MESSAGES" type="{http://xmlns.oracle.com/pcbpel/adapter/db/sp/checkExistingPolicyReference}TIA.TAB_MESSAGE" minOccurs="0"/>
 *         &lt;element name="CALL_DURATION" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="LOG_ID" type="{http://xmlns.oracle.com/pcbpel/adapter/db/sp/checkExistingPolicyReference}string255" minOccurs="0"/>
 *         &lt;element name="CORRELATION_ID" type="{http://xmlns.oracle.com/pcbpel/adapter/db/sp/checkExistingPolicyReference}string32" minOccurs="0"/>
 *         &lt;element name="OPERATION_SCN" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TIA.OBJ_RESULT", propOrder = {
    "resultcode",
    "messages",
    "callduration",
    "logid",
    "correlationid",
    "operationscn"
})
public class TIAOBJRESULT {

    @XmlElement(name = "RESULT_CODE", nillable = true)
    protected Integer resultcode;
    @XmlElement(name = "MESSAGES", nillable = true)
    protected TIATABMESSAGE messages;
    @XmlElement(name = "CALL_DURATION", nillable = true)
    protected BigDecimal callduration;
    @XmlElement(name = "LOG_ID", nillable = true)
    protected String logid;
    @XmlElement(name = "CORRELATION_ID", nillable = true)
    protected String correlationid;
    @XmlElement(name = "OPERATION_SCN", nillable = true)
    protected BigDecimal operationscn;

    /**
     * Gets the value of the resultcode property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public Integer getRESULTCODE() {
        return resultcode;
    }

    /**
     * Sets the value of the resultcode property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRESULTCODE(Integer value) {
        this.resultcode = value;
    }

    /**
     * Gets the value of the messages property.
     * 
     * @return
     *     possible object is
     *     {@link TIATABMESSAGE }
     *     
     */
    public TIATABMESSAGE getMESSAGES() {
        return messages;
    }

    /**
     * Sets the value of the messages property.
     * 
     * @param value
     *     allowed object is
     *     {@link TIATABMESSAGE }
     *     
     */
    public void setMESSAGES(TIATABMESSAGE value) {
        this.messages = value;
    }

    /**
     * Gets the value of the callduration property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCALLDURATION() {
        return callduration;
    }

    /**
     * Sets the value of the callduration property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCALLDURATION(BigDecimal value) {
        this.callduration = value;
    }

    /**
     * Gets the value of the logid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLOGID() {
        return logid;
    }

    /**
     * Sets the value of the logid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLOGID(String value) {
        this.logid = value;
    }

    /**
     * Gets the value of the correlationid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCORRELATIONID() {
        return correlationid;
    }

    /**
     * Sets the value of the correlationid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCORRELATIONID(String value) {
        this.correlationid = value;
    }

    /**
     * Gets the value of the operationscn property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getOPERATIONSCN() {
        return operationscn;
    }

    /**
     * Sets the value of the operationscn property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setOPERATIONSCN(BigDecimal value) {
        this.operationscn = value;
    }

}
