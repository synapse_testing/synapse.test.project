
package za.co.ominsure.synapse.iwyze.lob.backend.policy.vo;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ProdLineVerInProductCollection complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ProdLineVerInProductCollection">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="prodLineVerInProduct" type="{http://infrastructure.tia.dk/schema/policy/v2/}ProdLineVerInProduct" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProdLineVerInProductCollection", propOrder = {
    "prodLineVerInProducts"
})
public class ProdLineVerInProductCollection {

    @XmlElement(name = "prodLineVerInProduct")
    protected List<ProdLineVerInProduct> prodLineVerInProducts;

    /**
     * Gets the value of the prodLineVerInProducts property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the prodLineVerInProducts property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getProdLineVerInProducts().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProdLineVerInProduct }
     * 
     * 
     */
    public List<ProdLineVerInProduct> getProdLineVerInProducts() {
        if (prodLineVerInProducts == null) {
            prodLineVerInProducts = new ArrayList<ProdLineVerInProduct>();
        }
        return this.prodLineVerInProducts;
    }

}
