
package za.co.ominsure.synapse.iwyze.lob.backend.policy.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Is mapped into undefined".
 * 
 * <p>Java class for PolicySearchResult complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PolicySearchResult">
 *   &lt;complexContent>
 *     &lt;extension base="{http://infrastructure.tia.dk/schema/common/v2/}TIAObject">
 *       &lt;sequence>
 *         &lt;element name="policyDetails" type="{http://infrastructure.tia.dk/schema/policy/v4/}Policy"/>
 *         &lt;element name="policyHoldersDetailCollection" type="{http://infrastructure.tia.dk/schema/party/v2/}PartyCollection"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PolicySearchResult", namespace = "http://infrastructure.tia.dk/schema/policy/v4/", propOrder = {
    "policyDetails",
    "policyHoldersDetailCollection"
})
public class PolicySearchResult
    extends TIAObject
{

    @XmlElement(required = true)
    protected Policy policyDetails;
    @XmlElement(required = true)
    protected PartyCollection policyHoldersDetailCollection;

    /**
     * Gets the value of the policyDetails property.
     * 
     * @return
     *     possible object is
     *     {@link Policy }
     *     
     */
    public Policy getPolicyDetails() {
        return policyDetails;
    }

    /**
     * Sets the value of the policyDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link Policy }
     *     
     */
    public void setPolicyDetails(Policy value) {
        this.policyDetails = value;
    }

    /**
     * Gets the value of the policyHoldersDetailCollection property.
     * 
     * @return
     *     possible object is
     *     {@link PartyCollection }
     *     
     */
    public PartyCollection getPolicyHoldersDetailCollection() {
        return policyHoldersDetailCollection;
    }

    /**
     * Sets the value of the policyHoldersDetailCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link PartyCollection }
     *     
     */
    public void setPolicyHoldersDetailCollection(PartyCollection value) {
        this.policyHoldersDetailCollection = value;
    }

}
