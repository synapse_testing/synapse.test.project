
package za.co.ominsure.synapse.iwyze.lob.backend.interactive.claim.vo;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ClaimPaymentItemCollection complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ClaimPaymentItemCollection">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="claimPaymentItem" type="{http://infrastructure.tia.dk/schema/claim/v2/}ClaimPaymentItem" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ClaimPaymentItemCollection", propOrder = {
    "claimPaymentItems"
})
public class ClaimPaymentItemCollection {

    @XmlElement(name = "claimPaymentItem")
    protected List<ClaimPaymentItem> claimPaymentItems;

    /**
     * Gets the value of the claimPaymentItems property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the claimPaymentItems property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getClaimPaymentItems().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ClaimPaymentItem }
     * 
     * 
     */
    public List<ClaimPaymentItem> getClaimPaymentItems() {
        if (claimPaymentItems == null) {
            claimPaymentItems = new ArrayList<ClaimPaymentItem>();
        }
        return this.claimPaymentItems;
    }

}
