package za.co.ominsure.synapse.iwyze.lob.backend.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "userId", "language", "siteName", "traceOn", "commitOn", "addClaimCommitOn" })
@XmlRootElement(name = "ServiceConfiguration")
public class ServiceConfiguration {

	@XmlElement(name = "userId")
	protected String userId;
	@XmlElement(name = "language")
	protected String language;
	@XmlElement(name = "siteName")
	protected String siteName;
	@XmlElement(name = "traceOn")
	protected String traceOn;
	@XmlElement(name = "commitOn")
	protected String commitOn;
	@XmlElement(name = "addClaimCommitOn")
	protected String addClaimCommitOn;

	public String getAddClaimCommitOn() {
		return addClaimCommitOn;
	}

	public void setAddClaimCommitOn(String addClaimCommitOn) {
		this.addClaimCommitOn = addClaimCommitOn;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getSiteName() {
		return siteName;
	}

	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}

	public String getTraceOn() {
		return traceOn;
	}

	public void setTraceOn(String traceOn) {
		this.traceOn = traceOn;
	}

	public String getCommitOn() {
		return commitOn;
	}

	public void setCommitOn(String commitOn) {
		this.commitOn = commitOn;
	}

}
