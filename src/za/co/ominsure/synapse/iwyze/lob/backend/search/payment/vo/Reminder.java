
package za.co.ominsure.synapse.iwyze.lob.backend.search.payment.vo;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Is mapped into TIA metadata type "obj_reminder".
 * 
 * <p>Java class for Reminder complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Reminder">
 *   &lt;complexContent>
 *     &lt;extension base="{http://infrastructure.tia.dk/schema/common/v2/}TIAObject">
 *       &lt;sequence>
 *         &lt;element name="reminderId" type="{http://infrastructure.tia.dk/schema/common/v2/}seqNo"/>
 *         &lt;element name="reminderStepNo" type="{http://infrastructure.tia.dk/schema/account/v2/}reminderStepNo" minOccurs="0"/>
 *         &lt;element name="dueDate" type="{http://infrastructure.tia.dk/schema/common/v2/}date" minOccurs="0"/>
 *         &lt;element name="remindedItemCollection" type="{http://infrastructure.tia.dk/schema/account/v2/}RemindedItemCollection" minOccurs="0"/>
 *         &lt;element name="reminderSpecificationCollection" type="{http://infrastructure.tia.dk/schema/account/v2/}ReminderSpecificationCollection" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Reminder", propOrder = {
    "reminderId",
    "reminderStepNo",
    "dueDate",
    "remindedItemCollection",
    "reminderSpecificationCollection"
})
public class Reminder
    extends TIAObject
{

    @XmlSchemaType(name = "unsignedLong")
    protected long reminderId;
    @XmlElementRef(name = "reminderStepNo", namespace = "http://infrastructure.tia.dk/schema/account/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> reminderStepNo;
    @XmlElementRef(name = "dueDate", namespace = "http://infrastructure.tia.dk/schema/account/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> dueDate;
    protected RemindedItemCollection remindedItemCollection;
    protected ReminderSpecificationCollection reminderSpecificationCollection;

    /**
     * Gets the value of the reminderId property.
     * 
     */
    public long getReminderId() {
        return reminderId;
    }

    /**
     * Sets the value of the reminderId property.
     * 
     */
    public void setReminderId(long value) {
        this.reminderId = value;
    }

    /**
     * Gets the value of the reminderStepNo property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getReminderStepNo() {
        return reminderStepNo;
    }

    /**
     * Sets the value of the reminderStepNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setReminderStepNo(JAXBElement<Integer> value) {
        this.reminderStepNo = value;
    }

    /**
     * Gets the value of the dueDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getDueDate() {
        return dueDate;
    }

    /**
     * Sets the value of the dueDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setDueDate(JAXBElement<XMLGregorianCalendar> value) {
        this.dueDate = value;
    }

    /**
     * Gets the value of the remindedItemCollection property.
     * 
     * @return
     *     possible object is
     *     {@link RemindedItemCollection }
     *     
     */
    public RemindedItemCollection getRemindedItemCollection() {
        return remindedItemCollection;
    }

    /**
     * Sets the value of the remindedItemCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link RemindedItemCollection }
     *     
     */
    public void setRemindedItemCollection(RemindedItemCollection value) {
        this.remindedItemCollection = value;
    }

    /**
     * Gets the value of the reminderSpecificationCollection property.
     * 
     * @return
     *     possible object is
     *     {@link ReminderSpecificationCollection }
     *     
     */
    public ReminderSpecificationCollection getReminderSpecificationCollection() {
        return reminderSpecificationCollection;
    }

    /**
     * Sets the value of the reminderSpecificationCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReminderSpecificationCollection }
     *     
     */
    public void setReminderSpecificationCollection(ReminderSpecificationCollection value) {
        this.reminderSpecificationCollection = value;
    }

}
