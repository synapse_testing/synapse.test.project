
package za.co.ominsure.synapse.iwyze.lob.backend.policy.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="inputToken" type="{http://infrastructure.tia.dk/schema/common/v2/}InputToken"/>
 *         &lt;element name="policy" type="{http://infrastructure.tia.dk/schema/policy/v4/}Policy"/>
 *         &lt;element name="declinatureCode" type="{http://infrastructure.tia.dk/schema/common/v2/}string3" minOccurs="0"/>
 *         &lt;element name="getDepth" type="{http://infrastructure.tia.dk/schema/policy/v2/}getDepth" minOccurs="0"/>
 *         &lt;element name="action" type="{http://infrastructure.tia.dk/schema/common/v2/}long10" minOccurs="0"/>
 *         &lt;element name="contextName" type="{http://infrastructure.tia.dk/schema/common/v2/}string10" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "inputToken",
    "policy",
    "declinatureCode",
    "getDepth",
    "action",
    "contextName"
})
@XmlRootElement(name = "createQuoteRequest", namespace = "http://infrastructure.tia.dk/schema/policy/v4/")
public class CreateQuoteRequest {

    @XmlElement(namespace = "http://infrastructure.tia.dk/schema/policy/v4/", required = true)
    protected InputToken inputToken;
    @XmlElement(namespace = "http://infrastructure.tia.dk/schema/policy/v4/", required = true)
    protected Policy policy;
    @XmlElement(namespace = "http://infrastructure.tia.dk/schema/policy/v4/")
    protected String declinatureCode;
    @XmlElement(namespace = "http://infrastructure.tia.dk/schema/policy/v4/")
    @XmlSchemaType(name = "string")
    protected GetDepth getDepth;
    @XmlElement(namespace = "http://infrastructure.tia.dk/schema/policy/v4/")
    protected Long action;
    @XmlElement(namespace = "http://infrastructure.tia.dk/schema/policy/v4/")
    protected String contextName;

    /**
     * Gets the value of the inputToken property.
     * 
     * @return
     *     possible object is
     *     {@link InputToken }
     *     
     */
    public InputToken getInputToken() {
        return inputToken;
    }

    /**
     * Sets the value of the inputToken property.
     * 
     * @param value
     *     allowed object is
     *     {@link InputToken }
     *     
     */
    public void setInputToken(InputToken value) {
        this.inputToken = value;
    }

    /**
     * Gets the value of the policy property.
     * 
     * @return
     *     possible object is
     *     {@link Policy }
     *     
     */
    public Policy getPolicy() {
        return policy;
    }

    /**
     * Sets the value of the policy property.
     * 
     * @param value
     *     allowed object is
     *     {@link Policy }
     *     
     */
    public void setPolicy(Policy value) {
        this.policy = value;
    }

    /**
     * Gets the value of the declinatureCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeclinatureCode() {
        return declinatureCode;
    }

    /**
     * Sets the value of the declinatureCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeclinatureCode(String value) {
        this.declinatureCode = value;
    }

    /**
     * Gets the value of the getDepth property.
     * 
     * @return
     *     possible object is
     *     {@link GetDepth }
     *     
     */
    public GetDepth getGetDepth() {
        return getDepth;
    }

    /**
     * Sets the value of the getDepth property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetDepth }
     *     
     */
    public void setGetDepth(GetDepth value) {
        this.getDepth = value;
    }

    /**
     * Gets the value of the action property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getAction() {
        return action;
    }

    /**
     * Sets the value of the action property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setAction(Long value) {
        this.action = value;
    }

    /**
     * Gets the value of the contextName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContextName() {
        return contextName;
    }

    /**
     * Sets the value of the contextName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContextName(String value) {
        this.contextName = value;
    }

}
