
package za.co.ominsure.synapse.iwyze.lob.backend.caseItem.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CaseItemAttBlob complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CaseItemAttBlob">
 *   &lt;complexContent>
 *     &lt;extension base="{http://infrastructure.tia.dk/schema/case/v3/}CaseItemAttachment">
 *       &lt;sequence>
 *         &lt;element name="blob" type="{http://infrastructure.tia.dk/schema/common/v2/}BLOB" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CaseItemAttBlob", namespace = "http://infrastructure.tia.dk/schema/case/v3/", propOrder = {
    "blob"
})
public class CaseItemAttBlob
    extends CaseItemAttachment
{

    @XmlElement(nillable = true)
    protected byte[] blob;

    /**
     * Gets the value of the blob property.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getBlob() {
        return blob;
    }

    /**
     * Sets the value of the blob property.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setBlob(byte[] value) {
        this.blob = value;
    }

}
