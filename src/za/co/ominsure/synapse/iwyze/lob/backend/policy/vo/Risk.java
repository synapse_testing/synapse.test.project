
package za.co.ominsure.synapse.iwyze.lob.backend.policy.vo;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * Is mapped into xxxx".
 * 
 * <p>Java class for Risk complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Risk">
 *   &lt;complexContent>
 *     &lt;extension base="{http://infrastructure.tia.dk/schema/common/v2/}TIAObject">
 *       &lt;sequence>
 *         &lt;element name="riskNo" type="{http://infrastructure.tia.dk/schema/policy/v2/}riskNo" minOccurs="0"/>
 *         &lt;element name="riskYn" type="{http://infrastructure.tia.dk/schema/common/v2/}string1" minOccurs="0"/>
 *         &lt;element name="riskExcess" type="{http://infrastructure.tia.dk/schema/policy/v2/}policyAmount" minOccurs="0"/>
 *         &lt;element name="riskSum" type="{http://infrastructure.tia.dk/schema/policy/v2/}policyAmount" minOccurs="0"/>
 *         &lt;element name="riskDiscount" type="{http://infrastructure.tia.dk/schema/policy/v2/}policyAmount" minOccurs="0"/>
 *         &lt;element name="riskTerms" type="{http://infrastructure.tia.dk/schema/policy/v2/}riskTerms" minOccurs="0"/>
 *         &lt;element name="instlPlanType" type="{http://infrastructure.tia.dk/schema/common/v2/}instlPlanType" minOccurs="0"/>
 *         &lt;element name="riskFlex1" type="{http://infrastructure.tia.dk/schema/policy/v2/}riskFlex" minOccurs="0"/>
 *         &lt;element name="riskFlex2" type="{http://infrastructure.tia.dk/schema/policy/v2/}riskFlex" minOccurs="0"/>
 *         &lt;element name="riskFlex3" type="{http://infrastructure.tia.dk/schema/policy/v2/}riskFlex" minOccurs="0"/>
 *         &lt;element name="riskFlex4" type="{http://infrastructure.tia.dk/schema/policy/v2/}riskFlex" minOccurs="0"/>
 *         &lt;element name="clauseCollection" type="{http://infrastructure.tia.dk/schema/policy/v4/}ClauseCollection" minOccurs="0"/>
 *         &lt;element name="tariffBreakdownCollection" type="{http://infrastructure.tia.dk/schema/policy/v4/}TariffBreakdownCollection" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Risk", namespace = "http://infrastructure.tia.dk/schema/policy/v4/", propOrder = {
    "riskNo",
    "riskYn",
    "riskExcess",
    "riskSum",
    "riskDiscount",
    "riskTerms",
    "instlPlanType",
    "riskFlex1",
    "riskFlex2",
    "riskFlex3",
    "riskFlex4",
    "clauseCollection",
    "tariffBreakdownCollection"
})
public class Risk
    extends TIAObject
{

    @XmlSchemaType(name = "unsignedInt")
    protected Long riskNo;
    @XmlElement(nillable = true)
    protected String riskYn;
    @XmlElement(nillable = true)
    protected BigDecimal riskExcess;
    @XmlElement(nillable = true)
    protected BigDecimal riskSum;
    @XmlElement(nillable = true)
    protected BigDecimal riskDiscount;
    @XmlElement(nillable = true)
    protected String riskTerms;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "unsignedInt")
    protected Long instlPlanType;
    @XmlElement(nillable = true)
    protected String riskFlex1;
    @XmlElement(nillable = true)
    protected String riskFlex2;
    @XmlElement(nillable = true)
    protected String riskFlex3;
    @XmlElement(nillable = true)
    protected String riskFlex4;
    protected ClauseCollection clauseCollection;
    protected TariffBreakdownCollection tariffBreakdownCollection;

    /**
     * Gets the value of the riskNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getRiskNo() {
        return riskNo;
    }

    /**
     * Sets the value of the riskNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setRiskNo(Long value) {
        this.riskNo = value;
    }

    /**
     * Gets the value of the riskYn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRiskYn() {
        return riskYn;
    }

    /**
     * Sets the value of the riskYn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRiskYn(String value) {
        this.riskYn = value;
    }

    /**
     * Gets the value of the riskExcess property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRiskExcess() {
        return riskExcess;
    }

    /**
     * Sets the value of the riskExcess property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRiskExcess(BigDecimal value) {
        this.riskExcess = value;
    }

    /**
     * Gets the value of the riskSum property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRiskSum() {
        return riskSum;
    }

    /**
     * Sets the value of the riskSum property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRiskSum(BigDecimal value) {
        this.riskSum = value;
    }

    /**
     * Gets the value of the riskDiscount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRiskDiscount() {
        return riskDiscount;
    }

    /**
     * Sets the value of the riskDiscount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRiskDiscount(BigDecimal value) {
        this.riskDiscount = value;
    }

    /**
     * Gets the value of the riskTerms property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRiskTerms() {
        return riskTerms;
    }

    /**
     * Sets the value of the riskTerms property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRiskTerms(String value) {
        this.riskTerms = value;
    }

    /**
     * Gets the value of the instlPlanType property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getInstlPlanType() {
        return instlPlanType;
    }

    /**
     * Sets the value of the instlPlanType property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setInstlPlanType(Long value) {
        this.instlPlanType = value;
    }

    /**
     * Gets the value of the riskFlex1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRiskFlex1() {
        return riskFlex1;
    }

    /**
     * Sets the value of the riskFlex1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRiskFlex1(String value) {
        this.riskFlex1 = value;
    }

    /**
     * Gets the value of the riskFlex2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRiskFlex2() {
        return riskFlex2;
    }

    /**
     * Sets the value of the riskFlex2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRiskFlex2(String value) {
        this.riskFlex2 = value;
    }

    /**
     * Gets the value of the riskFlex3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRiskFlex3() {
        return riskFlex3;
    }

    /**
     * Sets the value of the riskFlex3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRiskFlex3(String value) {
        this.riskFlex3 = value;
    }

    /**
     * Gets the value of the riskFlex4 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRiskFlex4() {
        return riskFlex4;
    }

    /**
     * Sets the value of the riskFlex4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRiskFlex4(String value) {
        this.riskFlex4 = value;
    }

    /**
     * Gets the value of the clauseCollection property.
     * 
     * @return
     *     possible object is
     *     {@link ClauseCollection }
     *     
     */
    public ClauseCollection getClauseCollection() {
        return clauseCollection;
    }

    /**
     * Sets the value of the clauseCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link ClauseCollection }
     *     
     */
    public void setClauseCollection(ClauseCollection value) {
        this.clauseCollection = value;
    }

    /**
     * Gets the value of the tariffBreakdownCollection property.
     * 
     * @return
     *     possible object is
     *     {@link TariffBreakdownCollection }
     *     
     */
    public TariffBreakdownCollection getTariffBreakdownCollection() {
        return tariffBreakdownCollection;
    }

    /**
     * Sets the value of the tariffBreakdownCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link TariffBreakdownCollection }
     *     
     */
    public void setTariffBreakdownCollection(TariffBreakdownCollection value) {
        this.tariffBreakdownCollection = value;
    }

}
