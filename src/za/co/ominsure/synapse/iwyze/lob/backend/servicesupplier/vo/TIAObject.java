
package za.co.ominsure.synapse.iwyze.lob.backend.servicesupplier.vo;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TIAObject complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TIAObject">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="versionToken" type="{http://infrastructure.tia.dk/schema/common/v2/}VersionToken" minOccurs="0"/>
 *         &lt;element name="countrySpecifics" type="{http://infrastructure.tia.dk/schema/common/v2/}CountrySpecifics" minOccurs="0"/>
 *         &lt;element name="siteSpecifics" type="{http://infrastructure.tia.dk/schema/common/v2/}SiteSpecifics" minOccurs="0"/>
 *         &lt;element name="flexfieldCollection" type="{http://infrastructure.tia.dk/schema/common/v2/}FlexfieldCollection" minOccurs="0"/>
 *         &lt;element name="externalId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TIAObject", namespace = "http://infrastructure.tia.dk/schema/common/v2/", propOrder = {
    "versionToken",
    "countrySpecifics",
    "siteSpecifics",
    "flexfieldCollection",
    "externalId"
})
@XmlSeeAlso({
    Customer.class,
    ContactInfo.class,
    PartyRole.class,
    AbstractParty.class,
    Relation.class,
    Location.class,
    TranslatedCode.class
})
public abstract class TIAObject {

    @XmlElementRef(name = "versionToken", namespace = "http://infrastructure.tia.dk/schema/common/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<VersionToken> versionToken;
    @XmlElementRef(name = "countrySpecifics", namespace = "http://infrastructure.tia.dk/schema/common/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<CountrySpecifics> countrySpecifics;
    @XmlElementRef(name = "siteSpecifics", namespace = "http://infrastructure.tia.dk/schema/common/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<SiteSpecifics> siteSpecifics;
    @XmlElementRef(name = "flexfieldCollection", namespace = "http://infrastructure.tia.dk/schema/common/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<FlexfieldCollection> flexfieldCollection;
    @XmlElementRef(name = "externalId", namespace = "http://infrastructure.tia.dk/schema/common/v2/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> externalId;

    /**
     * Gets the value of the versionToken property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link VersionToken }{@code >}
     *     
     */
    public JAXBElement<VersionToken> getVersionToken() {
        return versionToken;
    }

    /**
     * Sets the value of the versionToken property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link VersionToken }{@code >}
     *     
     */
    public void setVersionToken(JAXBElement<VersionToken> value) {
        this.versionToken = value;
    }

    /**
     * Gets the value of the countrySpecifics property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link CountrySpecifics }{@code >}
     *     
     */
    public JAXBElement<CountrySpecifics> getCountrySpecifics() {
        return countrySpecifics;
    }

    /**
     * Sets the value of the countrySpecifics property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link CountrySpecifics }{@code >}
     *     
     */
    public void setCountrySpecifics(JAXBElement<CountrySpecifics> value) {
        this.countrySpecifics = value;
    }

    /**
     * Gets the value of the siteSpecifics property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link SiteSpecifics }{@code >}
     *     
     */
    public JAXBElement<SiteSpecifics> getSiteSpecifics() {
        return siteSpecifics;
    }

    /**
     * Sets the value of the siteSpecifics property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link SiteSpecifics }{@code >}
     *     
     */
    public void setSiteSpecifics(JAXBElement<SiteSpecifics> value) {
        this.siteSpecifics = value;
    }

    /**
     * Gets the value of the flexfieldCollection property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link FlexfieldCollection }{@code >}
     *     
     */
    public JAXBElement<FlexfieldCollection> getFlexfieldCollection() {
        return flexfieldCollection;
    }

    /**
     * Sets the value of the flexfieldCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link FlexfieldCollection }{@code >}
     *     
     */
    public void setFlexfieldCollection(JAXBElement<FlexfieldCollection> value) {
        this.flexfieldCollection = value;
    }

    /**
     * Gets the value of the externalId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getExternalId() {
        return externalId;
    }

    /**
     * Sets the value of the externalId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setExternalId(JAXBElement<String> value) {
        this.externalId = value;
    }

}
