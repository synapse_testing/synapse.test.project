
package za.co.ominsure.synapse.iwyze.lob.backend.caseItem.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;



/**
 * <p>Java class for CaseItemAttachment complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CaseItemAttachment">
 *   &lt;complexContent>
 *     &lt;extension base="{http://infrastructure.tia.dk/schema/common/v2/}TIAObject">
 *       &lt;sequence>
 *         &lt;element name="documentId" type="{http://infrastructure.tia.dk/schema/common/v2/}documentId" minOccurs="0"/>
 *         &lt;element name="archiveDate" type="{http://infrastructure.tia.dk/schema/common/v2/}date" minOccurs="0"/>
 *         &lt;element name="undeletableYN" type="{http://infrastructure.tia.dk/schema/common/v2/}tiaYN"/>
 *         &lt;element name="documentFormat" type="{http://infrastructure.tia.dk/schema/common/v2/}documentFormat" minOccurs="0"/>
 *         &lt;element name="sensitive" type="{http://infrastructure.tia.dk/schema/common/v2/}int4" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CaseItemAttachment", namespace = "http://infrastructure.tia.dk/schema/case/v2/", propOrder = {
    "documentId",
    "archiveDate",
    "undeletableYN",
    "documentFormat",
    "sensitive"
})
@XmlSeeAlso({
    CaseItemAttClob2 .class,
    CaseItemAttBlob2 .class
})
public abstract class CaseItemAttachment2
    extends TIAObject
{

    @XmlElement(nillable = true)
    @XmlSchemaType(name = "unsignedLong")
    protected Long documentId;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "date")
    protected String archiveDate;
    @XmlElement(required = true)
    protected String undeletableYN;
    @XmlElement(nillable = true)
    protected String documentFormat;
    @XmlElement(nillable = true)
    protected Long sensitive;

    /**
     * Gets the value of the documentId property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getDocumentId() {
        return documentId;
    }

    /**
     * Sets the value of the documentId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setDocumentId(Long value) {
        this.documentId = value;
    }

    /**
     * Gets the value of the archiveDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getArchiveDate() {
        return archiveDate;
    }

    /**
     * Sets the value of the archiveDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setArchiveDate(String value) {
        this.archiveDate = value;
    }

    /**
     * Gets the value of the undeletableYN property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUndeletableYN() {
        return undeletableYN;
    }

    /**
     * Sets the value of the undeletableYN property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUndeletableYN(String value) {
        this.undeletableYN = value;
    }

    /**
     * Gets the value of the documentFormat property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDocumentFormat() {
        return documentFormat;
    }

    /**
     * Sets the value of the documentFormat property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDocumentFormat(String value) {
        this.documentFormat = value;
    }

    /**
     * Gets the value of the sensitive property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getSensitive() {
        return sensitive;
    }

    /**
     * Sets the value of the sensitive property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setSensitive(Long value) {
        this.sensitive = value;
    }

}
