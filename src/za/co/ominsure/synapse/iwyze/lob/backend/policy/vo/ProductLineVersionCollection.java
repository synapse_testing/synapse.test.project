
package za.co.ominsure.synapse.iwyze.lob.backend.policy.vo;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ProductLineVersionCollection complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ProductLineVersionCollection">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="productLineVersion" type="{http://infrastructure.tia.dk/schema/policy/v2/}ProductLineVersion" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProductLineVersionCollection", propOrder = {
    "productLineVersions"
})
public class ProductLineVersionCollection {

    @XmlElement(name = "productLineVersion")
    protected List<ProductLineVersion> productLineVersions;

    /**
     * Gets the value of the productLineVersions property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the productLineVersions property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getProductLineVersions().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProductLineVersion }
     * 
     * 
     */
    public List<ProductLineVersion> getProductLineVersions() {
        if (productLineVersions == null) {
            productLineVersions = new ArrayList<ProductLineVersion>();
        }
        return this.productLineVersions;
    }

}
