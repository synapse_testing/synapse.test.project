
package za.co.ominsure.synapse.iwyze.lob.backend.detail.payment.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * Is mapped into TIA metadata type "obj_filter_token_acc_item".
 * 
 * <p>Java class for FilterTokenAccItem complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FilterTokenAccItem">
 *   &lt;complexContent>
 *     &lt;extension base="{http://infrastructure.tia.dk/schema/account/v2/}FilterTokenAccStatement">
 *       &lt;sequence>
 *         &lt;element name="dateType" type="{http://infrastructure.tia.dk/schema/account/v2/}dateType" minOccurs="0"/>
 *         &lt;element name="accountItemNo" type="{http://infrastructure.tia.dk/schema/common/v2/}seqNo" minOccurs="0"/>
 *         &lt;element name="centerCode" type="{http://infrastructure.tia.dk/schema/common/v2/}centerCode" minOccurs="0"/>
 *         &lt;element name="openItemsOnlyYN" type="{http://infrastructure.tia.dk/schema/common/v2/}tiaYN" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FilterTokenAccItem", propOrder = {
    "dateType",
    "accountItemNo",
    "centerCode",
    "openItemsOnlyYN"
})
public class FilterTokenAccItem
    extends FilterTokenAccStatement
{

    @XmlElement(nillable = true)
    protected String dateType;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "unsignedLong")
    protected Long accountItemNo;
    @XmlElement(nillable = true)
    protected String centerCode;
    @XmlElement(nillable = true)
    protected String openItemsOnlyYN;

    /**
     * Gets the value of the dateType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDateType() {
        return dateType;
    }

    /**
     * Sets the value of the dateType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDateType(String value) {
        this.dateType = value;
    }

    /**
     * Gets the value of the accountItemNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getAccountItemNo() {
        return accountItemNo;
    }

    /**
     * Sets the value of the accountItemNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setAccountItemNo(Long value) {
        this.accountItemNo = value;
    }

    /**
     * Gets the value of the centerCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCenterCode() {
        return centerCode;
    }

    /**
     * Sets the value of the centerCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCenterCode(String value) {
        this.centerCode = value;
    }

    /**
     * Gets the value of the openItemsOnlyYN property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOpenItemsOnlyYN() {
        return openItemsOnlyYN;
    }

    /**
     * Sets the value of the openItemsOnlyYN property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOpenItemsOnlyYN(String value) {
        this.openItemsOnlyYN = value;
    }

}
