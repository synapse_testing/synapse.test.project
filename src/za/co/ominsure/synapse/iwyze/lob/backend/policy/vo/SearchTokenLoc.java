
package za.co.ominsure.synapse.iwyze.lob.backend.policy.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Additional comments
 * 
 * <p>Java class for SearchTokenLoc complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SearchTokenLoc">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="street" type="{http://infrastructure.tia.dk/schema/common/v2/}string70" minOccurs="0"/>
 *         &lt;element name="streetNo" type="{http://infrastructure.tia.dk/schema/common/v2/}string15" minOccurs="0"/>
 *         &lt;element name="postArea" type="{http://infrastructure.tia.dk/schema/common/v2/}string10" minOccurs="0"/>
 *         &lt;element name="postStreet" type="{http://infrastructure.tia.dk/schema/common/v2/}string4" minOccurs="0"/>
 *         &lt;element name="city" type="{http://infrastructure.tia.dk/schema/common/v2/}string70" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SearchTokenLoc", namespace = "http://infrastructure.tia.dk/schema/party/v2/", propOrder = {
    "street",
    "streetNo",
    "postArea",
    "postStreet",
    "city"
})
public class SearchTokenLoc {

    @XmlElement(nillable = true)
    protected String street;
    @XmlElement(nillable = true)
    protected String streetNo;
    @XmlElement(nillable = true)
    protected String postArea;
    @XmlElement(nillable = true)
    protected String postStreet;
    @XmlElement(nillable = true)
    protected String city;

    /**
     * Gets the value of the street property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStreet() {
        return street;
    }

    /**
     * Sets the value of the street property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStreet(String value) {
        this.street = value;
    }

    /**
     * Gets the value of the streetNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStreetNo() {
        return streetNo;
    }

    /**
     * Sets the value of the streetNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStreetNo(String value) {
        this.streetNo = value;
    }

    /**
     * Gets the value of the postArea property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPostArea() {
        return postArea;
    }

    /**
     * Sets the value of the postArea property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPostArea(String value) {
        this.postArea = value;
    }

    /**
     * Gets the value of the postStreet property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPostStreet() {
        return postStreet;
    }

    /**
     * Sets the value of the postStreet property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPostStreet(String value) {
        this.postStreet = value;
    }

    /**
     * Gets the value of the city property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCity() {
        return city;
    }

    /**
     * Sets the value of the city property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCity(String value) {
        this.city = value;
    }

}
