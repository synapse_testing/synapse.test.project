
package za.co.ominsure.synapse.iwyze.lob.backend.policy.vo;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for UIConfiguration complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="UIConfiguration">
 *   &lt;complexContent>
 *     &lt;extension base="{http://infrastructure.tia.dk/schema/common/v2/}TIAObject">
 *       &lt;sequence>
 *         &lt;element name="productLineId" type="{http://infrastructure.tia.dk/schema/policy/v2/}productLineId" minOccurs="0"/>
 *         &lt;element name="objectTypeId" type="{http://infrastructure.tia.dk/schema/common/v2/}string12" minOccurs="0"/>
 *         &lt;element name="uiConfigVersion" type="{http://infrastructure.tia.dk/schema/common/v2/}numberType" minOccurs="0"/>
 *         &lt;element name="uiConfigName" type="{http://infrastructure.tia.dk/schema/common/v2/}string10" minOccurs="0"/>
 *         &lt;element name="description" type="{http://infrastructure.tia.dk/schema/common/v2/}string60" minOccurs="0"/>
 *         &lt;element name="status" type="{http://infrastructure.tia.dk/schema/common/v2/}string4" minOccurs="0"/>
 *         &lt;element name="uiConfigBlocks" type="{http://infrastructure.tia.dk/schema/policy/v4/}UIConfigBlockCollection" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UIConfiguration", namespace = "http://infrastructure.tia.dk/schema/policy/v4/", propOrder = {
    "productLineId",
    "objectTypeId",
    "uiConfigVersion",
    "uiConfigName",
    "description",
    "status",
    "uiConfigBlocks"
})
public class UIConfiguration
    extends TIAObject
{

    @XmlElement(nillable = true)
    protected String productLineId;
    @XmlElement(nillable = true)
    protected String objectTypeId;
    @XmlElement(nillable = true)
    protected BigDecimal uiConfigVersion;
    @XmlElement(nillable = true)
    protected String uiConfigName;
    @XmlElement(nillable = true)
    protected String description;
    @XmlElement(nillable = true)
    protected String status;
    @XmlElement(nillable = true)
    protected UIConfigBlockCollection uiConfigBlocks;

    /**
     * Gets the value of the productLineId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductLineId() {
        return productLineId;
    }

    /**
     * Sets the value of the productLineId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductLineId(String value) {
        this.productLineId = value;
    }

    /**
     * Gets the value of the objectTypeId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getObjectTypeId() {
        return objectTypeId;
    }

    /**
     * Sets the value of the objectTypeId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setObjectTypeId(String value) {
        this.objectTypeId = value;
    }

    /**
     * Gets the value of the uiConfigVersion property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getUiConfigVersion() {
        return uiConfigVersion;
    }

    /**
     * Sets the value of the uiConfigVersion property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setUiConfigVersion(BigDecimal value) {
        this.uiConfigVersion = value;
    }

    /**
     * Gets the value of the uiConfigName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUiConfigName() {
        return uiConfigName;
    }

    /**
     * Sets the value of the uiConfigName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUiConfigName(String value) {
        this.uiConfigName = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

    /**
     * Gets the value of the uiConfigBlocks property.
     * 
     * @return
     *     possible object is
     *     {@link UIConfigBlockCollection }
     *     
     */
    public UIConfigBlockCollection getUiConfigBlocks() {
        return uiConfigBlocks;
    }

    /**
     * Sets the value of the uiConfigBlocks property.
     * 
     * @param value
     *     allowed object is
     *     {@link UIConfigBlockCollection }
     *     
     */
    public void setUiConfigBlocks(UIConfigBlockCollection value) {
        this.uiConfigBlocks = value;
    }

}
