
package za.co.ominsure.synapse.iwyze.lob.backend.policy.vo;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Is mapped into undefined.
 * 
 * <p>Java class for SearchTokenPolicy complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SearchTokenPolicy">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="policyHolderId" type="{http://infrastructure.tia.dk/schema/policy/v2/}seqNo" minOccurs="0"/>
 *         &lt;element name="policyNo" type="{http://infrastructure.tia.dk/schema/common/v2/}policyNo" minOccurs="0"/>
 *         &lt;element name="objectId" type="{http://infrastructure.tia.dk/schema/policy/v2/}objectId" minOccurs="0"/>
 *         &lt;element name="prodId" type="{http://infrastructure.tia.dk/schema/policy/v2/}prodId" minOccurs="0"/>
 *         &lt;element name="productLineId" type="{http://infrastructure.tia.dk/schema/policy/v2/}productLineId" minOccurs="0"/>
 *         &lt;element name="objectTypeId" type="{http://infrastructure.tia.dk/schema/policy/v2/}typeId" minOccurs="0"/>
 *         &lt;element name="coverDate" type="{http://infrastructure.tia.dk/schema/common/v2/}date" minOccurs="0"/>
 *         &lt;element name="allVersionsYN" type="{http://infrastructure.tia.dk/schema/common/v2/}string1" minOccurs="0"/>
 *         &lt;element name="status" type="{http://infrastructure.tia.dk/schema/common/v2/}string1" minOccurs="0"/>
 *         &lt;element name="cancelledYN" type="{http://infrastructure.tia.dk/schema/common/v2/}string1" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SearchTokenPolicy", propOrder = {
    "policyHolderId",
    "policyNo",
    "objectId",
    "prodId",
    "productLineId",
    "objectTypeId",
    "coverDate",
    "allVersionsYN",
    "status",
    "cancelledYN"
})
@XmlSeeAlso({
    SearchTokenPolicy.class
})
public class SearchTokenPolicy2 {

    @XmlSchemaType(name = "unsignedLong")
    protected BigInteger policyHolderId;
    @XmlSchemaType(name = "unsignedLong")
    protected BigInteger policyNo;
    protected String objectId;
    protected String prodId;
    protected String productLineId;
    protected String objectTypeId;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar coverDate;
    protected String allVersionsYN;
    protected String status;
    protected String cancelledYN;

    /**
     * Gets the value of the policyHolderId property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getPolicyHolderId() {
        return policyHolderId;
    }

    /**
     * Sets the value of the policyHolderId property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setPolicyHolderId(BigInteger value) {
        this.policyHolderId = value;
    }

    /**
     * Gets the value of the policyNo property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getPolicyNo() {
        return policyNo;
    }

    /**
     * Sets the value of the policyNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setPolicyNo(BigInteger value) {
        this.policyNo = value;
    }

    /**
     * Gets the value of the objectId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getObjectId() {
        return objectId;
    }

    /**
     * Sets the value of the objectId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setObjectId(String value) {
        this.objectId = value;
    }

    /**
     * Gets the value of the prodId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProdId() {
        return prodId;
    }

    /**
     * Sets the value of the prodId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProdId(String value) {
        this.prodId = value;
    }

    /**
     * Gets the value of the productLineId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductLineId() {
        return productLineId;
    }

    /**
     * Sets the value of the productLineId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductLineId(String value) {
        this.productLineId = value;
    }

    /**
     * Gets the value of the objectTypeId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getObjectTypeId() {
        return objectTypeId;
    }

    /**
     * Sets the value of the objectTypeId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setObjectTypeId(String value) {
        this.objectTypeId = value;
    }

    /**
     * Gets the value of the coverDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCoverDate() {
        return coverDate;
    }

    /**
     * Sets the value of the coverDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCoverDate(XMLGregorianCalendar value) {
        this.coverDate = value;
    }

    /**
     * Gets the value of the allVersionsYN property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAllVersionsYN() {
        return allVersionsYN;
    }

    /**
     * Sets the value of the allVersionsYN property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAllVersionsYN(String value) {
        this.allVersionsYN = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

    /**
     * Gets the value of the cancelledYN property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCancelledYN() {
        return cancelledYN;
    }

    /**
     * Sets the value of the cancelledYN property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCancelledYN(String value) {
        this.cancelledYN = value;
    }

}
