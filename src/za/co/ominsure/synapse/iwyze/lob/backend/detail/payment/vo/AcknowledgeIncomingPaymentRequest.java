
package za.co.ominsure.synapse.iwyze.lob.backend.detail.payment.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="inputToken" type="{http://infrastructure.tia.dk/schema/common/v2/}InputToken"/>
 *         &lt;element name="collectionAcknowledgement" type="{http://infrastructure.tia.dk/schema/account/v2/}CollectionAcknowledgement"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "inputToken",
    "collectionAcknowledgement"
})
@XmlRootElement(name = "acknowledgeIncomingPaymentRequest")
public class AcknowledgeIncomingPaymentRequest {

    @XmlElement(required = true)
    protected InputToken inputToken;
    @XmlElement(required = true)
    protected CollectionAcknowledgement collectionAcknowledgement;

    /**
     * Gets the value of the inputToken property.
     * 
     * @return
     *     possible object is
     *     {@link InputToken }
     *     
     */
    public InputToken getInputToken() {
        return inputToken;
    }

    /**
     * Sets the value of the inputToken property.
     * 
     * @param value
     *     allowed object is
     *     {@link InputToken }
     *     
     */
    public void setInputToken(InputToken value) {
        this.inputToken = value;
    }

    /**
     * Gets the value of the collectionAcknowledgement property.
     * 
     * @return
     *     possible object is
     *     {@link CollectionAcknowledgement }
     *     
     */
    public CollectionAcknowledgement getCollectionAcknowledgement() {
        return collectionAcknowledgement;
    }

    /**
     * Sets the value of the collectionAcknowledgement property.
     * 
     * @param value
     *     allowed object is
     *     {@link CollectionAcknowledgement }
     *     
     */
    public void setCollectionAcknowledgement(CollectionAcknowledgement value) {
        this.collectionAcknowledgement = value;
    }

}
