
package za.co.ominsure.synapse.iwyze.lob.backend.policy.vo;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for requestedState.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="requestedState">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="QUOTE"/>
 *     &lt;enumeration value="DECLINED_QUOTE"/>
 *     &lt;enumeration value="REFERRED_QUOTE"/>
 *     &lt;enumeration value="SUSPENDED_QUOTE"/>
 *     &lt;enumeration value="POLICY"/>
 *     &lt;enumeration value="REFERRED_POLICY"/>
 *     &lt;enumeration value="SUSPENDED_POLICY"/>
 *     &lt;enumeration value="CANCELLED_POLICY"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "requestedState")
@XmlEnum
public enum RequestedState {

    QUOTE,
    DECLINED_QUOTE,
    REFERRED_QUOTE,
    SUSPENDED_QUOTE,
    POLICY,
    REFERRED_POLICY,
    SUSPENDED_POLICY,
    CANCELLED_POLICY;

    public String value() {
        return name();
    }

    public static RequestedState fromValue(String v) {
        return valueOf(v);
    }

}
