
package za.co.ominsure.synapse.iwyze.lob.backend.policy.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Additional comments
 * 
 * <p>Java class for InsuranceCompany complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="InsuranceCompany">
 *   &lt;complexContent>
 *     &lt;extension base="{http://infrastructure.tia.dk/schema/party/v2/}PartyRole">
 *       &lt;sequence>
 *         &lt;element name="insurerCode" type="{http://infrastructure.tia.dk/schema/common/v2/}string3" minOccurs="0"/>
 *         &lt;element name="insuranceCompanyName" type="{http://infrastructure.tia.dk/schema/common/v2/}string60" minOccurs="0"/>
 *         &lt;element name="parentInsurerCode" type="{http://infrastructure.tia.dk/schema/common/v2/}string3" minOccurs="0"/>
 *         &lt;element name="periodOfCancellation" type="{http://infrastructure.tia.dk/schema/common/v2/}int3" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InsuranceCompany", namespace = "http://infrastructure.tia.dk/schema/party/v2/", propOrder = {
    "insurerCode",
    "insuranceCompanyName",
    "parentInsurerCode",
    "periodOfCancellation"
})
public class InsuranceCompany
    extends PartyRole
{

    @XmlElement(nillable = true)
    protected String insurerCode;
    @XmlElement(nillable = true)
    protected String insuranceCompanyName;
    @XmlElement(nillable = true)
    protected String parentInsurerCode;
    @XmlElement(nillable = true)
    protected Long periodOfCancellation;

    /**
     * Gets the value of the insurerCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInsurerCode() {
        return insurerCode;
    }

    /**
     * Sets the value of the insurerCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInsurerCode(String value) {
        this.insurerCode = value;
    }

    /**
     * Gets the value of the insuranceCompanyName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInsuranceCompanyName() {
        return insuranceCompanyName;
    }

    /**
     * Sets the value of the insuranceCompanyName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInsuranceCompanyName(String value) {
        this.insuranceCompanyName = value;
    }

    /**
     * Gets the value of the parentInsurerCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getParentInsurerCode() {
        return parentInsurerCode;
    }

    /**
     * Sets the value of the parentInsurerCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setParentInsurerCode(String value) {
        this.parentInsurerCode = value;
    }

    /**
     * Gets the value of the periodOfCancellation property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getPeriodOfCancellation() {
        return periodOfCancellation;
    }

    /**
     * Sets the value of the periodOfCancellation property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setPeriodOfCancellation(Long value) {
        this.periodOfCancellation = value;
    }

}
