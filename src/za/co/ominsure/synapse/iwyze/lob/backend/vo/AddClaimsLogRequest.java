package za.co.ominsure.synapse.iwyze.lob.backend.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "claimNo", "eventNo", "description", "metadata" })
@XmlRootElement(name = "AddClaimsLogRequest")
public class AddClaimsLogRequest {
	@XmlElement(name = "claimNo")
	private String claimNo;
	@XmlElement(name = "eventNo")
	private String eventNo;
	@XmlElement(name = "description")
	private String description;
	@XmlElement(name = "metadata")
	private Metadata metadata;

	public String getClaimNo() {
		return claimNo;
	}

	public void setClaimNo(String input) {
		this.claimNo = input;
	}

	public String getEventNo() {
		return eventNo;
	}

	public void setEventNo(String input) {
		this.eventNo = input;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String input) {
		this.description = input;
	}

	public Metadata getMetadata() {
		if (metadata == null)
			metadata = new Metadata();
		return metadata;
	}

	public void setMetadata(Metadata input) {
		this.metadata = input;
	}
}