
package za.co.ominsure.synapse.iwyze.lob.backend.party.vo;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * Additional comments
 * 
 * <p>Java class for SearchTokenLoc complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SearchTokenLoc">
 *   &lt;complexContent>
 *     &lt;extension base="{http://infrastructure.tia.dk/schema/party/v2/}SearchTokenLoc">
 *       &lt;sequence>
 *         &lt;element name="addressCode" type="{http://infrastructure.tia.dk/schema/common/v2/}string100" minOccurs="0"/>
 *         &lt;element name="siteShortName" type="{http://infrastructure.tia.dk/schema/common/v2/}string10" minOccurs="0"/>
 *         &lt;element name="searchMethod" type="{http://infrastructure.tia.dk/schema/common/v2/}string20" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SearchTokenLoc", namespace = "http://infrastructure.tia.dk/schema/party/v3/", propOrder = {
    "addressCode",
    "siteShortName",
    "searchMethod"
})
public class SearchTokenLoc
    extends SearchTokenLoc2
{

    @XmlElementRef(name = "addressCode", namespace = "http://infrastructure.tia.dk/schema/party/v3/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> addressCode;
    @XmlElementRef(name = "siteShortName", namespace = "http://infrastructure.tia.dk/schema/party/v3/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> siteShortName;
    @XmlElementRef(name = "searchMethod", namespace = "http://infrastructure.tia.dk/schema/party/v3/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> searchMethod;

    /**
     * Gets the value of the addressCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAddressCode() {
        return addressCode;
    }

    /**
     * Sets the value of the addressCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAddressCode(JAXBElement<String> value) {
        this.addressCode = value;
    }

    /**
     * Gets the value of the siteShortName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSiteShortName() {
        return siteShortName;
    }

    /**
     * Sets the value of the siteShortName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSiteShortName(JAXBElement<String> value) {
        this.siteShortName = value;
    }

    /**
     * Gets the value of the searchMethod property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSearchMethod() {
        return searchMethod;
    }

    /**
     * Sets the value of the searchMethod property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSearchMethod(JAXBElement<String> value) {
        this.searchMethod = value;
    }

}
