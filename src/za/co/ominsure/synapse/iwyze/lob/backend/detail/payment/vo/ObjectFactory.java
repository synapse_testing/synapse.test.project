
package za.co.ominsure.synapse.iwyze.lob.backend.detail.payment.vo;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the za.co.ominsure.synapse.iwyze.lob.backend.detail.payment.vo package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: za.co.ominsure.synapse.iwyze.lob.backend.detail.payment.vo
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetRemindingHistoryRequest }
     * 
     */
    public GetRemindingHistoryRequest createGetRemindingHistoryRequest() {
        return new GetRemindingHistoryRequest();
    }

    /**
     * Create an instance of {@link InputToken }
     * 
     */
    public InputToken createInputToken() {
        return new InputToken();
    }

    /**
     * Create an instance of {@link FilterTokenRemHistory }
     * 
     */
    public FilterTokenRemHistory createFilterTokenRemHistory() {
        return new FilterTokenRemHistory();
    }

    /**
     * Create an instance of {@link ModifyReminderPostponementRequest }
     * 
     */
    public ModifyReminderPostponementRequest createModifyReminderPostponementRequest() {
        return new ModifyReminderPostponementRequest();
    }

    /**
     * Create an instance of {@link ReminderPostponement }
     * 
     */
    public ReminderPostponement createReminderPostponement() {
        return new ReminderPostponement();
    }

    /**
     * Create an instance of {@link RemoveReminderPostponementRequest }
     * 
     */
    public RemoveReminderPostponementRequest createRemoveReminderPostponementRequest() {
        return new RemoveReminderPostponementRequest();
    }

    /**
     * Create an instance of {@link GetAccountItemsResponse }
     * 
     */
    public GetAccountItemsResponse createGetAccountItemsResponse() {
        return new GetAccountItemsResponse();
    }

    /**
     * Create an instance of {@link AccountItemCollection }
     * 
     */
    public AccountItemCollection createAccountItemCollection() {
        return new AccountItemCollection();
    }

    /**
     * Create an instance of {@link Result }
     * 
     */
    public Result createResult() {
        return new Result();
    }

    /**
     * Create an instance of {@link GetPaymentMethodsResponse }
     * 
     */
    public GetPaymentMethodsResponse createGetPaymentMethodsResponse() {
        return new GetPaymentMethodsResponse();
    }

    /**
     * Create an instance of {@link PaymentMethodCollection }
     * 
     */
    public PaymentMethodCollection createPaymentMethodCollection() {
        return new PaymentMethodCollection();
    }

    /**
     * Create an instance of {@link GetRemindingHistoryResponse }
     * 
     */
    public GetRemindingHistoryResponse createGetRemindingHistoryResponse() {
        return new GetRemindingHistoryResponse();
    }

    /**
     * Create an instance of {@link RemindingHistoryCollection }
     * 
     */
    public RemindingHistoryCollection createRemindingHistoryCollection() {
        return new RemindingHistoryCollection();
    }

    /**
     * Create an instance of {@link GetPaymentDetailsRequest }
     * 
     */
    public GetPaymentDetailsRequest createGetPaymentDetailsRequest() {
        return new GetPaymentDetailsRequest();
    }

    /**
     * Create an instance of {@link RegisterCollectionRequestResponse }
     * 
     */
    public RegisterCollectionRequestResponse createRegisterCollectionRequestResponse() {
        return new RegisterCollectionRequestResponse();
    }

    /**
     * Create an instance of {@link ModifyReminderPostponementResponse }
     * 
     */
    public ModifyReminderPostponementResponse createModifyReminderPostponementResponse() {
        return new ModifyReminderPostponementResponse();
    }

    /**
     * Create an instance of {@link RegisterCollectionRequestRequest }
     * 
     */
    public RegisterCollectionRequestRequest createRegisterCollectionRequestRequest() {
        return new RegisterCollectionRequestRequest();
    }

    /**
     * Create an instance of {@link CollectionRequestConfirm }
     * 
     */
    public CollectionRequestConfirm createCollectionRequestConfirm() {
        return new CollectionRequestConfirm();
    }

    /**
     * Create an instance of {@link SearchPaymentResponse }
     * 
     */
    public SearchPaymentResponse createSearchPaymentResponse() {
        return new SearchPaymentResponse();
    }

    /**
     * Create an instance of {@link PaymentCollection }
     * 
     */
    public PaymentCollection createPaymentCollection() {
        return new PaymentCollection();
    }

    /**
     * Create an instance of {@link GetPaymentMethodsRequest }
     * 
     */
    public GetPaymentMethodsRequest createGetPaymentMethodsRequest() {
        return new GetPaymentMethodsRequest();
    }

    /**
     * Create an instance of {@link SearchPaymentRequest }
     * 
     */
    public SearchPaymentRequest createSearchPaymentRequest() {
        return new SearchPaymentRequest();
    }

    /**
     * Create an instance of {@link SearchTokenPayment }
     * 
     */
    public SearchTokenPayment createSearchTokenPayment() {
        return new SearchTokenPayment();
    }

    /**
     * Create an instance of {@link GetPaymentChannelsResponse }
     * 
     */
    public GetPaymentChannelsResponse createGetPaymentChannelsResponse() {
        return new GetPaymentChannelsResponse();
    }

    /**
     * Create an instance of {@link PaymentChannelCollection }
     * 
     */
    public PaymentChannelCollection createPaymentChannelCollection() {
        return new PaymentChannelCollection();
    }

    /**
     * Create an instance of {@link GetAccountStatementRequest }
     * 
     */
    public GetAccountStatementRequest createGetAccountStatementRequest() {
        return new GetAccountStatementRequest();
    }

    /**
     * Create an instance of {@link FilterTokenAccStatement }
     * 
     */
    public FilterTokenAccStatement createFilterTokenAccStatement() {
        return new FilterTokenAccStatement();
    }

    /**
     * Create an instance of {@link PageSort }
     * 
     */
    public PageSort createPageSort() {
        return new PageSort();
    }

    /**
     * Create an instance of {@link GetPaymentTermsRequest }
     * 
     */
    public GetPaymentTermsRequest createGetPaymentTermsRequest() {
        return new GetPaymentTermsRequest();
    }

    /**
     * Create an instance of {@link SearchAccountResponse }
     * 
     */
    public SearchAccountResponse createSearchAccountResponse() {
        return new SearchAccountResponse();
    }

    /**
     * Create an instance of {@link AccountCollection }
     * 
     */
    public AccountCollection createAccountCollection() {
        return new AccountCollection();
    }

    /**
     * Create an instance of {@link CreateAccountItemRequest }
     * 
     */
    public CreateAccountItemRequest createCreateAccountItemRequest() {
        return new CreateAccountItemRequest();
    }

    /**
     * Create an instance of {@link AccountItem }
     * 
     */
    public AccountItem createAccountItem() {
        return new AccountItem();
    }

    /**
     * Create an instance of {@link CreatePaymentDetailsRequest }
     * 
     */
    public CreatePaymentDetailsRequest createCreatePaymentDetailsRequest() {
        return new CreatePaymentDetailsRequest();
    }

    /**
     * Create an instance of {@link PaymentDetails }
     * 
     */
    public PaymentDetails createPaymentDetails() {
        return new PaymentDetails();
    }

    /**
     * Create an instance of {@link CreatePaymentDetailsResponse }
     * 
     */
    public CreatePaymentDetailsResponse createCreatePaymentDetailsResponse() {
        return new CreatePaymentDetailsResponse();
    }

    /**
     * Create an instance of {@link GetAccountItemsRequest }
     * 
     */
    public GetAccountItemsRequest createGetAccountItemsRequest() {
        return new GetAccountItemsRequest();
    }

    /**
     * Create an instance of {@link FilterTokenAccItem }
     * 
     */
    public FilterTokenAccItem createFilterTokenAccItem() {
        return new FilterTokenAccItem();
    }

    /**
     * Create an instance of {@link CreateAccountItemResponse }
     * 
     */
    public CreateAccountItemResponse createCreateAccountItemResponse() {
        return new CreateAccountItemResponse();
    }

    /**
     * Create an instance of {@link SearchAccountRequest }
     * 
     */
    public SearchAccountRequest createSearchAccountRequest() {
        return new SearchAccountRequest();
    }

    /**
     * Create an instance of {@link SearchTokenAccount }
     * 
     */
    public SearchTokenAccount createSearchTokenAccount() {
        return new SearchTokenAccount();
    }

    /**
     * Create an instance of {@link FaultMessageDetail }
     * 
     */
    public FaultMessageDetail createFaultMessageDetail() {
        return new FaultMessageDetail();
    }

    /**
     * Create an instance of {@link AcknowledgeIncomingPaymentRequest }
     * 
     */
    public AcknowledgeIncomingPaymentRequest createAcknowledgeIncomingPaymentRequest() {
        return new AcknowledgeIncomingPaymentRequest();
    }

    /**
     * Create an instance of {@link CollectionAcknowledgement }
     * 
     */
    public CollectionAcknowledgement createCollectionAcknowledgement() {
        return new CollectionAcknowledgement();
    }

    /**
     * Create an instance of {@link GetReminderPostponementsRequest }
     * 
     */
    public GetReminderPostponementsRequest createGetReminderPostponementsRequest() {
        return new GetReminderPostponementsRequest();
    }

    /**
     * Create an instance of {@link GetPaymentChannelsRequest }
     * 
     */
    public GetPaymentChannelsRequest createGetPaymentChannelsRequest() {
        return new GetPaymentChannelsRequest();
    }

    /**
     * Create an instance of {@link AcknowledgeIncomingPaymentResponse }
     * 
     */
    public AcknowledgeIncomingPaymentResponse createAcknowledgeIncomingPaymentResponse() {
        return new AcknowledgeIncomingPaymentResponse();
    }

    /**
     * Create an instance of {@link ModifyPaymentDetailsResponse }
     * 
     */
    public ModifyPaymentDetailsResponse createModifyPaymentDetailsResponse() {
        return new ModifyPaymentDetailsResponse();
    }

    /**
     * Create an instance of {@link GetPaymentDetailsResponse }
     * 
     */
    public GetPaymentDetailsResponse createGetPaymentDetailsResponse() {
        return new GetPaymentDetailsResponse();
    }

    /**
     * Create an instance of {@link PaymentDetailsCollection }
     * 
     */
    public PaymentDetailsCollection createPaymentDetailsCollection() {
        return new PaymentDetailsCollection();
    }

    /**
     * Create an instance of {@link CreateReminderPostponementRequest }
     * 
     */
    public CreateReminderPostponementRequest createCreateReminderPostponementRequest() {
        return new CreateReminderPostponementRequest();
    }

    /**
     * Create an instance of {@link ModifyPaymentDetailsRequest }
     * 
     */
    public ModifyPaymentDetailsRequest createModifyPaymentDetailsRequest() {
        return new ModifyPaymentDetailsRequest();
    }

    /**
     * Create an instance of {@link PreparePolicyPremiumPaymentResponse }
     * 
     */
    public PreparePolicyPremiumPaymentResponse createPreparePolicyPremiumPaymentResponse() {
        return new PreparePolicyPremiumPaymentResponse();
    }

    /**
     * Create an instance of {@link Payment }
     * 
     */
    public Payment createPayment() {
        return new Payment();
    }

    /**
     * Create an instance of {@link CreateReminderPostponementResponse }
     * 
     */
    public CreateReminderPostponementResponse createCreateReminderPostponementResponse() {
        return new CreateReminderPostponementResponse();
    }

    /**
     * Create an instance of {@link RemoveReminderPostponementResponse }
     * 
     */
    public RemoveReminderPostponementResponse createRemoveReminderPostponementResponse() {
        return new RemoveReminderPostponementResponse();
    }

    /**
     * Create an instance of {@link GetReminderPostponementsResponse }
     * 
     */
    public GetReminderPostponementsResponse createGetReminderPostponementsResponse() {
        return new GetReminderPostponementsResponse();
    }

    /**
     * Create an instance of {@link ReminderPostponementCollection }
     * 
     */
    public ReminderPostponementCollection createReminderPostponementCollection() {
        return new ReminderPostponementCollection();
    }

    /**
     * Create an instance of {@link GetAccountStatementResponse }
     * 
     */
    public GetAccountStatementResponse createGetAccountStatementResponse() {
        return new GetAccountStatementResponse();
    }

    /**
     * Create an instance of {@link AccountStatement }
     * 
     */
    public AccountStatement createAccountStatement() {
        return new AccountStatement();
    }

    /**
     * Create an instance of {@link GetPaymentTermsResponse }
     * 
     */
    public GetPaymentTermsResponse createGetPaymentTermsResponse() {
        return new GetPaymentTermsResponse();
    }

    /**
     * Create an instance of {@link PaymentTermsCollection }
     * 
     */
    public PaymentTermsCollection createPaymentTermsCollection() {
        return new PaymentTermsCollection();
    }

    /**
     * Create an instance of {@link PreparePolicyPremiumPaymentRequest }
     * 
     */
    public PreparePolicyPremiumPaymentRequest createPreparePolicyPremiumPaymentRequest() {
        return new PreparePolicyPremiumPaymentRequest();
    }

    /**
     * Create an instance of {@link Account }
     * 
     */
    public Account createAccount() {
        return new Account();
    }

    /**
     * Create an instance of {@link InstalmentPlan }
     * 
     */
    public InstalmentPlan createInstalmentPlan() {
        return new InstalmentPlan();
    }

    /**
     * Create an instance of {@link PlannedInstalmentCollection }
     * 
     */
    public PlannedInstalmentCollection createPlannedInstalmentCollection() {
        return new PlannedInstalmentCollection();
    }

    /**
     * Create an instance of {@link PaymentSpecificationCollection }
     * 
     */
    public PaymentSpecificationCollection createPaymentSpecificationCollection() {
        return new PaymentSpecificationCollection();
    }

    /**
     * Create an instance of {@link ReminderSpecificationCollection }
     * 
     */
    public ReminderSpecificationCollection createReminderSpecificationCollection() {
        return new ReminderSpecificationCollection();
    }

    /**
     * Create an instance of {@link AccountStatementItem }
     * 
     */
    public AccountStatementItem createAccountStatementItem() {
        return new AccountStatementItem();
    }

    /**
     * Create an instance of {@link ExchangeRateCollection }
     * 
     */
    public ExchangeRateCollection createExchangeRateCollection() {
        return new ExchangeRateCollection();
    }

    /**
     * Create an instance of {@link Reminder }
     * 
     */
    public Reminder createReminder() {
        return new Reminder();
    }

    /**
     * Create an instance of {@link PaymentItemCollection }
     * 
     */
    public PaymentItemCollection createPaymentItemCollection() {
        return new PaymentItemCollection();
    }

    /**
     * Create an instance of {@link RemindingHistory }
     * 
     */
    public RemindingHistory createRemindingHistory() {
        return new RemindingHistory();
    }

    /**
     * Create an instance of {@link PaymentItem }
     * 
     */
    public PaymentItem createPaymentItem() {
        return new PaymentItem();
    }

    /**
     * Create an instance of {@link RemindedItem }
     * 
     */
    public RemindedItem createRemindedItem() {
        return new RemindedItem();
    }

    /**
     * Create an instance of {@link ReminderCollection }
     * 
     */
    public ReminderCollection createReminderCollection() {
        return new ReminderCollection();
    }

    /**
     * Create an instance of {@link AccountStatementItemCollection }
     * 
     */
    public AccountStatementItemCollection createAccountStatementItemCollection() {
        return new AccountStatementItemCollection();
    }

    /**
     * Create an instance of {@link AccountItemMatchCollection }
     * 
     */
    public AccountItemMatchCollection createAccountItemMatchCollection() {
        return new AccountItemMatchCollection();
    }

    /**
     * Create an instance of {@link ReminderSpecification }
     * 
     */
    public ReminderSpecification createReminderSpecification() {
        return new ReminderSpecification();
    }

    /**
     * Create an instance of {@link InstalmentPlanCollection }
     * 
     */
    public InstalmentPlanCollection createInstalmentPlanCollection() {
        return new InstalmentPlanCollection();
    }

    /**
     * Create an instance of {@link CurrencyCollection }
     * 
     */
    public CurrencyCollection createCurrencyCollection() {
        return new CurrencyCollection();
    }

    /**
     * Create an instance of {@link PaymentMethod }
     * 
     */
    public PaymentMethod createPaymentMethod() {
        return new PaymentMethod();
    }

    /**
     * Create an instance of {@link GetTokenInstalmentPlan }
     * 
     */
    public GetTokenInstalmentPlan createGetTokenInstalmentPlan() {
        return new GetTokenInstalmentPlan();
    }

    /**
     * Create an instance of {@link ExchangeRate }
     * 
     */
    public ExchangeRate createExchangeRate() {
        return new ExchangeRate();
    }

    /**
     * Create an instance of {@link PaymentChannel }
     * 
     */
    public PaymentChannel createPaymentChannel() {
        return new PaymentChannel();
    }

    /**
     * Create an instance of {@link PaymentTerms }
     * 
     */
    public PaymentTerms createPaymentTerms() {
        return new PaymentTerms();
    }

    /**
     * Create an instance of {@link RemindedItemCollection }
     * 
     */
    public RemindedItemCollection createRemindedItemCollection() {
        return new RemindedItemCollection();
    }

    /**
     * Create an instance of {@link PlannedInstalment }
     * 
     */
    public PlannedInstalment createPlannedInstalment() {
        return new PlannedInstalment();
    }

    /**
     * Create an instance of {@link AccountItemMatch }
     * 
     */
    public AccountItemMatch createAccountItemMatch() {
        return new AccountItemMatch();
    }

    /**
     * Create an instance of {@link PaymentSpecification }
     * 
     */
    public PaymentSpecification createPaymentSpecification() {
        return new PaymentSpecification();
    }

    /**
     * Create an instance of {@link FaultMessageDetail2 }
     * 
     */
    public FaultMessageDetail2 createFaultMessageDetail2() {
        return new FaultMessageDetail2();
    }

    /**
     * Create an instance of {@link FlexfieldCollection }
     * 
     */
    public FlexfieldCollection createFlexfieldCollection() {
        return new FlexfieldCollection();
    }

    /**
     * Create an instance of {@link ValueTranslationPolicy }
     * 
     */
    public ValueTranslationPolicy createValueTranslationPolicy() {
        return new ValueTranslationPolicy();
    }

    /**
     * Create an instance of {@link NumberAttribute }
     * 
     */
    public NumberAttribute createNumberAttribute() {
        return new NumberAttribute();
    }

    /**
     * Create an instance of {@link KeyPair }
     * 
     */
    public KeyPair createKeyPair() {
        return new KeyPair();
    }

    /**
     * Create an instance of {@link TranslatedCode }
     * 
     */
    public TranslatedCode createTranslatedCode() {
        return new TranslatedCode();
    }

    /**
     * Create an instance of {@link ValueTranslationCollection }
     * 
     */
    public ValueTranslationCollection createValueTranslationCollection() {
        return new ValueTranslationCollection();
    }

    /**
     * Create an instance of {@link VersionToken }
     * 
     */
    public VersionToken createVersionToken() {
        return new VersionToken();
    }

    /**
     * Create an instance of {@link StringCollection }
     * 
     */
    public StringCollection createStringCollection() {
        return new StringCollection();
    }

    /**
     * Create an instance of {@link Attribute }
     * 
     */
    public Attribute createAttribute() {
        return new Attribute();
    }

    /**
     * Create an instance of {@link ValueTranslationClaim }
     * 
     */
    public ValueTranslationClaim createValueTranslationClaim() {
        return new ValueTranslationClaim();
    }

    /**
     * Create an instance of {@link VarcharAttribute }
     * 
     */
    public VarcharAttribute createVarcharAttribute() {
        return new VarcharAttribute();
    }

    /**
     * Create an instance of {@link MessageDetail }
     * 
     */
    public MessageDetail createMessageDetail() {
        return new MessageDetail();
    }

    /**
     * Create an instance of {@link KeyPairCollection }
     * 
     */
    public KeyPairCollection createKeyPairCollection() {
        return new KeyPairCollection();
    }

    /**
     * Create an instance of {@link Email }
     * 
     */
    public Email createEmail() {
        return new Email();
    }

    /**
     * Create an instance of {@link TranslatedCodeCollection }
     * 
     */
    public TranslatedCodeCollection createTranslatedCodeCollection() {
        return new TranslatedCodeCollection();
    }

    /**
     * Create an instance of {@link Flexfield }
     * 
     */
    public Flexfield createFlexfield() {
        return new Flexfield();
    }

    /**
     * Create an instance of {@link DateAttribute }
     * 
     */
    public DateAttribute createDateAttribute() {
        return new DateAttribute();
    }

    /**
     * Create an instance of {@link NumberCollection }
     * 
     */
    public NumberCollection createNumberCollection() {
        return new NumberCollection();
    }

    /**
     * Create an instance of {@link Value }
     * 
     */
    public Value createValue() {
        return new Value();
    }

    /**
     * Create an instance of {@link SiteSpecifics }
     * 
     */
    public SiteSpecifics createSiteSpecifics() {
        return new SiteSpecifics();
    }

    /**
     * Create an instance of {@link ValueTranslationPolicyCollection }
     * 
     */
    public ValueTranslationPolicyCollection createValueTranslationPolicyCollection() {
        return new ValueTranslationPolicyCollection();
    }

    /**
     * Create an instance of {@link ValueTranslation }
     * 
     */
    public ValueTranslation createValueTranslation() {
        return new ValueTranslation();
    }

    /**
     * Create an instance of {@link MessageCollection }
     * 
     */
    public MessageCollection createMessageCollection() {
        return new MessageCollection();
    }

    /**
     * Create an instance of {@link Number }
     * 
     */
    public Number createNumber() {
        return new Number();
    }

    /**
     * Create an instance of {@link CountrySpecifics }
     * 
     */
    public CountrySpecifics createCountrySpecifics() {
        return new CountrySpecifics();
    }

    /**
     * Create an instance of {@link EntityAttributeCollection }
     * 
     */
    public EntityAttributeCollection createEntityAttributeCollection() {
        return new EntityAttributeCollection();
    }

    /**
     * Create an instance of {@link ValueTranslationClaimCollection }
     * 
     */
    public ValueTranslationClaimCollection createValueTranslationClaimCollection() {
        return new ValueTranslationClaimCollection();
    }

}
