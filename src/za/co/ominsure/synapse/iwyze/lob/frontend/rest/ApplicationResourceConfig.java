package za.co.ominsure.synapse.iwyze.lob.frontend.rest;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.server.ResourceConfig;

import za.co.ominsure.synapse.common.filter.CORSRequestFilter;
import za.co.ominsure.synapse.common.filter.CORSResponseFilter;
import za.co.ominsure.synapse.common.filter.ServiceResponseAuditFilter;
import za.co.ominsure.synapse.common.filter.ServiceSecurityAuditRequestFilter;


@ApplicationPath("rest")
public class ApplicationResourceConfig extends ResourceConfig {

    public ApplicationResourceConfig() {
    	register(IwyzeRest.class);
        register(CORSRequestFilter.class);
        register(ServiceSecurityAuditRequestFilter.class);
        register(ServiceResponseAuditFilter.class);
        register(CORSResponseFilter.class);
    }
}
