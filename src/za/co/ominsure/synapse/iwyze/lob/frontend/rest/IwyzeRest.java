package za.co.ominsure.synapse.iwyze.lob.frontend.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import za.co.ominsure.synapse.cdm.claim.Audits;
import za.co.ominsure.synapse.cdm.claim.Case;
import za.co.ominsure.synapse.cdm.claim.Claim;
import za.co.ominsure.synapse.cdm.common.content.SearchRequest;
import za.co.ominsure.synapse.cdm.common.content.SearchRequest.SearchFields;
import za.co.ominsure.synapse.cdm.common.content.SearchRequest.SearchFields.Operator;
import za.co.ominsure.synapse.cdm.common.content.SearchRequest.SearchFields.SearchField;
import za.co.ominsure.synapse.cdm.common.content.SearchResult;
import za.co.ominsure.synapse.cdm.finance.Payments;
import za.co.ominsure.synapse.cdm.internal.error.ErrorResponse;
import za.co.ominsure.synapse.cdm.nonapproved.ThirdParties;
import za.co.ominsure.synapse.cdm.policy.PolicyOld;
import za.co.ominsure.synapse.cdm.workflow.ajs.CreateRequest;
import za.co.ominsure.synapse.cdm.workflow.ajs.RecoveryStatusChangeRequest;
import za.co.ominsure.synapse.common.constants.SynapseConstants;
import za.co.ominsure.synapse.common.exception.HttpException;
import za.co.ominsure.synapse.common.factory.BeanFactory;
import za.co.ominsure.synapse.common.log.Synlog;
import za.co.ominsure.synapse.common.util.ExceptionUtil;
import za.co.ominsure.synapse.iwyze.lob.backend.Facade;
import za.co.ominsure.synapse.iwyze.lob.backend.caseItem.vo.CaseItemGenericResponse;
import za.co.ominsure.synapse.iwyze.lob.backend.servicesupplier.vo.GetServiceSupplierResponse;
import za.co.ominsure.synapse.iwyze.lob.backend.vo.AddClaimsLogRequest;
import za.co.ominsure.synapse.iwyze.lob.backend.vo.AddClaimsLogResponse;
import za.co.ominsure.synapse.iwyze.lob.backend.vo.GetClaimsLogResponse;
import za.co.ominsure.synapse.iwyze.lob.backend.vo.GetPolicyNumberResponse;

@Path("")
public class IwyzeRest {
	private static final String CLAIM_EVENT_NUMBER = "claimEventNumber=";
	private static final String CASE_NUMBER = ";caseNumber=";
	private static final String CLAIM_NUMBER = "claim number = ";
	private static final String POLICY_HOLDER_ID = "policy holder id = ";
	private static final String RECEIVER_ID_NO = "receiver id no = ";
	private static final String POLICY_NUMBER = "policyNumber=";

	/**
	 * [addClaimAudit]
	 * <p>
	 * Adds a message to the claims log of a claim.
	 * </p>
	 * <p>
	 * <strong>Dependencies</strong>
	 * <ul>
	 * </ul>
	 * </p>
	 * 
	 * @param audits
	 *            A list of Audit data to add to the Audit logs
	 * @return Response.Status.CREATED on success else Response.Status.INTERNAL_SERVER_ERROR on error
	 */
	@POST
	@Path("/claim/audit")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Response addClaimAudit(Audits audits) {
		try {
			if (audits == null || audits.getAudit().isEmpty())
				return Response.status(Response.Status.BAD_REQUEST).build();
			Facade facade = (Facade) BeanFactory.lookupBean(SynapseConstants.FACADE_JNDI);
			facade.addAudit(audits);
			return Response.status(Response.Status.NO_CONTENT).build();
		}
		catch (HttpException e) {
			Synlog.error(e.getMessage(), ExceptionUtil.getStackTrace(e));
			return Response.status(e.getStatus()).entity(new ErrorResponse(e.getStatus(), e.getMessage())).build();
		}
		catch (Exception e) {
			Synlog.error(e.getMessage(), ExceptionUtil.getStackTrace(e));
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(new ErrorResponse(500, e.getMessage())).build();
		}
	}

	/**
	 * [addClaimLog]
	 * <p>
	 * Adds a message to the claims log of a iwyze REST API call.
	 * </p>
	 * <p>
	 * <strong>Dependencies</strong>
	 * <ul>
	 * </ul>
	 * </p>
	 * 
	 * @param AddClaimsLogRequest
	 * 
	 * @return AddClaimsLogResponse
	 */
	@POST
	@Path("/claim/{claimEventNumber}/audit")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Response addClaimLog(AddClaimsLogRequest request) {
		try {
			if (request == null)
				return Response.status(Response.Status.BAD_REQUEST).build();
			Facade facade = (Facade) BeanFactory.lookupBean(SynapseConstants.FACADE_JNDI);
			AddClaimsLogResponse response = facade.addClaimLog(request);
			if (response == null || !response.getResource().isSuccess()) {
				return Response.status(Response.Status.NOT_FOUND).entity(new ErrorResponse(404, "add claims log call fail for claim event number  " + request.getEventNo())).build();
			}
			return Response.status(Response.Status.CREATED).entity(response).build();
		}
		catch (HttpException e) {
			Synlog.error(e.getMessage(), ExceptionUtil.getStackTrace(e));
			return Response.status(e.getStatus()).entity(new ErrorResponse(e.getStatus(), e.getMessage())).build();
		}
		catch (Exception e) {
			Synlog.error(e.getMessage(), ExceptionUtil.getStackTrace(e));
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(new ErrorResponse(500, e.getMessage())).build();
		}
	}

	/**
	 * [getClaimCase]
	 * <p>
	 * Gets the case detail of a specific case linked to a claim.
	 * <p>
	 * <p>
	 * <strong>Dependencies</strong>
	 * <ul>
	 * </ul>
	 * </p>
	 * 
	 * @param claimNumber
	 *            The Claim number to search on.
	 * @param caseNumber
	 *            The case number in question.
	 * @return za.co.ominsure.synapse.lob.claim.backend.vo.Case
	 */
	@GET
	@Path("/claim/{claimNumber}/case/{caseNumber}")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Response getClaimCase(@PathParam("claimNumber") long claimNumber, @PathParam("caseNumber") long caseNumber) {
		try {
			Synlog.setAuditSearchFilter(new StringBuilder().append(CLAIM_NUMBER).append(claimNumber).append(CASE_NUMBER).append(caseNumber).toString());
			Facade facade = (Facade) BeanFactory.lookupBean(SynapseConstants.FACADE_JNDI);
			Case c = facade.getCaseInfo(claimNumber, caseNumber);
			if (c == null)
				return Response.status(Response.Status.NOT_FOUND).entity(new ErrorResponse(404, "No case found for claim number " + claimNumber + " and case number " + caseNumber)).build();

			return Response.status(Response.Status.OK).entity(c).build();
		}

		catch (Exception e) {
			Synlog.error(e.getMessage(), ExceptionUtil.getStackTrace(e));
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(new ErrorResponse(500, e.getMessage())).build();
		}
	}

	/**
	 * [getPolicyStatusDetail]
	 * <p>
	 * Gets existing active policy number from iwyze REST end point.
	 * 
	 *             <p>
	 *             <strong>Dependencies</strong>
	 *             <ul>
	 *             </ul>
	 *             </p>
	 * 
	 * @param policyHolderId
	 *            The policyHolderId to search on.
	 * @param secret
	 * 
	 * @return za.co.ominsure.synapse.iwyze.lob.backend.vo.GetPolicyNumberResponse
	 */
	@GET
	@Path("/policy/status/detail/{policyHolderId}/{secret}")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Response getPolicyDetail(@PathParam("policyHolderId") String policyHolderId, @PathParam("secret") String secret) {
		try {
			Synlog.setAuditSearchFilter(new StringBuilder().append(POLICY_HOLDER_ID).append(policyHolderId).toString());
			Facade facade = (Facade) BeanFactory.lookupBean(SynapseConstants.FACADE_JNDI);
			GetPolicyNumberResponse response = facade.getPolicyDetail(policyHolderId, secret);
			return Response.status(Response.Status.OK).entity(response).build();
		}
		catch (HttpException e) {
			Synlog.error(e.getMessage(), ExceptionUtil.getStackTrace(e));
			return Response.status(e.getStatus()).entity(new ErrorResponse(e.getStatus(), e.getMessage())).build();
		}
		catch (Exception e) {
			Synlog.error(e.getMessage(), ExceptionUtil.getStackTrace(e));
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(new ErrorResponse(500, e.getMessage())).build();
		}
	}

	/**
	 * [getClaimThirdParty]
	 * <p>
	 * Gets list of third parties for specific case.
	 * </p>
	 * <p>
	 * <strong>Dependencies</strong>
	 * <ul>
	 * <li>Source of Data: http://zamfqappv101.mufeq.co.za:7112/TIAServices/services/Party/PartyRouterService?wsdl</li>
	 * <li>Calls Iwyze TIA mediators: opGetParty</li>
	 * </ul>
	 * </p>
	 * 
	 * @param nameIdNumber
	 *            The nameIdNumber to search on.
	 * @param caseNumber
	 *            Iwyze claimNo required to get the sitename for that specific event number.
	 * @param pagesize
	 *            The number of records to return in each retrieve request.
	 * @param offset
	 *            The offset record number of the page to retrieve.
	 * @return za.co.ominsure.synapse.cdm.nonapproved.ThirdParties
	 */
	@GET
	@Path("/claim/{nameIdNumber}/case/{siteName}/thirdparty")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Response getClaimThirdParty(@PathParam("nameIdNumber") String nameIdNumber, @PathParam("siteName") String siteName, @DefaultValue("100") @QueryParam(SynapseConstants.PAGESIZE) Integer pagesize, @DefaultValue("0") @QueryParam(SynapseConstants.OFFSET) Integer offset) {
		try {
			Synlog.setAuditSearchFilter(new StringBuilder().append("nameIdNumber=").append(nameIdNumber).append("site name=").append(siteName).toString());
			Facade facade = (Facade) BeanFactory.lookupBean(SynapseConstants.FACADE_JNDI);
			ThirdParties c = facade.getThirdPartyInfo(nameIdNumber, siteName, offset == null ? 0 : offset, pagesize == null ? SynapseConstants.DEFAULT_PAGESIZE : pagesize);
			if (c == null)
				return Response.status(Response.Status.NOT_FOUND).entity(new ErrorResponse(404, "No third parties found for claim: " + nameIdNumber + " and site name: " + siteName)).build();

			return Response.status(Response.Status.OK).entity(c).build();
		}
		catch (HttpException e) {
			Synlog.error(e.getMessage(), ExceptionUtil.getStackTrace(e));
			return Response.status(e.getStatus()).entity(new ErrorResponse(e.getStatus(), e.getMessage())).build();
		}
		catch (Exception e) {
			Synlog.error(e.getMessage(), ExceptionUtil.getStackTrace(e));
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(new ErrorResponse(500, e.getMessage())).build();
		}
	}

	/**
	 * [getpolicyInformation]
	 * <p>
	 * Requests the policy details given a policy number. <br>
	 * </p>
	 * <p>
	 * <strong>Dependencies</strong>
	 * <ul>
	 * <li>Source of Data: http://zamfqappv101.mufeq.co.za:7112/TIAServices/services/Policy/PolicyAtomicRouterService?wsdl</li>
	 * <li>Calls Iwyze TIA mediators: opGetPolicy</li>
	 * </ul>
	 * </p>
	 *
	 * @param policyNumber
	 *            The Policy number for which the details is being requested
	 * @param caseNumber
	 *            The iwyze claimNo required to get the sitename for that specific iwyze event number.
	 * @param effectiveDate
	 *            The date from when to start the search in <a href="https://en.wikipedia.org/wiki/ISO_8601">ISO 8601</a> format (yyyy-MM-dd)
	 * @param lossRatioYears
	 *            The years for which the loss ratio needs to be calculated. This is either 3 or 5 years. If lossRatioYears==NULL loss ratio is not calculated.
	 * @return za.co.ominsure.synapse.lob.policy.backend.vo.Policy The Policy details
	 */
	@GET
	@Path("/policy/{policyNumber}/{caseNumber}")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Response getpolicyInformation(@PathParam("policyNumber") Long policyNumber, @PathParam("caseNumber") String caseNumber, @DefaultValue("") @QueryParam("effectiveDate") String effectiveDate, @DefaultValue("0") @QueryParam("lossRatioYears") Long lossRatioYears,
			@DefaultValue("0") @QueryParam("objectSequenceNumber") String objectSequenceNumber) {
		try {
			Synlog.setAuditSearchFilter(new StringBuilder().append(POLICY_NUMBER).append(policyNumber).toString());
			Facade facade = (Facade) BeanFactory.lookupBean(SynapseConstants.FACADE_JNDI);
			if (effectiveDate != null && effectiveDate.trim().length() == 0)
				effectiveDate = null;
			if (lossRatioYears != null && lossRatioYears == 0)
				lossRatioYears = null;
			PolicyOld policy = facade.getpolicyInformation(policyNumber, caseNumber, effectiveDate, lossRatioYears, objectSequenceNumber);
			if (policy == null)
				return Response.status(Response.Status.NOT_FOUND).entity(new ErrorResponse(404, "No policy found for policy number: " + policyNumber)).build();
			return Response.status(Response.Status.OK).entity(policy).build();
		}
		catch (HttpException e) {
			Synlog.error(e.getMessage(), ExceptionUtil.getStackTrace(e));
			return Response.status(e.getStatus()).entity(new ErrorResponse(e.getStatus(), e.getMessage())).build();
		}
		catch (Exception e) {
			Synlog.error(e.getMessage() == null ? e.toString() : e.getMessage(), ExceptionUtil.getStackTrace(e));
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(new ErrorResponse(500, e.getMessage())).build();
		}
	}

	/**
	 * [getClaimPayments]
	 * <p>
	 * Returns a list of claim payments.
	 * </p>
	 * <p>
	 * <strong>Dependencies</strong>
	 * <ul>
	 * </ul>
	 * </p>
	 * 
	 * @param claimNumber
	 *            The Claim number to search on.
	 * @param pagesize
	 *            The number of records to return in each retrieve request.
	 * @param offset
	 *            The offset record number of the page to retrieve.
	 * @return za.co.ominsure.synapse.cdm.Payments
	 */
	@GET
	@Path("/claim/{claimNumber}/payment")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Response getClaimPayments(@PathParam("claimNumber") String claimNumber, @DefaultValue("2000") @QueryParam(SynapseConstants.PAGESIZE) Integer pagesize, @DefaultValue("0") @QueryParam(SynapseConstants.OFFSET) Integer offset) {
		try {
			Synlog.setAuditSearchFilter(new StringBuilder().append(CLAIM_NUMBER).append(claimNumber).toString());
			Facade facade = (Facade) BeanFactory.lookupBean(SynapseConstants.FACADE_JNDI);
			offset = offset == null ? 0 : offset;
			pagesize = pagesize == null ? SynapseConstants.OFFPLATFORM_DEFAULT_PAGESIZE : pagesize;
			Payments payments = facade.getPayments(claimNumber, offset, pagesize);
			if (payments == null || payments.getPaymentList().isEmpty()) {
				return Response.status(Response.Status.NOT_FOUND).entity(new ErrorResponse(404, "No payments found for claim " + claimNumber + " at offset " + offset + " with page size " + pagesize)).build();
			}
			return Response.status(Response.Status.OK).entity(payments).build();
		}
		catch (Exception e) {
			Synlog.error(e.getMessage(), ExceptionUtil.getStackTrace(e));
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(new ErrorResponse(500, e.getMessage())).build();
		}
	}

	/**
	 * [getDocumentsForPolicy]
	 * <p>
	 * Returns a list of polcy documents.
	 * </p>
	 * <p>
	 *
	 * @param policyNumber
	 *            The policyNumber to search on.
	 * @return za.co.ominsure.synapse.cdm.common.content.SearchResult
	 */
	@GET
	@Path("/policy/{policyNumber}/content")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Response getDocumentsForPolicy(@PathParam("policyNumber") String policyNumber) {
		try {
			Facade facade = (Facade) BeanFactory.lookupBean("java:comp/env/Facade");
			SearchRequest searchRequest = new SearchRequest();
			SearchFields searchFields = new SearchFields();
			SearchField searchField1 = new SearchField();
			searchField1.setKey("POLICY_NUMBER");
			searchField1.setOperator(Operator.EQUAL);
			searchField1.setValue(policyNumber);
			searchFields.getSearchField().add(searchField1);
			searchRequest.setSearchFields(searchFields);
			searchRequest.setOffset(0);
			searchRequest.setPageSize(100);
			SearchResult searchResult = facade.getDocumentsForPolicy(searchRequest);

			if (searchResult == null || searchResult.getContents() == null || searchResult.getContents().getContent() == null || searchResult.getContents().getContent().isEmpty()) {
				return Response.status(Response.Status.NOT_FOUND).entity(new ErrorResponse(404, "No documents found for policy number: " + policyNumber)).build();
			}
			return Response.status(Response.Status.OK).entity(searchResult).build();
		}
		catch (HttpException e) {
			Synlog.error(e.getMessage(), ExceptionUtil.getStackTrace(e));
			return Response.status(e.getStatus()).entity(new ErrorResponse(e.getStatus(), e.getMessage())).build();
		}
		catch (Exception e) {
			Synlog.error(e.getMessage(), ExceptionUtil.getStackTrace(e));
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(new ErrorResponse(500, e.getMessage())).build();
		}
	}

	/**
	 * [getClaim]
	 * <p>
	 * Returns a list of claim audits.
	 * </p>
	 * <p>
	 * <strong>Dependencies</strong>
	 * <ul>
	 * <li>Source of Data: http://zamfqappv101.mufeq.co.za:7112/TIAServices/services/Claim/ClaimAtomicRouterService?wsdl</li>
	 * <li>Calls Iwyze TIA mediators: opGetClaim</li>
	 * </ul>
	 * </p>
	 *
	 * @param claimNumber
	 *            The Claim number to search on.
	 * @param pagesize
	 *            The number of records to return in each retrieve request.
	 * @param offset
	 *            The offset record number of the page to retrieve.
	 * @return za.co.ominsure.synapse.cdm.claim.Claim
	 */
	@GET
	@Path("/claim/{claimEventNumber}/{siteName}")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Response getClaim(@PathParam("claimEventNumber") String claimEventNumber, @PathParam("siteName") String siteName, @DefaultValue("100") @QueryParam(SynapseConstants.PAGESIZE) Integer pagesize, @DefaultValue("0") @QueryParam(SynapseConstants.OFFSET) Integer offset) {
		try {
			Synlog.setAuditSearchFilter(new StringBuilder().append(CLAIM_EVENT_NUMBER).append(claimEventNumber).toString());
			Facade facade = (Facade) BeanFactory.lookupBean(SynapseConstants.FACADE_JNDI);
			offset = offset == null ? 0 : offset;
			pagesize = pagesize == null ? SynapseConstants.DEFAULT_PAGESIZE : pagesize;
			Claim claim = facade.getClaim(claimEventNumber, siteName, offset, pagesize);
			if (claim == null) {
				return Response.status(Response.Status.NOT_FOUND).entity(new ErrorResponse(404, "No claim details found for claim number: " + claimEventNumber + " at offset: " + offset)).build();
			}
			return Response.status(Response.Status.OK).entity(claim).build();
		}
		catch (HttpException e) {
			Synlog.error(e.getMessage(), ExceptionUtil.getStackTrace(e));
			return Response.status(e.getStatus()).entity(new ErrorResponse(e.getStatus(), e.getMessage())).build();
		}
		catch (Exception e) {
			Synlog.error(e.getMessage(), ExceptionUtil.getStackTrace(e));
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(new ErrorResponse(500, e.getMessage())).build();
		}
	}

	/**
	 * [getAudits]
	 * <p>
	 * Returns a list of claim audits.
	 * </p>
	 * <p>
	 * <strong>Dependencies</strong>
	 * <ul>
	 * <li>calling iwyze REST api getClaimsLog</li>
	 * <li>calling iwyze REST api getClaimsLog</li>
	 * </ul>
	 * </p>
	 *
	 * @param claimNumber
	 *            The Claim number to search on.
	 * @param pagesize
	 *            The number of records to return in each retrieve request.
	 * @param offset
	 *            The offset record number of the page to retrieve.
	 * @return za.co.ominsure.synapse.cdm.claim.Claim
	 */
	@GET
	@Path("/claim/{claimEventNumber}/audit")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Response getAudits(@PathParam("claimEventNumber") String claimEventNumber, @DefaultValue("2000") @QueryParam(SynapseConstants.PAGESIZE) Integer pagesize, @DefaultValue("0") @QueryParam(SynapseConstants.OFFSET) Integer offset) {
		try {
			Synlog.setAuditSearchFilter(new StringBuilder().append(CLAIM_EVENT_NUMBER).append(claimEventNumber).toString());
			Facade facade = (Facade) BeanFactory.lookupBean(SynapseConstants.FACADE_JNDI);
			offset = offset == null ? 0 : offset;
			pagesize = pagesize == null ? SynapseConstants.OFFPLATFORM_DEFAULT_PAGESIZE : pagesize;
			Audits audits = facade.getAudits(claimEventNumber, offset, pagesize);
			if (audits == null || audits.getAudit().isEmpty()) {
				return Response.status(Response.Status.NOT_FOUND).entity(new ErrorResponse(404, "No claim audits found for claim number: " + claimEventNumber + " at offset: " + offset)).build();
			}
			return Response.status(Response.Status.OK).entity(audits).build();
		}
		catch (HttpException e) {
			Synlog.error(e.getMessage(), ExceptionUtil.getStackTrace(e));
			return Response.status(e.getStatus()).entity(new ErrorResponse(e.getStatus(), e.getMessage())).build();
		}
		catch (Exception e) {
			Synlog.error(e.getMessage(), ExceptionUtil.getStackTrace(e));
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(new ErrorResponse(500, e.getMessage())).build();
		}
	}

	/**
	 * [getClaimsLog]
	 * <p>
	 * Returns a list of claim audits.
	 * </p>
	 * <p>
	 * <strong>Dependencies</strong>
	 * <ul>
	 * <li>calling iwyze REST api</li>
	 * <li>calling iwyze REST api</li>
	 * </ul>
	 * </p>
	 *
	 * @param claimNumber
	 *            The claim number to search on. this service will return the all iwyze specific claims audits for the specific claim number.
	 * @return za.co.ominsure.synapse.iwyze.lob.backend.vo.Content
	 */
	@GET
	@Path("/claim/{claimNumber}/detail")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Response getClaimsLog(@PathParam("claimNumber") String claimNumber) {
		try {
			Synlog.setAuditSearchFilter(new StringBuilder().append(CLAIM_EVENT_NUMBER).append(claimNumber).toString());
			Facade facade = (Facade) BeanFactory.lookupBean(SynapseConstants.FACADE_JNDI);
			GetClaimsLogResponse response = facade.getClaimsLog(claimNumber);
			if (response.getContent().getGetClmLg().getClaEventLogArray().isEmpty()) {
				return Response.status(Response.Status.NOT_FOUND).entity(new ErrorResponse(404, "No claim audits found for claim number: " + claimNumber)).build();
			}
			return Response.status(Response.Status.OK).entity(response).build();
		}
		catch (HttpException e) {
			Synlog.error(e.getMessage(), ExceptionUtil.getStackTrace(e));
			return Response.status(e.getStatus()).entity(new ErrorResponse(e.getStatus(), e.getMessage())).build();
		}
		catch (Exception e) {
			Synlog.error(e.getMessage(), ExceptionUtil.getStackTrace(e));
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(new ErrorResponse(500, e.getMessage())).build();
		}
	}

	/**
	 * [getAJSData]
	 * <p>
	 * Return AJS createRequest object
	 * </p>
	 * <p>
	 * <strong>Dependencies</strong>
	 * <ul>
	 * <li>Calls Iwyze TIA mediators: 1.opGetClaim 2.opGetPolicy 3.opGetParty 4.opSearchPolicy 5.opSearchSubClaim</li>
	 * </ul>
	 * </p>
	 *
	 * @param claimNumber
	 *            this will be the claim event number of Iwyze. The claim event number to search on.
	 * @return za.co.ominsure.synapse.cdm.workflow.ajs.CreateRequest
	 */
	@GET
	@Path("/claim/ajs/{claimNumber}")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Response getAJSData(@PathParam("claimNumber") String claimNumber) {
		try {
			Synlog.setAuditSearchFilter(new StringBuilder().append(CLAIM_NUMBER).append(claimNumber).toString());
			Facade facade = (Facade) BeanFactory.lookupBean(SynapseConstants.FACADE_JNDI);
			CreateRequest request = facade.getAJSData(claimNumber);
			if (request == null) {
				return Response.status(Response.Status.NOT_FOUND).entity(new ErrorResponse(404, "no ajs data created by " + claimNumber)).build();
			}
			return Response.status(Response.Status.OK).entity(request).build();
		}
		catch (HttpException e) {
			Synlog.error(e.getMessage(), ExceptionUtil.getStackTrace(e));
			return Response.status(e.getStatus()).entity(new ErrorResponse(e.getStatus(), e.getMessage())).build();
		}
		catch (Exception e) {
			Synlog.error(e.getMessage(), ExceptionUtil.getStackTrace(e));
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(new ErrorResponse(500, e.getMessage())).build();
		}
	}

	/**
	 * [searchPolicy]
	 * <p>
	 * returns generic cdm PolicyOld object search by nameIdNumber of Iwyze.
	 * </p>
	 * <p>
	 * <strong>Dependencies</strong>
	 * <ul>
	 * <li>Source of Data: http://zamfqappv101.mufeq.co.za:7112/TIAServices/services/Policy/PolicyAtomicRouterService?wsdl</li>
	 * <li>Calls Iwyze TIA mediators: opSearchPolicy</li>
	 * </ul>
	 * </p>
	 * 
	 * @param caseNumber
	 *            iwyze claimNo required to get the sitename for that specific event number.
	 * @param nameIdNumber
	 *            this service will be used to get the iwyze policy number by name id number.
	 * @return Response.Status.CREATED on success else Response.Status.INTERNAL_SERVER_ERROR on error
	 */
	@POST
	@Path("/policy/{nameIdNumber}/{caseNumber}")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Response searchPolicy(@PathParam("nameIdNumber") String nameIdNumber, @PathParam("caseNumber") String caseNumber) {
		try {
			Facade facade = (Facade) BeanFactory.lookupBean(SynapseConstants.FACADE_JNDI);
			PolicyOld policyOld = facade.searchPolicy(nameIdNumber, caseNumber);
			if (policyOld == null) {
				return Response.status(Response.Status.NOT_FOUND).entity(new ErrorResponse(404, "no policy found for  " + nameIdNumber)).build();
			}
			return Response.status(Response.Status.CREATED).entity(policyOld).build();
		}
		catch (HttpException e) {
			Synlog.error(e.getMessage(), ExceptionUtil.getStackTrace(e));
			return Response.status(e.getStatus()).entity(new ErrorResponse(e.getStatus(), e.getMessage())).build();
		}
		catch (Exception e) {
			Synlog.error(e.getMessage(), ExceptionUtil.getStackTrace(e));
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(new ErrorResponse(500, e.getMessage())).build();
		}
	}

	/**
	 * [searchSubClaim]
	 * <p>
	 * retrieves other third party details of Iwyze claim.
	 * </p>
	 * <p>
	 * <strong>Dependencies</strong>
	 * <ul>
	 * <li>Source of Data: http://zamfqappv101.mufeq.co.za:7112/TIAServices/services/Claim/ClaimInteractiveRouterService?wsdl</li>
	 * <li>Calls Iwyze TIA mediators: opSearchSubClaim</li>
	 * </ul>
	 * </p>
	 * 
	 * @param claimNumber
	 *            iwyze claimNo required
	 * @return Response.Status.CREATED on success else Response.Status.INTERNAL_SERVER_ERROR on error
	 */
	@POST
	@Path("/claim/subclaim/{claimNumber}/{siteName}")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Response searchSubClaim(@PathParam("claimNumber") String claimNumber, @PathParam("siteName") String siteName) {
		try {
			Facade facade = (Facade) BeanFactory.lookupBean(SynapseConstants.FACADE_JNDI);
			ThirdParties thirdParties = facade.searchSubClaim(claimNumber, siteName);
			if (thirdParties == null) {
				return Response.status(Response.Status.NOT_FOUND).entity(new ErrorResponse(404, "no subclaim found for  " + claimNumber)).build();
			}
			return Response.status(Response.Status.CREATED).entity(thirdParties).build();
		}
		catch (HttpException e) {
			Synlog.error(e.getMessage(), ExceptionUtil.getStackTrace(e));
			return Response.status(e.getStatus()).entity(new ErrorResponse(e.getStatus(), e.getMessage())).build();
		}
		catch (Exception e) {
			Synlog.error(e.getMessage(), ExceptionUtil.getStackTrace(e));
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(new ErrorResponse(500, e.getMessage())).build();
		}
	}

	/**
	 * [searchParty]
	 * <p>
	 * returns generic cdm Claim object search by idNumber of Iwyze.
	 * </p>
	 * <p>
	 * <strong>Dependencies</strong>
	 * <ul>
	 * <li>Source of Data: http://zamfqappv101.mufeq.co.za:7112/TIAServices/services/Party/PartyRouterService?wsdl</li>
	 * <li>Calls Iwyze TIA mediators: opSearchParty</li>
	 * </ul>
	 * </p>
	 * 
	 * @param caseNumber
	 *            iwyze claimNo required to get the sitename.
	 * @param idNumber
	 *            this service will be used to get the iwyze policy holder name by idNumber.
	 * @return Response.Status.CREATED on success else Response.Status.INTERNAL_SERVER_ERROR on error
	 */
	@POST
	@Path("/party/{idNumber}/{siteName}")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Response searchParty(@PathParam("idNumber") String idNumber, @PathParam("siteName") String siteName) {
		try {
			Facade facade = (Facade) BeanFactory.lookupBean(SynapseConstants.FACADE_JNDI);
			Claim claim = facade.searchParty(idNumber, siteName);
			if (claim == null) {
				return Response.status(Response.Status.NOT_FOUND).entity(new ErrorResponse(404, "no party found for  " + idNumber)).build();
			}
			return Response.status(Response.Status.CREATED).entity(claim).build();
		}
		catch (HttpException e) {
			Synlog.error(e.getMessage(), ExceptionUtil.getStackTrace(e));
			return Response.status(e.getStatus()).entity(new ErrorResponse(e.getStatus(), e.getMessage())).build();
		}
		catch (Exception e) {
			Synlog.error(e.getMessage(), ExceptionUtil.getStackTrace(e));
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(new ErrorResponse(500, e.getMessage())).build();
		}
	}

	/**
	 * [claimStatusUpdate] This service will update the status to offplatform system iwyze receiving from AJS. this service called by AJS URL /offplatform/{platform}/account/status(offplatformStatusCodeUpdate)
	 * 
	 * @param claimNumber
	 *            the claim number that the status to be updated on iwyze system.
	 * @return void
	 */

	@PUT
	@Path("/claim/{claimNumber}/status")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Response claimStatusUpdate(@PathParam("claimNumber") String claimNumber, RecoveryStatusChangeRequest request) {
		try {
			Facade facade = (Facade) BeanFactory.lookupBean(SynapseConstants.FACADE_JNDI);
			facade.claimStatusUpdate(claimNumber, request);
			return Response.status(Response.Status.NO_CONTENT).build();
		}
		catch (HttpException e) {
			Synlog.error(e.toString(), ExceptionUtil.getStackTrace(e));
			return Response.status(e.getStatus()).entity(new ErrorResponse(e.getStatus(), e.toString())).build();
		}
		catch (Exception e) {
			Synlog.error(e.toString(), ExceptionUtil.getStackTrace(e));
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(new ErrorResponse(500, e.toString())).build();
		}
	}

	/**
	 * [getServiceSupplier]
	 * <p>
	 * Gets existing service supplier details from wsdl:http://zamfqappv101.mufeq.co.za:7112/TIAServices/services/Party/ServiceSupplierRouterService?wsdl
	 * 
	 * @receiverIdNo iwyze receiverIdNo required to get service supplier details.
	 *               <p>
	 *               <strong>Dependencies</strong>
	 *               <ul>
	 *               </ul>
	 *               </p>
	 * 
	 * @param siteName
	 *            The siteName to search on.
	 * 
	 * @return za.co.ominsure.synapse.iwyze.lob.backend.servicesupplier.vo.GetServiceSupplierResponse
	 */
	@GET
	@Path("/supplier/detail/{receiverIdNumber}/{siteName}")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Response getServiceSupplier(@PathParam("receiverIdNumber") String receiverIdNumber, @PathParam("siteName") String siteName) {
		try {
			Synlog.setAuditSearchFilter(new StringBuilder().append(RECEIVER_ID_NO).append(receiverIdNumber).toString());
			Facade facade = (Facade) BeanFactory.lookupBean(SynapseConstants.FACADE_JNDI);
			GetServiceSupplierResponse response = facade.getServiceSupplier(receiverIdNumber, siteName);
			if (response == null) {
				return Response.status(Response.Status.NOT_FOUND).entity(new ErrorResponse(404, "No supplier details found for receiver Id number: " + receiverIdNumber)).build();
			}
			return Response.status(Response.Status.OK).entity(response).build();
		}
		catch (HttpException e) {
			Synlog.error(e.getMessage(), ExceptionUtil.getStackTrace(e));
			return Response.status(e.getStatus()).entity(new ErrorResponse(e.getStatus(), e.getMessage())).build();
		}
		catch (Exception e) {
			Synlog.error(e.getMessage(), ExceptionUtil.getStackTrace(e));
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(new ErrorResponse(500, e.getMessage())).build();
		}
	}
	
	/**
	 * [searchCaseItem]
	 * <p>
	 * returns generic cdm Claim object search by idNumber of Iwyze.
	 * </p>
	 * <p>
	 * <strong>Dependencies</strong>
	 * <ul>
	 * <li>Source of Data: http://zamfqappv101.mufeq.co.za:7112/TIAServices/services/Case/CaseRouterService?wsdl</li>
	 * <li>Calls Iwyze TIA mediators: searchCaseItemRequest</li>
	 * </ul>
	 * </p>
	 * 
	 * @param caseNumber
	 *            iwyze cla_case_no
	 * @param sitename
	 *            iwyze sitename
	 * @return Response.Status.CREATED on success else Response.Status.INTERNAL_SERVER_ERROR on error
	 */
	@POST
	@Path("/caseitem/{claimNumber}/{siteName}")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Response searchCaseItem(@PathParam("claimNumber") String claimNumber, @PathParam("siteName") String siteName) {
		try {
			Facade facade = (Facade) BeanFactory.lookupBean(SynapseConstants.FACADE_JNDI);
			CaseItemGenericResponse response = facade.searchCaseItem(claimNumber, siteName);
			if (response == null) {
				return Response.status(Response.Status.NOT_FOUND).entity(new ErrorResponse(404, "no CLEG type found for claim number " + claimNumber + " please try again later.")).build();
			}
			return Response.status(Response.Status.CREATED).entity(response).build();
		}
		catch (HttpException e) {
			Synlog.error(e.getMessage(), ExceptionUtil.getStackTrace(e));
			return Response.status(e.getStatus()).entity(new ErrorResponse(e.getStatus(), e.getMessage())).build();
		}
		catch (Exception e) {
			Synlog.error(e.getMessage(), ExceptionUtil.getStackTrace(e));
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(new ErrorResponse(500, e.getMessage())).build();
		}
	}
}
