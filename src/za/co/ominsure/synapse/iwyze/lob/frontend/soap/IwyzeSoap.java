package za.co.ominsure.synapse.iwyze.lob.frontend.soap;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.ParameterStyle;
import javax.jws.soap.SOAPBinding.Style;
import javax.xml.ws.BindingType;
import javax.xml.ws.WebServiceException;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.utils.URIBuilder;

import za.co.ominsure.synapse.cdm.claim.Audits;
import za.co.ominsure.synapse.cdm.claim.Case;
import za.co.ominsure.synapse.cdm.common.content.SearchResult;
import za.co.ominsure.synapse.cdm.finance.Payments;
import za.co.ominsure.synapse.cdm.nonapproved.ThirdParties;
import za.co.ominsure.synapse.cdm.policy.PolicyOld;
import za.co.ominsure.synapse.common.config.GlobalConfig;
import za.co.ominsure.synapse.common.constants.SynapseConstants;
import za.co.ominsure.synapse.common.exception.HttpException;
import za.co.ominsure.synapse.common.rest.ServiceBridge;

@WebService(targetNamespace = SynapseConstants.DEFAULT_WORKSPACE, name = "IwyzeSoap")
@BindingType(value = javax.xml.ws.soap.SOAPBinding.SOAP12HTTP_BINDING)
@SOAPBinding(style = Style.DOCUMENT, parameterStyle = ParameterStyle.WRAPPED)
public class IwyzeSoap {

	private ServiceBridge serviceBridge = new ServiceBridge();

	/**
	 * @see za.co.ominsure.synapse.iwyze.lob.frontend.rest.IwyzeRest#getClaimAudits(long, Integer, Integer)
	 * @param token
	 *            The Security token to use
	 * @param claimNumber
	 *            The Claim number in question
	 * @param pagesize
	 *            The number of records to retrieve in each request
	 * @param offset
	 *            The record number to start from for the retrieval
	 * @return a list of Audits
	 */
	@WebMethod
	public @WebResult(name = "claimAuditResponse", targetNamespace = "http://synapse.ominsure.co.za/webservice/claim") Audits getClaimAudits(@WebParam(name = "token", targetNamespace = SynapseConstants.DEFAULT_WORKSPACE) String token,
			@WebParam(name = "claimNumber", targetNamespace = SynapseConstants.DEFAULT_WORKSPACE) long claimNumber, @WebParam(name = SynapseConstants.PAGESIZE, targetNamespace = SynapseConstants.DEFAULT_WORKSPACE) Integer pagesize,
			@WebParam(name = SynapseConstants.OFFSET, targetNamespace = SynapseConstants.DEFAULT_WORKSPACE) Integer offset) {
		try {
			Map<String, String> inputHeaders = new HashMap<>();
			inputHeaders.put(SynapseConstants.ACCEPT, SynapseConstants.APPLICATION_JSON);
			inputHeaders.put(SynapseConstants.CONTENT_TYPE, SynapseConstants.APPLICATION_JSON);
			if (token != null)
				inputHeaders.put(SynapseConstants.AUTHORIZATION, SynapseConstants.BEARER + token);

			URIBuilder uriBuilder = new URIBuilder(GlobalConfig.getInstance().getProperty(SynapseConstants.SERVICE_LOCATION));
			String path = "/synapse/iwyze/lob/rest/claim/" + claimNumber;

			uriBuilder.setPath(path);

			if (pagesize != null) {
				uriBuilder.setParameter(SynapseConstants.PAGESIZE, pagesize.toString());
			}
			if (offset != null) {
				uriBuilder.setParameter(SynapseConstants.OFFSET, offset.toString());
			}
			URI uri = uriBuilder.build();

			Audits response = (Audits) serviceBridge.callExternalService(uri.toString(), "GET", null, Audits.class, inputHeaders);
			return response;
		}
		catch (HttpException e) {
			if (e.getStatus() == 404)
				return new Audits();
			throw new WebServiceException(e.getMessage());
		}
		catch (Exception e) {
			throw new WebServiceException(e.getMessage());
		}
	}

	/**
	 * @see za.co.ominsure.synapse.iwyze.lob.frontend.rest.IwyzeRest#addClaimAudit(Audits)
	 * @param token
	 *            The Security token to use
	 * @param audits
	 *            A list of Audits to be added.
	 */
	@WebMethod
	public void addClaimAudit(@WebParam(name = "token", targetNamespace = SynapseConstants.DEFAULT_WORKSPACE) String token, @WebParam(name = "claimNumber", targetNamespace = SynapseConstants.DEFAULT_WORKSPACE) String claimNumber,
			@WebParam(name = "audits", targetNamespace = "http://synapse.ominsure.co.za/webservice/claim") Audits audits) {
		try {
			Map<String, String> inputHeaders = new HashMap<>();
			inputHeaders.put(SynapseConstants.ACCEPT, SynapseConstants.APPLICATION_JSON);
			inputHeaders.put(SynapseConstants.CONTENT_TYPE, SynapseConstants.APPLICATION_JSON);
			if (token != null)
				inputHeaders.put(SynapseConstants.AUTHORIZATION, SynapseConstants.BEARER + token);

			serviceBridge.callExternalService(GlobalConfig.getInstance().getProperty(SynapseConstants.SERVICE_LOCATION) + "/synapse/iwyze/lob/rest/claim/" + claimNumber + "/audit", "POST", audits, null, inputHeaders);
		}
		catch (Exception e) {
			throw new WebServiceException(e.getMessage());
		}
	}

	/**
	 * @see za.co.ominsure.synapse.iwyze.lob.frontend.rest.IwyzeRest#getClaimThirdParty(long, long, Integer, Integer)
	 * @param token
	 *            The Security token to use
	 * @param claimNumber
	 *            The Claim number in question
	 * @param caseNumber
	 *            The Case number as a criteria
	 * @param pagesize
	 *            The number of records to retrieve in each request
	 * @param offset
	 *            The record number to start from for the retrieval
	 * @return a list of thrid parties
	 */
	@WebMethod
	public @WebResult(name = "thirdPartyResponse", targetNamespace = "http://synapse.ominsure.co.za/webservice/nonapproved") ThirdParties getClaimThirdParty(@WebParam(name = "token", targetNamespace = SynapseConstants.DEFAULT_WORKSPACE) String token,
			@WebParam(name = "claimNumber", targetNamespace = SynapseConstants.DEFAULT_WORKSPACE) long claimNumber, @WebParam(name = "caseNumber", targetNamespace = SynapseConstants.DEFAULT_WORKSPACE) long caseNumber,
			@WebParam(name = SynapseConstants.PAGESIZE, targetNamespace = SynapseConstants.DEFAULT_WORKSPACE) Integer pagesize, @WebParam(name = SynapseConstants.OFFSET, targetNamespace = SynapseConstants.DEFAULT_WORKSPACE) Integer offset) {
		try {
			Map<String, String> inputHeaders = new HashMap<>();
			inputHeaders.put(SynapseConstants.ACCEPT, SynapseConstants.APPLICATION_JSON);
			inputHeaders.put(SynapseConstants.CONTENT_TYPE, SynapseConstants.APPLICATION_JSON);
			if (token != null)
				inputHeaders.put(SynapseConstants.AUTHORIZATION, SynapseConstants.BEARER + token);

			URIBuilder uriBuilder = new URIBuilder(GlobalConfig.getInstance().getProperty(SynapseConstants.SERVICE_LOCATION));
			String path = "/synapse/iwyze/lob/rest/claim/" + claimNumber + "/case/" + caseNumber + "/thirdparty";

			uriBuilder.setPath(path);

			if (pagesize != null) {
				uriBuilder.setParameter(SynapseConstants.PAGESIZE, pagesize.toString());
			}
			if (offset != null) {
				uriBuilder.setParameter(SynapseConstants.OFFSET, offset.toString());
			}
			URI uri = uriBuilder.build();

			ThirdParties response = (ThirdParties) serviceBridge.callExternalService(uri.toString(), "GET", null, ThirdParties.class, inputHeaders);
			return response;
		}
		catch (HttpException e) {
			if (e.getStatus() == 404)
				return new ThirdParties();
			throw new WebServiceException(e.getMessage());
		}
		catch (Exception e) {
			throw new WebServiceException(e.getMessage());
		}
	}

	/**
	 * @see za.co.ominsure.synapse.iwyze.lob.frontend.rest.IwyzeRest#getClaimCase(long, long)
	 * @param token
	 *            The Security token to use
	 * @param claimNumber
	 *            The Claim number in question
	 * @param caseNumber
	 *            The Case number as a criteria
	 * @return a case object
	 */
	@WebMethod
	public @WebResult(name = "claimCaseResponse", targetNamespace = "http://synapse.ominsure.co.za/webservice/claim") Case getClaimCase(@WebParam(name = "token", targetNamespace = SynapseConstants.DEFAULT_WORKSPACE) String token,
			@WebParam(name = "claimNumber", targetNamespace = SynapseConstants.DEFAULT_WORKSPACE) long claimNumber, @WebParam(name = "caseNumber", targetNamespace = SynapseConstants.DEFAULT_WORKSPACE) long caseNumber) {
		try {
			Map<String, String> inputHeaders = new HashMap<>();
			inputHeaders.put(SynapseConstants.ACCEPT, SynapseConstants.APPLICATION_JSON);
			inputHeaders.put(SynapseConstants.CONTENT_TYPE, SynapseConstants.APPLICATION_JSON);
			if (token != null)
				inputHeaders.put(SynapseConstants.AUTHORIZATION, SynapseConstants.BEARER + token);

			Case response = (Case) serviceBridge.callExternalService(GlobalConfig.getInstance().getProperty(SynapseConstants.SERVICE_LOCATION) + "/synapse/iwyze/lob/rest/claim/" + claimNumber + "/case/" + caseNumber, "GET", null, Case.class, inputHeaders);
			return response;
		}
		catch (HttpException e) {
			if (e.getStatus() == 404)
				return new Case();
			throw new WebServiceException(e.getMessage());
		}
		catch (Exception e) {
			throw new WebServiceException(e.getMessage());
		}
	}

	/**
	 * <p>
	 * Requests the policy details given a policy number. <br>
	 * </p>
	 * <p>
	 * <strong>Dependencies</strong>
	 * <ul>
	 * <li>Data source: jdbc/tiaDS</li>
	 * <li>TIA.IBBPM_POLICY.get_policy_info() stored procedure</li>
	 * </ul>
	 * </p>
	 *
	 * @param policyNumber
	 *            The Policy number for which the details is being requested
	 * @param effectiveDate
	 *            The date from when to start the search in <a href="https://en.wikipedia.org/wiki/ISO_8601">ISO 8601</a> format (yyyy-MM-dd)
	 * @param lossRatioYears
	 *            The years for which the loss ratio needs to be calculated. This is either 3 or 5 years. If lossRatioYears==NULL loss ratio is not calculated.
	 * @return za.co.ominsure.synapse.lob.policy.backend.vo.Policy The Policy details
	 */

	@WebMethod
	public @WebResult(name = "policyInfoResponse", targetNamespace = "http://synapse.ominsure.co.za/webservice/policy") PolicyOld getPolicyInfo(@WebParam(name = "token", targetNamespace = "http://synapse.ominsure.co.za/webservice") String token,
			@WebParam(name = "policyNumber", targetNamespace = "http://synapse.ominsure.co.za/webservice") long policyNumber, @WebParam(name = "lossRatioYears", targetNamespace = "http://synapse.ominsure.co.za/webservice") Long lossRatioYears,
			@WebParam(name = "effectiveDate", targetNamespace = "http://synapse.ominsure.co.za/webservice") String effectiveDate) {
		try {
			ServiceBridge serviceBridge = new ServiceBridge();
			Map<String, String> inputHeaders = new HashMap<>();
			inputHeaders.put(SynapseConstants.ACCEPT, SynapseConstants.APPLICATION_JSON);
			inputHeaders.put(SynapseConstants.CONTENT_TYPE, SynapseConstants.APPLICATION_JSON);
			if (token != null)
				inputHeaders.put(SynapseConstants.AUTHORIZATION, SynapseConstants.BEARER + token);

			URIBuilder uriBuilder = new URIBuilder(GlobalConfig.getInstance().getProperty(SynapseConstants.SERVICE_LOCATION));
			uriBuilder.setPath("/synapse/iwyze/lob/rest/policy/" + policyNumber);

			if (lossRatioYears != null && lossRatioYears > 0) {
				uriBuilder.setParameter("lossRatioYears", lossRatioYears.toString());
			}
			if (!StringUtils.isBlank(effectiveDate)) {
				uriBuilder.setParameter("effectiveDate", effectiveDate);
			}
			URI uri = uriBuilder.build();

			PolicyOld response = (PolicyOld) serviceBridge.callExternalService(uri.toString(), "GET", null, PolicyOld.class, inputHeaders);
			return response;
		}
		catch (HttpException e) {
			if (e.getStatus() == 404)
				return new PolicyOld();
			throw new WebServiceException(e.getMessage());
		}
		catch (Exception e) {
			throw new WebServiceException(e.getMessage());
		}
	}

	/**
	 * @see za.co.ominsure.synapse.iwyze.lob.frontend.rest.IwyzeRest#getClaimPayments(long, Integer, Integer)
	 * @param token
	 *            The Security token to use
	 * @param claimNumber
	 *            The Claim number in question
	 * @param pagesize
	 *            The number of records to retrieve in each request
	 * @param offset
	 *            The record number to start from for the retrieval
	 * @return a list of Payments
	 */
	@WebMethod
	public @WebResult(name = "claimPaymentsResponse", targetNamespace = "http://synapse.ominsure.co.za/webservice/finance") Payments getClaimPayments(@WebParam(name = "token", targetNamespace = SynapseConstants.DEFAULT_WORKSPACE) String token,
			@WebParam(name = "claimNumber", targetNamespace = SynapseConstants.DEFAULT_WORKSPACE) long claimNumber, @WebParam(name = SynapseConstants.PAGESIZE, targetNamespace = SynapseConstants.DEFAULT_WORKSPACE) Integer pagesize,
			@WebParam(name = SynapseConstants.OFFSET, targetNamespace = SynapseConstants.DEFAULT_WORKSPACE) Integer offset) {
		try {
			Map<String, String> inputHeaders = new HashMap<>();
			inputHeaders.put(SynapseConstants.ACCEPT, SynapseConstants.APPLICATION_JSON);
			inputHeaders.put(SynapseConstants.CONTENT_TYPE, SynapseConstants.APPLICATION_JSON);
			if (token != null)
				inputHeaders.put(SynapseConstants.AUTHORIZATION, SynapseConstants.BEARER + token);

			URIBuilder uriBuilder = new URIBuilder(GlobalConfig.getInstance().getProperty(SynapseConstants.SERVICE_LOCATION));
			String path = "/synapse/iwyze/lob/rest/claim/" + claimNumber + "/payment";

			uriBuilder.setPath(path);

			if (pagesize != null) {
				uriBuilder.setParameter(SynapseConstants.PAGESIZE, pagesize.toString());
			}
			if (offset != null) {
				uriBuilder.setParameter(SynapseConstants.OFFSET, offset.toString());
			}
			URI uri = uriBuilder.build();

			Payments response = (Payments) serviceBridge.callExternalService(uri.toString(), "GET", null, Payments.class, inputHeaders);
			return response;
		}
		catch (HttpException e) {
			if (e.getStatus() == 404)
				return new Payments();
			throw new WebServiceException(e.getMessage());
		}
		catch (Exception e) {
			throw new WebServiceException(e.getMessage());
		}
	}

	/**
	 * @see za.co.ominsure.synapse.iwyze.lob.frontend.rest.IwyzeRest#getDocumentsForPolicy(String)
	 * @param token
	 *            The Security token to use
	 * @param policyNumber
	 *            The policy number in question
	 * @return SearchResult
	 */
	@WebMethod
	public @WebResult(name = "documentResponse", targetNamespace = "http://synapse.ominsure.co.za/webservice") SearchResult getDocumentsForPolicy(@WebParam(name = "token", targetNamespace = SynapseConstants.DEFAULT_WORKSPACE) String token,
			@WebParam(name = "policyNumber", targetNamespace = SynapseConstants.DEFAULT_WORKSPACE) String policyNumber) {
		try {
			Map<String, String> inputHeaders = new HashMap<>();
			inputHeaders.put(SynapseConstants.ACCEPT, SynapseConstants.APPLICATION_JSON);
			inputHeaders.put(SynapseConstants.CONTENT_TYPE, SynapseConstants.APPLICATION_JSON);
			if (token != null)
				inputHeaders.put(SynapseConstants.AUTHORIZATION, SynapseConstants.BEARER + token);

			URIBuilder uriBuilder = new URIBuilder(GlobalConfig.getInstance().getProperty(SynapseConstants.SERVICE_LOCATION));
			String path = "/synapse/iwyze/lob/rest/policy/" + policyNumber + "/content";

			uriBuilder.setPath(path);

			URI uri = uriBuilder.build();

			SearchResult response = (SearchResult) serviceBridge.callExternalService(uri.toString(), "GET", null, Audits.class, inputHeaders);
			return response;
		}
		catch (HttpException e) {
			if (e.getStatus() == 404)
				return new SearchResult();
			throw new WebServiceException(e.getMessage());
		}
		catch (Exception e) {
			throw new WebServiceException(e.getMessage());
		}
	}
}
